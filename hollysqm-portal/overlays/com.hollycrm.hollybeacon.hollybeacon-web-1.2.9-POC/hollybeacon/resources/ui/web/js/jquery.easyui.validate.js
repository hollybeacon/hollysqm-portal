$.extend($.fn.validatebox.defaults.rules, {
	specialChar:{
		validator: function (value, param) {
			//   /^[^\\/$%<>.&\\?\\*\\|"\'\\\\]*$/.test(value);
			var vkeywords=/^[^`~!#$%^&*+=|\\\][\]\{\}:;'\,.<>?]{1}[^`~!$%^&+=|\\\][\]\{\};'\,.<>?]{0,1000}$/;
			return  vkeywords.test(value);
		},
		message: '输入不允许包含特殊字符'
	},
	passWordValid:{
		validator: function (value, param) {
			//   /^[^\\/$%<>.&\\?\\*\\|"\'\\\\]*$/.test(value);
			var vkeywords=/(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])/;
			return  vkeywords.test(value);
		},
		message: '密码必须包含数字、大小写字母、特殊字符'
	},
	simpleText:{
		validator: function (value, param) {
			return /^[^\\/$%<>&\\*\\\\]*$/.test(value);
		},
		message: '输入不允许包含特殊字符(/$%<>&*\)'
	},
	CHS: {
		validator: function (value, param) {
			return /^[\u0391-\uFFE5]+$/.test(value);
		},
		message: '请输入汉字'
	},
	charLength:{
		validator : function(value, param) {
		  var len = 0;
		  for (var i=0; i<value.length; i++) {
		    if (value.charCodeAt(i)>127 || value.charCodeAt(i)==94) {
		       len += 2;
		     } else {
		       len ++;
		     }
		   }
		  if(len >= param[0] && len <= param[1]){
		  	return true;
		  }else{
		  	return false;
		  }
		},
		message : '字符长度在{0}到{1}之间,汉字占2个字符'
	},
	english : {// 验证英语
		validator : function(value) {
			return /^[A-Za-z]+$/i.test(value);
		},
		message : '请输入英文'
	},
	ip : {// 验证IP地址
		validator : function(value) {
			return /\d+\.\d+\.\d+\.\d+/.test(value);
		},
		message : 'IP地址格式不正确'
	},
	ZIP: {
		validator: function (value, param) {
			return /^[0-9]\d{5}$/.test(value);
		},
		message: '邮政编码不存在'
	},
	mobile: {
		validator: function (value, param) {
			return /^1(3|4|5|7|8)\d{9}$/.test(value);
		},
		message: '手机号码不正确'
	},
	tel:{
		validator:function(value,param){
			return /^(\d{3}-|\d{4}-)?(\d{8}|\d{7})?(-\d{1,6})?$/.test(value);
		},
		message:'电话号码不正确'
	},
	mobileAndTel: {
		validator: function (value, param) {
			return /(^([0\+]\d{2,3})\d{3,4}\-\d{3,8}$)|(^([0\+]\d{2,3})\d{3,4}\d{3,8}$)|(^([0\+]\d{2,3}){0,1}13\d{9}$)|(^\d{3,4}\d{3,8}$)|(^\d{3,4}\-\d{3,8}$)/.test(value);
		},
		message: '请正确输入电话号码'
	},
	number: {
		validator: function (value, param) {
			return /^[0-9]+.?[0-9]*$/.test(value);
		},
		message: '请输入数字'
	},
	mone:{
		validator: function (value, param) {
			return (/^(([1-9]\d*)|\d)(\.\d{1,2})?$/).test(value);
		},
		message:'请输入正整数或小数,小数点后保留2位'
	},
	integer:{
		validator:function(value,param){
			return /^[+]?[1-9]\d*$/.test(value);
		},
		message: '请输入正整数'
	},
	integ:{
		validator:function(value,param){
			return /^-?\d+$/.test(value);
		},
		message: '请输入整数'
	},
	range:{
		validator:function(value,param){
			if(/^-?\d+$/.test(value)){
				return value >= param[0] && value <= param[1]
			}else{
				return false;
			}
		},
		message:'输入的整数在{0}到{1}之间'
	},
	minLength : {
		validator : function(value, param) {
			return value.length >= param[0]
		},
		message : '至少输入{0}个字'
	},
	maxLength : {
		validator : function(value, param) {
			return value.length <= param[0]
		},
		message : '最多输入{0}个字'
	},
	// select即选择框的验证
	mustSelect: {
		validator: function (value,param) {
			if (value == "" || value.indexOf('请选择') >= 0) {
			return false;
			}else {
			return true;
			}
		},
		message: '请选择'
	},
	idCode:{
		validator:function(value,param){
			return /(^\d{15}$)|(^\d{18}$)|(^\d{17}(\d|X|x)$)/.test(value);
		},
		message: '请输入正确的身份证号'
	},
	loginName: {
		validator: function (value, param) {
			return /^[\u0391-\uFFE5\w]+$/.test(value);
		},
		message: '只允许汉字、英文字母、数字及下划线。'
	},
	equalTo: {
		validator: function (value, param) {
			return value == $(param[0]).val();
		},
		message: '两次输入的字符不一致'
	},
	englishOrNum : {// 只能输入英文和数字
		validator : function(value) {
			return /^[a-zA-Z0-9_ ]{1,}$/.test(value);
		},
		message : '请输入英文、数字、下划线或者空格'
	},
	englishAndNum : {// 只能输入英文和数字
		validator : function(value) {
			return /^[a-zA-Z0-9]{1,}$/.test($("#myvalue").val());
		},
		message : '只允许英文或数字'
	},
	xiaoshu:{
		validator : function(value){
			return /^(([1-9]+)|([0-9]+\.[0-9]{1,2}))$/.test(value);
		},
		message : '最多保留两位小数！'
	},
	ddPrice:{
		validator:function(value,param){
			if(/^[1-9]\d*$/.test(value)){
				return value >= param[0] && value <= param[1];
			}else{
				return false;
			}
		},
		message:'请输入1到100之间正整数'
	},
	jretailUpperLimit:{
		validator:function(value,param){
			if(/^[0-9]+([.]{1}[0-9]{1,2})?$/.test(value)){
				return parseFloat(value) > parseFloat(param[0]) && parseFloat(value) <= parseFloat(param[1]);
			}else{
				return false;
			}
		},
		message:'请输入0到100之间的最多俩位小数的数字'
	},
	rateCheck:{
		validator:function(value,param){
			if(/^[0-9]+([.]{1}[0-9]{1,2})?$/.test(value)){
				return parseFloat(value) > parseFloat(param[0]) && parseFloat(value) <= parseFloat(param[1]);
			}else{
				return false;
			}
		},
		message:'请输入0到1000之间的最多俩位小数的数字'
	},
	isexist: {
        validator: function(value,param){
        	var result=false;
        	holly.get(param[1],{code:value,findid:param[0]},function(e){
				if(e.success){//先确认检查通过！
					result=true;
				}else{
					param[2]=e.errorMessage;
				}
    		},true);
            return result;
        },
        message: '{2}'
    },
    dateCompare:{
    	validator: function(value,param){
        	var result=false;
        	holly.get(holly.getPath() + "/rest/dictionary/compareDateRest",{comType:param[0],value:value},function(e){
				if(e.success){//先确认检查通过！
					result=true;
				}else{
					param[1]=e.errorMessage;
				}
    		},true);
            return result;
        },
        message: '{1}'
    },
    vaildOrgAccount:{
			validator : function(value) {
				var isSuccess=false;
				if(value){
					var vkeywords=/^[a-zA-Z0-9_]{1,}$/;
					if (!vkeywords.test(value)) {
               	 		 $.fn.validatebox.defaults.rules.vaildOrgAccount.message = '帐号只允许使用英文或数字';
               	  		return false;
               		}
					var param={userCode:value,orgId:$("#userOrgCombo").combotree("getValue")};
					holly.post(holly.getPath()+"/rest/user/checkUser",param,function(e){isSuccess=e;
						if(isSuccess==false){
					 	$.fn.validatebox.defaults.rules.vaildOrgAccount.message = '该帐号名称在本机构中已存在，请重新输入!';
					 	}
					},true);
				}
				return isSuccess;
			},
			message : '该帐号名称在本机构中已存在，请重新输入！'
	}
});