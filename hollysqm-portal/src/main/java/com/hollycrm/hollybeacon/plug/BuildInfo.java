package com.hollycrm.hollybeacon.plug;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;

public class BuildInfo { 
	//E:\\develop\\Java\\eclipse-jee-luna-SR2-win32-x86_64\\eclipse替换成自己的Eclipse\MyEclipse的安装目录
		final static String exportPluginPath = "E:\\develop\\Java\\EclipsePlugins\\EclipseMars\\eclipse-jee-mars\\dropins\\plugins";
	    final static String pluginPath ="dropins/plugins/";//dropins/plugins/插件的存放目录
	    final static String bundlePath = "E:\\develop\\Java\\EclipsePlugins\\EclipseMars\\eclipse-jee-mars\\configuration\\org.eclipse.equinox.simpleconfigurator\\bundles.info";
	    final static String oldJdtCore = "org.eclipse.jdt.core,";
	    final static String hollyPluginCore = "com.hollycrm.generator,";

	    public static void main(String[] args) throws IOException {
	    	//先清除原来存在的jar
	    	replaceTxtByStr(oldJdtCore,hollyPluginCore);
	        File direct = new File(exportPluginPath);
	        File[] files = direct.listFiles();
	        StringBuffer sb = new StringBuffer("\r\n");
	        for(File f : files){
	            String s = f.getName();
	            int p = s.lastIndexOf("_");
	            if(p == -1)
	                 continue;
	            String name = s.substring(0, p);
	            String version = s.substring(p+1);
	            if(version != null && !"".equals(version))
	                 version = version.substring(0, version.lastIndexOf("."));
	            //.jar直接拼接，文件夹形式的后边加"/"
	            if(f.isDirectory())
	                s = s+"/";
	            System.out.println(name+","+version+","+pluginPath +s+",4,false");
	            String fullPlugin = name+","+version+","+pluginPath +s+",4,false";
				sb.append(fullPlugin+"\r\n");
	        }
	    	FileWriter fw = new FileWriter(bundlePath,true);
	    	fw.write(sb.toString());
	    	fw.close();
	    }

	    /**
	     * 保证插件不重复添加
	     * @param oldStr
	     * @param hollyStr
	     */
	    public static void replaceTxtByStr(String oldStr,String hollyStr) {
	        String temp = "";
	        try {
	            File file = new File(bundlePath);
	            FileInputStream fis = new FileInputStream(file);
	            InputStreamReader isr = new InputStreamReader(fis);
	            BufferedReader br = new BufferedReader(isr);
	            StringBuffer buf = new StringBuffer();

	            // 保存该行前面的内容
	            for (int j = 1; (temp = br.readLine()) != null
	            		; j++) {
	            	System.out.println(j);
	            	if(!temp.contains(oldStr) && !temp.contains(hollyStr)){
	            		buf = buf.append(temp);
	                    buf = buf.append(System.getProperty("line.separator"));
	            	}
	            }

	            // 保存该行后面的内容
	            while ((temp = br.readLine()) != null) {
	                buf = buf.append(System.getProperty("line.separator"));
	                buf = buf.append(temp);
	            }

	            br.close();
	            FileOutputStream fos = new FileOutputStream(file);
	            PrintWriter pw = new PrintWriter(fos);
	            pw.write(buf.toString().toCharArray());
	            pw.flush();
	            pw.close();
	        } catch (IOException e) {
	            e.printStackTrace();
	        }
	    }
}
