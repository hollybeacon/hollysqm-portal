package com.hollycrm.hollysqm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.hollycrm.hollybeacon.basic.interfaces.dao.BaseEntity;

/**
 * 质检单评价实体
 * @author wangyf
 *
 * 2017年3月23日
 */
@Entity
@Table(name="TBL_QM_PLAN_CHECK")
public class PaperCheckData extends BaseEntity<String>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6789738408838506413L;

	/**
	 * 质检单表ID
	 */
	@Id
	@Column(name="PAPER_ID",unique=true,nullable=false)
	private String paperId;
	
	/**
	 * 检查评价分数（星级）
	 */
	@Column(name="CHECK_SCORE")
	private String checkScore;
	
	/**
	 * 检查评价内容
	 */
	@Column(name="CHECK_INFO")
	private String checkInfo;
	
	/**
	 * 评价状态：0.待评价1.已评价
	 */
	@Column(name="CHECK_STATUS")
	private String checkStatus;
	
	/**
	 * 修改人账号
	 */
	@Column(name="MODIFIER")
	private String modifier;
	
	/**
	 * 修改时间
	 */
	@Column(name="MODIFY_TIME")
	private String modifyTime;
	
	@Transient
	private String mofierName;

	public String getPaperId() {
		return paperId;
	}

	public void setPaperId(String paperId) {
		this.paperId = paperId;
	}

	public String getCheckScore() {
		return checkScore;
	}

	public void setCheckScore(String checkScore) {
		this.checkScore = checkScore;
	}

	public String getCheckInfo() {
		return checkInfo;
	}

	public void setCheckInfo(String checkInfo) {
		this.checkInfo = checkInfo;
	}

	public String getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getMofierName() {
		return mofierName;
	}

	public void setMofierName(String mofierName) {
		this.mofierName = mofierName;
	}
	
	
	

}
