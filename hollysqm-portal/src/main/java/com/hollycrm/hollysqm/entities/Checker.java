package com.hollycrm.hollysqm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.hollycrm.hollybeacon.basic.interfaces.dao.BaseEntity;

/**
 * 质检计划绑定的质检员
// * @author wangyf
 *
 */
@Entity
@Table(name="TBL_QM_QUALITY_USER")
public class Checker extends BaseEntity<String>{

	/**
	 * 抽取计划表ID
	 */
	@Id
	@Column(name="PLAN_ID")
	private String planId;
	
	/**
	 * 质检员账号
	 */
	@Column(name="USER_CODE")
	private String userCode;
	
	/**
	 * 质检员工号
	 */
	@Column(name="AGENT_CODE")
	private String agentCode;

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	
	
}
