package com.hollycrm.hollysqm.entities;

/**
 * 计划人员映射关系
 * @author wangyf
 *
 */
public class PlanUserMapping{

	private String userCode;
	
	private String agentCode;
	
	private String checkerCode;
	
	private String checkerAgentCode;
	
	private String agentName;
	
	private String checkerName;
	
	private String planId;
	
	private Object extractNumber;
	
	private String rank;
	
	private String agentDeptName;
	
	private String checkerDeptName;
	
	private String agentDepartment;
	
	private String checkerDepartment;

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getCheckerCode() {
		return checkerCode;
	}

	public void setCheckerCode(String checkerCode) {
		this.checkerCode = checkerCode;
	}

	public String getCheckerAgentCode() {
		return checkerAgentCode;
	}

	public void setCheckerAgentCode(String checkerAgentCode) {
		this.checkerAgentCode = checkerAgentCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getCheckerName() {
		return checkerName;
	}

	public void setCheckerName(String checkerName) {
		this.checkerName = checkerName;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public Object getExtractNumber() {
		return extractNumber;
	}

	public void setExtractNumber(Object extractNumber) {
		this.extractNumber = extractNumber;
	}

	public String getRank() {
		return rank;
	}

	public void setRank(String rank) {
		this.rank = rank;
	}

	public String getAgentDeptName() {
		return agentDeptName;
	}

	public void setAgentDeptName(String agentDeptName) {
		this.agentDeptName = agentDeptName;
	}

	public String getCheckerDeptName() {
		return checkerDeptName;
	}

	public void setCheckerDeptName(String checkerDeptName) {
		this.checkerDeptName = checkerDeptName;
	}

	public String getAgentDepartment() {
		return agentDepartment;
	}

	public void setAgentDepartment(String agentDepartment) {
		this.agentDepartment = agentDepartment;
	}

	public String getCheckerDepartment() {
		return checkerDepartment;
	}

	public void setCheckerDepartment(String checkerDepartment) {
		this.checkerDepartment = checkerDepartment;
	}
}
