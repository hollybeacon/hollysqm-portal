package com.hollycrm.hollysqm.entities;

import java.io.Serializable;
import java.util.List;

/**
 * 质检计划缓存
 * @author wangyf
 *
 * 2017年3月9日
 */
public class PlanCache implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -6971802340564934542L;

	/**
	 * 计划ID
	 */
	private String planId;
	
	/**
	 * 计划名称
	 */
	private String planName;
	
	/**
	 * 评分模板
	 */
	private String standardId;
	
	/**
	 * 质检来源
	 */
	private String dataType;
	
	/**
	 * 启用状态
	 */
	private String status;

	
	/**
	 * 执行周期
	 */
	private String taskTimer;
	
	/**
	 * 是否免检
	 */
	private String exemption;
	
	/**
	 * 抽取坐席
	 */
	private List<User> agents;
	
	/**
	 * 质检员
	 */
	private List<User> checkers;
	

	/**
	 * 抽取策略
	 */
	private String extractMethod;
	
	
	/**
	 * 抽取条件json串
	 */
	private String paramJson;
	
	
	/**
	 * 质检标签
	 */
	private String textItemId;
	
	/**
	 * 计划人员配置方式
	 * @return
	 */
	private String isExport;
	
	/**
	 * 计划人员映射关系
	 */
	private List<PlanUserMapping> userMsg;
	
	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getStandardId() {
		return standardId;
	}

	public void setStandardId(String standardId) {
		this.standardId = standardId;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}


	public String getTaskTimer() {
		return taskTimer;
	}

	public void setTaskTimer(String taskTimer) {
		this.taskTimer = taskTimer;
	}

	public String getExemption() {
		return exemption;
	}

	public void setExemption(String exemption) {
		this.exemption = exemption;
	}

	public List<User> getAgents() {
		return agents;
	}

	public void setAgents(List<User> agents) {
		this.agents = agents;
	}

	public List<User> getCheckers() {
		return checkers;
	}

	public void setCheckers(List<User> checkers) {
		this.checkers = checkers;
	}


	public String getExtractMethod() {
		return extractMethod;
	}

	public void setExtractMethod(String extractMethod) {
		this.extractMethod = extractMethod;
	}

	
	public String getTextItemId() {
		return textItemId;
	}

	public void setTextItemId(String textItemId) {
		this.textItemId = textItemId;
	}
	
	public String getParamJson() {
		return paramJson;
	}

	public void setParamJson(String paramJson) {
		this.paramJson = paramJson;
	}

	public String getIsExport() {
		return isExport;
	}

	public void setIsExport(String isExport) {
		this.isExport = isExport;
	}

	public List<PlanUserMapping> getUserMsg() {
		return userMsg;
	}

	public void setUserMsg(List<PlanUserMapping> userMsg) {
		this.userMsg = userMsg;
	}

}
