package com.hollycrm.hollysqm.entities;

/**
 * @author liujr
 * 2017年3月30日
 * @Description 各种状态分布情况
 */
public class DataStatus {

	private Object total;//总数
	
	private Object status0;//未质检
	
	private Object status1;//已质检
	
	private Object status2;//已申诉
	
	private Object status3;//已通过
	
	private Object status4;//已驳回

	public Object getTotal() {
		return total;
	}

	public void setTotal(Object total) {
		this.total = total;
	}

	public Object getStatus0() {
		return status0;
	}

	public void setStatus0(Object status0) {
		this.status0 = status0;
	}

	public Object getStatus1() {
		return status1;
	}

	public void setStatus1(Object status1) {
		this.status1 = status1;
	}

	public Object getStatus2() {
		return status2;
	}

	public void setStatus2(Object status2) {
		this.status2 = status2;
	}

	public Object getStatus3() {
		return status3;
	}

	public void setStatus3(Object status3) {
		this.status3 = status3;
	}

	public Object getStatus4() {
		return status4;
	}

	public void setStatus4(Object status4) {
		this.status4 = status4;
	}
	
}
