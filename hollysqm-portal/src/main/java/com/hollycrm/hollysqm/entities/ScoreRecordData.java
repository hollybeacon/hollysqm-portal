package com.hollycrm.hollysqm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.hollycrm.hollybeacon.basic.interfaces.dao.BaseEntity;

/**
 * 质检成绩打分记录
 * @author wangyf
 *
 * 2017年3月24日
 */
@Entity
@Table(name="TBL_QM_SCORE_RECORD")
//评分记录表
public class ScoreRecordData extends BaseEntity<String>{

	/**
	 * 得分记录ID
	 */
	/**
	 * 
	 */
	private static final long serialVersionUID = 2966785141846778277L;

	@Id
	@GenericGenerator(name="system-uuid",strategy="uuid")
	@GeneratedValue(generator="system-uuid")
	@Column(name="SCORE_RECORD_ID",unique=true,nullable=false)
	private String scoredRecordId;//分数记录ID

	/**
	 * 质检单ID
	 */
	@Column(name="PAPER_ID")
	private String paperId;//质检单ID

	/**
	 * 打分项ID
	 */
	@Column(name="ITEM_ID")
	private String itemId;//评分指标项ID

	/**
	 * 打分项得分
	 */
	@Column(name="ITEM_SCORE")
	private Integer itemScore;//指标得分（人工打分结果）

	/**创建时间
	 * 
	 */
	@Column(name="CREATE_TIME")
	private String createTime;//创建时间

	/**
	 * 创建人账号
	 */
	@Column(name="CREATE_CODE")
	private String createCode;//创建人账号

	/**
	 * 备注
	 */
	@Column(name="REMARK")
	private String remark;//备注

	/**
	 * 是否属于复核打分
	 */
	@Column(name="IS_REVIEW")
	private String isReview;//是否复核项（0否，1是），表示该分数复核过

	
	public String getScoredRecordId() {
		return scoredRecordId;
	}

	public void setScoredRecordId(String scoredRecordId) {
		this.scoredRecordId = scoredRecordId;
	}

	public String getPaperId() {
		return paperId;
	}

	public void setPaperId(String paperId) {
		this.paperId = paperId;
	}

	public String getItemId() {
		return itemId;
	}

	public void setItemId(String itemId) {
		this.itemId = itemId;
	}

	public Integer getItemScore() {
		return itemScore;
	}

	public void setItemScore(Integer itemScore) {
		this.itemScore = itemScore;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateCode() {
		return createCode;
	}

	public void setCreateCode(String createCode) {
		this.createCode = createCode;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getIsReview() {
		return isReview;
	}

	public void setIsReview(String isReview) {
		this.isReview = isReview;
	}
	
	
	
	
}
