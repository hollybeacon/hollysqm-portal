package com.hollycrm.hollysqm.entities;

import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.hollycrm.hollybeacon.basic.interfaces.dao.BaseEntity;

/**
 * @author liujr
 * 2017年3月27日
 * @Description 评价标准表
 */
@Entity
@Table(name="tbl_qm_standard")
public class Standard extends BaseEntity<String> {

	
	/**
	 * 
	 */
	private static final long serialVersionUID = -2719126829847454651L;

	@Id
	@GenericGenerator(name="system-uuid",strategy="uuid")
	@GeneratedValue(generator="system-uuid")
	@Column(name="STANDARD_ID",length = 40,nullable=false,unique=true)
	private String standardId;//考评标准ID
	
	@Column(name="STANDARD_NAME",length = 100)
	private String standardName;//考评标准名称
	
	@Column(name="TOTAL_SCORE",length = 22)
	private Integer totalScore;//总分
	
	@Column(name="`STATUS`",length = 2 )
	private String status;//状态：0:未发布，1:已发布
	
	@Column(name="IS_VALID",length = 2)
	private String isValid;//是否有效 0：无效 （已删除）1：有效（默认为1）
	
	@Transient
	private String isValidName;
	
	@Column(name="MODIFIER",length = 32)
	private String modifier;//修改人账号
	
	@Transient
	private String modifyName;
	
	@Column(name="MODIFY_TIME",length = 19)
	private String modifyTime;//修改时间
	
	@Column(name="REMARK",length = 200)
	private String remark;//备注
	
	@Column(name="AREA_ID",length = 40)
	private String areaId;//地域ID
	
	@Column(name="DOMAIN_ID",length = 6)
	private String domainId;//租户ID
	
	@Column(name="IS_UPDATE",length = 2)
	private String isUpdate;//是否可修改,0:否(可编辑) 1:是（不可编辑）
	
	@Transient
	private List<ScoreList> scoreList;//评分标准集合

	public String getIsValidName() {
		return isValidName;
	}

	public void setIsValidName(String isValidName) {
		this.isValidName = isValidName;
	}

	public String getModifyName() {
		return modifyName;
	}

	public void setModifyName(String modifyName) {
		this.modifyName = modifyName;
	}

	public List<ScoreList> getScoreList() {
		return scoreList;
	}

	public void setScoreList(List<ScoreList> scoreList) {
		this.scoreList = scoreList;
	}

	public String getStandardId() {
		return standardId;
	}

	public void setStandardId(String standardId) {
		this.standardId = standardId;
	}

	public String getStandardName() {
		return standardName;
	}

	public void setStandardName(String standardName) {
		this.standardName = standardName;
	}

	public Integer getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getIsValid() {
		return isValid;
	}

	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getDomainId() {
		return domainId;
	}

	public void setDomainId(String domainId) {
		this.domainId = domainId;
	}

	public String getIsUpdate() {
		return isUpdate;
	}

	public void setIsUpdate(String isUpdate) {
		this.isUpdate = isUpdate;
	}
	
}
