package com.hollycrm.hollysqm.entities;

import com.hollycrm.hollybeacon.basic.util.TimeUtils;

/**
 * 全文检索查询结果
 * @author wangyf
 *
 * 2017年3月20日
 */
public class TextResult {

	/**
	 * 接触记录ID
	 */
	private String contactId;

	/**
	 * 质检状态
	 */
	private String qualityStatus;

	/**
	 * 文本内容
	 */
	private String content;

	/**
	 * 主叫号码
	 */
	private String caller;

	/**
	 * 质检单ID
	 */
	private String paperId;

	/**
	 * 来电时间
	 */
	private String startTime;

	/**
	 * 坐席工号
	 */
	private String agentCode;

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getQualityStatus() {
		return qualityStatus;
	}

	public void setQualityStatus(String qualityStatus) {
		this.qualityStatus = qualityStatus;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	public String getPaperId() {
		return paperId;
	}

	public void setPaperId(String paperId) {
		this.paperId = paperId;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}
	
	public static void main(String[] args) {
		Long lon = TimeUtils.getTimeDiff("2017-03-13 15:18:13", "2017-03-12 15:14:13", "yyyy-MM-dd HH:mm:ss");
		System.out.println(lon);
	}
	
}
