package com.hollycrm.hollysqm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.hollycrm.hollybeacon.basic.interfaces.dao.BaseEntity;

/**
 * @author liujr
 * 2017年3月27日
 * @Description 评分标准集合表
 */
@Entity
@Table(name="tbl_qm_score_list")
public class ScoreList extends BaseEntity<String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 4164062698439158764L;

	@Id
	@GenericGenerator(name="system-uuid",strategy="uuid")
	@GeneratedValue(generator="system-uuid")
	@Column(name="SCORE_LIST_ID",length = 40,nullable=false,unique=true)
	private String scoreListId;//打分标准集合表ID
	
	@Column(name="ITEM_NAME",length=100)
	private String itemName;//指标名称
	
	@Column(name="SCORE",length=22)
	private Integer score;//指标分数
	
	@Column(name="SCORE_TYPE",length=2)
	private String scoreType;//是否加减分项(0加分项、1减分项，默认0)
	
	@Column(name="ITEM_TYPE",length=2)
	private String itemType;//项目分类：0服务质量类，1业务能力类；
	
	@Column(name="TEXT_ITEM_ID",length=40)
	private String textItemId;//关联质检标签表ID(TBL_QM_TEXT_ITEM.TEXT_ITEM_ID)
	
	@Column(name="REMARK",length=200)
	private String remark;//备注
	
	@Column(name="MODIFIER",length=32)
	private String modifier;//修改人账号
	
	@Column(name="MODIFY_TIME",length=19)
	private String modifyTime;//修改时间

	public String getScoreListId() {
		return scoreListId;
	}

	public void setScoreListId(String scoreListId) {
		this.scoreListId = scoreListId;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public Integer getScore() {
		return score;
	}

	public void setScore(Integer score) {
		this.score = score;
	}

	public String getScoreType() {
		return scoreType;
	}

	public void setScoreType(String scoreType) {
		this.scoreType = scoreType;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getTextItemId() {
		return textItemId;
	}

	public void setTextItemId(String textItemId) {
		this.textItemId = textItemId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}
	
}
