package com.hollycrm.hollysqm.entities;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.springframework.stereotype.Component;

import com.hollycrm.hollybeacon.basic.util.PropertiesUtils;
import com.hollycrm.hollysqm.bean.FtpConfigurBean;
import com.hollycrm.hollysqm.bean.VocFtpSourceBean;
import com.hollycrm.hollysqm.bean.XmlFtpSourceBean;

/**
 * ftp配置文件
 * @author wangyf
 *
 * 2017年4月25日
 */
@Component(value="ftpSourceConfig")
public class FtpSourceConfig {
	/**
	 * 基于xml的ftp配置
	 */
	private XmlFtpSourceBean xmlFtpSourceBean;
	
	/**
	 * 基于voc的ftp配置
	 */
	private VocFtpSourceBean vocFtpSourceBean;
	
	public FtpSourceConfig() {
		XmlFtpSourceBean xmlFtpBean = new XmlFtpSourceBean();
		String ftp="ftp.xmlserver";
		String count = PropertiesUtils.getProperty(ftp + ".count");
		int c = StringUtils.isNotBlank(count)?Integer.parseInt(count):0;
		List<FtpConfigurBean> xmlFtpList=new ArrayList<>();
		for (int i=1;i<=c;i++){
			xmlFtpList.add(getFtpSource(ftp,i));
		}
		xmlFtpBean.setFtpConfigurList(xmlFtpList);
		this.xmlFtpSourceBean = xmlFtpBean;
		
		VocFtpSourceBean vocFtpBean = new VocFtpSourceBean();
		ftp="ftp.vocserver";
		count = PropertiesUtils.getProperty(ftp + ".count");
		c = StringUtils.isNotBlank(count)?Integer.parseInt(count):0;
		List<FtpConfigurBean> vocFtpList=new ArrayList<>();
		for (int i=1;i<=c;i++){			
			vocFtpList.add(getFtpSource(ftp,i));
		}
		vocFtpBean.setFtpConfigurList(vocFtpList);
		this.vocFtpSourceBean = vocFtpBean;
	}
	
	public XmlFtpSourceBean getXmlFtpSourceBean() {
		return xmlFtpSourceBean;
	}

	public void setXmlFtpSourceBean(XmlFtpSourceBean xmlFtpSourceBean) {
		this.xmlFtpSourceBean = xmlFtpSourceBean;
	}

	public VocFtpSourceBean getVocFtpSourceBean() {
		return vocFtpSourceBean;
	}

	public void setVocFtpSourceBean(VocFtpSourceBean vocFtpSourceBean) {
		this.vocFtpSourceBean = vocFtpSourceBean;
	}

	public FtpConfigurBean getFtpSource(String ftp ,int i){
		FtpConfigurBean bean = new FtpConfigurBean();		
		String port = PropertiesUtils.getProperty(ftp+i+".port");//端口
		int p = StringUtils.isNotBlank(port)?Integer.parseInt(port):21;
		String index = PropertiesUtils.getProperty(ftp+i+".index");//第几个服务器
		int ind = StringUtils.isNotBlank(index)?Integer.parseInt(index):1;
		bean.setIndex(ind);
		bean.setParentPath(PropertiesUtils.getProperty(ftp+i+".parentPath"));//下载目录
		bean.setHost(PropertiesUtils.getProperty(ftp+i+".host"));//下载主机
		bean.setPort(p);
		bean.setUserName(PropertiesUtils.getProperty(ftp+i+".userName"));//用户名
		bean.setPassword(PropertiesUtils.getProperty(ftp+i+".password"));//密码
		bean.setType(PropertiesUtils.getProperty(ftp+i+".type"));//FTP类型
		return bean;
	}
	

}
