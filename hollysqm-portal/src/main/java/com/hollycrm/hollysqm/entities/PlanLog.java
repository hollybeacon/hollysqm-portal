package com.hollycrm.hollysqm.entities;

/**
 * 质检计划日志
 * @author wangyf
 *
 * 2017年3月9日
 */
public class PlanLog {
	
	/**
	 * 计划执行开始时间
	 */
	private String startTime;
	
	/**
	 * 计划执行结束时间
	 */
	private String endTime;
	
	/**
	 * 计划抽取数量
	 */
	private int paperCount;

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public int getPaperCount() {
		return paperCount;
	}

	public void setPaperCount(int paperCount) {
		this.paperCount = paperCount;
	}
	
	

}
