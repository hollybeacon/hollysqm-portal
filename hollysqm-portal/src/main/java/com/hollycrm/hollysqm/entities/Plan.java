package com.hollycrm.hollysqm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.hollycrm.hollybeacon.basic.interfaces.dao.BaseEntity;

/**
 * 质检计划列表
 * @author wangyf
 *
 * 2017年3月9日
 */
@Entity
@Table(name="TBL_QM_PLAN")
public class Plan extends BaseEntity<String> {
	
	/**
	 * 计划ID
	 */
	@Id
	@Column(name="PLAN_ID",unique=true,nullable=false)
	private String planId;
	
	/**
	 * 计划名称
	 */
	@Column(name="PLAN_NAME")
	private String planName;
	
	/**
	 * 评价标准
	 */
	@Column(name="STANDARD_ID")
	private String standardId;
	
	/**
	 * 质检来源
	 */
	@Column(name="DATA_TYPE")
	private String dataType;
	
	/**
	 * 启用状态
	 */
	@Column(name="`STATUS`")
	private String status;
	
	/**
	 * 是否有效
	 */
	@Column(name="IS_VALID")
	private String isValid;
	
	/**
	 * 修改人账号
	 */
	@Column(name="MODIFIER")
	private String modifier;
	
	/**
	 * 修改人姓名
	 */
	@Transient
	private String modifierName;
	
	/**
	 * 修改时间
	 */
	@Column(name="MODIFY_TIME")
	private String modifyTime;
	
	/**
	 * 备注
	 */
	@Column(name="REMARK")
	private String remark;
	
	/**
	 * 区域id
	 */
	@Column(name="AREA_ID")
	private String areaId;
	
	/**
	 * 租户id
	 */
	@Column(name="DOMAIN_ID")
	private String domainId;
	
	/**
	 * 及格分数线
	 */
	@Column(name="PASS_SCORE")
	private Integer passScore;
	
	/**
	 * 质检人员日质检量
	 */
	@Column(name="CHECKER_COUNT")
	private Integer checkerScore;
	
	/**
	 * 时间周期
	 */
	@Column(name="TASK_TIMER")
	private String taskTimer;
	
	/**
	 * 是否导入人员信息
	 */
	@Column(name="IS_EXPORT")
	private String isExport;

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getModifierName() {
		return modifierName;
	}

	public void setModifierName(String modifierName) {
		this.modifierName = modifierName;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getStandardId() {
		return standardId;
	}

	public void setStandardId(String standardId) {
		this.standardId = standardId;
	}

	public String getIsValid() {
		return isValid;
	}

	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getDomainId() {
		return domainId;
	}

	public void setDomainId(String domainId) {
		this.domainId = domainId;
	}

	public Integer getPassScore() {
		return passScore;
	}

	public void setPassScore(Integer passScore) {
		this.passScore = passScore;
	}

	public Integer getCheckerScore() {
		return checkerScore;
	}

	public void setCheckerScore(Integer checkerScore) {
		this.checkerScore = checkerScore;
	}

	public String getTaskTimer() {
		return taskTimer;
	}

	public void setTaskTimer(String taskTimer) {
		this.taskTimer = taskTimer;
	}

	public String getIsExport() {
		return isExport;
	}

	public void setIsExport(String isExport) {
		this.isExport = isExport;
	}
	
	

}
