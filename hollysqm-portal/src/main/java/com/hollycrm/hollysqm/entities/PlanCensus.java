package com.hollycrm.hollysqm.entities;

/**
 * 计划抽取统计
 * @author wangyf
 *
 */
public class PlanCensus {
	/**
	 * 计划ID
	 */
	private String planId;
	/**
	 * 计划名称
	 */
	private String planName;
	/**
	 * 计划抽取类别
	 */
	private String isExport;
	/**
	 * 坐席工号
	 */
	private String agentCode;
	/**
	 * 质检员工号
	 */
	private String checkerAgentCode;
	/**
	 * 计划抽取量
	 */
	private Integer planExtractNum;
	/**
	 * 计划已抽取量
	 */
	private Integer hasExtractNum;
	/**
	 * 计划未抽取量
	 */
	private Integer lastExtractNum;

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getIsExport() {
		return isExport;
	}

	public void setIsExport(String isExport) {
		this.isExport = isExport;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getCheckerAgentCode() {
		return checkerAgentCode;
	}

	public void setCheckerAgentCode(String checkerAgentCode) {
		this.checkerAgentCode = checkerAgentCode;
	}

	public Integer getPlanExtractNum() {
		return planExtractNum;
	}

	public void setPlanExtractNum(Integer planExtractNum) {
		this.planExtractNum = planExtractNum;
	}

	public Integer getHasExtractNum() {
		return hasExtractNum;
	}

	public void setHasExtractNum(Integer hasExtractNum) {
		this.hasExtractNum = hasExtractNum;
	}

	public Integer getLastExtractNum() {
		return lastExtractNum;
	}

	public void setLastExtractNum(Integer lastExtractNum) {
		this.lastExtractNum = lastExtractNum;
	}
	
	
}
