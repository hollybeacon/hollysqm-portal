package com.hollycrm.hollysqm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.hollycrm.hollybeacon.basic.interfaces.dao.BaseEntity;

/**
 * 典型案例
 * @author wangyf
 *
 * 2017年3月10日
 */

@Entity
@Table(name="TBL_QM_CASE")
public class Case extends BaseEntity<String>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -4807946991216464695L;


	/**
	 * 典型ID
	 */
	@Id
	@GenericGenerator(name="system-uuid",strategy="uuid")
	@GeneratedValue(generator="system-uuid")
	@Column(name="CASE_ID",unique=true,nullable=false)
	private String caseId;
	
	
	/**
	 * 案例标签
	 */
	@Column(name="CASE_ITEM")
	private String caseItem;
	
	
	/**
	 * 典型案例类型 1：录音，2：投诉，3：营销，4：其它
	 */
	@Column(name="CASE_TYPE")
	private String caseType;
	
	
	/**
	 * 客户问题描述
	 */
	@Column(name="QUESTION_DESC")
	private String questionDesc;
	
	
	/**
	 * 典型案例点评
	 */
	@Column(name="COMMENTS")
	private String comments;
	
	
	/**
	 * 典型案例状态 0：未发布，1：已发布
	 */
	@Column(name="`STATUS`")
	private String status;
	
	
	/**
	 * 典型案例素材类型 1：文档，2：录音
	 */
	@Column(name="MATERIAL_TYPE")
	private String materialType;
	
	
	/**
	 * 接触记录表ID(TBL_VOC_CUSTCONTINFO.CUSTCONTINFO_ID)
	 */
	@Column(name="CONTACT_ID")
	private String contactId;
	
	
	/**
	 * 修改人账号
	 */
	@Column(name="MODIFIER")
	private String modifier;
	
	
	/**
	 * 修改时间
	 */
	@Column(name="MODIFY_TIME")
	private String modifyTime;
	
	
	/**
	 * 录音类型 1：优秀录音，2：问题录音
	 */
	@Column(name="RECORD_TYPE")
	private String recordType;

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public String getCaseItem() {
		return caseItem;
	}

	public void setCaseItem(String caseItem) {
		this.caseItem = caseItem;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getCaseType() {
		return caseType;
	}

	public void setCaseType(String caseType) {
		this.caseType = caseType;
	}

	public String getQuestionDesc() {
		return questionDesc;
	}

	public void setQuestionDesc(String questionDesc) {
		this.questionDesc = questionDesc;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMaterialType() {
		return materialType;
	}

	public void setMaterialType(String materialType) {
		this.materialType = materialType;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}
	
	
	
	

}
