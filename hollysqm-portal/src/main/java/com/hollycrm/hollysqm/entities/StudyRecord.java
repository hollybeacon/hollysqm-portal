package com.hollycrm.hollysqm.entities;

public class StudyRecord {

	/*
	 *坐席 
	 * */
	private String userCode;
	/*
	 * 所属部门
	 * */
	private String deptName;
	/*
	 * 质检量
	 * */
	private String paperTotal;
	/*
	 * 已质检
	 * */
	private String paperedTotal;
	/*
	 * 关注量
	 * */
	private String attentionedTotal;
	/*
	 * 质检率
	 * */
	private String paperedRate;
	/*
	 * 关注率
	 * */
	private String attentionedRate;

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}

	public String getPaperTotal() {
		return paperTotal;
	}

	public void setPaperTotal(String paperTotal) {
		this.paperTotal = paperTotal;
	}

	public String getPaperedTotal() {
		return paperedTotal;
	}

	public void setPaperedTotal(String paperedTotal) {
		this.paperedTotal = paperedTotal;
	}

	public String getAttentionedTotal() {
		return attentionedTotal;
	}

	public void setAttentionedTotal(String attentionedTotal) {
		this.attentionedTotal = attentionedTotal;
	}

	public String getPaperedRate() {
		return paperedRate;
	}

	public void setPaperedRate(String paperedRate) {
		this.paperedRate = paperedRate;
	}

	public String getAttentionedRate() {
		return attentionedRate;
	}

	public void setAttentionedRate(String attentionedRate) {
		this.attentionedRate = attentionedRate;
	}
	
	
}
