package com.hollycrm.hollysqm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.hollycrm.hollybeacon.basic.interfaces.dao.BaseEntity;

/**
 * 质检计划参数
 * @author wangyf
 *
 */

@Entity
@Table(name="TBL_QM_PLAN_PARAM")
public class PlanParam extends BaseEntity<String>{

	/**
	 * 抽取计划表ID
	 */
	@Id
	@Column(name="PLAN_ID",unique=true,nullable=false)
	private String planId;
	
	/**
	 * 是否优秀免检（0不启用，1启用，默认不启用）
	 */
	@Column(name="EXEMPTION")
	private String exemption;
	
	/**
	 * 抽取策略（一条、两条），参见界面设计要求
	 */
	@Column(name="EXTRACT_METHOD")
	private String extractMethod;
	
	/**
	 * 所有抽取条件组合成一个json串进行保存
	 */
	@Column(name="PARAM_JSON")
	private String paramJson;
	
	/**
	 * 质检标签表ID(TBL_QM_SCORE_ITEM.SCORE_ITEM_ID)
	 */
	@Column(name="TEXT_ITEM_ID")
	private String textitemId;
	
	/**
	 * 修改人账号
	 */
	@Column(name="MODIFIER")
	private String modifier;
	
	/**
	 * 修改时间
	 */
	@Column(name="MODIFY_TIME")
	private String modifyTime;

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getExemption() {
		return exemption;
	}

	public void setExemption(String exemption) {
		this.exemption = exemption;
	}

	public String getExtractMethod() {
		return extractMethod;
	}

	public void setExtractMethod(String extractMethod) {
		this.extractMethod = extractMethod;
	}

	public String getParamJson() {
		return paramJson;
	}

	public void setParamJson(String paramJson) {
		this.paramJson = paramJson;
	}

	public String getTextitemId() {
		return textitemId;
	}

	public void setTextitemId(String textitemId) {
		this.textitemId = textitemId;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}
	
	
}
