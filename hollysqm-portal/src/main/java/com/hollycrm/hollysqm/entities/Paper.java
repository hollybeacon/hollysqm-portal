package com.hollycrm.hollysqm.entities;

/**
 * 质检单信息
 * @author wangyf
 *
 * 2017年3月15日
 */
public class Paper {

	/**
	 * 质检单ID
	 */
	private String paperId;

	/**
	 * 接触记录ID
	 */
	private String paperDetailId;

	/**
	 * 质检计划ID
	 */
	private String planId;

	/**
	 * 数据来源
	 */
	private String dataType;

	/**
	 * 质检计划名
	 */
	private String planName;

	/**
	 * 主叫号码
	 */
	private String caller;

	/**
	 * 来电时间
	 */
	private String startTime;

	/**
	 * 质检单生成时间
	 */
	private String createTime;

	/**
	 * 质检成绩
	 */
	private Integer totalScore;

	/**
	 * 申诉时间
	 */
	private String appealTime;
	
	/**
	 * 申诉状态
	 */
	private String appealStatus;
	
	
	/**
	 * 坐席账号
	 */
	private String userCode;
	
	/**
	 * 坐席姓名
	 */
	private String agentName;
	
	/**
	 * 坐席工号
	 */
	private String agentCode;
	
	/**
	 * 质检员账号
	 */
	private String checkerCode;
	
	/**
	 * 质检员姓名
	 */
	private String checkerName;
	
	/**
	 * 质检员工号
	 */
	private String checkerAgentCode;
	
	/**
	 * 复核分数
	 */
	private Integer reviewScore;
	
	/**
	 * 质检单评价
	 */
	private String checkScore;
	
	/**
	 *评价状态 
	 */
	private String evaluateStatus;
	
	/**
	 * 关闭方式
	 */
	private String closeType;
	
	/**
	 * 会话等级
	 */
	private String sessionType;
	
	/**
	 * 评价状态
	 */
	private String checkStatus;
	
	/**
	 * 最后修改时间
	 */
	private String modifyTime;
	
	/**
	 * 坐席是否查看
	 */
	private Integer hasRead;
	
	/**
	 * 申诉ID
	 */
	private String appealId;
	
	/**
	 * 申诉处理人
	 */
	private String executeCode;
	
	public String getPaperId() {
		return paperId;
	}

	public void setPaperId(String paperId) {
		this.paperId = paperId;
	}

	public String getPaperDetailId() {
		return paperDetailId;
	}

	public void setPaperDetailId(String paperDetailId) {
		this.paperDetailId = paperDetailId;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public Integer getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}

	public String getAppealTime() {
		return appealTime;
	}

	public void setAppealTime(String appealTime) {
		this.appealTime = appealTime;
	}

	public String getAppealStatus() {
		return appealStatus;
	}

	public void setAppealStatus(String appealStatus) {
		this.appealStatus = appealStatus;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getAgentName() {
		return agentName;
	}

	public void setAgentName(String agentName) {
		this.agentName = agentName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getCheckerCode() {
		return checkerCode;
	}

	public void setCheckerCode(String checkerCode) {
		this.checkerCode = checkerCode;
	}

	public String getCheckerName() {
		return checkerName;
	}

	public void setCheckerName(String checkerName) {
		this.checkerName = checkerName;
	}

	public String getCheckerAgentCode() {
		return checkerAgentCode;
	}

	public void setCheckerAgentCode(String checkerAgentCode) {
		this.checkerAgentCode = checkerAgentCode;
	}

	public Integer getReviewScore() {
		return reviewScore;
	}

	public void setReviewScore(Integer reviewScore) {
		this.reviewScore = reviewScore;
	}

	public String getCheckScore() {
		return checkScore;
	}

	public void setCheckScore(String checkScore) {
		this.checkScore = checkScore;
	}

	public String getEvaluateStatus() {
		return evaluateStatus;
	}

	public void setEvaluateStatus(String evaluateStatus) {
		this.evaluateStatus = evaluateStatus;
	}

	public String getCloseType() {
		return closeType;
	}

	public void setCloseType(String closeType) {
		this.closeType = closeType;
	}

	public String getSessionType() {
		return sessionType;
	}

	public void setSessionType(String sessionType) {
		this.sessionType = sessionType;
	}

	public String getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}

	public Integer getHasRead() {
		return hasRead;
	}

	public void setHasRead(Integer hasRead) {
		this.hasRead = hasRead;
	}

	public String getAppealId() {
		return appealId;
	}

	public void setAppealId(String appealId) {
		this.appealId = appealId;
	}

	public String getExecuteCode() {
		return executeCode;
	}

	public void setExecuteCode(String executeCode) {
		this.executeCode = executeCode;
	}
	
}
