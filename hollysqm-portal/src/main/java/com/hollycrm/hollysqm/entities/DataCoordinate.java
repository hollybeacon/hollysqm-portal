package com.hollycrm.hollysqm.entities;

/**
 * @author liujr
 * 2017年3月30日
 * @Description 坐标统计数据
 */
public class DataCoordinate {

	private String abscissa;//横坐标
	
	private Integer ordinate;//纵坐标

	public String getAbscissa() {
		return abscissa;
	}

	public void setAbscissa(String abscissa) {
		this.abscissa = abscissa;
	}

	public Integer getOrdinate() {
		return ordinate;
	}

	public void setOrdinate(Integer ordinate) {
		this.ordinate = ordinate;
	}
	
	
}
