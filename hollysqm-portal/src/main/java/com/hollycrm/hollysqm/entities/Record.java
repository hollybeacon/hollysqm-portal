package com.hollycrm.hollysqm.entities;

import java.io.Serializable;

/**
 * 接触记录
 * @author wangyf
 *
 */
public class Record implements Serializable{
	
	/**
	 * 接触记录ID
	 */
	private String contactId;
	
	/**
	 * 呼叫号码
	 */
	private String caller;
	
	/**
	 * 用户
	 */
	private User user;
	
	/**
	 * 归属地
	 */
	private String custArea;
	
	/**
	 * 品牌
	 */
	private String custBand;
	
	/**
	 * 满意度
	 */
	private String satisfaction;
	
	/**
	 * 服务类型
	 */
	private String serviceType;
	
	/**
	 * 开始时间
	 */
	private String startTime;
	
	/**
	 * 结束时间
	 */
	private String endTime;
	
	/**
	 * 质检状态
	 */
	private String qualityStatus;
	
	/**
	 * 交流时长
	 */
	private Long length;
	
	/**
	 * 文本信息
	 */
	private String txtContent;
	
	/**
	 * 关闭方式
	 */
	private String closeType;
	
	/**
	 * 会话等级
	 */
	private String sessionType;
	
	/**
	 * 数据来源
	 */
	private String dataType;
	
	/**
	 * v8接收时间
	 */
	private String acceptTime;
	
	/**
	 * 音频文件路径
	 */
	private String recordFile;
	
	/**
	 * 业务类型
	 */
	private String bussinessType;
	
	/**
	 * 质检计划id
	 */
	private String planId;
	
	/**
	 * 质检计划名称
	 */
	private String planName;
	
	private String custLevel;
	
	private Long silenceLength;
	
	private String callee;

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	public String getCustArea() {
		return custArea;
	}

	public void setCustArea(String custArea) {
		this.custArea = custArea;
	}

	public String getCustBand() {
		return custBand;
	}

	public void setCustBand(String custBand) {
		this.custBand = custBand;
	}

	public String getSatisfaction() {
		return satisfaction;
	}

	public void setSatisfaction(String satisfaction) {
		this.satisfaction = satisfaction;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getQualityStatus() {
		return qualityStatus;
	}

	public void setQualityStatus(String qualityStatus) {
		this.qualityStatus = qualityStatus;
	}

	public Long getLength() {
		return length;
	}

	public void setLength(Long length) {
		this.length = length;
	}

	public String getTxtContent() {
		return txtContent;
	}

	public void setTxtContent(String txtContent) {
		this.txtContent = txtContent;
	}

	public String getCloseType() {
		return closeType;
	}

	public void setCloseType(String closeType) {
		this.closeType = closeType;
	}

	public String getSessionType() {
		return sessionType;
	}

	public void setSessionType(String sessionType) {
		this.sessionType = sessionType;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public User getUser() {
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getAcceptTime() {
		return acceptTime;
	}

	public void setAcceptTime(String acceptTime) {
		this.acceptTime = acceptTime;
	}

	public String getRecordFile() {
		return recordFile;
	}

	public void setRecordFile(String recordFile) {
		this.recordFile = recordFile;
	}

	public String getBussinessType() {
		return bussinessType;
	}

	public void setBussinessType(String bussinessType) {
		this.bussinessType = bussinessType;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getCustLevel() {
		return custLevel;
	}

	public void setCustLevel(String custLevel) {
		this.custLevel = custLevel;
	}

	public Long getSilenceLength() {
		return silenceLength;
	}

	public void setSilenceLength(Long silenceLength) {
		this.silenceLength = silenceLength;
	}

	public String getCallee() {
		return callee;
	}

	public void setCallee(String callee) {
		this.callee = callee;
	}
	

}
