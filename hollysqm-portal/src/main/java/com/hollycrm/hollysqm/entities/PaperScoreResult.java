package com.hollycrm.hollysqm.entities;

/**
 * 质检单成绩
 * @author wangyf
 *
 * 2017年3月10日
 */
public class PaperScoreResult {

	/**
	 * 质检单ID
	 */
	private String paperId;

	/**
	 * 实际总得分
	 */
	private Integer totalScore;

	/**
	 * 评价标准总分
	 */
	private Integer amount;

	/**
	 * 复核总分
	 */
	private Integer reviewScore;
	
	/**
	 * 评分人账号
	 */
	private String scorerCode;
	
	/**
	 * 评分人姓名
	 */
	private String scorerName;

	/**
	 * 复核人账号
	 */
	private String checkerCode;
	
	/**
	 * 复核人姓名
	 */
	private String checkerName;

	/**
	 * 质检评论
	 */
	private String comments;
	
	/**
	 * 评分时间
	 */
	private String scoreTime;
	
	/**
	 * 复核时间
	 */
	private String reviewTime;
	
	/**
	 * 质检状态
	 */
	private String qualityStatus;
	
	/**
	 * 坐席帐号
	 */
	private String agentCode;
	
	/**
	 * 业务类型
	 */
	private String serviceType;
	
	/**
	 * 差错类型
	 */
	private String errorType;
	
	public String getPaperId() {
		return paperId;
	}

	public void setPaperId(String paperId) {
		this.paperId = paperId;
	}

	public Integer getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getReviewScore() {
		return reviewScore;
	}

	public void setReviewScore(Integer reviewScore) {
		this.reviewScore = reviewScore;
	}

	public String getCheckerCode() {
		return checkerCode;
	}

	public void setCheckerCode(String checkerCode) {
		this.checkerCode = checkerCode;
	}

	public String getCheckerName() {
		return checkerName;
	}

	public void setCheckerName(String checkerName) {
		this.checkerName = checkerName;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getScoreTime() {
		return scoreTime;
	}

	public void setScoreTime(String scoreTime) {
		this.scoreTime = scoreTime;
	}

	public String getReviewTime() {
		return reviewTime;
	}

	public void setReviewTime(String reviewTime) {
		this.reviewTime = reviewTime;
	}

	public String getQualityStatus() {
		return qualityStatus;
	}

	public void setQualityStatus(String qualityStatus) {
		this.qualityStatus = qualityStatus;
	}

	public String getScorerCode() {
		return scorerCode;
	}

	public void setScorerCode(String scorerCode) {
		this.scorerCode = scorerCode;
	}

	public String getScorerName() {
		return scorerName;
	}

	public void setScorerName(String scorerName) {
		this.scorerName = scorerName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}
	
}
