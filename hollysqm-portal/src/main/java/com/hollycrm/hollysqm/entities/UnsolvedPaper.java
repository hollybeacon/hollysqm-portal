package com.hollycrm.hollysqm.entities;

/**
 * 待解决质检单信息
 * @author wangyf
 *
 * 2017年3月10日
 */
public class UnsolvedPaper {
	
	/**
	 * 待解决质检单数目
	 */
	private int paperCount;
	
	/**
	 * 下一待解决质检单ID
	 */
	private String nextPaperId;

	public int getPaperCount() {
		return paperCount;
	}

	public void setPaperCount(int paperCount) {
		this.paperCount = paperCount;
	}

	public String getNextPaperId() {
		return nextPaperId;
	}

	public void setNextPaperId(String nextPaperId) {
		this.nextPaperId = nextPaperId;
	}
	
	

}
