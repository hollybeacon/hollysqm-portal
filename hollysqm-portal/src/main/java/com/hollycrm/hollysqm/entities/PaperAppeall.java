package com.hollycrm.hollysqm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.hollycrm.hollybeacon.basic.interfaces.dao.BaseEntity;

@Entity
@Table(name="TBL_QM_APPEALL")
public class PaperAppeall extends BaseEntity<String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 734357277477377616L;

	@Id
	@GenericGenerator(name="system-uuid",strategy="uuid")
	@GeneratedValue(generator="system-uuid")
	@Column(name="APPEAL_ID",nullable = false,unique = true,length = 40)
	private String appealId;//申诉单ID
	
	@Column(name="PAPER_ID",length=40,nullable = false)
	private String paperId;//质检单表ID(TBL_QM_PAPER.PAPER_ID)
	
	@Column(name="EXECUTE_CODE",length = 32)
	private String executeCode;//处理人账号
	
	@Column(name="`EXPLAIN`",length = 400)
	private String explain;//内容
	
	@Column(name="CREATE_TIME",length = 19)
	private String createTime;//创建时间
	
	@Column(name="`STATUS`",length = 2)
	private String status;//申诉状态（0：已申诉，1：已通过，2：已驳回，3待处理）
	
	@Column(name="JOB_TYPE",length = 2)
	private String jobType;//角色类型：1坐席、2组长、3班长
	
	@Column(name="IS_VALID",length = 2)
	private String isValid;//是否有效 0：无效 （已删除）1：有效（默认为1）
	
	@Column(name="REMARKS",length = 100)
	private String remarks;//备注
	
	@Column(name="EXECUTE_TIME" ,length = 19)
	private String executeime;//处理时间
	
	public String getAppealId() {
		return appealId;
	}

	public void setAppealId(String appealId) {
		this.appealId = appealId;
	}

	public String getPaperId() {
		return paperId;
	}

	public void setPaperId(String paperId) {
		this.paperId = paperId;
	}

	public String getExecuteCode() {
		return executeCode;
	}

	public void setExecuteCode(String executeCode) {
		this.executeCode = executeCode;
	}

	public String getExplain() {
		return explain;
	}

	public void setExplain(String explain) {
		this.explain = explain;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getJobType() {
		return jobType;
	}

	public void setJobType(String jobType) {
		this.jobType = jobType;
	}

	public String getIsValid() {
		return isValid;
	}

	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}

	public String getRemarks() {
		return remarks;
	}

	public void setRemarks(String remarks) {
		this.remarks = remarks;
	}

	public String getExecuteime() {
		return executeime;
	}

	public void setExecuteime(String executeime) {
		this.executeime = executeime;
	}
	
}
