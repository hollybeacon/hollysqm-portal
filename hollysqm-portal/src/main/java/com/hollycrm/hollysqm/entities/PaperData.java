package com.hollycrm.hollysqm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


import com.hollycrm.hollybeacon.basic.interfaces.dao.BaseEntity;

@Entity
@Table(name="TBL_QM_PAPER")
//质检单表
public class PaperData extends BaseEntity<String>{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = -1701310390925412942L;

	@Id
	@Column(name="PAPER_ID",unique=true,nullable=false)
	private String paperId;//质检单编号
	
	@Column(name="PLAN_ID")
	private String planId;//抽取计划表ID
	
	@Column(name="PAPER_DETAIL_ID")
	private String contactId;//接触记录表ID
	
	@Column(name="PAPER_TYPE")
	private String paperType;//质检单类型：V8：录音质检，I8：文本质检
	
	@Column(name="QUALITY_STATUS")
	private String qualityStatus;//质检状态（0：未质检，1：已质检，2已申诉，3已通过，4已驳回）
	
	@Column(name="START_TIME")
	private String startTime;//质检开始时间
	
	@Column(name="END_TIME")
	private String endTime;//质检结束时间
	
	@Column(name="STANDARD_ID")
	private String standardId;//评价标准表ID
	
	@Column(name="AMOUNT")
	private Integer amount;//评价标准总分
	
	@Column(name="TOTAL_SCORE")
	private Integer totalScore;//实际总得分
	
	@Column(name="COMMENTS")
	private String comments;//评语（问题描述）
	
	@Column(name="REVIEW_SCORE")
	private Integer reviewScore;//复核总得分(有申诉复核，才有该分数)
	
	@Column(name="AREA_ID")
	private String areaId;//地域ID
	
	@Column(name="DOMAIN_ID")
	private String domainId;//租户ID
	
	@Column(name="CREATE_TIME")
	private String createTime;//质检单创建时间
	
	@Column(name="USER_CODE")
	private String userCode;//坐席账号
	
	@Column(name="AGENT_CODE")
	private String agentCode;//坐席工号
	
	@Column(name="CHECKER_CODE")
	private String checkerCode;//质检员账号（来自人员信息扩展的账号）
	
	@Column(name="CHECKER_AGENT_CODE")
	private String checkerAgentCode;//质检员工号
	
	@Column(name="PAPER_NO")
	private String paperNo;//质检单序号
	
	@Column(name="RELEASE_TIME")
	private String releaseTime;//成绩发布时间
	
	@Column(name="IS_PUBLIC_RESULT")
	private String isPublicResult;//是否发布成绩（0否，1是）
	
	@Column(name="MODIFIER")
	private String modifier;//最终修改人
	
	@Column(name="MODIFY_TIME")
	private String modifyTime;//最终修改时间
	
	@Column(name="HAS_READ")
	private Integer hasRead;//坐席是否查看过
	
	public String getPaperId() {
		return paperId;
	}

	public void setPaperId(String paperId) {
		this.paperId = paperId;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getPaperType() {
		return paperType;
	}

	public void setPaperType(String paperType) {
		this.paperType = paperType;
	}

	public String getQualityStatus() {
		return qualityStatus;
	}

	public void setQualityStatus(String qualityStatus) {
		this.qualityStatus = qualityStatus;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getStandardId() {
		return standardId;
	}

	public void setStandardId(String standardId) {
		this.standardId = standardId;
	}

	public Integer getAmount() {
		return amount;
	}

	public void setAmount(Integer amount) {
		this.amount = amount;
	}

	public Integer getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public Integer getReviewScore() {
		return reviewScore;
	}

	public void setReviewScore(Integer reviewScore) {
		this.reviewScore = reviewScore;
	}

	public String getAreaId() {
		return areaId;
	}

	public void setAreaId(String areaId) {
		this.areaId = areaId;
	}

	public String getDomainId() {
		return domainId;
	}

	public void setDomainId(String domainId) {
		this.domainId = domainId;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getCheckerCode() {
		return checkerCode;
	}

	public void setCheckerCode(String checkerCode) {
		this.checkerCode = checkerCode;
	}

	public String getCheckerAgentCode() {
		return checkerAgentCode;
	}

	public void setCheckerAgentCode(String checkerAgentCode) {
		this.checkerAgentCode = checkerAgentCode;
	}

	public String getPaperNo() {
		return paperNo;
	}

	public void setPaperNo(String paperNo) {
		this.paperNo = paperNo;
	}

	public String getReleaseTime() {
		return releaseTime;
	}

	public void setReleaseTime(String releaseTime) {
		this.releaseTime = releaseTime;
	}

	public String getIsPublicResult() {
		return isPublicResult;
	}

	public void setIsPublicResult(String isPublicResult) {
		this.isPublicResult = isPublicResult;
	}

	public String getContactId() {
		return contactId;
	}

	public void setContactId(String contactId) {
		this.contactId = contactId;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}

	public Integer getHasRead() {
		return hasRead;
	}

	public void setHasRead(Integer hasRead) {
		this.hasRead = hasRead;
	}
	
}
