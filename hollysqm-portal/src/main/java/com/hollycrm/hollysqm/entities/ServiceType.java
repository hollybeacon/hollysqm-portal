package com.hollycrm.hollysqm.entities;

import java.util.List;

public class ServiceType {
	
	private String id;
	
	private String text;
	
	private String checked;
	
	private String state;
	
	private String parentId;
	
	private List<ServiceType> children;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	public String getChecked() {
		return checked;
	}

	public void setChecked(String checked) {
		this.checked = checked;
	}

	public String getState() {
		return state;
	}

	public void setState(String state) {
		this.state = state;
	}

	public List<ServiceType> getChildren() {
		return children;
	}

	public void setChildren(List<ServiceType> children) {
		this.children = children;
	}

	public String getParentId() {
		return parentId;
	}

	public void setParentId(String parentId) {
		this.parentId = parentId;
	}
	

}
