package com.hollycrm.hollysqm.entities;

public class RecordDir {
	
	private String wavDir;
	
	private String imgDir;

	public String getWavDir() {
		return wavDir;
	}

	public void setWavDir(String wavDir) {
		this.wavDir = wavDir;
	}

	public String getImgDir() {
		return imgDir;
	}

	public void setImgDir(String imgDir) {
		this.imgDir = imgDir;
	}
	
	

}
