package com.hollycrm.hollysqm.entities;

import com.hollycrm.hollybeacon.basic.interfaces.dao.BaseEntity;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;

/**
 * 收藏实体类
 * @author wangyf
 *
 * 2017年3月16日
 */

@Entity
@Table(name="TBL_QM_COLLECT")
public class MyCollection extends BaseEntity<String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 169296630522229024L;

	/**
	 * 收藏表ID
	 */

	@Id
	@GenericGenerator(name="system-uuid",strategy="uuid")
	@GeneratedValue(generator="system-uuid")
	@Column(name="COLLECT_ID",unique=true,nullable=false)
	private String collectId;

	/**
	 * 收藏名称
	 */
	
	@Column(name="NAME")
	private String comboName;

	/**
	 * 质检状态
	 */
	@Column(name="`STATUS`")
	private String qualityStatus;
	
	/**
	 * 主叫号码
	 */
	@Column(name="CALLER")
	private String caller;

	/**
	 * 组合关键词
	 */
	@Column(name="CONTENT")
	private String words;

	/**
	 * 开始时间
	 */
	@Column(name="START_TIME")
	private String startTime;

	/**
	 * 结束时间
	 */
	@Column(name="END_TIME")
	private String endTime;

	/**
	 * 坐席名称
	 */
	@Column(name="AGENT_NAME")
	private String agent;

	/**
	 * 起始接触时长
	 */
	@Column(name="RECOINFO_LENGTH_START")
	private String recoinfoLengthStart;
	
	/**
	 * 结束接触时长
	 */
	@Column(name="RECOINFO_LENGTH_END")
	private String recoinfoLengthEnd;
	
	/**
	 * 满意度
	 */
	@Column(name="SATISFACTION")
	private String satisfaction;
	
	
	/**
	 * 数据来源
	 */
	@Column(name="DATA_TYPE")
	private String dataType;

	/**
	 * 创建时间
	 */
	@Column(name="CREATE_TIME")
	private String createTime;

	/**
	 * 创建人账号
	 */
	@Column(name="CREATE_CODE")
	private String createCode;

	/**
	 * 被叫人号码
	 */
	@Column(name="CALLEE")
	private String callee;

	/**
	 * 归属地
	 */
	@Column(name="CUST_AREA")
	private String custArea;


	public String getCollectId() {
		return collectId;
	}

	public void setCollectId(String collectId) {
		this.collectId = collectId;
	}

	public String getComboName() {
		return comboName;
	}

	public void setComboName(String comboName) {
		this.comboName = comboName;
	}

	public String getQualityStatus() {
		return qualityStatus;
	}

	public void setQualityStatus(String qualityStatus) {
		this.qualityStatus = qualityStatus;
	}

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	public String getWords() {
		return words;
	}

	public void setWords(String words) {
		this.words = words;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getEndTime() {
		return endTime;
	}

	public void setEndTime(String endTime) {
		this.endTime = endTime;
	}

	public String getAgent() {
		return agent;
	}

	public void setAgent(String agent) {
		this.agent = agent;
	}

	public String getDataType() {
		return dataType;
	}

	public void setDataType(String dataType) {
		this.dataType = dataType;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getCreateCode() {
		return createCode;
	}

	public void setCreateCode(String createCode) {
		this.createCode = createCode;
	}

	public String getRecoinfoLengthStart() {
		return recoinfoLengthStart;
	}

	public void setRecoinfoLengthStart(String recoinfoLengthStart) {
		this.recoinfoLengthStart = recoinfoLengthStart;
	}

	public String getRecoinfoLengthEnd() {
		return recoinfoLengthEnd;
	}

	public void setRecoinfoLengthEnd(String recoinfoLengthEnd) {
		this.recoinfoLengthEnd = recoinfoLengthEnd;
	}

	public String getSatisfaction() {
		return satisfaction;
	}

	public void setSatisfaction(String satisfaction) {
		this.satisfaction = satisfaction;
	}

	public String getCallee() {
		return callee;
	}

	public void setCallee(String callee) {
		this.callee = callee;
	}

	public String getCustArea() {
		return custArea;
	}

	public void setCustArea(String custArea) {
		this.custArea = custArea;
	}
}
