package com.hollycrm.hollysqm.entities;

/**
 * v8质检单导出模型
 * @author wangyf
 *
 */
public class V8PaperExportTemplate {
	/**
	 * 主叫号码
	 */
	private String caller;
	
	/**
	 * 坐席工号
	 */
	private String agentCode;
	
	/**
	 * 坐席姓名
	 */
	private String userName;
	
	/**
	 * 归属地
	 */
	private String custArea;
	
	/**
	 * 用户品牌
	 */
	private String custBand;
	
	/**
	 * 满意度
	 */
	private String satisfaction;
	
	/**
	 * 业务类型
	 */
	private String bussinessType;
	
	/**
	 * 通话开始时间
	 */
	private String startTime;
	
	/**
	 * 通话时长
	 */
	private String length;
	
	/**
	 * 用户级别
	 */
	private String custLevel;
	
	/**
	 * 静音时长
	 */
	private String silenceLength;
	
	/**
	 * 服务类型
	 */
	private String serviceType;
	
	/**
	 * 错误类型
	 */
	private String errorType;
	
	/**
	 * 是否需要纠错
	 */
	private String needCorrect;
	
	/**
	 * 是否是致命错误
	 */
	private String isFatal;
	
	/**
	 * 服务请求
	 */
	private String serviceRequest;
	
	/**
	 * 打分项
	 */
	private String itemName;
	
	/**
	 * 总分
	 */
	private String totalScore;
	
	/**
	 * 是否是典型案例
	 */
	private String recordType;
	
	/**
	 * 案例类型
	 */
	private String caseName;
	
	/**
	 * 是否发布成绩
	 */
	private String isPublicResult;
	
	/**
	 * 问题描述
	 */
	private String comments;
	
	/**
	 * 对话录音
	 */
	private String txtContent;

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getCustArea() {
		return custArea;
	}

	public void setCustArea(String custArea) {
		this.custArea = custArea;
	}

	public String getCustBand() {
		return custBand;
	}

	public void setCustBand(String custBand) {
		this.custBand = custBand;
	}

	public String getSatisfaction() {
		return satisfaction;
	}

	public void setSatisfaction(String satisfaction) {
		this.satisfaction = satisfaction;
	}

	public String getBussinessType() {
		return bussinessType;
	}

	public void setBussinessType(String bussinessType) {
		this.bussinessType = bussinessType;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public String getLength() {
		return length;
	}

	public void setLength(String length) {
		this.length = length;
	}

	public String getCustLevel() {
		return custLevel;
	}

	public void setCustLevel(String custLevel) {
		this.custLevel = custLevel;
	}

	public String getSilenceLength() {
		return silenceLength;
	}

	public void setSilenceLength(String silenceLength) {
		this.silenceLength = silenceLength;
	}

	public String getServiceType() {
		return serviceType;
	}

	public void setServiceType(String serviceType) {
		this.serviceType = serviceType;
	}

	public String getErrorType() {
		return errorType;
	}

	public void setErrorType(String errorType) {
		this.errorType = errorType;
	}

	public String getNeedCorrect() {
		return needCorrect;
	}

	public void setNeedCorrect(String needCorrect) {
		this.needCorrect = needCorrect;
	}

	public String getIsFatal() {
		return isFatal;
	}

	public void setIsFatal(String isFatal) {
		this.isFatal = isFatal;
	}

	public String getServiceRequest() {
		return serviceRequest;
	}

	public void setServiceRequest(String serviceRequest) {
		this.serviceRequest = serviceRequest;
	}

	public String getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(String totalScore) {
		this.totalScore = totalScore;
	}

	public String getRecordType() {
		return recordType;
	}

	public void setRecordType(String recordType) {
		this.recordType = recordType;
	}

	public String getIsPublicResult() {
		return isPublicResult;
	}

	public void setIsPublicResult(String isPublicResult) {
		this.isPublicResult = isPublicResult;
	}

	public String getComments() {
		return comments;
	}

	public void setComments(String comments) {
		this.comments = comments;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getCaseName() {
		return caseName;
	}

	public void setCaseName(String caseName) {
		this.caseName = caseName;
	}

	public String getTxtContent() {
		return txtContent;
	}

	public void setTxtContent(String txtContent) {
		this.txtContent = txtContent;
	}
	
	

}
