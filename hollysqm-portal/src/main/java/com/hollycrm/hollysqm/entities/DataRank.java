package com.hollycrm.hollysqm.entities;

/**
 * @author liujr
 * 2017年3月30日
 * @Description 数据排名实体类
 */
public class DataRank {

	private Integer rank;//排名
	
	private String userCode;//用户账号
	
	private String userName;//用户名称
	
	private Object total;//数量（Integer or Double）
	
	private String photo;//用户图像ID

	public Integer getRank() {
		return rank;
	}

	public void setRank(Integer rank) {
		this.rank = rank;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPhoto() {
		return photo;
	}

	public void setPhoto(String photo) {
		this.photo = photo;
	}

	public Object getTotal() {
		return total;
	}

	public void setTotal(Object total) {
		this.total = total;
	}
	
}
