package com.hollycrm.hollysqm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.hibernate.annotations.GenericGenerator;

import com.hollycrm.hollybeacon.basic.interfaces.dao.BaseEntity;

/**
 * @author liujr
 * 2017年3月24日
 * @Description 文本标签表
 */
@Entity
@Table(name="TBL_QM_TEXT_ITEM")
public class TextItem extends BaseEntity<String> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5583170280665289336L;

	@Id
	@GenericGenerator(name="system-uuid",strategy="uuid")
	@GeneratedValue(generator="system-uuid")
	@Column(name="TEXT_ITEM_ID",length=40)
	private String textItemId;//标签项ID
	
	@Column(name="ITEM_TYPE",length=8)
	private String itemType;//标签项匹配类型:全部、坐席、用户
	
	@Transient 
	private String itemTypeName;
	
	@Column(name="ITEM_NAME",length=50)
	private String itemName;//标签名称
	
	@Column(name="CONTENT",length=2000)
	private String content;//标签组合关键词
	
	@Column(name="`STATUS`",length=2)
	private String status;//上线状态  0未上线 ，1上线
	
	@Transient
	private String statusName;//状态名称 0未上线 ，1上线
	
	@Column(name="ROLE_TYPE",length=10)
	private String roleType;//工单、语音文本、在线交流、微博、微信
	
	@Column(name="DOMAIN_ID",length=6)
	private String domainId;//租户编码
	
	@Column(name="IS_VALID",length=2)
	private String isValid;//是否有效  0无效，1有效  
	
	@Transient
	private String isValidName;
	
	@Column(name="MODIFIER",length=32)
	private String modifier;//修改人账号
	
	@Transient
	private String modifyName;
	
	@Column(name="MODIFY_TIME",length=19)
	private String modifyTime;//修改时间

	public String getTextItemId() {
		return textItemId;
	}

	public void setTextItemId(String textItemId) {
		this.textItemId = textItemId;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getItemTypeName() {
		return itemTypeName;
	}

	public void setItemTypeName(String itemTypeName) {
		this.itemTypeName = itemTypeName;
	}

	public String getItemName() {
		return itemName;
	}

	public void setItemName(String itemName) {
		this.itemName = itemName;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getRoleType() {
		return roleType;
	}

	public void setRoleType(String roleType) {
		this.roleType = roleType;
	}

	public String getDomainId() {
		return domainId;
	}

	public void setDomainId(String domainId) {
		this.domainId = domainId;
	}

	public String getIsValid() {
		return isValid;
	}

	public void setIsValid(String isValid) {
		this.isValid = isValid;
	}

	public String getIsValidName() {
		return isValidName;
	}

	public void setIsValidName(String isValidName) {
		this.isValidName = isValidName;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getModifyName() {
		return modifyName;
	}

	public void setModifyName(String modifyName) {
		this.modifyName = modifyName;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getStatusName() {
		return statusName;
	}

	public void setStatusName(String statusName) {
		this.statusName = statusName;
	}
	
}
