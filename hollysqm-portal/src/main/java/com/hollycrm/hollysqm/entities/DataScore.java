package com.hollycrm.hollysqm.entities;

/**
 * @author liujr
 * 2017年3月30日
 * @Description 成绩列表
 */
public class DataScore {
	
	private String paperType;//质检来源
	
	private String paperTypeName;
	
	private String planId;
	
	private String planName;//质检计划
	
	private String caller;//来电号码
	
	private String startTime;//来电时间
	
	private Integer totalScore;//评分得分
	
	private String createTime;//评分时间
	
	private String qualityStatus;//状态
	
	private String qualityStatusName;
	
	private Integer reviewScore;//复核得分
	
	private String createCode;//质检员
	
	private String createCodeName;
	
	private String agentCode;//坐席
	
	private String agentCodeName;
	
	private String checkScore;//评价星级
	
	private String paperId;
	
	private String caseItem;//案例类型
	
	private String modifyTime;//状态修改时间
	
	private String closeType;//关闭类型
	
	private String sessionLevel;//会话等级
	
	private String isPublicResult;//是否发布成绩
	
	private String caseId;//典型案例ID
	
	private Integer hasRead;//坐席是否查看

	public String getPlanName() {
		return planName;
	}

	public void setPlanName(String planName) {
		this.planName = planName;
	}

	public String getCaller() {
		return caller;
	}

	public void setCaller(String caller) {
		this.caller = caller;
	}

	public String getStartTime() {
		return startTime;
	}

	public void setStartTime(String startTime) {
		this.startTime = startTime;
	}

	public Integer getTotalScore() {
		return totalScore;
	}

	public void setTotalScore(Integer totalScore) {
		this.totalScore = totalScore;
	}

	public String getCreateTime() {
		return createTime;
	}

	public void setCreateTime(String createTime) {
		this.createTime = createTime;
	}

	public String getQualityStatus() {
		return qualityStatus;
	}

	public void setQualityStatus(String qualityStatus) {
		this.qualityStatus = qualityStatus;
	}

	public Integer getReviewScore() {
		return reviewScore;
	}

	public void setReviewScore(Integer reviewScore) {
		this.reviewScore = reviewScore;
	}

	public String getCreateCode() {
		return createCode;
	}

	public void setCreateCode(String createCode) {
		this.createCode = createCode;
	}

	public String getPaperId() {
		return paperId;
	}

	public void setPaperId(String paperId) {
		this.paperId = paperId;
	}

	public String getPaperType() {
		return paperType;
	}

	public void setPaperType(String paperType) {
		this.paperType = paperType;
	}

	public String getPaperTypeName() {
		return paperTypeName;
	}

	public void setPaperTypeName(String paperTypeName) {
		this.paperTypeName = paperTypeName;
	}

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public String getQualityStatusName() {
		return qualityStatusName;
	}

	public void setQualityStatusName(String qualityStatusName) {
		this.qualityStatusName = qualityStatusName;
	}

	public String getCreateCodeName() {
		return createCodeName;
	}

	public void setCreateCodeName(String createCodeName) {
		this.createCodeName = createCodeName;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getAgentCodeName() {
		return agentCodeName;
	}

	public void setAgentCodeName(String agentCodeName) {
		this.agentCodeName = agentCodeName;
	}

	public String getCheckScore() {
		return checkScore;
	}

	public void setCheckScore(String checkScore) {
		this.checkScore = checkScore;
	}

	public String getCaseItem() {
		return caseItem;
	}

	public void setCaseItem(String caseItem) {
		this.caseItem = caseItem;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}

	public String getCloseType() {
		return closeType;
	}

	public void setCloseType(String closeType) {
		this.closeType = closeType;
	}

	public String getSessionLevel() {
		return sessionLevel;
	}

	public void setSessionLevel(String sessionLevel) {
		this.sessionLevel = sessionLevel;
	}

	public String getIsPublicResult() {
		return isPublicResult;
	}

	public void setIsPublicResult(String isPublicResult) {
		this.isPublicResult = isPublicResult;
	}

	public String getCaseId() {
		return caseId;
	}

	public void setCaseId(String caseId) {
		this.caseId = caseId;
	}

	public Integer getHasRead() {
		return hasRead;
	}

	public void setHasRead(Integer hasRead) {
		this.hasRead = hasRead;
	}
	
	
}
