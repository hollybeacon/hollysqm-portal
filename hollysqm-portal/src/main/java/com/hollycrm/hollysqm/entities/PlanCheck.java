package com.hollycrm.hollysqm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;


/**
 * @author liujr
 * 2017年3月21日
 * @Description 质检评价表
 */
@Entity
@Table(name="TBL_QM_PLAN_CHECK")
public class PlanCheck{
	
	@Id
	@Column(name="plan_id",unique = true,nullable = false,length = 40)
	private String planId;//质检单表ID
	
	@Column(name="check_score",length = 22)
	private Integer checkScore;//检查评价分数（星级）
	
	@Column(name="check_info",length = 1000)
	private String checkInfo;//检查评价内容
	
	@Column(name="check_status",length = 2)
	private String checkStatus;//评价状态：0.待评价1.已评价
	
	@Column(name="modifier",length = 32)
	private String modifier;//修改人账号
	
	@Column(name="modify_time",length = 19)
	private String modifyTime;//修改时间

	public String getPlanId() {
		return planId;
	}

	public void setPlanId(String planId) {
		this.planId = planId;
	}

	public Integer getCheckScore() {
		return checkScore;
	}

	public void setCheckScore(Integer checkScore) {
		this.checkScore = checkScore;
	}

	public String getCheckInfo() {
		return checkInfo;
	}

	public void setCheckInfo(String checkInfo) {
		this.checkInfo = checkInfo;
	}

	public String getCheckStatus() {
		return checkStatus;
	}

	public void setCheckStatus(String checkStatus) {
		this.checkStatus = checkStatus;
	}

	public String getModifier() {
		return modifier;
	}

	public void setModifier(String modifier) {
		this.modifier = modifier;
	}

	public String getModifyTime() {
		return modifyTime;
	}

	public void setModifyTime(String modifyTime) {
		this.modifyTime = modifyTime;
	}

}
