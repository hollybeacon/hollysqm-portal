package com.hollycrm.hollysqm.entities;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.hollycrm.hollybeacon.basic.interfaces.dao.BaseEntity;

/**
 * 质检计划绑定的人员信息实体类
 * @author wangyf
 *
 */

public class User extends BaseEntity<String>{

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private String userId;

	/**
	 * 人员账号
	 */
	private String userCode;
	
	/**
	 * 人员工号
	 */
	private String agentCode;
	
	
	/**
	 * 所属部门
	 */
	private String department;
	
	/**
	 * 人员姓名
	 */
	private String username;
	
	/**
	 * 部门名称
	 */
	private String deptName;
	
	public User() {
		
	}
	
	public User(String userCode, String agentCode, String department, String username) {
		this.agentCode = agentCode;
		this.userCode = userCode;
		this.department = department;
		this.username = username;
	}

	public String getUserCode() {
		return userCode;
	}

	public void setUserCode(String userCode) {
		this.userCode = userCode;
	}

	public String getAgentCode() {
		return agentCode;
	}

	public void setAgentCode(String agentCode) {
		this.agentCode = agentCode;
	}

	public String getDepartment() {
		return department;
	}

	public void setDepartment(String department) {
		this.department = department;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getDeptName() {
		return deptName;
	}

	public void setDeptName(String deptName) {
		this.deptName = deptName;
	}
	
	
	
	
}
