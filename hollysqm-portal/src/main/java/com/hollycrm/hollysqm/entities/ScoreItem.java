package com.hollycrm.hollysqm.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.hollycrm.hollybeacon.basic.interfaces.dao.BaseEntity;

/**
 * 质检打分项
 * @author wangyf
 *
 */
@Entity
@Table(name="TBL_QM_SCORE_ITEM")
public class ScoreItem extends BaseEntity<String>{
	
	/**
	 * 打分项ID
	 */
	@Id
	@Column(name="SCORE_ITEM_ID",unique=true,nullable=false)
	private String scoreItemId;
	
	@Column(name="STANDARD_ID")
	private String standardId;
	@Column(name="SCORE_LIST_ID")
	private String scoreListId;
	@Column(name="REMARK")
	private String remark;
	
	/**
	 * 打分项名字
	 */
	@Transient
	private String scoreItemName;
	
	/**
	 * 分数
	 */
	@Transient
	private String score;
	
	/**
	 * 是否加减分项目
	 */
	@Transient
	private String  scoreType;
	
	/**
	 * 项目分类
	 */
	@Transient
	private String itemType;
	
	/**
	 * 质检标签ID
	 */
	@Transient
	private String textItemId;
	
	/**
	 *是否选中 
	 */
	@Transient
	private String isChecked;
	
	/**
	 * 排序号
	 */
	@Column(name="ORDER_NO")
	private int orderNo;

	public String getScoreItemId() {
		return scoreItemId;
	}

	public void setScoreItemId(String scoreItemId) {
		this.scoreItemId = scoreItemId;
	}

	public String getScoreItemName() {
		return scoreItemName;
	}

	public void setScoreItemName(String scoreItemName) {
		this.scoreItemName = scoreItemName;
	}

	public String getScore() {
		return score;
	}

	public void setScore(String score) {
		this.score = score;
	}

	public String getScoreType() {
		return scoreType;
	}

	public void setScoreType(String scoreType) {
		this.scoreType = scoreType;
	}

	public String getItemType() {
		return itemType;
	}

	public void setItemType(String itemType) {
		this.itemType = itemType;
	}

	public String getTextItemId() {
		return textItemId;
	}

	public void setTextItemId(String textItemId) {
		this.textItemId = textItemId;
	}

	public String getIsChecked() {
		return isChecked;
	}

	public void setIsChecked(String isChecked) {
		this.isChecked = isChecked;
	}

	public int getOrderNo() {
		return orderNo;
	}

	public void setOrderNo(int orderNo) {
		this.orderNo = orderNo;
	}

	public String getStandardId() {
		return standardId;
	}

	public void setStandardId(String standardId) {
		this.standardId = standardId;
	}

	public String getScoreListId() {
		return scoreListId;
	}

	public void setScoreListId(String scoreListId) {
		this.scoreListId = scoreListId;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}
	
	
}
