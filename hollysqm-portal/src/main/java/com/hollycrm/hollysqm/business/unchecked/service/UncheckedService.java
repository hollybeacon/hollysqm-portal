package com.hollycrm.hollysqm.business.unchecked.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.rest.web.util.WebUtil;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollysqm.business.common.dao.UserDao;
import com.hollycrm.hollysqm.entities.Paper;
import com.hollycrm.hollysqm.entities.UnsolvedPaper;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * 未复核质检单业务类
 * @author wangyf
 *
 * 2017年3月15日
 */
@Service("uncheckedService")
public class UncheckedService {

	@Autowired
	private ConfigedJdbc configedJdbc;
	
	@Resource(name="usersDao")
	private UserDao userDao;
	
	/**
	 * 查询未复核质检单列表
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public PageResult<Paper> getUncheckedPaperList(Map<String, Object> param) {
		@SuppressWarnings("unchecked")
		PageResult<Paper> page = (PageResult<Paper>)HollysqmUtil.createPageResult(param);	
		String userCode = WebUtil.getLoginUser().getUserCode();
		param.put("checkerCode", userCode);
		
		param.put("checkerCode", userCode);
		if(StringUtils.isNotEmpty(param.get("agent"))) {
			String agent = userDao.getUserCodeOrNo(param.get("agent").toString(), "agentCode");
			agent = HollysqmUtil.paramParseQueryText(agent, "sql", true);
			if(StringUtils.isNotEmpty(agent)) {
				param.put("agent", agent);
			} else {
				param.put("agent", "'" + param.get("agent").toString() + "'");
			}
		}
		
		if(StringUtils.isNotEmpty(param.get("checker"))) {
			String checker = userDao.getUserCodeOrNo(param.get("checker").toString(), "agentCode");
			checker = HollysqmUtil.paramParseQueryText(checker, "sql", true);
			if(StringUtils.isNotEmpty(checker)) {
				param.put("checker", checker);
			} else {
				param.put("checker", "'" + param.get("checker").toString() + "'");
			}
		}
		
		String sqlKey = "velocity.score.queryUnChecked";
		page = configedJdbc.selectPageResult(sqlKey, param, page, Paper.class);
		Long lon = configedJdbc.selectCount(sqlKey, param);
		page.setTotal(lon);	
		
		return page;
	}
	
	

	/**
	 * 获取未复核
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public UnsolvedPaper getUncheckedPaperMsg(Map<String, Object> param) {
		if(StringUtils.isEmpty(param.get("paperId"))) {
			return null;
		}
		
		String userCode = WebUtil.getLoginUser().getUserCode();
		param.put("checkerCode", userCode);
		
		if(StringUtils.isNotEmpty(param.get("agent"))) {
			String agent = userDao.getUserCodeOrNo(param.get("agent").toString(), "agentCode");
			agent = HollysqmUtil.paramParseQueryText(agent, "sql", true);
			param.put("agent", agent);
		}
		
		if(StringUtils.isNotEmpty(param.get("checker"))) {
			String checker = userDao.getUserCodeOrNo(param.get("checker").toString(), "agentCode");
			checker = HollysqmUtil.paramParseQueryText(checker, "sql", true);
			param.put("checker", checker);
		}
		StringBuilder sql = new StringBuilder();
		sql.append("with tt as( ");
		sql.append("select t1.paper_id paperId, t3.appeal_time appealTime from tbl_qm_paper t1 left join ");
		sql.append("tbl_qm_plan t2 on t1.plan_id = t2.plan_id left join tbl_qm_appeall t3 ");
		sql.append("on t1.paper_id = t3.paper_id where t1.quality_status = '2' ");
		if(StringUtils.isNotEmpty(param.get("planId"))) {//计划名称
			sql.append("and t2.plan_id :planId ");
		}
		
		if(StringUtils.isNotEmpty(param.get("dataType"))) {//数据来源
			sql.append("and t1.paper_type = :dataType ");
		}
		
		if(StringUtils.isNotEmpty(param.get("agent"))) {//坐席信息
			sql.append("and t1.agent_code in  " + param.get("agent").toString() + " ");
		}
		
		if(StringUtils.isNotEmpty(param.get("checker"))) {//质检员信息
			sql.append("and t1.checker_agent_code in  " + param.get("checker").toString() + " ");
		}
		
		if(StringUtils.isNotEmpty(param.get("startTime"))) {//开始时间
			sql.append("and substr(t3.appeal_time, 1, 10) >= :startTime ");
		}
		
		if(StringUtils.isNotEmpty(param.get("endTime"))) {//结束时间
			sql.append("and substr(t3.appeal_time, 1, 10) <= :endTime ");
		}
		
		if(StringUtils.isNotEmpty(param.get("checkerCode"))) {//当前质检员账号
			sql.append("and t1.checker_code = :checkerCode ");
		}
		sql.append(" order by t3.appeal_time desc, t1.paper_id desc ");
		sql.append(") ");
		
		sql.append("select nextPaperId, (select count(1) from tt) paperCount  from (select tt.paperId, ");
		sql.append("lead(tt.paperId, 1, 0) over(order by tt.appealTime desc, tt.paperId desc) as nextPaperId from tt) ");
		sql.append("where paperId = :paperId");
		
		List<UnsolvedPaper> list = configedJdbc.queryForList(sql.toString(), param, UnsolvedPaper.class);
		if(list == null || list.isEmpty()) {
			return null;
		} else {
			return list.get(0);
		}
	}
}
