package com.hollycrm.hollysqm.business.judgechecker.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.util.DateUtils;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollybeacon.business.personoa.security.entities.SysUser;
import com.hollycrm.hollybeacon.business.personoa.system.cache.DictionaryCacheHandler;
import com.hollycrm.hollysqm.business.common.dao.UserDao;
import com.hollycrm.hollysqm.business.judgechecker.dao.JudgeCheckerDao;
import com.hollycrm.hollysqm.entities.Paper;
import com.hollycrm.hollysqm.entities.PaperCheckData;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * 评价质检员业务实现
 * @author wangyf
 *
 * 2017年3月21日
 */

@Service("judgeCheckerService")
public class JudgeCheckerService {

	@Autowired
	private ConfigedJdbc configedJdbc;

	@Resource(name="judgeCheckerDao")
	private JudgeCheckerDao judgeCheckerDao;
	

	@Resource(name = "dictionaryCacheHandler")
	private DictionaryCacheHandler dictionaryCacheHandler;
	
	@Resource(name="usersDao")
	private UserDao userDao;

	/**
	 * 查找质检单列表
	 * @param param
	 * @return
	 * @throws 
	 * @author wangyf
	 */
	public PageResult<Paper> getPaperList(Map<String, Object> param) {
		@SuppressWarnings("unchecked")
		PageResult<Paper> page = (PageResult<Paper>)HollysqmUtil.createPageResult(param);
		if(StringUtils.isNotEmpty(param.get("agent"))) {
			String agent = userDao.getUserCodeOrNo(param.get("agent").toString(), "agentCode");
			agent = HollysqmUtil.paramParseQueryText(agent, "sql", true);
			if(StringUtils.isNotEmpty(agent)) {
				param.put("agent", agent);
			} else {
				param.put("agent", "'" + param.get("agent").toString() + "'");
			}
		}
		
		if(StringUtils.isNotEmpty(param.get("checker"))) {
			String checker = userDao.getUserCodeOrNo(param.get("checker").toString(), "agentCode");
			checker = HollysqmUtil.paramParseQueryText(checker, "sql", true);
			if(StringUtils.isNotEmpty(checker)) {
				param.put("checker", checker);
			} else {
				param.put("checker", "'" + param.get("checker").toString() + "'");
			}
		}
		String sqlKey = "velocity.score.queryJudgeList";
		page = configedJdbc.selectPageResult(sqlKey, param, page, Paper.class);
		Long lon = configedJdbc.selectCount(sqlKey, param);
		page.setTotal(lon);	
		return page;		
	}
	
	
	/**
	 * 评价质检单
	 * @param param
	 * @throws
	 * @author wangyf
	 */
	public boolean evaluatePaper(Map<String,Object> param) {
		if(StringUtils.isEmpty(param.get("paperId"))) {
			return false;
		} else {
			PaperCheckData p = judgeCheckerDao.findOne("from PaperCheckData where paperId = :paperId", param);
			if(p == null) {
				p = new PaperCheckData();
			}
			HollysqmUtil.fillParam(param, PaperCheckData.class, p);
			p.setModifier(((SysUser)SecurityUtils.getSubject().getPrincipal()).getUserCode());
			p.setModifyTime(DateUtils.getCurrentDateTimeAsString());
			judgeCheckerDao.save(p);
			return true;
		}
		
	}

	
	/**
	 * 查询质检单对应的质检评价
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public PaperCheckData getEvaluateResult(Map<String, Object> param) {
		StringBuilder sql = new StringBuilder();
		sql.append("select t1.paper_id paperId, check_score checkScore, check_info checkInfo, check_status checkStatus, ");
		sql.append("t1.modifier, t1.modify_time modifyTime, t2.user_name mofierName from tbl_qm_plan_check t1 ");
		sql.append("left join tbl_sys_user t2 on t1.modifier = t2.user_code where t1.paper_id = :paperId");
		
		List<PaperCheckData> list = configedJdbc.queryForList(sql.toString(), param, PaperCheckData.class);
		
		if(list != null && list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}
	}
	
}
