package com.hollycrm.hollysqm.business.common.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.CollectionUtils;

import com.hollycrm.hollybeacon.basic.util.PropertiesUtils;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollysqm.business.common.dao.UserDao;
import com.hollycrm.hollysqm.core.vo.I8DocBean;
import com.hollycrm.hollysqm.core.vo.V8DocBean;
import com.hollycrm.hollysqm.entities.Record;
import com.hollycrm.hollysqm.entities.User;
import com.hollycrm.hollysqm.util.Constant;
import com.hollycrm.hollysqm.util.HollysqmUtil;


/**
 * 用于solr数据查询的业务类，提供公共方法给子类继承使用
 * @author wangyf
 *
 * 2017年4月27日
 */
public class SolrTextSearchService {
	
	@Resource(name="usersDao")
	private UserDao userDao;
	
	/**
	 * 根据参数初始化solr查询参数map
	 * @param tag
	 * @param param
	 * @param userDao
	 * @return
	 */
	public Map<String,String> initParamMap(String tag, Map<String,Object> param) {
		HashMap<String,String> paramMap = new HashMap<String,String>();
		// 坐席信息过滤
		if (StringUtils.isNotEmpty(param.get("agent"))) {
			String userNo = null;
			if(param.get("agent").toString().indexOf(",") != -1) {
				userNo = userDao.getUserCodeOrNo(param.get("agent").toString().trim(), "userNo");
				userNo = HollysqmUtil.paramParseQueryText(userNo, "solr", this.isStringField(V8DocBean.class, "userCode"));
			} else {
				userNo = userDao.getUserCodeOrNo(param.get("agent").toString().trim(), "userNo");
				userNo = HollysqmUtil.paramParseQueryText(userNo, "solr", this.isStringField(I8DocBean.class, "agentCode"));
			}
			if (userNo == null)//找不到坐席，则不需要查询Solr
				return null;
			if(tag.equals(Constant.V8)) {
				paramMap.put("userCode", userNo);
			} else if(tag.equals(Constant.I8)) {
				paramMap.put("agentCode", userNo);
			}
		}
		
		// 主叫号码信息过滤
		if (StringUtils.isNotEmpty(param.get("caller"))) {
			if(tag.equals(Constant.V8)) {
				paramMap.put(Constant.caller_v8, param.get("caller").toString());
			} else if(tag.equals(Constant.I8)) {
				paramMap.put(Constant.caller_i8, param.get("caller").toString());
			}
		}
		// 质检状态信息过滤
		if (StringUtils.isNotEmpty(param.get("qualityStatus"))) {
			if(!StringUtils.isEmpty(param.get("startTime").toString())) {
				int activeTime = Integer.parseInt(PropertiesUtils.getProperty("solr.qualityStatus.activeTime"));
				int startTime = Integer.parseInt(param.get("startTime").toString().replaceAll("-", ""));
				if(startTime > activeTime) {
					paramMap.put("qualityStatus", param.get("qualityStatus").toString());
				}
			} 
			
		}
		// 开始时间结束时间信息过滤,两者必须同时存在
		if (StringUtils.isNotEmpty(param.get("startTime")) && StringUtils.isNotEmpty("endTime")) {
			String startTime = param.get("startTime").toString();
			String endTime = param.get("endTime").toString();
			startTime = startTime.replaceAll(":", "").replaceAll(" ", "").replaceAll("-", "");
			endTime = endTime.replaceAll(":", "").replaceAll(" ", "").replaceAll("-", "");
			paramMap.put(Constant.acceptTime, Constant.getAssemble("\"" + startTime + "\"", "\"" + endTime + "\""));
		}
		//满意度过滤
		if(StringUtils.isNotEmpty(param.get("satisfaction"))) {
			if(tag.equals(Constant.V8)) {
				paramMap.put(Constant.satisfication_v8, HollysqmUtil.paramParseQueryText(param.get("satisfaction").toString(), "solr", this.isStringField(V8DocBean.class, "satisfication")));
			} else if(tag.equals(Constant.I8)) {
				paramMap.put(Constant.satisfication_i8, HollysqmUtil.paramParseQueryText(param.get("satisfaction").toString(), "solr", this.isStringField(I8DocBean.class, "satisfaction")));
			}
			
		}
		//根据数据来源进行过滤
		if(tag.equals(Constant.V8)) {
			
			if(StringUtils.isNotEmpty(param.get("businessType"))) {
				paramMap.put(Constant.businessType, HollysqmUtil.paramParseQueryText(param.get("businessType").toString(), "solr",this.isStringField(V8DocBean.class, "businessType")));
			}
			if(StringUtils.isNotEmpty(param.get("custArea"))) {
				paramMap.put(Constant.custArea, HollysqmUtil.paramParseQueryText(param.get("custArea").toString(),"solr",this.isStringField(V8DocBean.class, "custArea")));
			}
			if(StringUtils.isNotEmpty(param.get("custBand"))) {
				paramMap.put(Constant.custBand, HollysqmUtil.paramParseQueryText(param.get("custBand").toString(),"solr",this.isStringField(V8DocBean.class, "custBand")));
			}
			if(StringUtils.isNotEmpty(param.get("custLevel"))) {
				paramMap.put(Constant.custLevel, HollysqmUtil.paramParseQueryText(param.get("custLevel").toString(),"solr",this.isStringField(V8DocBean.class, "custLevel")));
			}
			if(StringUtils.isNotEmpty(param.get("recoinfoLength"))) {
				paramMap.put(Constant.recoinfoLength, String.format("[%s]", param.get("recoinfoLength").toString().replace("-", " TO ")));
			}
			
			if(StringUtils.isNotEmpty(param.get("silenceLength"))) {
				paramMap.put(Constant.silenceLength, String.format("[%s]", param.get("silenceLength").toString().replace("-", " TO ")));
			}
		} else if(tag.equals(Constant.I8)){
			if(StringUtils.isNotEmpty(param.get("closeType"))) {
				paramMap.put(Constant.closeType, HollysqmUtil.paramParseQueryText(param.get("closeType").toString(),"solr",this.isStringField(I8DocBean.class, "closeType")));
			}
			if(StringUtils.isNotEmpty(param.get("serviceType"))) {
				paramMap.put(Constant.serviceType, HollysqmUtil.paramParseQueryText(param.get("serviceType").toString(),"solr",this.isStringField(I8DocBean.class, "serviceType")));
			}
			if(StringUtils.isNotEmpty(param.get("sessionLength"))) {
				paramMap.put(Constant.sessionLength, String.format("[%s]", param.get("sessionLength").toString().replace("-", " TO ")));
			}
			if(StringUtils.isNotEmpty(param.get("sessionType"))) {
				paramMap.put(Constant.sessionType, HollysqmUtil.paramParseQueryText(param.get("sessionType").toString(),"solr",this.isStringField(I8DocBean.class, "sessionType")));
			} 	
		} 
		
		return paramMap;
	}
	
	/**
	 * 封装Solr数据
	 * @param objList
	 * @param tag
	 * @param records
	 * @param userCodeList
	 */
	public void loadSolrData(List<?> objList, String tag, List<Record> records ,List<String> agentCodeList){
		Record record = null;
		User user = null;
		String agentCode = null;
		V8DocBean v8Bean = null;
		I8DocBean i8Bean = null;
		for (Object obj : objList){
			record = new Record();
			record.setDataType(tag);
			user = new User();
			if (obj instanceof V8DocBean){
				v8Bean = (V8DocBean)obj;
				HollysqmUtil.parseV8DocBeanToRecord(v8Bean, record);
				agentCode = v8Bean.getUserCode();//solr中的usercode对应用户表的userNo
			}else if (obj instanceof I8DocBean) {
				i8Bean = (I8DocBean)obj;
				HollysqmUtil.parseI8DocBeanToRecord(i8Bean, record);
				agentCode = i8Bean.getAgentCode();
			} else {
				continue;
			}
			user.setAgentCode(agentCode);
			record.setUser(user);
			records.add(record);
			agentCodeList.add(agentCode);
		}
	}
	
	/**
	 * 获取用户的Code和Name
	 * @param userCodeList
	 * @return
	 */
	public Map<String,String> getUsers(List<String> agentCodeList){
		if (CollectionUtils.isNotEmpty(agentCodeList)) {
			String agentCodeStr = "('" + StringUtils.join(agentCodeList,"','") + "')";	
			return userDao.getUserList(agentCodeStr);
		}		
		return null;
	}
	
	/**
	 * 设置Solr结果集中的质检员信息 
	 * @param recordList
	 * @param userCodeList
	 */
	public void setRecordUser(List<Record> recordList ,List<String> agentCodeList){
		if (CollectionUtils.isNotEmpty(recordList) && CollectionUtils.isNotEmpty(agentCodeList)){
			Map<String, String> map = this.getUsers(agentCodeList);
			if (map != null && map.size()>0){			
				User user = null;
				String agentCode = null;
				for (Record tempObj : recordList){
					user  = tempObj.getUser();
					agentCode = user.getAgentCode();
					user.setUsername((map.get(agentCode) !=null ? map.get(agentCode) : "") +"("+agentCode+")");
				}
			}
		}
	}
	
	public boolean isStringField(Class clazz,String field) {
		try {
			if(clazz.getDeclaredField(field).getType().equals(String.class)) {
				return true;
			} else {
				return false;
			}
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return false;
		} 
	}

	
}
