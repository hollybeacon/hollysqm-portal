package com.hollycrm.hollysqm.business.executioninfo.rest;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollybeacon.basic.util.DateUtils;
import com.hollycrm.hollysqm.business.executioninfo.service.ExecutionInfoService;
import com.hollycrm.hollysqm.entities.DataCoordinate;
import com.hollycrm.hollysqm.entities.DataRank;
import com.hollycrm.hollysqm.entities.DataStatus;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * @author liujr
 * 2017年3月23日
 * @Description //质检完成情况
 */
@RestService
@RequestMapping(value="/executioninfo")
public class ExecutionInfoRest {
	
	@Resource(name="executionInfoService")
	private ExecutionInfoService executionInfoService;
	
	@RequestMapping(value = "/executioninfoPage", method = RequestMethod.GET)
	public ModelAndView executioninfoPage(@RequestParam Map<String,String> param){
		String currentDate = DateUtils.getCurrentDateAsString();
		param.put("startTime", currentDate);
		param.put("endTime", currentDate);
		return new ModelAndView("forward:/hollysqm/executioninfo/page/executioninfo.jsp", param);
	}
	
	/** 
	* @Description:平均耗时排名
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月23日
	*/
	@RequestMapping(value="/executionInfoRank",method = RequestMethod.GET)
	public ServiceResult executionInfoRank(@RequestParam Map<String,Object> param){
		List<DataRank> list = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			list = executionInfoService.executionInfoRank(param);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(list, flag, errorMessage);
	}

	/** 
	* @Description:完成情况
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月23日
	*/
	@RequestMapping(value="/executionInfo",method = RequestMethod.GET)
	public ServiceResult executionInfo(@RequestParam Map<String,Object> param){
		DataStatus map = null;
		boolean flag = false;
		String errorMessage = "";
		try {
		    map = executionInfoService.executionInfo(param);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(map, flag, errorMessage);
	}

	/** 
	* @Description:时间分布查询
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月23日
	*/
	@RequestMapping(value="/timeDistribution",method = RequestMethod.GET)
	public ServiceResult timeDistribution(@RequestParam Map<String,Object> param){
		List<DataCoordinate> list = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			list = executionInfoService.timeDistribution(param);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(list, flag, errorMessage);
	}

	/** 
	* @Description:催一下
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月23日
	*/
	@RequestMapping(value="/urgePaper",method = RequestMethod.POST)
	public ServiceResult urgePaper(@RequestParam Map<String,Object> param){
		Map<String, Integer> map = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			map = executionInfoService.urgePaper(param);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(map, flag, errorMessage);
	}
	
}
