package com.hollycrm.hollysqm.business.textsearch.dao;

import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.orm.dao.jpa.JpaBaseDaoImpl;
import com.hollycrm.hollysqm.entities.MyCollection;

/**
 * 我的收藏Dao
 * @author wangyf
 *
 * 2017年3月23日
 */
@Service("collectionDao")
public class CollectionDao extends JpaBaseDaoImpl<MyCollection, String>{

}
