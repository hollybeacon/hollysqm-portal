package com.hollycrm.hollysqm.business.textsearch.rest;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.shiro.SecurityUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hollycrm.hollybeacon.basic.core.annotations.ActionLog;
import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.csv.CsvExport;
import com.hollycrm.hollybeacon.basic.csv.config.CsvConfiguration;
import com.hollycrm.hollybeacon.basic.csv.config.CsvTable.CsvField;
import com.hollycrm.hollybeacon.basic.excle.ExcleExport;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.pdf.PdfExport;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollybeacon.basic.util.WebUtils;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollybeacon.business.personoa.security.entities.SysUser;
import com.hollycrm.hollysqm.business.common.service.CommonService;
import com.hollycrm.hollysqm.business.textsearch.service.TextSearchService;
import com.hollycrm.hollysqm.entities.PaperData;
import com.hollycrm.hollysqm.entities.Record;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * 全文检索
 * @author wangyf
 *
 * 2017年4月7日
 */
@RestService
@RequestMapping(value="/textSearchRest")
public class TextSearchRest {
	protected final Log logger = LogFactory.getLog(getClass());
	
	@Resource(name="textSearchService")
	private TextSearchService textSearchService;	
	
	@Resource(name = "csvExport")
	protected CsvExport csvExport;

	@Resource(name = "pdfExport")
	protected PdfExport pdfExport;

	@Resource(name = "excleExport")
	protected ExcleExport excleExport;
	
	@Resource(name = "csvConfig")
	protected CsvConfiguration csvConfig;
	
	@Resource(name = "commonService")
	private CommonService commonService;
	/**
	 * 查询语音文本列表
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/getTextResultList", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getTextResultList(@RequestParam Map<String, Object> param) {
		PageResult<Record> page = textSearchService.getTextResultList(param);
		
		return HollysqmUtil.wrapUpPageResult(page, true, "");
	}
	
	/** 
	* @Description:全文检索导出 
	* @param:@param tableId
	* @param:@param options
	* @param:@param exportType
	* @param:@param param
	* @param:@param request
	* @param:@param response
	* @param:@throws Exception    设定文件 
	* @return:void    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年4月26日
	*/
	@ActionLog(content = "导出rest方法", description = "调用基类统一的导出列表方法")
	@RequestMapping(value = "/export", method = RequestMethod.POST)
	public void export(
			@RequestParam String tableId,
			@RequestParam(required = false) List<Integer> options,
			@RequestParam(required = false, defaultValue = "csv") String exportType,
			@RequestParam Map<String, Object> param,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		logger.debug("导出rest方法 ,查询参数：" + param );
		// 1.从DB获取数据
		
		List<Record> list = textSearchService.exportTextSearch(param);

		List<? extends CsvField> fields = csvConfig.getExportedCsvFields(tableId);
		//fields = this.expandFields(request, fields);
		// 2.写入临时文件
		File tempDir = WebUtils.getTempDir(request.getServletContext());
		File tmpFile = null;
		if ("pdf".equalsIgnoreCase(exportType)) {
			tmpFile = new File(tempDir, UUID.randomUUID().toString() + ".pdf");
			if (options != null) {
				pdfExport.expToFile(list, tableId, tmpFile, fields, options);
			} else {
				pdfExport.expToFile(list, tableId, tmpFile, fields);
			}
		}else if("Excle".equalsIgnoreCase(exportType)){
			tmpFile = new File(tempDir, UUID.randomUUID().toString() + ".xls");
			if (options != null) {
				excleExport.expToFile(list, tableId, tmpFile, fields, options);
			} else {
				excleExport.expToFile(list, tableId, tmpFile, fields);
			}
		} else {
			tmpFile = new File(tempDir, UUID.randomUUID().toString() + ".csv");
			if (options != null) {
				csvExport.expToFile(list, tableId, tmpFile, fields, options);
			} else {
				csvExport.expToFile(list, tableId, tmpFile, fields);
			}
		}
		logger.debug("生成文件存储位置:" + tmpFile.getAbsolutePath());
		// 3.下载
		InputStream in = new FileInputStream(tmpFile);
		WebUtils.download(response, in, tmpFile.getName());
	}

	/**
	 * 根据接触记录查询质检单id和质检状态
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/getForward", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getForward(@RequestParam Map<String, Object> param) {
		if(StringUtils.isEmpty(param.get("contactId"))) {
			return HollysqmUtil.wrapUpPageResult(null, false, ""); 
		}
		Map<String,Object> map = new HashMap<String, Object>();
		String paperId = "";
		String forward = "";
		PaperData paperData = textSearchService.getForward(param);
		if(paperData == null) {
			forward = "0";
		} else{
			paperId = paperData.getPaperId();
			if(paperData.getQualityStatus().equals("0")) {
				forward = "1";
			} else if(paperData.getQualityStatus().equals("2")) {
				forward = "2";
			} else {
				forward = "3";
			}
		}
		
		map.put("paperId", paperId);
		map.put("forward", forward);
		return HollysqmUtil.wrapUpPageResult(map, true, ""); 
	}
	
	
	/**
	 * 根据接触记录绑定的评分模板返回跳转页面
	 * @param param
	 * @return
	 */
	@RequestMapping(value="/getForwardByStandard", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getForwardByStandard(@RequestParam Map<String, Object> param) {
		if(StringUtils.isEmpty(param.get("contactId")) || StringUtils.isEmpty(param.get("standardId"))) {
			return HollysqmUtil.wrapUpPageResult(null, false, ""); 
		}
		
		Map<String,Object> map = new HashMap<String, Object>();
		String paperId = "";
		String forward = "";
		PaperData paperData = textSearchService.getForward(param);
		if(paperData == null) {
			if(StringUtils.isEmpty(param.get("paperType"))) {
				return HollysqmUtil.wrapUpPageResult(null, false, "无法获取当前接触记录所属的数据来源");  
			}
			paperData = commonService.createPaper(param);
			forward = "1";
			
		} else{
			String checkerAgentCode = ((SysUser)SecurityUtils.getSubject().getPrincipal()).getUserNo(); 
			if(paperData.getQualityStatus().equals("0")) {
				forward = "1";
				if(!paperData.getCheckerAgentCode().equals(checkerAgentCode)) {
					forward = "3";
				}
			} else if(paperData.getQualityStatus().equals("2")) {
				forward = "3";
				if(!paperData.getCheckerAgentCode().equals(checkerAgentCode)) {
					forward = "3";
				}
			} else {
				forward = "3";
			}
		}
		
		paperId = paperData.getPaperId();
		map.put("paperId", paperId);
		map.put("forward", forward);
		return HollysqmUtil.wrapUpPageResult(map, true, ""); 
	}
	
	
}
