package com.hollycrm.hollysqm.business.judgechecker.dao;

import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.orm.dao.jpa.JpaBaseDaoImpl;
import com.hollycrm.hollysqm.entities.PaperCheckData;

/**
 * 评价质检员Dao
 * @author wangyf
 *
 * 2017年3月23日
 */
@Service("judgeCheckerDao")
public class JudgeCheckerDao extends JpaBaseDaoImpl<PaperCheckData, String>{

}
