package com.hollycrm.hollysqm.business.common.service;

import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.rest.web.util.WebUtil;
import com.hollycrm.hollybeacon.basic.util.DateUtils;
import com.hollycrm.hollybeacon.basic.util.LabelValue;
import com.hollycrm.hollybeacon.basic.util.PropertiesUtils;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollybeacon.business.personoa.notice.entities.NoticeInfo;
import com.hollycrm.hollybeacon.business.personoa.notice.entities.NoticeUser;
import com.hollycrm.hollybeacon.business.personoa.security.entities.SysUser;
import com.hollycrm.hollybeacon.business.personoa.system.cache.DictionaryCacheHandler;
import com.hollycrm.hollybeacon.business.personoa.system.cache.SysConfigCacheHandler;
import com.hollycrm.hollysqm.bean.FtpConfigurBean;
import com.hollycrm.hollysqm.business.common.dao.*;
import com.hollycrm.hollysqm.business.itemmanager.service.ItemManagerService;
import com.hollycrm.hollysqm.business.judgechecker.dao.JudgeCheckerDao;
import com.hollycrm.hollysqm.business.standardmanager.dao.StandardmanagerDao;
import com.hollycrm.hollysqm.core.index.factory.IndexSearchFactory;
import com.hollycrm.hollysqm.core.index.service.IndexSearchService;
import com.hollycrm.hollysqm.core.vo.I8DocBean;
import com.hollycrm.hollysqm.core.vo.V8DocBean;
import com.hollycrm.hollysqm.entities.*;
import com.hollycrm.hollysqm.util.Constant;
import com.hollycrm.hollysqm.util.HollysqmUtil;
import com.hollycrm.hollysqm.util.UuidUtil;
import net.sf.json.JSONObject;
import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.time.DateFormatUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.io.File;
import java.util.*;

/**
 * 公共查询业务类
 * @author wangyf
 *
 * 2017年3月15日
 */
@Service("commonService")
public class CommonService {
	
	@Autowired
	private ConfigedJdbc configedJdbc;
	
	@Resource(name="paperDao")
	private PaperDao paperDao;
	
	@Resource(name="caseDao")
	private CaseDao caseDao;
	
	@Resource(name="usersDao")
	private UserDao userDao;
	
	@Resource(name="scoreRecordDao")
	private ScoreRecordDao scoreRecordDao;
	
	@Resource(name="standardManagerDao")
	private StandardmanagerDao standardmanagerDao;
	
	@Autowired
	private IndexSearchFactory indexSearchFactory;
	
	@Autowired
	private CustcontinfoDao custcontinfoDao;		
	
	@Resource(name="ftpSourceConfig")
	private FtpSourceConfig ftpSourceConfig;
	
	@Resource(name="commonNoticeService")
	private CommonNoticeService commonNoticeService;
	
	@Resource(name = "sysConfigCacheHandler")
	private SysConfigCacheHandler sysConfigCacheHandler;
	
	@Resource(name = "dictionaryCacheHandler")
	private DictionaryCacheHandler dictionaryCacheHandler;
	
	@Resource(name="judgeCheckerDao")
	private JudgeCheckerDao judgeCheckerDao;
	
	@Resource(name = "itemmanagerService")
	private ItemManagerService  itemmanagerService;
	

	/**
	 * 获取打分项详情
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public List<ScoreItem> getScoreItemList(Map<String, Object> param) {
		StringBuilder sql = new StringBuilder();
		
		if(StringUtils.isEmpty(param.get("orderNum"))) {
			return null;
		}
		
		if(param.get("orderNum").equals("0")) {//获取指定评分模板的所有评分项目
			sql.append("select  t3.score_item_id scoreItemId, t3.order_no orderNo, t4.item_name scoreItemName, t4.score, t4.score_type scoreType, ");
			sql.append("t4.item_type itemType, t4.text_item_id textItemId  ");
			
			if(StringUtils.isNotEmpty(param.get("standardId"))) {//根据质检单查询
				sql.append("from tbl_qm_standard t2 left join TBL_QM_SCORE_ITEM t3 on t2.standard_id = t3.standard_id ");
				sql.append("left join tbl_qm_score_list t4 on t3.score_list_id = t4.score_list_id ");
				sql.append("where t3.standard_id = :standardId ");
			} else if(StringUtils.isNotEmpty(param.get("paperId"))) {//根据评分模板查询
				sql.append("from tbl_qm_paper t1 left join tbl_qm_standard t2 on t1.standard_id = t2.standard_id left join TBL_QM_SCORE_ITEM t3 on t2.standard_id = t3.standard_id ");
				sql.append("left join tbl_qm_score_list t4 on t3.score_list_id = t4.score_list_id ");
				sql.append("where t1.paper_id = :paperId ");
			} else {
				return null;
			}
			
		} else {
			if(StringUtils.isEmpty(param.get("paperId"))) {
				return null;
			}
			
			if(param.get("orderNum").equals("1")) {//查询质检评分阶段选中的评分项目
				param.put("isReview", 0);
			} else if(param.get("orderNum").equals("2")) {//查询质检复核阶段的评分项目
				param.put("isReview", 1);
			} else {
				param.put("isReview", -1);
			}
			sql.append("select t3.item_name scoreItemName, t2.score_item_id scoreItemId, t3.score, t2.order_no orderNo,t3.score_type scoreType from tbl_qm_score_record t1 left join ");
			sql.append("tbl_qm_score_item t2 on t1.item_id = t2.score_item_id ");
			sql.append("left join tbl_qm_score_list t3 on t2.score_list_id = t3.score_list_id ");
			sql.append("where t1.paper_id = :paperId and is_review = :isReview");
		}
		List<ScoreItem> list = configedJdbc.queryForList(sql.toString(), param, ScoreItem.class);
		
		return list;
	}
	
	/**
	 * 获取典型案例
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public Case getPaperCase(Map<String, Object> param) {
		if(StringUtils.isEmpty(param.get("paperId"))) {
			return null;
		}
		StringBuilder sql = new StringBuilder();
		sql.append("select t1.case_id caseId, t1.case_item caseItem, t1.record_type recordType from tbl_qm_paper t2 ");
		sql.append("left join tbl_qm_case t1 on t1.contact_id = t2.paper_detail_id ");
		sql.append("where t2.paper_id = :paperId ");
	
		List<Case> list = configedJdbc.queryForList(sql.toString(), param, Case.class);
		
		if(list == null || list.isEmpty()) {
			return null;
		} else {
			return list.get(0);
		}
	}
	
	/**
	 * 获取质检成绩信息
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public PaperScoreResult getPaperScoreResult(Map<String, Object> param) {
		if(StringUtils.isEmpty(param.get("paperId"))) {
			return null;
		}
		
		PaperData paperData = paperDao.get(param.get("paperId").toString());
		if(paperData == null) {
			return null;
		}
		String qualityStatus = paperData.getQualityStatus();
		
		StringBuilder sql = new StringBuilder();
		sql.append("select t1.paper_id paperId,t1.user_code agentCode, t1.amount, t1.total_score totalScore, t1.review_score reviewScore, t1.comments, ");
		sql.append("t1.quality_status qualityStatus,t1.service_type serviceType,t1.error_type errorType, t2.modify_time scoreTime, t2.modifier scorerCode, ");
		sql.append("(select user_name from tbl_sys_user where user_code = t2.modifier) scorerName ");
		if(qualityStatus.equals("3") || qualityStatus.equals("4")) {
			sql.append(",t3.modify_time reviewTime, t3.modifier checkerCode, ");
			sql.append("(select user_name from tbl_sys_user where user_code = t3.modifier) checkerName ");
		}
		sql.append("from tbl_qm_paper t1 left join tbl_qm_paper_log t2 on t1.paper_id = t2.paper_id ");
		if(qualityStatus.equals("3") || qualityStatus.equals("4")) {
			sql.append("left join tbl_qm_paper_log t3 ");
			sql.append("on t1.paper_id = t3.paper_id ");
		}
		sql.append("where t1.paper_id = :paperId ");
		sql.append("and t2.quality_status = '1' ");
		if(qualityStatus.equals("3")) {
			sql.append("and t3.quality_status = '3'");
		} else if(qualityStatus.equals("4")) {
			sql.append("and t3.quality_status = '4'");
		}
		List<PaperScoreResult> list = configedJdbc.queryForList(sql.toString(), param, PaperScoreResult.class);
		if(list == null || list.isEmpty()) {
			return null;
		} else {
			return list.get(0);
		}
	}

	/**
	 * 质检打分
	 * @param param
	 * @throws
	 * @author wangyf
	 */
	@SuppressWarnings("deprecation")
	public boolean scorePaper(Map<String, Object> param) {
		//获取当前时间，当前操作人账号和工号
		String currentTime = DateUtils.getCurrentDateTimeAsString();
		String userCode = ((SysUser)SecurityUtils.getSubject().getPrincipal()).getUserCode();
		//String userNo = ((SysUser)SecurityUtils.getSubject().getPrincipal()).getUserNo(); 

		param.put("createTime", currentTime);//创建时间
		param.put("modifier", userCode);//修改人账号
		param.put("modifyTime", currentTime);//修改时间
		param.put("createCode", userCode);//创建人账号
		
		//根据质检单ID查询质检单
		PaperData paperData = paperDao.get(param.get("paperId").toString());
		//避免重复提交
		if(StringUtils.isNotEmpty(param.get("totalScore"))) {//评分阶段
			if(!paperData.getQualityStatus().equals("0")) {
				return false;
			}
		}
		
		if(StringUtils.isNotEmpty(param.get("reviewScore"))) {//复核阶段
			if(!paperData.getQualityStatus().equals("2")) {
				return false;
			}
		}
		String paper_createTime = paperData.getCreateTime();
		HollysqmUtil.fillParam(param, PaperData.class, paperData);
		
		if(param.get("isPublicResult")!= null && param.get("isPublicResult").toString().equals("1")) {
			//发布成绩需要记录成绩发布时间
			paperData.setReleaseTime(currentTime);
		}
		
		//重设同名树形
		paperData.setCreateTime(paper_createTime);
		
		//评分阶段复核成绩置空
		if(paperData.getQualityStatus().equals("1")) {
			paperData.setReviewScore(null);
		}
		paperDao.saveOrUpdate(paperData);
		
		int passScore = Integer.parseInt(dictionaryCacheHandler.getDictionaryValue("NOTICE_PASS_SCORE", "平均"));
		
		//根据评分发送业务通知  是发布成绩
		if(paperData.getQualityStatus().equals("1") && paperData.getTotalScore() < passScore && "1".equals(paperData.getIsPublicResult())) {
			String title = sysConfigCacheHandler.getSysConfigValue("ScoreNotice", "TITLE");//业务通知标题
			String content = sysConfigCacheHandler.getSysConfigValue("ScoreNotice", "CONTENT");//业务通知内容
			String publishTime = DateUtils.getCurrentDateAsString(DateUtils.FORMAT_DATE_YYYY_MM_DD_HH_MM_SS); 
			String expiryTime = DateUtils.getCurrentDateAsString() + " 23:59:59"; 
			String planname = "";
			if(StringUtils.isNotEmpty(paperData.getPlanId())) {
				param.put("planId", paperData.getPlanId());
				planname = configedJdbc.queryForMap("select plan_name planName from tbl_qm_plan where plan_id = :planId", param).get("planName").toString();
			} 
			
			SysUser sysUser = commonNoticeService.getUserId(paperData.getCheckerCode());
			String checker = sysUser.getUserName() + "(" + sysUser.getUserCode() + ")";
			String totalScore = String.valueOf(paperData.getTotalScore());
			String checkTime = paperData.getModifyTime();
			content = content.replaceAll("\\$\\{planname\\}", planname).replaceAll("\\$\\{checker\\}", checker).replaceAll("\\$\\{totalScore\\}", totalScore).replaceAll("\\$\\{checkTime\\}", checkTime);
			
			commonNoticeService.sendNotice(paperData.getUserCode(), paperData.getCheckerCode(), "1", NoticeInfo.NOTICE_EMERGENCY_NORMAL, title, content, publishTime, NoticeInfo.NOTICE_SEND_TYPE_IMMEDIATELY, NoticeInfo.NOTICE_ATTENTION_TYPE_SCROLL+","+NoticeInfo.NOTICE_ATTENTION_TYPE_POPUP, NoticeInfo.NOTICE_STATUS_PUBLISHED, expiryTime, paperData.getQualityStatus(), NoticeUser.NOTICE_STATUS_UNREAD);
		}
		
		//记录本次评分选中的打分项
		if(StringUtils.isNotEmpty(param.get("itemId"))) {
			String[] items = param.get("itemId").toString().split(",");
			if(items.length > 0) {
				for(int i=0; i<items.length; i++) {
					param.put("itemId",items[i]);
					ScoreRecordData scoreRecordData = new ScoreRecordData();
					HollysqmUtil.fillParam(param, ScoreRecordData.class, scoreRecordData);
					scoreRecordDao.save(scoreRecordData);	
				}
			}
		}
		
		if(param.get("qualityStatus").toString().equals("3")) {
			//复核阶段先删除与当前接触记录已绑定的典型案例
			StringBuilder sql = new StringBuilder();
			sql.append("delete from tbl_qm_case where contact_id = :contactId");
			configedJdbc.updateBySql(sql.toString(), param);
			sql.delete(0, sql.length());
		} 
		//记录本次评分的典型案例信息
		if(param.get("caseItem") != null && !param.get("caseItem").toString().equals("")) {
			Case c = new Case();
			HollysqmUtil.fillParam(param, Case.class, c);
			caseDao.save(c);
		}
		
		//处理申诉内容
		if(param.get("appealId") != null) {
			PaperAppeall appeal = paperDao.get(param.get("appealId").toString(),PaperAppeall.class);
			HollysqmUtil.fillParam(param, PaperAppeall.class, appeal);
			
			if(paperData.getQualityStatus().equals("3")) {
				appeal.setStatus("1");
			} else if(paperData.getQualityStatus().equals("4")) {
				appeal.setStatus("2");
			}
			paperDao.save(appeal);
		}
		
		//更新solr上的质检状态数据
		/*String tag = paperData.getPaperType();
		IndexSearchService indexSearchService = indexSearchFactory.getNewIndexService(tag);//根据V8或I8标识返回索引对象
		
		if(paperData.getQualityStatus().equals("1")) {
			try{
				if (tag.equalsIgnoreCase(Constant.V8)){//切换成V8
					V8DocBean v8bean = indexSearchService.getById(paperData.getContactId(), V8DocBean.class);
					v8bean.setQualityStatus(paperData.getQualityStatus());
					indexSearchService.updateDoc(v8bean);
							
				}else if (tag.equalsIgnoreCase(Constant.I8)){//切换成I8
					I8DocBean i8bean= indexSearchService.getById(paperData.getContactId(), I8DocBean.class);
					i8bean.setQualityStatus(paperData.getQualityStatus());
					indexSearchService.updateDoc(i8bean);
					
				}			
			}catch(Exception e){
				e.printStackTrace();
			}	
		}*/
		
		//评分时对符合要求的分数
		int lowScore = Integer.parseInt(dictionaryCacheHandler.getDictionaryValue("CHECK_PASS_SCORE", "不及格"));
		int highScore = Integer.parseInt(dictionaryCacheHandler.getDictionaryValue("CHECK_PASS_SCORE", "优秀"));
		if(paperData.getQualityStatus().equals("1") && (paperData.getTotalScore() < lowScore || paperData.getTotalScore() > highScore)) {
			PaperCheckData p = new PaperCheckData();
			HollysqmUtil.fillParam(param, PaperCheckData.class, p);
			p.setCheckStatus("0");
			judgeCheckerDao.save(p);
		}
		//添加质检单操作历史
		this.savePaperLog(param.get("paperId").toString(), param.get("qualityStatus").toString(), param.get("modifier").toString(), param.get("modifyTime").toString());
		return true;
	}
	
	/**
	 * 质检打分
	 * @param param
	 * @throws
	 * @author wangyf
	 */
	@SuppressWarnings("deprecation")
	public boolean editPaper(Map<String, Object> param) {
		//根据质检单ID查询质检单
		String modifyTime = DateUtils.getCurrentDateTimeAsString();
		param.put("modifyTime", modifyTime);
		PaperData paperData = paperDao.get(param.get("paperId").toString());
		HollysqmUtil.fillParam(param, PaperData.class, paperData);
		paperData.setReviewScore(null);
		paperDao.saveOrUpdate(paperData);
		String createrCode = "";
		
		//删除上次评分记录
		List<ScoreRecordData> list =  scoreRecordDao.findList("from ScoreRecordData where paperId = :paperId", param);
		for(ScoreRecordData scoreRecord : list) {
			if(StringUtils.isEmpty(createrCode)) {
				createrCode = scoreRecord.getCreateCode();
			}
			scoreRecordDao.delete(scoreRecord);
		}
		//记录本次评分选中的打分项
		if(StringUtils.isNotEmpty(param.get("itemId"))) {
			String[] items = param.get("itemId").toString().split(",");
			if(items.length > 0) {
				for(int i=0; i<items.length; i++) {
					param.put("itemId",items[i]);
					ScoreRecordData scoreRecordData = new ScoreRecordData();
					HollysqmUtil.fillParam(param, ScoreRecordData.class, scoreRecordData);
					scoreRecordData.setIsReview("0");
					scoreRecordData.setCreateTime(modifyTime);
					scoreRecordData.setCreateCode(createrCode);
					scoreRecordDao.save(scoreRecordData);	
				}
			}
		}
		
		if(StringUtils.isNotEmpty(param.get("caseItem"))) {
			//复核阶段先删除与当前接触记录已绑定的典型案例
			StringBuilder sql = new StringBuilder();
			sql.append("delete from tbl_qm_case where contact_id = :contactId");
			configedJdbc.updateBySql(sql.toString(), param);
			sql.delete(0, sql.length());
			
			Case c = new Case();
			HollysqmUtil.fillParam(param, Case.class, c);
			caseDao.save(c);
		} 
		
		//重设操作日志的修改时间
		configedJdbc.updateBySql("update tbl_qm_paper_log set modify_time = :modifyTime where paper_id = :paperId and quality_status = 1", param);
		
		return true;
	}

	/**
	 * 接触记录绑定评分模板生成质检单
	 * @param param
	 * @throws
	 * @author wangyf
	 */
	public PaperData createPaper(Map<String, Object> param) {
		if(StringUtils.isEmpty(param.get("paperType"))) {
			return null;
		}
		//获取当前时间，当前用户账号和工号
		String currentTime = DateUtils.getCurrentDateTimeAsString();
		String id = "";
		String checkerCode = ((SysUser)SecurityUtils.getSubject().getPrincipal()).getUserCode();
		String checkerAgentCode = ((SysUser)SecurityUtils.getSubject().getPrincipal()).getUserNo(); 
		String domainId = WebUtil.getLoginUser().getDomainId();//租户ID
		String areaId = WebUtil.getLoginUser().getOrgId();//区域ID
		
		//生成质检单ID
		id = UuidUtil.getUuid();
		param.put("paperId", id);
		param.put("checkerCode", checkerCode);
		param.put("checkerAgentCode", checkerAgentCode);
		param.put("qualityStatus", "0");
		param.put("createTime", currentTime);
		param.put("modifier", checkerCode);
		param.put("modifyTime", currentTime);
		param.put("createCode", checkerCode);
		param.put("areaId", areaId);
		param.put("domainId", domainId);
		param.put("planId", "system");
		//查询当前模板的总分
		if(StringUtils.isNotEmpty(param.get("standardId"))) {
			int amount = standardmanagerDao.get(param.get("standardId").toString()).getTotalScore();
			param.put("amount", amount);
		}
		
		if(StringUtils.isNotEmpty(param.get("agentCode"))) {
			String userCode = userDao.getUserCodeOrNo(param.get("agentCode").toString(), "userCode");
			param.put("userCode", userCode);
		}
		PaperData paperData = new PaperData();
		HollysqmUtil.fillParam(param, PaperData.class, paperData);
		
		//质控评分成绩和复核成绩 
		paperData.setTotalScore(null);
		paperData.setReviewScore(null);
		//保存
		paperDao.save(paperData);
		
		//添加质检单操作历史
		this.savePaperLog(param.get("paperId").toString(), param.get("qualityStatus").toString(), param.get("modifier").toString(), param.get("modifyTime").toString());
		
		return paperData;
	}

	
	/** 
	* @Description:发起申诉
	* @param:@param param    设定文件 
	* @return:void    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public void appealPaper(Map<String,Object> param){
		String userCode = WebUtil.getLoginUser().getUserCode();
		String currentTime = DateUtils.getCurrentDateTimeAsString();
	
		String groupLeader = MapUtils.getString(param,"auditorCode");
		PaperAppeall pa = new PaperAppeall();
		pa.setPaperId(MapUtils.getString(param, "paperId"));//质检单ID
		pa.setExecuteCode(userCode);//申诉人帐号
		pa.setExplain(MapUtils.getString(param, "appealReason"));//申诉理由
		pa.setCreateTime(currentTime);//申诉时间
		pa.setStatus("0");//已申诉
		//pa.setJobType("1");//坐席
		pa.setExecuteime(currentTime);
		pa.setIsValid("1");
		
		//下一流程
		PaperAppeall pa1 = new PaperAppeall();
		pa1.setPaperId(pa.getPaperId());
		pa1.setExecuteCode(groupLeader);//申诉人帐号
		pa1.setCreateTime(currentTime);
		pa1.setStatus("3");//待处理
		//pa1.setJobType("2");//组长
		pa1.setIsValid("1");
		System.out.println(JSONObject.fromObject(pa));
		paperDao.save(pa);
		System.out.println(JSONObject.fromObject(pa1));
		paperDao.save(pa1);
		
		String updateSql = "update tbl_qm_paper t set t.QUALITY_STATUS = ?,t.modifier = ?,t.modify_time = ?,t.LAST_APPEAL_ID = ? where t.paper_id = ?";
		configedJdbc.getJdbcTemplate().update(updateSql, "2",userCode, currentTime ,pa1.getAppealId(),MapUtils.getString(param, "paperId"));//2已申诉
		
		savePaperLog(pa.getPaperId(), "2", userCode, currentTime);
	}
	
	/** 
	* @Description:审核坐席申诉
	* @param:@param param    设定文件 
	* @return:void    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	 * @throws Exception 
	*/
	public void doPaperAppeal(Map<String,Object> param) throws Exception{
		String appealId = param.get("appealId").toString();
		PaperAppeall pa = paperDao.get(appealId,PaperAppeall.class);
		String actionType = MapUtils.getString(param, "qualityStatus");
		String appealUserCode = WebUtil.getLoginUser().getUserCode();
		String currentTime = DateUtils.getCurrentDateTimeAsString();
		//String status = MapUtils.getString(param,"status");//1 通过,2 驳回
		param.put("appealId", appealId);
		param.put("appealUserCode", appealUserCode);
		param.put("currentTime", currentTime);
		if("5".equals(actionType)){//流转下一环节
			String classLeader = MapUtils.getString(param,"auditorCode");
			
			pa.setExecuteime(currentTime);
			pa.setExplain(MapUtils.getString(param, "answer"));
			pa.setStatus("1");
			//下一流程
			PaperAppeall pa1 = new PaperAppeall();
			pa1.setPaperId(pa.getPaperId());
			pa1.setExecuteCode(classLeader);//申诉人帐号
			pa1.setCreateTime(currentTime);
			pa1.setStatus("3");//待处理
			pa1.setIsValid("1");
			
			paperDao.update(pa);
			paperDao.save(pa1);
			
			String updateSql = "update tbl_qm_paper t set t.LAST_APPEAL_ID = ? where t.paper_id = ?";
			configedJdbc.getJdbcTemplate().update(updateSql, pa1.getAppealId(),pa1.getPaperId());//2已申诉
			savePaperLog(pa.getPaperId(), "2", appealUserCode, currentTime);
		}else{//复核、驳回结束当前环节
			endPaperAppeal(param);
			if("3".equals(actionType)){
				StringBuffer updateSql =  new StringBuffer();
				updateSql.append("update tbl_qm_paper t ");
				if(StringUtils.isNotEmpty(param.get("errorType"))){
					updateSql.append(" set t.error_type = ").append(param.get("errorType"));
				}
				if(StringUtils.isNotEmpty(param.get("serviceType"))){
					updateSql.append(" , t.service_type = ").append(param.get("serviceType"));
				}
				updateSql.append(" where t.paper_id = ?");
				System.out.println(updateSql.toString());
				configedJdbc.getJdbcTemplate().update(updateSql.toString(),pa.getPaperId());//2
			}
		}
		
	}
	
	/** 
	* @Description:结束申诉
	* @param:@param param    设定文件 
	* @return:void    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年6月30日
	 * @throws Exception 
	*/
	public void endPaperAppeal(Map<String,Object> param) throws Exception{
		String appealUserCode = MapUtils.getString(param, "appealUserCode");
		String currentTime = MapUtils.getString(param, "currentTime");
		String appealId = MapUtils.getString(param, "appealId");
		String paperId = MapUtils.getString(param, "paperId");
		String qualityStatus = MapUtils.getString(param,"qualityStatus");
		String status = "3".equals(qualityStatus)?"1":"2";
		PaperAppeall appeal = paperDao.get(appealId,PaperAppeall.class);
		appeal.setExplain(MapUtils.getString(param, "answer"));//回复内容
		appeal.setStatus(status);//1：已通过，2：已驳回
		appeal.setExecuteime(currentTime);//创建时间
		
		param.put("comments", appeal.getExplain());
		if("1".equals(status)){//审核评分
			if(!scorePaper(param)){
				throw new Exception("操作失败");
			}
		}
		PaperData  paper = paperDao.get(paperId);
		paper.setQualityStatus(qualityStatus);//3已通过，4已驳回
		paper.setModifier(appealUserCode);
		paper.setModifyTime(currentTime);
		if("1".equals(status)){
			paper.setReviewScore(MapUtils.getInteger(param, "reviewScore"));//复核得分
		}
		
		paperDao.update(appeal);
		paperDao.update(paper);
		
		savePaperLog(paperId, paper.getQualityStatus(), appealUserCode, currentTime);
	}
	
	
	/** 
	* @Description:查询申诉记录
	* @param:@param param
	* @param:@return    设定文件 
	* @return:PaperAppeal    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	@SuppressWarnings("unchecked")
	public List<PaperAppeall> queryPaperAppeal(Map<String,Object> param){
		List<PaperAppeall> list = paperDao.findListToObject(" from PaperAppeall where paperId = :paperId order by createTime,executeime", param);
	    return list;
	}

	
	/**
	 * 查询接触记录信息
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public Record getRecord(Map<String, Object> param) {
		String contactId  = "";//接触记录ID
		String tag = "";//数据来源标签
		String planId = "";
		String planName = "";
		if(StringUtils.isNotEmpty(param.get("paperId"))) {//质检单ID
			PaperData paperData = paperDao.get(param.get("paperId").toString());
			if(paperData == null) {
				return null;
			}
			
			contactId = paperData.getContactId();
			tag = paperData.getPaperType();
			planId = paperData.getPlanId();
			param.put("planId", planId);
			List<Map<String,Object>> list =configedJdbc.queryForList("select plan_name planName from tbl_qm_plan where plan_id = :planId", param);
			if(!list.isEmpty())
				planName = list.get(0).get("planName").toString();
		} else {
			if(StringUtils.isEmpty(param.get("contactId")) || StringUtils.isEmpty(param.get("dataType"))) {
				return null;
			}
			contactId = param.get("contactId").toString();
			tag = param.get("dataType").toString();
		}

		IndexSearchService indexSearchService = indexSearchFactory.getNewIndexService(tag);//根据V8或I8标识返回索引对象
		Record record = new Record();
		record.setDataType(tag);
		String userCode = "";
		try{
			if (tag.equalsIgnoreCase(Constant.V8)){//切换成V8
				V8DocBean v8bean = indexSearchService.getById(contactId, V8DocBean.class);
				HollysqmUtil.parseV8DocBeanToRecord(v8bean, record);
				userCode = v8bean.getUserCode();
			}else if (tag.equalsIgnoreCase(Constant.I8)){//切换成I8
				I8DocBean i8bean = indexSearchService.getById(contactId, I8DocBean.class);
				HollysqmUtil.parseI8DocBeanToRecord(i8bean, record);
				userCode = i8bean.getAgentCode();
			}
		}catch(Exception e){
			e.printStackTrace();
		}	

		User user = userDao.getUserByCode(userCode);
		record.setUser(user);
		record.setPlanId(planId);
		record.setPlanName(planName);
		return record;
	}
	
	
	

	/** 
	* @Description:获取评分模板（查询页面上的下拉框）
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年4月19日
	*/
	public List<LabelValue> queryStandardList(Map<String,Object> map){
		String sql = "select t.standard_id value,t.standard_name name from TBL_QM_STANDARD t";
		if(!StringUtils.isEmpty(map.get("status")))
			sql += " where t.status = :status ";
		sql += " order by t.modify_time desc ";
		List<LabelValue> list = configedJdbc.queryForList(sql, map, LabelValue.class);
		return list;
	}
	/** 
	* @Description:获取所有标签项
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年4月19日
	*/
	public List<TextItem> queryTextItem(){
		String hql = "from TextItem t where t.status = ? order by t.modifyTime desc";
		List<TextItem> list = paperDao.findList(hql, TextItem.class, "1");
		return list;
	}
	/** 
	* @Description:获取质检计划下拉数据
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年4月19日
	*/
	public List<LabelValue> queryPlan(){
		String sql = "select t.plan_id value,t.plan_name name from tbl_qm_plan t where t.status = 1";
		List<LabelValue> list = configedJdbc.queryForList(sql, new HashMap<String,Object>(), LabelValue.class);
		return list;
	}

	/**
	 * 下载文件并获取路径
	 * @param record
	 * @param path
	 * @return
	 */
	public RecordDir downloadRecord(Record record, String path) {
		String contactId = record.getContactId();
		String timeDir = record.getAcceptTime().substring(0, 10);
		String recordFile = record.getRecordFile();
		path = path.replace("\\", "/");
		String saveDir = path + PropertiesUtils.getProperty("saveDir");
		File file = new File(saveDir);
		if(!file.exists()) {
			file.mkdir();
		}
			
		FtpConfigurBean xmlFtpConfigurBean = null;
		FtpConfigurBean vocFtpConfigurBean = null;
		
//		if((Integer.parseInt(timeDir)%2) == 0) {//双数日期
//			xmlFtpConfigurBean = ftpSourceConfig.getXmlFtpSourceBean().getFtpConfigurList().get(1);
//		} else {
			xmlFtpConfigurBean = ftpSourceConfig.getXmlFtpSourceBean().getFtpConfigurList().get(0);
//		}
		
		vocFtpConfigurBean = ftpSourceConfig.getVocFtpSourceBean().getFtpConfigurList().get(0);
		HollysqmUtil.ftpDownload(timeDir, saveDir, contactId, xmlFtpConfigurBean, vocFtpConfigurBean, recordFile);
		
		RecordDir recordDir = new RecordDir();
		recordDir.setImgDir("record/" + contactId + ".jpg");
		//recordDir.setImgDir("record/2ECBED29B2AF453B9A51CE43DC63A323.jpg");
		//recordDir.setWavDir("record/2ECBED29B2AF453B9A51CE43DC63A323.mp3");
		recordDir.setWavDir("record/" + contactId + ".mp3");
		return recordDir;
	}
	
	/**
	 * 下载文件并获取路径
	 * @param record
	 * @param path
	 * @return
	 */
	public RecordDir downloadRecordTest(Record record, String path) {
		String contactId = record.getContactId();
		String timeDir = record.getAcceptTime().substring(0, 10);
		String recordFile = record.getRecordFile();
		path = path.replace("\\", "/");
		String saveDir = path + PropertiesUtils.getProperty("saveDir");
		File file = new File(saveDir);
		if(!file.exists()) {
			file.mkdir();
		}
			
		FtpConfigurBean xmlFtpConfigurBean = ftpSourceConfig.getXmlFtpSourceBean().getFtpConfigurList().get(0);
		FtpConfigurBean vocFtpConfigurBean = ftpSourceConfig.getVocFtpSourceBean().getFtpConfigurList().get(0);
		

		HollysqmUtil.ftpDownloadTest(timeDir, saveDir, contactId, xmlFtpConfigurBean, vocFtpConfigurBean, recordFile);
		
		RecordDir recordDir = new RecordDir();
		recordDir.setImgDir("record/2ECBED29B2AF453B9A51CE43DC63A323.jpg");
		recordDir.setWavDir("record/2ECBED29B2AF453B9A51CE43DC63A323.mp3");
		return recordDir;
	}
	
	/**
	 * 标签预览
	 * @param param
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public PageResult<Record> itemPreview(Map<String, Object> param){
		IndexSearchService indexSearchService = indexSearchFactory.getNewIndexService(Constant.V8);
		PageResult<Record> page = (PageResult<Record>) HollysqmUtil.createPageResult(param);
		String qs = "*:*";
		String content = (String) param.get("content");
		String itemType = (String) param.get("itemType");
		if (StringUtils.isNotEmpty(content)){
			String textType = Constant.allContent;
			if ("1".equals(itemType)){
				textType = Constant.txtContentAgent;
			} else if ("2".equals(itemType)){
				textType = Constant.txtContentUser;
			}
			qs = String.format("%s:(%s)", textType,content);	
		} else {
			page.setTotal(0);
			page.setRows(new ArrayList<Record>());
			return page;
		}
		Map<String,String> paramMap =  new HashMap<String,String>();
		//默认在做标签预览时，只查询当前时间往前推90天的数据，也就一个季度的数据量，否则超大数据量会有性能问题
		Date endDayDate = new Date();	
		Date startDayDate = org.apache.commons.lang3.time.DateUtils.addDays(endDayDate, -90);
		String startDay = DateFormatUtils.format(startDayDate, "yyyyMMdd");
		String endDay = DateFormatUtils.format(new Date(), "yyyyMMdd");
		paramMap.put(Constant.day, Constant.getAssemble(startDay, endDay));
		Record record = null;
		List<Record> records = new ArrayList<Record>();
		int count = 0;// 查询记录数	
		try{
			count = indexSearchService.count(qs, paramMap);
			page.setTotal(count);
			if (count > 0){
				List<V8DocBean> dataList = indexSearchService.queryResult(qs, paramMap, (int)page.getPageNo(), 
						(int)page.getPageSize(), false, V8DocBean.class);
				User user = null;
				for (V8DocBean bean : dataList){
					record = new Record();
					user = userDao.getUserByCode(bean.getUserCode());;
					HollysqmUtil.parseV8DocBeanToRecord(bean, record);
					if(user == null){
					    user = new User();
						user.setAgentCode(bean.getUserCode());
					}
					record.setUser(user);
					records.add(record);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		page.setRows(records);
		return page;
	}
	
	
	/**
	 * 记录质检单执行日志
	 * @param paperId
	 * @param qualityStatus
	 * @param modifier
	 * @param modifyTime
	 */
	@SuppressWarnings("deprecation")
	public void savePaperLog(String paperId, String qualityStatus, String modifier, String modifyTime) {
		HashMap<String,Object> param = new HashMap<String,Object>();
		String paperLogId = UuidUtil.getUuid();
		param.put("paperLogId", paperLogId);
		param.put("paperId", paperId);
		param.put("qualityStatus", qualityStatus);
		param.put("modifier", modifier);
		param.put("modifyTime", modifyTime);
		
		String sql = "insert into tbl_qm_paper_log values(:paperLogId, :paperId, :qualityStatus, :modifyTime, :modifier)";
		configedJdbc.updateBySql(sql, param);
		
	}
	
	/**
	 * 查询接触记录并高亮显示
	 * @param param
	 * @return
	 */
	public Record getHighlightRecord(Map<String, Object> param) {
		IndexSearchService indexSearchService = null;
		
		if(StringUtils.isEmpty(param.get("dataType")) || StringUtils.isEmpty(param.get("contactId"))) {
			return null;
		}
		String tag = param.get("dataType").toString();
		String contactId = param.get("contactId").toString();
		indexSearchService = indexSearchFactory.getNewIndexService(tag);
		String content = "*";
		String wordType = "";
		//如果绑定了标签，就用标签的内容替代文本
		if(StringUtils.isNotEmpty(param.get("textItemId"))) {
			TextItem textItem = itemmanagerService.getItem(param.get("textItemId").toString());
			content = textItem.getContent();
			wordType = textItem.getItemType();
		}else if (StringUtils.isNotEmpty(MapUtils.getString(param, "words"))){
			content = MapUtils.getString(param, "words").trim();
			wordType = MapUtils.getString(param, "wordType");
		}
		String textType = Constant.allContent;
		if ("1".equals(wordType)){
			textType = Constant.txtContentAgent;
		} else if ("2".equals(wordType)){
			textType = Constant.txtContentUser;
		}
		
		String qs = String.format("%s:(%s)", textType,content);
		Map<String,String> paramMap =  new HashMap<String,String>();
		paramMap.put(Constant.id, contactId);
		
		List<?> dataList = null;
		Record record = new Record();
		record.setDataType(tag);
		String userCode = "";
		boolean isHighlight = true;
		if(content.equals("*")) {
			isHighlight = false;
		}
		try{
			if(tag.equals(Constant.V8)) {
				dataList =  indexSearchService.queryResult(qs, paramMap,1, 
						1, isHighlight, V8DocBean.class);
				V8DocBean v8bean = (V8DocBean)dataList.get(0);
				HollysqmUtil.parseV8DocBeanToRecord(v8bean, record);
				userCode = v8bean.getUserCode();
				
			} else if(tag.equals(Constant.I8)) {
				dataList =  indexSearchService.queryResult(qs, paramMap,1, 
						1, isHighlight, I8DocBean.class);
				I8DocBean i8bean = (I8DocBean)dataList.get(0);
				HollysqmUtil.parseI8DocBeanToRecord(i8bean, record);
				userCode = i8bean.getUserCode();
			}
			User user = userDao.getUserByCode(userCode);
			record.setUser(user);
			
			return record;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	/**
	 * 通过标签关键词高亮预览文本内容
	 * @param param
	 */
	public String highlightPreview(Map<String, Object> param){
		IndexSearchService indexSearchService = indexSearchFactory.getNewIndexService(Constant.V8);
		String id = (String) param.get("id");
		String content = (String) param.get("content");
		String itemType = (String) param.get("itemType");
		if (StringUtils.isEmpty(content)){
			return null;
		}
		String textType = Constant.allContent;
		if ("1".equals(itemType)){
			textType = Constant.txtContentAgent;
		} else if ("2".equals(itemType)){
			textType = Constant.txtContentUser;
		}
		String qs = String.format("%s:(%s)", textType,content);
		Map<String,String> paramMap =  new HashMap<String,String>();
		paramMap.put(Constant.id, id);
		try{
			indexSearchService.setHighlightFragsize(10000);
			//通过ID查询，ID唯一，只需要获取一条即可		
			List<V8DocBean> dataList = indexSearchService.queryResult(qs, paramMap,1, 
					1, true, V8DocBean.class);
			if (dataList!=null && dataList.size()>0){
				return dataList.get(0).getTxtContent();
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	

	public List<ServiceType> getServiceType(Map<String, Object> param) {
		StringBuilder sql = new StringBuilder();
		sql.append("select service_type_id id, parent_service_type_id parentId, service_name text from tbl_service_type where is_valid = 1 ");
		List<ServiceType> list = configedJdbc.queryForList(sql.toString(), param, ServiceType.class);
		return list;
	}
	
	/**
	 * 获取服务类型全路径名
	 * @param param
	 * @return
	 */
	public List<ServiceType> getServiceTypeNames(Map<String, Object> param) {
		if(StringUtils.isEmpty(param.get("serviceTypeId"))) {
			return null;
		}
		
		String serviceTypeId = HollysqmUtil.paramParseQueryText(param.get("serviceTypeId").toString(), "sql", true);
		StringBuilder sql = new StringBuilder();
		sql.append("select service_type_id id, full_name text from tbl_service_type where service_type_id in ");
		sql.append(serviceTypeId);
		
		List<ServiceType> list = configedJdbc.queryForList(sql.toString(), param, ServiceType.class);
		return list;
	}

	/** 
	* @Description:设置领导查看权限
	* @param:@param param    设定文件 
	* @return:void    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年7月27日
	*/
	public void setParam(Map<String,Object> param){
		String sql = "select distinct o.org_id orgId,o.sort_no from tbl_sys_org o,tbl_sys_general_code g where"
				+ "  g.code_type = ? "
				+ "and trim(g.code_name) = o.org_name "
				+ "and trim(g.code_value) = ? "
				+ "and g.enabled = 1 order by o.sort_no";
		String userNo = WebUtil.getLoginUser().getUserNo();
		//GROUP_LEADER
		JdbcTemplate jdbc = configedJdbc.getJdbcTemplate();
		List<Map<String,Object>> list;
		Object deptId = "";
		list = jdbc.queryForList(sql,"CLASS_LEADER",userNo);
		if(list.isEmpty()){//判断当前登陆人是否班长角色
			list = jdbc.queryForList(sql, "GROUP_LEADER",userNo);
		}else{
			param.put("agentCode", "");//如果为班长则查询所有坐席
			list.removeAll(list);//删除列表
		}
		StringBuffer sb = new StringBuffer();
		for(Map<String,Object> map : list){
			sb.append("'").append(map.get("orgId")).append("',");
		}
		
		if(sb.length() > 0)
			deptId = sb.substring(0,sb.length()-1);
		
		param.put("deptId", deptId);
	}
}
