package com.hollycrm.hollysqm.business.paperscore.rest;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollysqm.business.paperscore.service.PaperScoreService;
import com.hollycrm.hollysqm.entities.DataScore;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * @author liujr
 * 2017年3月21日 
 * 质检成绩调用类
 */
@RestService
@RequestMapping(value="/paperscore")
public class PaperScoreRest {
	
	@Resource(name="paperScoreService")
	private PaperScoreService paperScoreService;
	
	/** 
	* @Description:质检成绩分页查询
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	@RequestMapping(value="/queryPaperscore",method = RequestMethod.GET)
	public ServiceResult queryPaperscore(@RequestParam Map<String, Object> param) {
		// 开始分页查询
		PageResult<DataScore> page = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			//每页大小
			String limit = (String) param.get("rows");
			if (StringUtils.isEmpty(limit))
				limit = com.hollycrm.hollybeacon.basic.Constants.PAGE_DEFAULT_LIMIT;
			// 开始页
			String start = (String) param.get("page");
			if (StringUtils.isEmpty(start))
				start = "1";
			
			page = new PageResult<DataScore>(Integer.valueOf(limit),Integer.valueOf(start),false);
			
			page = paperScoreService.queryPaperScore(param, page);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(page, flag, errorMessage);
	}
	
	/** 
	* @Description:优秀案例查询
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年5月2日
	*/
	@RequestMapping(value="/queryPaperCase",method = RequestMethod.GET)
	public ServiceResult queryPaperCase(@RequestParam Map<String, Object> param) {
		// 开始分页查询
		PageResult<DataScore> page = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			//每页大小
			String limit = (String) param.get("rows");
			if (StringUtils.isEmpty(limit))
				limit = com.hollycrm.hollybeacon.basic.Constants.PAGE_DEFAULT_LIMIT;
			// 开始页
			String start = (String) param.get("page");
			if (StringUtils.isEmpty(start))
				start = "1";
			
			page = new PageResult<DataScore>(Integer.valueOf(limit),Integer.valueOf(start),false);
			
			page = paperScoreService.queryPaperCase(param, page);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(page, flag, errorMessage);
	}
	/** 
	* @Description:发布成绩
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年5月10日
	*/
	@RequestMapping(value="/publishedScore",method = RequestMethod.POST)
	public ServiceResult publishedScore(@RequestParam Map<String, Object> param) {
		// 开始分页查询
		PageResult<DataScore> page = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			paperScoreService.publishedScore(param);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(page, flag, errorMessage);
	}
	
	/** 
	* @Description:删除典型案例
	* @param:@param caseId
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年5月15日
	*/
	@RequestMapping(value="/deleteCase",method = RequestMethod.POST)
	public ServiceResult deleteCase(@RequestParam String caseId) {
		
		boolean flag = false;
		String errorMessage = "";
		try {
			paperScoreService.deleteCase(caseId);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(null, flag, errorMessage);
	}

}
