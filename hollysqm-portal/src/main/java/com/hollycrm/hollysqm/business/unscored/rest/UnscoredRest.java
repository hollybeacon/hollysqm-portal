package com.hollycrm.hollysqm.business.unscored.rest;

import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollysqm.business.unscored.service.UnscoredService;
import com.hollycrm.hollysqm.entities.Paper;
import com.hollycrm.hollysqm.entities.UnsolvedPaper;
import com.hollycrm.hollysqm.util.HollysqmUtil;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 未评分质检单
 * @author wangyf
 *
 * 2017年3月15日
 */
@RestService
@RequestMapping(value="/unscoredRest")
public class UnscoredRest {

	@Resource(name="unscoredService")
	private UnscoredService unscoredService;

	/**
	 * 查找待评分的质检单列表
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	@RequestMapping(value="/getUnscoredPaperList",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getUnscoredPaperList(@RequestParam Map<String,Object> param) {
		PageResult<Paper> page = unscoredService.getUnscoredPaperList(param);
		
		return HollysqmUtil.wrapUpPageResult(page, true, "");
	}

	/**
	 * 获取未评分质检单信息
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	@RequestMapping(value="/getUnscoredPaperMsg",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getUnscoredPaperMsg(@RequestParam Map<String,Object> param) {
		UnsolvedPaper paperMsg = unscoredService.getUnscoredPaperMsg(param);
		
		return HollysqmUtil.wrapUpPageResult(paperMsg, true, "");
	}
}
