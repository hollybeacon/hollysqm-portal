package com.hollycrm.hollysqm.business.standardmanager.rest;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollysqm.business.standardmanager.service.StandardmanagerService;
import com.hollycrm.hollysqm.entities.Standard;
import com.hollycrm.hollysqm.util.HollysqmUtil;


/**
 * @author liujr
 * 2017年3月27日
 * @Description 评分模板管理
 */
@RestService
@RequestMapping(value="/standardmanager")
public class StandardManagerRest {

	@Resource(name = "standardManagerService")
	private StandardmanagerService  standardManagerService;
	
	/** 
	* @Description:添加或修改评分模板
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	@RequestMapping(value="/standardManager",method = RequestMethod.POST)
	public ServiceResult standardManager(@RequestParam Map<String, Object> param){
		boolean flag = false;
		String errorMessage = "";
		try {
			
			String success = standardManagerService.saveOrUpdate(param);
			if(success.equals("1")){
				errorMessage = "该模板名称已存在！请重新填写";
			}else
				flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(null, flag, errorMessage);
	}
	
	/** 
	* @Description:启用或是停用评分模板
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月27日
	*/
	@RequestMapping(value="/enable",method = RequestMethod.POST)
	public ServiceResult enable(@RequestParam Map<String, Object> param){
		boolean flag = false;
		String errorMessage = "";
		try {
			flag = standardManagerService.enable(param);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(null, flag, errorMessage);
	}
	
	/** 
	* @Description:评分模板管理查询
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月27日
	*/
	@RequestMapping(value="/queryStandard",method = RequestMethod.GET)
	public ServiceResult queryStandard(@RequestParam Map<String, Object> param){
		PageResult<Standard>  page = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			//每页大小
			String limit = (String) param.get("rows");
			if (StringUtils.isEmpty(limit))
				limit = com.hollycrm.hollybeacon.basic.Constants.PAGE_DEFAULT_LIMIT;
			// 开始页
			String start = (String) param.get("page");
			if (StringUtils.isEmpty(start))
				start = "1";
			page = new PageResult<>(Integer.valueOf(limit),Integer.valueOf(start),true);
			page = standardManagerService.queryStandard(page,param);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(page, flag, errorMessage);
	}
	
	/** 
	* @Description:查看评分模板
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月27日
	*/
	@RequestMapping(value="/checkStandard",method = RequestMethod.GET)
	public ServiceResult checkStandard(@RequestParam Map<String, Object> param){
		Standard standard = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			standard = standardManagerService.checkStandard(param);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(standard, flag, errorMessage);
	}

	/** 
	* @Description:获取所有评分项
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月28日
	*/
	@RequestMapping(value="/scoreAllList",method = RequestMethod.GET)
	public ServiceResult scoreAllList(@RequestParam Map<String, Object> param){
		List<?> list = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			list = standardManagerService.queryScoreAllList();
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(list, flag, errorMessage);
	}
	
}
