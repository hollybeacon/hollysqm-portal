package com.hollycrm.hollysqm.business.scorelist.dao;

import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.orm.dao.jpa.JpaBaseDaoImpl;
import com.hollycrm.hollysqm.entities.ScoreList;

@Service("scoreListDao")
public class ScoreListDao extends JpaBaseDaoImpl<ScoreList, String> {

}
