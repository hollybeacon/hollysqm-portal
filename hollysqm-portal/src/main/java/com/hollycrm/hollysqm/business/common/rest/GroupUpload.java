package com.hollycrm.hollysqm.business.common.rest;

import java.io.File;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.google.common.collect.Lists;
import com.hollycrm.hollybeacon.basic.core.annotations.ActionLog;
import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.csv.config.CsvConfiguration;
import com.hollycrm.hollybeacon.basic.csv.config.CsvTable;
import com.hollycrm.hollybeacon.basic.csv.config.CsvTable.CsvField;
import com.hollycrm.hollybeacon.basic.csv.impl.CsvImportImpl;
import com.hollycrm.hollybeacon.basic.csv.impl.ImportResult;
import com.hollycrm.hollybeacon.basic.csv.parser.AbstractParser;
import com.hollycrm.hollybeacon.basic.excle.ExcelImportImpl;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollybeacon.basic.util.WebUtils;

@RestService
@RequestMapping(value="/groupUpload")
public class GroupUpload {
	
	@Resource(name = "excleImport")
	protected ExcelImportImpl excleImport;
	
	@Autowired
	private ConfigedJdbc configedJdbc;
	
	@Resource(name = "csvConfig")
	protected CsvConfiguration csvConfig;
	
	@ActionLog(content = "CSV HSQL导入向导页", description = "CSV导入向导页")
	@RequestMapping(value = "/wizard/import")
	public void importWizard(@RequestParam String tableId,
			@RequestParam Map<String, Object> param,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		CsvTable csvTable = csvConfig.getCsvTable(tableId);
		Assert.notNull(csvTable, tableId + "对应的csvTable配置不存在");
		List<? extends CsvField> fields = csvConfig.getImportedCsvFields(tableId);
		request.setAttribute("fields", fields);
		request.getRequestDispatcher("/hollysqm/common/import/page/import.jsp").forward(request,response);
	}
	
	@ActionLog(content = "导入rest方法", description = "调用基类统一的导入列表方法")
	@RequestMapping(value = "/import", method = RequestMethod.POST)
	public ServiceResult baseImport(@RequestParam MultipartFile file,
			@RequestParam String tableId,HttpServletRequest request)
			throws Exception {
		ServiceResult serviceResult = new ServiceResult();
		JdbcTemplate jdbc = configedJdbc.getJdbcTemplate();
		// 1.保存到临时文件中
		InputStream in = file.getInputStream();
		File tempDir = WebUtils.getTempDir(request.getServletContext());
		File tmpFile = null;
		ImportResult result = null;
		CsvImportImpl.setRequest(request);
		//保存临时文件
		tmpFile = new File(tempDir, UUID.randomUUID().toString() + ".xls");
		FileUtils.copyInputStreamToFile(in, tmpFile);
		
		AbstractParser parser = excleImport.getParser();
		List<String[]> allrecords = parser.parseRows(tmpFile);//获取表格中数据
		
		boolean headerEnabled = csvConfig.getCsvTable(tableId).getHeaderEnabled();//判断是否有表头
		int totalCnt = headerEnabled ? allrecords.size() - 1: allrecords.size();
		int recordsize = 0;
		List<Object[]> lists = Lists.newArrayList();
		String sql1 = "select t.user_code from tbl_sys_user t where t.user_code = ?";//判断帐号是否存在
		String sql2 = "select t.org_id from tbl_sys_org t where t.org_name = ?";//判断组名是否存在
		int userCount = 0;
		String deptId = "";
		for(String[] record : allrecords){
			boolean flag = true;
			userCount = jdbc.queryForList(sql1, record[0]).size();
			List<Map<String,Object>> list = jdbc.queryForList(sql2, record[2]);
			deptId = list.isEmpty()?"":list.get(0).get("ORG_ID").toString();
			if(StringUtils.isEmpty(deptId)){
				flag = false;
			}
			if(userCount == 0){
				flag = false;
			}
			if(flag){
				Object[] objs = new Object[2];
				objs[0] = deptId;
				objs[1] = record[0];
				recordsize += 1;
				lists.add(objs);
			}
		}
		//批量修改
		jdbc.batchUpdate("update tbl_sys_user t set t.dept_id = ? where t.user_code= ?",lists);
		
		result = new ImportResult(totalCnt, recordsize,"");
		tmpFile.delete();//删除临时文件
		serviceResult.setSuccess(true);
		serviceResult.setContent(result);
		return serviceResult;
	}
}
