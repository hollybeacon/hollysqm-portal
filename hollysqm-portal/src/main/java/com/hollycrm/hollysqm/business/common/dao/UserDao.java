package com.hollycrm.hollysqm.business.common.dao;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.orm.dao.jpa.JpaBaseDaoImpl;
import com.hollycrm.hollysqm.entities.User;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * 用户信息Dao
 * @author wangyf
 *
 * 2017年3月23日
 */
@Repository("usersDao")
public class UserDao extends JpaBaseDaoImpl<User, String>{
	
	@Autowired
	private ConfigedJdbc configedJdbc;
	
	public User getUserByCode(String userCode) {
		HashMap<String, Object> params = new HashMap<String,Object>();
		params.put("userCode", userCode);
		List<User> list = configedJdbc.queryForList("select user_code userCode, user_name username, user_no agentCode from tbl_sys_user where user_code = :userCode or user_no = :userCode",
				params, User.class);
		if(!list.isEmpty()) {
			return list.get(0);
		} else {
			return null;
		}
	}
	
	/**
	 * 获取用户的Code和Name
	 * @param agentCodes
	 * @return
	 */
	public Map<String,String> getUserList(String agentCodes) {
		HashMap<String, Object> params = new HashMap<String,Object>();
		List<User> list = configedJdbc.queryForList("select user_no agentCode, user_name username from tbl_sys_user where user_no in " + agentCodes, params , User.class);
		HashMap<String,String> map = new HashMap<String,String>();
		for(User user :list) {
			map.put(user.getAgentCode(), user.getUsername());
		}
		return map;
	}
	

	/**
	 * 获取用户工号
	 * @param agent
	 * @return
	 */
	public String getUserCodeOrNo(String agent, String type) {
		HashMap<String, Object> params = new HashMap<String, Object>();
		List<User> list = null;
		String agents = "";
		params.put("userCode", agent);
		params.put("userNo", agent);
		params.put("username", agent);
		String sql = "select user_no as agentCode, user_code as userCode from tbl_sys_user where user_code=:userCode or user_no=:userNo or user_name = :username";
		if (agent.indexOf(",") != -1) {
			agents = HollysqmUtil.paramParseQueryText(agent, "sql", true);
			sql = "select user_no as agentCode, user_code as userCode from tbl_sys_user where user_code in "
					+ agents + " or user_no in " + agents + "or user_name in " + agents;
		}
		list = configedJdbc.queryForList(sql, params, User.class);
		if (!list.isEmpty()) {
			if (list.size() > 1) {
				String agentCodes = "";
				for (int i = 0; i < list.size(); i++) {
					if (i == list.size() - 1) {
						if (type.equals("userCode")) {
							agentCodes += list.get(i).getUserCode();
						} else {
							agentCodes += list.get(i).getAgentCode();
						}
					} else {
						if (type.equals("userCode")) {
							agentCodes += list.get(i).getUserCode() + ",";
						} else {
							agentCodes += list.get(i).getAgentCode() + ",";
						}
					}
				}
				return agentCodes;
			}
			if (type.equals("userCode")) {
				return list.get(0).getUserCode();
			} else {
				return list.get(0).getAgentCode();
			}
		} else {
			return null;
		}
	}
}

