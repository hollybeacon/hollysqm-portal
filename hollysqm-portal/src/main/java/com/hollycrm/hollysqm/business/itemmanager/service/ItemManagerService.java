package com.hollycrm.hollysqm.business.itemmanager.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.interfaces.entities.IUser;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.orm.service.jpa.JpaBaseServiceImpl;
import com.hollycrm.hollybeacon.basic.rest.web.util.WebUtil;
import com.hollycrm.hollybeacon.basic.util.DateUtils;
import com.hollycrm.hollysqm.entities.TextItem;

@Service("itemmanagerService")
public class ItemManagerService extends JpaBaseServiceImpl<TextItem, String>{

	@Autowired
	private ConfigedJdbc configedJdbc;
	
	/** 
	* @Description:保存或修改标签
	* @param:@param item
	* @param:@return    设定文件 
	* @return:boolean    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public String saveOrUpdateItem(TextItem item){
		IUser user = WebUtil.getLoginUser();
		String currentTime = DateUtils.getCurrentDateAsString(DateUtils.FORMAT_DATE_YYYY_MM_DD_HH_MM_SS);
		item.setModifier(user.getUserCode());
		item.setModifyTime(currentTime);
		item.setDomainId(user.getDomainId());
		//标签主键为空，则为添加，反之为修改
		if(StringUtils.isBlank(item.getTextItemId())){
			item.setTextItemId(null);//防止前端传入“”空值
			//判断是否已存在相同名称标签
			if (isExistItemName(item.getItemName())){
				return "1";
			}
			save(item);
		}else{
			String sql = "select t.plan_id from TBL_QM_PLAN_PARAM t where t.text_item_id = ?";
			//若开启中的质检计划用到该标签，标签将无法停用
			if("0".equals(item.getStatus())){
				List<Map<String,Object>> list = configedJdbc.getJdbcTemplate().queryForList(sql, item.getTextItemId());
				if(!list.isEmpty()){
					return "2";
				}
			}
			update(item);
		}
		
		return "0";
	}
	
	/** 
	* @Description:启用或是停用标签
	* @param:@param params
	* @param:@return    设定文件 
	* @return:boolean    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public boolean enableItem(Map<String,Object> params){
		String status = MapUtils.getString(params, "status");
		String textItemId = MapUtils.getString(params, "textItemId");
		String sql = "select t.plan_id from TBL_QM_PLAN_PARAM t where t.text_item_id = ?";
		//若开启中的质检计划用到该标签，标签将无法停用
		if("0".equals(status)){
			List<Map<String,Object>> list = configedJdbc.getJdbcTemplate().queryForList(sql, textItemId);
			if(!list.isEmpty()){
				return false;
			}
		}
		
		String userCode = WebUtil.getLoginUser().getUserCode();
		String currentTime = DateUtils.getCurrentDateAsString(DateUtils.FORMAT_DATE_YYYY_MM_DD_HH_MM_SS);
		TextItem item = get(textItemId);
		//item.setStatus("1".equals(status) ? "0" : "1");
		item.setStatus(status);
		item.setModifier(userCode);
		item.setModifyTime(currentTime);
		update(item);
		
		return true;
	}
	
	/** 
	* @Description:标签管理分页查询
	* @param:@param result
	* @param:@param params
	* @param:@return    设定文件 
	* @return:PageResult<TextItem>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public PageResult<TextItem> queryItem(PageResult<TextItem> result, Map<String,Object> params){
		String sqlKey = "velocity.manager.queryItemManager";
		Long lon = configedJdbc.selectCount(sqlKey, params);

		result = configedJdbc.selectPageResult(sqlKey, params,result, TextItem.class);
		result.setTotal(lon);
		
		return result;
	}
	
	/** 
	* @Description:查看标签详情
	* @param:@param itemId
	* @param:@return    设定文件 
	* @return:TextItem    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public TextItem getItem(String itemId){
		TextItem item = get(itemId);
		return item == null ? new TextItem() :item;
	}
	
	/**
	 * 判断是否已存在相同名称标签，存在则不能保存
	 * @param itemName
	 * @return
	 */
	public boolean isExistItemName(String itemName){
		List<TextItem> tempList = this.findList("select t from TextItem t where t.itemName=? ", itemName);
		if (tempList != null && tempList.size()>0){
			return true;
		}
		return false;
	}
	
}
