package com.hollycrm.hollysqm.business.agentscore.rest;

import java.util.Map;
import javax.annotation.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollysqm.business.agentscore.service.AgentScoreService;
import com.hollycrm.hollysqm.entities.DataScore;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * @author liujr
 * 2017年3月22日
 * @Description 坐席成绩
 */
@RestService
@RequestMapping(value="/agentscore")
public class AgentScoreRest {
	
		@Resource(name="agentScoreService")
		private AgentScoreService agentScoreService;
		/** 
		* @Description:坐席成绩查询
		* @param:@param param
		* @param:@return    设定文件 
		* @return:ServiceResult    返回类型 
		* @throws: 
		* @author:liujr
		* 2017年3月30日
		*/
		@RequestMapping(value="/queryAgentscore",method = RequestMethod.GET)
		public ServiceResult queryAgentscore(@RequestParam Map<String, Object> param) {
			// 开始分页查询
		    PageResult<DataScore> page = null;
			boolean flag = false;
			String errorMessage = "";
			try {
				//每页大小
				String limit = (String) param.get("rows");
				if (StringUtils.isEmpty(limit))
					limit = com.hollycrm.hollybeacon.basic.Constants.PAGE_DEFAULT_LIMIT;
				// 开始页
				String start = (String) param.get("page");
				if (StringUtils.isEmpty(start))
					start = "1";
				page = new PageResult<DataScore>(Integer.valueOf(limit),Integer.parseInt(start),false);
				
				page = agentScoreService.queryAgentScore(param, page);
				flag = true;
			} catch (Exception e) {
				errorMessage = e.getMessage();
				e.printStackTrace();
			}
			
			return HollysqmUtil.wrapUpPageResult(page, flag, errorMessage);
		}
		
		/**
		 * 记录坐席查看自己的坐席成绩
		 * @param param
		 * @return
		 */
		@RequestMapping(value = "/readPaper", produces = MediaType.APPLICATION_JSON_VALUE)
		public ServiceResult readPaper(@RequestParam Map<String, Object> param) {
			boolean flag = agentScoreService.readPaper(param);
			return HollysqmUtil.wrapUpPageResult(null, flag, "");
		}
		
}
