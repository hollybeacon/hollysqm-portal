package com.hollycrm.hollysqm.business.common.dao;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import com.hollycrm.hollysqm.entities.TblVocCustcontinfo;
/**
 * V8接触记录数据访问层
 * @author jianglong
 * @date 2017年3月29日 下午3:14:49
 */
public interface CustcontinfoDao extends CrudRepository<TblVocCustcontinfo, Integer>  {

	/**
	 * @param startTime
	 * 查询指定质检计划的接触记录信息
	 * @param endTime
	 * @return
	 */
	@Query("select t from TblVocCustcontinfo t where  custcontinfoId=:custcontinfoId")
	public List<TblVocCustcontinfo> getCustcontinfoList(@Param("custcontinfoId") String custcontinfoId);	
}