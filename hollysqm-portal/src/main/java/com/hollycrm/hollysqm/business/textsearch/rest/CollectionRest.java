package com.hollycrm.hollysqm.business.textsearch.rest;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollysqm.business.textsearch.service.CollectionService;
import com.hollycrm.hollysqm.entities.MyCollection;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * 我的收藏
 * @author wangyf
 *
 * 2017年3月20日
 */
@RestService
@RequestMapping(value="/collectionRest")
public class CollectionRest {

	@Resource(name="collectionService")
	private CollectionService collectionService;
	
	/**
	 * 添加我的收藏
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	@RequestMapping(value="/addCollections", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult addCollections(@RequestParam Map<String,Object> param) {
		collectionService.addCollections(param);
		
		return HollysqmUtil.wrapUpPageResult(null, true, "");
	}
	
	
	/**
	 * 删除我的收藏
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	@RequestMapping(value="/delCollections",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult delCollections(@RequestParam Map<String,Object> param) {
		collectionService.delCollections(param);
		
		return HollysqmUtil.wrapUpPageResult(null, true, "");
	}
	
	
	/**
	 * 查询我的收藏列表
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	@RequestMapping(value="/getCollectionList",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getCollectionList(@RequestParam Map<String,Object> param) {
		List<MyCollection> list = collectionService.getCollectionList(param);
		
		return HollysqmUtil.wrapUpPageResult(list, true, "");
	}
	
	/**
	 * 查询我的收藏列表
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	@RequestMapping(value="/getCollectionById",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getCollectionById(@RequestParam Map<String,Object> param) {
		MyCollection myCollection = collectionService.getCollectionById(param);
		
		return HollysqmUtil.wrapUpPageResult(myCollection, true, "");
	}
	
}
