package com.hollycrm.hollysqm.business.common.dao;

import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.orm.dao.jpa.JpaBaseDaoImpl;
import com.hollycrm.hollysqm.entities.Case;

/**
 * 典型案例Dao
 * @author wangyf
 *
 * 2017年3月23日
 */
@Service("caseDao")
public class CaseDao extends JpaBaseDaoImpl<Case, String>{

}
