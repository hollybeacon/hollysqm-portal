package com.hollycrm.hollysqm.business.studyrecord.rest;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollysqm.business.studyrecord.service.StudyRecordService;
import com.hollycrm.hollysqm.entities.StudyRecord;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * @author liujr
 * 2017年7月6日
 * @Description 员工学习记录统计
 */
@RestService
@RequestMapping(value="/studyrecordcount")
public class StudyRecordRest {

	@Resource(name="studyRecordService")
	private StudyRecordService studyRecordService;
	
	/** 
	* @Description:员工学习记录统计
	* @param:@param param 
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年7月6日
	*/
	@RequestMapping(value="/recordCount",method = RequestMethod.GET)
	public ServiceResult recordCount(@RequestParam Map<String,Object> param){
		System.out.println(param);
		PageResult<StudyRecord> page = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			//每页大小
			String limit = (String) param.get("rows");
			if (StringUtils.isEmpty(limit))
				limit = com.hollycrm.hollybeacon.basic.Constants.PAGE_DEFAULT_LIMIT;
			// 开始页
			String start = (String) param.get("page");
			if (StringUtils.isEmpty(start))
				start = "1";
			
			page = new PageResult<StudyRecord>(Integer.valueOf(limit),Integer.valueOf(start),false);
			page = studyRecordService.recordCount(param,page);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(page, flag, errorMessage);
	}
	
	
	
}
