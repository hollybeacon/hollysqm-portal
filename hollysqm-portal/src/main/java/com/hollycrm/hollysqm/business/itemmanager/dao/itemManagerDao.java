package com.hollycrm.hollysqm.business.itemmanager.dao;

import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.orm.dao.jpa.JpaBaseDaoImpl;
import com.hollycrm.hollysqm.entities.TextItem;

@Service("itemManagerDao")
public class itemManagerDao extends JpaBaseDaoImpl<TextItem, String> {

}
