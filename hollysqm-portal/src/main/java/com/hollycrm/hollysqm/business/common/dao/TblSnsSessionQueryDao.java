package com.hollycrm.hollysqm.business.common.dao;

import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import com.hollycrm.hollysqm.entities.TblSnsSessionQuery;

public interface TblSnsSessionQueryDao extends CrudRepository<TblSnsSessionQuery, Integer> {
	
	/**
	 * @param startTime
	 * 查询指定质检计划的接触记录信息
	 * @param endTime
	 * @return
	 */
	@Query("select t from TblSnsSessionQuery t where  sessionId=:sessionId")
	public List<TblSnsSessionQuery> geSnsSessionList(@Param("sessionId") String sessionId);	

}
