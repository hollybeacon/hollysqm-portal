package com.hollycrm.hollysqm.business.usermonth.service;

import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.util.DateUtils;
import com.hollycrm.hollysqm.entities.EchartData;

@Service("usermonthService")
public class UsermonthService {
     
	 @Autowired
	 private ConfigedJdbc configedJdbc;
	
	 /** 
	* @Description:满意度调查
	* @param:@param param
	* @param:@return    设定文件 
	* @return:List<EchartData>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年8月1日
	*/
	public List<EchartData> satisfactionStatistics(Map<String,Object> param){
		 String sqlKey = "velocity.count.satisfactionStatistics";
		 List<EchartData> list = returnList(sqlKey, param, false);
		 return list;
	 }
	 
	 /** 
	* @Description:评分模板统计
	* @param:@param param
	* @param:@return    设定文件 
	* @return:List<DataRank>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public List<EchartData> standartStatistics(Map<String,Object> param){
		 String sqlKey = "velocity.count.standartStatistics";
		
		 List<EchartData> list = returnList(sqlKey, param,false);
		 return list;
	 }
	 
	/** 
	* @Description:坐席成绩趋势
	* @param:@param param
	* @param:@return    设定文件 
	* @return:List<EchartData>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年8月1日
	*/
	public List<EchartData> scoreStatistics(Map<String,Object> param){
		 String sqlKey = "velocity.count.scoreStatistics";
			
		 List<EchartData> list = returnList(sqlKey, param,true);
		 return list;
	}
	
	/** 
	* @Description:评分项使用情况统计
	* @param:@param param
	* @param:@return    设定文件 
	* @return:List<EchartData>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年8月2日
	*/
	public List<EchartData> scoreListStatistics(Map<String,Object> param){
		 String sqlKey = "velocity.count.scoreListStatistics";
			
		 List<EchartData> list = returnList(sqlKey, param,false);
		 return list;
	}
	/** 
	* @Description:评分模板年度使用情况统计
	* @param:@param param
	* @param:@return    设定文件 
	* @return:List<EchartData>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年8月2日
	*/
	public List<EchartData> standardMonthStatistics(Map<String,Object> param){
//		String sqlKey = "velocity.count.standardMonthStatistics";
		String sqlKey = "velocity.count.standartStatistics";
		
		 PageResult<EchartData> page = new PageResult<>(1000, 1, false);
		 page = configedJdbc.selectPageResult(sqlKey, initMap(param),page,EchartData.class);
		 List<String> list1 = getLegend(page.getRows());
		 List<EchartData> list = addMonth(page.getRows(), param.get("years").toString(),list1);
		 for(EchartData ed : list){
				System.out.println(ed.getId()+":"+ed.getName()+":"+ed.getValue());
		 }
		 return list;
	}
	
	/** 
	* @Description:查询统计图
	* @param:@param sqlKey
	* @param:@param param
	* @param:@param isMonth
	* @param:@return    设定文件 
	* @return:List<EchartData>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年8月1日
	*/
	private List<EchartData> returnList(String sqlKey,Map<String,Object> param,boolean isMonth){
		 PageResult<EchartData> page = new PageResult<>(1000, 1, false);
		 page = configedJdbc.selectPageResult(sqlKey, initMap(param),page,EchartData.class);
		 if(isMonth)
			 return addMonth(page.getRows(), param.get("years").toString());
		 else
			 return page.getRows();
		 
	}
	
	/** 
	* @Description:初始化查询条件
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Map<String,Object>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年8月1日
	*/
	private Map<String,Object> initMap(Map<String,Object> param){
		 if(StringUtils.isEmpty(MapUtils.getString(param,"param","agentCode"))){
			 param.put("agentCode", "agentCode");
		 }
		 
		 param.put("years", DateUtils.getCurrentDateAsString("yyyy"));
		return param;
	}
	
	/** 
	* @Description:补充月份为空的数量为0
	* @param:@param list
	* @param:@param year
	* @param:@return    设定文件 
	* @return:List<EchartData>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年8月1日
	*/
	public  List<EchartData> addMonth(List<EchartData> list,String year){
		List<EchartData> li = addMonth(list, year, null);
		return li;
	}
	public  List<EchartData> addMonth(List<EchartData> list,String year,List<String> list1){
		List<EchartData> li = Lists.newArrayList();
		if(list1 == null || list1.isEmpty()){
			list1 = Lists.newArrayList();
			list1.add(null);
		}
		for(String str : list1){
			for(int i = 1;i<=12;i++){
				String date = year+"-"+(i<10?"0"+i:i);
				EchartData d = new EchartData();
				d.setName(date);
				d.setValue("0");
				d.setId(str==null?"":str);
				for(EchartData ed : list){
					//d.setId(str==null?"":str);
					if(date.equals(ed.getName()) && (str == null||str.equals(ed.getId()))){//2017-06 2017-06
						d.setValue(ed.getValue());
						break;
					}else{
						d.setValue("0");
					}
				}
				li.add(d);
			}
		}
		return li;
	}
	public  List<String> getLegend(List<EchartData> list){
		List<String> li = Lists.newArrayList();
		for(EchartData e : list){
			if(!li.contains(e.getId())){
				li.add(e.getId());
			}
		}
		return li;
	}
}
