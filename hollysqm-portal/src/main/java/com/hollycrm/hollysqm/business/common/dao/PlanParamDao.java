package com.hollycrm.hollysqm.business.common.dao;

import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.orm.dao.jpa.JpaBaseDaoImpl;
import com.hollycrm.hollysqm.entities.PlanParam;

@Service("planParamDao")
public class PlanParamDao extends JpaBaseDaoImpl<PlanParam,String>{

}
