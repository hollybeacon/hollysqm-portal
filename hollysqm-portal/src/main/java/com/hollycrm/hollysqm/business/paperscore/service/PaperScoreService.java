package com.hollycrm.hollysqm.business.paperscore.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.shiro.subject.Subject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.rest.web.util.WebUtil;
import com.hollycrm.hollybeacon.basic.util.DateUtils;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollybeacon.business.personoa.common.model.PermissionUtils;
import com.hollycrm.hollybeacon.business.personoa.notice.entities.NoticeInfo;
import com.hollycrm.hollybeacon.business.personoa.notice.entities.NoticeUser;
import com.hollycrm.hollybeacon.business.personoa.security.entities.SysUser;
import com.hollycrm.hollybeacon.business.personoa.system.cache.DictionaryCacheHandler;
import com.hollycrm.hollybeacon.business.personoa.system.cache.SysConfigCacheHandler;
import com.hollycrm.hollysqm.business.common.dao.PaperDao;
import com.hollycrm.hollysqm.business.common.service.CommonNoticeService;
import com.hollycrm.hollysqm.entities.DataScore;
import com.hollycrm.hollysqm.entities.PaperData;


/**
 * @author liujr
 * 2017年5月19日
 * @Description 质检成绩
 */
@Service("paperScoreService")
public class PaperScoreService {
	
	@Autowired
	private ConfigedJdbc configedJdbc;
	
	@Resource(name = "dictionaryCacheHandler")
	private DictionaryCacheHandler dictionaryCacheHandler;
	
	@Resource(name = "sysConfigCacheHandler")
	private SysConfigCacheHandler sysConfigCacheHandler;
	
	@Resource(name="commonNoticeService")
	private CommonNoticeService commonNoticeService;
	
	@Resource(name="paperDao")
	private PaperDao paperDao;
	
	/** 
	* @Description:质检成绩分页查询
	* @param:@param param
	* @param:@param pageResult
	* @param:@param currentPage
	* @param:@param rowCount
	* @param:@return    设定文件 
	* @return:PageResult<Map<String,Object>>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public  PageResult<DataScore> queryPaperScore(Map<String, Object> param, PageResult<DataScore> pageResult){

		Subject subject = PermissionUtils.getSubject();
		//获取当前操作人帐号
		String userCode = WebUtil.getLoginUser().getUserCode();
		String roleId = sysConfigCacheHandler.getSysConfigValue("USER_ROLE_TAG", "SQM_ZJY");
		//质检员角色只能查看自己质检过的单子
		if(subject.hasRole(roleId)){
			param.put("createCode", userCode);
		};
		
		String sqlKey = "velocity.score.queryPaperScore";
		Long lon = configedJdbc.selectCount(sqlKey, param);

		pageResult =  configedJdbc.selectPageResult(sqlKey, param, pageResult,DataScore.class);
		pageResult.setTotal(lon);
		
		return pageResult;
	}
	/** 
	* @Description:优秀案例查询 
	* @param:@param param
	* @param:@param pageResult
	* @param:@return    设定文件 
	* @return:PageResult<DataScore>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年5月2日
	*/
	public  PageResult<DataScore> queryPaperCase(Map<String, Object> param, PageResult<DataScore> pageResult){
		
		Subject subject = PermissionUtils.getSubject();
		//获取当前操作人帐号
		String userCode = WebUtil.getLoginUser().getUserCode();
		//质检员角色只能查看自己质检过的单子
		String roleId = sysConfigCacheHandler.getSysConfigValue("USER_ROLE_TAG", "SQM_ZJY");
		if(subject.hasRole(roleId)){
			param.put("createCode", userCode);
		};
		
		String sqlKey = "velocity.score.queryPaperCase";
		Long lon = configedJdbc.selectCount(sqlKey, param);
		
		pageResult =  configedJdbc.selectPageResult(sqlKey, param, pageResult,DataScore.class);
		pageResult.setTotal(lon);
		
		return pageResult;
	}
	
	/** 
	* @Description:发布成绩
	* @param:@param paperId    设定文件 
	* @return:void    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年5月10日
	*/
	public void publishedScore(Map<String,Object> param){
		Subject subject = PermissionUtils.getSubject();
		//获取当前操作人帐号
		String userCode = WebUtil.getLoginUser().getUserCode();
		//质检员角色只能查看自己质检过的单子
		String roleId = sysConfigCacheHandler.getSysConfigValue("USER_ROLE_TAG", "SQM_ZJY");
		if(subject.hasRole(roleId)){
			param.put("createCode", userCode);
		};
		
		List<Object[]> listObj = Lists.newArrayList();
		String currentTime = DateUtils.getCurrentDateTimeAsString();
		//批量发布成绩
		if(StringUtils.isEmpty(param.get("paperId").toString())){
			String sqlKey = "velocity.score.queryPaperScore";
			List<Map<String,Object>> list = configedJdbc.selectAllList(sqlKey, param);
			if(list.isEmpty()) return;
			for(Map<String,Object> map : list){
				Object[] objs = new Object[]{currentTime,map.get("PAPER_ID")};
				listObj.add(objs);
				sendNotice(map.get("PAPER_ID"));
			}
		}else{
			Object[] objs = new Object[]{currentTime,param.get("paperId").toString()};
			listObj.add(objs);
			sendNotice(param.get("paperId"));
		}
		configedJdbc.getJdbcTemplate().batchUpdate("update tbl_qm_paper t set t.is_public_result = '1' ,t.release_time = ? where t.paper_id = ?", listObj);
	}
	
	public void sendNotice(Object paperId){
		//根据评分发送业务通知  是发布成绩
		Map<String,Object> param = Maps.newHashMap();
		PaperData paperData = paperDao.get(paperId.toString());
		int passScore = Integer.parseInt(dictionaryCacheHandler.getDictionaryValue("NOTICE_PASS_SCORE", "平均"));
		if(paperData.getQualityStatus().equals("1") && paperData.getTotalScore() < passScore) {
			String title = sysConfigCacheHandler.getSysConfigValue("ScoreNotice", "TITLE");//业务通知标题
			String content = sysConfigCacheHandler.getSysConfigValue("ScoreNotice", "CONTENT");//业务通知内容
			String publishTime = DateUtils.getCurrentDateAsString(DateUtils.FORMAT_DATE_YYYY_MM_DD_HH_MM_SS); 
			String expiryTime = DateUtils.getCurrentDateAsString() + " 23:59:59"; 
			String planname = "";
			if(StringUtils.isNotEmpty(paperData.getPlanId())) {
				param.put("planId", paperData.getPlanId());
				planname = configedJdbc.queryForMap("select plan_name planName from tbl_qm_plan where plan_id = :planId", param).get("planName").toString();
			} 
			
			SysUser sysUser = commonNoticeService.getUserId(paperData.getCheckerCode());
			String checker = sysUser.getUserName() + "(" + sysUser.getUserCode() + ")";
			String totalScore = String.valueOf(paperData.getTotalScore());
			String checkTime = paperData.getModifyTime();
			content = content.replaceAll("\\$\\{planname\\}", planname).replaceAll("\\$\\{checker\\}", checker).replaceAll("\\$\\{totalScore\\}", totalScore).replaceAll("\\$\\{checkTime\\}", checkTime);
			
			commonNoticeService.sendNotice(paperData.getUserCode(), paperData.getCheckerCode(), "1", NoticeInfo.NOTICE_EMERGENCY_NORMAL, title, content, publishTime, NoticeInfo.NOTICE_SEND_TYPE_IMMEDIATELY, NoticeInfo.NOTICE_ATTENTION_TYPE_SCROLL+","+NoticeInfo.NOTICE_ATTENTION_TYPE_POPUP, NoticeInfo.NOTICE_STATUS_PUBLISHED, expiryTime, paperData.getQualityStatus(), NoticeUser.NOTICE_STATUS_UNREAD);
		}
	}		
	
	/** 
	* @Description:删除典型案例单
	* @param:@param caseId    设定文件 
	* @return:void    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年5月15日
	*/
	public void deleteCase(String caseId){
		String currentTime = DateUtils.getCurrentDateTimeAsString();
		String userCode = WebUtil.getLoginUser().getUserCode();
		configedJdbc.getJdbcTemplate().update("update tbl_qm_case t set t.record_type = '2',t.modifier = ?,t.modify_time = ? where t.case_id = ?",userCode ,currentTime, caseId);
	}

}
