package com.hollycrm.hollysqm.business.common.service;

import javax.annotation.Resource;
import org.springframework.stereotype.Service;
import com.hollycrm.hollybeacon.basic.util.DateUtils;
import com.hollycrm.hollybeacon.business.personoa.notice.entities.NoticeInfo;
import com.hollycrm.hollybeacon.business.personoa.notice.entities.NoticeUser;
import com.hollycrm.hollybeacon.business.personoa.notice.service.NoticeUserServiceImpl;
import com.hollycrm.hollybeacon.business.personoa.security.entities.SysUser;
import com.hollycrm.hollybeacon.business.personoa.security.service.UserServiceImpl;
import com.hollycrm.hollybeacon.business.personoa.system.cache.SysConfigCacheHandler;

@Service("commonNoticeService")
public class CommonNoticeService {
	
	@Resource(name = "userService")
	private UserServiceImpl userService;

	@Resource(name = "noticeUserService")
	private NoticeUserServiceImpl noticeUserService;

	@Resource(name = "sysConfigCacheHandler")
	private SysConfigCacheHandler sysConfigCacheHandler;
	
	public void sendNotice(String receiveCode, String senderCode, String noticeType, String emergency, String title, String content, String publishTime, String sendType, String attentionType, String isDelete, String expiryTime, String remark, String noticeStatus) {
		SysUser receiver = getUserId(receiveCode);
		SysUser sender =  getUserId(senderCode);
		String currentTime = DateUtils.getCurrentDateAsString(DateUtils.FORMAT_DATE_YYYY_MM_DD_HH_MM_SS);
		
		NoticeInfo notice = new NoticeInfo();
		notice.setNoticeType(noticeType);//公告类型:0-故障,1-通告,2-公告
		notice.setEmergency(emergency);//紧急程度:0:一般,1:紧急,2:非常紧急
		notice.setTitle(title);//通知标题
		notice.setContent(content);//通知内容
		notice.setPublisher(sender.getUserName());//发布人
		notice.setPublishTime(publishTime);//发布时间
		 
		notice.setCreateTime(currentTime);//创建时间
		notice.setIsImmediately(sendType);//发布方式:0:立即发送;1:定时发送
		notice.setAttentionType(attentionType);//提醒方式,0:滚动,1:弹出
		notice.setIsDelete(isDelete);//是否已删除 0:已删除,1:未发布,2:已发布,3:已过期
		notice.setExpiryTime(expiryTime);//失效时间   当天有效
		notice.setDomainId(sender.getDomainId());
		notice.setOrgId(sender.getOrgId());
		notice.setSendee(receiver.getUserCode());//收件人帐号
		notice.setSendeeNames(receiver.getUserName()+"("+receiver.getUserCode()+")");//收件人名称
		notice.setIsSticky("1");//不置顶
		notice.setStickyTime("1970-01-02 11:59:59");//置顶时间（如果不置顶则设置靠后的时间）
		noticeUserService.save(notice);
		 
		NoticeUser noticeUser = new NoticeUser();
		noticeUser.setCreateTime(currentTime);//创建时间
		noticeUser.setLastModifyTime(currentTime);//最后修改时间
		noticeUser.setNoticeType(notice.getNoticeType());//公告类型
		noticeUser.setNoticeId(notice.getId());//公告ID
		noticeUser.setPublisher(sender.getUserCode());//发布人帐号
		noticeUser.setPublishTime(notice.getPublishTime());//发布时间
		noticeUser.setUserId(receiver.getId());//发布人ID
		noticeUser.setRemark(remark);//质检单状态
		//阅读状态.0:未读,1:已读
		noticeUser.setStatus(noticeStatus);//阅读状态.0:未读,1:已读
		noticeUserService.save(noticeUser);
		
	}
	
	/** 
	* @Description:通过user_code获取用户信息实体类
	* @param:@param userCode
	* @param:@return    设定文件 
	* @return:SysUser    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public SysUser getUserId(String userCode){
		  SysUser user = userService.findOneBySql("select * from tbl_sys_user t where t.user_code = ?", userCode);
		  return user;
	}

}
