package com.hollycrm.hollysqm.business.planmanager.rest;

import java.io.File;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.io.FileUtils;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.MediaType;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSONObject;
import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hollycrm.hollybeacon.basic.cache.redis.client.IRedisClient;
import com.hollycrm.hollybeacon.basic.core.annotations.ActionLog;
import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.csv.config.CsvConfiguration;
import com.hollycrm.hollybeacon.basic.csv.config.CsvTable;
import com.hollycrm.hollybeacon.basic.csv.config.CsvTable.CsvField;
import com.hollycrm.hollybeacon.basic.csv.impl.CsvImportImpl;
import com.hollycrm.hollybeacon.basic.csv.parser.AbstractParser;
import com.hollycrm.hollybeacon.basic.excle.ExcelImportImpl;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollybeacon.basic.util.WebUtils;
import com.hollycrm.hollysqm.business.planmanager.service.PlanService;
import com.hollycrm.hollysqm.entities.Plan;
import com.hollycrm.hollysqm.entities.PlanCache;
import com.hollycrm.hollysqm.entities.PlanCensus;
import com.hollycrm.hollysqm.entities.PlanLog;
import com.hollycrm.hollysqm.entities.PlanUserMapping;
import com.hollycrm.hollysqm.entities.Record;
import com.hollycrm.hollysqm.entities.User;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * 质检计划处理
 * 
 * @author wangyf
 * @date 2017年3月9日
 */

@SuppressWarnings({"deprecation","resource"})
@RestService
@RequestMapping(value = "/planRest")
public class PlanRest {

	@Resource(name = "planService")
	private PlanService planService;
	
	@Autowired
	private IRedisClient springBinaryRedisClient;
	
	@Resource(name = "excleImport")
	protected ExcelImportImpl excleImport;
	
	@Autowired
	private ConfigedJdbc configedJdbc;
	
	@Resource(name = "csvConfig")
	protected CsvConfiguration csvConfig;

	/**
	 * 执行质检计划的微服务调用地址
	 */
	@Value("${hollysqm.handle.planurl}")
	private String handleUrl;

	/**
	 * 获取质检计划列表
	 * 
	 * @param param
	 * @param session
	 * @return
	 * @throws 
	 * @author wangyf
	 */
	@RequestMapping(value = "/getPlanList", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getPlanList(@RequestParam Map<String, Object> param) {
		PageResult<Plan> page = planService.getPlanList(param);
		return HollysqmUtil.wrapUpPageResult(page, true, "");
	}
	
	/**
	 * 获取质检抽取统计情况
	 * 
	 * @param param
	 * @param session
	 * @return
	 * @throws 
	 * @author wangyf
	 */
	@RequestMapping(value = "/getPlanCensus", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getPlanCensus(@RequestParam Map<String, Object> param) {
		PageResult<PlanCensus> page = planService.getPlanCensus(param);
		return HollysqmUtil.wrapUpPageResult(page, true, "");
	}
	
	/**
	 * 获取质检计划列表
	 * 
	 * @param param
	 * @param session
	 * @return
	 * @throws 
	 * @author wangyf
	 */
	@RequestMapping(value = "/copyPlan", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult copyPlan(@RequestParam Map<String, Object> param) {
		boolean flag = planService.copyPlan(param);
		return HollysqmUtil.wrapUpPageResult(null, flag, "");
	}

	/**
	 * 清除缓存中的质检计划
	 * @return
	 */
	@RequestMapping(value = "/clearChachePlan", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult clearChachePlan(@RequestParam Map<String, Object> param, HttpSession session) {
		//session.removeAttribute("cache");
		String sessionId = session.getId();
		springBinaryRedisClient.delete(sessionId);
		return HollysqmUtil.wrapUpPageResult(null, true, "");
	}
	/**
	 * 进入填写基本信息页面
	 * 
	 * @param param
	 * @param session
	 * @return
	 * @throws 
	 * @author wangyf
	 */
	@RequestMapping(value = "/addOrEditBaseInformation", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult addOrEditBaseInformation(@RequestParam Map<String, Object> param, HttpSession session) {
		PlanCache cache = null;
		String planId = param.get("planId") == null ? "" : param.get("planId").toString();
		String sessionId = session.getId();
		if (!planId.equals("")) {// 点击列表的修改按钮进行修改
			// 首先清空缓存中的计划
			//session.removeAttribute("cache");
			springBinaryRedisClient.delete(sessionId);
			cache = planService.getPlanDetailById(planId);
			if (cache != null) {
				if(cache.getIsExport().equals("0")) {
					List<User> agents = planService.getUsers(planId, "agent");
					List<User> checkers = planService.getUsers(planId, "checker");
					cache.setAgents(agents);
					cache.setCheckers(checkers);
				} else {
					List<PlanUserMapping> userMsg = planService.getPlanUserMapping(planId);
					List<PlanUserMapping> falseMapping = Lists.newArrayList();//创建失败数据集合
					this.getUserMappingAgents(userMsg,falseMapping);
					cache.setUserMsg(userMsg);
				}
				
				//session.setAttribute("cache", cache);
				springBinaryRedisClient.put(sessionId, cache);
			}
		} else {// 新建质检计划或者再次修改未保存的质检计划
			if (this.getCache(sessionId) == null) {
				ArrayList<User> agents = new ArrayList<User>();
				ArrayList<User> checkers = new ArrayList<User>();
				cache = new PlanCache();
				cache.setAgents(agents);
				cache.setCheckers(checkers);
				//session.setAttribute("cache", cache);
				springBinaryRedisClient.put(sessionId, cache);
			} else {
				//cache = (PlanCache) session.getAttribute("cache");
				cache = this.getCache(sessionId);
			}
		}

		if (cache == null) {
			return HollysqmUtil.wrapUpPageResult(cache, false, "error");
		} else {
			return HollysqmUtil.wrapUpPageResult(cache, true, "");
		}
	}

	/**
	 * 保存计划的基本信息("填写基本信息页面"的下一步按钮)
	 * 
	 * @param param
	 * @param session
	 * @return
	 * @throws 
	 * @author wangyf
	 */
	@RequestMapping(value = "/saveBaseInfomation", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult saveBaseInfomation(@RequestParam Map<String, Object> param, HttpSession session) {
		String sessionId = session.getId();
		//PlanCache cache = (PlanCache) session.getAttribute("cache");
		PlanCache cache = this.getCache(sessionId);
		if (cache == null) {
			return HollysqmUtil.wrapUpPageResult(null, false, "error");
		} else {
			HollysqmUtil.fillParam(param, PlanCache.class, cache);
			if(!planService.validatePlanName(param, cache)) {
				return HollysqmUtil.wrapUpPageResult(null, false, "已有相同名字的质检计划");
			}
			//session.setAttribute("cache", cache);
			springBinaryRedisClient.put(sessionId, cache);
			return HollysqmUtil.wrapUpPageResult(null, true, "");
		}
	}

	/**
	 * 设置抽取规则
	 * 
	 * @param param
	 * @param session
	 * @return
	 * @throws 
	 * @author wangyf
	 */
	@RequestMapping(value = "/setPlanExtractRule", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult setPlanExtractRule(@RequestParam Map<String, Object> param, HttpSession session) {
		String sessionId = session.getId();
		//PlanCache cache = (PlanCache) session.getAttribute("cache");
		PlanCache cache = this.getCache(sessionId);
		if (cache == null) {
			return HollysqmUtil.wrapUpPageResult(cache, false, "error");
		} else {
			return HollysqmUtil.wrapUpPageResult(cache, true, "");
		}
	}

	/**
	 * 保存抽取规则(设置抽取规则下一步按钮)
	 * 
	 * @param param
	 * @param session
	 * @return
	 * @throws 
	 * @author wangyf
	 */
	@RequestMapping(value = "/savePlanExtractRule", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult savePlanExtractRule(@RequestParam Map<String, Object> param, HttpSession session) {
		String sessionId = session.getId();
		//PlanCache cache = (PlanCache) session.getAttribute("cache");
		PlanCache cache = this.getCache(sessionId);
		if (cache == null) {
			return HollysqmUtil.wrapUpPageResult(null, false, "error");
		} else {
			HollysqmUtil.fillParam(param, PlanCache.class, cache);
			if(StringUtils.isNotEmpty(param.get("agent"))) {
				List<User> agents = planService.getUsers(param);
				cache.setAgents(agents);
			}
			//session.setAttribute("cache", cache);
			springBinaryRedisClient.put(sessionId, cache);
			return HollysqmUtil.wrapUpPageResult(null, true, "");
		}
	}

	/**
	 * 设置质检员
	 * 
	 * @param param
	 * @param session
	 * @return
	 * @throws 
	 * @author wangyf
	 */
	@RequestMapping(value = "/setPlanChekers", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult setPlanChekers(@RequestParam Map<String, Object> param, HttpSession session) {
		String sessionId = session.getId();
		//PlanCache cache = (PlanCache) session.getAttribute("cache");
		PlanCache cache = this.getCache(sessionId);
		if (cache == null) {
			return HollysqmUtil.wrapUpPageResult(cache, false, "error");
		} else {
			return HollysqmUtil.wrapUpPageResult(cache, true, "");
		}
	}

	/**
	 * 保存质检员(选择质检员下一步按钮)
	 * 
	 * @param param
	 * @param session
	 * @return
	 * @throws 
	 * @author wangyf
	 */
	@RequestMapping(value = "/savePlanCheckers", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult savePlanCheckers(@RequestParam Map<String, Object> param, HttpSession session) {
		String sessionId = session.getId();
		//PlanCache cache = (PlanCache) session.getAttribute("cache");
		PlanCache cache = this.getCache(sessionId);
		if (cache == null) {
			return HollysqmUtil.wrapUpPageResult(null, false, "error");
		} else {
			if(StringUtils.isNotEmpty(param.get("checker"))) {
				List<User> checkers = planService.getUsers(param);
				cache.setCheckers(checkers);
			}
			//session.setAttribute("cache", cache);
			springBinaryRedisClient.put(sessionId, cache);
			return HollysqmUtil.wrapUpPageResult(null, true, "");
		}
	}

	/**
	 * 展示质检计划详情
	 * 
	 * @param param
	 * @param session
	 * @return
	 * @throws 
	 * @author wangyf
	 */
	@RequestMapping(value = "/showPlanDetail", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult showPlanDetail(@RequestParam Map<String, Object> param, HttpSession session) {
		String sessionId = session.getId();
		PlanCache cache = null;
		String planId = param.get("planId") == null ? "" : param.get("planId").toString();
		if (!planId.equals("")) {
			cache = planService.getPlanDetailById(planId);
			if(cache != null) {
				if(cache.getIsExport().equals("0")) {
					List<User> agents = planService.getUsers(planId, "agent");
					List<User> checkers = planService.getUsers(planId, "checker");
					cache.setAgents(agents);
					cache.setCheckers(checkers);
				} else {
					List<PlanUserMapping> userMsg = planService.getPlanUserMapping(planId);
					List<PlanUserMapping> falseMapping = Lists.newArrayList();//创建失败数据集合
					this.getUserMappingAgents(userMsg,falseMapping);
					cache.setUserMsg(userMsg);
				}
			}
			
			
		} else {
			//cache = (PlanCache) session.getAttribute("cache");
			cache = this.getCache(sessionId);
		}

		if (cache == null) {
			return HollysqmUtil.wrapUpPageResult(cache, false, "error");
		} else {
			return HollysqmUtil.wrapUpPageResult(cache, true, "");
		}
	}

	/**
	 * 保存质检计划
	 * 
	 * @param param
	 * @param session
	 * @return
	 * @throws 
	 * @author wangyf
	 */
	@RequestMapping(value = "/saveOrUpdatePlan", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult saveOrUpdatePlan(@RequestParam Map<String, Object> param, HttpSession session) {
		String sessionId = session.getId();
		//PlanCache cache = (PlanCache) session.getAttribute("cache");
		PlanCache cache = this.getCache(sessionId);
		if (cache == null) {
			return HollysqmUtil.wrapUpPageResult(null, false, "error");
		} else {
			planService.saveOrUpdate(cache);
			//session.removeAttribute("cache");
			springBinaryRedisClient.delete(sessionId);
			return HollysqmUtil.wrapUpPageResult(null, true, "");
		}

	}

	/**
	 * 开启或关闭质检计划
	 * 
	 * @param param
	 * @return
	 * @throws 
	 * @author wangyf
	 */
	@RequestMapping(value = "/switchPlan", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult switchPlan(@RequestParam Map<String, Object> param) {
		planService.switchPlan(param);
		return HollysqmUtil.wrapUpPageResult(null, true, "");
	}

	/**
	 * 查询质检计划的执行日志
	 * @param param
	 * @param session
	 * @return
	 * @throws 
	 * @author wangyf
	 */
	@RequestMapping(value = "/getPlanLog", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getPlanLog(@RequestParam Map<String, Object> param, HttpSession session) {
		PageResult<PlanLog> page = planService.getPlanLogList(param);
		return HollysqmUtil.wrapUpPageResult(page, true, "");
	}

	/**
	 * 生成预览当前计划的质检单
	 * 
	 * @return
	 */
	@RequestMapping(value = "/viewPlanPaper", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult viewPlanPaper(@RequestParam Map<String, Object> param, HttpSession session) {
//		List<Record> records = null;
//		records = planService.getPlanRecords(param);
//		records = JSONObject.parseArray(recordsJson, Record.class);
//		List<String> contactIds = new ArrayList<String>();
//		for(Record record : records) {
//			contactIds.add(record.getContactId());
//		}
// 		return HollysqmUtil.wrapUpPageResult(contactIds, true, "");
		return JSONObject.parseObject("{\"content\":[\"000E7AA13AE24878BBF85106D99ADC6E\",\"000C7D7F07884B8785EAAE2BC12506BD\",\"00A7ECF8681649B08CB9D90FF8C1B5F0\",\"00A3A9ECD14C48AF84BC9C303A231A56\",\"00A3F741B1104BA0B5A62B9EBFBAD3B3\"],\"errorMessage\":\"\",\"errorType\":null,\"success\":true}",ServiceResult.class);
	}

	
	/**
	 * 修改计划用户
	 * 
	 * @return
	 */
	@RequestMapping(value = "/editPlanUser", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult editPlanUser(@RequestParam Map<String, Object> param, HttpSession session) {
		boolean flag = planService.editPlanUser(param);
 		return HollysqmUtil.wrapUpPageResult(null, flag, "");
	}
	
	/**
	 * 执行计划
	 * 
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/executePlan", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult executePlan(@RequestParam Map<String, Object> param) {
		if (StringUtils.isNotEmpty(param.get("planId"))) {
			String result = "";
			String url = handleUrl + param.get("planId").toString();
			try {
				HttpGet request = new HttpGet(url);// 这里发送get请求
				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse response = httpClient.execute(request);
				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
					result = EntityUtils.toString(response.getEntity(), "utf-8");
				}
			} catch (Exception e) {
				e.printStackTrace();
			}
			return HollysqmUtil.wrapUpPageResult(result, true, "");
		} else {
			return HollysqmUtil.wrapUpPageResult(null, false, "");
		}

	}
	
	/**
	 * 查询质检计划所属人员的同组人员信息
	 * @param param
	 * @return
	 */
	@RequestMapping(value = "/getPlanRelevanceUser", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getPlanRelevanceUser(@RequestParam Map<String, Object> param) {
		List<User> list = null;
		list = planService.getPlanRelevanceUser(param);
		return HollysqmUtil.wrapUpPageResult(list, true, "");
	}
	
	/**
	 * 获取质检计划抽取预估数据
	 * @return
	 */
	@SuppressWarnings("unused")
	@RequestMapping(value="/getPlanExtractDetail", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getPlanExtractDetail(@RequestParam Map<String,Object> param, HttpSession session) {
		
		if(StringUtils.isEmpty(param.get("planId"))) {
			String sessionId = session.getId();
			PlanCache cache = this.getCache(sessionId);
			int extractNum = 0;
			int extractUserNum = 0;
			int extractDeptNum = 0;
			
			HashMap<String,Integer>  agentMap = null;
			HashMap<String,Integer>  deptMap = null;
			if(cache.getIsExport().equals("1")) {//导入模式
				List<PlanUserMapping> userMsg = cache.getUserMsg();
				agentMap = new HashMap<String,Integer>();
				for(PlanUserMapping msg : userMsg) {
					agentMap.put(msg.getAgentCode(), (agentMap.get(msg.getAgentCode()) == null ? 0 : agentMap.get(msg.getAgentCode())) + 1);
					extractNum += (Integer)msg.getExtractNumber();
				}
				deptMap = new HashMap<String,Integer>();
				for(PlanUserMapping msg : userMsg) {
					deptMap.put(msg.getAgentDepartment(), (deptMap.get(msg.getAgentDepartment()) == null ? 0 : deptMap.get(msg.getAgentDepartment())) + 1);
				}
				
				for(String key : agentMap.keySet()) {
					extractUserNum += 1;
				}
				
			} else {//非导入模式
				deptMap = new HashMap<String,Integer>();
				List<User> agents = cache.getAgents();
				for(User user : agents) {
					deptMap.put(user.getDepartment(), (deptMap.get(user.getDepartment()) == null ? 0 : deptMap.get(user.getDepartment())) + 1);
				}
				extractUserNum = agents.size();
			}
			
			for(String key : deptMap.keySet()) {
				extractDeptNum += 1;
			}
			
			HashMap<String,Object> map = new HashMap<String,Object>();
			map.put("EXTRACTNUM", extractNum);
			map.put("EXTRACTUSERNUM", extractUserNum);
			map.put("EXTRACTDEPTNUM", extractDeptNum);
			return HollysqmUtil.wrapUpPageResult(map, true, "");
		} else {
			Map<String,Object> map = planService.getPlanExtractDetail(param);
			return HollysqmUtil.wrapUpPageResult(map, true, "");
		}
		
	}
	
	public PlanCache getCache(String sessionId) {
		if(StringUtils.isEmpty(sessionId)) {
			return null;
		}
		
		String json = springBinaryRedisClient.get(sessionId);
		PlanCache cache = JSONObject.parseObject(json, PlanCache.class);
		return cache;
	}
	
	@ActionLog(content = "CSV HSQL导入向导页", description = "CSV导入向导页")
	@RequestMapping(value = "/wizard/import")
	public void importWizard(@RequestParam String tableId,
			@RequestParam Map<String, Object> param,
			HttpServletRequest request, HttpServletResponse response)
			throws Exception {
		CsvTable csvTable = csvConfig.getCsvTable(tableId);
		Assert.notNull(csvTable, tableId + "对应的csvTable配置不存在");
		List<? extends CsvField> fields = csvConfig.getImportedCsvFields(tableId);
		request.setAttribute("fields", fields);
		request.getRequestDispatcher("/hollysqm/common/import/page/import_plan.jsp").forward(request,response);
	}
	
	@ActionLog(content = "导入rest方法", description = "调用基类统一的导入列表方法")
	@RequestMapping(value = "/import", method = RequestMethod.POST)
	public ServiceResult baseImport(@RequestParam MultipartFile file,
			@RequestParam String tableId,HttpServletRequest request)
			throws Exception {
		String sessionId = request.getSession().getId();
		PlanCache cache = this.getCache(sessionId);

		// 1.保存到临时文件中
		InputStream in = file.getInputStream();
		File tempDir = WebUtils.getTempDir(request.getServletContext());
		File tmpFile = null;
		CsvImportImpl.setRequest(request);
		//保存临时文件
		tmpFile = new File(tempDir, UUID.randomUUID().toString() + ".xls");
		FileUtils.copyInputStreamToFile(in, tmpFile);
		boolean headerEnabled = csvConfig.getCsvTable(tableId).getHeaderEnabled();//判断是否有表头
		AbstractParser parser = excleImport.getParser();
		List<String[]> allrecords = parser.parseRows(tmpFile);//获取表格中数据
		if(allrecords.size()==1) throw new Exception("导入模板不正确，或不能导入空数据！");
		List<PlanUserMapping> userMsg = new ArrayList<PlanUserMapping>();
		PlanUserMapping planMapping = null;
		for(int i = headerEnabled?1:0;i< allrecords.size();i++){
			String[] record = allrecords.get(i);
			planMapping = new PlanUserMapping();
			planMapping.setAgentCode(record[0]);
			planMapping.setCheckerAgentCode(record[1]);
			planMapping.setExtractNumber(record[2]);
			planMapping.setRank(record[3]);
			userMsg.add(planMapping);
		}
		List<PlanUserMapping> falseMapping = Lists.newArrayList();//创建失败数据集合
		String msg = this.getUserMappingAgents(userMsg,falseMapping);
		Map<String,Object> map = Maps.newHashMap();
		map.put("msg", msg);
		cache.setUserMsg(userMsg);
		map.put("planCache", cache);
		map.put("planCacheFalse", falseMapping);
		springBinaryRedisClient.put(sessionId, cache);
		tmpFile.delete();//删除临时文件
		
		return HollysqmUtil.wrapUpPageResult(map, true, "");
	}
	
	public String getUserMappingAgents(List<PlanUserMapping> mapping,List<PlanUserMapping> falseMapping) {
		String agents = "";
		for(int i=0; i<mapping.size(); i++) {
			if(i == mapping.size() -1) {
				agents += mapping.get(i).getAgentCode() + "," + mapping.get(i).getCheckerAgentCode();
			} else {
				agents += mapping.get(i).getAgentCode() + "," + mapping.get(i).getCheckerAgentCode() + ",";
			}
		}
		
		
		List<User> users = planService.getUserMappingDetail(agents);
		int totalCount = mapping.size();//导入总数
		int falseCount = 0;//失败个数
		for(PlanUserMapping userMapping : mapping) {
			try {
				if(userMapping.getAgentCode() == null)
					throw new Exception("坐席工号不能为空");
				userMapping.setAgentCode(userMapping.getAgentCode().trim());
				if(userMapping.getCheckerAgentCode() == null)
					throw new Exception("质检员工号不能为空");
				userMapping.setCheckerAgentCode(userMapping.getCheckerAgentCode().trim());
				if(userMapping.getExtractNumber() == null)
					throw new Exception("抽取条数不能为空");
				userMapping.setExtractNumber(Integer.parseInt(userMapping.getExtractNumber()+""));
				/*if(userMapping.getRank() == null)
					throw new Exception("分层类型不能为空");
				  userMapping.setRank(userMapping.getRank().trim());
				*/
				
				for(User user :users) {
					if(userMapping.getAgentCode().equals(user.getAgentCode())) {
						userMapping.setUserCode(user.getUserCode());
						userMapping.setAgentName(user.getUsername());
						userMapping.setAgentDepartment(user.getDepartment());
						userMapping.setAgentDeptName(user.getDeptName());
					}
					
					if(userMapping.getCheckerAgentCode().equals(user.getAgentCode())) {
						userMapping.setCheckerCode(user.getUserCode());
						userMapping.setCheckerName(user.getUsername());
						userMapping.setCheckerDepartment(user.getDepartment());
						userMapping.setCheckerDeptName(user.getDeptName());
					}
				}
				if(userMapping.getAgentDepartment() == null || userMapping.getCheckerDepartment() == null){
					falseCount += 1;
					falseMapping.add(userMapping);
				}
			} catch (Exception e) {
				falseCount += 1;
				falseMapping.add(userMapping);
			}
		}
		//删除失败数据
		for(PlanUserMapping userMapping : falseMapping){
			userMapping.setAgentCode(userMapping.getAgentCode()==null?"":userMapping.getAgentCode());
			userMapping.setAgentDepartment(userMapping.getAgentDepartment()==null?"":userMapping.getAgentDepartment());
			userMapping.setAgentDeptName(userMapping.getAgentDeptName()==null?"":userMapping.getAgentDeptName());
			userMapping.setAgentName(userMapping.getAgentName()==null?"":userMapping.getAgentName());
			
			userMapping.setCheckerAgentCode(userMapping.getCheckerCode()==null?"":userMapping.getCheckerCode());
			userMapping.setCheckerDepartment(userMapping.getCheckerDepartment()==null?"":userMapping.getCheckerDepartment());
			userMapping.setCheckerDeptName(userMapping.getCheckerDeptName()==null?"":userMapping.getCheckerDeptName());
			userMapping.setCheckerName(userMapping.getCheckerName()==null?"":userMapping.getCheckerName());
			
			userMapping.setExtractNumber(userMapping.getExtractNumber()==null?"":userMapping.getExtractNumber());
			userMapping.setPlanId(userMapping.getPlanId()==null?"":userMapping.getPlanId());
			userMapping.setRank(userMapping.getRank()==null?"":userMapping.getRank());
			userMapping.setUserCode(userMapping.getUserCode()==null?"":userMapping.getUserCode());
			
			mapping.remove(userMapping);
		}
		String msg = "一共导入"+totalCount+"条,成功"+(totalCount-falseCount)+"条,失败"+falseCount+"条";
		return msg;
	}
}
