package com.hollycrm.hollysqm.business.studyrecord.service;

import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollysqm.entities.StudyRecord;

@Service("studyRecordService")
public class StudyRecordService {
     
	 @Autowired
	 private ConfigedJdbc configedJdbc;
	
	 /** 
	* @Description:员工学习记录统计
	* @param:@param param
	* @param:@return    设定文件 
	* @return:List<DataRank>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年7月6日
	*/
	public PageResult<StudyRecord> recordCount(Map<String,Object> param,PageResult<StudyRecord> page){
		if(!param.containsKey("sort")){
			param.put("sort", "");
		}
		if(!param.containsKey("order")){
			param.put("order", "");
		}
		System.out.println(param);
		String sqlKey = "velocity.count.studyrecordcount";
		Long lon = configedJdbc.selectCount(sqlKey, param);
		page = configedJdbc.selectPageResult(sqlKey, param, page, StudyRecord.class);
		page.setTotal(lon);
		return page;
	 }
	 
	}
