package com.hollycrm.hollysqm.business.executioninfo.service;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.google.common.collect.Maps;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.interfaces.entities.IUser;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.rest.web.util.WebUtil;
import com.hollycrm.hollybeacon.basic.util.DateUtils;
import com.hollycrm.hollybeacon.basic.util.TimeUtils;
import com.hollycrm.hollybeacon.business.personoa.notice.entities.NoticeInfo;
import com.hollycrm.hollybeacon.business.personoa.notice.entities.NoticeUser;
import com.hollycrm.hollybeacon.business.personoa.notice.service.NoticeUserServiceImpl;
import com.hollycrm.hollybeacon.business.personoa.security.entities.SysUser;
import com.hollycrm.hollybeacon.business.personoa.security.service.UserServiceImpl;
import com.hollycrm.hollybeacon.business.personoa.system.cache.DictionaryCacheHandler;
import com.hollycrm.hollybeacon.business.personoa.system.cache.SysConfigCacheHandler;
import com.hollycrm.hollysqm.entities.DataCoordinate;
import com.hollycrm.hollysqm.entities.DataRank;
import com.hollycrm.hollysqm.entities.DataStatus;

@Service("executionInfoService")
public class ExecutionInfoService {
    
	@Autowired
	private ConfigedJdbc configedJdbc;

	@Resource(name = "userService")
	private UserServiceImpl userService;

	@Resource(name = "noticeUserService")
	private NoticeUserServiceImpl noticeUserService;

	@Resource(name = "sysConfigCacheHandler")
	private SysConfigCacheHandler sysConfigCacheHandler;

	@Resource(name = "dictionaryCacheHandler")
	private DictionaryCacheHandler dictionaryCacheHandler;
	
	/** 
	* @Description:完成情况
	* @param:@param params
	* @param:@return    设定文件 
	* @return:Map<String,Object>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public DataStatus executionInfo(Map<String, Object> params) {
		Integer limit = MapUtils.getInteger(params, "rows", 10);
		PageResult<DataStatus> page = new PageResult<>(limit, 1, false);
		page = configedJdbc.selectPageResult("velocity.count.executionInfoCount", params,page, DataStatus.class);
	    List<DataStatus> list = page.getRows();
	    
		return list.get(0);
	}
	 
	/** 
	* @Description:平均耗时排名
	* @param:@param params
	* @param:@return    设定文件 
	* @return:List<Map<String,Object>>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public List<DataRank> executionInfoRank(Map<String,Object> params){
		Integer limit = MapUtils.getInteger(params, "rows", 10);
		IUser user = WebUtil.getLoginUser();
		// 默认显示当前操作人的统计数据
		params.put("authtokenCode", user.getUserCode());// 用户帐号
		params.put("authtokenName", user.getUserName());// 用户名称
		params.put("authtokenPhoto", user.getPhoto());// 图像ID
		
		PageResult<DataRank> page = new PageResult<>(limit, 1, false);
		page = configedJdbc.selectPageResult("velocity.count.executionInfoRank", params,page,DataRank.class);
		List<DataRank> list = page.getRows();
		
		return list;
	}
	 
	/** 
	* @Description:时间分布统计图
	* @param:@param params
	* @param:@return    设定文件 
	* @return:List<DataCoordinate>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public List<DataCoordinate> timeDistribution(Map<String,Object> params){
		 
		String startTime = MapUtils.getString(params, "startTime");// 开始时间
		String endTime = MapUtils.getString(params, "endTime");// 结束时间
		// DateUtils
		Long lon = TimeUtils.getTimeDiff(endTime, startTime,"yyyy-MM-dd");
		// 开始时间和结束时间相差小于一天 横坐标以小时显示
		if (lon < 1) {
			params.put("time_", "1");
		} else {
			// 开始时间和结束时间相差小于一天 横坐标以年月日显示
			params.put("date_", "1");
		}
		 
		List<Map<String,Object>> list = configedJdbc.selectAllList("velocity.count.executionInfoTimeDistribution", params);
		List<DataCoordinate> newMap = null;
		if (lon < 1) {
			newMap = dealTime_(list);
		} else {
			newMap = dealDate_(list, startTime, endTime);
		}
		return newMap;
	}
	
	/** 
	* @Description:处理以小时为横坐标的数据
	* @param:@param list
	* @param:@return    设定文件 
	* @return:List<DataCoordinate>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	private List<DataCoordinate> dealTime_(List<Map<String,Object>> list){
		String[] strs = new String[]{"00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23"};//24小时
		List<DataCoordinate> newMap = completeNullDate(Arrays.asList(strs), list);
		return newMap;
	}
	
	/** 
	* @Description:处理以日期为横坐标的数据
	* @param:@param list
	* @param:@param startTime
	* @param:@param endTime
	* @param:@return    设定文件 
	* @return:List<DataCoordinate>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	private List<DataCoordinate> dealDate_(List<Map<String,Object>> list,String startTime,String endTime){
		 Date startDate = DateUtils.parse(startTime);
		 Date endDate = DateUtils.parse(endTime);
		 long dayNum = (endDate.getTime() - startDate.getTime())/1000/60/60/24;//开始时间和结束时间相差的天数
		 
		 List<String> strList = Lists.newArrayList();
		 //获取开始时间和结束时间之间所有日期
		 for(int i=0;i<=dayNum;i++){
			 strList.add(DateUtils.format(DateUtils.add(startDate, Calendar.DATE, i)));
		 }

		 List<DataCoordinate> newMap = completeNullDate(strList,list);
		 return newMap;
	}
	
	/** 
	* @Description:补充在时间范围内为空的时间，全部默认数量为0
	* @param:@param list1
	* @param:@param list2
	* @param:@return    设定文件 
	* @return:List<DataCoordinate>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	private List<DataCoordinate> completeNullDate(List<String> list1,List<Map<String,Object>> list2){
		 List<DataCoordinate> newMap = Lists.newArrayList();
		 Map<String,Object> map = null;
		 //循环横坐标节点
		 for(int i=0;i < list1.size();i++){
			 DataCoordinate data = new DataCoordinate();
			 data.setAbscissa(list1.get(i));//横坐标
			 data.setOrdinate(0);//纵坐标
			 //循环统计数据
			 for(int j=0;j<list2.size();j++){//{DATE_=16, COUNT_=3}
				 map = list2.get(j);
				 //横坐标节点是否在统计数据中
				 if(!map.get("DATE_").equals(list1.get(i))){
					 //不存在时默认为0
					 data.setAbscissa(list1.get(i));//横坐标
					 data.setOrdinate(0);//纵坐标
				 }else{
					 data.setAbscissa(MapUtils.getString(map, "DATE_"));//横坐标
					 data.setOrdinate(MapUtils.getInteger(map, "COUNT_"));//纵坐标
					 break;
				 }
			 }
			 newMap.add(data);
		 }
		 return newMap;
	}
	 
	/** 
	* @Description:催一下
	* @param:@param params
	* @param:@return
	* @param:@throws Exception    设定文件 
	* @return:Map<String,Integer>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public Map<String,Integer> urgePaper(Map<String,Object> params) throws Exception{
		 Map<String,Integer> paramMap = Maps.newHashMap();
		 String status = "0,2";
//		 if(!"0".equals(status) && !"2".equals(status)){
//			 throw new Exception("只能催未评分和待审核状态的质检单");
//		 }
		 //获取状态名称
		// String statusName = dictionaryCacheHandler.getDictionaryName("QUALITY_STATUS", status);
		 
		 String userName = WebUtil.getLoginUser().getUserName();
		 String currentTime = DateUtils.getCurrentDateAsString(DateUtils.FORMAT_DATE_YYYY_MM_DD_HH_MM_SS);
		 String currentDate = DateUtils.getCurrentDateAsString();
		 String domainId = WebUtil.getLoginUser().getDomainId();//租户ID
		 String orgId = WebUtil.getLoginUser().getOrgId();//机构ID
		 
		 String title = sysConfigCacheHandler.getSysConfigValue("UrgeNotice", "TITLE");//业务通知标题
		 String content = sysConfigCacheHandler.getSysConfigValue("UrgeNotice", "CONTENT");//业务通知内容
		 
		 List<Map<String,Object>> list = configedJdbc.selectAllList("velocity.count.executionInfoUrge", params);
		 NoticeInfo notice = null;
		 int total = 0;//一共需发送
		 int send = 0;//成功发送
		 int resend = 0;//重复发送
		 for(Map<String,Object> map : list){
			 total = total+1;
			 SysUser user = getUserId(MapUtils.getString(map, "USER_CODE"));
			 Integer total_= MapUtils.getInteger(map, "TOTAL");
			 //是否同一天重复发送业务通知
			 boolean isRe = isCurrentDaySend(status, user.getId(), currentDate);
			 if(isRe){
				 notice = new NoticeInfo();
				 notice.setNoticeType("1");//公告类型:0-故障,1-通告,2-公告
				 notice.setEmergency(NoticeInfo.NOTICE_EMERGENCY_NORMAL);//紧急程度:0:一般,1:紧急,2:非常紧急
				 notice.setTitle(title.replaceAll("\\$\\{status\\}","未质检或待复核"));//通知标题
				 notice.setContent(content.replaceAll("\\$\\{count\\}", String.valueOf(total_)).replaceAll("\\$\\{status\\}", "未质检或待复核"));//通知内容
				 notice.setPublisher(userName);//发布人
				 notice.setPublishTime(currentTime);//发布时间
				 
				 notice.setCreateTime(currentTime);//创建时间
				 notice.setIsImmediately(NoticeInfo.NOTICE_SEND_TYPE_IMMEDIATELY);//发布方式:0:立即发送;1:定时发送
				 notice.setAttentionType(NoticeInfo.NOTICE_ATTENTION_TYPE_SCROLL+","+NoticeInfo.NOTICE_ATTENTION_TYPE_POPUP);//提醒方式,0:滚动,1:弹出
				 notice.setIsDelete(NoticeInfo.NOTICE_STATUS_PUBLISHED);//是否已删除 0:已删除,1:未发布,2:已发布,3:已过期
				 notice.setExpiryTime(currentDate+" 23:59:59");//失效时间   当天有效
				 notice.setDomainId(domainId);
				 notice.setOrgId(orgId);
				 notice.setSendee(user.getUserCode());//收件人帐号
				 notice.setSendeeNames(user.getUserName()+"("+user.getUserCode()+")");//收件人名称
				 notice.setIsSticky("1");//不置顶
				 notice.setStickyTime("1970-01-02 11:59:59");//置顶时间（如果不置顶则设置靠后的时间）
				 noticeUserService.save(notice);
				 
				 NoticeUser noticeUser = new NoticeUser();
				 noticeUser.setCreateTime(currentTime);//创建时间
				 noticeUser.setLastModifyTime(currentTime);//最后修改时间
				 noticeUser.setNoticeType(notice.getNoticeType());//公告类型
				 noticeUser.setNoticeId(notice.getId());//公告ID
				 noticeUser.setPublisher(notice.getPublisher());//发布人帐号
				 noticeUser.setPublishTime(notice.getPublishTime());//发布时间
				 noticeUser.setUserId(user.getId());//发布人ID
				 noticeUser.setRemark(status);//质检单状态
				 //阅读状态.0:未读,1:已读
				 noticeUser.setStatus(NoticeUser.NOTICE_STATUS_UNREAD);//阅读状态.0:未读,1:已读
				 noticeUserService.save(noticeUser);
				 
				 send = send +1;
				 
			 }else{
				 resend = resend +1;
			 }
		 }
		
		 paramMap.put("total", total);
		 paramMap.put("send", send);
		 paramMap.put("resend", resend);
		 return paramMap;
	}
	 
	/** 
	* @Description:通过user_code获取用户信息实体类
	* @param:@param userCode
	* @param:@return    设定文件 
	* @return:SysUser    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public SysUser getUserId(String userCode){
		 
		  SysUser user = userService.findOneBySql("select * from tbl_sys_user t where t.user_code = ?", userCode);
		  return user;
	}
	
	/** 
	* @Description:检验该质检单一天只能发给同一人一次
	* @param:@param status
	* @param:@param userId
	* @param:@param date
	* @param:@return    设定文件 
	* @return:boolean    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public boolean isCurrentDaySend(String status,String userId,String date){
		 List<?> list = configedJdbc.getJdbcTemplate().queryForList("select t.id from TBL_SYS_NOTICE_USER t where t.remark = ? and t.user_id = ? and substr(t.publish_time,1,10) = ?",
				 status,userId,date);
		 return list.isEmpty();
	}
	
}
