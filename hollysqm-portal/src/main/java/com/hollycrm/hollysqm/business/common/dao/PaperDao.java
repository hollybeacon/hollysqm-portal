package com.hollycrm.hollysqm.business.common.dao;

import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.orm.dao.jpa.JpaBaseDaoImpl;
import com.hollycrm.hollysqm.entities.PaperData;

/**
 * 质检单列表Dao
 * @author wangyf
 *
 * 2017年3月23日
 */
@Service("paperDao")
public class PaperDao extends JpaBaseDaoImpl<PaperData, String>{

}
