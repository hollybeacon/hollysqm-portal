package com.hollycrm.hollysqm.business.textsearch.service;

import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.util.DateUtils;
import com.hollycrm.hollybeacon.business.personoa.security.entities.SysUser;
import com.hollycrm.hollysqm.business.textsearch.dao.CollectionDao;
import com.hollycrm.hollysqm.entities.MyCollection;
import com.hollycrm.hollysqm.util.HollysqmUtil;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 我的收藏业务实现类
 * @author wangyf
 *
 * 2017年3月20日
 */
@Service("collectionService")
public class CollectionService {
	
	@Autowired
	private ConfigedJdbc configedJdbc;

	@Resource(name="collectionDao")
	private CollectionDao collectionDao;

	/**
	 * 添加我的收藏
	 * @param param
	 * @throws
	 * @author wangyf
	 */
	public void addCollections(Map<String, Object> param) {
		String createTime = DateUtils.getCurrentDateTimeAsString();
		String createCode = ((SysUser)SecurityUtils.getSubject().getPrincipal()).getUserCode();
		
		//创建时间
		param.put("createTime", createTime);
		//创建人账号
		param.put("createCode", createCode);
		MyCollection collection = new MyCollection();
		HollysqmUtil.fillParam(param, MyCollection.class, collection);	
		collectionDao.save(collection);	
	}

	/**
	 * 删除我的收藏
	 * @param param
	 * @throws
	 * @author wangyf
	 */
	public void delCollections(Map<String, Object> param) {
		collectionDao.delete(param.get("collectId").toString());
	}

	/**
	 * 查询我的收藏列表
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public List<MyCollection> getCollectionList(Map<String, Object> param) {
		String createCode = ((SysUser)SecurityUtils.getSubject().getPrincipal()).getUserCode();
		param.put("createCode", createCode);
		StringBuilder sql = new StringBuilder();
		sql.append("select cust_area custArea, callee, collect_id collectId, name comboName, status qualityStatus,  caller, content words, start_time startTime, satisfaction, ");
		sql.append("end_time endTime, agent_name agent, data_type dataType, ");
		sql.append("create_time createTime, create_code createCode,recoinfo_length_start recoinfoLengthStart,recoinfo_length_end recoinfoLengthEnd ");
		sql.append("from tbl_qm_collect where create_code = :createCode ");
		sql.append("order by create_time desc");
		List<MyCollection> list = configedJdbc.selectPageListBySql(sql.toString(), param, 0, 10, MyCollection.class);
		
		return list;
	}
	
	public MyCollection getCollectionById(Map<String, Object> param) {
		if(com.hollycrm.hollybeacon.basic.util.StringUtils.isNotEmpty(param.get("collectId"))) {
			return collectionDao.get(param.get("collectId").toString());
		} 
		return null;
	}

}
