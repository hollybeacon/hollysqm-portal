package com.hollycrm.hollysqm.business.workcount.service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Maps;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.interfaces.entities.IUser;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.rest.web.util.WebUtil;
import com.hollycrm.hollybeacon.basic.util.DateUtils;
import com.hollycrm.hollysqm.entities.DataRank;
import com.hollycrm.hollysqm.entities.DataStatus;

@Service("workCountService")
public class WorkCountService {
     
	 @Autowired
	 private ConfigedJdbc configedJdbc;
	
	 /** 
	* @Description:工作排名（按评分量）
	* @param:@param param
	* @param:@return    设定文件 
	* @return:List<DataRank>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public List<DataRank> workRank(Map<String,Object> param){
		 IUser user = WebUtil.getLoginUser();
		
		 List<Map<String, Object>> list  = configedJdbc.selectAllList("velocity.count.workRank", initMap(param));
		 List<DataRank> rankList = new ArrayList<DataRank>();
		 int length = list.size();
		 boolean flag = false;
		 boolean flag_= false;
		 int showCount = 10;
		 if(length > showCount){//显示排名总数小于总条数
			 for(int i = 0;i<showCount;i++ ){//优先循环需要展现的排名
				 DataRank rank = new DataRank();
				 Map<String, Object> map = list.get(i);
				 rank.setRank(i+1);
				 rank.setUserCode(MapUtils.getString(map, "MODIFIER"));
				 rank.setTotal(MapUtils.getObject(map, "TOTAL"));
				 rank.setPhoto(MapUtils.getString(map, "PHOTO"));
				 rank.setUserName(MapUtils.getString(map, "USER_NAME"));
				 if(user.getUserCode().equals(MapUtils.getString(map, "MODIFIER"))){
					 flag = true;
				 }
				 rankList.add(rank);
			 }
			 if(!flag){//如果前n名中不存在当前用户,继续循环
				 for(int i = showCount;i<length;i++ ){
					 DataRank rank = new DataRank();
					 Map<String, Object> map = list.get(i);
					 rank.setRank(i+1);
					 rank.setUserCode(MapUtils.getString(map, "MODIFIER"));
					 rank.setTotal(MapUtils.getObject(map, "TOTAL"));
					 rank.setPhoto(MapUtils.getString(map, "PHOTO"));
					 rank.setUserName(MapUtils.getString(map, "USER_NAME"));
					 if(user.getUserCode().equals(MapUtils.getString(map, "MODIFIER"))){
						 flag_ = true;
					 }
					 //循环至当前用户时，保存数据，结束循环
					 if(flag_){
						 rankList.add(rank);
						 break;
					 }
				 }
			 }
			 
		 }else{
			 for(int i = 0;i< length;i++ ){//优先循环需要展现的排名
				 DataRank rank = new DataRank();
				 Map<String, Object> map = list.get(i);
				 rank.setRank(i+1);
				 rank.setUserCode(MapUtils.getString(map, "MODIFIER"));
				 rank.setTotal(MapUtils.getObject(map, "TOTAL"));
				 rank.setPhoto(MapUtils.getString(map, "PHOTO"));
				 rank.setUserName(MapUtils.getString(map, "USER_NAME"));
				 if(user.getUserCode().equals(MapUtils.getString(map, "MODIFIER"))){
					 flag = true;
				 }
				 rankList.add(rank);
			 }
		 }
		 if(!flag && !flag_){//如果两次循环都为发现当前用户的排名则默认
			 DataRank rank = new DataRank();
			 rank.setRank(list.size()+1);
			 rank.setUserCode(user.getUserCode());
			 rank.setTotal(0);
			 rank.setPhoto(user.getPhoto());
			 rank.setUserName(user.getUserName()); 
			 rankList.add(rank);
		 }
		 //List<DataRank> list = page.getRows();
		 return rankList;
	 }
	 
	 /** 
	* @Description:查看自己处理质检情况
	* @param:@param param
	* @param:@return    设定文件 
	* @return:DataStatus    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public DataStatus mySelfWorkCount(Map<String,Object> param){
		String userCode = WebUtil.getLoginUser().getUserCode();
		//每页大小
		Integer limit = MapUtils.getInteger(param,"rows",10);

		param.put("createCode", userCode);
		PageResult<DataStatus> page = new PageResult<>(limit, 1, false);
		page = configedJdbc.selectPageResult("velocity.count.workCount", initMap(param),page,DataStatus.class);
		
		return page.getRows().get(0);
	 }
	 
	 /** 
	* @Description:查看所有质检员平均情况
	* @param:@param param
	* @param:@return    设定文件 
	* @return:DataStatus    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public DataStatus averWorkCount(Map<String,Object> param){
		 List<Map<String,Object>> list = configedJdbc.selectAllList("velocity.count.workCount", initMap(param));
		 //获取质检人数量
		 Long lon = configedJdbc.selectCountBySql("select distinct  c.user_code from TBL_QM_QUALITY_USER c", null);
		 Map<String,Object> map = list.get(0);
		 
		 DataStatus data = new DataStatus();
		 data.setTotal(division(MapUtils.getInteger(map, "TOTAL"),lon));
		 data.setStatus0(division(MapUtils.getInteger(map, "STATUS0"),lon));//未质检
		 data.setStatus1(division(MapUtils.getInteger(map, "STATUS1"),lon));//已质检
		 data.setStatus2(division(MapUtils.getInteger(map, "STATUS2"),lon));//已申诉
		 data.setStatus3(division(MapUtils.getInteger(map, "STATUS3"),lon));//已通过
		 data.setStatus4(division(MapUtils.getInteger(map, "STATUS4"),lon));//已驳回
		 
		 return data;
	 }
	
	 /** 
	* @Description:计算平均数，保留1位小数
	* @param:@param i
	* @param:@param j
	* @param:@return    设定文件 
	* @return:double    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	private double division(Integer i,long j){
		if (j == 0)
			return 0;
		
		double f = (double) i / (double) j;
		BigDecimal b = new BigDecimal(f);
		double f1 = b.setScale(2, BigDecimal.ROUND_HALF_UP).doubleValue();//
		
		return f1;
	 }
	private Map<String,Object> initMap(Map<String,Object> map){
		Integer flag = MapUtils.getInteger(map, "flag");
		String startTime = MapUtils.getString(map, "startTime"),endTime = MapUtils.getString(map, "endTime");
		Map<String,String> timeMap = Maps.newHashMap();
		if(flag == 0){//笨日
			startTime = DateUtils.getCurrentDateAsString();
			endTime = startTime;
		}else if(flag == 1){//本周
			timeMap = getWeekDay();
			startTime = timeMap.get("mon");
			endTime = timeMap.get("sun");
		}else if(flag == 2){//本月
			timeMap = getMonthDate();
			startTime = timeMap.get("monthF");
			endTime = timeMap.get("monthL");
		}
		map.put("startTime", startTime);
		map.put("endTime", endTime);
		return map;
	}
	
	/** 
	* @Description:获取本周的开始时间和结束时间
	* @param:@return    设定文件 
	* @return:Map<String,String>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年4月20日
	*/
	public static Map<String,String> getWeekDay() {
		Map<String, String> map = new HashMap<String, String>();
		Calendar cal = Calendar.getInstance();
		cal.set(Calendar.DAY_OF_WEEK, Calendar.MONDAY); // 获取本周一的日期
		map.put("mon", DateUtils.format(cal.getTime()));
		// 这种输出的是上个星期周日的日期，因为老外那边把周日当成第一天
		cal.set(Calendar.DAY_OF_WEEK, Calendar.SUNDAY);
		// 增加一个星期，才是我们中国人理解的本周日的日期
		cal.add(Calendar.WEEK_OF_YEAR, 1);
		map.put("sun", DateUtils.format(cal.getTime()));
		return map;
	}

	/** 
	* @Description:获取本月的开始时间和结束时间
	* @param:@return    设定文件 
	* @return:Map<String,String>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年4月20日
	*/
	public static Map<String,String> getMonthDate() {
		Map<String, String> map = new HashMap<String, String>();
		// 获取Calendar
		Calendar calendar = Calendar.getInstance();
		// 设置时间,当前时间不用设置
		calendar.set(Calendar.DATE, calendar.getActualMinimum(Calendar.DATE));
		map.put("monthF", DateUtils.format(calendar.getTime()));
		// 设置日期为本月最大日期
		calendar.set(Calendar.DATE, calendar.getActualMaximum(Calendar.DATE));
		map.put("monthL", DateUtils.format(calendar.getTime()));
		return map;
	}
}
