package com.hollycrm.hollysqm.business.textsearch.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollysqm.business.common.dao.PaperDao;
import com.hollycrm.hollysqm.business.common.service.SolrTextSearchService;
import com.hollycrm.hollysqm.core.index.factory.IndexSearchFactory;
import com.hollycrm.hollysqm.core.index.service.IndexSearchService;
import com.hollycrm.hollysqm.core.vo.I8DocBean;
import com.hollycrm.hollysqm.core.vo.V8DocBean;
import com.hollycrm.hollysqm.entities.PaperData;
import com.hollycrm.hollysqm.entities.Record;
import com.hollycrm.hollysqm.util.Constant;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * 全文检索业务处理类
 * @author jianglong
 * @date 2017年4月25日 下午5:42:25
 */
@Service("textSearchService")
public class TextSearchService extends SolrTextSearchService{
	
	@Autowired
	private IndexSearchFactory indexSearchFactory;
	
	@Resource(name="paperDao")
	private PaperDao paperDao;

	/**
	 * 查询语音文本列表
	 * @param param
	 * @return
	 */
	@SuppressWarnings("unchecked")
	public PageResult<Record> getTextResultList(Map<String, Object> param) {		
		PageResult<Record> page = (PageResult<Record>) HollysqmUtil.createPageResult(param);		
		String tag = (String) param.get("dataType");
		// 必须根据数据来源进行查询
		if (StringUtils.isEmpty(tag)) {
			return null;
		}	
		Map<String,String> paramMap = this.initParamMap(tag,param);
		if (paramMap == null){
			return null;
		}
		//有关键词则高亮，否则不高亮
		boolean isHighlight = StringUtils.isNotEmpty(MapUtils.getString(param, "words"));
		String qs =  "*";
		String words = MapUtils.getString(param, "words");
		if (words!=null && !"".equals(words.trim())){
			qs = Constant.allContent + ":" +  (isHighlight ? "(" + words.trim() + ")" : "*");
		}
		
		int count = 0;// 查询记录数		
		List<Record> records = new ArrayList<Record>();
		List<String> agentCodeList = new ArrayList<String>();
		// 根据V8或I8标识返回索引对象
		IndexSearchService indexSearchService = indexSearchFactory.getNewIndexService(tag);
		try {
			count = indexSearchService.count(qs, paramMap);
			page.setTotal(count);
			if (count <= 0)
				return page;
			List<?> list = null;
			if (isHighlight)
				indexSearchService.setHighlightFragsize(180);//设置高亮摘要字符个数
			if (tag.equalsIgnoreCase(Constant.V8)) {// 切换成V8				
				list = indexSearchService.queryResult(qs, paramMap, (int)page.getPageNo(), 
						(int)page.getPageSize(), isHighlight, V8DocBean.class);									
			} else if (tag.equalsIgnoreCase(Constant.I8)) {// 切换成I8
				list = indexSearchService.queryResult(qs, paramMap, (int)page.getPageNo(), 
						(int)page.getPageSize(), isHighlight, I8DocBean.class);				
			} else {
				return null;
			}
			this.loadSolrData(list, tag , records, agentCodeList);
			this.setRecordUser(records, agentCodeList);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		page.setRows(records);
		return page;
	}
	
	/** 
	* @Description:全文检索导出查询方法
	* @param:@param param
	* @param:@return    设定文件 
	* @return:List<Record>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年4月26日
	*/
	public List<Record> exportTextSearch(Map<String, Object> param) {		
		String tag = (String) param.get("dataType");
		// 必须根据数据来源进行查询
		if (StringUtils.isEmpty(tag)) {
			return null;
		}	
		Map<String,String> paramMap = this.initParamMap(tag,param);
		if (paramMap == null){
			return null;
		}
		String qs =  "*";
		String words = MapUtils.getString(param, "words");
		if (words!=null && !"".equals(words.trim())){
			qs =  Constant.allContent + ":" + (StringUtils.isNotEmpty(MapUtils.getString(param, "words")) ? "(" + MapUtils.getString(param, "words").trim() + ")" : "*");
		}
		
		List<Record> records = new ArrayList<Record>();
		List<String> agentCodeList = new ArrayList<String>();
		// 根据V8或I8标识返回索引对象
		IndexSearchService indexSearchService = indexSearchFactory.getNewIndexService(tag);
		List<?> list = null;
		try {
			if (tag.equalsIgnoreCase(Constant.V8)) {// 切换成V8
				list = indexSearchService.queryResult(qs, paramMap ,V8DocBean.class);									
			} else if (tag.equalsIgnoreCase(Constant.I8)) {// 切换成I8
				list = indexSearchService.queryResult(qs, paramMap, I8DocBean.class);				
			} else {
				return null;
			}
			this.loadSolrData(list, tag , records, agentCodeList);
			this.setRecordUser(records, agentCodeList);	
		} catch (Exception e) {
			e.printStackTrace();
		}
		return records;
	}
	
	public PaperData getForward(Map<String, Object> param) {
		PaperData paperData = paperDao.findOne("from PaperData where contactId = :contactId and standardId = :standardId", param);
		return paperData;
	}

}
