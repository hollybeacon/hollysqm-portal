package com.hollycrm.hollysqm.business.itemmanager.rest;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollysqm.business.itemmanager.service.ItemManagerService;
import com.hollycrm.hollysqm.entities.TextItem;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * @author liujr
 * 2017年3月24日
 * @Description 标签管理
 */
@RestService
@RequestMapping("/itemmanager")
public class ItemManagerRest {

	@Resource(name = "itemmanagerService")
	private ItemManagerService  itemmanagerService;
	
	/** 
	* @Description:添加标签
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月24日
	*/
	@RequestMapping(value="/itemManager",method = RequestMethod.POST)
	public ServiceResult addItemManager(TextItem item){
		boolean flag = false;
		String errorMessage = "";
		try {
			String success = itemmanagerService.saveOrUpdateItem(item);
			if ("1".equals(success)){
				errorMessage = "质检标签名称已被使用，不可重复建立！";
			}else if("2".equals(success)){
				errorMessage = "质检计划用到该标签，标签无法停用";
			}else{
				flag = true;
			}
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(null, flag, errorMessage);
	}

	/** 
	* @Description:启用/停用标签
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月24日
	*/
	@RequestMapping(value="/enableItem",method = RequestMethod.POST)
	public ServiceResult enableItem(@RequestParam Map<String,Object> param){
		boolean flag = false;
		String errorMessage = "";
		try {
			flag = itemmanagerService.enableItem(param);
			if(!flag){
				errorMessage = "质检计划用到该标签，标签无法停用";
			}
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(null, flag, errorMessage);
	}
	
	/** 
	* @Description:查询标签
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月24日
	*/
	@RequestMapping(value="/queryItem",method = RequestMethod.GET)
	public ServiceResult queryItem(@RequestParam Map<String,Object> param){
		PageResult<TextItem> page = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			//每页大小
			String limit = (String) param.get("rows");
			if (StringUtils.isEmpty(limit))
				limit = com.hollycrm.hollybeacon.basic.Constants.PAGE_DEFAULT_LIMIT;
			// 开始页
			String start = (String) param.get("page");
			if (StringUtils.isEmpty(start))
				start = "1";
			page = new PageResult<>(Integer.valueOf(limit),Integer.valueOf(start),true);
			page = itemmanagerService.queryItem(page,param);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(page, flag, errorMessage);
	}

	/** 
	* @Description:查询单个标签实例，用语修改标签
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月24日
	*/
	@RequestMapping(value="/modifyItemBefor",method = RequestMethod.GET)
	public ServiceResult modifyItemBefor(@RequestParam String itemId){
		TextItem item = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			item = itemmanagerService.getItem(itemId);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(item, flag, errorMessage);
	}
	
}
