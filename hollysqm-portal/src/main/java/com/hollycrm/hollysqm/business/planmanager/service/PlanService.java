package com.hollycrm.hollysqm.business.planmanager.service;


import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.rest.web.util.WebUtil;
import com.hollycrm.hollybeacon.basic.util.DateUtils;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollybeacon.business.personoa.security.entities.SysUser;
import com.hollycrm.hollysqm.business.common.dao.AgentDao;
import com.hollycrm.hollysqm.business.common.dao.CheckerDao;
import com.hollycrm.hollysqm.business.common.dao.PlanDao;
import com.hollycrm.hollysqm.business.common.dao.PlanParamDao;
import com.hollycrm.hollysqm.business.common.dao.UserDao;
import com.hollycrm.hollysqm.business.common.service.SolrTextSearchService;
import com.hollycrm.hollysqm.business.itemmanager.service.ItemManagerService;
import com.hollycrm.hollysqm.core.index.factory.IndexSearchFactory;
import com.hollycrm.hollysqm.core.index.service.IndexSearchService;
import com.hollycrm.hollysqm.core.vo.I8DocBean;
import com.hollycrm.hollysqm.core.vo.V8DocBean;
import com.hollycrm.hollysqm.entities.Agent;
import com.hollycrm.hollysqm.entities.Checker;
import com.hollycrm.hollysqm.entities.Plan;
import com.hollycrm.hollysqm.entities.PlanCache;
import com.hollycrm.hollysqm.entities.PlanCensus;
import com.hollycrm.hollysqm.entities.PlanLog;
import com.hollycrm.hollysqm.entities.PlanParam;
import com.hollycrm.hollysqm.entities.PlanUserMapping;
import com.hollycrm.hollysqm.entities.Record;
import com.hollycrm.hollysqm.entities.TextItem;
import com.hollycrm.hollysqm.entities.User;
import com.hollycrm.hollysqm.util.Constant;
import com.hollycrm.hollysqm.util.HollysqmUtil;
import com.hollycrm.hollysqm.util.UuidUtil;

/**
 * 查看质检计划执行历史记录业务类
 * @author wangyf
 * @date 2017年4月24日 下午5:23:38
 */
@Service("planService")
@SuppressWarnings("deprecation")
public class PlanService extends SolrTextSearchService{

	@Autowired
	private ConfigedJdbc configedJdbc;
	
	@Resource(name="usersDao")
	private UserDao userDao;
	
	@Resource(name="planDao")
	private PlanDao planDao;
	
	@Resource(name="planParamDao")
	private PlanParamDao planParamDao;
	
	@Resource(name="agentDao")
	private AgentDao agentDao;
	
	@Resource(name="checkerDao")
	private CheckerDao checkerDao;
	
	@Autowired
	private IndexSearchFactory indexSearchFactory;
	
	@Resource(name = "itemmanagerService")
	private ItemManagerService  itemmanagerService;

	/**
	 * 保存或修改缓存中的质检计划
	 * @param cache
	 * @throws
	 * @author wangyf
	 */
	public void saveOrUpdate(PlanCache cache) {
		StringBuilder sql = new StringBuilder();
		String planId = "";
		String domainId = WebUtil.getLoginUser().getDomainId();//租户ID
		String areaId = WebUtil.getLoginUser().getOrgId();//区域ID
		
		Map<String,Object> map = new HashMap<String,Object>();
		map.put("planName", cache.getPlanName().trim());//计划名称
		map.put("standardId", cache.getStandardId());//评价模板id
		map.put("dataType", cache.getDataType());//数据来源
		map.put("status", cache.getStatus());//质检单状态
		map.put("taskTimer", cache.getTaskTimer());//时间周期
		map.put("paramJson", cache.getParamJson());//抽取条件
		map.put("exemption", cache.getExemption());//优秀免检
		map.put("extractMethod", cache.getExtractMethod());//抽取策略
		map.put("textItemId", cache.getTextItemId());//文本标签
		map.put("areaId", areaId);
		map.put("domainId", domainId);
		map.put("isExport", cache.getIsExport());
		
		//当前登录用户
		SysUser loginUser=(SysUser)SecurityUtils.getSubject().getPrincipal();
		map.put("modifier", loginUser.getUserCode());//
		map.put("modifyTime", DateUtils.getCurrentDateTimeAsString());//
		
		//新建质检单
		if(cache.getPlanId() == null || cache.getPlanId().equals("")) {
			sql.append("insert into tbl_qm_plan  ");
			sql.append("(plan_id, plan_name, standard_id, data_type, status, task_timer, modifier, modify_time, area_id, domain_id, is_export) ");
			sql.append("values (:planId, :planName, :standardId, :dataType, :status, :taskTimer, :modifier, :modifyTime, :areaId, :domainId, :isExport) ");
			planId = UuidUtil.getUuid();
			map.put("planId", planId);
			configedJdbc.updateBySql(sql.toString(), map);
			sql.delete(0, sql.length());
			
			//存取计划参数
			sql.append("insert into tbl_qm_plan_param ");
			sql.append("(plan_id, exemption, extract_method, text_item_id, param_json, modifier, modify_time) ");
			sql.append(" values (:planId, :exemption, :extractMethod, :textItemId, :paramJson, :modifier, :modifyTime) ");
			configedJdbc.updateBySql(sql.toString(), map);	
		} else {//修改计划
			planId = cache.getPlanId();
			map.put("planId", planId);
			sql.append("update tbl_qm_plan set plan_id = :planId, plan_name = :planName, ");
			sql.append("standard_id = :standardId, data_type = :dataType, status = :status, task_timer = :taskTimer, area_id = :areaId, domain_id = :domainId ");
			sql.append(" , modifier = :modifier,modify_time = :modifyTime, is_export = :isExport ");
			sql.append("where plan_id = :planId");
			configedJdbc.updateBySql(sql.toString(), map);
			
			//更新计划参数
			sql.delete(0, sql.length());
			sql.append("update tbl_qm_plan_param set  ");
			sql.append("exemption = :exemption, extract_method = :extractMethod, text_item_id= :textItemId, param_json = :paramJson, modifier = :modifier, modify_time = :modifyTime ");
			sql.append("where plan_id = :planId");
			configedJdbc.updateBySql(sql.toString(), map);
			
			if(cache.getIsExport().equals("0")) {
				//删除计划人员信息
				sql = sql.delete(0, sql.length());
				sql.append("delete from  tbl_qm_plan_user where plan_id = :planId");
				configedJdbc.updateBySql(sql.toString(), map);
				
				sql.delete(0, sql.length());
				sql.append("delete from tbl_qm_quality_user where plan_id = :planId");
				configedJdbc.updateBySql(sql.toString(), map);
			} else {
				sql = sql.delete(0, sql.length());
				sql.append("delete from tbl_qm_plan_user_mapping where plan_id :planId");
			}
			
			
		}	
		sql.delete(0, sql.length());
		
		if(cache.getIsExport().equals("0")) {
			//存取计划人员（坐席和质检员）
			for(User agent : cache.getAgents()) {
				sql.append("insert into tbl_qm_plan_user values (");
				sql.append("'" + planId + "'");
				if(StringUtils.isNotEmpty(agent.getUserCode())) {
					sql.append(", '" + agent.getUserCode() + "'");
				} else {
					sql.append(", null");
				}
				if(StringUtils.isNotEmpty(agent.getAgentCode())) {
					sql.append(", '" + agent.getAgentCode() + "'");
				} else {
					sql.append(", null");
				}
				sql.append(")");
				configedJdbc.updateBySql(sql.toString());
				sql.delete(0, sql.length());
			}
			
			for(User check : cache.getCheckers()) {
				sql.append("insert into tbl_qm_quality_user values (");
				sql.append("'" + planId + "'");
				if(StringUtils.isNotEmpty(check.getUserCode())) {
					sql.append(", '" + check.getUserCode() + "'");
				} else {
					sql.append(", null");
				}
				if(StringUtils.isNotEmpty(check.getAgentCode())) {
					sql.append(", '" + check.getAgentCode() + "'");
				} else {
					sql.append(", null");
				}
				sql.append(")");
				configedJdbc.updateBySql(sql.toString());
				sql.delete(0, sql.length());
			}
		} else {
			sql.append("delete from tbl_qm_plan_user_mapping where plan_id = '" + planId + "'");
			configedJdbc.updateBySql(sql.toString());
			sql.delete(0, sql.length());
			
			List<PlanUserMapping> list = cache.getUserMsg();
			Map<String, ?>[] maps = new Map[list.size()];
			if(list != null && list.size() > 0) {
				for(int i=0; i<list.size(); i++) {
					PlanUserMapping userMsg = list.get(i);
					HashMap<String,Object> param = new HashMap<String,Object>();
					param.put("planId", planId);
					param.put("agentCode", userMsg.getAgentCode());
					param.put("checkerAgentCode", userMsg.getCheckerAgentCode());
					param.put("extractNumber", userMsg.getExtractNumber());
					param.put("rank", userMsg.getRank());
					maps[i] = param;
				}
				
				sql.append("insert into tbl_qm_plan_user_mapping values(:planId, :agentCode, :checkerAgentCode, :extractNumber, :rank)");
				configedJdbc.batchUpdateBySql(sql.toString(), maps);
			}
		}
		
		
	}

	/**
	 * 查询指定ID的质检计划
	 * @param planId
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public PlanCache getPlanDetailById(String planId) {
		if(StringUtils.isEmpty(planId)) {
			return null;
		}
		
		StringBuilder sql = new StringBuilder();
		Map<String,Object> map = new HashMap<String,Object>();
		sql.append("select t1.plan_id planId, t1.plan_name planName, t1.standard_id standardId, t1.data_type dataType, ");
		sql.append("t1.status, t1.task_timer taskTimer, t1.is_export isExport, ");
		sql.append("t2.exemption, t2.extract_method extractMethod, ");
		sql.append("t2.text_item_id textItemId, t2.param_json paramJson from tbl_qm_plan t1 left join tbl_qm_plan_param t2 ");
		sql.append("on t1.plan_id = t2.plan_id where t1.plan_id = :planId ");
		map.put("planId",planId);
		List<PlanCache> list = configedJdbc.queryForList(sql.toString(), map, PlanCache.class);
		
		if(list != null && list.size() > 0) {
			return list.get(0);
		} else {
			return null;
		}	
	}

	/**
	 * 查询质检计划的执行日志
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public PageResult<PlanLog> getPlanLogList(Map<String, Object> param) {
		@SuppressWarnings("unchecked")
		PageResult<PlanLog> page = (PageResult<PlanLog>)HollysqmUtil.createPageResult(param);	
		int offset = (int) ((page.getPageNo() - 1) * (page.getPageSize()));
		int limit = page.getPageSize();
		StringBuilder sql = new StringBuilder();
		sql.append("select t.start_time, t.end_time, t.paper_count from tbl_qm_plan_log t ");
		
		if(StringUtils.isEmpty(param.get("planId"))) {//计划ID
			return null;
		}
		sql.append("where t.plan_id = :planId ");
		
		if(StringUtils.isNotEmpty(param.get("startTime"))) {//开始时间
			param.put("startTime", param.get("startTime")+" 00:00:00");
			sql.append(" and t.start_time >= :startTime ");
		}
		
		if(StringUtils.isNotEmpty(param.get("endTime"))) {//结束时间
			param.put("endTime", param.get("endTime")+" 23:59:59");
			sql.append(" and t.end_time <= :endTime ");
		}
		sql.append(" order by t.start_time desc ");
		
		List<PlanLog> list = configedJdbc.selectPageListBySql(sql.toString(), param, offset, limit, PlanLog.class);
		Long lon = 
				configedJdbc.selectCountBySql(sql.toString(), param);
		page.setTotal(lon);
		page.setRows(list);
		
		return page;
		
	}

	/**
	 * 获取质检计划列表
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public PageResult<Plan> getPlanList(Map<String, Object> param) {
		@SuppressWarnings("unchecked")
		PageResult<Plan> page = (PageResult<Plan>)HollysqmUtil.createPageResult(param);	
		int offset = (int) ((page.getPageNo() - 1) * (page.getPageSize()));
		int limit = page.getPageSize();
		
		StringBuilder sql = new StringBuilder();
		sql.append("select t.plan_id planId, t.data_type dataType, t.plan_name planName, t.status, t.modifier, t1.user_name modifierName, t.modify_time modifyTime from tbl_qm_plan t left join tbl_sys_user t1 on t.modifier = t1.user_code where t.plan_id <> '1' ");
		if(StringUtils.isNotEmpty(param.get("dataType"))) {//数据来源
			sql.append(" and t.data_type = :dataType ");
		}
		
		if(StringUtils.isNotEmpty(param.get("planName"))) {//计划名称
			sql.append(" and t.plan_name like '%"+ param.get("planName").toString() + "%' " );
		}
		
		if(StringUtils.isNotEmpty(param.get("status"))) {//计划状态
			sql.append(" and t.status = :status ");
		}
		
		sql.append("order by t.modify_time desc");
			
		List<Plan> list = configedJdbc.selectPageListBySql(sql.toString(), param, offset, limit, Plan.class);
		Long lon = 
				configedJdbc.selectCountBySql(sql.toString(), param);
		page.setTotal(lon);
		page.setRows(list);
		
		return page;
	}
	
	
	/**
	 * 获取质检统计情况
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public PageResult<PlanCensus> getPlanCensus(Map<String, Object> param) {
		@SuppressWarnings("unchecked")
		PageResult<PlanCensus> page = (PageResult<PlanCensus>)HollysqmUtil.createPageResult(param);	
		int offset = (int) ((page.getPageNo() - 1) * (page.getPageSize()));
		int limit = page.getPageSize();
		if(StringUtils.isEmpty(param.get("planId"))) {
			return null;
		}
		//判断当前计划抽取类型
		Plan plan = planDao.get(param.get("planId").toString());
		PlanParam planparam = planParamDao.get(param.get("planId").toString());
		String paramJson = planparam.getParamJson();
		ObjectMapper objMapper = new ObjectMapper();
		Map<String,Object> paramMap = null;
		try {
			paramMap = objMapper.readValue(paramJson, Map.class);
		} catch (Exception e) {
			e.printStackTrace();
		} 
		String startTime = paramMap.get("startTime") == null ? "" : paramMap.get("startTime").toString().substring(0, 8);
		String endTime = paramMap.get("endTime") == null ? "" : paramMap.get("endTime").toString().substring(0,8);
		SimpleDateFormat sd = new SimpleDateFormat("yyyyMMdd");
		long days = 0;
		try {
			days = (sd.parse(endTime).getTime() - sd.parse(startTime).getTime()) / (3600 * 1000 * 24)  + 1;
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		StringBuilder sql = new StringBuilder();
		if(plan.getIsExport().equals("0")) {//非导入
			sql.append("select plan_name planName, agent_code agentCode, is_export isExport, planExtractNum, hasExtractNum, (planExtractNum - hasExtractNum) lastExtractNum ");
			sql.append(" from (select t1.plan_name, t2.agent_code, t1.is_export,  ");
			if(planparam.getExtractMethod().startsWith("M")) {
				//周期抽取
				sql.append("CAST(substr(t3.extract_method, 3) AS SIGNED)  ");
				
			} else if(planparam.getExtractMethod().startsWith("D")) {
				//每日抽取
				sql.append("CAST(substr(t3.extract_method, 3) AS SIGNED) *  " + days + " ");
			}
			sql.append("planExtractNum, (select count(1) from tbl_qm_paper  where agent_code = t2.agent_code ");
			sql.append("and plan_id = t1.plan_id) hasExtractNum  from tbl_qm_plan t1 ");
			sql.append(" left join tbl_qm_plan_user t2 on t1.plan_id = t2.plan_id  left join tbl_qm_plan_param t3 ");
			sql.append("on t1.plan_id = t3.plan_id where t1.plan_id =  :planId ) tb");
			
		} else {
			sql.append("select plan_name planName, agent_code agentCode, checker_agent_code checkerAgentCode, extract_number planExtractNum, ");
			sql.append("hasExtractNum, (extract_number - hasExtractNum) lastExtractNum from ( ");
			sql.append("select t1.plan_name, t2.agent_code, t2.checker_agent_code, ");
			if(planparam.getExtractMethod().startsWith("M")) {//周期抽取
				//周期抽取
				sql.append("t2.extract_number, ");
				
			} else if(planparam.getExtractMethod().startsWith("D")) {//每天抽取
				//每日抽取
				sql.append("(t2.extract_number * " + days + ") extract_number, ");
			}
			sql.append("(select count(1) from tbl_qm_paper where agent_code = t2.agent_code and ");
			sql.append("plan_id = t1.plan_id and checker_agent_code = t2.checker_agent_code) hasExtractNum ");
			sql.append("from tbl_qm_plan t1 left join tbl_qm_plan_user_mapping t2 on t1.plan_id = t2.plan_id ");
			sql.append("where t1.plan_id = :planId) tb");
		}
		
		
			
		List<PlanCensus> list = configedJdbc.selectPageListBySql(sql.toString(), param, offset, limit, PlanCensus.class);
		Long lon = 
				configedJdbc.selectCountBySql(sql.toString(), param);
		page.setTotal(lon);
		page.setRows(list);
		
		return page;
	}

	/**
	 * 根据用户账号查找用户信息
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public List<User> getUsers(Map<String, Object> param) {
		StringBuilder sql = new StringBuilder();
		sql.append("select t1.user_code userCode, t1.user_name username, t1.user_no agentCode, t1.dept_id department, t2.org_name deptName from tbl_sys_user t1 left join tbl_sys_org t2 on t1.dept_id = t2.org_id where 1=1 ");
		sql.append("and t1.user_code in ");
		
		if(StringUtils.isNotEmpty(param.get("agent"))) {//坐席
			sql.append(HollysqmUtil.paramParseQueryText(param.get("agent").toString(), "sql", true));
		} else {
			sql.append(HollysqmUtil.paramParseQueryText(param.get("checker").toString(), "sql", true));
		}
		List<User> list = configedJdbc.queryForList(sql.toString(), param, User.class);
		
		return list;
	}
	
	/**
	 * 根据用户账号查找用户信息
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public List<User> getUserMappingDetail(String agents) {
		StringBuilder sql = new StringBuilder();
		sql.append("select t1.user_code userCode, t1.user_name username, t1.user_no agentCode, t1.dept_id department, t2.org_name deptName from tbl_sys_user t1 left join tbl_sys_org t2 on t1.dept_id = t2.org_id where 1=1 ");
		sql.append("and t1.user_no in ");
		sql.append(HollysqmUtil.paramParseQueryText(agents, "sql", true));
		
		List<User> list = configedJdbc.queryForList(sql.toString(), new HashMap<String,Object>(), User.class);
		
		return list;
	}

	
	/**
	 * 查找质检计划的质检员和坐席信息
	 * @param planId
	 * @param userType
	 * @return
	 * @throws
	 * @author wangyf
	 */
	public List<User> getUsers(String planId, String userType) {
		Map<String,Object> param = new HashMap<String,Object>();
		String tabName = "";
		if(StringUtils.isEmpty(planId) || StringUtils.isEmpty(userType)) {
			return null;
		}
		
		if(userType.equals("agent")) {
			tabName = "tbl_qm_plan_user";
		} else {
			tabName = "tbl_qm_quality_user";
		}
		param.put("planId", planId);//计划ID
		param.put("tabName", tabName);//表名称
		StringBuilder sql = new StringBuilder();
		sql.append("select t1.user_code userCode, t1.user_name username, t1.user_no agentCode, t1.dept_id department, t3.org_name deptName from tbl_sys_user t1 join " + tabName + " t2 ");
		sql.append("on t1.user_code = t2.user_code left join tbl_sys_org t3 on t1.dept_id = t3.org_id where t2.plan_id = :planId");
		List<User> list = configedJdbc.queryForList(sql.toString(), param, User.class);
		
		return list;
	}

	/**
	 * 更新质检计划状态
	 * @param param
	 * @throws
	 * @author wangyf
	 */
	public void switchPlan(Map<String, Object> param) {
		StringBuilder sql = new StringBuilder();
		sql.append("update tbl_qm_plan set status = :status where plan_id = :planId");
		configedJdbc.updateBySql(sql.toString(), param);
	}

	/**
	 * 查询预览质检计划的接触记录
	 * @param param
	 * @return
	 */
	public List<Record> getPlanRecords(Map<String, Object> param) {
		if(StringUtils.isEmpty(param.get("dataType"))) {
			return null;
		}
		//根据数据来源决定V8或I8索引对象
		String tag = param.get("dataType").toString();
		Map<String,String> paramMap = this.initParamMap(tag,param);
		//Map<String,String> paramMap = new HashMap<String,String>();
		
		String words = "*";
		String wordType = null;
		String txtObj = Constant.txtContent;
		
		//如果绑定了标签，就用标签的内容替代文本
		if(StringUtils.isNotEmpty(param.get("textItemId"))) {
			TextItem textItem = itemmanagerService.getItem(param.get("textItemId").toString());
			words = textItem.getContent();
			wordType = textItem.getItemType();
		}else if (StringUtils.isNotEmpty(MapUtils.getString(param, "words"))){
			words = MapUtils.getString(param, "words").trim();
			wordType = MapUtils.getString(param, "wordType");
		}
		
		//角色：0所有、1坐席、2用户	
		if ("1".equals(wordType)){
			txtObj = Constant.txtContentAgent;
		} else if ("2".equals(wordType)){
			txtObj = Constant.txtContentUser;
		}
		
		String qs =  String.format("%s:(%s)", txtObj,words); 
		List<Record> records = new ArrayList<Record>();
		List<String> userCodeList = new ArrayList<String>();
		// 根据V8或I8标识返回索引对象
		IndexSearchService indexSearchService = indexSearchFactory.getNewIndexService(tag);
		List<?> list = null;
		try {
			if (tag.equalsIgnoreCase(Constant.V8)) {// 切换成V8
				list = indexSearchService.queryResult(qs, paramMap, 1, 
						5, false, V8DocBean.class);									
			} else if (tag.equalsIgnoreCase(Constant.I8)) {// 切换成I8
				list = indexSearchService.queryResult(qs, paramMap, 1, 
						5, false, I8DocBean.class);				
			} else {
				return null;
			}
			this.loadSolrData(list, tag , records, userCodeList);
			this.setRecordUser(records, userCodeList);		
		} catch (Exception e) {
			e.printStackTrace();
		}
		return records;
	}
	
	/**
	 * 判断质检计划是否重名
	 * @param param
	 * @return
	 */
	public boolean validatePlanName(Map<String, Object> param, PlanCache cache) {
		if(StringUtils.isEmpty(param.get("planName").toString().trim())) {
			return false;
		} else {
			param.put("planName", param.get("planName").toString().trim());
			String sql = "select * from tbl_qm_plan where plan_name = :planName";
			if(StringUtils.isNotEmpty(cache.getPlanId())) {
				param.put("planId", cache.getPlanId());
				sql += " and plan_id <> :planId";
			}
			long count = configedJdbc.selectCountBySql(sql, param);
			if(count == 0) {
				return true;
			} else {
				return false;
			}
			
		}
	}
	
	/**
	 * 复制计划 
	 * @param param
	 * @return
	 */
	public boolean copyPlan(Map<String, Object> param) {
		if(StringUtils.isEmpty(param.get("planId").toString())) {
			return false;
		} else {
			String planId = param.get("planId").toString();
			String planName = param.get("planName").toString();
			SysUser loginUser=(SysUser)SecurityUtils.getSubject().getPrincipal();
			String modifier = loginUser.getUserCode();
			String modifierName = loginUser.getUserName();
			String modifyTime = DateUtils.getCurrentDateTimeAsString();
			Plan plan = planDao.get(planId);
			Plan copyPlan = new Plan();
			copyPlan.setPlanId(UuidUtil.getUuid());
			copyPlan.setAreaId(plan.getAreaId());
			copyPlan.setCheckerScore(plan.getCheckerScore());
			copyPlan.setDataType(plan.getDataType());
			copyPlan.setDomainId(plan.getDomainId());
			copyPlan.setIsValid(plan.getIsValid());
			copyPlan.setModifier(modifier);
			copyPlan.setModifierName(modifierName);
			copyPlan.setModifyTime(modifyTime);
			copyPlan.setPassScore(plan.getPassScore());
			copyPlan.setRemark(plan.getRemark());
			copyPlan.setStandardId(plan.getStandardId());
			copyPlan.setStatus("0");
			copyPlan.setTaskTimer(plan.getTaskTimer());
			copyPlan.setIsExport(plan.getIsExport());
			
			if(StringUtils.isEmpty(planName)) {
				planName = plan.getPlanName() + "副本";
			}
			copyPlan.setPlanName(planName);
			planDao.save(copyPlan);
			
			PlanParam planParam = planParamDao.get(planId);
			PlanParam copyParam = new PlanParam();
			
			copyParam.setPlanId(copyPlan.getPlanId());
			copyParam.setExemption(planParam.getExemption());
			copyParam.setExtractMethod(planParam.getExtractMethod());
			copyParam.setModifier(modifier);
			copyParam.setModifyTime(modifyTime);
			copyParam.setParamJson(planParam.getParamJson());
			copyParam.setTextitemId(planParam.getTextitemId());
			
			planParamDao.save(copyParam);
			String sql = "";
			if(plan.getIsExport().equals("1")) {
				List<PlanUserMapping> mappings = configedJdbc.queryForList("select agent_code agentCode, checker_agent_code checkerAgentCode, extract_number extractNumber, rank from tbl_qm_plan_user_mapping where plan_id = :planId", param, PlanUserMapping.class);
				for(PlanUserMapping userMapping : mappings) {
					sql = "insert into tbl_qm_plan_user_mapping values('" + copyPlan.getPlanId() + "','" + userMapping.getAgentCode() + "','" + userMapping.getCheckerAgentCode() + "'," + userMapping.getExtractNumber() + ",'" + userMapping.getRank() + "')";
					configedJdbc.updateBySql(sql);
				}
			} else {
				List<Agent> agents = configedJdbc.queryForList("select plan_id planId, user_code userCode, agent_code agentCode from tbl_qm_plan_user where plan_id = :planId ", param, Agent.class);
				
				for(Agent agent: agents) {
					sql = "insert into tbl_qm_plan_user values('"+copyPlan.getPlanId() +"','" + agent.getUserCode() + "','" + agent.getAgentCode() + "')";
					configedJdbc.updateBySql(sql);
				}
				
				
				List<Checker> checkers = configedJdbc.queryForList("select plan_id planId, user_code userCode, agent_code agentCode from tbl_qm_quality_user where plan_id = :planId", param, Checker.class);
				
				for(Checker checker: checkers) {
					sql = "insert into tbl_qm_quality_user values('"+copyPlan.getPlanId() +"','" + checker.getUserCode() + "','" + checker.getAgentCode() + "')";
					configedJdbc.updateBySql(sql);
				}
			}
			
			return true;
		}
		
	}

	/**
	 * 查询质检计划绑定人员的同组人员信息
	 * @param param
	 * @return
	 */
	public List<User> getPlanRelevanceUser(Map<String, Object> param) {
		if(StringUtils.isEmpty(param.get("planId"))) {
			return null;
		}
		
		if(StringUtils.isEmpty(param.get("type"))) {
			return null;
		}
		
		Plan plan = planDao.get(param.get("planId").toString());
		
		StringBuilder sql = new StringBuilder();
		sql.append("select t1.dept_id   department, ");
		sql.append("t1.user_name userName, ");
		sql.append("t1.user_code userCode, ");
		sql.append("t1.user_no   agentCode, ");
		sql.append("t2.org_name  deptName ");
		sql.append("from tbl_sys_user t1 ");
		sql.append("left join tbl_sys_org t2 ");
		sql.append("on t1.dept_id = t2.org_id ");
		sql.append("where t1.dept_id in ");
		sql.append("(select distinct (dept_id) ");
		sql.append("from tbl_sys_user ");
		sql.append("where user_no in ");
		
		if(plan.getIsExport().equals("1")) {
			if(param.get("type").toString().equals("agent")) {
				sql.append("(select agent_code from tbl_qm_plan_user_mapping ");
			}else {
				sql.append("(select checker_agent_code from tbl_qm_plan_user_mapping ");
			}
		} else {
			sql.append("(select agent_code ");
			if(param.get("type").toString().equals("agent")) {
				sql.append("from tbl_qm_plan_user ");
			}else {
				sql.append("from tbl_qm_quality_user ");
			}
		}
		
		sql.append("where plan_id = :planId))");
		
		List<User> list = configedJdbc.queryForList(sql.toString(), param, User.class);
		return list;
	}

	/**
	 * 获取计划人员映射和配额
	 * @param planId
	 * @return
	 */
	public List<PlanUserMapping> getPlanUserMapping(String planId) {
		Map<String,Object> param = new HashMap<String,Object>();
		param.put("planId", planId);
		StringBuilder sql = new StringBuilder();
		sql.append("select t1.agent_code agentCode, t1.checker_agent_code checkerAgentCode, t1.extract_number extractNumber, t1.rank, t2.user_code userCode, t2.user_name agentName, t3.user_code checkerCode, t3.user_name checkerName  ");
		sql.append("from tbl_qm_plan_user_mapping t1 left join tbl_sys_user t2 on t1.agent_code = t2.user_no ");
		sql.append("left join tbl_sys_user t3 on t1.checker_agent_code = t3.user_no ");
		sql.append("where t1.plan_id = :planId");
		
		List<PlanUserMapping> list = configedJdbc.queryForList(sql.toString(), param, PlanUserMapping.class);
		return list;
	}

	
	
	public boolean editPlanUser(Map<String, Object> param) {
		if(StringUtils.isEmpty(param.get("planId"))) {
			return false;
		}
		String planId = param.get("planId").toString();
		configedJdbc.updateBySql("delete from tbl_qm_plan_user where plan_id = '" + planId + "'");
		configedJdbc.updateBySql("delete from tbl_qm_quality_user where plan_id = '" + planId + "'");
		
		HashMap<String,Object> param1 = new HashMap<String,Object>();
		HashMap<String,Object> param2 = new HashMap<String,Object>();
		param1.put("agent", param.get("agents"));
		param2.put("checker", param.get("checkers"));
		List<User> agents = this.getUsers(param1);
		List<User> checkers = this.getUsers(param2);
		
		StringBuilder sql = new StringBuilder();
		//存取计划人员（坐席和质检员）
		for(User agent : agents) {
			sql.append("insert into tbl_qm_plan_user values (");
			sql.append("'" + planId + "'");
			if(StringUtils.isNotEmpty(agent.getUserCode())) {
				sql.append(", '" + agent.getUserCode() + "'");
			} else {
				sql.append(", null");
			}
			if(StringUtils.isNotEmpty(agent.getAgentCode())) {
				sql.append(", '" + agent.getAgentCode() + "'");
			} else {
				sql.append(", null");
			}
			sql.append(")");
			configedJdbc.updateBySql(sql.toString());
			sql.delete(0, sql.length());
		}
		
		for(User check : checkers) {
			sql.append("insert into tbl_qm_quality_user values (");
			sql.append("'" + planId + "'");
			if(StringUtils.isNotEmpty(check.getUserCode())) {
				sql.append(", '" + check.getUserCode() + "'");
			} else {
				sql.append(", null");
			}
			if(StringUtils.isNotEmpty(check.getAgentCode())) {
				sql.append(", '" + check.getAgentCode() + "'");
			} else {
				sql.append(", null");
			}
			sql.append(")");
			configedJdbc.updateBySql(sql.toString());
			sql.delete(0, sql.length());
		}
		return true;
	}

	public Map<String,Object> getPlanExtractDetail(Map<String, Object> param) {
		if(StringUtils.isEmpty(param.get("planId"))) {
			return null;
		}
		Plan plan = planDao.get(param.get("planId").toString());
		boolean isExport = false;
		if(plan.getIsExport().equals("1")) {
			isExport = true;
		}
		StringBuilder sql = new StringBuilder();
		if(isExport) {
//			sql.append("with tt as (select user_no, extract_number, dept_id ");
//			sql.append("from tbl_qm_plan_user_mapping t1 ");
			
			sql.append("select user_no, extract_number, dept_id from tbl_qm_plan_user_mapping t1 ");
		}else {
//			sql.append("with tt as (select user_no, dept_id ");
//			sql.append("from tbl_qm_plan_user t1 ");
			
			sql.append("select user_no, dept_id from tbl_qm_plan_user t1 ");
		}
		
		sql.append("left join tbl_sys_user t2 ");
		sql.append("on t1.agent_code = t2.user_no ");
		sql.append("where plan_id = :planId) ");
		
		if(isExport) {
//			sql.append("select (select sum(extract_number) from tt) extractNum, ");			
			sql = new StringBuilder("select sum(extract_number) EXTRACTNUM,count(distinct (user_no)) EXTRACTUSERNUM,count(distinct (dept_id)) EXTRACTDEPTNUM from ( ").append(sql).append(" t");
		}else {
//			sql.append("select  ");
			sql = new StringBuilder("select count(distinct (user_no)) EXTRACTUSERNUM,count(distinct (dept_id)) EXTRACTDEPTNUM from ( ").append(sql).append(" t");
		}
		
//		sql.append("(select count(1) from (select distinct (user_no) from tt group by user_no)) extractUserNum, ");
//		sql.append("(select count(1) from (select distinct (dept_id) from tt group by dept_id)) extractDeptNum ");
//		sql.append("from dual ");
		Map<String,Object> map = configedJdbc.queryForMap(sql.toString(), param);
		return map;
		
	}

	
}
