package com.hollycrm.hollysqm.business.unchecked.rest;

import java.util.Map;
import javax.annotation.Resource;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollysqm.business.unchecked.service.UncheckedService;
import com.hollycrm.hollysqm.entities.Paper;
import com.hollycrm.hollysqm.entities.UnsolvedPaper;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * 未复核质检单
 * @author wangyf
 *
 * 2017年3月15日
 */
@RestService
@RequestMapping(value="uncheckedRest")
public class UncheckedRest {

	@Resource(name="uncheckedService")
	private UncheckedService uncheckedService;
	
	/**
	 * 查询未复核质检单列表
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	@RequestMapping(value="/getUncheckedPaperList",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getUncheckedPaperList(@RequestParam Map<String,Object> param) {
		PageResult<Paper> page = uncheckedService.getUncheckedPaperList(param);
		
		return HollysqmUtil.wrapUpPageResult(page, true, "");	
	}
	
	/**
	 * 获取未复核质检单信息
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	@RequestMapping(value="/getUncheckedPaperMsg",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getUncheckedPaperMsg(@RequestParam Map<String,Object> param) {
		UnsolvedPaper paperMsg = uncheckedService.getUncheckedPaperMsg(param);
		
		return HollysqmUtil.wrapUpPageResult(paperMsg, true, "");
	}
	
}
