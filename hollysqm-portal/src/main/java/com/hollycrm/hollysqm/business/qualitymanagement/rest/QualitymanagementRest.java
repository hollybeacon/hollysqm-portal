package com.hollycrm.hollysqm.business.qualitymanagement.rest;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollysqm.business.qualitymanagement.service.QualitymanagementService;
import com.hollycrm.hollysqm.entities.Paper;
import com.hollycrm.hollysqm.entities.PaperAppeall;
import com.hollycrm.hollysqm.entities.UnsolvedPaper;
import com.hollycrm.hollysqm.util.HollysqmUtil;

@RestService
@RequestMapping(value="qualitymanagement")
public class QualitymanagementRest {

	@Resource(name="qualitymanagementService")
	private QualitymanagementService qualitymanagementService;

	@RequestMapping(value = "/qualitymanagement", method = RequestMethod.GET)
	public ModelAndView qualitymanagement(@RequestParam Map<String,Object> param) throws Exception{
		String deptIds = qualitymanagementService.getDeptIds(param);
		param.put("deptId", deptIds);
		return new ModelAndView("forward:/hollysqm/qualitymanagement/page/qualitymanagement.jsp", param);
	}
	
	@RequestMapping(value="/getQualitymanagementList",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getQualitymanagementList(@RequestParam Map<String,Object> param) {
		PageResult<Paper> page = qualitymanagementService.qualitymanagementList(param);
		
		return HollysqmUtil.wrapUpPageResult(page, true, "");	
	}
	
	@RequestMapping(value="/nextQualitymanagement",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult nextQualitymanagement(@RequestParam Map<String,Object> param) {
		UnsolvedPaper paperMsg = qualitymanagementService.nextQualitymanagement(param);
		
		return HollysqmUtil.wrapUpPageResult(paperMsg, true, "");
	}

	@RequestMapping(value="/getQualitymanagement",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getQualitymanagement(@RequestParam Map<String,Object> param) {
		String id = MapUtils.getString(param, "appealId");
		PaperAppeall paper = qualitymanagementService.get(id);
		
		return HollysqmUtil.wrapUpPageResult(paper, true, "");
	}

	@RequestMapping(value="/getQualitymanagementPaper",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getQualitymanagementPaper(@RequestParam Map<String,Object> param) {
		//PageResult<PaperAppeall> page = new PageResult<PaperAppeall>(100,1,false);
		List<PaperAppeall> paper = qualitymanagementService.paperAppealList(param);
		//page.setRows(paper);
		//page.setTotal(paper.size());
		return HollysqmUtil.wrapUpPageResult(paper, true, "");
	}
}
