package com.hollycrm.hollysqm.business.scorelist.rest;

import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.orm.service.jpa.JpaBaseServiceImpl;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollybeacon.basic.rest.base.BaseDatagridRest;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollysqm.business.scorelist.service.ScoreListService;
import com.hollycrm.hollysqm.entities.ScoreList;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * @author liujr
 * 2017年6月16日
 * @Description 评分项页面操作
 */
@RestService
@RequestMapping(value="/scoreList")
public class ScoreListRest extends BaseDatagridRest<ScoreList>{
	
	@Resource(name = "scoreListService")
	private ScoreListService scoreListService;
	
	@Override
	public JpaBaseServiceImpl<ScoreList, String> getService() {
		return scoreListService;
	}

	/** 
	* @Description:检查该评分项是否被使用
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年6月16日
	*/
	@RequestMapping(value="/checkScoreList",method = RequestMethod.GET)
	public ServiceResult checkScoreList(@RequestParam Map<String, Object> param) {
		boolean check = false;
		boolean flag = false;
		String errorMessage = "";
		try {
			check = scoreListService.checkScoreList(MapUtils.getString(param, "id"));
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(check, flag, errorMessage);
	}

	@Override
	protected StringBuffer getPageHql(Map<String, Object> param)
			throws InstantiationException, IllegalAccessException {
		StringBuffer sb = getService().getPageBaseHql();
		sb.append(" where obj.scoreListId is not null ");
		String itemName = MapUtils.getString(param, "itemName");
		if(StringUtils.isNotEmpty(itemName)){
			sb.append(" and obj.itemName like '%"+itemName+"%'");
		}
		String scoreType = MapUtils.getString(param, "scoreType");
		if(StringUtils.isNotEmpty(scoreType)){
			sb.append(" and obj.scoreType = '"+scoreType+"'");
		}
		sb.append(" order by obj.modifyTime ");
		return sb;
	}
	
	
}
