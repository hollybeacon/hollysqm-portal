package com.hollycrm.hollysqm.business.agentscore.service;

import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.rest.web.util.WebUtil;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollysqm.entities.DataScore;

@Service("agentScoreService")
public class AgentScoreService {
	
	@Autowired
	private ConfigedJdbc configedJdbc;
	
	/** 
	* @Description:坐席成绩查询
	* @param:@param param
	* @param:@param pageResult
	* @param:@return    设定文件 
	* @return:PageResult<DataScore>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public  PageResult<DataScore> queryAgentScore(Map<String, Object> param, PageResult<DataScore> pageResult){
		// 获取当前操作人帐号
		String userCode = WebUtil.getLoginUser().getUserCode();
		String sqlKey = "velocity.score.queryAgentScore";

		param.put("agentCode", userCode);
		pageResult = configedJdbc.selectPageResult(sqlKey, param, pageResult,DataScore.class);
		
		Long lon = configedJdbc.selectCount(sqlKey, param);// 获取总条数
		pageResult.setTotal(lon);
		
		return pageResult;
	}
	
	/**
	 * 坐席查看质检单
	 * @param param
	 * @return
	 */
	public boolean readPaper(Map<String, Object> param) {
		if(StringUtils.isEmpty(param.get("paperId"))) {
			return false;
		} else {
			String sql = "update tbl_qm_paper set has_read = 1 where paper_id = :paperId";
			configedJdbc.updateBySql(sql, param);
			return true;
		}
	}
	
}
