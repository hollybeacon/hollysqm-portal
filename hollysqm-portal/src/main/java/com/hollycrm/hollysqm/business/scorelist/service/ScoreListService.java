package com.hollycrm.hollysqm.business.scorelist.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.orm.service.jpa.JpaBaseServiceImpl;
import com.hollycrm.hollysqm.entities.ScoreList;

/**
 * @author liujr
 * 2017年6月16日
 * @Description 评分项业务类
 */
@Service("scoreListService")
public class ScoreListService extends JpaBaseServiceImpl<ScoreList, String>{
	@Autowired
	private ConfigedJdbc configedJdbc;
	/** 
	* @Description:检查该评分项是否被使用
	* @param:@param id
	* @param:@return    设定文件 
	* @return:boolean    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年6月16日
	*/
	public boolean checkScoreList(String id){
		String sql = "select t.score_list_id from tbl_qm_score_item t where t.score_list_id = '"+id+"' and exists(select * from tbl_qm_standard s where s.standard_id = t.standard_id and s.status = '1')";
		Long lon = configedJdbc.selectCountBySql(sql, null);
		if(lon == 0)
			return true;
		else
			return false;
	}
}
