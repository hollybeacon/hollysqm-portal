package com.hollycrm.hollysqm.business.standardmanager.dao;

import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.orm.dao.jpa.JpaBaseDaoImpl;
import com.hollycrm.hollysqm.entities.Standard;

@Service("standardManagerDao")
public class StandardmanagerDao extends JpaBaseDaoImpl<Standard, String> {

}
