package com.hollycrm.hollysqm.business.unscored.service;

import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.rest.web.util.WebUtil;
import com.hollycrm.hollybeacon.basic.util.CollectionUtils;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollysqm.business.common.dao.UserDao;
import com.hollycrm.hollysqm.entities.Paper;
import com.hollycrm.hollysqm.entities.UnsolvedPaper;
import com.hollycrm.hollysqm.util.HollysqmUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * 未评分质检单业务类
 *
 * @author wangyf
 *         <p>
 *         2017年3月15日
 */
@Service("unscoredService")
public class UnscoredService {

    @Autowired
    private ConfigedJdbc configedJdbc;

    @Resource(name = "usersDao")
    private UserDao userDao;

    /**
     * 查找未处理质检单列表
     *
     * @param param
     * @return
     * @throws
     * @author wangyf
     */
    public PageResult<Paper> getUnscoredPaperList(Map<String, Object> param) {
        @SuppressWarnings("unchecked")
        PageResult<Paper> page = (PageResult<Paper>) HollysqmUtil.createPageResult(param);
        String userCode = WebUtil.getLoginUser().getUserCode();
        param.put("checkerCode", userCode);
        if (StringUtils.isNotEmpty(param.get("agent"))) {
            String agent = userDao.getUserCodeOrNo(param.get("agent").toString(), "agentCode");
            agent = HollysqmUtil.paramParseQueryText(agent, "sql", true);
            if (StringUtils.isNotEmpty(agent)) {
                param.put("agent", agent);
            } else {
                param.put("agent", "'" + param.get("agent").toString() + "'");
            }

        }

        if (StringUtils.isNotEmpty(param.get("checker"))) {
            String checker = userDao.getUserCodeOrNo(param.get("checker").toString(), "agentCode");
            checker = HollysqmUtil.paramParseQueryText(checker, "sql", true);
            if (StringUtils.isNotEmpty(checker)) {
                param.put("checker", checker);
            } else {
                param.put("checker", "'" + param.get("checker").toString() + "'");
            }

        }

        String sqlKey = "velocity.score.queryUnScore";

        page = configedJdbc.selectPageResult(sqlKey, param, page, Paper.class);
        Long lon = configedJdbc.selectCount(sqlKey, param);
        page.setTotal(lon);

        return page;
    }


    /**
     * 获取未评分
     *
     * @param param
     * @return
     * @throws
     * @author wangyf
     */
    public UnsolvedPaper getUnscoredPaperMsg(Map<String, Object> param) {
        //参数规整
        //paperId
        String handlingPaperId = null;
        if (StringUtils.isEmpty(param.get("paperId"))) {
            return null;
        } else {
            handlingPaperId = (String) param.get("paperId");
            param.remove("paperId");
        }

        //agent
        if (StringUtils.isNotEmpty(param.get("agent"))) {
            String agent = userDao.getUserCodeOrNo(param.get("agent").toString(), "agentCode");
            agent = HollysqmUtil.paramParseQueryText(agent, "sql", true);
            param.put("agent", agent);
        }

        //checker
        if (StringUtils.isNotEmpty(param.get("checker"))) {
            String checker = userDao.getUserCodeOrNo(param.get("checker").toString(), "agentCode");
            checker = HollysqmUtil.paramParseQueryText(checker, "sql", true);
            param.put("checker", checker);
        }

        //userCode
        String userCode = WebUtil.getLoginUser().getUserCode();
        param.put("checkerCode", userCode);

        //获取所有待处理的质检单（包含正在处理）
        StringBuilder sql = new StringBuilder();
        sql.append("select t1.paper_id as nextPaperId from ");
        sql.append("tbl_qm_paper t1 left join tbl_qm_plan t2 on t1.plan_id = t2.plan_id ");
        sql.append("where 1=1 ");
        if (StringUtils.isNotEmpty(param.get("dataType"))) {//数据来源
            sql.append("and t1.paper_type = :dataType ");
        }
        if (StringUtils.isNotEmpty(param.get("planId"))) {//计划名称
            sql.append("and t2.plan_id = :planId ");
        }
        if (StringUtils.isNotEmpty(param.get("startTime"))) {//开始时间
            sql.append("and substr(t1.create_time, 1, 10) >= :startTime ");
        }
        if (StringUtils.isNotEmpty(param.get("endTime"))) {//结束时间
            sql.append("and substr(t1.create_time, 1, 10) <= :endTime ");
        }
        if (StringUtils.isNotEmpty(param.get("agent"))) {//坐席
            sql.append("and t1.agent_code in  " + param.get("agent").toString() + " ");
        }
        if (StringUtils.isNotEmpty(param.get("checker"))) {//质检员
            sql.append("and t1.checker_agent_code in  " + param.get("checker").toString() + " ");
        }
        if (StringUtils.isNotEmpty(param.get("checkerCode"))) {//当前质检员
            sql.append("and t1.checker_code = :checkerCode ");
        }
        sql.append("and t1.quality_status = '0' ");

        //获取待处理总数
        Long count = configedJdbc.selectCountBySql(sql.toString(), param);

        if (count <= 0) {
            //不存在数据
            return null;
        }else if (count == 1 ) {
            //待处理的数量为1，即待处理的必然是正在处理的,即没有下一项
            UnsolvedPaper nextPaper = new UnsolvedPaper();
            nextPaper.setNextPaperId(handlingPaperId);
            nextPaper.setPaperCount(1);
            return nextPaper;
        }

        //查询待处理的两条记录
        sql.append("limit 2");
        List<UnsolvedPaper> list = configedJdbc.queryForList(sql.toString(), param, UnsolvedPaper.class);


        if (CollectionUtils.isEmpty(list)) {
            return null;
        } else {
            //获取第一条数据
            UnsolvedPaper nextPaper = list.get(0);
            if (handlingPaperId.equals(nextPaper.getNextPaperId())) {
                nextPaper = list.get(1);
            }

            nextPaper.setPaperCount(count.intValue());
            return nextPaper;
        }
    }
}
