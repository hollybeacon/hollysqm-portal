package com.hollycrm.hollysqm.business.judgechecker.rest;

import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollysqm.business.judgechecker.service.JudgeCheckerService;
import com.hollycrm.hollysqm.entities.Paper;
import com.hollycrm.hollysqm.entities.PaperCheckData;
import com.hollycrm.hollysqm.util.HollysqmUtil;

/**
 * 评价质检员
 * @author wangyf
 *
 * 2017年3月21日
 */


@RestService
@RequestMapping(value="/judgeCheckerRest")
public class JudgeCheckerRest {
	
	@Resource(name="judgeCheckerService")
	private JudgeCheckerService judgeCheckerService;
	
	/**
	 * 获取质检单评价列表
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	@RequestMapping(value="/getPaperList", produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getPaperList(@RequestParam Map<String, Object> param) {
		PageResult<Paper> page = judgeCheckerService.getPaperList(param);
		
		return HollysqmUtil.wrapUpPageResult(page, true, "");
	}
	
	/**
	 * 评价质检单
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	@RequestMapping(value="/evaluatePaper",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult evaluatePaper(@RequestParam Map<String, Object> param) {
		boolean flag = judgeCheckerService.evaluatePaper(param);
		
		return HollysqmUtil.wrapUpPageResult(null, flag, "");
	}
	
	/**
	 * 获取质检单的评价
	 * @param param
	 * @return
	 * @throws
	 * @author wangyf
	 */
	@RequestMapping(value="/getEvaluateResult",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getEvaluateResult(@RequestParam Map<String, Object> param) {	
		PaperCheckData p = judgeCheckerService.getEvaluateResult(param);
		
		return HollysqmUtil.wrapUpPageResult(p, true, "");
	}
	

}
