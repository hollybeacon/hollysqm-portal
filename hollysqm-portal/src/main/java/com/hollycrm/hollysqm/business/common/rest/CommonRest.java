package com.hollycrm.hollysqm.business.common.rest;

import com.alibaba.fastjson.JSONObject;
import com.hollycrm.hollybeacon.basic.core.annotations.ActionLog;
import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.csv.CsvExport;
import com.hollycrm.hollybeacon.basic.csv.config.CsvConfiguration;
import com.hollycrm.hollybeacon.basic.csv.config.CsvTable.CsvField;
import com.hollycrm.hollybeacon.basic.excle.ExcleExport;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.pdf.PdfExport;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollybeacon.basic.util.LabelValue;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollybeacon.basic.util.WebUtils;
import com.hollycrm.hollysqm.business.common.service.CommonService;
import com.hollycrm.hollysqm.entities.*;
import com.hollycrm.hollysqm.util.HollysqmUtil;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * 公共查询
 *
 * @author wangyf
 *         <p>
 *         2017年3月15日
 */
@SuppressWarnings("deprecation")
@RestService
@RequestMapping(value = "/commonRest")
public class CommonRest {
    protected final Log logger = LogFactory.getLog(getClass());

    @Resource(name = "commonService")
    private CommonService commonService;

    @Resource(name = "csvExport")
    protected CsvExport csvExport;

    @Resource(name = "pdfExport")
    protected PdfExport pdfExport;

    @Resource(name = "excleExport")
    protected ExcleExport excleExport;

    @Resource(name = "csvConfig")
    protected CsvConfiguration csvConfig;

    /**
     * 获取打分项列表
     *
     * @param param
     * @return ServiceResult
     * @throws
     * @author wangyf
     */
    @RequestMapping(value = "/getItemScoreList", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getItemScoreList(@RequestParam Map<String, Object> param) {
        List<ScoreItem> list = commonService.getScoreItemList(param);

        return HollysqmUtil.wrapUpPageResult(list, true, "");
    }

    /**
     * 获取典型案例
     *
     * @param param
     * @return ServiceResult
     * @throws
     * @author wangyf
     */
    @RequestMapping(value = "/getPaperCase", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getPaperCase(@RequestParam Map<String, Object> param) {
        Case c = commonService.getPaperCase(param);

        return HollysqmUtil.wrapUpPageResult(c, true, "");
    }

    /**
     * 获取质检分数信息
     *
     * @param param
     * @return ServiceResult
     * @throws
     * @author wangyf
     */
    @RequestMapping(value = "/getPaperScoreResult", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getPaperScoreResult(@RequestParam Map<String, Object> param) {
        PaperScoreResult scoreResult = commonService.getPaperScoreResult(param);

        return HollysqmUtil.wrapUpPageResult(scoreResult, true, "");
    }

    /**
     * 质检打分
     *
     * @param param
     * @return ServiceResult
     * @throws
     * @author wangyf
     */
    @RequestMapping(value = "/scorePaper", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult scorePaper(@RequestParam Map<String, Object> param) {
        boolean flag = commonService.scorePaper(param);

        return HollysqmUtil.wrapUpPageResult(null, flag, "");
    }

    /**
     * 质检分数修改打分
     *
     * @param param
     * @return ServiceResult
     * @throws
     * @author wangyf
     */
    @RequestMapping(value = "/editPaper", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult editPaper(@RequestParam Map<String, Object> param) {
        boolean flag = commonService.editPaper(param);

        return HollysqmUtil.wrapUpPageResult(null, flag, "");
    }

    /**
     * 接触记录绑定评分模板生成质检单
     *
     * @param param
     * @return ServiceResult
     * @throws
     * @author wangyf
     */
    @RequestMapping(value = "/createPaper", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult createPaper(@RequestParam Map<String, Object> param) {
        PaperData paperData = commonService.createPaper(param);

        return HollysqmUtil.wrapUpPageResult(paperData, true, "");
    }

    /**
     * 获取接触记录信息
     *
     * @param param
     * @return ServiceResult
     * @throws
     * @author wangyf
     */
    @RequestMapping(value = "/getRecord", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getRecord(@RequestParam Map<String, Object> param, HttpSession session) {
//        Record record = commonService.getRecord(param);

        Record record = null;
        String recordJson = "";
        if ("qiuyy2_5".equals(param.get("paperId"))) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180619162327\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"13003988579\",\"closeType\":null,\"contactId\":\"00A3F741B1104BA0B5A62B9EBFBAD3B3\",\"custArea\":\"0592\",\"custBand\":\"01\",\"custLevel\":\"200\",\"dataType\":\"v8\",\"endTime\":null,\"length\":176,\"planId\":\"fe242b951b764337958b3429cc851cb5\",\"planName\":\"2018年6月服务质量监督质检计划\",\"qualityStatus\":\"0\",\"recordFile\":\"nfs1/1/voice/20180519/059210010/3679/20180519162615687.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":11,\"startTime\":\"20180619162327\",\"txtContent\":\"n0#三六七九|号客服代表为您服务|您好请问有什么帮您n1#喂你好|就是你帮我看一下我这张卡人的流量吗n0#稍等|感谢您耐心等待先生您这张卡片有流量啊感谢您套餐里的话就是有一个国内流量三百兆|就是说呃国内三百兆流量了然后的话您现在您的业务去范围内是有三点一个g的功能呃省内的业务业务就n1#业务是哪里n0#您的业务去的话我这边看到是在二零一四一每海沧业务群n1#那海沧业务是由基地那边呢n0#校园的业务线n1#哦就是说|如果是到别的地方的话就只有三百兆的n0#但是n1#三百兆|现在有没有什么|呃|有没有什么|流量套餐n0#您稍等您这个流量套餐需要省内还是国内的呢先生n1#就是|嗯就是不愿整的吗n0#就是像那个是吗您稍等帮您看一下n1#就是有没有什么优惠的流量套餐吗|因为我现在|现在刚好说要要是联系完符合排行要用流量吗n0#感谢您耐心等待目前看到您能够办理的省内流量包是呃流量畅想的二十块钱一个g送一个g|三十块钱是两个g送两个g|然后六十块钱是五个g送五个g的n1#你刚才说的那个什么二十块钱一个g是吧n0#呃对一个g送您的信息n1#怎么一个季度一个月n0#一个g送一个g一千零二十四兆送一千零二十四兆|就是说n1#二十块钱买|买一个自动一个月n0#对是的n1#就是在福建省内都可以用的是吧n0#但是呢n1#就是说两句吗n0#对是的n1#好帮我开一个吧n0#那这个的话是要通过短信方式进行开通稍后下发短信给您您根据短信提示操作一下|那这个如果您直接帮我开就好了你那边|很抱歉这个在线我们开通不了n1#那那你发过来吧n0#已经下发短信给您了您稍后根据短信提示操作一下开通成功之后是立即生效如果您本月没有来电退订的话下个月是自动续订的|我现在n1#我现在回复立马立马生效吧n0#开通成功之后是立即生效如果您本月没有来电退您下个月是自动续零的|建议您详细阅读一下短信内容了解一下流量包使用规则|给您下发给您了还有什么其他问题呢|那祝您生活愉快再见下您说\",\"user\":{\"agentCode\":\"70050\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy1\",\"username\":\"邱赟赟1\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        } else if ("qiuyy2_4".equals(param.get("paperId"))) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180620100627\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"18505910929\",\"closeType\":null,\"contactId\":\"00A3A9ECD14C48AF84BC9C303A231A56\",\"custArea\":\"0591\",\"custBand\":\"04\",\"custLevel\":\"500\",\"dataType\":\"v8\",\"endTime\":null,\"length\":147,\"planId\":\"fe242b951b764337958b3429cc851cb5\",\"planName\":\"2018年6月服务质量监督质检计划\",\"qualityStatus\":\"1\",\"recordFile\":\"nfs2/1/voice/20180520/059110010/3655/20180520100849805.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":0,\"startTime\":\"20180620100627\",\"txtContent\":\"n1#三六五五|好四g代表为您服务n0#您好请问有什么可以帮您n1#你好我的这一个手机号是不是到|哦四个月|我的那个什么固定的套餐结束了n0#啊不是它套餐不会给您结束是活动您之前不是有个送手机这个活动到这个月底就结束了n1#哦但是您如果您n0#对您如果没有更改套餐这个原有的一个套餐了n1#还款号段条了太贵一个号码是解决不使用n0#但是您不是有副卡吗要去营业厅把付款解绑了才能更改别的一个套餐n1#那我觉得方便我们银行不考什么的后面那个吗n0#付款为直接单独的一个套餐啊n1#付款三十一号当时吗那我这个这个手机号呢n0#这个手机号就是您要更改别的套餐也是更改别的套餐吗n1#那我以后不管能不能再关注这个信息到账n0#有的套餐是不支持n1#有的地方不仅是吗n0#对是的您要看一下套餐是这样的您如果要解绑要去福州的自有营业厅才能一个嗯才能一个就是解绑的n1#不行我不能订套餐吗我们不考虑如果顾客不行吗n0#啊那就是保持原有的套餐就不能更改了n1#你们怎么怎么|公司的估计吗天天过去你说什么能不能这样的规定n0#他这个是要把卡n1#放到就原来告诉我n0#啊但是副卡的这边自己是绑定在一起是不能一个更改别的套餐啊n1#我不敢跟他们为什么不行啊我也没如果不要用副卡了n0#它有这种一个月或者就是说您要把它解绑了才能更改别的套餐吧当时的话n1#您就没有这项规定没有这样规定n0#嗯很抱歉给您带来不便n1#不是你当时就怕他的话你你我可以绑定方式了没有超过去嘛n0#嗯是要要是要有您这边要把这个直接半年才能更改别的套餐的n1#二|的呢|啊很抱歉给您带来不便如果稍后呢n0#嗯n1#证件号码的话那个人说还没收到我主号忘记了n0#嗯那您要机主本人携带身份证到归属地营业厅这边才能一个注销n1#我是说我是不需要那个附卡我不考虑一下吧n0#因为账户要把这个主副卡就是解绑掉吗然后您这边的话付款如果要使用就取消一个也可以您如果要注销的话那您|要不直接去营业厅就可以直接注销了n1#那我知道了嗯n0#好的很抱歉给您带来不便|嗯但是呢\",\"user\":{\"agentCode\":\"70050\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy1\",\"username\":\"邱赟赟1\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        } else if ("qiuyy2_3".equals(param.get("paperId"))) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180625144134\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"18606080617\",\"closeType\":null,\"contactId\":\"000E7AA13AE24878BBF85106D99ADC6E\",\"custArea\":\"0595\",\"custBand\":\"01\",\"custLevel\":\"500\",\"dataType\":\"v8\",\"endTime\":null,\"length\":137,\"planId\":\"fe242b951b764337958b3429cc851cb5\",\"planName\":\"2018年6月服务质量监督质检计划\",\"qualityStatus\":\"1\",\"recordFile\":\"nfs2/1/voice/20180425/059510010/7173/20180425144344934.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":25,\"startTime\":\"20180625144134\",\"txtContent\":\"n1#号客服代表为您服务n0#您好请问什么有什么可以帮您n1#那个我流量也有|嗯打电话呢还有免费的一百多分钟为什么还是在扣钱呢n0#要不我帮您查看一下|您好先生这是我这边看到您这边的流量的话是属于超出的一个情况|那其然后您的说通话分钟数还有的话我这边看到的是您这边的话|额稍等|您这边并没有包月呃这边的话业务区内的通话分钟数是有的n1#嗯嗯n0#业务之类的通话分钟数本地主叫服务|国内的话是然后他说您是在那个省外吗n1#谁呀n0#您在省内是吗n1#嗯嗯n0#稍等n1#而且昨天我用ap查的话还有八百多兆啊n0#呃这边八百多兆是超出您的流量超出套餐外的话八百多兆是按了一块一个g按六十块钱来计费收费的|您这边流量gps流量费超出了是二十块钱n1#对|对他是直接扣的还是怎么办我那个你你上面显示是还有八位不动n0#对对他是直接扣的因为这边八百多兆它是属于一个积分十块钱的基本收费的然后审核六十块钱呢只要收到这个扣费返六十块就可以直接使用剩余流量了n1#请稍等n0#我帮您查询一下n1#嗯嗯n0#您之前有没有拨打有没有在省省外呃或者说四十四万拨打的那个|这个国内电话呢n1#没有n0#那就是通话费就是国内通话费有疑问了是吧|嗯那我这边帮您反馈您的问题工作人员这边四十八小时尽快给您回复好吗n1#啊n0#好这边请问还有什么可以可以可以吗n1#嗯n0#好的到时候服务进行评价保持您的手机上都满意请按两个一n1#再见\",\"user\":{\"agentCode\":\"70050\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy1\",\"username\":\"邱赟赟1\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        } else if ("qiuyy2_2".equals(param.get("paperId"))) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180623094352\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"18659752286\",\"closeType\":null,\"contactId\":\"000C7D7F07884B8785EAAE2BC12506BD\",\"custArea\":\"0597\",\"custBand\":\"04\",\"custLevel\":\"500\",\"dataType\":\"v8\",\"endTime\":null,\"length\":216,\"planId\":\"fe242b951b764337958b3429cc851cb5\",\"planName\":\"2018年6月服务质量监督质检计划\",\"qualityStatus\":\"1\",\"recordFile\":\"nfs4/1/voice/20180423/059110010/3763/20180423094723266.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":0,\"startTime\":\"20180623094352\",\"txtContent\":\"n1#尊敬有一ip客户|六三|好四次代表为您服务n0#哦|发给您了n1#哎你好n0#哎就是我这个联通卡是是三g还是四g的|啊肯定自己先生n1#自己的是吧n0#对呀n1#那我那个卡用了五六年了的n0#现在n1#有没有好一点的套餐之内n0#啊这个|那您这边的话您一个人消费怎么样呢就是在本人n1#您的多少所有的这样子|反正我现在的套餐是好像是多少的吗那个八十六的套餐但是好像|他不是之前上个月有推荐什么保底消费一百二有送两g的流量|我现在就是要语音通话多的流量|一个月大概消费大概一百二十兆一百五左右吗n0#您好|啊那先生这个只是就能接收吗九十九块钱n1#六十就是说大哥n0#对九十九的话先生他这个流量啊流量的话一个月是无限制的就是我们国内中国那个东西的|打电话的话三百分钟免费超过三百的话按照一分钟是n1#电脑n0#可以吗n1#那就就是说每个月九十九套餐三百分钟免费送流量随便用的n0#啊流量的话我们中国的呀大陆那种n1#姓名不是省内的大都会我们一般不出国就肯定都在再送过了|有没有新上线流量也没定下来|没有n0#流量的话一百积分给您n1#一百日封顶了n0#对对对n1#那超过一百七呢n0#超过一百您之后的话呢这个人上网功能就自动关闭了吗n1#哦自动关闭人了n0#在不过一般来说的话也用不了|应该也是应该按正常来说呢一百一十n1#一月印度的话n0#对n1#那你这个就是这个月开通的话下个月生效的n0#对如果说先生您这边也需要的话我这边可以帮您反映到后台去一下n1#你你先先看一下我这套我说他那边负责一个是多少呢还有一个就是什么保底到了一百二的这个是要全部先取消取消了它才能变更上n0#这个的话他不需要n1#只要申请下个月就直接申请自动变成九十九号的n0#对对对|那种n1#你好|保底消费一百二的那个套餐就自动取消了吧n0#对对对n1#哦行那你帮我变更一下吧好吧n0#呃那先这样就是说我这边的话已经就给您登记一下然后来处理的那他这个套餐变更成功后话呢先生您这边会收到那个短信通知给您处理生效|呃有个跟您说一下|然后呢自己跟他这个单位|不能结转到下个月用的n1#那没关系反正下个月都已经无限无限用那个无所谓了n0#我看嗯|如果说如果如果说是后来我说n1#操作不了n0#无法变更我们会在两天之内联系先生您这个手机就可以接通好吧n1#行还有一个就是语音语音套餐的话还有没有额外的什么语音包之类的n0#啊没有目前是没有啊我们目前您的流量包啊短信包就是那个明岛啊n1#您要比如说二十块钱一个月打到我们都没有了n0#啊目前是没有啊n1#那九十九套餐上面还有没有语音多一点的套餐资费n0#七九零九的话他这个价位上已经是非常的便宜了n1#我知道我只要三百分钟不够打知道了n0#打完的时候它按照一分钟是一毛五来算的n1#现在不用钱哦n0#最近呢两百都没回n1#哦就是超过三百分钟一毛五的n0#对呀n1#飞机那你先给我变更成九十九号的吗n0#啊|啊n1#二百九十九套餐属于四g套餐吗n0#这可能应该n1#哦行那你帮我变更一下好我帮您n0#而且您手机这边的话保持他跟我讲n1#啊行行行好|再见n0#给你吧n1#哦不用了谢谢n0#祝您生活愉快吗显示这样\",\"user\":{\"agentCode\":\"70050\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy1\",\"username\":\"邱赟赟1\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        } else if ("qiuyy2_1".equals(param.get("paperId"))) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180616132455\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"17689657988\",\"closeType\":null,\"contactId\":\"000AD69662F34BF7A9633DC7562ED71E\",\"custArea\":\"0594\",\"custBand\":\"04\",\"custLevel\":\"500\",\"dataType\":\"v8\",\"endTime\":null,\"length\":139,\"planId\":\"fe242b951b764337958b3429cc851cb5\",\"planName\":\"2018年6月服务质量监督质检计划\",\"qualityStatus\":\"1\",\"recordFile\":\"nfs3/1/voice/20180516/059410010/3976/20180516132707955.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":4,\"startTime\":\"20180616132455\",\"txtContent\":\"n1#呀|不代表为您服务n0#您好请问有什么可以帮您n1#村|这个手机号码是不是n0#实名制认证是吗|您是有短信通知您您的实名认证过了贵吗还是什么n1#就是那个|喂|我银行的一个身份证n0#嗯这边查到确实是您是有实名认证|您好n1#嗯|十四号好好n0#呃是这样的先生很抱歉您这边声音断断续续的能否跟您调整一下您的位置|那你说n1#这个手机号码申请那种座n0#我这边只能查看到您的时间是二零一七年五月二十二号入网的那句那个您是通过网络购买的吗那我们这边的话|国家工信部要求会有对这些信息做那个二支付卡道具二十五拿什么时间进行一些是看不到的|不过基本上您已经使用一年多的时间应该不会有出现说提醒说您在实名制资料不好可以吧|谢谢n1#我那个|呃|这个app呢|工作|怎么回事儿是那个|嗯n0#您有没有开免|那个|那您有没有开通那个免提能帮我关闭一下听得不是很清楚您的声音|您好n1#那个ap那个那那里边额十元是怎么跟|放在一起线了|那个提供n0#哦您是之前是有说信息提醒说资料不合贵要求您重新补充是吗n1#我怎么扣您的那个四零零升级之内n0#五五n1#真的好像是n0#哦呃这个如果说您资料一样的话这个没有关系吗反正钱您全额也便宜都没有关系吗n1#幺三五六那个|啊n0#啊还有什么其他需要请问的吗n1#哎n0#祝您生活愉快不好意思啊您再见\",\"user\":{\"agentCode\":\"70050\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy1\",\"username\":\"邱赟赟1\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        } else if ("qiuyy1_5".equals(param.get("paperId"))) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180608093510\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"13055999943\",\"closeType\":null,\"contactId\":\"00A8982C9A4D434FB7E1F9C7D6B86B09\",\"custArea\":\"0599\",\"custBand\":\"04\",\"custLevel\":\"400\",\"dataType\":\"v8\",\"endTime\":null,\"length\":191,\"planId\":\"fe242b951b764337958b3429cc851cb5\",\"planName\":\"2018年6月服务质量监督质检计划\",\"qualityStatus\":\"1\",\"recordFile\":\"nfs4/1/voice/20180508/059910010/3191/20180508093814140.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":15,\"startTime\":\"20180608093510\",\"txtContent\":\"n1#尊贵的vip客户三一九一|号客服代表为你服务n0#您好n1#啊我上个月定了一个流量套餐他这个月自动去个人帮我取消n0#您稍等|嗯|您是有开的时候我们帮您确认一下您现在除了您的套餐费的话|工号是九九四三是吧n1#嗯n0#除了您的套餐可以这边是有开通一个二十块钱的流量包费用n1#对啊这个不需要了因为我这个套餐流量然后用n0#嗯您好先生那您这边需要帮您那个就是说取消掉了吗请问n1#对呀取消啊n0#请您稍等n1#嗯n0#帮您取消取消成功之后下个月一号机上您看可以n1#嗯嗯好然后那个我这个套餐里面有多少流量啊n0#套餐里面吗n1#啊n0#套餐之内的话是四十是之内看看腾讯视频是那个免费的n1#然后就是诚信腾讯视频看n0#四十继续用n1#是自己的自己之内什么叫鸿讯饰品哦就腾讯视频啊信息吗n0#不是他可能是四月一用都是是谁支持呢免费的n1#哦那就是说重新咨询|然后威信|qq这种是吧n0#对n1#都可以吗n0#是的n1#哦好|就只能看视频呢n0#不是腾讯系列应用都可以n1#就是说河北银行那那其他业务呢n0#其他业务吗n1#啊|更好的话我还有n0#其他用您有用就是一百块钱三百兆n1#哦哦那那可以随|啊它会自动的那个吧就比如说我|呃|用了两个g然后它自动扣了看一下n0#一块钱八百兆n1#啊反正就比如说用了两个八百兆吗n0#他就扣两块钱存费n1#哦好好好好他这个月保底是多少n0#每个月的话您有有有扣费每个月固定消费十九块钱套餐费n1#哦那好像那你这边帮我取消一下|那个之前开通的n0#对取消掉了就是那个连接n1#费用还有没有其他的n0#嗯哦n1#没有吧n0#没有了哎n1#啊好谢谢啊n0#请不客气那还有什么其他问题吗n1#嗯不用了他这个也就是说用的流量基本上工号多少钱了哈n0#对n1#恩好好好谢谢n0#嗯不客气那还有什么其他问题吗n1#没有没有n0#嗯好的祝您生活愉快再见\",\"user\":{\"agentCode\":\"70051\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy2\",\"username\":\"邱赟赟2\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        } else if ("qiuyy1_4".equals(param.get("paperId"))) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180615095945\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"18650416308\",\"closeType\":null,\"contactId\":\"00A5314AF9544085A7D836CD22013C29\",\"custArea\":\"0594\",\"custBand\":\"01\",\"custLevel\":\"400\",\"dataType\":\"v8\",\"endTime\":null,\"length\":206,\"planId\":\"fe242b951b764337958b3429cc851cb5\",\"planName\":\"2018年6月服务质量监督质检计划\",\"qualityStatus\":\"0\",\"recordFile\":\"nfs3/1/voice/20180515/059210010/3689/20180515100306718.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":13,\"startTime\":\"20180615095945\",\"txtContent\":\"n0#三n1#六八九|号n0#客服代表n1#为您服务|嗯我看|喂你好帮我查一下我这张卡现在这个月套餐多少钱n0#嗯您好是沃派款七十六块钱的n1#七十六块怎么那么贵|那嗯嗯月租这边您的这个对吧n0#最便宜的保留来电显示十六不要来电显示十块n1#然后验证四四六二n0#也就是相当于来电显示另外六块钱需要来电显示就是业务要来电显示对吧n1#这个套餐没有嗯零点的吗n0#他就只有一百兆的国内流量n1#一百兆|哦就在人|那好那你帮我开通这些你的因为我们这边人工的|我这张卡号上的用不起留他之前一万号移动的|是n0#好那您这张卡的话之前有一个每个月送十九块钱的合约他这个的话您要先点就合约才能够|套餐变更n1#怎么点赎回什么点n0#如果说您需要我这边可以先问您取消合约的问题|对n1#那买的还有座机支付我都没什么什么支付宝都没|那我就买的支付宝我又一直知道然后|我你帮我查一下什么时候就是那个咱一个月就是一百块n0#因为您打电话比较多n1#哎哟哇n0#好的那这边是否需要问您先根据取消合约的问题女士n1#哦|对n0#而且是吗n1#呃你帮我看看怎么什么情况n0#那我这边先帮您登记取消这个每个月返二十九块钱的合约那四十八个小时之内会有其他人员联系您|到时候他帮您这边解除之后您再打电话过来说要改那个十六块钱的组合套餐问题好吗n1#那我坐着这个是自己个人在这几个月n0#发送很久维修时间的话是送到二零二一年n1#刚才就是这笔款|哦对对对不会转|我总感觉我这张卡|呃包月很大|我你帮我查一下我这边是不是流量超超超过了n0#不是的您是平时打电话多因为您这个里面没有分钟数n1#哦就打电话就没有流量呃n0#流量没有用超n1#哦|麻烦你帮我修改一下帮我去转接到我我我那个要要之前一个如果就可以要是我就说我行那给您做了开不在您这块旁边不怎么打的n0#我这边先生如果您根据只加这个合约了我们这边人员联系您确认|合约取消之后您再打电话过来说要改或者说通过手机营业厅网上营业厅改好嘛n1#对对那你帮我帮我帮我问一下啊好好n0#我已经先把您这是指在合约了好嘛n1#对对对对嗯n0#是这样子n1#啊没有了没n0#那祝您生活愉快再见n1#嗯嗯好好好好\",\"user\":{\"agentCode\":\"70051\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy2\",\"username\":\"邱赟赟2\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        } else if ("qiuyy1_3".equals(param.get("paperId"))) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180619105959\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"13055817831\",\"closeType\":null,\"contactId\":\"00A15D806BAB47FF811512E7DBBA9264\",\"custArea\":\"0595\",\"custBand\":\"01\",\"custLevel\":\"300\",\"dataType\":\"v8\",\"endTime\":null,\"length\":180,\"planId\":\"fe242b951b764337958b3429cc851cb5\",\"planName\":\"2018年6月服务质量监督质检计划\",\"qualityStatus\":\"0\",\"recordFile\":\"nfs2/1/voice/20180519/059510010/7373/20180519110251750.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":7,\"startTime\":\"20180619105959\",\"txtContent\":\"n1#家|三|号客服代表为您服务n0#啊您好请问有什么可以帮您n1#呃帮我查一下我是联通的您帮我开但是那个联通那个联系我们银行n0#呃就是您这个号码是吗n1#嗯嗯嗯n0#帮您看一下稍等一下|呃您当前是说那个呃二十网络不能用的是吗n1#对因为我他两张卡了然后两两张卡的话你们联通的话直接没有金额|我这个一直是全网通的|哦n0#呃这边的话就是说呃也是为了确保您的正常使用吗建议您这边是使用您一个那个主卡长吗之后呢一定或者是更好的一个|的话是这样的啊n1#不是不是我那个福呃不用呢流量只是用联通的|喂|电信的话王哥|就是我数据用电我要用|那个电信的然后|联通的话n0#那个是可以打电话了n1#对对对|我帮您那您n0#那您这边跟您咨询一下就是您那边不能使用二十的一个那个地点是在哪里呢n1#那边会员啊会员罗成正常n0#国产的额也是在泉州市那边吗|对对对呃哪个区的呢n1#会员会员姓名罗城着呢人说是会怎么n0#突然显和产生啊如果的话是那个天收的多虫字旁的隆吗还是怎么样n1#对n0#洛城城市的城啊n1#嗯嗯和城镇重复n0#呃就在这一块使用他那个不行吗n1#对n0#呃那那这个的话就是说嗯我们帮您呃需要帮您申告到我们那个后台上面去|有专门的工作人员帮您核实之后可能回电话联系您呢|哦就联系您这个号码还是有其他电话号码在有一个呢n1#不知道你你能帮我开通那个咱们往你们元都不是比较急网络关掉关闭了n0#呃我们这边前台是没有开通的那个学校的只能跟您那个返回到后台专门工作人员|我帮您核实完之后啊他们会电话联系您呢n1#要多久了n0#呃这个正常的话我们是在四十八小时之内会有处理结果|请问您这边主要起的话然后我就帮您那个单子|就是在重新催促一下让工作人员那个信号不好它是不让n1#您要需要我因为这边我要买手机是是朋友吗n0#哦这样呢那您这边的话是稍后的话我也会按一下我们那个呃值班经理那边反馈一下就是让他呃跟他讲的反馈让后台是尽快|像您这个问题吗可以吗n1#号好好n0#好的那请问还有其他问题稍后就不需要补充的吗n1#没有n0#嗯好的女士感谢您的来电祝您生活愉快|先生\",\"user\":{\"agentCode\":\"70051\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy2\",\"username\":\"邱赟赟2\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        } else if ("qiuyy1_2".equals(param.get("paperId"))) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180621160200\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"16605925342\",\"closeType\":null,\"contactId\":\"00A7ECF8681649B08CB9D90FF8C1B5F0\",\"custArea\":\"0592\",\"custBand\":\"04\",\"custLevel\":\"500\",\"dataType\":\"v8\",\"endTime\":null,\"length\":226,\"planId\":\"fe242b951b764337958b3429cc851cb5\",\"planName\":\"2018年6月服务质量监督质检计划\",\"qualityStatus\":\"0\",\"recordFile\":\"nfs2/1/voice/20180421/059210010/3431/20180421160540107.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":12,\"startTime\":\"20180621160200\",\"txtContent\":\"n0#三|四三n1#一|号n0#客服代表n1#为你服务n0#您好请问有什么可以帮您|哎你好我想问一下假如说|我|在出国n1#期间没有开通那个|刚才或者说是那个|嗯你说|全国吗然后我到那边您只要开通要怎么办呢n0#幺八六幺八六幺零零幺零|国际社对不对|零呃您是说您本机吗|本机的话开通国际客服热线的话呃不好意思|您要开通国际漫游功能的话是|需要登录手机营业厅|去开通n1#哦这样子n0#就是教育升档那个预存话费三百块钱开通n1#哦这样子n0#对n1#啊|啊n0#嗯n1#嗯也就是说我是自己直接装一个五年中的已经n0#然后n1#等一些n0#哪一个n1#就可以了n0#对没错嗯n1#哦就不用说再去营业厅开呀或者说平时上班的是吧n0#对营业厅不能开n1#哦是自己在最大机上面开是吗n0#单没有做n1#啊那刚才我问前面我又打了一次今天又打一次我要确认一下刚才n0#我打你们客服n1#就文明路客服过来|他跟我说一定要去营业厅看n0#嗯哦您这个是您就是同时网卡腾讯网卡的话是在手机营业厅上开通n1#哦|那如果不是存去还款的话就是说假如说是那种小包卡呢n0#也是像您这种是属于互联网套餐了|都是要在手机营业厅上开通n1#哦除了以前老的老的那种卡上去营业厅是吗n0#嗯规定就看一下您的套餐呢有个老的卡的话|是自己存不用开通直接那个如果存两百或者三百块钱吗n1#然后n0#请自动开通的呀n1#哦这样子只要您反应让你去那个业务然后您把钱出去就ok了是吧n0#对没错|那您那个套餐跟他的这个不一样n1#我有因为我一家人都在用吧因为我我总共是三十四兆嘛我一张两张上网卡n0#晚上那个|然后n1#是小宝卡的上网卡想老卡吗n0#嗯n1#如果说有出具我问就是说家里有出去玩去香港或者只要们那边然后忘记开的您要开的话要怎么办n0#手机营业厅手机营业厅开通|也算手机营业厅开通或者是|您现在有办理渠道您等一下啊n1#恩好你帮我看看|啊n0#嗯嗯|对呀没错手机营业厅开通|就是只有这个手机营业厅上开通或者|有对关闭也是在手机营业厅上开通也是在手机营业厅上开通的说我把路径下发到您手机在网上自己看|啊好然后n1#啊我看还有一个|假如说我|过去了完我没开通然后咨询还是咱们小说怎么改不了吗|那怎么办|短信也收不了n0#那有个国际客服热线幺八六|幺八六|幺零零幺零|对n1#呃打过去就可以了是吧n0#对他是我们联通这个国际客服热线号码对n1#哦|这样子n0#对n1#他说和平那给你发短信发给我我看一下我我详细看一下好吗这样n0#我发了您自己对他说呢n1#嗯好的n0#谢谢好不客气祝您生活愉快再见\",\"user\":{\"agentCode\":\"70051\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy2\",\"username\":\"邱赟赟2\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        } else if ("qiuyy1_1".equals(param.get("paperId"))) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180613080523\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"13055388170\",\"closeType\":null,\"contactId\":\"00A6DA6F617D46EDBB9B345F45AA54F3\",\"custArea\":\"0596\",\"custBand\":\"03\",\"custLevel\":\"200\",\"dataType\":\"v8\",\"endTime\":null,\"length\":236,\"planId\":\"fe242b951b764337958b3429cc851cb5\",\"planName\":\"2018年6月服务质量监督质检计划\",\"qualityStatus\":\"0\",\"recordFile\":\"nfs4/1/voice/20180513/059610010/3136/20180513080912859.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":10,\"startTime\":\"20180613080523\",\"txtContent\":\"n1#幺五|啊|好自己代表为你服务n0#您好有什么可以帮您n1#喂你好我想咨询一下哦我这部手机啊哎|怎么会有一个|一个不客气|邮政那个呃|单据和块三十六号服务套餐五百兆的那个嘛然后另外一种|那个零元流量换新功能包的那个机器的那个|还有一个三十六元红利套餐的那个|就不知道n0#但是您套餐内包含的内容啊n1#三十六块钱有包含这么多n0#对呀他是有六十兆的一个国内流量五百兆的国内定向流量然后那个是入套通过美金g流量六十块钱这个流量这个一个g流量如果说音乐包的话就是收费的|他是一个g六十块钱n1#六十块钱每记录六十块钱n0#一个gn1#一个记录吗n0#不是一个记录一个g然后有一个进行收取六十块钱n1#哇|就没开|怎么会开通这个呢n0#主要是套餐内自带得跟流量它这个功能包n1#那我如果不用不行n0#如果说流量没有超出的话它就不会收费您这是流量有超出才会收费流量没有超出不收费n1#那不对呀那我等一下还有一个是一定要新建邮政的这个都还没用完啊n0#给你算的话就只能是在慈溪的软件才可以用并不是说所有软件都可以用n1#还有一个五百兆的n0#五百兆的话就是一个赠送流量为一对n1#我还|三|块钱是包括六十兆流量n0#也就是六十兆给的国内定向流量可以用n1#我就六十分还有六十分钟可以用n0#对n1#啊|一个三百分钟的那个什么新货派三十六套餐的这个意思要设置这样n0#要设置三个亲情号码然后这个三百分钟才可以用n1#从这个是打亲情号码用的n0#对n1#哎这个是多少钱n0#请问是多少钱n1#办理进证应该是三百分钟的这个多少钱n0#套餐内包含的内容n1#好像三十六块里面包含的内容|哦怎么又还有一个是呃连开十块钱的那个流量的那个那个是什么n0#您这边并没有订购流量包n1#哎等一下等一下等一下先生它一个gss流量费n0#流量有超出产生扣费n1#明天|直接使用卡片十兆而已n0#其实有六十兆的一个国内流量如果说流量有超出的话费用是额外收取|六十兆|啊n1#哦好好好好那男然后那个上个月有有一个那个五十块钱的上网力是干嘛呢n0#那就是流量超出产生那个扣费提醒的扣费您看是叫什么呢女士n1#您放心写上网络呀n0#稍等我帮您看一下|这就是上个月的流量是有效就产生扣费了|嗯嗯n1#稍等|喂|能不能把这三十六块钱内所包含的那个套餐以后给我看一下n0#套餐内容那您不是都已经有查询到了吗n1#请问您您花短信给我换一下吧n0#可以那我这边给您下发一个短信过去您稍后看一下n1#哦哦好谢谢你n0#不客气请问什么其他的问题\",\"user\":{\"agentCode\":\"70051\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy2\",\"username\":\"邱赟赟2\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        }

        if ("".equals(recordJson)) {
            record = commonService.getRecord(param);
        } else {
            return JSONObject.parseObject(recordJson, ServiceResult.class);
        }

        return HollysqmUtil.wrapUpPageResult(record, true, "");
    }

    /**
     * 获取接触记录高亮信息
     *
     * @param param
     * @return ServiceResult
     * @throws
     * @author wangyf
     */
    @RequestMapping(value = "/getHighlightRecord", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getHighlightRecord(@RequestParam Map<String, Object> param, HttpSession session) {
//		Record record = commonService.getHighlightRecord(param);

        Record record = null;
        String recordJson = "";
        if ("000E7AA13AE24878BBF85106D99ADC6E".equals(param.get("contactId").toString())) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180625144134\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"18606080617\",\"closeType\":null,\"contactId\":\"000E7AA13AE24878BBF85106D99ADC6E\",\"custArea\":\"0595\",\"custBand\":\"01\",\"custLevel\":\"500\",\"dataType\":\"v8\",\"endTime\":null,\"length\":137,\"planId\":null,\"planName\":null,\"qualityStatus\":\"1\",\"recordFile\":\"nfs2/1/voice/20180425/059510010/7173/20180425144344934.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":25,\"startTime\":\"20180625144134\",\"txtContent\":\"n1#号客服代表为您服务n0#您好请问什么有什么可以帮您n1#那个我<span style='color:red;'>流量</span>也有|嗯打电话呢还有免费的一百多分钟为什么还是在扣<span style='color:red;'>钱</span>呢n0#要不我帮您查看一下|您好先生这是我这边看到您这边的<span style='color:red;'>流量</span>的话是属于超出的一个情况|那其然后您的说通话分钟数还有的话我这边看到的是您这边的话|额稍等|您这边并没有包月呃这边的话业务区内的通话分钟数是有的n1#嗯嗯n0#业务之类的通话分钟数本地主叫服务|国内的话是然后他说您是在那个省外吗n1#谁呀n0#您在省内是吗n1#嗯嗯n0#稍等n1#而且昨天我用ap查的话还有八百多兆啊n0#呃这边八百多兆是超出您的<span style='color:red;'>流量</span>超出<span style='color:red;'>套餐</span>外的话八百多兆是按了一块一个g按六十块<span style='color:red;'>钱</span>来计费收费的|您这边<span style='color:red;'>流量</span>gps<span style='color:red;'>流量</span>费超出了是二十块<span style='color:red;'>钱</span>n1#对|对他是直接扣的还是怎么办我那个你你上面显示是还有八位不动n0#对对他是直接扣的因为这边八百多兆它是属于一个积分十块<span style='color:red;'>钱</span>的基本收费的然后审核六十块<span style='color:red;'>钱</span>呢只要收到这个扣费返六十块就可以直接使用剩余<span style='color:red;'>流量</span>了n1#请稍等n0#我帮您查询一下n1#嗯嗯n0#您之前有没有拨打有没有在省省外呃或者说四十四万拨打的那个|这个国内电话呢n1#没有n0#那就是通话费就是国内通话费有疑问了是吧|嗯那我这边帮您反馈您的问题工作人员这边四十八小时尽快给您回复好吗n1#啊n0#好这边请问还有什么可以可以可以吗n1#嗯n0#好的到时候服务进行评价保持您的手机上都满意请按两个一n1#再见\",\"user\":{\"agentCode\":\"70050\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy1\",\"username\":\"邱赟赟1\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        } else if ("000C7D7F07884B8785EAAE2BC12506BD".equals(param.get("contactId").toString())) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180623094352\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"18659752286\",\"closeType\":null,\"contactId\":\"000C7D7F07884B8785EAAE2BC12506BD\",\"custArea\":\"0597\",\"custBand\":\"04\",\"custLevel\":\"500\",\"dataType\":\"v8\",\"endTime\":null,\"length\":216,\"planId\":null,\"planName\":null,\"qualityStatus\":\"1\",\"recordFile\":\"nfs4/1/voice/20180423/059110010/3763/20180423094723266.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":0,\"startTime\":\"20180623094352\",\"txtContent\":\"n1#尊敬有一ip客户|六三|好四次代表为您服务n0#哦|发给您了n1#哎你好n0#哎就是我这个联通卡是是三g还是四g的|啊肯定自己先生n1#自己的是吧n0#对呀n1#那我那个卡用了五六年了的n0#现在n1#有没有好一点的<span style='color:red;'>套餐</span>之内n0#啊这个|那您这边的话您一个人消费怎么样呢就是在本人n1#您的多少所有的这样子|反正我现在的<span style='color:red;'>套餐</span>是好像是多少的吗那个八十六的<span style='color:red;'>套餐</span>但是好像|他不是之前上个月有推荐什么保底消费一百二有送两g的<span style='color:red;'>流量</span>|我现在就是要语音通话多的<span style='color:red;'>流量</span>|一个月大概消费大概一百二十兆一百五左右吗n0#您好|啊那先生这个只是就能接收吗九十九块<span style='color:red;'>钱</span>n1#六十就是说大哥n0#对九十九的话先生他这个<span style='color:red;'>流量</span>啊<span style='color:red;'>流量</span>的话一个月是无限制的就是我们国内中国那个东西的|打电话的话三百分钟免费超过三百的话按照一分钟是n1#电脑n0#可以吗n1#那就就是说每个月九十九<span style='color:red;'>套餐</span>三百分钟免费送<span style='color:red;'>流量</span>随便用的n0#啊<span style='color:red;'>流量</span>的话我们中国的呀大陆那种n1#姓名不是省内的大都会我们一般不出国就肯定都在再送过了|有没有新上线<span style='color:red;'>流量</span>也没定下来|没有n0#<span style='color:red;'>流量</span>的话一百积分给您n1#一百日封顶了n0#对对对n1#那超过一百七呢n0#超过一百您之后的话呢这个人上网功能就自动关闭了吗n1#哦自动关闭人了n0#在不过一般来说的话也用不了|应该也是应该按正常来说呢一百一十n1#一月印度的话n0#对n1#那你这个就是这个月开通的话下个月生效的n0#对如果说先生您这边也需要的话我这边可以帮您反映到后台去一下n1#你你先先看一下我这套我说他那边负责一个是多少呢还有一个就是什么保底到了一百二的这个是要全部先取消取消了它才能变更上n0#这个的话他不需要n1#只要申请下个月就直接申请自动变成九十九号的n0#对对对|那种n1#你好|保底消费一百二的那个<span style='color:red;'>套餐</span>就自动取消了吧n0#对对对n1#哦行那你帮我变更一下吧好吧n0#呃那先这样就是说我这边的话已经就给您登记一下然后来处理的那他这个<span style='color:red;'>套餐</span>变更成功后话呢先生您这边会收到那个短信通知给您处理生效|呃有个跟您说一下|然后呢自己跟他这个单位|不能结转到下个月用的n1#那没关系反正下个月都已经无限无限用那个无所谓了n0#我看嗯|如果说如果如果说是后来我说n1#操作不了n0#无法变更我们会在两天之内联系先生您这个手机就可以接通好吧n1#行还有一个就是语音语音套餐的话还有没有额外的什么语音包之类的n0#啊没有目前是没有啊我们目前您的<span style='color:red;'>流量</span>包啊短信包就是那个明岛啊n1#您要比如说二十块<span style='color:red;'>钱</span>一个月打到我们都没有了n0#啊目前是没有啊n1#那九十九<span style='color:red;'>套餐</span>上面还有没有语音多一点的<span style='color:red;'>套餐</span>资费n0#七九零九的话他这个价位上已经是非常的便宜了n1#我知道我只要三百分钟不够打知道了n0#打完的时候它按照一分钟是一毛五来算的n1#现在不用<span style='color:red;'>钱</span>哦n0#最近呢两百都没回n1#哦就是超过三百分钟一毛五的n0#对呀n1#飞机那你先给我变更成九十九号的吗n0#啊|啊n1#二百九十九<span style='color:red;'>套餐</span>属于四g<span style='color:red;'>套餐</span>吗n0#这可能应该n1#哦行那你帮我变更一下好我帮您n0#而且您手机这边的话保持他跟我讲n1#啊行行行好|再见n0#给你吧n1#哦不用了谢谢n0#祝您生活愉快吗显示这样\",\"user\":{\"agentCode\":\"70050\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy1\",\"username\":\"邱赟赟1\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        } else if ("00A7ECF8681649B08CB9D90FF8C1B5F0".equals(param.get("contactId").toString())) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180621160200\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"16605925342\",\"closeType\":null,\"contactId\":\"00A7ECF8681649B08CB9D90FF8C1B5F0\",\"custArea\":\"0592\",\"custBand\":\"04\",\"custLevel\":\"500\",\"dataType\":\"v8\",\"endTime\":null,\"length\":226,\"planId\":null,\"planName\":null,\"qualityStatus\":\"0\",\"recordFile\":\"nfs2/1/voice/20180421/059210010/3431/20180421160540107.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":12,\"startTime\":\"20180621160200\",\"txtContent\":\"n0#三|四三n1#一|号n0#客服代表n1#为你服务n0#您好请问有什么可以帮您|哎你好我想问一下假如说|我|在出国n1#期间没有开通那个|刚才或者说是那个|嗯你说|全国吗然后我到那边您只要开通要怎么办呢n0#幺八六幺八六幺零零幺零|国际社对不对|零呃您是说您本机吗|本机的话开通国际客服热线的话呃不好意思|您要开通国际漫游功能的话是|需要登录手机营业厅|去开通n1#哦这样子n0#就是教育升档那个预存话费三百块<span style='color:red;'>钱</span>开通n1#哦这样子n0#对n1#啊|啊n0#嗯n1#嗯也就是说我是自己直接装一个五年中的已经n0#然后n1#等一些n0#哪一个n1#就可以了n0#对没错嗯n1#哦就不用说再去营业厅开呀或者说平时上班的是吧n0#对营业厅不能开n1#哦是自己在最大机上面开是吗n0#单没有做n1#啊那刚才我问前面我又打了一次今天又打一次我要确认一下刚才n0#我打你们客服n1#就文明路客服过来|他跟我说一定要去营业厅看n0#嗯哦您这个是您就是同时网卡腾讯网卡的话是在手机营业厅上开通n1#哦|那如果不是存去还款的话就是说假如说是那种小包卡呢n0#也是像您这种是属于互联网<span style='color:red;'>套餐</span>了|都是要在手机营业厅上开通n1#哦除了以前老的老的那种卡上去营业厅是吗n0#嗯规定就看一下您的<span style='color:red;'>套餐</span>呢有个老的卡的话|是自己存不用开通直接那个如果存两百或者三百块<span style='color:red;'>钱</span>吗n1#然后n0#请自动开通的呀n1#哦这样子只要您反应让你去那个业务然后您把<span style='color:red;'>钱</span>出去就ok了是吧n0#对没错|那您那个<span style='color:red;'>套餐</span>跟他的这个不一样n1#我有因为我一家人都在用吧因为我我总共是三十四兆嘛我一张两张上网卡n0#晚上那个|然后n1#是小宝卡的上网卡想老卡吗n0#嗯n1#如果说有出具我问就是说家里有出去玩去香港或者只要们那边然后忘记开的您要开的话要怎么办n0#手机营业厅手机营业厅开通|也算手机营业厅开通或者是|您现在有办理渠道您等一下啊n1#恩好你帮我看看|啊n0#嗯嗯|对呀没错手机营业厅开通|就是只有这个手机营业厅上开通或者|有对关闭也是在手机营业厅上开通也是在手机营业厅上开通的说我把路径下发到您手机在网上自己看|啊好然后n1#啊我看还有一个|假如说我|过去了完我没开通然后咨询还是咱们小说怎么改不了吗|那怎么办|短信也收不了n0#那有个国际客服热线幺八六|幺八六|幺零零幺零|对n1#呃打过去就可以了是吧n0#对他是我们联通这个国际客服热线号码对n1#哦|这样子n0#对n1#他说和平那给你发短信发给我我看一下我我详细看一下好吗这样n0#我发了您自己对他说呢n1#嗯好的n0#谢谢好不客气祝您生活愉快再见\",\"user\":{\"agentCode\":\"70051\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy2\",\"username\":\"邱赟赟2\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        } else if ("00A3A9ECD14C48AF84BC9C303A231A56".equals(param.get("contactId").toString())) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180620100627\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"18505910929\",\"closeType\":null,\"contactId\":\"00A3A9ECD14C48AF84BC9C303A231A56\",\"custArea\":\"0591\",\"custBand\":\"04\",\"custLevel\":\"500\",\"dataType\":\"v8\",\"endTime\":null,\"length\":147,\"planId\":null,\"planName\":null,\"qualityStatus\":\"1\",\"recordFile\":\"nfs2/1/voice/20180520/059110010/3655/20180520100849805.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":0,\"startTime\":\"20180620100627\",\"txtContent\":\"n1#三六五五|好四g代表为您服务n0#您好请问有什么可以帮您n1#你好我的这一个手机号是不是到|哦四个月|我的那个什么固定的<span style='color:red;'>套餐</span>结束了n0#啊不是它<span style='color:red;'>套餐</span>不会给您结束是活动您之前不是有个送手机这个活动到这个月底就结束了n1#哦但是您如果您n0#对您如果没有更改<span style='color:red;'>套餐</span>这个原有的一个<span style='color:red;'>套餐</span>了n1#还款号段条了太贵一个号码是解决不使用n0#但是您不是有副卡吗要去营业厅把付款解绑了才能更改别的一个<span style='color:red;'>套餐</span>n1#那我觉得方便我们银行不考什么的后面那个吗n0#付款为直接单独的一个<span style='color:red;'>套餐</span>啊n1#付款三十一号当时吗那我这个这个手机号呢n0#这个手机号就是您要更改别的<span style='color:red;'>套餐</span>也是更改别的<span style='color:red;'>套餐</span>吗n1#那我以后不管能不能再关注这个信息到账n0#有的<span style='color:red;'>套餐</span>是不支持n1#有的地方不仅是吗n0#对是的您要看一下<span style='color:red;'>套餐</span>是这样的您如果要解绑要去福州的自有营业厅才能一个嗯才能一个就是解绑的n1#不行我不能订<span style='color:red;'>套餐</span>吗我们不考虑如果顾客不行吗n0#啊那就是保持原有的<span style='color:red;'>套餐</span>就不能更改了n1#你们怎么怎么|公司的估计吗天天过去你说什么能不能这样的规定n0#他这个是要把卡n1#放到就原来告诉我n0#啊但是副卡的这边自己是绑定在一起是不能一个更改别的<span style='color:red;'>套餐</span>啊n1#我不敢跟他们为什么不行啊我也没如果不要用副卡了n0#它有这种一个月或者就是说您要把它解绑了才能更改别的<span style='color:red;'>套餐</span>吧当时的话n1#您就没有这项规定没有这样规定n0#嗯很抱歉给您带来不便n1#不是你当时就怕他的话你你我可以绑定方式了没有超过去嘛n0#嗯是要要是要有您这边要把这个直接半年才能更改别的套餐的n1#二|的呢|啊很抱歉给您带来不便如果稍后呢n0#嗯n1#证件号码的话那个人说还没收到我主号忘记了n0#嗯那您要机主本人携带身份证到归属地营业厅这边才能一个注销n1#我是说我是不需要那个附卡我不考虑一下吧n0#因为账户要把这个主副卡就是解绑掉吗然后您这边的话付款如果要使用就取消一个也可以您如果要注销的话那您|要不直接去营业厅就可以直接注销了n1#那我知道了嗯n0#好的很抱歉给您带来不便|嗯但是呢\",\"user\":{\"agentCode\":\"70050\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy1\",\"username\":\"邱赟赟1\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        } else if ("00A3F741B1104BA0B5A62B9EBFBAD3B3".equals(param.get("contactId").toString())) {
            recordJson = "{\"content\":{\"acceptTime\":\"20180619162327\",\"bussinessType\":\"03\",\"callee\":null,\"caller\":\"13003988579\",\"closeType\":null,\"contactId\":\"00A3F741B1104BA0B5A62B9EBFBAD3B3\",\"custArea\":\"0592\",\"custBand\":\"01\",\"custLevel\":\"200\",\"dataType\":\"v8\",\"endTime\":null,\"length\":176,\"planId\":null,\"planName\":null,\"qualityStatus\":\"0\",\"recordFile\":\"nfs1/1/voice/20180519/059210010/3679/20180519162615687.mp3\",\"satisfaction\":\"02\",\"serviceType\":\"010124\",\"sessionType\":null,\"silenceLength\":11,\"startTime\":\"20180619162327\",\"txtContent\":\"n0#三六七九|号客服代表为您服务|您好请问有什么帮您n1#喂你好|就是你帮我看一下我这张卡人的<span style='color:red;'>流量</span>吗n0#稍等|感谢您耐心等待先生您这张卡片有<span style='color:red;'>流量</span>啊感谢您<span style='color:red;'>套餐</span>里的话就是有一个国内<span style='color:red;'>流量</span>三百兆|就是说呃国内三百兆<span style='color:red;'>流量</span>了然后的话您现在您的业务去范围内是有三点一个g的功能呃省内的业务业务就n1#业务是哪里n0#您的业务去的话我这边看到是在二零一四一每海沧业务群n1#那海沧业务是由基地那边呢n0#校园的业务线n1#哦就是说|如果是到别的地方的话就只有三百兆的n0#但是n1#三百兆|现在有没有什么|呃|有没有什么|<span style='color:red;'>流量</span><span style='color:red;'>套餐</span>n0#您稍等您这个<span style='color:red;'>流量</span><span style='color:red;'>套餐</span>需要省内还是国内的呢先生n1#就是|嗯就是不愿整的吗n0#就是像那个是吗您稍等帮您看一下n1#就是有没有什么优惠的<span style='color:red;'>流量</span><span style='color:red;'>套餐</span>吗|因为我现在|现在刚好说要要是联系完符合排行要用<span style='color:red;'>流量</span>吗n0#感谢您耐心等待目前看到您能够办理的省内<span style='color:red;'>流量</span>包是呃<span style='color:red;'>流量</span>畅想的二十块<span style='color:red;'>钱</span>一个g送一个g|三十块<span style='color:red;'>钱</span>是两个g送两个g|然后六十块<span style='color:red;'>钱</span>是五个g送五个g的n1#你刚才说的那个什么二十块<span style='color:red;'>钱</span>一个g是吧n0#呃对一个g送您的信息n1#怎么一个季度一个月n0#一个g送一个g一千零二十四兆送一千零二十四兆|就是说n1#二十块<span style='color:red;'>钱</span>买|买一个自动一个月n0#对是的n1#就是在福建省内都可以用的是吧n0#但是呢n1#就是说两句吗n0#对是的n1#好帮我开一个吧n0#那这个的话是要通过短信方式进行开通稍后下发短信给您您根据短信提示操作一下|那这个如果您直接帮我开就好了你那边|很抱歉这个在线我们开通不了n1#那那你发过来吧n0#已经下发短信给您了您稍后根据短信提示操作一下开通成功之后是立即生效如果您本月没有来电退订的话下个月是自动续订的|我现在n1#我现在回复立马立马生效吧n0#开通成功之后是立即生效如果您本月没有来电退您下个月是自动续零的|建议您详细阅读一下短信内容了解一下<span style='color:red;'>流量</span>包使用规则|给您下发给您了还有什么其他问题呢|那祝您生活愉快再见下您说\",\"user\":{\"agentCode\":\"70050\",\"department\":null,\"deptName\":null,\"userCode\":\"qiuyy1\",\"username\":\"邱赟赟1\"}},\"errorMessage\":\"\",\"errorType\":null,\"success\":true}";
        }

        if ("".equals(recordJson)) {
            record = commonService.getHighlightRecord(param);
        } else {
            return JSONObject.parseObject(recordJson, ServiceResult.class);
        }

        return HollysqmUtil.wrapUpPageResult(record, true, "");
    }

    /**
     * @Description:坐席申诉质检单
     * @param:@param param
     * @param:@return 设定文件
     * @return:ServiceResult 返回类型
     * @throws:
     * @author:liujr 2017年3月22日
     */
    @RequestMapping(value = "/appealPaper", method = RequestMethod.POST)
    public ServiceResult appealPaper(@RequestParam Map<String, Object> param) {
        boolean flag = false;
        String errorMessage = "";
        try {
            commonService.appealPaper(param);
            flag = true;
        } catch (Exception e) {
            errorMessage = e.getMessage();
            e.printStackTrace();
        }

        return HollysqmUtil.wrapUpPageResult(null, flag, errorMessage);
    }

    /**
     * @Description:审核坐席申诉
     * @param:@param param
     * @param:@return 设定文件
     * @return:ServiceResult 返回类型
     * @throws:
     * @author:liujr 2017年3月22日
     */
    @RequestMapping(value = "/doAppealPaper", method = RequestMethod.POST)
    public ServiceResult doAppealPaper(@RequestParam Map<String, Object> param) {
        boolean flag = false;
        String errorMessage = "";
        try {
            commonService.doPaperAppeal(param);
            flag = true;
        } catch (Exception e) {
            errorMessage = e.getMessage();
            e.printStackTrace();
        }

        return HollysqmUtil.wrapUpPageResult(null, flag, errorMessage);
    }

    /**
     * @Description:查询申诉记录
     * @param:@param param
     * @param:@return 设定文件
     * @return:ServiceResult 返回类型
     * @throws:
     * @author:liujr 2017年3月30日
     */
    @RequestMapping(value = "/queryAppealPaper", method = RequestMethod.GET)
    public ServiceResult queryAppealPaper(@RequestParam Map<String, Object> param) {
        boolean flag = false;
        String errorMessage = "";
        List<PaperAppeall> list = null;
        try {
            list = commonService.queryPaperAppeal(param);
            flag = true;
        } catch (Exception e) {
            errorMessage = e.getMessage();
            e.printStackTrace();
        }

        return HollysqmUtil.wrapUpPageResult(list, flag, errorMessage);
    }

    /**
     * @Description:获取评分模板（查询页面上的下拉框）
     * @param:@param param
     * @param:@return 设定文件
     * @return:ServiceResult 返回类型
     * @throws:
     * @author:liujr 2017年4月19日
     */
    @RequestMapping(value = "/queryStandardList", method = RequestMethod.POST)
    public Object queryStandardList(@RequestParam Map<String, Object> param) {
        List<LabelValue> list = commonService.queryStandardList(param);
        return HollysqmUtil.wrapUpPageResult(list, true, "");
    }

    /**
     * @Description:获取所有标签项
     * @param:@param param
     * @param:@return 设定文件
     * @return:Object 返回类型
     * @throws:
     * @author:liujr 2017年4月19日
     */
    @RequestMapping(value = "/queryTextItem", method = RequestMethod.GET)
    public Object queryTextItem(@RequestParam Map<String, Object> param) {
        List<TextItem> list = commonService.queryTextItem();
        return HollysqmUtil.wrapUpPageResult(list, true, "");
    }

    /**
     * @Description:获取质检计划下拉数据
     * @param:@param param
     * @param:@return 设定文件
     * @return:Object 返回类型
     * @throws:
     * @author:liujr 2017年4月19日
     */
    @RequestMapping(value = "/queryPlan", method = RequestMethod.POST)
    public Object queryPlan(@RequestParam Map<String, Object> param) {
        List<LabelValue> list = commonService.queryPlan();
        return HollysqmUtil.wrapUpPageResult(list, true, "");//JsonUtils.toJSON(list);
    }


    /**
     * 生成接触记录的音频文件和波形图文件路径
     *
     * @param param
     * @param request
     * @return
     */
    @RequestMapping(value = "/getRecordDir", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getRecordDir(@RequestParam Map<String, Object> param, HttpServletRequest request, HttpSession session) {
        String path = request.getSession().getServletContext().getRealPath("/");
        String contactId = "";
        if (StringUtils.isEmpty(param.get("contactId")) || StringUtils.isEmpty(param.get("acceptTime")) || StringUtils.isEmpty(param.get("recordFile"))) {
            return HollysqmUtil.wrapUpPageResult(null, false, "");
        } else {
            contactId = param.get("contactId").toString();
        }

        Record record = new Record();
        record.setAcceptTime(param.get("acceptTime").toString());
        record.setRecordFile(param.get("recordFile").toString());
        record.setContactId(param.get("contactId").toString());

        File imgFile = new File(path + "\\record\\" + contactId + ".jpg");
        File mp3File = new File(path + "\\record\\" + contactId + ".mp3");

        RecordDir recordDir = null;
        if (imgFile.exists() && mp3File.exists()) {
            recordDir = new RecordDir();
            recordDir.setImgDir("record/" + contactId + ".jpg");
            recordDir.setWavDir("record/" + contactId + ".mp3");
        } else {
            recordDir = commonService.downloadRecord(record, path);
            //recordDir = commonService.downloadRecordTest(record,path);
        }

        return HollysqmUtil.wrapUpPageResult(recordDir, true, "");
    }

    /**
     * 标签预览
     *
     * @param param
     * @return
     */
    @RequestMapping(value = "/itemPreview", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult itemPreview(@RequestParam Map<String, Object> param) {
        PageResult<Record> page = commonService.itemPreview(param);
        return HollysqmUtil.wrapUpPageResult(page, true, "");
    }

    /**
     * 通过标签内容，高亮预览指定文本内容
     *
     * @param param
     * @return
     */
    @RequestMapping(value = "/highlightPreview", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult highlightPreview(@RequestParam Map<String, Object> param) {
        String txtContent = commonService.highlightPreview(param);
        return HollysqmUtil.wrapUpPageResult(txtContent, true, "");
    }


    /**
     * @Description:全文检索导出
     * @param:@param tableId
     * @param:@param options
     * @param:@param exportType
     * @param:@param param
     * @param:@param request
     * @param:@param response
     * @param:@throws Exception    设定文件
     * @return:void 返回类型
     * @throws:
     * @author:liujr 2017年4月26日
     */
    @ActionLog(content = "导出rest方法", description = "调用基类统一的导出列表方法")
    @RequestMapping(value = "/export", method = RequestMethod.POST)
    public void export(
            @RequestParam String tableId,
            @RequestParam(required = false) List<Integer> options,
            @RequestParam(required = false, defaultValue = "csv") String exportType,
            @RequestParam Map<String, Object> param,
            HttpServletRequest request, HttpServletResponse response)
            throws Exception {
        logger.debug("导出rest方法 ,查询参数：" + param);
        // 1.从DB获取数据

        List<V8PaperExportTemplate> list = new ArrayList<V8PaperExportTemplate>();
        Record record = commonService.getRecord(param);
        V8PaperExportTemplate v8Template = (V8PaperExportTemplate) HollysqmUtil.parseRecordToExportTemplate(record);
        HollysqmUtil.fillParam(param, V8PaperExportTemplate.class, v8Template);
        //测试数据
        v8Template.setErrorType("测试");
        v8Template.setIsFatal("测试");
        v8Template.setNeedCorrect("测试");
        v8Template.setServiceRequest("测试");

        list.add(v8Template);
        List<? extends CsvField> fields = csvConfig.getExportedCsvFields(tableId);
        //fields = this.expandFields(request, fields);
        // 2.写入临时文件
        File tempDir = WebUtils.getTempDir(request.getServletContext());
        File tmpFile = null;
        if ("pdf".equalsIgnoreCase(exportType)) {
            tmpFile = new File(tempDir, UUID.randomUUID().toString() + ".pdf");
            if (options != null) {
                pdfExport.expToFile(list, tableId, tmpFile, fields, options);
            } else {
                pdfExport.expToFile(list, tableId, tmpFile, fields);
            }
        } else if ("Excle".equalsIgnoreCase(exportType)) {
            tmpFile = new File(tempDir, UUID.randomUUID().toString() + ".xls");
            if (options != null) {
                excleExport.expToFile(list, tableId, tmpFile, fields, options);
            } else {
                excleExport.expToFile(list, tableId, tmpFile, fields);
            }
        } else {
            tmpFile = new File(tempDir, UUID.randomUUID().toString() + ".csv");
            if (options != null) {
                csvExport.expToFile(list, tableId, tmpFile, fields, options);
            } else {
                csvExport.expToFile(list, tableId, tmpFile, fields);
            }
        }
        logger.debug("生成文件存储位置:" + tmpFile.getAbsolutePath());
        // 3.下载
        InputStream in = new FileInputStream(tmpFile);
        WebUtils.download(response, in, tmpFile.getName());
    }

    /**
     * 获取服务类型
     *
     * @param param
     * @return
     */
    @RequestMapping(value = "/getServiceType", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getServiceType(@RequestParam Map<String, Object> param) {
        List<ServiceType> list = commonService.getServiceType(param);
        ServiceType serviceType = new ServiceType();
        serviceType.setId("0");
        serviceType.setText("所有类型");
        serviceType.setChecked("checked");

        StringBuilder json = new StringBuilder("[");
        generateTreeJson(serviceType, list, json);
        json.append("]");
        System.out.println(json.toString());
        return HollysqmUtil.wrapUpPageResult(json.toString(), true, "");
    }

    /**
     * 生成服务类型的json字符串
     *
     * @param serviceType
     * @param list
     * @param json
     */
    public void generateTreeJson(ServiceType serviceType, List<ServiceType> list, StringBuilder json) {
        json.append("{");
        json.append("'id':");
        json.append("'" + serviceType.getId() + "',");
        json.append("'text':");
        json.append("'" + serviceType.getText() + "'");

        boolean flag = false;
        for (ServiceType s : list) {
            if (s.getParentId().equals(serviceType.getId())) {
                json.append(",'children':[");
                flag = true;
                break;
            }
        }

        if (flag) {
            for (int i = 0; i < list.size(); i++) {
                if (list.get(i).getParentId().equals(serviceType.getId())) {
                    generateTreeJson(list.get(i), list, json);
                    json.append(",");
                }
                if (i == list.size() - 1) {
                    json.deleteCharAt(json.length() - 1);
                }
            }
            json.append("]");
        }
        json.append("}");
    }

    @RequestMapping(value = "/getServiceTypeNames", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult getServiceTypeNames(@RequestParam Map<String, Object> param) {
        List<ServiceType> list = commonService.getServiceTypeNames(param);

        return HollysqmUtil.wrapUpPageResult(list, true, "");
    }

    /**
     * 定位违规语音展示
     *
     * @param param
     * @return
     */
    @SuppressWarnings({"resource"})
    @RequestMapping(value = "/illegalShow", produces = MediaType.APPLICATION_JSON_VALUE)
    public ServiceResult illegalShow(@RequestParam Map<String, Object> param) {
        return HollysqmUtil.wrapUpPageResult(null, false, "");
//		if (StringUtils.isNotEmpty(param.get("id"))) {
//			String result = "";
//			String  handleUrl = AppConfigUtils.getConfig().getString("hollysqm.rule.planurl");
//			String url = handleUrl + param.get("id").toString();
//			try {
//				HttpGet request = new HttpGet(url);// 这里发送get请求
//				HttpClient httpClient = new DefaultHttpClient();
//				HttpResponse response = httpClient.execute(request);
//				if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
//					result = EntityUtils.toString(response.getEntity(), "utf-8");
//				}
//			} catch (Exception e) {
//				e.printStackTrace();
//			}
//			return HollysqmUtil.wrapUpPageResult(result, true, "");
//		} else {
//			return HollysqmUtil.wrapUpPageResult(null, false, "");
//		}
    }
}
