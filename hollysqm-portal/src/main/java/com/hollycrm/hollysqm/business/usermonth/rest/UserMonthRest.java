package com.hollycrm.hollysqm.business.usermonth.rest;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollysqm.business.usermonth.service.UsermonthService;
import com.hollycrm.hollysqm.entities.EchartData;
import com.hollycrm.hollysqm.util.HollysqmUtil;


@RestService
@RequestMapping(value="/userMonth")
public class UserMonthRest {

	@Resource(name="usermonthService")
	private UsermonthService usermonthService;

	/** 
	* @Description:T满意度调查统计
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年8月1日
	*/
	@RequestMapping(value="/satisfactionStatistics",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult getUnscoredPaperList(@RequestParam Map<String,Object> param) {
		List<EchartData> list = usermonthService.satisfactionStatistics(param);
		
		return HollysqmUtil.wrapUpPageResult(list, true, "");
	}
	/** 
	* @Description:评分模板统计
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年8月1日
	*/
	@RequestMapping(value="/standartStatistics",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult standartStatistics(@RequestParam Map<String,Object> param) {
		List<EchartData> list = usermonthService.standartStatistics(param);
		
		return HollysqmUtil.wrapUpPageResult(list, true, "");
	}
	/** 
	* @Description:坐席成绩趋势统计 
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年8月1日
	*/
	@RequestMapping(value="/scoreStatistics",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult scoreStatistics(@RequestParam Map<String,Object> param) {
		List<EchartData> list = usermonthService.scoreStatistics(param);
		
		return HollysqmUtil.wrapUpPageResult(list, true, "");
	}

	/** 
	* @Description:TODO(这里用一句话描述这个方法的作用) 
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年8月2日
	*/
	@RequestMapping(value="/standardDetailStatistics",  produces = MediaType.APPLICATION_JSON_VALUE)
	public ServiceResult standardDetailStatistics(@RequestParam Map<String,Object> param) {
		List<EchartData> list = usermonthService.standardMonthStatistics(param);
		
		return HollysqmUtil.wrapUpPageResult(list, true, "");
	}

	

}
