package com.hollycrm.hollysqm.business.workcount.rest;

import com.hollycrm.hollybeacon.basic.core.annotations.RestService;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollysqm.business.workcount.service.WorkCountService;
import com.hollycrm.hollysqm.entities.DataRank;
import com.hollycrm.hollysqm.entities.DataStatus;
import com.hollycrm.hollysqm.util.HollysqmUtil;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.annotation.Resource;
import java.util.List;
import java.util.Map;

/**
 * @author liujr
 * 2017年3月30日
 * @Description 质检员工作统计
 */
@RestService
@RequestMapping(value="/workcount")
public class WorkCountRest {

	@Resource(name="workCountService")
	private WorkCountService workCountService;
	
	/** 
	* @Description:返回按评分量排名
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月22日
	*/
	@RequestMapping(value="/workRank",method = RequestMethod.GET)
	public ServiceResult workRank(@RequestParam Map<String,Object> param){
		List<DataRank> list = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			list = workCountService.workRank(param);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(list, flag, errorMessage);
	}
	
	/** 
	* @Description:当前操作人质检数量
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月22日
	*/
	@RequestMapping(value="/workCount",method = RequestMethod.GET)
	public ServiceResult workCount(@RequestParam Map<String,Object> param){
		DataStatus status = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			status = workCountService.mySelfWorkCount(param);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(status, flag, errorMessage);
	}
	
	/** 
	* @Description:所有质检员平均质检数量
	* @param:@param param
	* @param:@return    设定文件 
	* @return:ServiceResult    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月22日
	*/
	@RequestMapping(value="/averWorkCount",method = RequestMethod.GET)
	public ServiceResult averWorkCount(@RequestParam Map<String,Object> param){
		DataStatus status = null;
		boolean flag = false;
		String errorMessage = "";
		try {
			status = workCountService.averWorkCount(param);
			flag = true;
		} catch (Exception e) {
			errorMessage = e.getMessage();
			e.printStackTrace();
		}
		
		return HollysqmUtil.wrapUpPageResult(status, flag, errorMessage);
	}
	
}
