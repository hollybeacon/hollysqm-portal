package com.hollycrm.hollysqm.business.common.dao;

import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.orm.dao.jpa.JpaBaseDaoImpl;
import com.hollycrm.hollysqm.entities.Checker;

@Service("checkerDao")
public class CheckerDao extends JpaBaseDaoImpl<Checker, String>{

}
