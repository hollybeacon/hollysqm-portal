package com.hollycrm.hollysqm.business.common.dao;

import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.orm.dao.jpa.JpaBaseDaoImpl;
import com.hollycrm.hollysqm.entities.ScoreRecordData;

/**
 * 质检成绩Dao
 * @author wangyf
 *
 * 2017年3月23日
 */
@Service("scoreRecordDao")
public class ScoreRecordDao extends JpaBaseDaoImpl<ScoreRecordData, String>{

}
