package com.hollycrm.hollysqm.business.qualitymanagement.service;

import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.rest.web.util.WebUtil;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollybeacon.business.personoa.system.cache.DictionaryCacheHandler;
import com.hollycrm.hollysqm.business.common.dao.UserDao;
import com.hollycrm.hollysqm.business.common.service.CommonService;
import com.hollycrm.hollysqm.entities.Paper;
import com.hollycrm.hollysqm.entities.PaperAppeall;
import com.hollycrm.hollysqm.entities.UnsolvedPaper;
import com.hollycrm.hollysqm.util.HollysqmUtil;

@Service("qualitymanagementService")
public class QualitymanagementService {
	@Autowired
	private ConfigedJdbc configedJdbc;
	
	@Resource(name="usersDao")
	private UserDao userDao;
	
	@Resource(name = "dictionaryCacheHandler")
	private DictionaryCacheHandler dictionaryCacheHandler;
	
	@Resource(name="commonService")
	private CommonService commonService;
	
	public String getDeptIds(Map<String, Object> param) throws Exception{
		commonService.setParam(param);
		return MapUtils.getString(param, "deptId", "");
	}
	
	public PageResult<Paper> qualitymanagementList(Map<String, Object> param) {
		@SuppressWarnings("unchecked")
		PageResult<Paper> page = (PageResult<Paper>)HollysqmUtil.createPageResult(param);	
		
		String sqlKey = "velocity.score.queryQuality";
		page = configedJdbc.selectPageResult(sqlKey, param, page, Paper.class);
		Long lon = configedJdbc.selectCount(sqlKey, param);
		page.setTotal(lon);	
		
		return page;
	}
	
	public UnsolvedPaper nextQualitymanagement(Map<String, Object> param) {
		if(StringUtils.isEmpty(param.get("paperId"))) {
			return null;
		}
		
		String userCode = WebUtil.getLoginUser().getUserCode();
		param.put("executeCode", userCode);
		param.put("paperId", param.get("paperId"));
		param.put("status", "3");//待处理
		
		String sqlKey = "velocity.score.queryQuality";
		commonService.setParam(param);
		List<Map<String,Object>> list = configedJdbc.selectAllList(sqlKey, param);
		if(list == null || list.isEmpty()) {
			return null;
		} else {
			UnsolvedPaper paper = new UnsolvedPaper();
			paper.setPaperCount(list.size());
			paper.setNextPaperId(MapUtils.getString(list.get(0),"paperId")+","+MapUtils.getString(list.get(0),"appealId"));
			return paper;
		}
	}
	
	public PaperAppeall get(String id){
		PaperAppeall pa = userDao.get(id, PaperAppeall.class);
		return pa;
	}
	
	public List<PaperAppeall> paperAppealList(Map<String,Object> param){
		List<PaperAppeall> list = userDao.findList(" from PaperAppeall t where t.paperId = :paperId order by t.createTime,t.executeime", PaperAppeall.class, param);
		return list;
	}
}
