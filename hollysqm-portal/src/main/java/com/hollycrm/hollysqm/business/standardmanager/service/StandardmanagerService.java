package com.hollycrm.hollysqm.business.standardmanager.service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.apache.commons.collections.MapUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.jdbc.ConfigedJdbc;
import com.hollycrm.hollybeacon.basic.orm.service.jpa.JpaBaseServiceImpl;
import com.hollycrm.hollybeacon.basic.rest.web.util.WebUtil;
import com.hollycrm.hollybeacon.basic.util.DateUtils;
import com.hollycrm.hollysqm.business.common.dao.ScoreItemDao;
import com.hollycrm.hollysqm.entities.ScoreItem;
import com.hollycrm.hollysqm.entities.ScoreList;
import com.hollycrm.hollysqm.entities.Standard;
import com.hollycrm.hollysqm.util.UuidUtil;

@Service("standardManagerService")
public class StandardmanagerService extends JpaBaseServiceImpl<Standard, String>{

	@Autowired
	private ConfigedJdbc configedJdbc;
	
	@Resource(name="scoreItemDao")
	private ScoreItemDao scoreItemDao;
	
	/** 
	* @Description:添加或修改评分模板
	* @param:@param params    设定文件 
	* @return:void    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public String saveOrUpdate(Map<String,Object> params){
		String standardId = MapUtils.getString(params , "standardId");//主键
		String standardName = MapUtils.getString(params , "standardName");//评分模板名称
		Integer totalScore = MapUtils.getInteger(params , "totalScore",0);//总分
		String isValid = MapUtils.getString(params , "isValid","1");//
		String status = MapUtils.getString(params , "status","0");//状态：0:未发布，1:已发布
		String remark = MapUtils.getString(params , "remark");//备注
		String domainId = WebUtil.getLoginUser().getDomainId();//租户ID
		String areaId = WebUtil.getLoginUser().getOrgId();//区域ID
		String isUpdate = MapUtils.getString(params , "isUpdate");//是否可修改,0:否(可编辑) 1:是（不可编辑）
		String currentTime = DateUtils.getCurrentDateAsString(DateUtils.FORMAT_DATE_YYYY_MM_DD_HH_MM_SS);
		String userCode = WebUtil.getLoginUser().getUserCode();
		String sql = "select t.standard_name from tbl_qm_standard t where t.standard_name = '"+standardName.trim()+"'";
		//判断是否有重复评分模板名称
		long lon = configedJdbc.selectCountBySql(sql, new HashMap<String,Object>());
		Standard standard = get(standardId);
		standard = standard == null ? new Standard() : standard;
		
		if(lon > 0 && !standardName.equals(standard.getStandardName())){
			return "1";
		}
		standard.setIsValid(isValid);//修改评分模板后，其状态自动变为停用
		standard.setModifier(userCode);
		standard.setModifyTime(currentTime);
		standard.setStandardName(standardName);
		standard.setTotalScore(totalScore);
		standard.setStatus(status);
		standard.setRemark(remark);
		standard.setAreaId(areaId);
		standard.setDomainId(domainId);
		standard.setIsUpdate(isUpdate);
		if(standardId == null || "".equals(standardId)){
			
			standard = save(standard);
		}else
			update(standard);
		
		String[] scoreArray = MapUtils.getString(params , "scoreArray").split(",");//关联的评分项
		String[] sqls = new String[scoreArray.length];
		List<String> scoreList = new ArrayList<String>();
		for(int i=0; i<scoreArray.length; i++) {
			scoreList.add(scoreArray[i]);
		}
		
		List<ScoreItem> scoreitems = scoreItemDao.findList("from ScoreItem s where s.standardId = :standardId", params);
		int orderNo = scoreitems.size();
		if(configedJdbc.selectCountBySql("select * from tbl_qm_plan where standard_id = :standardId", params) > 0) {
			for(ScoreItem s :scoreitems) {
				scoreList.remove(s.getScoreListId());
			}
			
			ScoreItem scoreItem  = null;
			for(String s : scoreList) {
				scoreItem = new ScoreItem();
				orderNo ++;
				scoreItem.setOrderNo(orderNo);
				scoreItem.setScoreItemId(UuidUtil.getUuid());
				scoreItem.setScoreListId(s);
				scoreItem.setStandardId(standardId);
				scoreItemDao.save(scoreItem);
			}
		} else {
			//删除之前关联的评分项
			configedJdbc.getJdbcTemplate().execute("delete from TBL_QM_SCORE_ITEM where standard_id = '"+standard.getStandardId()+"'");
			for(int i=0; i < scoreArray.length;i++){
				sqls[i] = "insert into TBL_QM_SCORE_ITEM (score_item_id,standard_id,score_list_id,order_no,remark) values(REPLACE(UUID(),'-',''),'"+standard.getStandardId()+"','"+scoreArray[i]+"',"+(i+1)+",'')";
			}
			//批量添加评分项
			configedJdbc.getJdbcTemplate().batchUpdate(sqls);
		}
		
		return "0";
	}
	
	/** 
	* @Description:启用或停用评分模板
	* @param:@param params
	* @param:@return    设定文件 
	* @return:boolean    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public boolean enable(Map<String,Object> params){
		String standardId = MapUtils.getString(params , "standardId");
		String status = MapUtils.getString(params , "status");
		String currentTime = DateUtils.getCurrentDateAsString(DateUtils.FORMAT_DATE_YYYY_MM_DD_HH_MM_SS);
		String userCode = WebUtil.getLoginUser().getUserCode();
		
		Standard standard = get(standardId);
		standard.setStatus("1".equals(status)?"0":"1");
		standard.setModifier(userCode);
		standard.setModifyTime(currentTime);
		update(standard);
		return false;
	}
	
	/** 
	* @Description:评分模板分页查询
	* @param:@param page
	* @param:@param params
	* @param:@return    设定文件 
	* @return:PageResult<Standard>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public PageResult<Standard> queryStandard(PageResult<Standard> page,Map<String,Object> params){
		String sqlKey = "velocity.manager.queryStandardManager";
		Long lon = configedJdbc.selectCount(sqlKey, params);
		
		page = configedJdbc.selectPageResult(sqlKey, params, page, Standard.class);
		page.setTotal(lon);
		
		return page;
	}
	

	/** 
	* @Description:查看评分模板
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Standard    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public Standard checkStandard(Map<String, Object> param) {
		String standardId = MapUtils.getString(param , "standardId");
		String sql = "select l.score_list_id scoreListId,l.item_name itemName,l.score,l.score_type scoreType,l.item_type itemType,l.text_item_id textItemId,l.remark,l.modifier,l.modify_time modifyTime "
				+ "from TBL_QM_STANDARD s,TBL_QM_SCORE_ITEM i,TBL_QM_SCORE_LIST l "
				+ "where s.standard_id = i.standard_id  and i.score_list_id = l.score_list_id and i.standard_id = :standardId";
		String sql1 = "select count(t2.paper_id) paperCount,count(t3.plan_id) planCount from tbl_qm_standard t1 left join tbl_qm_paper t2 on t1.standard_id = t2.standard_id left join tbl_qm_plan t3 on t1.standard_id = t3.standard_id where  t1.standard_id = :standardId";
		
		Standard standard = get(standardId);
		List<ScoreList> list = configedJdbc.queryForList(sql, param, ScoreList.class);
		if(standard == null)
			standard = new Standard();
		else
			standard.setScoreList(list);
		List<Map<String,Object>> list1 = configedJdbc.queryForList(sql1, param);
		Map<String,Object> map = list1.get(0);
		int paperCount = MapUtils.getInteger(map, "paperCount");//质检单使用评分模板量
		int planCount = MapUtils.getInteger(map, "planCount");//质检计划使用评分模板量
		if(paperCount == 0 && planCount == 0){//如果都没有使用该评分模板，就可以对评分项进行删除操作
			standard.setIsUpdate("0");
		}else 
			standard.setIsUpdate("1");
		return standard;
	}
	
	/** 
	* @Description:获取所有评分项数据
	* @param:@return    设定文件 
	* @return:List<?>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年3月30日
	*/
	public List<?> queryScoreAllList(){
		List<?> list = this.findListToObject(" from ScoreList");
		return list;
	}
	
}
