package com.hollycrm.hollysqm.business.common.dao;

import org.springframework.stereotype.Service;

import com.hollycrm.hollybeacon.basic.orm.dao.jpa.JpaBaseDaoImpl;
import com.hollycrm.hollysqm.entities.ScoreItem;

/**
 * 评分项
 * @author wangyf
 *
 * 2017年6月6日
 */
@Service("scoreItemDao")
public class ScoreItemDao extends JpaBaseDaoImpl<ScoreItem,String> {

}
