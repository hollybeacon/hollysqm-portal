package com.hollycrm.hollysqm.util;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.hollycrm.hollybeacon.basic.csv.impl.BaseConverter;
import com.hollycrm.hollybeacon.basic.util.LabelValue;
import com.hollycrm.hollybeacon.basic.util.PatternMatchUtils;
import com.hollycrm.hollybeacon.basic.util.SpringUtil;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollybeacon.business.personoa.system.cache.DictionaryCacheHandler;

/**
 * @author liujr
 * 2017年4月26日
 * @Description 导出功能自定义类
 */
public class CsvExportConverter extends BaseConverter {
	private static final Logger logger = LoggerFactory
			.getLogger(CsvExportConverter.class);
	
	private DictionaryCacheHandler dictionaryCacheHandler;
	private boolean isImport = true;
	
	public CsvExportConverter(){
		try {
			isImport = isImportReq();
			dictionaryCacheHandler = SpringUtil.getBean("dictionaryCacheHandler",DictionaryCacheHandler.class);
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	/** 
	* @Description:获取星级
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年4月26日
	*/
	public Object getCheckScore(Object param) {
		logger.info("参数：",param);
		Map<String, Object>  checkScoreMap = initMap("CHECK_SCORE");
		return checkScoreMap == null || param == null ? null : checkScoreMap.get(param.toString());
	}
	
	/** 
	* @Description:获取质检类型
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年4月26日
	*/
	public Object getDataType(Object param){
		logger.info("参数：",param);
		Map<String, Object>  dataTypeMap = initMap("DATATYPE");
		return dataTypeMap == null || param == null ? null : dataTypeMap.get(param.toString()); 
	}
	
	/** 
	* @Description:获取v8用户级别
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:wangyf
	* 2017年6月13日
	*/
	public Object getCustLevel(Object param){
		logger.info("参数：",param);
		Map<String, Object>  dataTypeMap = initMap("CUST_LEVEL");
		return dataTypeMap == null || param == null ? null : dataTypeMap.get(param.toString()); 
	}
	
	/** 
	* @Description:获取v8用户归属地
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:wangyf
	* 2017年6月13日
	*/
	public Object getCustArea(Object param){
		logger.info("参数：",param);
		Map<String, Object>  dataTypeMap = initMap("CUST_AREA");
		return dataTypeMap == null || param == null ? null : dataTypeMap.get(param.toString()); 
	}

	/** 
	* @Description:获取v8用户品牌
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:wangyf
	* 2017年6月13日
	*/
	public Object getCustBand(Object param){
		logger.info("参数：",param);
		Map<String, Object>  dataTypeMap = initMap("CUST_BAND");
		return dataTypeMap == null || param == null ? null : dataTypeMap.get(param.toString()); 
	}
	
	/** 
	* @Description:获取v8业务类型
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:wangyf
	* 2017年6月13日
	*/
	public Object getBusinessType(Object param){
		logger.info("参数：",param);
		Map<String, Object>  dataTypeMap = initMap("BUSINESS_TYPE");
		return dataTypeMap == null || param == null ? null : dataTypeMap.get(param.toString()); 
	}
	
	/** 
	* @Description:获取是否典型案例
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:wangyf
	* 2017年6月13日
	*/
	public Object getRecordType(Object param){
		logger.info("参数：",param);
		if(StringUtils.isEmpty(param)) {
			return null;
		} else {
			if(param.equals("1")) {
				return "是";
			} else {
				return "否";
			}
		}
	}
	
	/** 
	* @Description:获取是否发布成绩
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:wangyf
	* 2017年6月13日
	*/
	public Object getPublicResult(Object param){
		logger.info("参数：",param);
		if(StringUtils.isEmpty(param)) {
			return null;
		} else {
			if(param.equals("0")) {
				return "不发布成绩";
			} else {
				return "发布成绩";
			}
		}
	}
	
	/** 
	* @Description:获取v8满意度
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:wangyf
	* 2017年6月13日
	*/
	public Object getV8Satisfaction(Object param){
		logger.info("参数：",param);
		Map<String, Object>  dataTypeMap = initMap("V8SATISFACTION");
		return dataTypeMap == null || param == null ? null : dataTypeMap.get(param.toString()); 
	}
	
	/** 
	* @Description:获取质检状态
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年4月26日
	*/
	public Object getQualityStatus(Object param){
		logger.info("参数：",param);
		Map<String, Object> map = initMap("QUALITY_STATUS");
		return map == null || param == null ? null : map.get(param.toString()); 
	}
	
	/** 
	* @Description:获取i8关闭方式
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年5月5日
	*/
	public Object getCloseType(Object param){
		logger.info("参数：",param);
		Map<String, Object> map = initMap("CLOSE_TYPE");
		System.out.println(map.get(param.toString()));
		return map == null || param == null ? null : map.get(param.toString()); 
	}
	
	/** 
	* @Description:获取i8会话等级
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年5月5日
	*/
	public Object getSessionType(Object param){
		logger.info("参数：",param);
		Map<String, Object> map = initMap("SESSION_TYPE");
		return map == null || param == null ? null : map.get(param.toString()); 
	}
	/** 
	* @Description: 录音或聊天文件中编码替换
	* @param:@param param
	* @param:@return    设定文件 
	* @return:Object    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年4月28日
	*/
	public Object getTxtContent(Object param){
		logger.info("参数：",param);
		return param == null ? null : param.toString().replaceAll("n0#", "坐席:").replaceAll("n1#", "客户:"); 
	}
	
	
	/** 
	* @Description:将list对象转换成map对象
	* @param:@param list
	* @param:@param isImport
	* @param:@return    设定文件 
	* @return:Map<String,Object>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年4月26日
	*/
	public Map<String, Object> initMapResult(List<LabelValue> list,
			boolean isImport) {
		Map<String, Object> result = new HashMap<String, Object>();
		if (list != null && !list.isEmpty()) {
			for (LabelValue r : list) {
				if (isImport) {// 瀵煎叆鏃朵负Name鎵綱alue
					result.put(r.getName(), r.getValue());
				} else {// 瀵煎嚭鏃禫alue鎵綨ame
					result.put(r.getValue(), r.getName());
				}
			}
		}
		return result;
	}
	
	/** 
	* @Description:判断是否导出
	* @param:@return    设定文件 
	* @return:boolean    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年4月26日
	*/
	protected static boolean isImportReq() {
		String uri = getRequest().getRequestURI();
		logger.debug("export/import url :" + uri);

		return PatternMatchUtils.simpleMatch("*/rest/*/import", uri )||uri.endsWith("/csv/exportInValidRecord")||uri.endsWith("/csv/sqlimport") ;
	}
	
	/** 
	* @Description:获取数据字典map对象
	* @param:@param codeType
	* @param:@return    设定文件 
	* @return:Map<String,Object>    返回类型 
	* @throws: 
	* @author:liujr
	* 2017年4月26日
	*/
	protected  Map<String,Object> initMap(String codeType){
		List<LabelValue> list = dictionaryCacheHandler
				.getDictionaryByCodeTypeToPro(codeType);
		return this.initMapResult(list, isImport);
	}
}
