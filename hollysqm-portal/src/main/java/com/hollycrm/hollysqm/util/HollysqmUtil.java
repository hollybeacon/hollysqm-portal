package com.hollycrm.hollysqm.util;
import com.hollycrm.hollybeacon.basic.interfaces.dao.PageResult;
import com.hollycrm.hollybeacon.basic.rest.ServiceResult;
import com.hollycrm.hollybeacon.basic.util.StringUtils;
import com.hollycrm.hollysqm.bean.FtpConfigurBean;
import com.hollycrm.hollysqm.core.vo.I8DocBean;
import com.hollycrm.hollysqm.core.vo.V8DocBean;
import com.hollycrm.hollysqm.core.waveform.util.AudioUtil;
import com.hollycrm.hollysqm.entities.Record;
import com.hollycrm.hollysqm.entities.V8PaperExportTemplate;
import com.hollycrm.hollysqm.util.ftp.FTPManager;
import com.hollycrm.hollysqm.util.ftp.FTPServerInterface;

import java.io.IOException;
import java.lang.reflect.Method;
import java.util.Map;


public class HollysqmUtil {
	
	
	/**
	 * 根据Map参数设置实体类属性
	 * @param param
	 * @param clazz
	 * @param o
	 * @author wangyf
	 */
	@SuppressWarnings("unchecked")
	public static void fillParam(Map<String,Object> param, Class clazz, Object o) {
		Method m = null;

		for (Object key : param.keySet()) {
			if (param.get(key) != null) {
				String fieldname = key.toString();
				try {
					m = clazz.getMethod("set"
							+ fieldname.substring(0, 1).toUpperCase()
							+ fieldname.substring(1, fieldname.length()), clazz
							.getDeclaredField(fieldname).getType());
					if (m == null) {
						continue;
					}
					if (java.util.Collection.class.isAssignableFrom(clazz
							.getDeclaredField(fieldname).getType())) {// 忽略集合类型
						continue;
					}
					if (clazz.getDeclaredField(fieldname).getType()
							.equals(int.class)
							|| clazz.getDeclaredField(fieldname).getType()
									.equals(Integer.class)) {
						m.invoke(o, Integer.parseInt(param.get(key).toString()));
					} else {
						m.invoke(o, param.get(key));
					}
				} catch (Exception e) {

				}
			}
		}
	}
	
	/**
	 * 将数据包装成ServiceResult返回
	 * @param content
	 * @param success
	 * @param errorMessage
	 * @return
	 */
	public static ServiceResult wrapUpPageResult(Object content, boolean success, String errorMessage) {
		ServiceResult result = new ServiceResult();
		result.setContent(content);
		result.setSuccess(success);
		result.setErrorMessage(errorMessage);

		return result;
	}
	
	/**
	 * 初始化PageResult对象
	 * @param param
	 * @return
	 */
	public static PageResult<?> createPageResult(Map<String,Object> param) {
		PageResult<?> page = null;
		
		String limit = (String) param.get("rows");
		if (StringUtils.isEmpty(limit))
			limit = com.hollycrm.hollybeacon.basic.Constants.PAGE_DEFAULT_LIMIT;
		// 开始页
		String start = (String) param.get("page");
		if (StringUtils.isEmpty(start))
			start = "1";
		
		page = new PageResult<>(
				Integer.valueOf(limit));
		page.setPageNo(Integer.parseInt(start));
		
		return page;
	}
	
	/**
	 * v8接触记录转化为接触记录公共实体
	 * @param v8bean
	 * @param record
	 */
	public static void parseV8DocBeanToRecord(V8DocBean v8bean, Record record) {
		record.setCaller(v8bean.getCaller());
		record.setCustArea(v8bean.getCustArea());
		record.setCustBand(v8bean.getCustBand());
		//record.setEndTime(v8bean);
		record.setLength(v8bean.getRecoinfoLength());
		record.setSatisfaction(v8bean.getSatisfication());
		record.setServiceType(v8bean.getServiceType());
		record.setStartTime(v8bean.getStartTime());
		//record.setStatus(v8bean.getStatus());
		record.setTxtContent(v8bean.getTxtContent());
		record.setContactId(v8bean.getCustcontinfoId());
		record.setAcceptTime(v8bean.getAcceptTime());
		record.setQualityStatus(v8bean.getQualityStatus());		
		record.setRecordFile(v8bean.getRecordFile());
		record.setBussinessType(v8bean.getBusinessType());
		record.setCustLevel(v8bean.getCustLevel());
		record.setSilenceLength(v8bean.getSilenceLength());
	}
	
	/**
	 * i8接触记录转化为接触记录公共实体
	 * @param i8bean
	 * @param record
	 */
	public static void parseI8DocBeanToRecord(I8DocBean i8bean, Record record) {
		record.setCaller(i8bean.getPhone());
		record.setEndTime(i8bean.getCloseTime());
		record.setLength(i8bean.getSessionLength());
		record.setSatisfaction(i8bean.getSatisfaction().toString());
		record.setServiceType(i8bean.getServiceType());
		record.setStartTime(i8bean.getCreateTime());
		//record.setStatus(i8bean.getStatus());
		record.setTxtContent(i8bean.getTxtContent());
		record.setQualityStatus(i8bean.getQualityStatus());
		record.setCloseType(i8bean.getCloseType().toString());
		record.setSessionType(i8bean.getSessionType());
		record.setContactId(i8bean.getSessionId());
		
	}
	
	/**
	 * 根据参数转换成solr或sql的查询条件
	 * @param condition
	 * @param queryType
	 * @return
	 */
	public static String paramParseQueryText(String condition, String queryType, boolean stringType) {
		if(StringUtils.isEmpty(condition)) {
			return null;
		}
		condition = condition.replace("'", "");
		condition = condition.replace("\"", "");
		String[] sarray = condition.split(",");
		String pattern = "(";
		for(int i=0; i<sarray.length; i++) {
			if(i != sarray.length - 1) {
				if(queryType.equals("solr")) {
					if(stringType) {
						pattern += "\"%s\",";
					} else {
						pattern += "%s ";
					}
					
				} else if(queryType.equals("sql")) {
					if(stringType) {
						pattern += "'%s',";  
					} else {
						pattern += "%s,";  
					}
					
				}
				
			} else {
				if(queryType.equals("solr")) {
					if(stringType) {
						pattern += "\"%s\"";
					} else {
						pattern += "%s";
					}
					
				} else if(queryType.equals("sql")) {
					if(stringType) {
						pattern += "'%s'";
					} else {
						pattern += "%s";
					}
				}
			}
		}
		pattern += ")";
		
		return String.format(pattern, sarray);
	}
	
	/**
	 * Ftp文件下载
	 * @param timeDir
	 * @param saveDir
	 * @param filename
	 * @param xmlFtpConfigurBean
	 * @param vocFtpConfigurBean
	 */
	public static void  ftpDownload(String timeDir, String saveDir, String filename, FtpConfigurBean xmlFtpConfigurBean, FtpConfigurBean vocFtpConfigurBean, String recordFile) {
		String xmlParentPath = xmlFtpConfigurBean.getParentPath();
		//String mp3ParentPath = vocFtpConfigurBean.getParentPath();
		String xmldonwDir = xmlParentPath + timeDir + "/";
		//String mp3downDir = mp3ParentPath + timeDir + "/";
		String suffix = recordFile.substring(recordFile.indexOf("."), recordFile.length());
		String mp3downDir = recordFile.substring(0,recordFile.lastIndexOf("/") + 1);
		
		//String xmlParentPath = "/home/HollyQSM/voice/";
		//String mp3ParentPath = "/home/HollyQSM/voice/";
		//String xmldonwDir = xmlParentPath + "2017020100" + "/";
		//String mp3downDir = mp3ParentPath + "2017020100" + "/";
		//filename = "2ECBED29B2AF453B9A51CE43DC63A323";	
		
		if(suffix.indexOf("null") != -1) {
			suffix = suffix.substring(0,suffix.length() -4);
		}
		
		if(!recordFile.startsWith("/")) {
			recordFile = "/" + recordFile;
		}
		String xmlFilename = filename + ".xml";
		String mp3FileName = filename + suffix;
		String wavFileName = filename + ".wav";
		String imgFileName = filename + ".jpg";
		FTPManager ftpManager = new FTPManager();
		FTPServerInterface xmlftp = null;
		FTPServerInterface vocftp = null;
		xmlftp = ftpManager.getFtpConn(xmlFtpConfigurBean);// 获取FTP连接
		vocftp = ftpManager.getFtpConn(vocFtpConfigurBean);
		if (xmlftp == null || vocftp == null) {
			try {
				throw new Exception("FTP对象不可用...");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			if (!xmlftp.isExistsDir(xmldonwDir)) {// 判断远程ftp目录是否生成
				try {
					xmlftp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					throw new Exception("下载失败，FTP服务器上未生成对目录:" + xmldonwDir);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (!vocftp.isExistsDir(mp3downDir)) {// 判断远程ftp目录是否生成
				try {
					vocftp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					throw new Exception("下载失败，FTP服务器上未生成对目录:" + mp3downDir);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			xmlftp.download(xmldonwDir + xmlFilename, saveDir + xmlFilename);// 下载XML
			//vocftp.download(mp3downDir + mp3FileName, saveDir + mp3FileName); // 下载MP3
			vocftp.download(recordFile, saveDir + mp3FileName); // 下载MP3
			AudioUtil
					.createWavAndImage(saveDir + mp3FileName, saveDir
							+ wavFileName, saveDir + xmlFilename, saveDir
							+ imgFileName);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				xmlftp.close();
				vocftp.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	
	/**
	 * Ftp文件下载
	 * @param timeDir
	 * @param saveDir
	 * @param filename
	 * @param xmlFtpConfigurBean
	 * @param vocFtpConfigurBean
	 */
	public static void  ftpDownloadTest(String timeDir, String saveDir, String filename, FtpConfigurBean xmlFtpConfigurBean, FtpConfigurBean vocFtpConfigurBean, String recordFile) {
		String xmlParentPath = "/home/HollyQSM/voice/";
		String mp3ParentPath = "/home/HollyQSM/voice/";
		String xmldonwDir = xmlParentPath + "2017020100" + "/";
		String mp3downDir = mp3ParentPath + "2017020100" + "/";
		filename = "2ECBED29B2AF453B9A51CE43DC63A323";	
		
		String xmlFilename = filename + ".xml";
		String mp3FileName = filename + ".mp3";
		String wavFileName = filename + ".wav";
		String imgFileName = filename + ".jpg";
		FTPManager ftpManager = new FTPManager();
		FTPServerInterface xmlftp = null;
		FTPServerInterface vocftp = null;
		xmlftp = ftpManager.getFtpConn(xmlFtpConfigurBean);// 获取FTP连接
		vocftp = ftpManager.getFtpConn(vocFtpConfigurBean);
		if (xmlftp == null || vocftp == null) {
			try {
				throw new Exception("FTP对象不可用...");
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		try {
			if (!xmlftp.isExistsDir(xmldonwDir)) {// 判断远程ftp目录是否生成
				try {
					xmlftp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					throw new Exception("下载失败，FTP服务器上未生成对目录:" + xmldonwDir);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
			if (!vocftp.isExistsDir(mp3downDir)) {// 判断远程ftp目录是否生成
				try {
					vocftp.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
				try {
					throw new Exception("下载失败，FTP服务器上未生成对目录:" + mp3downDir);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		} catch (Exception e1) {
			e1.printStackTrace();
		}
		try {
			xmlftp.download(xmldonwDir + xmlFilename, saveDir + xmlFilename);// 下载XML
			vocftp.download(mp3downDir + mp3FileName, saveDir + mp3FileName); // 下载MP3
			
			AudioUtil
					.createWavAndImage(saveDir + mp3FileName, saveDir
							+ wavFileName, saveDir + xmlFilename, saveDir
							+ imgFileName);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			try {
				xmlftp.close();
				vocftp.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
	}
	

	public static Object parseRecordToExportTemplate(Record record) {
		if(record.getDataType().equals(Constant.V8)) {
			V8PaperExportTemplate v8Template = new V8PaperExportTemplate();
			v8Template.setAgentCode(record.getUser().getAgentCode());
			v8Template.setBussinessType(record.getBussinessType());
			v8Template.setCaller(record.getCaller());
			v8Template.setCustArea(record.getCustArea());
			v8Template.setCustBand(record.getCustBand());
			v8Template.setCustLevel(record.getCustLevel());
			v8Template.setLength(record.getLength().toString());
			v8Template.setSatisfaction(record.getSatisfaction());
			v8Template.setServiceType(record.getServiceType());
			v8Template.setSilenceLength(record.getSilenceLength().toString());
			v8Template.setStartTime(record.getStartTime());
			v8Template.setUserName(record.getUser().getUsername());
			v8Template.setTxtContent(record.getTxtContent());
			return v8Template;
		} else {
			return null;
		}
	}
	
}
