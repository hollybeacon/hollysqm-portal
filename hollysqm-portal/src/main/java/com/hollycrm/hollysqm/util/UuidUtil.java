package com.hollycrm.hollysqm.util;

import java.util.UUID;

/**
 * 生成UUID主键唯一值
 * @author jianglong
 * @date 2017年3月13日 上午9:55:52
 */
public class UuidUtil {

	/**
	 * 返回UUID字符串
	 * @return
	 */
	public static String getUuid(){
		return UUID.randomUUID().toString().replaceAll("-", "");
	}
}
