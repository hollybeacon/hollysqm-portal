(function() {
	$.extend($.fn.datagrid.methods, {
		init : function(jq,param) {
			$(jq).datagrid({  
	            width: 'auto',
	            singleSelect : param.singleSelect==undefined?true:param.singleSelect,  
	            method:'get',
	            border:false,
	            fit:true,
	            fitColumns:true,
	            pagination: true,
	            queryParams:param.param==undefined?{}:param.param,
	            url:param.url,
	            loadFilter: function(data){return (data.content)?data.content:data;},
	            loadMsg:'数据加载中请稍后……',  
	            columns:[param.columns],
	            onLoadSuccess:(param.event==null||param.event.onLoadSuccess==null)?function(){}:param.event.onLoadSuccess,
	            onLoadError:(param.event==null||param.event.onLoadError==null)?function(){}:param.event.onLoadError,
	            onBeforeLoad:(param.event==null||param.event.onBeforeLoad==null)?function(){}:param.event.onBeforeLoad,
	            onClickRow:(param.event==null||param.event.onClickRow==null)?function(){}:param.event.onClickRow,
	            onDblClickRow:(param.event==null||param.event.onDblClickRow==null)?function(){}:param.event.onDblClickRow,
	            onClickCell:(param.event==null||param.event.onClickCell==null)?function(){}:param.event.onClickCell,
	            onDblClickCell:(param.event==null||param.event.onDblClickCell==null)?function(){}:param.event.onDblClickCell,
	            onBeforeSortColumn:(param.event==null||param.event.onBeforeSortColumn==null)?function(){}:param.event.onBeforeSortColumn,
	            onSortColumn:(param.event==null||param.event.onSortColumn==null)?function(){}:param.event.onSortColumn,
	            onResizeColumn:(param.event==null||param.event.onResizeColumn==null)?function(){}:param.event.onResizeColumn,
	            onBeforeSelect:(param.event==null||param.event.onBeforeSelect==null)?function(){}:param.event.onBeforeSelect,
	            onSelect:(param.event==null||param.event.onSelect==null)?function(){}:param.event.onSelect,
	            onBeforeUnselect:(param.event==null||param.event.onBeforeUnselect==null)?function(){}:param.event.onBeforeUnselect,
	            onUnselect:(param.event==null||param.event.onUnselect==null)?function(){}:param.event.onUnselect,
	            onSelectAll:(param.event==null||param.event.onSelectAll==null)?function(){}:param.event.onSelectAll,
	            onUnselectAll:(param.event==null||param.event.onUnselectAll==null)?function(){}:param.event.onUnselectAll,
	            onBeforeCheck:(param.event==null||param.event.onBeforeCheck==null)?function(){}:param.event.onBeforeCheck,
	            onCheck:(param.event==null||param.event.onCheck==null)?function(){}:param.event.onCheck,
	            onBeforeUncheck:(param.event==null||param.event.onBeforeUncheck==null)?function(){}:param.event.onBeforeUncheck,
	            onUncheck:(param.event==null||param.event.onUncheck==null)?function(){}:param.event.onUncheck,
	            onCheckAll:(param.event==null||param.event.onCheckAll==null)?function(){}:param.event.onCheckAll,
	            onUncheckAll:(param.event==null||param.event.onUncheckAll==null)?function(){}:param.event.onUncheckAll,
	            checkRow:(param.event==null||param.event.checkRow==null)?function(){}:param.event.checkRow,
	            uncheckRow:(param.event==null||param.event.uncheckRow==null)?function(){}:param.event.uncheckRow,
	            onCheck:(param.event==null||param.event.onCheck==null)?function(){}:param.event.onCheck,
	            onBeforeEdit:(param.event==null||param.event.onBeforeEdit==null)?function(){}:param.event.onBeforeEdit,
	            onBeginEdit:(param.event==null||param.event.onBeginEdit==null)?function(){}:param.event.onBeginEdit,
	            onEndEdit:(param.event==null||param.event.onEndEdit==null)?function(){}:param.event.onEndEdit,
	            onAfterEdit:(param.event==null||param.event.onAfterEdit==null)?function(){}:param.event.onAfterEdit,
	            onCancelEdit:(param.event==null||param.event.onCancelEdit==null)?function(){}:param.event.onCancelEdit,
	            onHeaderContextMenu:(param.event==null||param.event.onHeaderContextMenu==null)?function(){}:param.event.onHeaderContextMenu,
	            onRowContextMenu:(param.event==null||param.event.onRowContextMenu==null)?function(){}:param.event.onRowContextMenu
	        });	
		},
		search : function(jq,param){
			$(jq).datagrid('options').queryParams=param.param;
			//$(jq).datagrid('getPager').pagination('select',1);
			$(jq).datagrid('load');  
		},
		remove : function(jq,param){
			var curRow=$(jq).datagrid('getSelected');
			param.id=(param.id==null?curRow.id:param.id);
			$.messager.confirm('删除', '确认删除吗?', function(r){
				if (r){
					holly.get(param.url,{id:param.id},function(e){
						if(e.success){//先确认检查通过！
							$(jq).datagrid('reload');
						}else{
							holly.showError("错误，原因:"+e.errorMessage);
							return;
						}
		    		});
				}
			});
		},
		getdictname:function(jq,param){
			var res=null;
			holly.get(holly.getPath()+"/rest/dictionary/getDictionaryNameRest",param.param,function(e){
				if(e.success){//先确认检查通过！
					res = e.content;
				}else{
					holly.showError("错误，原因:"+e.errorMessage);
				}
    		},true);
			return res;
		},
		getdictnames:function(jq,param){
			var res=null;
			holly.get(holly.getPath()+"/rest/dictionary/getDictionaryNamesRest",param.param,function(e){
				if(e.success){//先确认检查通过！
					res = e.content;
				}else{
					holly.showError("错误，原因:"+e.errorMessage);
				}
    		},true);
			return res;
		}
	});
	
	/**
	 * combobox初始化从redis中读取数据字典，可以选择是否加入"请选择"这个选项
	 * @param hasEmptyOption 是否需要在下拉选项中加入请选择,默认为加入,传入false则不加入
	 * @param codeType 数据字典的类型
	 */
	$.extend($.fn.combobox.methods, {
		init : function(jq,param) {
			$(jq).combobox({
				valueField:'value',
			    textField:'name',
				url:holly.getPath() + "/rest/dictionary/getDictionaryByCodeType?codeType="+(param.codeType==undefined?"":param.codeType),
				method:"get",
				onLoadSuccess:(param.event==null||param.event.onLoadSuccess==null)?function(){}:param.event.onLoadSuccess,
				onBeforeLoad:(param.event==null||param.event.onBeforeLoad==null)?function(){}:param.event.onBeforeLoad,
				onLoadError:(param.event==null||param.event.onLoadError==null)?function(){}:param.event.onLoadError,
				onSelect:(param.event==null||param.event.onSelect==null)?function(){}:param.event.onSelect,
				onUnselect:(param.event==null||param.event.onUnselect==null)?function(){}:param.event.onUnselect,
				loadFilter:function(data){
					var jsonArray=new Array(); 
					if(param.hasEmptyOption==undefined || param.hasEmptyOption){
						var defaultVal = {"value":"","name":'请选择'};
						jsonArray.push(defaultVal);
					}
					for(var o in data.content){
						jsonArray.push(data.content[o]);
					}
					return jsonArray;
				}
			});	
		}
	});
	
	/**
	 * combobox初始化从redis中读取数据字典，可以选择是否加入"全部"这个选项
	 * @param hasAllOption 是否需要在下拉选项中加入"全部",默认为加入,传入false则不加入
	 * @param codeType 数据字典的类型
	 */
	$.extend($.fn.combobox.methods, {
		init_extend : function(jq,param) {
			$(jq).combobox({
				valueField:'value',
			    textField:'name',
				url:holly.getPath() + "/rest/dictionary/getDictionaryByCodeType?codeType="+(param.codeType==undefined?"":param.codeType),
				method:"get",
				onLoadSuccess:(param.event==null||param.event.onLoadSuccess==null)?function(){}:param.event.onLoadSuccess,
				onBeforeLoad:(param.event==null||param.event.onBeforeLoad==null)?function(){}:param.event.onBeforeLoad,
				onLoadError:(param.event==null||param.event.onLoadError==null)?function(){}:param.event.onLoadError,
				onSelect:(param.event==null||param.event.onSelect==null)?function(){}:param.event.onSelect,
				onUnselect:(param.event==null||param.event.onUnselect==null)?function(){}:param.event.onUnselect,
				loadFilter:function(data){
					var jsonArray=new Array(); 
					if(param.hasAllOption==undefined || param.hasAllOption){
						var defaultVal = {"value":"","name":'全部'};
						jsonArray.push(defaultVal);
					}
					for(var o in data.content){
						jsonArray.push(data.content[o]);
					}
					return jsonArray;
				}
			});	
		}
	});
	/**
	 * combobox初始化从redis中读取数据字典，可以选择是否加入"全部"这个选项
	 * @param hasAllOption 是否需要在下拉选项中加入"全部",默认为加入,传入false则不加入,过滤指定项
	 * @param codeType 数据字典的类型
	 */
	$.extend($.fn.combobox.methods, {
		init_filter : function(jq,param) {
			$(jq).combobox({
				valueField:'value',
				textField:'name',
				url:holly.getPath() + "/rest/dictionary/getDictionaryByCodeType?codeType="+(param.codeType==undefined?"":param.codeType),
				method:"get",
				onLoadSuccess:(param.event==null||param.event.onLoadSuccess==null)?function(){}:param.event.onLoadSuccess,
				onBeforeLoad:(param.event==null||param.event.onBeforeLoad==null)?function(){}:param.event.onBeforeLoad,
				onLoadError:(param.event==null||param.event.onLoadError==null)?function(){}:param.event.onLoadError,
				onSelect:(param.event==null||param.event.onSelect==null)?function(){}:param.event.onSelect,
				onUnselect:(param.event==null||param.event.onUnselect==null)?function(){}:param.event.onUnselect,
				loadFilter:function(data){
					var jsonArray=new Array(); 
					if(param.hasAllOption==undefined || param.hasAllOption){
						var defaultVal = {"value":"","name":'全部'};
						jsonArray.push(defaultVal);
					}
					var filterValue = param.filterValue==undefined ? "" :param.filterValue.split(",");
					for(var o in data.content){
						var flag = true;
						for(var p in filterValue){
							if(data.content[o].value == filterValue[p]){
								flag = false;
								break;
							}
						}
						if(flag)
							jsonArray.push(data.content[o]);
					}
					return jsonArray;
				}
			});	
		}
	});
})(jQuery);


(function($){
   $.fn.radio = function(param){
	    var res=null;
		holly.get(holly.getPath() + "/rest/dictionary/getDictionaryByCodeType?codeType="+param.codeType,{},function(e){
			if(e.success){//先确认检查通过！
				res = e.content;
			}else{
				holly.showError("错误，原因:"+e.errorMessage);
			}
		},true);
		if(res!=null){
			var content = "";
			for(var o in res){
				content += "<label class='extend-input radio'><input type='radio' name='"+param.name+"' value="+res[o].value+"> "+res[o].name+"</label>";
			}
			$(this).html(content);
		}
   };
   $.fn.checkbox = function(param){
	    var res=null;
		holly.get(holly.getPath() + "/rest/dictionary/getDictionaryByCodeType?codeType="+param.codeType,{},function(e){
			if(e.success){//先确认检查通过！
				res = e.content;
			}else{
				holly.showError("错误，原因:"+e.errorMessage);
			}
		},true);
		if(res!=null){
			var content = "";
			for(var o in res){
				content += "<label class='extend-input checkbox'><input type='checkbox' name='"+param.name+"' value="+res[o].value+"> "+res[o].name+"</label>";
			}
			$(this).html(content);
		}
  };
})(jQuery);