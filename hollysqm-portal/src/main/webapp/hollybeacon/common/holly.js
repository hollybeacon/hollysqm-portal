/**
 重构后的公共工具类（待完善）
 杨凯
 **/
(function ($) {
    window['holly'] = {};

    /***/
    /**
     * 功能说明：得到应用路径
     * @method holly.getPath
     * @param { string } url 参数可选。默认为当前网页
     * @return{ string } appPath 应用的URL路径
     */
    holly.getPath = function (url) {
        var path = (url || self.location.href).replace("//", "@@").split("/");
        return path[0].replace("@@", "//") + "/" + path[1];
    };

    /***/
    /**
     * 功能说明:通过mediaId得到系统资源(文件+内容)
     * @method mediaId   资源编号
     * @method isImage   是否图片
     */
    holly.getResource = function (mediaId, isImage) {
        var path = (holly.getPath() + "/rest/mediafile/") + (isImage ? "view" : "download") + "?mediaId=" + mediaId;
        return path;
    };
    /**
     * 功能说明：得到页面URL参数值
     * @method holly.getUrlParams
     * @param { string } param 参数名
     * @return{ string } value 参数值
     */
    holly.getUrlParams = function (param) {
        var uri = window.location.search;
        var reg = new RegExp("" + param + "=([^&?]*)", "ig");
        return ((uri.match(reg)) ? (decodeURI(uri.match(reg)[0].substr(param.length + 1))) : null);
    };

    /**
     * 功能说明：得到时间戳。
     * @method holly.getTimeStamp
     * @return{ long } time 序列后的时间
     */
    holly.getTimeStamp = function () {
        return new Date().getTime();
    };

    /**
     * 功能说明：过滤html字符,仅保留filter指定的标签。
     * @method holly.filterHtml
     * @param { string } html 待过滤的html文本串
     * @param { string } el   保留的指定标签：如：img，input
     * @return{ string } html 过滤后的html
     * @example
     * 输出结果:<li>测试</li>
     */
    holly.filterHtml = function (html, el) {
        var tm = el.replace(/,/g, '|!');
        var reg = "/<(?!" + tm + ").*?>/ig";
        return html.replace(eval(reg), "");
    };

    /**
     * 功能说明：得到HTML返回多个<img>标签中src路径 返回为路径数组
     * @method holly.getImgArray
     * @param { string } html  待过滤的html文本串
     * @return { array } 得到多个img标签的路径数组
     */
    holly.getImgArray = function (html) {
        var imgReg = /<img.*?(?:>)/gi;
        var srcReg = /src=['"]?([^'"]*)['"]?/i;
        var array = html.match(imgReg);
        var srcArray = new Array();
        if (array && array.length > 0) {
            $.each(array, function (i, val) {
                var src = val.match(eval(srcReg));
                if (src[1]) {
                    srcArray.push(src[1]);
                }
            });
        }
        return srcArray;
    };
    /*
     * 批量渲染easyui
     * 主要处理easyui隐藏切换成显示时变形
     * 在该对象包含中所有easyui控件
     * @param 对象
     * */
    holly.renderEasyUI = function (obj) {
        $.each(obj, function (index, row) {
            var child = $(row).children();
            if (child.length > 0) {
                holly.renderEasyUI(child);
            }
            if ($(row).attr("data-options") || ($(row).attr("class") && $(row).attr("class").indexOf("easyui-") == 0)) {
                if ($(row).hasClass("easyui-linkbutton")) {//可拓展更多easyui控件类型
                    $(row).linkbutton();
                } else if ($(row).hasClass("easyui-combobox")) {
                    $(row).combobox();
                } else {
                    $(row).textbox();//重新渲染控件
                }
            }
        });
    };
    /**
     * 功能说明：替换转义str中的字符 & < > ' " \n
     * @method holly.replace_str
     * @param { string } str  待替换的字符串
     * @param { isnowrap } boolean  默认不换行（true:换行符/n替换成空格  false:换行符/n替换成<br />）
     * @return { string } str 替换后的字符串
     */
    holly.replace_str = function (str, nowrap) {
        if (str != null) {
            str = str.replace(/\&/g, '&amp;');
            str = str.replace(/\</g, '&lt;');
            str = str.replace(/\"/g, '&quot;');
            str = str.replace(/\'/g, '&apos;');
            str = str.replace(/\>/g, '&gt;');
            if (nowrap == undefined || nowrap) {
                str = str.replace(/\n/g, ' ');
            } else if (nowrap == false) {
                str = str.replace(/\n/g, '<br />');
            }
        }
        return str;
    },
        /**字符串转成日期类型 格式YYYY-MM-dd YYYY/MM/dd MM/dd/YYYY MM-dd-YYYY   */
        holly.string2Date = function (DateStr, fomratStr) {
            var converted = Date.parse(DateStr.replace(/-/g, "/"));
            var myDate = new Date(converted);
            if (isNaN(myDate)) {
                //var delimCahar = DateStr.indexOf('/')!=-1?'/':'-';
                var arys = DateStr.split('-');
                myDate = new Date(arys[0], --arys[1], arys[2]);
            }
            if (fomratStr) {
                myDate.format(fomratStr);
            }
            return myDate;
        };

    /**
     * 功能说明：得到指定格式的当前时间
     * @method holly.currentTime
     * @param { string } formatStr 格式化参数  不使用时默认为"yyyy-MM-dd hh:mi:ss",可选：hh:mi:ss,yyyy-MM-dd
     * @return { string } 返回格式化后的时间
     */
    holly.currentTime = function (formatStr) {
        return formatStr ? new Date().format(formatStr) : new Date().format("yyyy-MM-dd hh:mi:ss");
    };

    /**
     * 功能说明：得到指定的时间
     * @param 当前时间和期望结果的天数间隔，若取今天，则dates=0，若为取昨天的日期，则dates为-1，若为取明天，则dates=1，以此类推
     * @return yyyy-MM-dd格式的日期字符串
     */
    holly.getDateStr = function (dates) {
        var dd = new Date();
        dd.setDate(dd.getDate() + dates);
        var y = dd.getFullYear();
        var m = dd.getMonth() + 1;
        var d = dd.getDate();
        return y + "-" + m + "-" + d;
    };

    /**
     * 功能说明：得到指定日期往前推一个月的日期
     * @param 指定日期时间，字符串，格式为"yyyy-MM-dd"
     * @return 格式为"yyyy-MM-dd"的上个月的日期
     */
    holly.getLastMonthDateStr = function (DateStr) {
        var myDate = holly.string2Date(DateStr, "yyyy-MM-dd");
        myDate.setMonth(myDate.getMonth() - 1);
        return myDate.getFullYear() + "-" + (myDate.getMonth() + 1) + "-" + myDate.getDate();
    };

    /**
     * 将秒数转换为XX小时XX分钟XX秒
     */
    holly.secondToDate = function (time) {
        if (null != time && "" != time) {
            if (time > 60 && time < 60 * 60) {
                time = parseInt(time / 60.0) + "分钟" + parseInt((parseFloat(time / 60.0) -
                        parseInt(time / 60.0)) * 60) + "秒";
            }
            else if (time >= 60 * 60 && time < 60 * 60 * 24) {
                time = parseInt(time / 3600.0) + "小时" + parseInt((parseFloat(time / 3600.0) -
                        parseInt(time / 3600.0)) * 60) + "分钟" +
                    parseInt((parseFloat((parseFloat(time / 3600.0) - parseInt(time / 3600.0)) * 60) -
                        parseInt((parseFloat(time / 3600.0) - parseInt(time / 3600.0)) * 60)) * 60) + "秒";
            }
            else {
                time = parseInt(time) + "秒";
            }
        }
        return time;
    };
    /**预加载图片**/
    holly.prevLoadImage = function (rootpath, paths) {
        for (var i in paths) {
            $('<img />').attr('src', rootpath + paths[i]);
        }
    };
    //使用name查找jquery元素
    holly.$E = function (name) {
        $field = $('#' + name);
        if ($field.size() == 0)
            return $('[name=' + name + ']');
        else
            return $field;
    };

    /**
     * @param domId
     * @param reload 为true,每次调用时都会重载iframe
     * 根据data-src的URL在合适的时候加载IFRAME内容
     * <iframe name="domId" data-src="${ctx}/rest/security/home" src="" />
     */
    holly.loadIframe = function (domId, reload) {
        var $iframe = holly.$E(domId);
        var src = $iframe.attr('src');
        var src0 = $iframe.attr('data-src');
        if (src == '' || reload) {
            $iframe.attr('src', src0);
        }
    };

    /**
     * 兼容IE ,FF ,CHROME
     * 获得iframe的window对象
     * 举例：getIframeWindow("MyIFrame").func();
     * @param iframeId
     * @returns
     */
    holly.getIframeWindow = function (iframeId) {
        var element = document.getElementById(iframeId);
        return element.contentWindow || element.contentDocument.parentWindow;
    };

    /**显示loading**/
    holly.showLoading = function (message, targetDiv) {
        message = message || "正在加载中...";
        // $('body').append("<div class='modal-backdrop fade in'>" + message + "</div>");
        $("<div class=\"loading-mask\"></div>").css({display: "block"}).appendTo(targetDiv || "body");
        $("<div class=\"loading-mask-msg\"><div class=\"loading-mask-msgbg\"></div></div>").append("<div class=\"loading-mask-msgtext\">" + message + "</div>").appendTo(targetDiv || "body").css({display: "block"});
    };
    /**隐藏loading**/
    holly.hideLoading = function (targetDiv) {
        targetDiv = targetDiv || "body";
        $(targetDiv).find(".loading-mask,.loading-mask-msg").remove();
    };
    /**显示成功提示窗口**/
    holly.showSuccess = function (title, content, callback) {
        toastr.success(content, title);
    };
    /**显示成功提示窗口**/
    holly.showWarn = function (title, content, callback) {
        try {
            toastr.warning(content, title);
        } catch (e) {
            alert("未加载入toastr插件包!");
        }
    };
    /**显示成功提示窗口**/
    holly.showInfo = function (title, content, callback) {
        try {
            toastr.info(content, title);
        } catch (e) {
            alert("未加载入toastr插件包!");
        }
    };
    /**显示失败提示窗口**/
    holly.showError = function (title, content, callback) {
        try {
            toastr.error(content, title);
        } catch (e) {
            alert("未加载入toastr插件包!");
        }
    };
    /**
     * 功能说明：得到中文或英文字符真实长席
     * @method holly.getLength
     * @param { string } str 待计算的字符串
     * @return { int } 返回字符串长度
     */
    holly.getLength = function (str) {
        return str.replace(/[^\x00-\xff]/g, "xx").length;
    };

    /**
     * 功能说明：判断字符串是否在某个范围中
     * @method holly.isRange；
     * @param { string } minLen 最小长度
     * @param { string } maxLen 最大长度【选填】
     * @return { boolean } 是否在给定的长度范围内。
     */
    holly.isRange = function (str, minLen, maxLen) {
        var len = holly.getLength(str);
        return len <= (maxLen || minLen) ? true : false;
    };

    /**
     * 功能说明：Cookies操作类
     * @class holly.cookies
     * @method holly.cookies.get(@param cookieName)
     * @method holly.cookies.set(@param cookieName,@param cookieValue,@param cookieValue,@param DayValue)
     * @method holly.cookies.remove(@param cookieName)
     * @example
     * holly.cookies.set('loginUser','张三',3)
     * holly.cookies.get('loginUser') 结果值： '张三';
     * holly.cookies.remove('loginUser') 删除指定cookies;
     */
    holly.cookies = (function () {
        var fn = function () {
        };
        fn.prototype.get = function (name) {
            var cookieValue = "";
            var search = name + "=";
            if (document.cookie.length > 0) {
                offset = document.cookie.indexOf(search);
                if (offset != -1) {
                    offset += search.length;
                    end = document.cookie.indexOf(";", offset);
                    if (end == -1) end = document.cookie.length;
                    cookieValue = decodeURIComponent(document.cookie.substring(offset, end));
                }
            }
            return cookieValue;
        };
        fn.prototype.set = function (cookieName, cookieValue, saveDay) {
            var expire = "";
            var _day = 1;
            if (saveDay != null) {
                _day = saveDay;
            }
            expire = new Date((new Date()).getTime() + _day * 86400000);
            expire = "; expires=" + expire.toGMTString();
            document.cookie = cookieName + "=" + encodeURIComponent(cookieValue) + ";path=/" + expire;
        };
        fn.prototype.remvoe = function (cookieName) {
            var expire = "";
            expire = new Date((new Date()).getTime() - 1);
            expire = "; expires=" + expire.toGMTString();
            document.cookie = cookieName + "=" + escape("") + ";path=/" + expire;
            /*path=/*/
        };
        return new fn();
    })();

    /**
     * 功能说明：hashMap存储器
     * *********  操作实例  **************
     *   var map = holly.hashMap();
     *   map.put("key1","Value1");
     *   map.put("key2","Value2");
     *   map.put("key3","Value3");
     *   map.put("key4","Value4");
     *   map.put("key5","Value5");
     *   alert("size："+map.size()+" key1："+map.get("key1"));
     *   map.remove("key1");
     *   map.put("key3","newValue");
     *   var values = map.values();
     *   for(var i in values){
   		*       document.write(i+"："+values[i]+"   "); 
   		*   }
     *   document.write("<br>");
     *   var keySet = map.keySet();
     *   for(var i in keySet){
   		*       document.write(i+"："+keySet[i]+"  "); 
   		*   }
     *   alert(map.isEmpty());
     */
    holly.hashMap = function () {
        var fn = function () {
        };
        var length = 0;
        var obj = new Object();
        /**
         * 判断Map是否为空
         */
        fn.prototype.isEmpty = function () {
            return length == 0;
        };
        /**
         * 判断对象中是否包含给定Key
         */
        fn.prototype.containsKey = function (key) {
            return (key in obj);
        };
        /**
         * 判断对象中是否包含给定的Value
         */
        fn.prototype.containsValue = function (value) {
            for (var key in obj) {
                if (obj[key] == value) {
                    return true;
                }
            }
            return false;
        };
        /**
         *向map中添加数据
         */
        fn.prototype.put = function (key, value) {
            if (!this.containsKey(key)) {
                length++;
            }
            obj[key] = value;
        };
        /**
         * 根据给定的Key获得Value
         */
        fn.prototype.get = function (key) {
            return this.containsKey(key) ? obj[key] : null;
        };
        /**
         * 根据给定的Key删除一个值
         */
        fn.prototype.remove = function (key) {
            if (this.containsKey(key) && (delete obj[key])) {
                length--;
            }
        };
        /**
         * 获得Map中的所有Value
         */
        fn.prototype.values = function () {
            var _values = new Array();
            for (var key in obj) {
                _values.push(obj[key]);
            }
            return _values;
        };

        /**
         * 获得Map中的所有Key
         */
        fn.prototype.keySet = function () {
            var _keys = new Array();
            for (var key in obj) {
                _keys.push(key);
            }
            return _keys;
        };

        /**
         * 获得Map的长度
         */
        fn.prototype.size = function () {
            return length;
        };

        /**
         * 清空Map
         */
        fn.prototype.clear = function () {
            length = 0;
            obj = new Object();
        };
        return new fn();
    };

    /**
     * 功能说明：判断浏览器是否支持Cookies
     * @method holly.isSupportCookie
     * @param { string } rootPath 根路径
     * @param { array } imgArray  加载图像数组
     * @return { boolean } 返回是否支持
     */
    holly.isSupportCookie = function () {
        var isSupport = false;
        if (typeof(navigator.cookieEnabled) != 'undefined')
            isSupport = navigator.cookieEnabled;
        else {
            document.cookie = 'test';
            isSupport = document.cookie == 'test';
            document.cookie = '';
        }
        if (!isSupport) {
            alert('您的浏览器禁用了Cookie。\n此系统需要您的浏览器支持Cookie。');
        }
        return isSupport;
    };

    /**
     * 功能说明：预加载图片
     * @method holly.prevLoadImage
     * @param { string } rootPath 根路径
     * @param { array } imgArray  加载图像数组
     * @example
     */
    holly.prevLoadImage = function (rootPath, imgArray) {
        for (var i in imgArray) {
            $('<img />').attr('src', rootPath + imgArray[i]);
        }
    };
    /**
     * 表单序列转化为后台可接收的Bean模型JSON 支持转码
     * @param form
     * @param isCN
     * @returns JSONBEAN
     */
    holly.form2bean = function (form, isCN) {
        var serializedParams = isCN ? $(form).serialize() : decodeURIComponent($(form).serialize(), true);
        var obj = {};

        function evalThem(str) {
            var attributeName = str.split("=")[0];
            var attributeValue = str.split("=")[1];
            if (!attributeValue) {
                return;
            }
            var array = attributeName.split(".");
            for (var i = 1; i < array.length; i++) {
                var tmpArray = Array();
                tmpArray.push("obj");
                for (var j = 0; j < i; j++) {
                    tmpArray.push(array[j]);
                }
                ;
                var evalString = tmpArray.join(".");
                if (!eval(evalString)) {
                    eval(evalString + "={};");
                }
            }
            ;
            eval("obj." + attributeName + "='" + attributeValue + "';");
        };
        var properties = serializedParams.split("&");
        for (var i = 0; i < properties.length; i++) {
            evalThem(properties[i]);
        }
        ;
        return obj;
    };

    /**
     * 表单序列转化为json支持多选控件
     * @param form
     * @returns
     */
    holly.form2json = function (form) {
        var o = {};
        $(form.serializeArray()).each(function () {
            if (o[this.name]) {
                if ($.isArray(o[this.name])) {
                    o[this.name].push(this.value);
                } else {
                    o[this.name] = [o[this.name], this.value];
                }
            } else {
                o[this.name] = this.value;
            }
        });
        //对只有单name的checkbox和radio明确标著是否有选中
        form.find(":checkbox,:radio").each(function () {
            var el = $("[name='" + this.name + "']", form);
            if (el && el.length == 1) {
                o[this.name] = el.is(":checked") ? true : false;
            }
        });
        return o;
    };

    //填充表单数据
    holly.loadForm = function (mainform, data) {
        var form = mainform ? mainform : $("form:first");
        if (!form) {
            //根据返回的属性名，找到相应ID的表单元素，并赋值
            for (var p in data) {
                var ele = $("[name=" + (preID + p) + "]", form);
                //针对复选框和单选框 处理
                if (ele.is(":checkbox,:radio")) {
                    ele[0].checked = data[p] ? true : false;
                }
                else {
                    ele.val(data[p]);
                }
            }
        }
        ;
    };
    /**
     * 功能说明：延迟指定时间后执行的函数fn, 如果在延迟时间内再次执行该方法， 将会根据指定的exclusion表达式的值，
     * 决定是否取消前一次函数的执行， 如果exclusion的值为true， 则取消执行，反之，将继续执行前一个方法。
     * @method holly.delayTask
     * @param { function } fn 需要延迟执行的函数对象
     * @param { int } delay 延迟的时间， 单位是毫秒
     * @param { string } targentId 操作的id
     * @param { boolean } exclusion 如果在延迟时间内再次执行该函数，该值将决定是否取消执行前一次函数的执行，
     *                     值为true表示取消执行， 反之则将在执行前一次函数之后才执行本次函数调用。
     * @return { function } 目标函数fn的代理函数， 只有执行该函数才能起到延时效果
     * @example
     * holly.delayTask(function(){alert("abc")},5000);
     */
    holly.delayTask = function (fn, delay, targentId, exclusion) {
        return function () {
            if (exclusion) {
                window.clearTimeout(targetId);
            }
            timerID = window.setTimeout(fn, delay);
        };
    },
        /**
         * 将字符串数组转换成Json对象，key为数组中的元素
         * @method listToJson
         * @warning 同时生成大写的key和原生key。
         * @param { array } arr 字符串数组
         * @return{ json } 转化之后的json
         */
        holly.listToJson = function (arr) {
            if (!arr)return {};
            arr = $.isArray(arr) ? arr : arr.split(',');
            for (var i = 0, ci, obj = {}; ci = arr[i++];) {
                obj[ci.toUpperCase()] = obj[ci] = 1;
            }
            return obj;
        },

        /**
         * 功能说明：获取元素item数组array中首次出现的位置, 不存在返回-1。否则返回下标。
         * @method indexOf
         * @remind 该方法的匹配过程使用的是恒等“===”
         * @param { Array } array 需要查找的数组对象
         * @param { * } item 需要在目标数组中查找的值
         * @return { int } 未找到则返回-1，找到则返回下标位置
         */
        holly.indexOf = function (array, item) {
            var index = -1;
            $.each(array, function (v, i) {
                if (i >= 0 && v === item) {
                    index = i;
                    return false;
                }
            });
            return index;
        },

        /**
         * 功能说明：判断item是否存在于arr字符或arr数组,
         * @method holly.contains
         * @remind 该方法的匹配过程使用的是恒等“===”
         * @param { array or string} arr 需要查找的数组或字符串
         * @param { * } item 需要在目标中查找的值
         * @return { boolean } 是否存在
         */
        holly.contains = function (arr, item) {
            if ($.type(arr) === "array") {
                return (holly.indexOf(arr, item) >= 0) ? true : false;
            } else if ($.type(arr) === "string") {
                return (arr.indexOf(item) >= 0) ? true : false;
            } else {
                return false;
            }
        },

        /**
         * 将“'，&，<，"，>”符号转义为网页中表示的转义符&lt; &#39; &quot;
         * @method holly.unhtml
         * @param { String } str 需要转义的字符串
         * @return{ String } 转义后的HTML字符串
         */
        holly.coverHtml = function (str) {
            return str ? str.replace(/[&<">'](?:(amp|lt|quot|gt|#39|nbsp|#\d+);)?/g, function (a, b) {
                return (b) ? a : {'<': '&lt;', '&': '&amp;', '"': '&quot;', '>': '&gt;', "'": '&#39;'}[a];
            }) : '';
        },

        /**
         * 将html中的&lt; &#39; &quot;还原成正常字符
         * @method unHtml
         * @param { String } html 需要逆转义的字符串
         * @return{ String } 还原后的正常字符串
         */
        holly.unHtml = function (html) {
            return html ? html.replace(/&((g|l|quo)t|amp|#39|nbsp);/g, function (m) {
                return {'&lt;': '<', '&amp;': '&', '&quot;': '"', '&gt;': '>', '&#39;': "'", '&nbsp;': ' '}[m];
            }) : '';
        },

        /**
         * 功能说明：JQuery1.8后简化配置的ajax
         * @method holly.ajax
         * @remind 官方推荐done和fail来淘汰旧的success和error写法
         * @param  option [必需]       配置项 可精简配置为 {"url":url,"param":param}
         * @param  successFn  [非必需]  成功时执行的回调函数
         * @param  errorFn      [非必需]  错误时执行的回调函数
         * @example
         * 简单用法:
         *        holly.ajax({url:'http://localhost/action',param:{'id':'01'},"sync":true 为真表示同步，不配置或为假表示异步
	     *  	},done:function(response){
	     * 			...todo somethine...
	     * 		}});
         * 复杂用法:
         * holly.ajax({url:'http://202.103.22.33/action',param:{'id':'01'},crossDomain:true,
	     * jsonpCallback:function(e){...},beforeSend:function(e){...},contentType:"charset=utf-8"
	     * },done:function(response){
	     * 	  ...todo something...
	     * },fail:function(xhr,status,error){
	     *    ...todo...
	     * });
         */
        holly.ajax = function (options, successFn, errorFn) {
            $.ajax({
                type: options.type || 'post',					    //默认post提交
                url: options.url || '',
                data: options.data || {},
                dataType: options.dataType || "json",					//可选xml,json,html,script
                cache: false,
                async: options.async,
                contentType: options.contentType,	//原始默认：application/x-www-form-urlencoded
                crossDomain: options.crossDomain,
                jsonpCallback: options.callback,
                beforeSend: options.beforeSend || function () {
                },
                //success:(successFn || function(data,status,xhr){}),
                //error:(errorFn || function(xhr, status, error){alert("连接请求失败：故障原因:"+status)}),
                complete: options.complete || function (xhr, ts) {
                    xhr = null;
                }
            }).done(successFn || function (data, status, xhr) {
                })
                .fail(errorFn || function (xhr, status, error) {
                        holly.showError("数据访问失败,请联系管理员!");
                    });
        },

        /**
         * 功能说明：ajax post简单封装
         * @method holly.post
         * @param  url      [必需]       请求的URL链接
         * @param  param  [非必需]  JSON格式参数
         * @param  fn      [非必需]  成功后执行的回调函数
         * @param  isSync [非必需]  true同步，默认为异步
         * @example
         * holly.post("http://localhost:8080/Test.do",{id:'001'},function(data){ ..do something.. });
         */
        holly.post = function (url, param, fn, isSync) {
            if (isSync) {			//同步使用预配的重写函数
                holly.ajax({"url": url, "data": param, "async": false}, fn);
            } else {	   			//异步使用自带的精简函数
                $.post(url, param, fn || function () {
                    }, "json");
            }
        };

    /**
     * 功能说明：ajax get简单封装
     * @method holly.get
     * @param  url      [必需]       请求的URL链接
     * @param  param  [非必需]  JSON格式参数
     * @param  fn      [非必需]  成功后执行的回调函数
     * @param  isSync [非必需]  true同步，默认为异步
     * @example
     * holly.get("http://localhost:8080/Test.do",{id:'001'},function(data){ ..do something.. });
     */
    holly.get = function (url, param, fn, isSync) {
        url += (url) ? ((url.indexOf("?") >= 0 ? "&" : "?") + "curtime=" + holly.getTimeStamp()) : "";
        if (isSync) {			//同步使用预配的重写函数
            holly.ajax({"type": "get", "url": url, "data": param, "async": false}, fn);
        } else {					//异步使用自带的精简函数
            $.get(url, param, fn || function () {
                }, "json");
        }
    };
    holly.compareDate = function (data1str, data2str, format) {
        var dateA = holly.string2Date(data1str, format);
        var dateB = holly.string2Date(data2str, format);
        if (isNaN(dateA) || isNaN(dateB)) return null;
        if (dateA > dateB) return 1;
        if (dateA < dateB) return -1;
        return 0;
    };

    /**对特殊字符进行替换**/
    holly.transferSpecialChar = function (s) {
        var str = s.replace(/%/g, "%25").replace(/\+/g, "%2B").replace(/\s/g, "+ "); //   %   +   \s
        str = str.replace(/-/g, "%2D").replace(/\*/g, "%2A").replace(/\//g, "%2F");   //   -   *   /
        str = str.replace(/\&/g, "%26").replace(/!/g, "%21").replace(/\=/g, "%3D");   //   &   !   =
        str = str.replace(/\?/g, "%3F").replace(/:/g, "%3A").replace(/\|/g, "%7C");   //   ?   :   |
        str = str.replace(/\,/g, "%2C").replace(/\./g, "%2E").replace(/#/g, "%23").replace(/\'/g, "%60");   //   ,   .   #
        return str;
    };

    //日期格式化YYYY-MM-DD hh:mm:ss
    Date.prototype.format = function (formatStr) {
        var str = formatStr;
        var Week = ['日', '一', '二', '三', '四', '五', '六'];

        str = str.replace(/yyyy|YYYY/, this.getFullYear());
        str = str.replace(/yy|YY/, (this.getYear() % 100) > 9 ? (this.getYear() % 100).toString() : '0' + (this.getYear() % 100));

        str = str.replace(/mm|MM/, this.getMonth() > 8 ? this.getMonth() + 1 : '0' + (this.getMonth() + 1));
        str = str.replace(/M/g, this.getMonth());

        str = str.replace(/w|W/g, Week[this.getDay()]);

        str = str.replace(/dd|DD/, this.getDate() > 9 ? this.getDate().toString() : '0' + this.getDate());
        str = str.replace(/d|D/g, this.getDate());

        str = str.replace(/hh|HH/, this.getHours() > 9 ? this.getHours().toString() : '0' + this.getHours());
        str = str.replace(/h|H/g, this.getHours());
        str = str.replace(/mi/, this.getMinutes() > 9 ? this.getMinutes().toString() : '0' + this.getMinutes());
        str = str.replace(/m/g, this.getMinutes());

        str = str.replace(/ss|SS/, this.getSeconds() > 9 ? this.getSeconds().toString() : '0' + this.getSeconds());
        str = str.replace(/s|S/g, this.getSeconds());
        return str;
    };
    //日期时间相加 允许秒，分，小时，日，周，月
    Date.prototype.dateAdd = function (strInterval, Number) {
        var dtTmp = this;
        switch (strInterval) {
            case 's' :
                return new Date(Date.parse(dtTmp) + (1000 * Number));
            case 'n' :
                return new Date(Date.parse(dtTmp) + (60000 * Number));
            case 'h' :
                return new Date(Date.parse(dtTmp) + (3600000 * Number));
            case 'd' :
                return new Date(Date.parse(dtTmp) + (86400000 * Number));
            case 'w' :
                return new Date(Date.parse(dtTmp) + ((86400000 * 7) * Number));
            case 'q' :
                return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number * 3, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
            case 'm' :
                return new Date(dtTmp.getFullYear(), (dtTmp.getMonth()) + Number, dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
            case 'y' :
                return new Date((dtTmp.getFullYear() + Number), dtTmp.getMonth(), dtTmp.getDate(), dtTmp.getHours(), dtTmp.getMinutes(), dtTmp.getSeconds());
        }
    };

    //比较日期差 dtEnd 格式为日期型或者有效日期格式字符串
    Date.prototype.dateDiff = function (strInterval, EndDate) {
        var dtStart = this;
        if (typeof dtEnd == 'string') {
            dtEnd = holly.string2Date(dtEnd);
        }
        switch (strInterval) {
            case 's' :
                return parseInt((dtEnd - dtStart) / 1000);
            case 'n' :
                return parseInt((dtEnd - dtStart) / 60000);
            case 'h' :
                return parseInt((dtEnd - dtStart) / 3600000);
            case 'd' :
                return parseInt((dtEnd - dtStart) / 86400000);
            case 'w' :
                return parseInt((dtEnd - dtStart) / (86400000 * 7));
            case 'm' :
                return (dtEnd.getMonth() + 1) + ((dtEnd.getFullYear() - dtStart.getFullYear()) * 12) - (dtStart.getMonth() + 1);
            case 'y' :
                return dtEnd.getFullYear() - dtStart.getFullYear();
        }
    };
    //取得当前日期所在月的最大天数
    Date.prototype.maxDayOfDate = function () {
        var myDate = this;
        var ary = myDate.toArray();
        var date1 = (new Date(ary[0], ary[1] + 1, 1));
        var date2 = date1.dateAdd(1, 'm', 1);
        var result = dateDiff(date1.format('yyyy-MM-dd'), date2.format('yyyy-MM-dd'));
        return result;
    };

    // 取得日期数据信息   y 年 m月 d日 w星期 ww周 h时 n分 s秒
    Date.prototype.datePart = function (dType) {
        var myDate = this;
        var partStr = 0;
        var Week = ['日', '一', '二', '三', '四', '五', '六'];
        switch (dType) {
            case 'y' :
                partStr = myDate.getFullYear();
                break;
            case 'm' :
                partStr = myDate.getMonth() + 1;
                break;
            case 'd' :
                partStr = myDate.getDate();
                break;
            case 'w' :
                partStr = Week[myDate.getDay()];
                break;
            case 'ww' :
                partStr = myDate.WeekNumOfYear();
                break;
            case 'h' :
                partStr = myDate.getHours();
                break;
            case 'n' :
                partStr = myDate.getMinutes();
                break;
            case 's' :
                partStr = myDate.getSeconds();
                break;
        }
        return partStr;
    };

    //计算本周是一年中的第几周
    Date.prototype.weekNumOfYear = function () {
        var myDate = this;
        var ary = myDate.toArray(), year = ary[0], month = ary[1] + 1, day = ary[2];
        document.write("< script language=VBScript\> \n");
        document.write("myDate = Datue('" + month + "-" + day + "-" + year + "') \n");
        document.write("result = datePart('ww', myDate) \n");
        document.write("\n");
        return result;
    };

    // 将小数转换为百分数，支持四舍五入位数，默认为2位
    Number.prototype.toPercent = function (n) {
        n = n || 2;
        return (Math.round(this * Math.pow(10, n + 2)) / Math.pow(10, n))
                .toFixed(n)
            + '%';
    };

    /* 初始化消息插件包 */
    try {
        toastr = parent.toastr ? parent.toastr : toastr;
        toastr.options = {
            "closeButton": false,
            "debug": false,
            "positionClass": "toast-top-center",
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "3000",
            "extendedTimeOut": "1000",
            "showEasing": "linear",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
        };
    } catch (e) {
        // alert("未成功加载toastr插件包！");
    }


})(jQuery);