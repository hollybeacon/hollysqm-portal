<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>导出向导页</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<link href="${ctx}/hollybeacon/business/csv/css/ExportImport.css"
	rel="Stylesheet" type="text/css" />
<script type="text/javascript">
	var callBackFun = '${ empty param.callBackFun  ? "closeImportWin" :  param.callBackFun}' ;
	/*
	$(function() {
		var exportMode = '${exportMode}';
		if(exportMode == 'csv'){
			$("input:radio[value='csv']").attr("checked",true);
		}else if(exportMode == 'pdf'){
			$("input:radio[value='pdf']").attr("checked",true);
		}else{
			$("input:radio[value='Excle']").attr("checked",true);
		}
		
	});*/
</script>
<script type="text/javascript" src="${ctx}/hollybeacon/business/csv/js/export.js"> </script>
</head>

<body style="padding: 10px;">
	<form id="exportForm" action="${ctx}/rest/${param.module}/export" method="post">
		<input type='hidden' name="exportType" value="${exportMode}" />
		<c:forEach items="${param}" var="data">
				<input type='hidden' name="${data.key}" value="${data.value}" />
		</c:forEach>
		<div class="iform-horizontal" style="width: 550px;">
			<div class="icontrol-group">
				<div class="icontrol-label">导出字段</div>
				<div class="icontrols">
					<div class="check-panel">
						<label class="check-item"><input type="checkbox"
							id="allCkb" value=''  checked/> 全部</label>
						<c:forEach var="field" items="${fields}" varStatus="loop">
							<label class="check-item"><input type="checkbox"
								name="options" value='${loop.index}' checked /> ${field.desc}</label>
						</c:forEach>
					</div>
				</div>
			</div>
			<!-- <div class="icontrol-group">
				<div class="icontrol-label">导出文件类型</div>
				<div class="icontrols"> 
					<div class="check-panel">
						<label class="check-item"> <input name="exportType"
							type="radio" value="csv"/> csv
						</label>
						<label class="check-item"> <input name="exportType"
							type="radio" value="Excle"/>Excel
						</label>
						 <label class="check-item"> <input name="exportType"
							type="radio" value="pdf"/>pdf
						</label>
					</div>
				</div>
			</div>-->
		</div>
	</form>
	<input type="button" value="导出 " style="display:none;" onclick="exportData()"/>
</body>
</html>