/**
部门人员
**/
	var deptManager={
		/**
		 * 载入部门并绑定相关事件
		 */
		loadDeptBind:function(){
			if(perm.createDept){//新增部门权限检查
				// $(".treetoppane").removeClass();
				$(".treetoppane a[href]").show();
			}else{
				$(".treetoppane").addClass("permission");
				$(".treetoppane a[href]").hide();
			}
			$("#deptTree").tree({
				queryParams:$("input[name='showNoUsed']").is(':checked')?{}:{enabled:true},
				url: holly.getPath()+"/rest/org/findTree",
				method:"get",dnd:true,
				loadFilter:function(data){return (data.content)?data.content:data;},
				onClick:function(node){
					userManager.searchUser();
				},
				onBeforeExpand:function(node,param){
					$("#deptTree").tree('options').queryParams=$("input[name='showNoUsed']").is(':checked')?{}:{enabled:true};
                },
                onLoadSuccess:function(){
                	deptManager.expandAll($("#deptTree"));
                },
                formatter:function(node){
					//var newText=(node.text||"未知").replace("/[已停用]/g","");
                	var newText=(node.text||"未知").replace("/["+$.i18n.prop('SYS_COMMON_CLOSED')+"]/g","");
					//newText =(!node.attributes.enabled)?(newText+"[已停用]"):newText;
                	newText =(!node.attributes.enabled)?(newText+"["+$.i18n.prop('SYS_COMMON_CLOSED')+"]"):newText;
					if(node && node.attributes && !node.attributes.isOrg){
						newText += '<span class=\'tree-opt-wrap toggle-oper\'>';
						if(perm.updateDept){//编辑权限则显示编辑部门
							//newText += '<span class=\'tree-optcon icon-edit\' title=\'编辑部门\' onclick=\'deptManager.editDept(false,"'+node.id+'")\'></span>';
							newText += '<span class=\'tree-optcon icon-edit\' title="'+$.i18n.prop('SYS_DEPT_EDIT')+'" onclick=\'deptManager.editDept(false,"'+node.id+'")\'></span>';
						}
						if(perm.deleteDept){//停用/启用权限则显示停用或者启用部门
							if(node.attributes.enabled==true){//当前状态是启用
								//newText += '<span class=\'tree-optcon icon-ban-circle\' title=\'停用部门\' onclick=\'deptManager.removeDept(false,"'+node.id+'")\'></span>';
								newText += '<span class=\'tree-optcon icon-ban-circle\' title="'+$.i18n.prop('STS_DEPT_STOP')+'" onclick=\'deptManager.removeDept(false,"'+node.id+'")\'></span>';
							}else{//当前状态是停用
								//newText += '<span class=\'tree-optcon icon-play-circle\' title=\'启用部门\' onclick=\'deptManager.removeDept(true,"'+node.id+'")\'></span>';
								newText += '<span class=\'tree-optcon icon-play-circle\' title="'+$.i18n.prop('STS_DEPT_OPEN')+'" onclick=\'deptManager.removeDept(true,"'+node.id+'")\'></span>';
							}
						}
						newText += '</span>';
					}
					return newText;
				},
                 /**拖动判断：机构类型的源节点不允许拖动*/
                onBeforeDrag:function(node){
                	return (node && node.attributes && node.attributes.isOrg)?false:true;
                },
                /**拖动判断：源与目标机构相同才允许拖放*/
                onDragEnter:function(targetDom, source ){
                	var target=$("#deptTree").tree("getNode",targetDom);
                	var srcRootId=(source && source.attributes && source.attributes.rootId)?source.attributes.rootId:"";
                	var isOrg=(target && target.attributes && target.attributes.isOrg)?true:false;
                	var tarRootId=(!isOrg && target.attributes && target.attributes.rootId)?target.attributes.rootId:target.id;//得到目标机构ID
        			var srcParentId=(source && source.parentId)?source.parentId:"";
        			var tarParentId= (target &&target.parentId)?target.parentId:"";
                	return (srcRootId && tarRootId && srcRootId==tarRootId && tarParentId==srcParentId);												   //如果源节点机构ID与目标节点机构ID相同才允许拖
                },
                /**拖动保存节点*/
                onBeforeDrop:function(targetDom,source,point){

                	var target=$("#deptTree").tree("getNode",targetDom);
                	/*holly.post(holly.getPath()+"/rest/org/dragNode",{"sourceId":source.id,"targetId":target.id},function(e){
                		if(e.success){
                			$("#deptTree").tree("update",{
                				target:source.target,
                				parentId:e.content.parentId,
                				state:e.content.state,
                				attributes:e.content.attributes
                			});
                  		}
                	},true);*/
                	var moveUp='flase';
        			if(point === 'append'){
        				return false;
        			}else if(point === 'top'){
        				moveUp='true';
        			}
        			holly.post(holly.getPath()+"/rest/org/orderNode",{"sourceId":source.id,"targetId":target.id,"moveUp":moveUp},function(e){
                		if(e.success){
                			$("#deptTree").tree("update",{
                				target:source.target,
                				parentId:e.content.parentId,
                				state:e.content.state,
                				attributes:e.content.attributes
                			});
                  		}
                	},true);
                }
			});
			/**
			 * 停用部门事件->委托deptManager.searchDept查询
			 */
			$("input[name='showNoUsed']").change(deptManager.searchDept);
			/**
			 * 查询部门事件->委托deptManager.searchDept查询
			 */
			$('#search').searchbox({
				prompt: $.i18n.prop('SYS_DEPT_SEARCH'),//'按部门名称搜索',
				iconWidth:22,
				searcher:function(value){
					$('#deptTree').hide();
					$('#searchDiv').show();
					deptManager.searchDept();
					$('#searchDiv').on('click', 'li', function() {
						if($(this).attr("id") != undefined){
							var id = $(this).attr("id");
							var rootId = $(this).attr("name");
							$(this).addClass('active').siblings()
									.removeClass('active');
							//var pathStr = $(this).text();
							var node = new Object();
							node.id = id;
							node.rootId = rootId ;
							userManager.searchUser();
						}
						//deptManager.onClick(node);
					});
				}
			}).textbox('addClearBtn', {
				iconCls:'icon-clear',
				handler: function(){
					$('#searchDiv').hide();
					$('#deptTree').show();
					var params=$("input[name='showNoUsed']").is(':checked')?{}:{enabled:true};
					params.orgName=$.trim($("#search").val())==""?"":$.trim($("#search").val());
					$("#deptTree").tree('options').queryParams=params;
					$("#deptTree").tree('reload');
					//deptManager.searchDept();
				}
			});
			//$("#search").searchbox({prompt: '按部门名称搜索',iconWidth:22,searcher:deptManager.searchDept});
			if(perm.assignUser){
			/**
			 * 人员分配->委托assign.searchUser查询
			 */
			//$("#searchName").searchbox({prompt: '按姓名搜索',iconWidth:22,searcher:assign.searchUser});
			$("#searchName").searchbox({prompt:  $.i18n.prop('SYS_COMMON_USERNAME_SEARCH'),iconWidth:22,searcher:assign.searchUser});
			/**
			 * 人员分配->委托assign.searchDept查询
			 */
			$("input[name='chkShowAll']").change(assign.searchDept);
			}
			/**
			 * 部门下拉处理
			 */
			$("#deptCombo").combotree({
				url:"",method:'get',valueField:'id',textField:'text',
				novalidate:true,
				loadFilter: function(data){return (data.content)?data.content:data;},
				onLoadSuccess:function(node, data){
						var pid=$("#deptForm input[name='parentId']").val();
						if(pid){
							var node=$("#deptTree").tree('find',pid);
							if(node && node.attributes && !node.attributes.isOrg){//部门类型才设值
								$("#deptCombo").combotree("setValue",node.id).combotree("setText",node.text);
							}
						}
						var deptId=$("#deptForm input[name='id']").val();
						if(deptId){//如果是编辑状态；则应提前去除部门中等于自身的项。避免选择自身。
							var node=$('#deptCombo').combotree("tree").tree('find', deptId);
							if(node){
								var parentNode=$('#deptCombo').combotree("tree").tree('getParent',node.target);
								$('#deptCombo').combotree("tree").tree('remove',node.target);
								if(parentNode){
									$('#deptCombo').combotree("tree").tree('expandTo',parentNode.target);
								}
							}
						}
				}
			}).textbox('comboClearBtn', 'icon-clear').combotree('clear');
			/**
			 * 机构下拉联动
			 */
			$("#orgCombo").combotree({
				url:holly.getPath()+'/rest/org/findOrg',
				required:true,
				method:"post",valueField:"id",textField:"text",
				loadFilter: function(data){return (data.content)?data.content:data;},
				onLoadSuccess:function(){
						$("#orgCombo").combotree("setValue",loginUser.org.id);
				},
				onChange:function(newValue,oldValue){
					if(newValue){
						if(newValue!=oldValue && oldValue){
							$("#deptForm input[name='parentId']").val("");
						}
						var path=holly.getPath()+"/rest/org/findDept?rootId="+newValue;
						$("#deptCombo").combotree("reload",path).combotree('clear');
					}
				}
			});
			userManager.loadUserBind();
			//设置证件可用
			$('#cervificateType').combobox({
			    onChange:function(newValue,oldValue){
			    	userManager.setCervificateNo();
			    }
			});
			//密码有效期过滤
			$('#pwdValidDay').datebox('calendar').calendar({
			    validator: function(date){
			        var now = new Date();
			        var d1 = new Date(now.getFullYear(), now.getMonth(), now.getDate());
			        return d1<=date;
			    }
			});
		},

		/** 部门树查询*/
		searchDept:function(){
			$("#searchContent").html('');
			/*var params=$("input[name='showNoUsed']").is(':checked')?{}:{enabled:true};
			params.orgName=$.trim($("#search").val())==""?"":$.trim($("#search").val());
			$("#deptTree").tree('options').queryParams=params;
			$("#deptTree").tree('reload');*/
			var params=$("input[name='showNoUsed']").is(':checked')?{}:{enabled:true};
			$("#deptTree").tree('options').queryParams=params;
			$("#deptTree").tree('reload');
			params.orgName=$.trim($("#search").val())==""?"":$.trim($("#search").val());
			if( params.orgName !=""){
				holly.post(holly.getPath()+"/rest/org/searchDept",params,function(e){
					if(e.success){
						var stlist = e.content;
						if(stlist!=null&&stlist.length>0){
							$.each(stlist,function(i,n){
								var st = stlist[i];
								var nodePathStr=st.nodePath;
								var index = nodePathStr.lastIndexOf(st.orgName);
								var nodePath=nodePathStr.substr(0,index);
								var html = '<li id="'+st.id+'" name="'+st.rootId+'">'
								+nodePath+'<em>'+st.orgName+'</em></li>';
								$('#searchContent').append(html);
							});
						}else{
							//var html = '<li diabled><em>无查询结果</em></li>';
							var html = '<li diabled><em>'+$.i18n.prop('SYS_COMMON_SER_NODATA')+'</em></li>';
							$('#searchContent').append(html);
						}
					}
				});
			}else{
				$('#deptTree').show();
				var params=$("input[name='showNoUsed']").is(':checked')?{}:{enabled:true};
				params.orgName=$.trim($("#search").val())==""?"":$.trim($("#search").val());
				$("#deptTree").tree('options').queryParams=params;
				$("#deptTree").tree('reload');
			}

		},
		/**每行格式化的操作列*/
		formatTitle:function(val,rec,index){
            return val?"<span title='"+val+"'>"+val+"</span>":"";
		},

		/**
		 * 部门树编辑
		 * @param obj
		 * @param czType
		 */
		editDept:function(isAdd,ids){
			var id =isAdd?"":ids;
			$("#deptForm").form('clear');
			holly.post(holly.getPath()+"/rest/org/edit",{orgId:id},function(e){
				if(e.success && e.content){//编辑
					$("#deptForm").form('load',e.content);
					if(e.content.rootId){
						$("#deptCombo").combotree("reload",holly.getPath()+"/rest/org/findDept?rootId="+e.content.rootId);
					}
					$('#orgCombo').combotree('readonly');
					//$("#deptEnabled").text((e.content.enabled)?"启用":"停用");
					$("#deptEnabled").text((e.content.enabled)?$.i18n.prop('SYS_COMMON_OPEN'):$.i18n.prop('SYS_COMMON_STOP'));
				}else{						//新增
					var node=$("#deptTree").tree('getSelected');
					if(node){		//主页面上有选中树节点时.需要将选中信息带入
						var isEnabled=(node.attributes && node.attributes.enabled);
						if(isEnabled){//非停用才可以新增子部门
							var isOrg=(node.attributes && node.attributes.isOrg);
							var rootId=(node.attributes && node.attributes.rootId)?node.attributes.rootId:"";
							if(isOrg){//选中的为机构时
								$("#orgCombo").combotree("setValue",node.id);
							}else if(!isOrg && rootId){//选中为部门，并且rootID存在时
								$("#orgCombo").combotree("setValue",rootId);
								$("#deptForm input[name='parentId']").val(node.id);
							}
						}else{
							holly.showError("停用部门不能新增子部门");
							return;
						}
					}else{				//未默认选中时
						$("#orgCombo").combotree("setValue",loginUser.org.id);
					}
					//$('#orgCombo').combotree('enable');
					$('#orgCombo').combotree('readonly',false);
					$("#deptForm").find("input[name='enabled']").val("true");
					$("#deptForm").find("input[name='isSys']").val("false");
					$("#vdnId").combobox('setValue',"");
					//$("#deptEnabled").text("启用");
					$("#deptEnabled").text($.i18n.prop('SYS_COMMON_OPEN'));
				}
				//$("#deptDiv").dialog('setTitle',isAdd?"新建部门":"编辑部门").dialog('open');
				$("#deptDiv").dialog('setTitle',isAdd?$.i18n.prop('SYS_ORG_ADDDEPT'):$.i18n.prop('SYS_DEPT_EDIT')).dialog('open');
			});
		},
		/**
		 * 修改树节点状态()
		 * 该部门有子部门，系统提示“不允许删除有子部门的部门”
		 * 该部门内有人员，系统提示“不允许删除有人员的部门”
		 * @param enabled  false 停用，true启用
		 */
		removeDept:function(enabled,id){

			var node =$("#deptTree").tree('find',id);
			//var label=node.attributes.enabled==true?'停用':'启用';
			var label=node.attributes.enabled==true?$.i18n.prop('SYS_COMMON_STOP'):$.i18n.prop('SYS_COMMON_OPEN');
			var selNodeState=node.attributes.enabled;//当前节点实际状态
			var isCanUpdate=(selNodeState && enabled==false)?false:true;//操作的节点当前为在用,需进行（停用或删除）时要做检查是否可以操作。
			if(!isCanUpdate){
				holly.post(holly.getPath()+"/rest/org/checkDept",{id:node.id},function(e){
					if(e.success){//先确认检查通过！
						isCanUpdate=true;
					}else{
						holly.showError(e.errorMessage);
						return;
					}
	    		},true);
			}
			if(isCanUpdate){//通过检查才允许提示删除
				var isUsedState=$("input[name='showNoUsed']").is(':checked')?false:true; 	//当前是否只显示在用状态？
				//$.messager.confirm('确认'+label+'','是否'+label+'['+node.text+']?',function(r){
				$.messager.confirm($.i18n.prop('SYS_COMMON_CONFIRM')+label+'',$.i18n.prop('SYS_COMMON_ISNOT')+label+'['+node.text+']?',function(r){
				    if (r){
				    	holly.post(holly.getPath()+"/rest/org/remove", {'orgId':node.id,'enabled':enabled},function(q){
				    		if(q.success){
				    			if(isUsedState && (enabled==false)){//只显示在用，并且修改值为停用时允许删除节点
				    				$('#deptTree').tree('remove', node.target);
				    			}else{									//否则只允许修改节点
				    				node.attributes.enabled=enabled;
				    				$('#deptTree').tree('update',{
				    					target:node.target,
				    					attributes:node.attributes
				    				});
				    			}
				    			//holly.showSuccess(label+"成功");
				    			holly.showSuccess(label+$.i18n.prop('SYS_COMMON_SUCCESS'));
				    		}else{
				    			//holly.showError(label+"错误，原因:"+e.errorMessage);
				    			holly.showError(label+$.i18n.prop('SYS_COMMON_ERROR_REASON')+e.errorMessage);
				    		}
				    	});
				    }
				});
			 }
		},
		/**
		 * 保存部门信息
		 */
		saveDept:function(){
			var deptform=$("#deptForm");
			//var pid=$("#deptCombo").combotree("getValue")||deptform.find("#parentId").val();
			var id=deptform.find("#id").val();
			$.each(deptform.find(".easyui-textbox"), function (index, v) {
				var text=$.trim($(this).textbox("getValue"));
				$(this).textbox("setValue",text);
			});
			if($("#vdnId").combobox("getValue")!=""){
				$("#deptForm").find("input[name='isVdn']").val("true");
			}else{
				$("#deptForm").find("input[name='isVdn']").val("false");
			}
			if(deptform.form('enableValidation').form("validate")){
				deptform.form("submit",{url:holly.getPath()+"/rest/org/save",
				onSubmit:function(){
					$.messager.progress();
					deptform.find("#parentId").val($("#deptCombo").combotree("getValue"));
					isCanSave=false;
					holly.post(holly.getPath()+"/rest/org/checkDept",{"id":id||"","orgName":deptform.find("input[name='orgName']").val(),"rootId":$("#orgCombo").combotree("getValue")},function(e){
						if(e.success){//先确认检查通过！
							isCanSave=true;
						}else{
							$.messager.progress('close');
							holly.showError(e.errorMessage);
							return;
						}
		    		},true);
					return isCanSave;

				},
				success:function(e){
					e=(typeof(e)== "object")?e:$.parseJSON(e);
					$.messager.progress('close');
					if(e.success){
						//ID存在时节点对象为待修改节点（修改时用）,否则节点对象为目标父节点（新增时用）
						var node=id?$("#deptTree").tree("find",id):"";
						if(node){//修改时需要先删除原节点
							$("#deptTree").tree("remove",node.target);
						}
						var parentNode=$("#deptTree").tree("find",e.content.parentId);
						if(parentNode){
							$("#deptTree").tree('append', {parent: parentNode.target,data: e.content});
						}
						node=$("#deptTree").tree("find",e.content.id);
						if(node){
							$(node.target).trigger("click");
						}
						$('#deptDiv').dialog('close');
						//holly.showSuccess("部门保存成功");
						holly.showSuccess($.i18n.prop('SYS_DEPT_SAVE_SUCCESS'));
					}else{
						//holly.showError("部门保存发生问题，原因:"+e.errorMessage);
						holly.showError($.i18n.prop('SYS_DEPT_SAVE_ERROR_REASON')+e.errorMessage);
					}
				}
				});
			}
		},
		/**
		 * 展开根节点或树的所有节点
		 * @param {} ele 如果存在表示指定树展开根节点
		 */
		expandAll:function(ele){
			if(ele){//展开指定的树
				var root=ele.tree("getRoot");
				if(root){ele.tree("expand",root.target);}
			}else{//展开树的所有的
				$('#deptTree').tree('expandAll');
			}
		},
		collapseAll:function (){
			$('#deptTree').tree('collapseAll');
		},
		alertParentId:function(){
			//$('#win').dialog({href:holly.getPath()+'/rest/user/test'}).dialog('open');
		},

		/**加载页面所需数据字典**/
		loadDictionaries:function(){
			$("#education").combobox("init",{'codeType':'DEGREE'});
			$("#cervificateType").combobox("init",{'codeType':'CERTIFICATE_TYPE'});
			$("#areaCode").combobox("init",{'codeType':'AREA'});
			$("#vdnID").combobox("init",{'codeType':'VDN_TYPE'});
			$("#vdnId").combobox("init",{'codeType':'VDN_TYPE'});
		},

		/**
		 * 点击查询部门结果的所有li获取部门下的用户信息
		 */
		/*onClick:function(node){
			userManager.searchUser();
			var param=holly.form2json($("#searchUser"));
			param.deptId=node.id;
			param.orgId=node.rootId;
			$('#userGrid').datagrid('options').queryParams=param;
			$('#userGrid').datagrid('reload');
		}*/
	};

/**
 * 人员管理
 */
	var userManager={
			/**绑定用户相关事件*/
			loadUserBind:function(){
				$("#userGrid").datagrid({
	                width: 'auto',
	                singleSelect : true,
	                method:'get',
	                border:false,
	                fit:true,
	                fitColumns:true,
	                pagination: true,
	                rownumbers: false,
	                remoteSort:false,
					multiSort:true,
	                queryParams:userManager.getParams(),
	                url:holly.getPath()+"/rest/user/showListRest?SQL_FLAG=2",
	                loadFilter: function(data){return (data.content)?data.content:data;},
	                //loadMsg:'数据加载中请稍后……',
	                loadMsg:$.i18n.prop('SYS_COMMON_DATAING'),
	                onLoadSuccess: function (data) {
	        			$('.jsSetting-menubutton').menubutton();
	        		}
	            });

				/**
				 * 部门下拉处理
				 */
				$("#userDeptCombo").combotree({
					url:"",
					method:'get',valueField:'id',textField:'text',
					loadFilter: function(data){return (data.content)?data.content:data;}
				});
				/**
				 * 机构下拉联动
				 */
				$("#userOrgCombo").combotree({
					url:holly.getPath()+'/rest/org/findOrg',
					method:"post",valueField:"id",textField:"text",
					loadFilter: function(data){return (data.content)?data.content:data;},
					onLoadSuccess:function(){
							$("#userOrgCombo").combotree("setValue",loginUser.org.id);
					},
					onChange:function(newValue,oldValue){
						if(newValue){
							var path=holly.getPath()+"/rest/org/findDept?rootId="+newValue;
							$("#userDeptCombo").combotree("reload",path);

							var pathRole=holly.getPath()+"/rest/role/findRoleNode?orgId="+newValue;
							$("#userRoleCombo").combotree("reload",pathRole);
						}
					}
				});
				/**
				 * 角色下拉
				 */
				$("#userRoleCombo").combotree({
					url:"",
					method:"get",valueField:"id",textField:"text",
					multiple:true,animate:true,
					cascadeCheck:false,
					loadFilter: function(data){
						return (data.content)?data.content:data;
					},
					/*onCheck: function(node, checked){
						if(node.iconCls!="tree-role"){
							$("#userRoleCombo").combotree("tree").tree("uncheck",node.target);
						}

					},*/
					onLoadSuccess:function(node, data){
						deptManager.expandAll($("#userRoleCombo").combotree("tree"));
					}
				});
				userManager.uploadEventBind();
			},

			/**查找用户具有的角色**/
			searchUserRole:function(uid){
				var nodes = $('#userRoleCombo').tree('getChecked');
				if(nodes.length>0){
					$.each(nodes,function(){
                    $("#userRoleCombo").tree('uncheck',this.target);
					});
				}
                holly.post(holly.getPath()+"/rest/role/findUserRole",{"id":uid},function(e){
                		if(e.success && e.content){

                			var idArr=e.content.split(",");
                			$.each(idArr,function(){
                				deptManager.expandAll($("#userRoleCombo").combotree("tree"));
                				var n = $("#userRoleCombo").combotree("tree").tree('find', this);
                                if(n){
                                    $("#userRoleCombo").combotree("tree").tree('check',n.target);
                                }
                			});
                			}

                	});

			},
			/**每行格式化的操作列*/
			formatType:function(val,rec,index){
                     var html="<span class='grid-opt-wrap'>" +
                    (perm.updateUser?"<a href='javascript:void(0)' onclick='userManager.showUser(\""+rec.id+"\")' class='grid-optcon icon-edit' title=\""+$.i18n.prop('SYS_COMMON_EDIT')+"\"></a>" +
					(rec.isLock?"<a class='grid-optcon icon-unlock "+rec.id+"' href='javascript:void(0)' onclick='userManager.userEvnet(3,this)'  title=\""+$.i18n.prop('SYS_COMMON_UNLOCK')+"\"></a>":""):"") +
					"<a href='javascript:void(0)' onMouseOver='userManager.menuPamams(\""+rec.id+"\","+rec.enabled+","+index+")'  class='jsSetting-menubutton grid-optcon icon-cog' data-options=\"menu:'#user-menu'\"></a>" +
					"</span>";
                    return (perm.updateUser||perm.deleteUser)?html:"";
			},
			/**对enabled属性格式化 */
			formatType2:function(enabled){
				var str1 = "";
		    	if(enabled==1){
		    		str1 = $.i18n.prop('SYS_COMMON_OPEN');
		    	}else if(enabled==0){
		    		str1 = $.i18n.prop('SYS_COMMON_STOP');
		    	}else if(enabled==2){
		    		str1 = $.i18n.prop('SYS_COMMON_DEPARTURE');
		    	}
		    	return str1;
			},
			menuPamams:function(userId,srcEnabled,index){
				$("#user-menu").attr("userId",userId).attr("selectRow",index);
				$("#user-menu >.czItem:hidden").show();
				$("#user-menu >.czItem").eq(srcEnabled).hide();
				if(!perm.deleteUser){//无删除权限则不显示停用
					$("#tree-menu >.czItem").eq(0).hide();
					$("#tree-menu >.czItem").eq(2).hide();
				}
				if(!perm.updateUser){//无修改权限则不显示启用和重置密码
					$("#tree-menu >.czItem:gt(0)").hide();
				}
			},
			/**得到表单表格查询参数*/
			getParams:function(){
				var param=holly.form2json($("#searchUser"));
				if($('#deptTree').css('display') == "block"){
					var orgNode=$('#deptTree').tree("getSelected");
					if(orgNode){//将当前选中的部门或机构作为查询参数
							var isOrg=(orgNode.attributes && orgNode.attributes.isOrg)?true:false;
							//$("#includeUser").panel("setTitle",(isOrg?"机构:":"部门:")+"[<span class='sub'>"+orgNode.text+"</span>]包含成员");
							$("#includeUser").panel("setTitle",(isOrg?$.i18n.prop('SYS_COMMON_ORG')+":":$.i18n.prop('SYS_COMMON_DEPT')+":")+"[<span class='sub'>"+orgNode.text+"</span>]"+$.i18n.prop('SYS_COMMON_INCLUDEMEMBER'));
						if(orgNode.attributes && orgNode.attributes.isOrg){
							param.orgId=orgNode.id;
							param.deptId="";
						}else{
							param.deptId=orgNode.id;
							param.orgId=orgNode.attributes.rootId;
						}
						$("#searchUser").find("input[name='orgId']").val(param.orgId||"");
						$("#searchUser").find("input[name='deptId']").val(param.deptId||"");
					}
					//param.orgId=(param.orgId)?param.orgId:loginUser.orgId;
				}else{
					/*alert($("#searchDiv li.active").attr("id"));
					alert($("#searchDiv li.active").attr("name"));*/
					var node = $("#searchDiv li.active");
					param.deptId=node.attr("id");
					param.orgId=node.attr("name");
					var isOrg = false;
					//$("#includeUser").panel("setTitle",(isOrg?"机构:":"部门:")+"[<span class='sub'>"+$("#searchDiv li.active em").text()+"</span>]包含成员");
					$("#includeUser").panel("setTitle",(isOrg?$.i18n.prop('SYS_COMMON_ORG')+":":$.i18n.prop('SYS_COMMON_DEPT')+":")+"[<span class='sub'>"+$("#searchDiv li.active em").text()+"</span>]"+$.i18n.prop('SYS_COMMON_INCLUDEMEMBER'));

				}
				param.orgId=(param.orgId)?param.orgId:loginUser.orgId;
				return param;
			},
			/**查询用户*/
			searchUser:function(){
				$('#userGrid').datagrid('options').queryParams=userManager.getParams();
				$('#userGrid').datagrid('reload');
			},
			/**新增用户*/
			addUser:function(){
				$("#pwdDiv").show();
				$("#account").textbox({validType:['length[5,20]','vaildOrgAccount']});
				var node=$("#deptTree").tree('getSelected');
				$('#userOrgCombo').combotree('enable');
				$('#userRoleCombo').combotree('enable');
				$("#account").combotree('enable');
				$("#password").textbox({value:"12345678Az"});
				$("img[name='pre_photo']").attr("src","");
				$(".img-preview").removeClass("uploaded");
				$("#pwdValidDay").datebox("setValue",new Date().dateAdd("m",3).format("yyyy-MM-dd"));
				$("#education,#cervificateType,#areaCode,#vdnID").combobox('setValue',"");
				$("#cervificateNo").textbox({"disabled":true});
				$(":radio[name='sex']").get(0).checked = true;
				$("#hireDate").datebox("setValue", new Date().toDateString());
				if(node){		//主页面上有选中树节点时.需要将选中信息带入
					var isOrg=(node.attributes && node.attributes.isOrg);
					var rootId=(node.attributes && node.attributes.rootId)?node.attributes.rootId:"";

					$("#userDeptCombo").combotree('tree').tree({// 覆盖编辑用户时的onLoadSuccess（注意是tree的onLoadSuccess，而不是combotree的onLoadSuccess）
						onLoadSuccess:function(){
							var tmpId=$("#userDeptCombo").attr("tmpId");
							if(tmpId){//存在临时设置过的值，则在加载完成后选中它//新增中时
								$("#userDeptCombo").combotree("setValue",tmpId);
								$("#userDeptCombo").attr("tmpId","");
							}
							var selValue=$("#userDeptCombo").combotree("getValue");
							if(selValue){
								var node=$("#userDeptCombo").combotree("tree").tree('find', selValue);
								if(node){
								$("#userDeptCombo").combotree("setValue",node.id);
								$("#userDeptCombo").combotree("tree").tree("expandTo",node.target);
								}
							}
						}
					});
					if(isOrg){//选中的为机构时
						$("#userOrgCombo").combotree("setValue",node.id);
					}else if(!isOrg && rootId){//选中为部门，并且rootID存在时
						$("#userDeptCombo").attr("tmpId",node.id);//设置临时选中的值。避免联动加载数据时丢失Value
						$("#userOrgCombo").combotree("setValue",rootId);
					}
				}else{				//未默认选中时，先判断是否是模糊搜索
					var node = $("#searchDiv li.active");
					if(node.length>0&&!$("#searchDiv").is(':hidden')){//模糊查询有选中部门
						$("#userDeptCombo").combotree('tree').tree({// 覆盖编辑用户时的onLoadSuccess（注意是tree的onLoadSuccess，而不是combotree的onLoadSuccess）
							onLoadSuccess:function(){
								var tmpId=$("#userDeptCombo").attr("tmpId");
								if(tmpId){//存在临时设置过的值，则在加载完成后选中它//新增中时
									$("#userDeptCombo").combotree("setValue",tmpId);
									$("#userDeptCombo").attr("tmpId","");
								}
								var selValue=$("#userDeptCombo").combotree("getValue");
								if(selValue){
									var node=$("#userDeptCombo").combotree("tree").tree('find', selValue);
									if(node){
									$("#userDeptCombo").combotree("setValue",node.id);
									$("#userDeptCombo").combotree("tree").tree("expandTo",node.target);
									}
								}
							}
						});
						//模糊查询选择的必定是部门，不是组织机构
						$("#userDeptCombo").attr("tmpId",node.attr("id"));//设置临时选中的值。避免联动加载数据时丢失Value
						$("#userOrgCombo").combotree("setValue",node.attr("name"));
					}else{
						$("#userOrgCombo").combotree("setValue",loginUser.org.id);
						//将新建DIV里的组织机构和部门combobox的值设置为空
						$("#userDeptCombo").attr("tmpId",'');
						//$("#userOrgCombo").combotree("setValue",loginUser.orgId);//获取org对象为空，直接去user的orgid
					}
				}
			},
			/**编辑用户*/
			editUser:function(uid){
				$("#pwdDiv").hide();
				$("#account").textbox({validType:""});
				holly.get(holly.getPath()+"/rest/user/showUpdateRest",{id:uid},function(e){
						if(e.success && e.content){
							$("#account").combotree('disable');
							if(e.content.orgId){
								$("#userDeptCombo").combotree("reload",holly.getPath()+"/rest/org/findDept?rootId="+e.content.orgId).combotree('tree').tree({
									onLoadSuccess:function(){
										var tmpId=$("#userDeptCombo").attr("tmpId");
										if(tmpId){//存在临时设置过的值，则在加载完成后选中它//新增中时
											$("#userDeptCombo").combotree("setValue",tmpId);
											$("#userDeptCombo").attr("tmpId","");
										}
										var selValue=$("#userDeptCombo").combotree("getValue");
										if(selValue){
											var node=$("#userDeptCombo").combotree("tree").tree('find', selValue);
											if(node){
											$("#userDeptCombo").combotree("setValue",node.id);
											$("#userDeptCombo").combotree("tree").tree("expandTo",node.target);
											}
										}
										$("#userDeptCombo").combotree('setValue',e.content.deptId);
									}
								});
								$("#userRoleCombo").combotree("reload",holly.getPath()+"/rest/role/findRoleNode?orgId="+e.content.orgId);
							}
							if(e.content.photo){//预览图
								$("img[name='pre_photo']").attr("src",holly.getResource(e.content.photo,true));
								$(".img-preview").addClass("uploaded");
							}else{
								$("img[name='pre_photo']").attr("src","");
								$(".img-preview").removeClass("uploaded");
							}
							$("#userOrgCombo").combotree('disable');
							$("#userForm").form('load',e.content);

							//加载用户角色
							//userManager.searchUserRole(uid);
						}
					});


			},

			/**显示用户*/
			showUser:function(uid){
				$("#userForm").form('clear');
				if(uid){	//编辑
					userManager.editUser(uid);
					$("#userRoleCombo").combotree({
						onLoadSuccess:function(){
							//加载用户角色
							userManager.searchUserRole(uid);
						}
					});
				}else{	//新增
					userManager.addUser();
					$("#userRoleCombo").combotree({
						onLoadSuccess:function(){
							//清空用户角色
							userManager.searchUserRole("");
						}
					});
				}
				//$("#userDiv").dialog({title:uid?"编辑人员":"新建人员"}).dialog('open');
				$("#userDiv").dialog({title:uid?$.i18n.prop('SYS_DEPT_EDITUSER'):$.i18n.prop('SYS_ORG_ADDUSER')}).dialog('open');
				//$('#userDiv').dialog('options').title=uid?"编辑人员":"新建人员",
				//$('#userDiv').dialog('open');
			},

			/**保存用户*/
			saveUser:function(){
				var userform=$("#userForm");
				//空格文本验证前替换
				$.each(userform.find(".easyui-textbox"), function (index, v) {
					var text=$.trim($(this).textbox("getValue"));
					$(this).textbox("setValue",text);
				});
				//给予角色设置参数
				var ids=new Array();
				$.each( $('#userRoleCombo').combotree('tree').tree('getChecked'),function(){
					if(this.iconCls=="tree-role"){
						ids.push(this.id);
					}
				});
				$("#roleIds").val(ids.join(","));

				var isVaild=userform.form('enableValidation').form('validate');
				if(isVaild){
					$.messager.progress();
					userform.form("submit",{url:holly.getPath()+"/rest/user/saveOrUpdateRest",
							onSubmit:function(){},
							success:function(e){
								$.messager.progress('close');
								e=(typeof(e)== "object")?e:$.parseJSON(e);
								$('#userDiv').dialog('close');
								if(e.success){
									$('#userGrid').datagrid('reload');
									//holly.showSuccess("人员保存成功");
									
									holly.showSuccess($.i18n.prop('STS_COMMON_OPERATE_SUCCESS'));
								}else{
									//holly.showError("人员保存发生问题，原因:"+e.errorMessage);
									holly.showError($.i18n.prop('SYS_DEPT_SAVEUSER_ERROR_REASON')+e.errorMessage);
								}
							}
					});
				}
			},
			/**
			 * 用户事件请求
			 * @param type	1:密码重置 2:状态修改(停用/启用/离职) 3:解锁用户 
			 * @param enabled false:"停用" true:"启用"
			 */
			userEvnet:function(type,enabled){
				var userId=$('#user-menu').attr("userId");
				if(type==3){//解锁时需要把enabled当对象this来得到userId
					userId=$.trim($(enabled).attr("class").replace("grid-optcon icon-unlock ",''));
				}
				if(userId){
				    var json={};
				    switch(type){
				    	case 1://"密码重置"
				    	//json={title:"密码重置",param:{'type':type,'userId':userId},content:"确定为该帐号重置密码吗？",message:"帐号已重置为初始密码！"};
			    		json={title:$.i18n.prop('SYS_ORG_RESETPASSWORD'),param:{'type':type,'userId':userId,'enabled':0},content:$.i18n.prop('SYS_DEPT_RESTPWD_TIP'),message:$.i18n.prop('SYS_DEPT_RESTPWD_SUCCESS_TIP')};
			    		break;
				    	case 2://"状态修改(停用/启用)"
				    	//json={title:"变更状态",param:{'type':type,'userId':userId,'enabled':enabled},content:"确定修改该帐号状态为"+(enabled?"启用":"停用")+"吗？",message:"帐号已成功"+(enabled?"启用":"停用")};
				    	var str1 = userManager.formatType2(enabled);
			    		json={title:$.i18n.prop('SYS_DEPT_USER_STATE'),param:{'type':type,'userId':userId,'enabled':enabled},content:$.i18n.prop('SYS_DEPT_USER_STATE_TIP1')+str1+$.i18n.prop('SYS_DEPT_USER_STATE_TIP2'),message:$.i18n.prop('SYS_DEPT_USER_STATE_SUCCESS')+str1};
			    		break;
				    	case 3://"解锁用户"
				    	//json={title:"解锁用户",param:{'type':type,'userId':userId},content:"确认解锁该帐号吗？",message:"帐号已成功解锁"};
			    		json={title:$.i18n.prop('SYS_DEPT_USER_ULOCK'),param:{'type':type,'userId':userId,'enabled':0},content:$.i18n.prop('SYS_DEPT_USER_UNLOCK_TIP'),message:$.i18n.prop('SYS_DEPT_USER_UNLOCK_SUCCESS')};
				    	break;
				    	//case 4://"状态修改(离职)"
				    	//json={title:$.i18n.prop('SYS_COMMON_DEPARTURE'),param:{'type':type,'userId':userId},content:$.i18n.prop('SYS_DEPT_USER_DEPARTURE_TIP'),message:$.i18n.prop('SYS_DEPT_USER_DEPARTURE_SUCCESS')}
				    	//break;
				    }
				 	$.messager.confirm(json.title,json.content,function(r){
				    	if(r){
							holly.post(holly.getPath()+"/rest/user/userEvent",json.param,function(e){
								if(e.success){
									if(type==3){
										$("a[class='grid-optcon icon-unlock "+userId+"']").remove();
									}
									var curRowIndex=$("#user-menu").attr("selectRow");
									if(curRowIndex && e.content){
										$("#userGrid").datagrid("updateRow", { index: curRowIndex, row:{enabled:e.content.enabled}});
										$('.jsSetting-menubutton').menubutton();
									}
								holly.showSuccess(json.message);
							}else{
								holly.showError(e.errorMessage);
							}});
						}
					});
				}
			},
			/**用户头像上传*/
			uploadEventBind:function(){
				$(".file-op").fileupload({		  //绑定上传按钮及动作
					autoUpload : true,							  // 是否自动上传
					dataType: 'json',
					singleFileUploads:true,
					url : holly.getPath()+"/rest/mediafile/upload?accept=jpg,png,gif",// 上传地址
					start: function (e) {
						$(".img-preview >.img-loading").css("display", "block");
					},
					done : function(e, data) {// 设置文件上传完毕事件的回调函数
						var json=data.result;
						$(".img-preview >.img-loading").css("display", "none");
						if(json.success && json.content){
							$("img[name='pre_photo']").attr("src",holly.getResource(json.content.mediaId,true));
							$("input[name='photo']").val(json.content.mediaId);
							$(".img-preview").addClass("uploaded");
						}else{
							holly.showError(json.errorMessage);
						}
					}
				});
				$(".img-preview .img-close").bind('click', function(){//绑定图片删除事件
					//$.messager.confirm('删除图片', '确定需要删除该图片吗？', function(r){
					$.messager.confirm($.i18n.prop('SYS_DEPT_USER_DELPIC_TITLE'), $.i18n.prop('SYS_DEPT_USER_DELPIC_CONTENT'), function(r){
						if (r){
							$(".img-preview img[name='pre_photo']").attr("src","");
							$(".img-preview").removeClass("uploaded");
							$("#userForm input[name='photo']").val("");
						}
					});
				});
			},
			/**选中部门**/
			selectDept:function(){
				var node=$("#deptTree").tree('getSelected');
				if(node){
					return (node && node.attributes && !node.attributes.isOrg)?node:"";
				}else{
					var node = $("#searchDiv li.active");
					if(node.length>0&&!$("#searchDiv").is(':hidden')){//模糊查询有选中角色
						return node;
					}
				}		
			},
			/**分配用户*/
			assignUser:function(){
				var deptNode=userManager.selectDept();
				if(deptNode&&deptNode.attributes){
					if(!deptNode.attributes.enabled){
						//holly.showError("已停用部门不允许分配成员！");
						holly.showError($.i18n.prop('SYS_DEPT_USER_SIGN_Warn_TIP1'));
						return;
					}
					var rootId=(deptNode.attributes && deptNode.attributes.rootId)?deptNode.attributes.rootId:"";
					assign.resetDialg();
					$("#searchName").textbox("setValue","");
					$('#assign-deptId').val(deptNode.id);
					$('#assign-orgId').val(rootId);
					//$('#assignUser').dialog('options').title="分配成员至指定部门:<span class='tagging'>["+deptNode.text+"]</span>",
					$('#assignUser').dialog('options').title=$.i18n.prop('SYS_DEPT_USER_SIGN_TIP')+"<span class='tagging'>["+deptNode.text+"]</span>",
					$('#assignUser').dialog({onBeforeOpen:assign.loadTree}).dialog("open");
				}else if(deptNode&&!deptNode.attributes){//模糊查询选中
					assign.resetDialg();
					$("#searchName").textbox("setValue","");
					$('#assign-deptId').val(deptNode.attr("id"));
					$('#assign-orgId').val(deptNode.attr("name"));
					var deptname_ = $("#searchDiv li.active em").text();
					$('#assignUser').dialog('options').title=$.i18n.prop('SYS_DEPT_USER_SIGN_TIP') +"<span class='tagging'>["+deptname_+"]</span>",
					$('#assignUser').dialog({onBeforeOpen:assign.loadTree}).dialog("open");
				}else{
					//holly.showError("请选择人员分配的部门");
					holly.showError($.i18n.prop('SYS_DEPT_USER_SIGN_Warn_TIP2'));
				}
			},
			importUser:function(toPage){
				if(toPage){
					var url =holly.getPath()+"/rest/csv/wizard/import?module=user&tableId=entities.SysUser";
					var targetIframe ="iframe-import";
					$('#searchUser').attr("action",url);
					$('#searchUser').attr("target",targetIframe);
					$('#searchUser').submit();
					$('#dlg-import').dialog('open');
				}else{
					$("#iframe-import")[0].contentWindow.importData();
					$("#file").filebox("setValue","");
					//$('#dlg-import').dialog('close');
				}
			},
			importGroup:function(toPage){//导入组别
				if(toPage){
					var url =holly.getPath()+"/rest/groupUpload/wizard/import?module=groupUpload&tableId=group.importUser";
					var targetIframe ="iframe-import";
					$('#searchUser').attr("action",url);
					$('#searchUser').attr("target",targetIframe);
					$('#searchUser').submit();
					$('#dlg-import').dialog('open');
				}else{
					$("#iframe-import")[0].contentWindow.importData();
					$("#file").filebox("setValue","");
				}
			},
			exportUser:function(toPage){
				if(toPage){
					var url =holly.getPath()+"/rest/csv/wizard/sqlexport?tableId=personoa.findUserList&sqlKey=velocity.findUserList&mediaId=1";
					var targetIframe ="iframe-export";
					$('#searchUser').attr("action",url);
					$('#searchUser').attr("target",targetIframe);
					$('#searchUser').submit();
					$('#dlg-export').dialog('open');
				}else{
					$("#iframe-export")[0].contentWindow.exportData();
				}
			},
			setCervificateNo:function(){
				var val=$("#cervificateType").combobox('getValue');
				if(val!=""){
					$("#cervificateNo").textbox({"disabled":false});
				}else{
					$("#cervificateNo").textbox({"disabled":true});
				}
			}
	};

	var assign={
		/**查询用户*/
		searchUser:function(){
			var param={enabled:1};
			var orgNode=$('#ctrl-tree').tree("getSelected");
			if(orgNode){//将当前选中的部门或机构作为查询参数
				if(orgNode.attributes && orgNode.attributes.isOrg){
					param.orgId=orgNode.id;
				}else{
					param.deptId=orgNode.id;
					param.orgId=orgNode.attributes.rootId;
				}
			}
			param.orgId=(param.orgId)?param.orgId:loginUser.org.id;
			param.userName=$("#searchName").val();
			holly.post(holly.getPath()+"/rest/user/findUser",param,function(e){
				if(e.success){
					$("#srcList .select-check:eq(0)").prop('checked',false);
					$("#srcList > li:gt(0)").remove();
					var html="";
					$.each(e.content, function (index, v) {
						if(v.id && v.userCode){
						  var toolsTip={deptName:v.deptName,orgName:v.orgName};
						  var checked=$("#targetList .select-item[userId='"+v.id+"']").length>0?"checked=\"checked\"":"";	//检查目标项是否存在
						  //var tip="<div id='"+v.id+"' class='opt-dropdown-noline assign' data-options='minWidth:100'><div>所属部门：{deptName}</div></div>".replace(/{deptName}/g,toolsTip.deptName||"");
						  var tip="<div id='"+v.id+"' class='opt-dropdown-noline assign' data-options='minWidth:100'><div>"+$.i18n.prop('SYS_COMMON_BELONGDEPT')+"：{deptName}</div></div>".replace(/{deptName}/g,toolsTip.deptName||"");
						  html+='<li><label class="select-item" userId="'+v.id+'">'+'<input type="checkbox" userId="'+v.id+'" class="select-check" '+checked+'>'+
						  		'<span class="name">'+v.userName+'</span><span class="userCode">（'+v.userCode+'）</span>'+
						  		'</label><span class="opr-wrp"><i class="jsassignUser-menubutton opr-icon icon-info-sign" data-options="menu:\'#'+v.id+'\'"></i>'+tip+'</span></li>';
						}
					});
					$("#srcList").append(html);
					$("#srcList .select-check").bind('change',assign.changeEvent);
					$('.jsassignUser-menubutton').menubutton();
					if(e.content!=''){
						//全选是否选中
						$("#srcList .select-check:eq(0)").prop('checked',($("#srcList .select-check:gt(0):checked").length==$("#srcList .select-check:gt(0)").length && $("#srcList .select-check:gt(0):checked").length > 0));
					}
				}
			});

		},
		changeEvent:function(){
			var selAll=$(this).parent().hasClass('selsrcAll');
				if(selAll){//如果是全选
					if($(this).is(':checked')==true){//全选勾取
						$("#srcList .select-check:gt(0)").prop('checked',true);
						$.each($("#srcList > li:gt(0)"),function(i,v){
							assign.moveToTarget(v);
						});
					}else{//全选取消
						$("#srcList .select-check:gt(0)").prop('checked',false);
						$.each($("#srcList > li:gt(0)"),function(i,v){
							assign.removeTarget(v,true);
						});
					}
				}else{//单选
					//单选项勾取或取消的数量与可选项总数相等则全选按钮 保持切换一致
					$("#srcList .select-check:eq(0)").prop('checked',($("#srcList .select-check:gt(0):checked").length==$("#srcList .select-check:gt(0)").length));
					if($(this).is(':checked')==true){//单项勾取
						assign.moveToTarget($(this).parent().parent());
					}else{							//单项取消
						assign.removeTarget($(this).parent().parent(),true);
					}
				}
				$('.jsassignUser-menubutton').menubutton();
		},
		moveToTarget:function(src){
			var userId=$(src).find("label[userId]").attr("userId");
			if($("#targetList .select-item[userId='"+userId+"']").length==0){
				var $target=$(src).clone();
				$target.find(".select-check").remove();
				$("#targetList").append($target);
				$target.find(".opr-wrp").append("<i class='opr-icon icon-remove' onclick='assign.removeTarget(this);'></i>");
			}
		},
		removeTarget:function(obj,isfromSrc){
			if(isfromSrc){
				var userId=$(obj).find("label[userId]").attr("userId");
				$("#targetList .select-item[userId='"+userId+"']").parent().remove();
			}else{
				var userId=$(obj).parent().parent().find(".select-item[userId]").attr("userId");
				$("#srcList input[userId='"+userId+"']").prop('checked',false);
				$(obj).parent().parent().remove();
				$("#srcList .select-check:eq(0)").prop('checked',($("#srcList .select-check:gt(0):checked").length==$("#srcList .select-check:gt(0)").length));
			}
		},

		resetDialg:function(){
			$("#targetList").empty();
			$("#srcList > li:gt(0)").empty();
			$("input[name='chkShowAll']").attr("checked",false);
			$('#assign-deptId,#assign-orgId').val("");
		},
		/**确认分配*/
		saveAssign:function(){
			var ids=$("#targetList .select-item[userId]").map(function(index){
				return $(this).attr("userId");
			}).get();
			if(ids.length>0){
				var param={"deptId":$('#assign-deptId').val(),"orgId":$('#assign-orgId').val(),"id":ids.join(",")};
				holly.post(holly.getPath()+"/rest/user/assignUser",param,function(e){
					if(e.success){
						$("#userGrid").datagrid("reload");
						$('#assignUser').dialog('close');
						//holly.showSuccess("所选人员已移动至指定部门");
						holly.showSuccess($.i18n.prop('SYS_DEPT_SIGN_USER'));
						assign.resetDialg();
					}else{
						//holly.showError("分配失败");
						holly.showError($.i18n.prop('SYS_DEPT_SIGN_USER_ERROR'));
					}
				});
			}else{
				//holly.showError("请先选择分配的人员！");
				holly.showError($.i18n.prop('SYS_GROUP_TIP12'));
			}
		},
		/** 部门树查询*/
		searchDept:function(){
			var params=$("input[name='chkShowAll']").is(':checked')?{}:{enabled:true};
			$("#ctrl-tree").tree('options').queryParams=params;
			$("#ctrl-tree").tree('reload');
		},
		loadTree:function(){
			$("#ctrl-tree").tree({
				queryParams:{enabled:true},method:"get",
				url: holly.getPath()+"/rest/org/findTree",
				loadFilter:function(data){return (data.content)?data.content:data;},
				onClick:function(node){
					assign.searchUser();
				},
				onLoadSuccess:function(){
                	deptManager.expandAll($("#ctrl-tree"));
                }
			});
		}
	};

/**监听分配人员dialog关闭，清空tips的div,保证数据是最新的**/
$(function() {
	$("#assignUser").dialog({
		onClose : function() {
			$(".opt-dropdown-noline.assign").remove();
			$("#srcList .select-check:eq(0)").prop('checked',false);
		}
	});
});