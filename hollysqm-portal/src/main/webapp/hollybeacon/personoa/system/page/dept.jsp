<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<%@ include file="/hollybeacon/common/language.jsp"%>
<fmt:bundle basename="${language}" >
<!DOCTYPE html>
<html>
<head>
<title><fmt:message key='SYS_DEPT_TITLE'/></title>
<script type="text/javascript" src="${ctx}/hollybeacon/personoa/system/js/dept.js" > </script>
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type="text/javascript">
var loginUser=${loginUser};
var perm={view:<%=SecurityUtils.getSubject().isPermitted("SysUser:view")%>,
createDept:<%=SecurityUtils.getSubject().isPermitted("SysOrg:create")%>,
updateDept:<%=SecurityUtils.getSubject().isPermitted("SysOrg:update")%>,
deleteDept:<%=SecurityUtils.getSubject().isPermitted("SysOrg:delete")%>,
createUser:<%=SecurityUtils.getSubject().isPermitted("SysUser:create")%>,
updateUser:<%=SecurityUtils.getSubject().isPermitted("SysUser:update")%>,
deleteUser:<%=SecurityUtils.getSubject().isPermitted("SysUser:delete")%>,
assignUser:<%=SecurityUtils.getSubject().isPermitted("SysUser:assign")%>
};
$(function() {

	deptManager.loadDeptBind();
	deptManager.loadDictionaries();
	$('input[name=orgId]').val(loginUser.orgId);
	$("#searchUser").keyup(function(event){
		if(event.keyCode==13){
			userManager.searchUser();
		}
	});
});

</script>
</head>
<body class="easyui-layout">
	<div data-options="region:'west',border:false" class="layout-treepane" style="width:220px;">
		<div class="treetoppane">
			<div class="treeoptpane">
				<a href="javascript:;" class="add-btn" onclick="deptManager.editDept(true);" title="<fmt:message key='SYS_ORG_ADDDEPT' />">+</a>
				<div class="searchWrap">
					<input id="search" data-options="width:'100%',height:35">
				</div>
			</div>
		</div>
		<div class="scrltopn">
			<div class="treecontentpane">
				<label class="easyui-radio"><input name="showNoUsed" type="checkbox" value=""><fmt:message key="SYS_COMMON_SHOWSTOPDEPTS" /></label>
				<ul id="deptTree"></ul>
				<div id="searchDiv" style="display:none;">
					<ul id="searchContent" class="SearchResult-list"></ul>
				</div>
			</div>

		</div>

	</div>
	<div data-options="region:'center',border:false" style="padding:10px;">
		<div class="easyui-layout" data-options="fit:true">
			<div data-options="region:'north',border:false" style="padding-bottom:10px;">
			    <form id="searchUser" method="post" class="search-form">
				<div class="screeningbar searchUser">
					<span class="screeningbar-item">
						<span class="label"><fmt:message key="SYS_COMMON_USERNAME" /></span>
						<div class="valueGroup">
							<input name="userName" class="easyui-textbox " data-options="width:'100%'">
						</div>
					</span>
					<span class="screeningbar-item">
						<span class="label"><fmt:message key="SYS_COMMON_ACCOUNT" /></span>
						<div class="valueGroup">
							<input name="userCode" class="easyui-textbox " data-options="width:'100%'">
						</div>
					</span>
					<!-- 工号: <input name="agentCode" class="easyui-textbox " style="width:110px">-->
					<span class="screeningbar-item">
						<span class="label"><fmt:message key="SYS_COMMON_STATUS" /></span>
						<div class="valueGroup">
							<select name="enabled" class="easyui-combobox" data-options="editable:false,width:'100%'">
								<option value="" selected><fmt:message key="SYS_COMMON_FIELD_ALL" /></option>
								<option value="1" ><fmt:message key="SYS_COMMON_OPEN" /></option>
								<option value="0" ><fmt:message key="SYS_COMMON_STOP" /></option>
								<option value="2" ><fmt:message key="SYS_COMMON_DEPARTURE" /></option>
							</select>
						</div>
					</span>
					<input type="hidden" name="orgId"   value="" />
					<input type="hidden" name="deptId" value="" />
				</div>
				<div class="btn-group">
					<a href="javascript:;" class="operation search-btn" onclick="userManager.searchUser();" title="<fmt:message key='SYS_COMMON_SEARCH' />"></a>
					<a href="javascript:;" class="operation reset-btn" onclick="$('#searchUser').form('reset');" title="<fmt:message key='SYS_COMMON_RESET' />"></a>
				</div>
				</form>
			</div>
			<div data-options="region:'center',border:false">
				<div id="tt">
					<shiro:hasPermission name="SysUser:create">
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="userManager.showUser();"><fmt:message key="SYS_ORG_ADDUSER" /></a>
					</shiro:hasPermission>
					<shiro:hasPermission name="SysUser:assign">
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="userManager.assignUser()"><fmt:message key="SYS_COMMON_DISTRIBUTE" /></a>
					</shiro:hasPermission>
					<shiro:hasPermission name="SysUser:create">
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="userManager.importUser(true)"><fmt:message key="SYS_ORG_IMPORTUSER" /></a>
					</shiro:hasPermission>
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="userManager.exportUser(true)"><fmt:message key="SYS_ORG_EXPORTUSER" /></a>

					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="userManager.importGroup(true)">组别导入</a>
				</div>
				<div class="easyui-panel" id="includeUser" title="<fmt:message key='SYS_COMMON_INCLUDEMEMBER' />" data-options="tools:'#tt',fit:true">
					<table id="userGrid" >
						<thead>
							<tr>
								<th data-options="field:'userName',sortable:true,width:50,align:'left',formatter:deptManager.formatTitle"><fmt:message key="SYS_COMMON_USERNAME" /></th>
								<th data-options="field:'userCode',sortable:true,width:80,align:'left',formatter:deptManager.formatTitle"><fmt:message key="SYS_COMMON_ACCOUNT" /></th>
								<th data-options="field:'deptName',width:100,align:'left',formatter:deptManager.formatTitle"><fmt:message key="SYS_COMMON_BELONGDEPT" /></th>
								<th data-options="field:'rolesLabel',width:100,align:'left',formatter:deptManager.formatTitle"><fmt:message key="SYS_COMMON_BELONGROLE" /></th>
								<th data-options="field:'mobile',width:100,align:'left'"><fmt:message key="SYS_COMMON_PHONE" /></th>
								<th data-options="field:'enabled',width:60,align:'left',formatter:userManager.formatType2"><fmt:message key="SYS_COMMON_STATUS" /></th>
								<th data-options="field:'cz',width:60,align:'left',formatter:userManager.formatType"><fmt:message key="SYS_COMMON_OPERATE" /></th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>
	</div>


<div id="user-menu" userId=""  class="opt-dropdown-noline easyui-menu" data-options="minWidth:100">
	<div class="czItem" onclick="userManager.userEvnet(2,0)"><fmt:message key="SYS_COMMON_STOP" /></div>
	<div class="czItem" onclick="userManager.userEvnet(2,1)"><fmt:message key="SYS_COMMON_OPEN" /></div>
	<div class="czItem" onclick="userManager.userEvnet(2,2)"><fmt:message key="SYS_COMMON_DEPARTURE" /></div>
	<div class="czItem" onclick="userManager.userEvnet(1)"><fmt:message key="SYS_ORG_RESETPASSWORD" /></div>
</div>


<!-- dialog新建人员 -->
<div id="userDiv" class="easyui-dialog iform-horizontal column-2" title="<fmt:message key="SYS_ORG_ADDUSER" />" style="padding:20px 10px" data-options="closed:true,buttons: '#editUser-btns',width:800,height:'95%'">
	<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_ORG_PICTURE" /></div>
			<div class="icontrols">
				<div class="img-preview">
					<span class="img-icon img-show"></span>
					<span class="img-text img-show"><fmt:message key="SYS_ORG_CHOOSEPIC" /></span>
					<span class="img-add img-hide"><img name="pre_photo" src="" /></span>
					<span class="img-mask img-hide"></span>
					<span class="img-loading" style="display:none;"><span class="loadingbg"></span><img src="${ctx}/hollybeacon/personoa/system/images/loading.gif"></span>
					<div class="op-delay">
						<span class="img-close img-hide"><span class="icon-trash" title="<fmt:message key="SYS_COMMON_DELETE" />"></span></span>
						<div class="imgbtn-file"><fmt:message key="SYS_ORG_MODIFYPIC" /><input title="<fmt:message key="SYS_COMMON_UPLOAD" />" class="file-op" name="file" type="file"></div>
					</div>
				</div>
			</div>
		</div>
	<form id="userForm"  class="easyui-form iform-horizontal column-2" method="post" data-options="novalidate:true">
		<input type="hidden" name="id" value="">
		<input type="hidden" name="photo" value="">
		<div class="icontrol-group">
			<div class="icontrol-label"><i class="required">*</i><fmt:message key="SYS_COMMON_ORGNAME" /></div>
			<div class="icontrols">
				<input id="userOrgCombo" type="text" name="orgId" data-options="required:true,width:'100%'"></input>
			</div>
		</div>
				<div class="icontrol-group">
			<div class="icontrol-label"><i class="required">*</i><fmt:message key="SYS_COMMON_BELONGDEPT" /></div>
			<div class="icontrols">
				<input id="userDeptCombo" name="deptId" data-options="required:true,width:'100%'" type="text"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><i class="required">*</i><fmt:message key="SYS_COMMON_NICKNAME" /></div>
			<div class="icontrols">
				<input class="easyui-textbox" name="agentName" type="text" data-options="required:true,validType:['maxLength[30]','specialChar'],width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><i class="required">*</i><fmt:message key="SYS_COMMON_USERNAME" /></div>
			<div class="icontrols">
				<input class="easyui-textbox" name="userName" type="text" data-options="required:true,validType:['maxLength[30]','specialChar'],width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><i class="required">*</i><fmt:message key="SYS_COMMON_ACCOUNT" /></div>
			<div class="icontrols">
				<input class="easyui-textbox" id="account" name="userCode"  type="text" data-options="required:true,validType:['length[3,20]','vaildOrgAccount'],width:'100%'"" missingMessage="<fmt:message key="SYS_ORG_ACCOUNT_NOEMPTY" />"></input>
			</div>
		</div>
		<div class="icontrol-group" id="pwdDiv" style="display:none">
			<div class="icontrol-label"><i class="required">*</i><fmt:message key="SYS_ORG_INITPASSWORD" /></div>
			<div class="icontrols">
				<input class="easyui-textbox required" id="password" name="password" disabled="true" type="text" data-options="editable:false,width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><i class="required">*</i><fmt:message key="SYS_ORG_PASSWORD_EXPIRY" /></div>
			<div class="icontrols">
				<input class="easyui-datebox"  id='pwdValidDay' name="pwdValidDay" type="text" data-options="required:true,editable:false,width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_COMMON_BELONGROLE" /></div>
			<div class="icontrols">
				<select  id="userRoleCombo"   type="text" name="roleId" data-options="width:'100%'" multiple></select >
				<input id="roleIds" name="roleIds" type="hidden"  value="">
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_ORG_ENTRYDATE" /></div>
			<div class="icontrols">
				<input class="easyui-datebox" id='hireDate' name="hireDate" type="text" data-options="editable:false,validType:['dataFormat','specialChar'],width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_COMMON_CARDTYPE" /></div>
			<div class="icontrols">
				<input id="cervificateType" class="easyui-combobox" name='cervificateType' data-options="editable:false,width:'100%'"/>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_ORG_BIRTHDAY" /></div>
			<div class="icontrols">
				<input class="easyui-datebox" name="birthday" type="text" data-options="editable:false,validType:'dataFormat',width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_ORG_CARDNO" /></div>
			<div class="icontrols">
				<input class="easyui-textbox" id="cervificateNo" name='cervificateNo' type="text" data-options="validType:['maxLength[20]','englishAndNum'],width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><i class="required">*</i><fmt:message key="SYS_ORG_VDN" /></div>
			<div class="icontrols">
				<select id="vdnID" class="easyui-combobox" name='vdnID' data-options="required:true,editable:false,width:'100%',validType:'mustSelect'"></select>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><i class="required">*</i><fmt:message key="SYS_COMMON_AREACODE" /></div>
			<div class="icontrols">
				<select id="areaCode" class="easyui-combobox" name='areaCode' data-options="required:true,editable:false,width:'100%',validType:'mustSelect'"></select>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_ORG_GENDER" /></div>
			<div class="icontrols">
				<label class="easyui-radio inline"><input type="radio" name="sex" value="0"><fmt:message key="SYS_ORG_MALE" /> </label>&nbsp;
				<label class="easyui-radio inline"><input type="radio" name="sex" value="1"><fmt:message key="SYS_ORG_FEMALE" /> </label>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_ORG_EDUCATION" /></div>
			<div class="icontrols">
				<input id="education" class="easyui-combobox" name='education' data-options="editable:false,width:'100%'"/>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_ORG_TEL" /></div>
			<div class="icontrols">
				<input class="easyui-textbox" name="telephone" type="text" data-options="validType:'mobileAndTel',width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_COMMON_PHONE" /></div>
			<div class="icontrols">
				<input class="easyui-textbox" name="mobile"  type="text" data-options="validType:'mobileAndTel',width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_ORG_EMAIL" /></div>
			<div class="icontrols">
				<input class="easyui-textbox" name="email" type="text" data-options="validType:['email','maxLength[30]'],width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_ORG_ADDRESS" /></div>
			<div class="icontrols">
				<input class="easyui-textbox" name="address" type="text" data-options="validType:['maxLength[50]','specialChar'],width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_ORG_POSTCARD" /></div>
			<div class="icontrols">
				<input class="easyui-textbox" name="postCode" type="text" data-options="validType:['ZIP','specialChar'],width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_ORG_CONTRACTPERSON" /></div>
			<div class="icontrols">
				<input class="easyui-textbox" name="contactName"  type="text" data-options="validType:['maxLength[20]','specialChar'],width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_ORG_CONTRACTTEL" /></div>
			<div class="icontrols">
				<input class="easyui-textbox" name="contactPhone" type="text" data-options="validType:'mobileAndTel',width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group icontrol-full">
			<div class="icontrol-label"><fmt:message key="SYS_COMMON_REMARK" /></div>
			<div class="icontrols">
				<input class="easyui-textbox" name="remark"  type="text" data-options="multiline:true,validType:['maxLength[100]','simpleText'],width:'100%',height:72"></input>
			</div>
		</div>
	</form>
</div>
<div id="editUser-btns">
	<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red" onclick="userManager.saveUser()"><fmt:message key="SYS_COMMON_SUBMIT" /></a>
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#userDiv').dialog('close')"><fmt:message key="SYS_COMMON_CANCEL" /></a>
</div>



<!-- dialog设置角色成员 -->
<div id="assignUser" class="easyui-dialog" title="<fmt:message key="SYS_COMMON_ALLOTMEMEBER" /><span class='tagging'><fmt:message key="SYS_COMMON_DEPT" /></span>" style="padding:10px"
		data-options="closed:true,buttons: '#assignUser-btns',width:800,height:'95%'">
		<input type="hidden" id="assign-deptId" value="" />
		<input type="hidden" id="assign-orgId" value="" />
	<div class="easyui-layout" data-options="fit:true">
		<div data-options="region:'west',border:false" style="width:500px;">
			<div class="easyui-panel" title="<fmt:message key="SYS_COMMON_ENABLEDMEMBER" />" data-options="width:480,height:'100%'">
				<div class="easyui-layout" data-options="fit:true">
					<div data-options="region:'west',border:false,width:'50%'">
						<div class="treecontentpane">
							<label class="easyui-radio"><input name="chkShowAll" type="checkbox" value=""> <fmt:message key="SYS_COMMON_SHOWSTOPDEPTS" /></label>
							<ul id="ctrl-tree"></ul>
						</div>
					</div>
					<div data-options="region:'center',border:false" style="border-left:1px solid #ddd;">
						<div class="easyui-layout" data-options="fit:true">
							<div data-options="region:'north',border:false,height:50" style="padding:8px 5px;">
								<div><input id="searchName" data-options="width:'100%'" /></div>
							</div>
							<div data-options="region:'center',border:false">
								<div class="select-panel out">
									<ul id="srcList" class="select-list">
										<li><label class='select-item selsrcAll'>
											<input type='checkbox' class='select-check'><span><fmt:message key="SYS_COMMON_ALLMEMBER" /></span>
											</label>
										</li>
									</ul>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>

		<div data-options="region:'center',border:false">
			<div class="easyui-panel" title="<fmt:message key="SYS_COMMON_SELECTMEMBER" />" data-options="fit:true">
				<div class="select-panel in">
					<ul id="targetList" class="select-list">

					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="assignUser-btns">
	<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red" onclick="assign.saveAssign();"><fmt:message key="SYS_COMMON_SUBMIT" /></a>
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#assignUser').dialog('close')"><fmt:message key="SYS_COMMON_CANCEL" /></a>
</div>


<!-- dialog新建部门 -->
<div id="deptDiv" class="easyui-dialog" title="<fmt:message key="SYS_ORG_ADDDEPT" />" style="width:600px;height:400px;padding:20px 10px"
		data-options="
			closed:true,
			buttons: '#deptDiv-btns'
		">
	<form id="deptForm" class="easyui-form department-info-form iform-horizontal" method="post" data-options="novalidate:true">
		<div class="icontrol-group">
			<input type="hidden" id="id" name="id" value=""/>
			<input type="hidden" id="parentId" name="parentId" value=""/>
			<input type="hidden" name="isSys" value=""/>
			<input type="hidden" name="isVdn" value=""/>
			<div class="icontrol-label"><i class="required">*</i><fmt:message key="SYS_COMMON_ORGNAME" /></div>
			<div class="icontrols">
				<input id="orgCombo" name="rootId" type="text" data-options="width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><i class="required"></i><fmt:message key="SYS_COMMON_PARENTDEPT" /></div>
			<div class="icontrols">
				<input id="deptCombo" name="deptParentId" type="text" data-options="width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><i class="required">*</i><fmt:message key="SYS_COMMON_DEPTNAME" /></div>
			<div class="icontrols">
				<input name="orgName" class="easyui-textbox" type="text" data-options="required:true,validType:['maxLength[30]','specialChar'],width:'100%'"></input>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_ORG_VDN" /></div>
			<div class="icontrols">
				<select id="vdnId" class="easyui-combobox" name='vdnId' data-options="editable:false,width:'100%'"></select>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><i class="required">*</i><fmt:message key="SYS_COMMON_USESTATUS" /></div>
			<div class="icontrols icontrols-text"><input type="hidden"  name="enabled" value=""  />
				<span id="deptEnabled"></span>
			</div>
		</div>
		<div class="icontrol-group">
			<div class="icontrol-label"><fmt:message key="SYS_COMMON_REMARK" /></div>
			<div class="icontrols">
				<input name="remark" class="easyui-textbox" type="text" data-options="multiline:true,validType:['maxLength[100]','simpleText'],width:'100%',height:72"></input>
			</div>
		</div>
	</form>

</div>
<div id="deptDiv-btns">
	<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red" onclick="deptManager.saveDept();"><fmt:message key="SYS_COMMON_SUBMIT" /></a>
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#deptDiv').dialog('close')"><fmt:message key="SYS_COMMON_CANCEL" /></a>
</div>

<!-- dialog导入人员 -->
<div id="dlg-import" class="easyui-dialog" title="<fmt:message key="SYS_ORG_IMPORTUSER" />"
		data-options="
			closed:true,
			buttons: '#dlg-import-buttons',
			cache:false,
			width:650,
			height:'95%'
		">
	<iframe id="iframe-import" name="iframe-import" src="" frameborder="0" style="width:100%;height:100%;display:block;"></iframe>
</div>
<div id="dlg-import-buttons">
	<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red" onclick="userManager.importUser(false)"><fmt:message key="SYS_COMMON_IMPORT" /></a>
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#dlg-import').dialog('close')"><fmt:message key="SYS_COMMON_CANCEL" /></a>
</div>
<div id="dlg-export" class="easyui-dialog" title="<fmt:message key="SYS_ORG_EXPORTUSER" />"
		data-options="
			closed:true,
			buttons: '#dlg-export-buttons',
			width:650,
			height:'95%'
		">
	<iframe id="iframe-export" name="iframe-export" src="" frameborder="0" style="width:100%;height:100%;display:block;"></iframe>
</div>
<div id="dlg-export-buttons">
	<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red" onclick="userManager.exportUser(false)"><fmt:message key="SYS_COMMON_SUBMIT" /></a>
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#dlg-export').dialog('close')"><fmt:message key="SYS_COMMON_CANCEL" /></a>
</div>
</body>
<script>
function closeImportWin(){
	$('#dlg-export').dialog('close');
	$('#dlg-import').dialog('close');
	$("#userGrid").datagrid("load");
}

</script>
</html>
</fmt:bundle>
