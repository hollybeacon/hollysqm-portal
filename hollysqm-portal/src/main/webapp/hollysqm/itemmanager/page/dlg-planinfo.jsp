<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%
String itemId = request.getParameter("itemId");
%>

<form id="formjibenxinxi" class="iform-horizontal">
	<div class="icontrol-group">
		<div class="icontrol-label">标签名称</div>
		<div class="icontrols icontrols-text">
			<input name="itemName" class="easyui-textbox" data-options="width:'100%',readonly:true" />
		</div>
	</div>
	<div class="icontrol-group">
		<div class="icontrol-label">所属分类</div>
		<div class="icontrols icontrols-text">
			<select name="itemType" id="itemType" data-options="editable:false,width:'100%',readonly:true"></select>
		</div>
	</div>
	<div class="icontrol-group">
		<div class="icontrol-label">匹配规则</div>
		<div class="icontrols">
			<input name="content" class="easyui-textbox" data-options="multiline:true,width:'100%',height:80,readonly:true" />
		</div>
	</div>
	<div class="icontrol-group">
		<div class="icontrol-label">启用状态</div>
		<div class="icontrols icontrols-text">
			<select name="status" id="status" data-options="editable:false,width:'100%',readonly:true"></select>
		</div>
	</div>
</form>
<script>
$(function(){
	$("#itemType").combobox("init",{'codeType':'ITEM_TYPE','hasEmptyOption':false});
	$("#status").combobox("init",{'codeType':'STATUS','hasEmptyOption':false});
	holly.get(holly.getPath()+"/rest/itemmanager/modifyItemBefor",{"itemId":"<%=itemId%>"},function(e){
		if(e.success){//先确认检查通过！
			$('#formjibenxinxi').form('load', e.content);
			// with(e.content){
			// 	$("#formjibenxinxi #itemName").text(itemName);
			// 	$("#formjibenxinxi #showcontent").textbox().textbox('setValue',content);
			// 	$("#formjibenxinxi #itemType").text(itemTypeMap[itemType]);
			// 	$("#formjibenxinxi #status").text(itemStatusMap[status]);
			// }
		}else{
			holly.showError("错误，原因:"+e.errorMessage);
		}
	},true);
	
});
</script>