<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html>
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>标签管理</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/switch.css">
<link rel="stylesheet" href="${ctx}/hollysqm/itemmanager/css/itemdlg.css">
<script type="text/javascript" src="../js/itemmanager.js" ></script>
<script type="text/javascript">
// 获取必要的数据字典MAP		
//var itemTypeMap=$("#itemManagerdatagrid").datagrid("getdictnames",{'param':{'codeType':'ITEM_TYPE'}}); 
//var itemNameMap=$("#itemManagerdatagrid").datagrid("getdictnames",{'param':{'codeType':'ITEM_NAME'}});
var stateMap=$("#itemManagerdatagrid").datagrid("getdictnames",{'param':{'codeType':'YES_NO'}});
$(function() {
	$("#formitemdlgType").combobox("init_extend",{'codeType':'ITEM_TYPE'});
	itemManager.init();
	$("#searchitemManager").keyup(function(event){
		if(event.keyCode==13){
			itemManager.search();
		}
	});
});
</script> 
</head>
<body class="easyui-layout">
	<div data-options="region:'center',border:false" style="padding:10px;">
		<div class="easyui-layout" data-options="fit:true">
			<!-- 搜索区域 -->
			<div data-options="region:'north',border:false" style="padding-bottom:10px;">
				<form id="searchitemManager" method="post" class="search-form">
					<div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">所属分类</span>
							<div class="valueGroup">
								<select name="itemType" id="formitemType" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">标签名称</span>
							<div class="valueGroup">
								<imput id="itemName" name="itemName" class="easyui-textbox"  data-options="width:'100%'"/>
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">启用状态</span>
							<div class="valueGroup">
								<select name="status" id="formStatus" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span> 
					</div>
					<div class="btn-group">
						<a href="javascript:;" class="operation search-btn" onclick="itemManager.search()" title="搜索"></a>
						<a href="javascript:;" class="operation reset-btn" onclick="$('#searchitemManager').form('reset');" title="重置"></a>
					</div>
				</form>
			</div>
			<!-- 搜索区域 end -->	
			<div data-options="region:'center',border:false">
				<div id="tt">
					<!-- 
					<a href="javascript:void(0)" class="easyui-linkbutton"
						onclick="$('#ff').form('reset');$('#dlg1').dialog('open');">新建</a> 
						 -->
					<a href="javascript:void(0)" class="easyui-linkbutton"
						onclick="itemManager.newItem()">新建</a> 
				</div>
				<div class="easyui-panel" title="标签管理" data-options="tools:'#tt',fit:true">
					<table id="itemManagerdatagrid" ></table>
				</div>
			</div>
		</div>
	</div>
	<div id="dlg-planinfo" class="easyui-dialog" style="padding:10px;"
		data-options="
			title:'查看标签',
			closed:true,
			width:600,
			height:300">
	</div>
	<div id="dlg1" class="easyui-dialog" title="新建标签" style="width: 600px; padding: 10px" data-options="iconCls:'icon-exclamation-sign',closed:true,buttons:'#dlg-buttons1'">
		<form id="ff" method="post" data-options="novalidate:true">	
			<input type="hidden" name="textItemId"/>
			<input type="hidden" name="isValid" value="1"/>
			<input type="hidden" name="roleType" value="0"/>
			<div class="dlg_bar clearfix">
				<span class="nam"><span class="dlg_red">*</span>所属分类:</span>
				<div class="dlg_input">
					<select name="itemType" id="formitemdlgType" data-options="editable:false,width:'100%',required:true"></select>
				</div>
			</div> 
			<div class="dlg_bar clearfix">
				<span class="nam"><span class="dlg_red">*</span>标签名称:</span>
				<div class="dlg_input">
					<input class="easyui-textbox" name="itemName" data-options="width:'100%',required:true,novalidate:true,validType:'length[1,25]'">
				</div>
			</div>
			<div class="dlg_bar clearfix">
				<span class="nam"><span class="dlg_red">*</span>匹配规则:</span>
				<div class="dlg_input">
					<input name="content" class="easyui-textbox " data-options="multiline:true,width:'100%',height:'200px',required:true,novalidate:true,validType:'length[1,1000]'">
					<div>
						注：(控制逻辑AND【必需】,OR【或】,NOT【排除】三种关系，词与词之间用空格隔开...)
					</div>
				</div>
			</div>
			<div class="dlg_bar clearfix">
				<span class="nam"><span class="dlg_red">*</span>启用状态:</span>
				<div class="dlg_input">
					<div class="switch" id="switch"></div>
					<input type="hidden" name="status" value="0">
			</div>
			<script>
				var s = document.querySelector('#switch');
				var i = document.querySelector('input[name="status"]');
				s.onclick = function() {
					this.className = this.className == "switch" ? "switch switch-on" : "switch";
					i.value = i.value == '0' ? '1' : '0';
				};
			</script>
		</form>
		<div id="dlg-buttons1">
			<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red"onclick="itemManager.saveOrUpdate()">保存</a> 
			<a href="javascript:void(0)" class="easyui-linkbutton"
			onclick="$('#dlg1').dialog('close')">取消</a>
		</div>
	</div> 
</body>
</html>