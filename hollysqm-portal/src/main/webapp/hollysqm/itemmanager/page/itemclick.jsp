<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html> 
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>标签管理</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/switch.css">
<link rel="stylesheet" href="${ctx}/hollysqm/itemmanager/css/itemdlg.css">
<script type="text/javascript" src="../js/itemclick.js" ></script>
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type="text/javascript">
// 获取必要的数据字典MAP		
//var itemTypeMap=$("#itemManagerdatagrid").datagrid("getdictnames",{'param':{'codeType':'ITEM_TYPE'}}); 
//var itemNameMap=$("#itemManagerdatagrid").datagrid("getdictnames",{'param':{'codeType':'ITEM_NAME'}});
var stateMap=$("#itemClickdatagrid").datagrid("getdictnames",{'param':{'codeType':'YES_NO'}});
$(function() {
	$("#formitemdlgType").combobox("init_extend",{'codeType':'ITEM_TYPE'});
	itemClick.init();
	$("#searchitemClick").keyup(function(event){
		if(event.keyCode==13){
			itemClick.search();
		}
	});
	var textItemId = $('#textItemId').val();
	if (textItemId!=null && textItemId.length>0 ){
		itemClick.loadItemManageData(textItemId);
	}
});
</script> 
</head>
<body class="easyui-layout">
	<div data-options="region:'center',border:false" style="padding:10px;">
		<div class="easyui-layout" data-options="fit:true">
			<!-- 搜索区域 -->
			<div data-options="region:'north',border:false" style="padding-bottom:10px;">
				<form id="searchitemClick" method="post" class="search-form">
				<input type="hidden" name="textItemId" id="textItemId" value="${param.textItemId}" />
				<input type="hidden" id="frameId" value="${param.frameId}" />
				<input type="hidden" name="isValid" value="1"/>
				<input type="hidden" name="roleType" value="0"/>
					<div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">所属分类</span>
							<div class="valueGroup">
								<select name="itemType" id="formItemType" data-options="editable:false,width:'100%',required:true"></select>	
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">标签名称</span>
							<div class="valueGroup">
								<imput name="itemName" id="formItemName" class="easyui-textbox"  data-options="width:'100%',validType:'length[1,25]',required:true"/>
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">启用状态</span>
							<div class="valueGroup">
								<select name="status" id="formStatus" data-options="editable:false,width:'100%',required:true"></select>	
							</div>
						</span>
						<span class="screeningbar-item" style="width: 100%">
							<span class="label">匹配规则</span>
							<div class="valueGroup">
								<textarea class="easyui-textbox" name="content" id="formContent" data-options="multiline:true,validType:'length[1,1000]',required:true" style="height: 50px;width: 90%;"></textarea>	
								<div style="color:#999;font-size:12px;">
									注：(控制逻辑AND【并且】,OR【或】,NOT【排除】三种关系，*【通配符】可以匹配包含词，如：*套餐*，表示任意词中包含“套餐”即符合条件，词与词之间用空格隔开)

								</div>
							</div>
						</span> 
					</div>
					<div class="btn-group">
						<a href="javascript:;" class="grid-optcon icon-search" style="width: 18px;height: 18px;display: block;float: left;margin-left: 15px;" onclick="itemClick.search()" title="搜索"></a>
						<a href="javascript:;" class="grid-optcon icon-save" style="width: 18px;height: 18px;display: block;float: left;margin-left: 15px;" onclick="itemClick.saveOrUpdate()" title="保存"></a>
						<a href="javascript:;" class="grid-optcon icon-refresh" style="width: 18px;height: 18px;display: block;float: left;margin-left: 15px;" onclick="$('#searchitemClick').form('reset');" title="重置"></a>
					</div>
				</form>
			</div>
			<!-- 搜索区域 end -->	
			<div data-options="region:'center',border:false">
				<div class="easyui-panel" title="预览列表" data-options="fit:true">
					<table id="itemClickdatagrid" ></table>
				</div>
			</div>
		</div>
	</div>
	<div id="dlg-dialogueinfo" class="easyui-dialog" title="对话详情" style="width: 600px; padding: 20px;height: 450px;" data-options="closed:true">
		<div id="chatqa"></div>
	</div> 
</body>
</html>