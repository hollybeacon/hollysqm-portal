var dlgPlaninfo = {
	content:"", //计划详情数据
	
	/**
	 * 初始化加载数据
	 */
	init:function(){
		$('#cxsearch').searchbox({
			searcher:function(value,name){
				value = $.trim(value);
				dlgPlaninfo.workerSearch(value,dlgPlaninfo.content.agents,'#cxgroplistpane');
			},
			height:30,
			width:228,
			prompt:'按姓名搜索'
		});
		$("#zjysearch").searchbox({
			searcher:function(value,name){
				value = $.trim(value);
				dlgPlaninfo.workerSearch(value,dlgPlaninfo.content.checkers,'#zjygroplistpane');
			},
			height:30,
			width:228,
			prompt:'按姓名搜索'
		});

		holly.post(holly.getPath()+"/hollysqm/itemmanager/json/showPlanDetail.json",{"planId":planId},function(e){
			if(e.success){
				// 修改数据结构
				dlgPlaninfo.content = e.content;
				var agents_departmentArr = new Array();
				var agentsArr = new Array();
				var agents_map = new Array();
				var cxgropHtml = "";//坐席列表html
				for(var i in e.content.agents){
					//检索department在该数组中是否存在，不存在才会允许添加到departmentArr
					if(agents_departmentArr.indexOf(e.content.agents[i].department)==-1){
						agents_departmentArr.push(e.content.agents[i].department);
					}
				}
				for(var i in agents_departmentArr){
					// 从e.content.agents中过滤出含有departmentArr的数据
					var dept = e.content.agents.filter(function(n){
						return n.department==agents_departmentArr[i];
					});
					agentsArr.push(dept);
				}
				agents_map = $.map(agentsArr,function(item,index){
					return{
						department:agentsArr[index][0].department,
						children:item
					};
				});

				cxgropHtml = dlgPlaninfo.workerHtmlList(agents_map);
				$('#cxgroplistpane').html(cxgropHtml);


				var checkers_departmentArr = new Array();
				var checkersArr = new Array();
				var checkers_map = new Array();
				var zjygropHtml = "";//坐席列表html
				for(var i in e.content.checkers){
					//检索department在该数组中是否存在，不存在才会允许添加到departmentArr
					if(checkers_departmentArr.indexOf(e.content.checkers[i].department)==-1){
						checkers_departmentArr.push(e.content.checkers[i].department);
					}
				}
				for(var i in checkers_departmentArr){
					// 从e.content.agents中过滤出含有departmentArr的数据
					var dept = e.content.checkers.filter(function(n){
						return n.department==checkers_departmentArr[i];
					});
					checkersArr.push(dept);
				}
				checkers_map = $.map(checkersArr,function(item,index){
					return{
						department:checkersArr[index][0].department,
						children:item
					};
				});

				zjygropHtml = dlgPlaninfo.workerHtmlList(checkers_map);
				$('#zjygroplistpane').html(zjygropHtml);

				var jibenxinxiHtml = '';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">标签名称</div>';
				jibenxinxiHtml += '<div class="icontrols" style="padding:6px 10px">'+e.content.itemName+'</div>';
				jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">匹配规则</div>';
				jibenxinxiHtml += '<div class="icontrols" style="padding:6px 10px">'+e.content.content+'</div>';
				jibenxinxiHtml += '</div>';
				$('#jibenxinxi').html(jibenxinxiHtml);
			}else{
				holly.showError(e.errorMessage);
			}
		});
	},
	/**
	 * 返回坐席列表html
	 * @param map 数据 { array }
	**/
	workerHtmlList:function(map){
		var html = "";
		$.each(map, function(index, item){
			html += '<div class="groupitem">';
			html += '<div class="title">'+item.department+'</div>';
			html += '<ul class="list">';
			$.each(item.children, function(index1, item1){
				html += '<li title="'+item1.username+'('+item1.userCode+')">'+item1.username+'('+item1.userCode+')</li>';
			});
			html += '</ul>';
			html += '</div>';
		});
		return html;
	},

	/**
	 * 搜索坐席
	 * @param keyword 关键词 { string }
	 * @param content 数据 { array }
	 * @param id 要填充的div id名 { string }
	**/
	workerSearch:function(keyword,content,id){
		var departmentArr = new Array();
		var searchArr = new Array();
		var searchMap = new Array();
		var html = "";//坐席列表html
		for(var i in content){
			if(departmentArr.indexOf(content[i].department)==-1){
				if(content[i].username.indexOf(keyword)>=0 || content[i].userCode.indexOf(keyword)>=0){
					departmentArr.push(content[i].department);
				}
			}
		}
		if(departmentArr.length > 0){
			for(var i in departmentArr){
				var dept = content.filter(function(n){
					if(n.username.indexOf(keyword)>=0 || n.userCode.indexOf(keyword)>=0){
						return n.department==departmentArr[i];
					}
				});
				searchArr.push(dept);
			}
			searchMap = $.map(searchArr,function(item,index){
				return{
					department:searchArr[index][0].department,
					children:item
				};
			});
			html = dlgPlaninfo.workerHtmlList(searchMap);
			$(id).html(html);
		}else{
			$(id).html('<div class="nodata">暂无搜索结果</div>');
		}
	}
};
 