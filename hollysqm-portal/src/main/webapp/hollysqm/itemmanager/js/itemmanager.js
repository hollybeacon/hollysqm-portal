var statusMap = ['停用', '启用'];
var itemManager = {
    /**
     * 初始化加载数据
     */
    init: function () {
        //$("#formitemType").combobox("init_extend",{'codeType':'ITEM_TYPE'});
        $("#formitemType").combobox("init", {'codeType': 'ITEM_TYPE'});
        $("#formStatus").combobox("init_extend", {'codeType': 'STATUS'});
        $("#itemManagerdatagrid").datagrid({
            fit: true,
            url: holly.getPath() + '/rest/itemmanager/queryItem',
            method: 'get',
            singleSelect: true,
            border: false,
            fitColumns: true,
            pagination: true,
            loadFilter: function (data) {
                return (data.content) ? data.content : data;
            },
            columns: [[
                {field: 'itemTypeName', title: '所属分类', width: 60, formatter: itemManager.formatItemTypeName},
                {field: 'itemName', title: '标签名称', width: 100, formatter: itemManager.formatLink},
                {field: 'statusName', title: '启用状态', width: 40},
                {field: 'modifyName', title: '最后修改人', width: 40},
                {field: 'modifyTime', title: '最后修改时间', width: 60},
                {field: 'option', title: '操作', formatter: itemManager.formatOption}
            ]],
            loadMsg: '数据加载中请稍后……',
            onLoadSuccess: function (data) {
                if (data.rows.length == 0) {
                    var body = $(this).data().datagrid.dc.body2;
                    body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
                }
            }
        });
    },//设置质检标签类型
    formatItemTypeName: function (val, row, index) {
//		if (row.itemTypeName == null || row.itemTypeName == ''){
//			return "请选择";
//		}else {
//			return row.itemTypeName;
//		}
        return row.itemTypeName;
    },
    /**
     * 搜索
     */
    search: function () {
        $('#itemName').textbox({value: strReplace($('#itemName').val())});
        var param = holly.form2json($("#searchitemManager"));
        $("#itemManagerdatagrid").datagrid("search", {'param': param});
    },
    // 所属分类
    formatitemTyp: function (val, row, index) {
        var res = itemTypeMap[val]; //获取对应的数据
        return "<span title='" + res + "'>" + res + "</span>";
    },
    // 启用状态
    formatstate: function (val, row, index) {
        var res = stateMap[val]; //获取对应的数据
        return res;
    },
    // 最后修改人
    formatModifier: function (val, row, index) {
        return val ? "<span title='" + row.modifierName + "(" + val + ")" + "'>" + row.modifierName + "(" + val + ")" + "</span>" : "";
    },
    // 操作
    formatOption: function (val, row, index) {
        var content = '';
        content += "<span class='grid-opt-wrap'>";
        content += "<div class='switch " + (row.status == "1" ? 'switch-on' : '') + "' id='switch_status" + index + "' onclick=\"itemManager.switchStatus(this,'" + row.status + "','" + row.textItemId + "','" + index + "')\"></div>";
        content += "<a href='javascript:void(0)' onclick=\"itemManager.modifyItem('" + row.textItemId + "')\" class='grid-optcon icon-edit' title='编辑'></a>";
        content += "</span>";
        return content;
    },
    // 标签名称
    formatLink: function (val, row, index) {
        return val ? "<a href='javascript:;' title='" + val + "' onclick=\"itemManager.planinfo('" + row.textItemId + "')\">" + val + "</a>" : "";
    },
    switchStatus: function (obj, status, textItemId, index) {
        status = '';
        //var enable = status=="1"?stateMap["0"]:stateMap["1"];
        var switchName = $("#switch_status" + index).attr("class");
        if (switchName == 'switch switch-on') {
            status = '0';
        } else {
            status = '1';
        }
        var enable = status == '0' ? statusMap[0] : statusMap[1];

        var json = {
            title: enable,
            content: "确定修改该标签状态为" + enable + "吗？",
            message: "该标签已成功" + enable
        };
        $.messager.confirm(json.title, json.content, function (r) {
            if (r) {
                holly.post(holly.getPath() + "/rest/itemmanager/enableItem", {
                    "textItemId": textItemId,
                    "status": status
                }, function (e) {
                    if (e.success) {
                        /*$("#itemManagerdatagrid").datagrid("getData").rows[index].statusName = enable;
                         $("#itemManagerdatagrid").datagrid('refreshRow',index);
                         if (switchName == 'switch switch-on'){
                         $("#switch_status"+index).attr("class","switch ");
                         }else {
                         $("#switch_status"+index).attr("class","switch switch-on");
                         }*/
                        //$("#switch_status"+index).attr("class","switch "+(status =="0"? "switch-on":""));
                        $("#itemManagerdatagrid").datagrid("reload");
                        holly.showSuccess(json.message);
                    } else {
                        holly.showError(e.errorMessage);
                    }
                });
            }
        });
    },
    //查看质检标签
    planinfo: function (id) {
        var url = holly.getPath() + "/hollysqm/itemmanager/page/dlg-planinfo.jsp";
        url += "?itemId=" + id;
        $('#dlg-planinfo').dialog('open').dialog('refresh', url);
    },
    saveOrUpdate: function () {
        var vaild = $("#ff").form('enableValidation').form('validate');
        if (vaild) {
            $.messager.confirm('提示', '确定提交吗？', function (r) {
                if (r) {
                    holly.post(holly.getPath() + "/rest/itemmanager/itemManager", holly.form2json($("#ff")), function (e) {
                        if (e.success) {
                            holly.showSuccess("操作成功!");
                            $('#dlg1').dialog('close');
                            itemManager.search();
                        } else {
                            holly.showError(e.errorMessage);
                        }
                    });
                }
            });
        }
    },
    /**
     * 【注：该方法已取消使用】
     * @param id
     */
    loadItemManageData: function (id) {
        $("#ff").form("reset");
        holly.get(holly.getPath() + "/rest/itemmanager/modifyItemBefor", {"itemId": id}, function (e) {
            if (e.success) {//先确认检查通过！
                $('#ff').form('load', e.content);
                itemManager.setSwitch(e.content.status);
            } else {
                holly.showError("错误，原因:" + e.errorMessage);
            }
        }, true);
        $('#dlg1').dialog('open');
    },
    //修改时设置滑块状态
    setSwitch: function (status) {
        var s = document.querySelector('#switch');
        var i = document.querySelector('input[name="status"]');
        s.className = status == '1' ? "switch switch-on" : "switch";
        i.value = status == '1' ? '1' : '0';
    },
    newItem: function () {
        var frameId = window.frameElement && window.frameElement.id || '';
        // var url = holly.getPath()+'/hollysqm/itemmanager/page/itemclick.jsp?frameId='+frameId;
        // parent.main.addTab('iframescore_newItem',url,'新建标签');
        var url = holly.getPath() + '/hollysqm/itemmanager/page/itemclick.jsp?frameId=' + frameId;
        window.location = url;
    },
    modifyItem: function (textItemId) {
        var frameId = window.frameElement && window.frameElement.id || '';
        // var url = holly.getPath() + '/hollysqm/itemmanager/page/itemclick.jsp?textItemId=' + textItemId + "&frameId=" + frameId;
        // parent.main.addTab('iframescore_newItem', url, '修改标签');
        var url = holly.getPath()+ '/hollysqm/itemmanager/page/itemclick.jsp?textItemId=' + textItemId + "&frameId=" + frameId;
        window.location = url;
    }

};
/**
 *    过滤特殊字符
 *    str：要进行过滤的字符串
 */
function strReplace(str) {
    var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
    var rs = "";
    for (var i = 0; i < str.length; i++) {
        rs = rs + str.substr(i, 1).replace(pattern, '');
    }
    return rs;
}
 