var itemClick = {
    /**
     * 初始化加载数据
     */
    init: function () {
        $("#formItemType").combobox("init_extend", {'codeType': 'ITEM_TYPE', 'hasAllOption': false});
        $("#formStatus").combobox("init_extend", {'codeType': 'STATUS', 'hasAllOption': false});
        $("#itemClickdatagrid").datagrid({
            fit: true,
            url: holly.getPath() + '/rest/commonRest/itemPreview',
            method: 'post',
            singleSelect: true,
            border: false,
            fitColumns: true,
            pagination: true,
            loadFilter: function (data) {
                return (data.content) ? data.content : data;
            },
            columns: [[
                {field: 'length', title: '通话时长', width: 60},
                {field: 'agentCode', title: '坐席', width: 80, formatter: itemClick.formatAgentCode},
                {field: 'acceptTime', title: '来电时间', width: 80, formatter: itemClick.formatAcceptime},
                {field: 'option', title: '操作', width: 60, formatter: itemClick.formatOption}
            ]],
            loadMsg: '数据加载中请稍后……',
            onLoadSuccess: function (data) {
                if (data.rows.length == 0) {
                    var body = $(this).data().datagrid.dc.body2;
                    body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
                }
            }
        });
    },
    /**
     * 加载修改数据
     * @param id
     */
    loadItemManageData: function (id) {
        holly.get(holly.getPath() + "/rest/itemmanager/modifyItemBefor", {"itemId": id}, function (e) {
            if (e.success) {//先确认检查通过！
                //var content  = e.content;
                $('#formItemType').combobox({value: e.content.itemType});
                $('#formItemName').textbox({value: e.content.itemName});
                $('#formStatus').combobox({value: e.content.status});
                $('#formContent').textbox({value: e.content.content});

                //itemManager.setSwitch(e.content.status);
            } else {
                holly.showError("错误，原因:" + e.errorMessage);
            }
        }, true);
    },
    /**
     * 搜索
     */
    search: function () {
        $('#formItemName').textbox({value: strReplace($('#formItemName').val())});
        var param = holly.form2json($("#searchitemClick"));
        $("#itemClickdatagrid").datagrid("search", {'param': param});
    },
    /**
     * 操作
     */
    formatOption: function (val, row, index) {
        var content = '';
        content += "<span class='grid-opt-wrap'>";
        content += "<a id=\"" + row.contactId + "\" href='javascript:void(0)'  onclick=\"itemClick.showText(this)\" data-txtContent=\"'" + row.contactId + "'\" class='grid-optcon icon-eye-open' title='预览'></a>";
        content += "</span>";
        return content;
    },
    /**
     * 高亮显示文本
     * @param el
     */
    showText: function (el) {
        $('#chatqa').html('');
        $('#dlg-dialogueinfo').dialog('open');
        var id = el.id;
        var itemType = $('#formItemType').combobox("getValue");
        var itemContent = $('#formContent').val();
        holly.post(holly.getPath() + "/rest/commonRest/highlightPreview", {
            id: id,
            itemType: itemType,
            content: itemContent
        }, function (e) {
            if (e.success) {
                var content = e.content;
                var chatHtml = '';
                var arrFlag = [];
                var arrText = [];
                arrFlag = content.match(/n\d#/ig);
                arrText = content.split(/n\d#/ig);

                for (var i = 0; i < arrFlag.length; i++) {
                    if (arrFlag[i] == 'n0#') {
                        chatHtml += '<div class="text worker"><span style="text-align:right;">' + (i + 1) + '&nbsp;&nbsp;</span>坐席：' + (arrText[i + 1]) + '</div>';
                    } else if (arrFlag[i] == 'n1#') {
                        chatHtml += '<div class="text visiter"><span style="text-align:right;">' + (i + 1) + '&nbsp;&nbsp;</span>客户：' + (arrText[i + 1]) + '</div>';
                    }
                }
                $('#chatqa').html(chatHtml);
            }
        });
    },
    formatAgentCode: function (val, row, index) {
        with (row.user) {

            return agentCode ? (username ? username : "") + "(" + agentCode + ")" : "";
        }
    },
    saveOrUpdate: function () {
        $('#formItemName').textbox({value: strReplace($('#formItemName').val())});
        var vaild = $("#searchitemClick").form('enableValidation').form('validate');
        if (vaild) {
            $.messager.confirm('提示', '确定保存吗？', function (r) {
                if (r) {
                    holly.post(holly.getPath() + "/rest/itemmanager/itemManager", holly.form2json($("#searchitemClick")), function (e) {
                        if (e.success) {
                            var frameId = $('#frameId').val();
                            // var $obj = parent.$("#"+frameId)[0].contentWindow;//刷新列表
                            // $obj.itemManager.search();
                            holly.showSuccess("操作成功!");
                            // itemClick.closeCurrentIframe();

                            var url = holly.getPath() + '/hollysqm/itemmanager/page/itemmanager.jsp?frameId=' + frameId;
                            window.location = url;
                        } else {
                            holly.showError(e.errorMessage);
                        }
                    });
                }
            });
        }
    }, closeCurrentIframe: function () {//关闭当前页面
        var $tabs = parent.$('#tabs');
        var curTab = $tabs.tabs('getSelected');
        var tabIndex = $tabs.tabs('getTabIndex', curTab);
        $tabs.tabs('close', tabIndex);
        return false;
    }
};

/**
 *    过滤特殊字符
 *    str：要进行过滤的字符串
 */
function strReplace(str) {
    var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
    var rs = "";
    for (var i = 0; i < str.length; i++) {
        rs = rs + str.substr(i, 1).replace(pattern, '');
    }
    return rs;
}
 