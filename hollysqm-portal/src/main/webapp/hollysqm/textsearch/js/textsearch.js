function displaysubMenu(obj) {
	$("#colul").hide();
	event.stopPropagation();
	var $this = $(obj);
	//var x = $this.offset().left;
	var y = $this.offset().top;
	var z = document.body.clientWidth;
	var w = $('#colul').outerWidth();
	var subMenu = $('#colul');
	subMenu.toggle().css({'top':y+50,'left':z-w-50}); 
	$(document).on('click',docDisplaysubMenu);
	if(!$('#colul').is(":hidden")){
		searchCollectionList();
	} 
}
function docDisplaysubMenu(){
	$("#colul").hide();
	$(document).off('click',docDisplaysubMenu);
}
$(function(){
	// 初始化预览dialog
	$('#dlg-sqminfo').dialog({
		onClose:function(){
			// 关闭dialog停止播放语音
			if(listenTape.player.pause){
				listenTape.player.pause();
			}
		}
	});
	$('#standardId').combobox({
		url : holly.getPath() + '/rest/commonRest/queryStandardList?status=1',
		valueField : 'value', 
		textField : 'name',
		loadFilter: function(data){
			var jsonArray=new Array();
			var defaultVal = {"value":"","name":'请选择'};
			jsonArray.push(defaultVal);
			for(var o in data.content){
				jsonArray.push(data.content[o]);
			}
			return jsonArray;
		}
	}); 
});

//全文检索
function searchTextResult() {	
	var uri = "/rest/textSearchRest/getTextResultList";
    var recoinfostart = $('#recoinfoLengthstart').numberspinner('getValue');
	var recoinfoend = $('#recoinfoLengthend').numberspinner('getValue');
	if(parseInt(recoinfostart) > parseInt(recoinfoend)){
		holly.showError('通话时长开始时间不能大于结束时间');
		return false;
	}
	strReplace("caller");//,agent,words
	strReplace("agent");//,agent,words
	strReplace("words");//,agent,words
	strReplace("callee");//,agent,words
	$('#recoinfoLength').val(recoinfostart + '-' + recoinfoend);
	var param = holly.form2json($("#searchTextSearch"));
	param.serviceType = returnStrArr($('#formserviceType').combotree('getValues'));
	param.startTime = param.startTime.split('-').join("").split(':').join("").replace(' ','');
	param.endTime = param.endTime.split('-').join("").split(':').join("").replace(' ','');

	//静态数据处理
	if (param.caller.startsWith('186')) {
        uri = "/hollysqm/textsearch/json/textRecord_186.json";
	}else {
        uri = "/hollysqm/textsearch/json/textRecord.json";
    }

    if(!validateTime(param)) return false;
	
	holly.showLoading();
	holly.post(holly.getPath()+ uri,param,
	function(e){
		if(e.success) {
			textsearchHmtl(e.content);
			if(e.content){
				var total = e.content.total;
				$('#bottpage').pagination({
					total: total,
					pageSize:10,
					pageNumber:1,
					onSelectPage:function(pageNumber, pageSize){
						strReplace("caller");//,agent,words
						strReplace("agent");//,agent,words
						strReplace("words");//,agent,words
						strReplace("callee");//,agent,words
						param = holly.form2json($("#searchTextSearch"));
						if(!validateTime(param)) return false;
						param["page"] = pageNumber;
						param["rows"] = pageSize;
						holly.post(holly.getPath()+ uri,param ,
						function(a){
							if(a.success) {
								textsearchHmtl(a.content);
							}
							holly.hideLoading();
						});
					}
				});
				holly.hideLoading();
			}else{
				holly.hideLoading();
			}
			
		} else {
			holly.showError(e.errorMessage);
			holly.hideLoading();
		}
	});
	
}
function textsearchHmtl(content){
	var rows = content ? content.rows : null;
	var html = "";
	if(rows == null || rows.length == 0){
		$("#exportTable").hide();
		html = '<div class="nodatagrid"><span class="text">没有数据</span></div>';
	}else {
		$("#exportTable").attr("title",content.total).show();
		for(var i in rows) {
			var arrFlag = [];
			var arrText = [];
			var chatHtml = '';
			arrFlag= rows[i].txtContent.match(/n\d#/ig);
			arrText= rows[i].txtContent.split(/n\d#/ig);
			if(arrFlag) {
				for(var o=0; o<arrFlag.length; o++){
					if (arrFlag[o]=='n0#') {
						chatHtml += '<div class="text worker">坐席：'+(arrText[o+1])+'</div>';
					}else if(arrFlag[o]=='n1#'){
						chatHtml += '<div class="text visitor">客户：'+(arrText[o+1])+'</div>';
					}
				}
			} else {
				chatHtml += '<div class="text worker">'+rows[i].txtContent+'</div>';
			}
			
			html += "<div class='score_box'>";
			html += "<div class='dialogue'><a href='javascript:;' onclick=\"sqminfo('"+rows[i].contactId+"','"+rows[i].dataType+"')\">"+chatHtml + "</a></div>";
			//if(rows[i].qualityStatus == '0'){//未质检显示评分按钮
			html += "<div class='score_button'>";
			html += "<a href='javascript:;' class='score_boxbtn l-btn-main' onclick=\"jumptoscore('"+rows[i].contactId+"','"+rows[i].dataType+ "','" + rows[i].user.agentCode + "')\">评分</a>";
			//}
			
			html += "</div>";
			html += "<div class='dialogue_info clearfix'>";
			html += "<div class='detail_info clearfix '>";
			html += "<i class='info_icon state'></i>";
			html += "<span class='detail_target'>质检状态:</span>";
			html += "<span class='detail_content'>";
			if(rows[i].qualityStatus){
				html += qualityStatesMap[rows[i].qualityStatus];
			}
			html += "</span>";
			html += "</div>";
			
			html += "<div class='detail_info clearfix '>";
			html += "<i class='info_icon state'></i>";
			html += "<span class='detail_target'>客户品牌:</span>";
			html += "<span class='detail_content'>";
			if(rows[i].custBand){
				html += custBandMap[rows[i].custBand];
			}
			html += "</span>";
			html += "</div>";
			
			html += "<div class='detail_info clearfix '>";
			html += "<i class='info_icon state'></i>";
			html += "<span class='detail_target'>归属地:</span>";
			html += "<span class='detail_content'>";
			if(rows[i].custArea){
				if(custAreaMap[rows[i].custArea]) {
					html += custAreaMap[rows[i].custArea];
				}
				
			}
			html += "</span>";
			html += "</div>";
			html += "<div class='detail_info clearfix'>";
			html += "<i class='info_icon zuoxi'></i>";
			html += "<span class='detail_target'>坐席:</span>";
			html += "<span class='detail_content'>" + rows[i].user.username +"</span>";
			html += "</div>";
			html += "<div class='detail_info clearfix'>";
			html += "<i class='info_icon tel'></i>";
			html += "<span class='detail_target'>来电号码:</span>";
			if(rows[i].caller) {
				html += "<span class='detail_content'>" + rows[i].caller + "</span>";	
			} else {
				html += "<span class='detail_content'>无数据</span>";	
			}
			html += "</div>";
			html += "<div class='detail_info clearfix'>";
			html += "<i class='info_icon time'></i>";
			html += "<span class='detail_target'>来电时间:</span>";
			html += "<span class='detail_content'>" + formatTime(rows[i].startTime) + "</span>";
			html += "</div>";
			html += "</div>";
			html += "</div>";
		}
	}
	$("#text_content").html(html);
	$('.score_boxbtn').linkbutton();
}
//搜索我的收藏列表
function searchCollectionList() {
	holly.post(holly.getPath()+"/rest/collectionRest/getCollectionList",
	{},
	function(e){
		if(e.success) {
			var content = e.content;	
			var html = "";
			for(var i in content) {
				
				html += "<li>";
				html += "<div class='li_header clearfix'>";
				html += "<div class='green_item'></div>";
				html += "<span class='li_text'>" + content[i].comboName + "</span>";
				html += "<i class='li_icon delete' onclick=\"deleteCollection('"+content[i].collectId+"',this)\"></i>";
				html += "<i class='li_icon search' onclick=\"searchTextRecordByCollection('"+content[i].dataType+"','"+(content[i].words?content[i].words:"")+"','"+(content[i].caller?content[i].caller:"")+"','"+(content[i].agent?content[i].agent:"")+"','"+(content[i].qualityStatus?content[i].qualityStatus:"")+"','"+content[i].startTime+"','" + (content[i].callee?content[i].callee:"") + "','" + (content[i].custArea?content[i].custArea:"") + "','" + (content[i].custBand?content[i].custBand:"") + "','" + (content[i].recoinfoLengthStart?content[i].recoinfoLengthStart:"") + "','" + (content[i].recoinfoLengthEnd?content[i].recoinfoLengthEnd:"") + "','" + (content[i].satisfaction?content[i].satisfaction:"") + "','" + content[i].collectId + "','"  + content[i].endTime+"')\"></i>";
				html += "</div>";
				html += "<div class='li_content clearfix'>";
				html += "<div class='li_combom clearfix'>";
				html += "<div class='combom_name'>质检来源</div>";
				html += "<div class='combom_content'>" + dataTypeMap[content[i].dataType] +"</div>";
				html += "</div>";
				html += "<div class='li_combom clearfix'>"; 
				html += "<div class='combom_name'>关键词</div>";
				if(content[i].words) {
					html += "<div class='combom_content'>" + content[i].words + "</div>";
				}
				html += "</div>";
				html += "<div class='li_combom clearfix'>";
				html += "<div class='combom_name'>来电号码</div>";
				if(content[i].caller) {
					html += "<div class='combom_content'>" + content[i].caller + "</div>";
				}
				
				html += "</div>";
				html += "<div class='li_combom clearfix'>";
				html += "<div class='combom_name'>坐席</div>";
				if(content[i].agent) {
					html += "<div class='combom_content'>" + content[i].agent + "</div>";
				}
				html += "</div>";
				html += "<div class='li_combom clearfix'>"; 
				html += "<div class='combom_name'>开始时间</div>";
				html += "<div class='combom_content'>" + content[i].startTime + "</div>";
				html += "</div>";
				html += "<div class='li_combom clearfix'>";
				html += "<div class='combom_name'>结束时间</div>";
				html += "<div class='combom_content'>" + content[i].endTime + "</div>";
				html += "</div>";
				html += "<div class='li_combom clearfix'>";
				html += "<div class='combom_name'>状态</div>";
				if(content[i].qualityStatus) {
					html += "<div class='combom_content'>" + qualityStatesMap[content[i].qualityStatus] + "</div>";
				}
				
				html += "</div>"; 
				html += "<div class='li_combom clearfix'>";
				html += "<div class='combom_name'>归属地</div>";
				if(content[i].custArea) {
					html += "<div class='combom_content'>" + custAreaMap[content[i].custArea] +"</div>";
				}
				
				html += "</div>";
				html += "<div class='li_combom clearfix'>";
				html += "<div class='combom_name'>用户品牌</div>";
				if(content[i].custBand) {
					html += "<div class='combom_content'>" + custBandMap[content[i].custBand] +"</div>";
				}
				
				html += "</div>";
				
				html += "<div class='li_combom clearfix'>";
				html += "<div class='combom_name'>满意度</div>";
				if(content[i].satisfaction) {
					html += "<div class='combom_content'>" + satisfactionMap[content[i].satisfaction] +"</div>";
				}
				
				html += "</div>";
				
				html += "<div class='li_combom clearfix'>";
				html += "<div class='combom_name'>被叫号码</div>";
				if(content[i].callee) {
					html += "<div class='combom_content'>" + content[i].callee +"</div>";
				}
				
				html += "</div>";
				html += "<div class='li_combom clearfix'>";
				html += "<div class='combom_name'>接触时长</div>";
				html += "<div class='combom_content'>" + content[i].recoinfoLengthStart + '-' + content[i].recoinfoLengthEnd +"</div>";
				html += "</div>";
				
				/*holly.post(holly.getPath()+'/rest/commonRest/getServiceTypeNames',{"serviceTypeId":content[i].serviceType},function(e1){
					if(e1.success){
						if(e1.content){
							var serviceTypeArr = [];
							$.each(e1.content,function(index,item){
								serviceTypeArr.push(item.text);
							});
							serviceTypeText = serviceTypeArr.join('<br/>');
							html += "<div class='li_combom clearfix'>";
							html += "<div class='combom_name'>服务请求</div>";
							html += "<div class='combom_content'>" +serviceTypeText +"</div>";
							html += "</div>";
						}
					}
				},true);*/
				
				
				html += "</li>";
				
				
			}
			if(content == null || content.length == 0){
				html = '<div class="nodatagrid"><span class="text" style="color:#fff;">没有数据</span></div>';
			}
			$("#colul_list").html(html);
			
		} else {
			holly.showError(e.errorMessage);
		}
	});
}
function sqminfo(contactId,dataType){
	var url = holly.getPath()+"/hollysqm/textsearch/page/dlg-sqminfo.jsp";
	url += "?contactId=" + contactId;
	url += "&dataType=" + dataType;
	$('#dlg-sqminfo').dialog('open').dialog('setTitle','详情').dialog('refresh',url);//.dialog('open');
}
//评分跳转页面
function jumptoscore(contactId,dataType,agentCode){
	$('#dlg-templet').dialog('open');
	$('#standardId').combobox('setValue','');
	$("input[name='contactId']").val(contactId);
	$("input[name='paperType']").val(dataType);
	$("input[name='agentCode']").val(agentCode);
}
//评分
function createPaper(){
	if($('#formtemplet').form('enableValidation').form("validate")){
		var param = holly.form2json($('#formtemplet'));
		holly.post(holly.getPath()+"/rest/textSearchRest/getForwardByStandard",param,function(e){
			if(e.success){
				var frameId = window.frameElement && window.frameElement.id || '';
				$('#dlg-templet').dialog('close');
				if(e.content.forward=='1'){
					var url = holly.getPath()+'/hollysqm/unscored/page/score.jsp';
					url += '?dataType='+param.paperType;
					url += '&contactId='+param.contactId;
					url += '&paperId='+e.content.paperId;
					url += '&frameId=' + frameId;
					// parent.main.addTab('iframescore_textsearch',url,'评分');
					window.location = url;
				}else if(e.content.forward=='2'){
					var url = holly.getPath()+'/hollysqm/unchecked/page/check.jsp';
					url += '?dataType='+param.paperType;
					url += '&contactId='+param.contactId;
					url += '&paperId='+e.content.paperId;
					url += '&frameId=' + frameId;
					// parent.main.addTab('iframecheck_textsearch',url,'复核');
                    window.location = url;
                }else if(e.content.forward=='3'){
					var url = holly.getPath()+"/hollysqm/paperscore/page/dlg-sqminfo.jsp";
					url += "?paperId=" + e.content.paperId;
					$('#dlg-sqminfo').dialog('open').dialog('setTitle','质检单信息').dialog('refresh',url);
				}else if(e.content.forward=='4'){
					holly.showError('没有权限');
				}
			}else {
				holly.showError(e.errorMessage);
			}
		});
		// holly.post(holly.getPath()+"/rest/commonRest/createPaper",param,function(e){
		// 	if(e.content != null){
		// 		var frameId = window.frameElement && window.frameElement.id || '';
		// 		var url = holly.getPath()+'/hollysqm/unscored/page/score.jsp';
		// 		url += '?dataType='+param.paperType;
		// 		url += '&contactId='+param.contactId;
		// 		url += '&paperId='+e.content.paperId;
		// 		url += '&frameId=' + frameId;
		// 		parent.main.addTab('iframescore_textsearch',url,'评分');
		// 		$('#dlg-templet').dialog('close');
		// 	}else {
		// 		holly.showError(e.errorMessage);
		// 	}
		// });
	}
}
//删除收藏
function deleteCollection(collectId,el) {
	$.messager.confirm('提示', '确定删除此收藏?', function(r){
		if (r){
			holly.post(holly.getPath()+"/rest/collectionRest/delCollections",
			{"collectId":collectId},
			function(e){
				if(e.success){
					if($(el).closest('li').siblings().length == 0){
						$("#colul_list").html('<div class="nodatagrid"><span class="text" style="color:#fff;">没有数据</span></div>');
					}
					$(el).closest('li').remove();
					holly.showSuccess('删除成功');
				}
				else {
					holly.showError(e.errorMessage);
				}
			});
		}
	});
}

//根据指定收藏条件进行搜索
function searchTextRecordByCollection(dataType,content,caller,agentName,qualityStatesMap,startTime,callee,custArea,custBand,recoinfoLengthStart,recoinfoLengthEnd,satisfaction,collectId,endTime) {
	holly.post(holly.getPath()+"/rest/collectionRest/getCollectionById", {"collectId":collectId}, function(e){
		if(e.content) {
			var serviceType = '';
			if(e.content.serviceType){
				serviceType = e.content.serviceType;
				serviceType = serviceType.replace(/\'/g,'');
				$("#formserviceType").combotree('setValues',serviceType);
			}
			$('#searchTextSearch').form('load',{
				'dataType':dataType,
				'words':content,
				'caller':caller,
				'agent':agentName,
				'qualityStatus':qualityStatesMap[qualityStatesMap],
				'startTime':startTime,
				'endTime':endTime,
				'custArea':custAreaMap[custArea],
				'custBand':custBandMap[custBand],
				'callee':callee,
				'recoinfoLengthStart':recoinfoLengthStart,
				'recoinfoLengthEnd':recoinfoLengthEnd,
				'satisfaction':satisfactionMap[satisfaction]
			});
			searchTextResult(); 
			docDisplaysubMenu();
		}
	});
	
}
function addCollection(){
	var param = holly.form2json($('#searchTextSearch'));
	param.comboName = $('#comboName').textbox('getValue');
	param.serviceType=param.serviceType = returnStrArr($('#formserviceType').combotree('getValues'));
	if($('#collectionForm').form('enableValidation').form('validate')){
		holly.post(holly.getPath()+"/rest/collectionRest/addCollections",param,
		function(e){
			if(e.success){
				holly.showSuccess('添加成功');
				$('#dlg1').dialog('close');
				$('#comboName').textbox('setValue',"");

			}
			else {
				holly.showError(e.errorMessage);
			}
		});	
	}
}
/*
导出
*/
function exportPaper (toPage){
	
	var total = parseInt($("#exportTable").attr("title"));
	if(total > export_total ){//导出数据大于10万给予提醒
		$.messager.alert("提示","导出数据不能大于"+export_total+"，请继续筛选数据！");
		return false;
	}
	if(toPage){
		var url =holly.getPath()+"/rest/csv/wizard/export?tableId=csv.score.queryTextsearch&module=textSearchRest&mediaId=1";
		var targetIframe ="iframe-export";
		$('#searchTextSearch').attr("action",url);
		$('#searchTextSearch').attr("target",targetIframe);
		$('#searchTextSearch').submit();
		$('#dlg-export').dialog('open');
	}else{
		$("#iframe-export")[0].contentWindow.exportData();
		$('#dlg-export').dialog('close');
	}
}

function initParams(){
	//var today = holly.getDateStr(0);
	var start = getNowFormatDate().substr(0,10) + ' 00:00:00';
	var end = getNowFormatDate().substr(0,10) + ' 23:59:59';
	$("#searchTextSearch").form("reset");
	$('#startTime').datetimebox('setValue',start);
	$('#endTime').datetimebox('setValue',end);
	$('#formDataType').combobox('setValue','v8');//默认V8
}

/**
*	过滤特殊字符
*	str：要进行过滤的字符串
*/
function strReplace(ids){
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
	var rs = "",array = ids.split(",");
	for(var i =0;i<array.length;i++){
		var id = array[i];
		var str = $("#"+id).textbox("getValue");
		for (var i = 0; i < str.length; i++) {
			rs = rs+str.substr(i, 1).replace(pattern, '');
		}
		$("#"+id).textbox("setValue",rs);
	}
}

function validateTime(params){
	var startTime = params.startTime;
	var endTime = params.endTime;
	
	var startTimeLon = new Date(startTime).getTime();
	var lastMonthLon = new Date(endTime).getTime();
	
	if(startTime > endTime){
		$.messager.alert('提示','来电开始时间不能大于来电结束时间!');
		return false;
	}
	var timeLon = (lastMonthLon - startTimeLon) / (1000*60*60*24);
	if(timeLon > 31){
		$.messager.alert('提示','来电开始时间和来电结束时间不能超过31天!');
		return false;
	}
	return true;
}

function getNowFormatDate() {
    var date = new Date();
    var seperator1 = "-";
    var seperator2 = ":";
    var month = date.getMonth() + 1;
    var strDate = date.getDate();
    if (month >= 1 && month <= 9) {
        month = "0" + month;
    }
    if (strDate >= 0 && strDate <= 9) {
        strDate = "0" + strDate;
    }
    var currentdate = date.getFullYear() + seperator1 + month + seperator1 + strDate
            + " " + date.getHours() + seperator2 + date.getMinutes()
            + seperator2 + date.getSeconds();
    return currentdate;
}

function formatTime (time) {
	var year = time.substr(0,4);
	var month = time.substr(4,2);
	var day = time.substr(6,2);
	var hour = time.substr(8,2);
	var minute = time.substr(10,2);
	var second = time.substr(12,2);
	return year + '-' + month + '-' + day + ' ' + hour + ":" + minute + ':' + second;
}

 function returnStrArr (arr){
	var str = '';
	$.each(arr,function(index,item){
		str+=index==0?'':',';
		str+="'"+item+"'";
	});
	return str;
}