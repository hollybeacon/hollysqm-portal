var dlgSqminfo_TS = {
	/**
	 * 初始化加载数据  
	 */
	param : {
		"contactId":contactId_TS,
		"dataType":dataType_TS
	},
	init:function(){

		holly.post(holly.getPath()+"/rest/commonRest/getRecord",dlgSqminfo_TS.param,function(e){
			if(e.success){
				var content = e.content;
				var html = '';
				var chatHtml = '';
				var arrFlag = [];
				var arrText = [];
				if(e.content.dataType=='v8'){
					dlgSqminfo_TS.param.contactId = content.contactId;
					dlgSqminfo_TS.param.acceptTime = content.acceptTime;
					dlgSqminfo_TS.param.recordFile = content.recordFile;
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">来电号码</div>';
					if(content.caller != null) {
						html += '<div class="icontrols">'+content.caller+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">坐席</div>';
					if(content.user != null){
						html += '<div class="icontrols">'+content.user.username+'('+content.user.agentCode+')'+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">归属地</div>';
					if(content.custArea != null) {
						html += '<div class="icontrols">'+dlgSqminfo_TS.paramJson(content.custArea,'CUST_AREA')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">品牌</div>';
					if(content.custBand != null) {
						html += '<div class="icontrols">'+dlgSqminfo_TS.paramJson(content.custBand,'CUST_BAND')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">满意度</div>';
					if(content.satisfaction != null) {
						html += '<div class="icontrols">'+dlgSqminfo_TS.paramJson(content.satisfaction,'V8SATISFACTION')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">业务类型</div>';
					if(content.serviceType != null) {
						html += '<div class="icontrols">'+dlgSqminfo_TS.paramJson(content.bussinessType,'BUSINESS_TYPE')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">开始时间</div>';
					if(content.startTime != null) {
						html += '<div class="icontrols">'+content.startTime+'</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">通话时长</div>';
					if(content.length != null) {
						html += '<div class="icontrols">'+content.length+'秒</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">用户级别</div>';
					if(content.custLevel != null) {
						html += '<div class="icontrols">'+dlgSqminfo_TS.paramJson(content.custLevel,'CUST_LEVEL')+'</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">静音时长</div>';
					if(content.custLevel != null) {
						html += '<div class="icontrols">'+ content.silenceLength +'秒</div>';
					}
					html += '</div>';
				
					$('#voicechatDiv_TS').show();
					// 接触记录图片音频地址查询
					holly.post(holly.getPath()+"/rest/commonRest/getRecordDir",{"contactId":dlgSqminfo_TS.param.contactId, "acceptTime":dlgSqminfo_TS.param.acceptTime, "recordFile":dlgSqminfo_TS.param.recordFile},function(e){
						if(e.success){
							listenTape.init(e.content.wavDir,e.content.imgDir);
						}else{
							toastr.error(e.errorMessage);
						}
					});
				}else if(e.content.dataType=='i8'){
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">来话号码</div>';
					if(content.caller != null) {
						html += '<div class="icontrols">'+content.caller+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">坐席</div>';
					if(content.user != null){
						html += '<div class="icontrols">'+content.user.username+'('+content.user.agentCode+')'+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">关闭方式</div>';
					if(content.closeType != null) {
						html += '<div class="icontrols">'+dlgSqminfo_TS.paramJson(content.closeType,'CLOSE_TYPE')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">会话等级</div>';
					if(content.sessionType != null) {
						html += '<div class="icontrols">'+dlgSqminfo_TS.paramJson(content.sessionType,'SESSION_TYPE')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">满意度</div>';
					if(content.satisfaction != null) {
						html += '<div class="icontrols">'+dlgSqminfo_TS.paramJson(content.satisfaction,'I8SATISFACTION')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">服务类型</div>';
					if(content.serviceType != null) {
						html += '<div class="icontrols">'+dlgSqminfo_TS.paramJson(content.serviceType,'I8_SERVICE_TYPE')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">开始时间</div>';
					if(content.startTime != null) {
						html += '<div class="icontrols">'+content.startTime+'</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">会话时长</div>';
					if(content.length != null) {
						html += '<div class="icontrols">'+content.length+'秒</div>';
					}
					html += '</div>';					
				}
				$('#getRecordDiv').html(html);

				arrFlag= content.txtContent.match(/n\d#/ig);
				arrText= content.txtContent.split(/n\d#/ig);
				for(var i=0; i<arrFlag.length; i++){
					if (arrFlag[i]=='n0#') {
						chatHtml += '<div class="text worker"><span style="text-align:right;">'+(i+1)+'&nbsp;&nbsp;</span>坐席：'+(arrText[i+1])+'</div>';
					}else if(arrFlag[i]=='n1#'){
						chatHtml += '<div class="text visiter"><span style="text-align:right;">'+(i+1)+'&nbsp;&nbsp;</span>客户：'+(arrText[i+1])+'</div>';
					}
				}
				$('#chatqa').html(chatHtml);
			}else{
				toastr.error(e.errorMessage);
			}
		});
	},
	paramJson:function(param,getdictname){
		if(param){
			var map = $("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':getdictname}});
			var text = '';
			$.each(param.split(","), function(index, item){
				text += (index==0?'':',')+(map[item]?map[item]:'');
			});
			return text;
		}
	}

};


var listenTape = {
	player: new Object(),
	init:function(voicePath,imgPath){
		var intever = null;//定时器
		listenTape.player = new _mu.Player({
			baseDir: holly.getPath() + '/hollysqm/common/muplayer/dist'
		});//初始播放器
		//setSong(contextPath+path);//设置播放录音
		//音量调节
		$("#voice").slider();
		$(document).on('sliderchange', "#voice", function (e, result) {
			listenTape.player.setVolume(result.value);
		});
		$('.myautoplayer-play').on('click',function(){
			if($(this).hasClass('icon-play')){
				listenTape.player.play();//播放事件
				$(this).removeClass("icon-play").addClass("icon-pause");
				intever = scrollImg();
			}else if($(this).hasClass('icon-pause')){
				listenTape.player.pause();//暂停
				$(this).removeClass("icon-pause").addClass("icon-play");
				clearInterval(intever);//清除事件
			}
		});
		//图片点击播放事件
		$("#divScroll").click(function (e) {
			var scrollLeft = $("#divScroll").scrollLeft();
			var offset = $(this).offset();
			var relativeX = (e.pageX - offset.left) + scrollLeft;
			var len = parseInt(relativeX / $("#imgVoice").width() * listenTape.player.duration() * 1000);
			listenTape.player.play(len);
			intever = scrollImg();
			$(".icon-play").addClass("icon-pause").removeClass("icon-play");
		});
		//滚动图片
		function scrollImg() {
			return setInterval(function () {//改变滑动块
				var l = listenTape.player.curPos() / listenTape.player.duration();
				var left = $("#imgVoice").width();//图片宽度
				if(l >(1-900/left)){
					clearInterval(intever);//清除事件
					$(".icon-pause").addClass("icon-play").removeClass("icon-pause");
					return ;
				}
				$("#divScroll").animate({scrollLeft: left * l}, 1000);
			}, 1000);
		}

		//滚动条拖动事件
		$("#divScroll").mousedown(function () {
			var state = listenTape.player.getState();
			if (state == 'playing') {
				listenTape.player.pause();//暂停
				$(".icon-pause").addClass("icon-play").removeClass("icon-pause");
				clearInterval(intever);//清除事件
			}
		});
		listenTape.player.setUrl(holly.getPath() + "/" + voicePath );//添加录音文件
		$("#imgVoice").attr("src",holly.getPath() + "/" + imgPath).removeClass('voiceloading').addClass('voicechatimg');
	}
};