<%@page import="com.hollycrm.hollybeacon.basic.util.AppConfigUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>全文检索</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script src="../js/textsearch.js"></script>
<link rel="stylesheet" href="../css/textsearch.css">
<%
Integer export_total = AppConfigUtils.getConfig().getInteger("system.export.total", 100000);
%>
<script type="text/javascript">
var export_total = <%=export_total%>;
var dataTypeMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'DATATYPE'}});
var custBandMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'CUST_BAND'}});
var custAreaMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'CUST_AREA'}});
var satisfactionMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'V8SATISFACTION'}});
var qualityStatesMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'QUALITY_STATUS2'}});
$(function() {
	$("#formDataType").combobox("init_extend",{'codeType':'DATATYPE','hasAllOption':false,'event':{
		onLoadSuccess:function(){
			$(this).combobox('setValue','v8');
		}
	}});
	$("#formQualityState").combobox("init_extend",{'codeType':'QUALITY_STATUS2'});
	$("#formCustBand").combobox("init_extend",{'codeType':'CUST_BAND'});
	$("#formSatisfaction").combobox("init_extend",{'codeType':'V8SATISFACTION'});
	$("#formCustArea").combobox("init_extend",{'codeType':'CUST_AREA'});
	
	$('#formserviceType').combotree({
		url:holly.getPath()+"/rest/commonRest/getServiceType",
		panelMinHeight:200,
		panelMaxHeight:300,
		checkbox:true,
		loadFilter:function(data){
			var newData = eval('(' + data.content + ')');
			return newData; 
		},
		onLoadSuccess:function(){
			
		}
	});
	
	initParams();
	$("#searchTextSearch").keyup(function(event){
		if(event.keyCode==13){
			searchTextResult();
		}
	});
	
	searchTextResult();
});
function search(){
	searchTextResult();
}
</script>
</head>  
<body class="easyui-layout">
	<div data-options="region:'center',border:false">
		<div class="easyui-layout" data-options="fit:true">
			<!-- 搜索区域 -->
			<div data-options="region:'north',border:false" style="padding-bottom:10px;">
				<div class="title_btn clearfix">
					<a href="javascript:void(0)" class="easyui-linkbutton" id= "list1"  onclick="displaysubMenu(this,event)">我的收藏</a> 
					<a href="javascript:void(0)" class="easyui-linkbutton"
					 onclick="$('#dlg1').dialog('open');">添加至收藏</a> 
					<a href="javascript:void(0)" class="easyui-linkbutton" id= "exportTable" onclick="exportPaper(true)">导出</a> 
				</div> 
				<form id="searchTextSearch" method="post" class="search-form">
					<div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">质检来源</span>
							<div class="valueGroup">
								<select id="formDataType" name="dataType" class="easyui-combobox" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">来电时间</span>
							<div class="valueGroup">
								<input type="text" class="easyui-datetimebox" name="startTime" id="startTime" data-options="editable:false,width:'100%'">
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">至</span>
							<div class="valueGroup">
								<input type="text" class="easyui-datetimebox" name="endTime" id="endTime" data-options="editable:false,width:'100%'">
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">来电号码</span>
							<div class="valueGroup">
								<input type="text" class="easyui-textbox" name="caller" id="caller" data-options="editable:true,width:'100%',validType:'length[1,15]'">
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">质检状态</span>
							<div class="valueGroup">
								<select id="formQualityState" name="qualityStatus" class="easyui-combobox" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">坐席</span>
							<div class="valueGroup">
								<input type="text" class="easyui-textbox" name="agent" id="agent" data-options="editable:true,width:'100%',validType:'length[1,40]'">
							</div>
						</span>
						<span class="screeningbar-item" style="width: 50%">
							<span class="label">关键词</span>
							<div class="valueGroup">
								<input type="text" class="easyui-textbox"  name="words" id = "words" data-options="editable:true,width:'100%',prompt:'多个关键词用空格隔开',validType:'length[1,1000]'">
							</div>
						</span>		
						
						<span class="screeningbar-item">
							<span class="label">满意度</span>
							<div class="valueGroup">
								<select id="formSatisfaction" name="satisfaction" class="easyui-combobox" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>
						
						<span class="screeningbar-item">
							<span class="label">客户品牌</span>
							<div class="valueGroup">
								<select id="formCustBand" name="custBand" class="easyui-combobox" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>
						
						<span class="screeningbar-item">
							<span class="label">归属地</span>
							<div class="valueGroup">
								<select id="formCustArea" name="custArea" class="easyui-combobox" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>
						
						<span class="screeningbar-item">
							<span class="label">被叫号码</span>
							<div class="valueGroup">
								<input type="text" class="easyui-textbox" name="callee" id="callee" data-options="editable:true,width:'100%',validType:'length[1,15]'">
							</div>
						</span>
						
						<span class="screeningbar-item">
							<span class="label">接触时长</span>
							<div class="valueGroup">
								<input class="easyui-numberspinner" name="recoinfoLengthStart" id="recoinfoLengthstart" data-options="width:'100%',required: true,novalidate:true,value:'1',min:1">
							</div>
						</span>
						
						<span class="screeningbar-item">
							<span class="label">至</span>
							<div class="valueGroup">
								<input class="easyui-numberspinner" name="recoinfoLengthEnd" id="recoinfoLengthend" data-options="width:'100%',required: true,novalidate:true,value:'6000',min:1">
							</div>
						</span>
						
						<span class="screeningbar-item">
							<span class="label">服务请求</span>
							<div class="valueGroup">
								<select id="formserviceType" name="serviceType" style="width:560px;" data-options="editable:false,width:'100%',multiple:true"></select>
							</div>
						</span>
						
						
					</div>
					<div class="btn-group">
						<a href="javascript:;" class="operation search-btn" onclick="searchTextResult()" title="搜索"></a>
						<a href="javascript:;" class="operation reset-btn" onclick="initParams()" title="重置"></a>
					</div>
					<input type="hidden" name="recoinfoLength" id="recoinfoLength"/>
				</form>
			</div>
			<!-- 搜索区域 end -->			
			<div data-options="region:'center',border:false" id="text_content">
			</div>
			<div data-options="region:'south',border:'false'">
				<div style="height:42px;"><div id="bottpage"></div></div> 
			</div>
		</div>
	</div>
	<div class="combo_list" id="colul" onclick="event.stopPropagation()">
		<ul class="ul_list" id="colul_list"></ul><div class='list_angel'></div>
	</div>
	<div class="add_list" id="list1"></div>
	<div class="easyui-dialog" id="dlg1" title="添加到收藏" style="width: 400px; height:200px;padding: 10px" data-options="closed:true,buttons:'#dlg-buttons1'">
		<form class="easyui-form iform-horizontal" id="collectionForm" data-options="novalidate:true">
			<div class="icontrol-group">
				<div class="icontrol-label"><i class="required">*</i>组合条件名称</div>
				<div class="icontrols">
					<input name="comboName" id="comboName" class="easyui-textbox" data-options="width:'100%',required:true,validType:{length:[0,8]}" />
				</div>
			</div>
		</form>
	</div>
	<div id="dlg-buttons1">
		<a href="javascript:void(0)" class="easyui-linkbutton l-btn-main" onclick="addCollection()">添加</a> 
		<a href="javascript:void(0)" class="easyui-linkbutton"onclick="$('#dlg1').dialog('close')">取消</a>
	</div>
	
	<div id="dlg-export" class="easyui-dialog" title="导出全文检索"
		data-options="
			closed:true,
			buttons: '#dlg-export-buttons',
			width:650,
			height:'95%'
		">
		<iframe id="iframe-export" name="iframe-export" src="" frameborder="0" style="width:100%;height:100%;display:block;"></iframe>
	</div>
	<div id="dlg-export-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red" onclick="exportPaper(false)">确定</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#dlg-export').dialog('close')">取消</a>
	</div>

	<div class="easyui-dialog" id="dlg-templet" title="选择评分模板" style="width: 400px; height:200px;padding: 10px" data-options="closed:true,buttons:'#dlg-buttons-templet'">
		<form id="formtemplet" class="easyui-form iform-horizontal" method="post" data-options="novalidate:true">
			<div class="icontrol-group">
				<div class="icontrol-label">选择评分模板</div>
				<div class="icontrols">
					<select name="standardId" id="standardId" data-options="editable:false,width:'100%',required: true,validType:'mustSelect',novalidate:true"></select>
				</div>
			</div>
			<input type="hidden" name="contactId" />
			<input type="hidden" name="paperType" />
			<input type="hidden" name="agentCode" />
		</form>
		
	</div>
	<div id="dlg-buttons-templet">
		<a href="javascript:void(0)" class="easyui-linkbutton l-btn-main" onclick="createPaper()">确定</a> 
		<a href="javascript:void(0)" class="easyui-linkbutton"onclick="$('#dlg-templet').dialog('close')">取消</a>
	</div>
	<div id="dlg-sqminfo" class="easyui-dialog" title="标题"
		data-options="
			closed:true,
			width:'95%',
			height:'95%',
			draggable:false
		">
	</div>
</body>
</html>