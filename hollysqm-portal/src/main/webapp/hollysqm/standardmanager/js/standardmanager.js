var  standardManager= {
	/**
	 * 初始化加载数据
	 */
	init:function(){

		//$("#formstandardName").combobox("init_extend",{'codeType':'ITEM_NAME'});
		$("#formstate").combobox("init_extend",{'codeType':'STATUS'});
		$("#standardManagerdatagrid").datagrid({
				fit:true,
				url:holly.getPath()+'/rest/standardmanager/queryStandard',
				method:'get',
				singleSelect : true, 
				border:false,
				fitColumns:true,
				pagination: true,
				loadFilter: function(data){return (data.content)?data.content:data;},
				columns:[[
					{field:'standardName',title:'模板名称',width:100},
					{field:'totalScore',title:'总分',width:30},
					{field:'status',title:'启用状态',width:60,formatter:standardManager.formatstate},
					{field:'modifyName',title:'最后修改人',width:60},
					{field:'modifyTime',title:'最后修改时间',width:80},
					{field:'option',title:'操作',width:60,formatter:standardManager.formatOption}
				]],
				loadMsg:'数据加载中请稍后……',
				onLoadSuccess: function (data)  {
					if (data.rows.length == 0) {
						var body = $(this).data().datagrid.dc.body2;
						body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
					}
				}
			});	
	},

	
	/**
	 * 搜索
	 */
	search:function(){
		strReplace("standardName");
		var param = holly.form2json($("#searchstandardManager"));
		$("#standardManagerdatagrid").datagrid("search",{'param':param});
	},
	// 启用状态
	formatstate:function(val, row, index) {
		var res =stateMap[val]; //获取对应的数据
		return res;
	},

	// 最后修改人
	formatModifier:function(val,row,index){
		return val?"<span title='"+row.modifierName + "(" +val+ ")"+"'>"+row.modifierName + "(" + val+ ")"+"</span>":"";
	},
	// 操作
	formatOption:function(val,row,index){
		var content = '';
			content += "<span class='grid-opt-wrap'>";
			content += "<div class='switch "+(row.status=="1"?'switch-on':'')+"' onclick=\"standardManager.switchStatus(this,'"+row.status+"','"+row.standardId+"','"+index+"')\"></div>";
			content	+= "<a href='javascript:void(0)' onclick=\"standardManager.edit('"+row.standardId+"')\" class='grid-optcon icon-edit' title='编辑'></a>";
			content += "</span>";
		return content;
	},
	// 标签名称
	formatLink:function(val,row,index){
		return val?"<a href='javascript:;' title='"+val+"' onclick=\"standardManager.planinfo('"+row.planId+"')\">"+val+"</a>":"";
	},
	switchStatus:function(obj,state,standardId,index){
		var enable = state=="1"?stateMap["0"]:stateMap["1"];
		var json={
			title:enable,
			content:"确定修改该评分模板状态为"+enable+"吗？",
			message:"该评分模板已成功"+enable
		};
		$.messager.confirm(json.title,json.content,function(r){
			if(r){
				 holly.post(holly.getPath()+"/rest/standardmanager/enable",{"standardId":standardId,"status":state},function(e){
					 if(e.success){
						$("#standardManagerdatagrid").datagrid('reload');
						holly.showSuccess(json.message);
					 }else{
						 holly.showError(e.errorMessage);
					 }
				 });
			}
		});
	},
	initScoreList : function(){//加载评分项
		 holly.get(holly.getPath()+"/rest/standardmanager/scoreAllList",'',function(e){
			 if(e.success){
				var html = "<tr><th>名称</th><th>分值</th><th>名称</th><th>分值</th><th>名称</th><th>分值</th></tr>";
				var rows = e.content;
				html += "<tr>";
				for(var i in rows){

					html +="<td><input type='checkbox' onclick='return standardManager.calculateScore(this)' name='scoreListName' score="+
						rows[i].score+" scoreType='"+rows[i].scoreType+"' value='"+rows[i].scoreListId+"' id='"+rows[i].scoreListId+"'><label for='"+rows[i].scoreListId+"'>"
						+rows[i].itemName+"</label></input></td>";
					html +="<td><span>"+((rows[i].scoreType == "0")?"+":"-")+rows[i].score+"</span></td>";
					if((i+1)%3 == 0){//每3列，就换一行
						html += "</tr><tr>";
					}
				}
				html += "</tr>";
				$("#scoreList").html(html);
			 }else{
				 holly.showError(e.errorMessage);
			 }
		 });
	},
	calculateScore : function(obj){//计算总分数
		var total = 0;
		//获取所有已选中的评分项
		var class_value = $(obj).attr('class');
		if("checked" == class_value && $("#isUpdate").val() == 1){
			$.messager.alert("提示","该评分模板已经引用到质检计划中，评分项不允许取消");
			return false;//禁止取消保存后选中评分项
		}
		$('input[name="scoreListName"]:checked').each(function(){ 
			if($(this).attr("scoreType") == "0")//判断加分项
				total += parseInt($(this).attr("score"));
			else if($(this).attr("scoreType") == "1")//判断减分项
				total -= parseInt($(this).attr("score"));
			else{
				holly.showError("该评分项无加减分项！");
			}
		}); 
		$(".summary span").text(total);
		$("#totalScore").val(total);
		return true;
	},
	getScreList : function(obj){
		var a = new Array();
		$(obj).each(function(){ 
			a.push(this.value);
		}); 
		return a;
	},
	saveOrUpdate : function(){
		var form = holly.form2json($("#ff"));
		
		if(!$.trim(form.standardName)){
			$.messager.alert("提示","模板名称不能为空");
			return false;
		}
		var scoreListName=  $('input[name="scoreListName"]:checked');
		if(scoreListName.length == 0){
			$.messager.alert("提示","评分项不能为空");
			return false;
		}
		
		form["scoreArray"] = standardManager.getScreList(scoreListName).toString();
		var vaild = $("#ff").form('enableValidation').form('validate');
		if(vaild){
			$.messager.confirm('提示', '确定提交吗？', function(r){	
				if(r){
					holly.post(holly.getPath()+"/rest/standardmanager/standardManager",form,function(e){
						if(e.success){
							holly.showSuccess("操作成功!");
							$("#standardManagerdatagrid").datagrid('reload');
							$('#dlg1').dialog('close');
						}else{
							holly.showError(e.errorMessage);
						}
					});
				}
			});
		}
	},
	edit : function(id){
		standardManager.resetFFForm();
		holly.get(holly.getPath()+"/rest/standardmanager/checkStandard",{"standardId":id},function(e){
			if(e.success){
				$('#ff').form('clear');
				with(e){
					$("#ff").form('load',content);
					standardManager.defaultScoreList(content.scoreList);
					$(".summary span").text(content.totalScore);
					$("#isUpdate").val(content.isUpdate);
					if(content.status == 1){
						$("#switch").attr("class","switch switch-on");
					}
				}
				$('#dlg1').dialog({'title':'修改评分模板'}).dialog('open');
			}else{
				holly.showError(e.errorMessage);
			}
		});
	},
	defaultScoreList : function(array){
		for(var i in array){
			$("input:checkbox[value='"+array[i].scoreListId+"']").attr('checked','true').attr("class","checked");
			//$("input:checkbox[value='"+array[i].scoreListId+"']").trigger("click");
		}
	},
	resetFFForm : function(){//重置评分项模板form表达那
		$("#ff #standardId").val("");
		$("input:checkbox[name='scoreListName']").removeAttr("checked");
		$("#switch").attr("class","switch");
		$('#ff').form('reset');
		$("#ff input:hidden[name='status']").val("0");
		$('.summary span').text(0);
		$('#dlg1').dialog({'title':'新建评分模板'}).dialog('open');
	}
};
/**
*	过滤特殊字符
*	str：要进行过滤的字符串
*/
function strReplace(id){
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
	var rs = "";
	var str = $("#"+id).textbox("getValue");
	if($.trim(str).length > 0 ){
		for (var i = 0; i < str.length; i++) {
			rs = rs+str.substr(i, 1).replace(pattern, '');
		}
		$("#"+id).textbox("setValue",rs);
	}
} 