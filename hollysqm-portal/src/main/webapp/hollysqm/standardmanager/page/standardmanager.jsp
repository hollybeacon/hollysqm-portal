<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>评分模板管理</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/switch.css">
<link rel="stylesheet" href="${ctx}/hollysqm/standardmanager/css/dialog.css">
<script type="text/javascript" src="../js/standardmanager.js" ></script>
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type="text/javascript">
// 获取必要的数据字典MAP  
//var standardNameMap=$("#standardManagerdatagrid").datagrid("getdictnames",{'param':{'codeType':'STANDARD_NAME'}});
var stateMap=$("#standardManagerdatagrid").datagrid("getdictnames",{'param':{'codeType':'STATUS'}});
$(function() {
	standardManager.init();
	standardManager.initScoreList();
	
	$("#searchstandardManager").keyup(function(event){
		if(event.keyCode==13){
			standardManager.search();
		}
	});
});
</script>
</head>
<body class="easyui-layout">
	<div data-options="region:'center',border:false" style="padding:10px;">
		<div class="easyui-layout" data-options="fit:true">
			<!-- 搜索区域 -->
			<div data-options="region:'north',border:false" style="padding-bottom:10px;">
				<form id="searchstandardManager" method="post" class="search-form">
					<div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">模板名称</span>
							<div class="valueGroup">
								 <input class="easyui-textbox" name="standardName" id="standardName" data-options="width:'100%'" >		
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">启用状态</span>
							<div class="valueGroup">
								<select name="status" id="formstate" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>	
					</div>
					<div class="btn-group">
						<a href="javascript:;" class="operation search-btn" onclick="standardManager.search()" title="搜索"></a>
						<a href="javascript:;" class="operation reset-btn" onclick="$('#searchstandardManager').form('reset');" title="重置"></a>
					</div>
				</form>
			</div>
			<!-- 搜索区域 end -->			
			<div data-options="region:'center',border:false">
				<div id="tt">
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="standardManager.resetFFForm()">新建</a> 
				</div>
				<div class="easyui-panel" title="评分模板" data-options="tools:'#tt',fit:true">
					<table id="standardManagerdatagrid" ></table>
				</div>
			</div>
		</div>
	</div>
	<div id="dlg-Template" class="easyui-dialog" style="width:500px;height:750px;padding:10px;" data-options="closed:true,buttons:'#dlg-Template-buttons'">
	<form id="TemplateForm" class="easyui-form" method="post" data-options="novalidate:true">
		
	</form>
	</div>
	<div id="dlg-Template-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton l-btn-main" onclick="standardManager.save()">保存</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#dlg-Template').dialog('close')">取消</a>
	</div>
	<div id="dlg1" class="easyui-dialog" title="新建评分模板" style="width: 80%; height:90%;padding: 10px" data-options="iconCls:'icon-exclamation-sign',closed:true,buttons:'#dlg-buttons1',onOpen:function(){$(this).scrollTop(0)}">
		<form id="ff" method="post" data-options="novalidate:true">
			<input type="hidden" name="totalScore" id="totalScore"/>
			<input type="hidden" name="standardId" id="standardId"/>
			<input type="hidden" name="scoreArray"/>
			<input type="hidden" name="isValid" value="1"/>
			<input type="hidden" id="isUpdate"/>
			
			<div class="dlg_bar clearfix">
				<span class="nam"><span class="red">*</span>模板名称</span>
				<div class="dlg_input">
					 <input class="easyui-textbox" name="standardName"  data-options="width:'100%',novalidate:true,required:true,validType:'length[1,100]'" >
				</div>
			</div>
			<div class="dlg_bar clearfix">
				<span class="nam"><span class="red">*</span>评分项</span>
				<div class="combo_box">
					<div class="combo_text">人工评分项</div>
					<div class="combo_tbl">
						<table id="scoreList">
							
						</table>
					</div>
				</div>
				<div class="summary">已选扣分项总分值：<span>0</span></div>
			</div>
			<div class="dlg_bar clearfix">
				<span class="nam">启用状态</span>
				<div class="combo_box" style="padding-top: 3px;">
					<div class="switch" id="switch"></div>
					<input type="hidden" name="status" value="0">
				</div>
			</div>
			<script>
				var s = document.querySelector('#switch');
				var i = document.querySelector('input[name="status"]');
				s.onclick = function() {
					this.className = this.className == "switch" ? "switch switch-on" : "switch";
					i.value = i.value == '1' ? '0' : '1';
				};
			</script>	
		</form>
		<div id="dlg-buttons1">
			<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red"onclick="standardManager.saveOrUpdate()">保存</a> 
			<a href="javascript:void(0)" class="easyui-linkbutton"
			onclick="$('#dlg1').dialog('close')">取消</a>
		</div>
	</div>
</body>
</html>