<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="utf-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<c:set var="ctx" value="${pageContext.request.contextPath}" />
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>导入向导页</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<link href="${ctx}/hollybeacon/business/csv/css/ExportImport.css"
	rel="Stylesheet" type="text/css" />
<script type="text/javascript"
	src="${ctx}/hollybeacon/resources/plugins/jqueryform/2.8/jquery.form.js">
</script>
<script type="text/javascript">
	var ctx = "${ctx}";
	var importActionUrl = "${ctx}/rest/${param.module}/import";
	var callBackFun = '${ empty param.callBackFun  ? "closeImportWin" :  param.callBackFun}' ;
</script>
<script type="text/javascript" src="${ctx}/hollysqm/common/import/js/import_plan.js"> </script>
</head>

<body style="padding: 10px;">

	<div class="iform-horizontal lable-v" style="width: 300px;">
		<form id="form1" action="${ctx}/rest/${param.module}/import" method="POST"
			enctype="multipart/form-data">
			<c:forEach items="${param}" var="data">
				<input type='hidden' name="${data.key}" value="${data.value}" />
			</c:forEach>
			<div class="icontrol-group">
				<div class="icontrol-label">导入文件 <span style="color:#ff0000;">目前只支持单个Sheet导入</span></div>
			</div>
			<div class="icontrol-group">
				<div class="icontrols">
					<input class="easyui-filebox" name="file" id="file" data-options="width:'100%'">
				</div>
			</div>
			<div class="icontrol-group">
				<div class="icontrol-label">
					<c:if test="${empty param.templateUrl}"><a href="${ctx}/rest/csv/template/download?type=excel&tableId=${param.tableId}">excel导入模板下载</a></c:if>
				</div>
			</div>

		</form>
	</div>

	<div class="easyui-panel" title="导入字段说明">
		<table class="easyui-datagrid" data-options="
		singleSelect:true,
		border:false,
		fitColumns:true">
			<thead>
				<tr>
					<th data-options="field:'name',width:100">字段名称</th>
					<!-- <th   data-options="field:'type',width:100">字段类型</th>
					<th   data-options="field:'options',width:100">转化处理</th> -->
					<th data-options="field:'desc',width:200 ">字段描述</th>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="field" items="${fields}" varStatus="loop">
					<tr>
						<td>${field.name}</td>
						<%-- <td  >${field.type}</td>
						<td  >${field.format}</td> --%>
						<td>${field.desc}
						<c:if test='${field.required}'>(必填)</c:if>
						<c:if test='${field.type=="number"}'>(数值型)</c:if>
						<c:if test='${field.type=="date"}'>格式:yyyy-MM-dd</c:if>
						<!-- <c:if test='${field.maxLength!="2147483647"}'>最大长度(${field.maxLength})</c:if> -->
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>


</body>
</html>