
var options = {

		dataType : "json",
		success : function(data) {
		if (data.success) {
			var totalCnt = data.content.totalCnt;
			var successCnt = data.content.successCnt;
			//var filename = data.content.filename ;
			var html = "记录总数" + totalCnt + "条,导入成功"
					+ successCnt + "条！";
			/*if (successCnt != totalCnt) {
				var url = ctx+"/rest/csv/invalid/download?filename="+filename;
				html = html + "<a href='"+url+"'>下载导入失败的数据</a>";
			}*/
			$('#file').textbox('setValue','');
			$.messager.progress('close');
			$.messager.alert('导入结果', html,'info',function(){
				if(parent[callBackFun])
					parent[callBackFun]();
			});
		} else {
			$.messager.progress('close');
			holly.showError(  data.errorMessage );
		}

	}
};

function exportInValidRecord() {
	var url = ctx+"/rest/csv/exportInValidRecord";
	$('#form1').attr("action", url);
	$('#form1').submit();
}

//外部调用
function importData() {
	var regExcel = new RegExp(".xls$");
	var fileName = 	$('#file').textbox('getValue');
	if(fileName==''||!regExcel.test(fileName)){
		holly.showError('未选择Excel文件');
		return ;
	}

	if(fileName==''||!regExcel.test(fileName)){
		holly.showError('文件格式不对');
		return ;
	}
	
	$.messager.progress();
	$('#form1').attr("action", importActionUrl);
	$("#form1").ajaxSubmit(options);
}

$(document) .ready( function() {
	// ajaxForm
	$("#form1").ajaxForm(options);

	// ajaxSubmit
	$("#btnAjaxSubmit").click(function() {
		importData();
	});
});
