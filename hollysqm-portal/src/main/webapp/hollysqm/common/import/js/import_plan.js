
var options = {

		dataType : "json",
		success : function(data) {
		if (data.success) {
			//var html = "导入成功 ";
			$.messager.progress('close');
			$('#file').textbox('setValue','');
			$.messager.alert('导入结果', data.content.msg,'info',function(){
				if(parent[callBackFun])
					parent[callBackFun](data.content);
			});
		} else {
			$.messager.progress('close');
			holly.showError(  data.errorMessage );
		}

	}
};

function exportInValidRecord() {
	var url = ctx+"/rest/csv/exportInValidRecord";
	$('#form1').attr("action", url);
	$('#form1').submit();
}

//外部调用
function importData() {
	var regExcel = new RegExp(".xls$");
	var fileName = 	$('#file').textbox('getValue');
	if(fileName==''||!regExcel.test(fileName)){
		holly.showError('未选择Excel文件');
		return ;
	}

	if(fileName==''||!regExcel.test(fileName)){
		holly.showError('文件格式不对');
		return ;
	}
	
	$.messager.progress();
	$('#form1').attr("action", importActionUrl);
	$("#form1").ajaxSubmit(options);
}

$(document) .ready( function() {
	// ajaxForm
	$("#form1").ajaxForm(options);

	// ajaxSubmit
	$("#btnAjaxSubmit").click(function() {
		importData();
	});
});
