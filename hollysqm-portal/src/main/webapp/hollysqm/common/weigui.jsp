<%@page import="com.hollycrm.hollybeacon.basic.util.AppConfigUtils"%>
<div class="heading">
	<i class="icon"></i>
	<div class="title">违规展示</div>
</div>
<div class="contentpane" style="padding:15px;">
	<div class="iform-horizontal column-3 labelmin iform-text" id="Illegalshow">
		<table style="width: 100%">
			<tr>
				<th style="width: 50%">违规项</th>
				<th style="width: 25%">是否通过</th>
				<th style="width: 25%">定位提示</th>
			</tr>
			<!-- ----------------------------- -->
			<tr>
				<td id="name_01">业务类》快静音超长未关怀</td>
				<td id="isRule_01"><img src="../../common/images/cha.png" style="width: 30px;"></td>
				<td id="occurRows_01"></td>
			</tr>
			<!-- ----------------------------- -->
			<tr>
				<td id="name_02">业务类》无声电话处理违规</td>
				<td id="isRule_02" style=""><img src="../../common/images/cha.png" style="width: 30px;"/></td>
				<td id="occurRows_02"></td>
			</tr>
			<!-- ----------------------------- -->
			<tr>
				<td id="name_03">业务类》情绪异常</td>
				<td id="isRule_03"><img src="../../common/images/cha.png" style="width: 30px;"></td>
				<td id="occurRows_03"></td>
			</tr>
			<!-- ----------------------------- -->
			<tr>
				<td id="name_04">语速类》语速过快</td>
				<td id="isRule_04"><img src="../../common/images/cha.png" style="width: 30px;"></td>
				<td id="occurRows_04"></td>
			</tr>
			<!-- ----------------------------- -->
			<tr>
				<td id="name_05">语速类》抢话</td>
				<td id="isRule_05"><img src="../../common/images/cha.png" style="width: 30px;"></td>
				<td id="occurRows_05"></td>
			</tr>
			<!-- ----------------------------- -->
			<tr>
				<td id="name_06">服务类》未及时响应客户需求</td>
				<td id="isRule_06"><img src="../../common/images/cha.png" style="width: 30px;"></td>
				<td id="occurRows_06"></td>
			</tr>
			<!-- ----------------------------- -->
			<tr>
				<td id="name_07">服务类》使用服务忌语</td>
				<td id="isRule_07"><img src="../../common/images/cha.png" style="width: 30px;"></td>
				<td id="occurRows_07"></td>
			</tr>
			<!-- ----------------------------- -->
			<tr>
				<td id="name_08">服务类》开头/结束语不规范</td>
				<td id="isRule_08"><img src="../../common/images/cha.png" style="width: 30px;"></td>
				<td id="occurRows_08"></td>
			</tr>
			<!-- ----------------------------- -->
			<tr>
				<td id="name_09">承诺类》下发短信</td>
				<td id="isRule_09"><img src="../../common/images/cha.png" style="width: 30px;"></td>
				<td id="occurRows_09"></td>
			</tr>
			<!-- ----------------------------- -->
			<tr>
				<td id="name_10">承诺类》问题登记</td>
				<td id="isRule_10"><img src="../../common/images/cha.png" style="width: 30px;"></td>
				<td id="occurRows_10"></td>
			</tr>
			<!-- ----------------------------- -->
		</table>
	</div>
</div>
<%
%>
<script type="text/javascript">
	$(function(){
		var id = holly.getUrlParams("contactId");
		if(!id){
			id = "<%=request.getParameter("contactId")%>";
		}
		if(!id || id == "null"){
			holly.showError("接触记录ID为空");
			return false;
		}
		init_weigui(id);
	});
	/*违规语音查询*/
	function init_weigui(id){
		holly.get(holly.getPath()+"/rest/commonRest/illegalShow",{"id":id},function(e){
			if(e.success){
				var d = eval("("+e.content+")");
				$.each(d,function(index,val){
					var imgUrl = val.isRule?"../../common/images/gou.png":"../../common/images/cha.png";
					$("#isRule_"+val.code+" img").attr("src",imgUrl);
					$("#occurRows_"+val.code).text((val.occurRows == null || val.occurRows.length==0)?"":("第"+val.occurRows.join("、")+"行"));//obj.join("")
				});
			}else{
				alert("查询违规语音定位出错");
			}
		});
	}
	
</script>