var  scorelist= {
	/**
	 * 初始化加载数据
	 */
	init:function(){
		$("#scorelistdatagrid").datagrid({
				fit:true,
				url:holly.getPath()+'/rest/scoreList/showListRest',
				method:'get',
				singleSelect : true, 
				border:false,
				fitColumns:true,
				pagination: true,
				loadFilter: function(data){return (data.content)?data.content:data;},
				columns:[[
					{field:'itemName',title:'评分名称',width:100},
					{field:'score',title:'总分',width:30},
					{field:'scoreType',title:'评分类型',width:60,formatter:scorelist.formatScoreType},
					{field:'itemType',title:'项目分类',width:60,formatter:scorelist.formatItemType},
					{field:'modifier',title:'最后修改人',width:60},
					{field:'modifyTime',title:'最后修改时间',width:80},
					{field:'option',title:'操作',width:60,formatter:scorelist.formatOption}
				]],
				loadMsg:'数据加载中请稍后……',
				onLoadSuccess: function (data)  {
					if (data.rows.length == 0) {
						var body = $(this).data().datagrid.dc.body2;
						body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
					}
				}
			});	
	},

	
	/**
	 * 搜索
	 */
	search:function(){
		strReplace("itemName");
		var param = holly.form2json($("#searchScorelist"));
		$("#scorelistdatagrid").datagrid("search",{'param':param});
	},
	// 评分项类型
	formatScoreType:function(val, row, index) {
		return val == "0" ?"加分项":"减分项";
	},
	// 项目分类
	formatItemType:function(val, row, index) {
		return val == "0" ?"服务质量类":"业务能力类";;
	},

	// 最后修改人
	formatModifier:function(val,row,index){
		return val?"<span title='"+row.modifierName + "(" +val+ ")"+"'>"+row.modifierName + "(" + val+ ")"+"</span>":"";
	},
	// 操作
	formatOption:function(val,row,index){
		var content = '';
			content += "<span class='grid-opt-wrap'>";
			content	+= "<a href='javascript:void(0)' onclick=\"scorelist.edit('"+row.scoreListId+"')\" class='grid-optcon icon-edit' title='编辑'></a>";
			content	+= "<a href='javascript:void(0)' onclick=\"scorelist.del('"+row.scoreListId+"')\" class='grid-optcon icon-remove' title='删除'></a>";
			content += "</span>";
		return content;
	},
	
	saveOrUpdate : function(){
		$(".warnSpan").hide();
		$("#modifier").val(loginUser.userName+"("+loginUser.userCode+")");
		$("#modifyTime").val(holly.currentTime());
		var form = holly.form2json($("#ff"));
		var scoreListId = form.scoreListId;
		var flag = scorelist.checkModify(scoreListId);
		if(scoreListId && !flag){
			if(scoreListData.score != form.score){//总分
				$(".warnSpan").eq(0).addClass("red").show();
			}

			if(scoreListData.scoreType != form.scoreType){//评分类型
				$(".warnSpan").eq(1).addClass("red").show();
			}

			if(scoreListData.itemType != form.itemType){//项目类型
				$(".warnSpan").eq(2).addClass("red").show();
			}
			return false;
		}
		var vaild = $("#ff").form('enableValidation').form('validate');
		if(vaild){
			$.messager.confirm('提示', '确定提交吗？', function(r){	
				if(r){
					holly.post(holly.getPath()+"/rest/scoreList/saveOrUpdateRest",form,function(e){
						if(e.success){
							holly.showSuccess("操作成功!");
							$("#scorelistdatagrid").datagrid('reload');
							$('#dlg1').dialog('close');
						}else{
							holly.showError(e.errorMessage);
						}
					});
				}
			});
		}
	},
	edit : function(id){
		$(".warnSpan").hide();
		holly.get(holly.getPath()+"/rest/scoreList/showUpdateRest",{"id":id},function(e){
			if(e.success){
				$('#ff').form('clear');
				with(e){
					scoreListData = content;
					$("#ff").form('load',content);
				}
				$('#dlg1').dialog({'title':'修改评分模板'}).dialog('open');
			}else{
				holly.showError(e.errorMessage);
			}
		});
	},
	del : function(id){
		var flag = scorelist.checkModify(id);
		if(!flag){
			$.messager.alert('提示', '该评分项已被使用，禁止删除！');
			return false;
		}
		$.messager.confirm('提示', '确定删除该评分项吗？', function(r){	
			if(r){
				holly.get(holly.getPath()+"/rest/scoreList/deleteRest",{"id":id},function(e){
					if(e.success){
						$("#scorelistdatagrid").datagrid('reload');
						holly.showSuccess("操作成功！");
					}else{
						holly.showError(e.errorMessage);
					}
				});
			}
		});
	},
	resetFFForm : function(){//重置评分项模板form表达那
		$(".warnSpan").hide();
		$('#ff').form('reset');
		$('#dlg1').dialog({'title':'新建评分项模板'}).dialog('open');
	},
	checkModify : function(id){
		var flag = null;
		holly.get(holly.getPath()+"/rest/scoreList/checkScoreList",{"id":id},function(e){
			if(e.success){
				flag = e.content;
			}else{
				holly.showError(e.errorMessage);
				flag = false;
			}
		},true);
		return flag;
	}
};
var scoreListData = null;
/**
*	过滤特殊字符
*	str：要进行过滤的字符串
*/
function strReplace(id){
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
	var rs = "";
	var str = $("#"+id).textbox("getValue");
	if($.trim(str).length > 0 ){
		for (var i = 0; i < str.length; i++) {
			rs = rs+str.substr(i, 1).replace(pattern, '');
		}
		$("#"+id).textbox("setValue",rs);
	}
} 