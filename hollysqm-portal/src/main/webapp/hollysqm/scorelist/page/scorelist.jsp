<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>评分项管理</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<link rel="stylesheet" href="${ctx}/hollysqm/scorelist/css/scoelist.css">
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/switch.css">
<script type="text/javascript" src="../js/scorelist.js" ></script>
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type="text/javascript">
var loginUser = ${loginUser};
$(function() {
	scorelist.init();
	
	$("#searchScorelist").keyup(function(event){
		if(event.keyCode==13){
			scorelist.search();
		}
	});
});
</script>
</head>
<body class="easyui-layout">
	<div data-options="region:'center',border:false" style="padding:10px;">
		<div class="easyui-layout" data-options="fit:true">
			<!-- 搜索区域 -->
			<div data-options="region:'north',border:false" style="padding-bottom:10px;">
				<form id="searchScorelist" method="post" class="search-form">
					<div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">评分名称:</span>
							<div class="valueGroup">
								 <input class="easyui-textbox" name="itemName" id="itemName" data-options="width:'100%'" >		
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">评分类型:</span>
							<div class="valueGroup">
								<select name="scoreType" class="easyui-combobox" data-options="editable:false,width:'100%'">
									<option value="">全部</option>
									<option value="0">加分项</option>
									<option value="1">减分项</option>
								</select>	
							</div>
						</span>	
					</div>
					<div class="btn-group">
						<a href="javascript:;" class="operation search-btn" onclick="scorelist.search()" title="搜索"></a>
						<a href="javascript:;" class="operation reset-btn" onclick="$('#searchScorelist').form('reset');" title="重置"></a>
					</div>
				</form>
			</div>
			<!-- 搜索区域 end -->			
			<div data-options="region:'center',border:false">
				<div id="tt">
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="scorelist.resetFFForm()">新建</a> 
				</div>
				<div class="easyui-panel" title="评分项模板" data-options="tools:'#tt',fit:true">
					<table id="scorelistdatagrid" ></table>
				</div>
			</div>
		</div>
	</div>

	<div id="dlg1" class="easyui-dialog" title="新建评分项模板" style="width: 80%; height:90%;padding: 10px" data-options="iconCls:'icon-exclamation-sign',closed:true,buttons:'#dlg-buttons1',onOpen:function(){$(this).scrollTop(0)}">
		<form id="ff" method="post" data-options="novalidate:true">
			<input type="hidden" name="scoreListId" id="scoreListId"/>
			<input type="hidden" name="modifier" id="modifier"/>
			<input type="hidden" name="modifyTime" id="modifyTime"/>
			
			<div class="dlg_bar clearfix">
				<span class="nam"><span class="red">*</span>评分名称</span>
				<div class="dlg_input">
					 <input class="easyui-textbox" name="itemName"  data-options="width:'50%',novalidate:true,required:true,validType:'length[1,100]'" >
				</div>
			</div>
			<div class="dlg_bar clearfix">
				<span class="nam"><span class="red">*</span>总分</span>
				<div class="dlg_input">
					 <input class="easyui-numberbox" name="score"  data-options="width:'50',novalidate:true,required:true,min:1,max:100" >
					 <span class="warnSpan">此评分项已使用，禁止修改总分</span>
				</div>
			</div>
			<div class="dlg_bar clearfix">
				<span class="nam"><span class="red">*</span>评分类型</span>
				<div class="dlg_input">
					 <select class="easyui-combobox" name="scoreType"  data-options="width:'50%',novalidate:true,required:true,editable:false" >
					 	<option value="0">加分项</option>
					 	<option value="1">减分项</option>
					 </select>
					 <span class="warnSpan">此评分项已使用，禁止修改评分类型</span>
				</div>
			</div>
			<div class="dlg_bar clearfix">
				<span class="nam"><span class="red">*</span>项目分类</span>
				<div class="dlg_input">
					 <select class="easyui-combobox" name="itemType"  data-options="width:'50%',novalidate:true,required:true,editable:false" >
					 	<option value="0">服务质量类</option>
					 	<option value="1">业务能力类</option>
					 </select>
					 <span class="warnSpan">此评分项已使用，禁止修改项目分类</span>
				</div>
			</div>
			<div class="dlg_bar clearfix">
				<span class="nam">备注</span>
				<div class="dlg_input">
					 <input class="easyui-textbox" name="remark"  data-options="width:'50%',validType:'length[1,100]',multiline:true,height:44" >
				</div>
			</div>
			
			
		</form>
		<div id="dlg-buttons1">
			<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red"onclick="scorelist.saveOrUpdate()">保存</a> 
			<a href="javascript:void(0)" class="easyui-linkbutton"
			onclick="$('#dlg1').dialog('close')">取消</a>
		</div>
	</div>
</body>
</html>