var recordtype = {
	/**
	 * 初始化加载数据
	 */
	init:function(){

		$("#formDataType").combobox("init_extend",{'codeType':'DATATYPE','hasAllOption':false,event:{
			onLoadSuccess:function(){
				$(this).combobox('setValue','v8');
			}
		}});
		recordtype.planNameOption();
		$("#caseItem").combobox("init_extend",{'codeType':'CASE_ITEM'});
		$("#recordtypedatagrid").datagrid({
				fit:true,
				url:holly.getPath()+'/rest/paperscore/queryPaperCase',
				method:'get',
				singleSelect : true,
				border:false,
				fitColumns:true,
				pagination: true,
				queryParams:{'paperType':'v8'},
				loadFilter: function(data){return (data.content)?data.content:data;},
				columns:[[
					{field:'paperTypeName',title:'质检来源',width:60},
					{field:'planName',title:'质检计划',formatter:recordtype.formatPlanName},
					{field:'caller',title:'来电号码',width:60},
					{field:'agentCodeName',title:'坐席',width:80},
					{field:'modifyTime',title:'质检时间',width:100},
					{field:'createCodeName',title:'质检员',width:80},
					{field:'caseItem',title:'案例类型',width:80,formatter:recordtype.formatDataTyp},
					{field:'paperId',title:'操作',width:80,formatter:recordtype.formatOption}
				]],
				loadMsg:'数据加载中请稍后……',
				onLoadSuccess: function (data)  {
					if (data.rows.length == 0) {
						$("#exportTable").hide();//没有数据时隐藏导出按钮
						var body = $(this).data().datagrid.dc.body2;
						body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
					}
				}
			});	
	},

	
	/**
	 * 搜索
	 */
	search:function(){
		var param = holly.form2json($("#searchrecordtype"));
		$("#recordtypedatagrid").datagrid("search",{'param':param});
	},
	formatPlanName : function(val,row,index){//质检计划名称
        if(val == undefined || val =='' || val == null || val=='null') {
            val = '全文检索';
        }
        return '<span title='+val+'>'+val+'</span>';
	},
	// 案例类型
	formatDataTyp:function(val, row, index) {
		var res =caseItemMap[val]; //获取对应的数据
		return res;
	},
	// 操作
	formatOption:function(val,row,index){
		var content = '';
			content += "<span class='grid-opt-wrap'>";
			content	+= "<a href='javascript:void(0)' onclick=\"recordtype.sqminfo('"+val+"','"+row.planId+"')\" class='grid-optcon icon-eye-open' title='查看'></a>";
			content	+= "<a href='javascript:void(0)' onclick=\"recordtype.deleteCase('"+row.caseId+"')\" class='grid-optcon icon-remove' title='删除'></a>";
			content += "</span>";
		return content;
	},
	sqminfo : function(paperId,contactId){
		var url = holly.getPath()+"/hollysqm/paperscore/page/dlg-sqminfo.jsp";
		url += "?paperId=" + paperId+"&contactId="+contactId;
		$('#dlg-sqminfo').dialog('open').dialog('refresh',url);
	},
	deleteCase : function(caseId){
		$.messager.confirm('提示', '是否确定删除该典型案例？', function(r){
			if(r){
				holly.post(holly.getPath()+"/rest/paperscore/deleteCase",{"caseId":caseId},function(e){
					if(e.success){
						holly.showSuccess("操作成功!");
						$("#recordtypedatagrid").datagrid('reload');
					}else{
						holly.showError(e.errorMessage);
					}
				});
			}
		});
		//deleteCase
	}, 
	planNameOption : function(){
		$('#formplanName').combobox({
		    url : holly.getPath() + '/rest/commonRest/queryPlan',
		    valueField : 'value', 
			textField : 'name',
			loadFilter: function(data){
				var jsonArray=new Array();
//				var defaultVal = {"value":"","name":'全部'};
//				jsonArray.push(defaultVal);
				for(var o in data.content){
					jsonArray.push(data.content[o]);
				}
				return jsonArray;
			},onHidePanel : function(){
				var el = $(this);
				var value = el.combobox("getValue");
				if(!value)//如果没有匹配到值，则默认设置成空
					 el.combobox("setValue","");
			}
		});  
	},
	exportPaper : function(toPage){
		if(toPage){
			var url =holly.getPath()+"/rest/csv/wizard/sqlexport?tableId=csv.score.queryAgentScore&sqlKey=velocity.score.queryAgentScore&mediaId=1";
			var targetIframe ="iframe-export";
			$('#searchrecordtype').attr("action",url);
			$('#searchrecordtype').attr("target",targetIframe);
			$('#searchrecordtype').submit();
			$('#dlg-export').dialog('open');
		}else{
			$("#iframe-export")[0].contentWindow.exportData();
			$('#dlg-export').dialog('close');
		}
	},
	resetForm : function() {
		$('#searchrecordtype').form('reset');

		var data = $('#formDataType').combobox('getData');
		$('#formDataType').combobox('select', data[0].value);// 默认选中第一个

	}
};
 