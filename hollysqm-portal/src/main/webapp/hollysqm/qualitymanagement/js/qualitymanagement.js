var unChecked = {
	/**
	 * 初始化加载数据
	 */
	init:function(){ 

		$("#formDataType").combobox("init_extend",{'codeType':'DATATYPE','hasAllOption':false,"event":{
			onLoadSuccess:function(){
				unChecked.dataTypeData = $(this).combobox('getData');
				$(this).combobox('setValue',unChecked.dataTypeData[0].value);
			}
		}});
		$("#appealStatus").combobox("init_extend",{'codeType':'APPEAL_STATUS','hasAllOption':true});
		
		$("#formPlanId").combobox({
			url:holly.getPath()+'/rest/commonRest/queryPlan',
			valueField:'value',
			textField:'name',
			method:"post",
			loadFilter:function(data){
				var jsonArray=new Array();
				for(var o in data.content){
					jsonArray.push(data.content[o]);
				}
				return jsonArray;
			},onHidePanel : function(){
				var el = $(this);
				var value = el.combobox("getValue");
				if(!value)//如果没有匹配到值，则默认设置成空
					 el.combobox("setValue","");
			}
		});
		var param = holly.form2json($("#searchunChecked"));
		param["dataType"] = "v8";//设置默认值
		$("#unCheckeddatagrid").datagrid({
			fit:true,
			url:holly.getPath()+'/rest/qualitymanagement/getQualitymanagementList',
			method:'get',
			singleSelect : true,
			border:false,
			fitColumns:true,
            queryParams:param,
			pagination: true,
			loadFilter: function(data){return (data.content)?data.content:data;},
			columns:[[
				{field:'dataType',title:'质检来源',width:60,formatter:unChecked.formatDataTyp},
				{field:'planName',title:'质检计划',width:100,formatter:unChecked.formatPlanName},
				{field:'caller',title:'来电号码',width:60},
				{field:'appealStatus',title:'申诉状态',width:80,formatter:unChecked.formatAppealStatus},
				{field:'agentName',title:'坐席',width:80},
				{field:'checkerName',title:'质检员',width:80},
				{field:'totalScore',title:'评分得分',width:60},
				{field:'appealTime',title:'申诉时间',width:80},
				{field:'option',title:'操作',width:60,formatter:unChecked.formatOption}
			]],
			loadMsg:'数据加载中请稍后……',
			onLoadSuccess: function (data)  {
				if (data.rows.length == 0) {
					$("#exportTable").hide();//没有数据时隐藏导出按钮
					var body = $(this).data().datagrid.dc.body2;
					body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
				}else $("#exportTable").show();
			}
		});
		param["dataType"] = "i8";//设置默认值
		$("#unCheckeddatagrid_i8").datagrid({
			fit:true,
			method:'get',
			singleSelect : true,
			border:false,
			fitColumns:true,
			pagination: true,
			queryParams:param,
			closed:true,
			loadFilter: function(data){return (data.content)?data.content:data;},
			columns:[[
				{field:'dataType',title:'质检来源',width:60,formatter:unChecked.formatDataTyp},
				{field:'planName',title:'质检计划',width:100,formatter:unChecked.formatPlanName},
				{field:'caller',title:'来电号码',width:60},
				{field:'appealStatus',title:'申诉状态',width:80,formatter:unChecked.formatAppealStatus},
				{field:'agentName',title:'坐席',width:80},
				{field:'checkerName',title:'质检员',width:80},
				{field:'totalScore',title:'评分得分',width:60},
				{field:'appealTime',title:'申诉时间',width:80},
				{field:'closeType',title:'关闭方式',width:80,formatter:unChecked.formatCloseType},
				{field:'sessionType',title:'会话等级',width:80,formatter:unChecked.formatSessionType},
				{field:'option',title:'操作',width:60,formatter:unChecked.formatOption}
			]],
			loadMsg:'数据加载中请稍后……',
			onLoadSuccess: function (data)  {
				if (data.rows.length == 0) {
					$("#exportTable").hide();//没有数据时隐藏导出按钮
					var body = $(this).data().datagrid.dc.body2;
					body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
				}else $("#exportTable").show();
			}
		});

		//回设默认值为V8
        param["dataType"] = "v8";//设置默认值
    },

	
	/**
	 * 搜索
	 */
	search:function(){
		var param = holly.form2json($("#searchunChecked"));
		if(param.dataType=='v8'){
			if($("#unCheckeddatagrid").datagrid('getPanel').panel('options').closed){
				$("#unCheckeddatagrid_i8").datagrid('getPanel').panel('close');
				$("#unCheckeddatagrid").datagrid('getPanel').panel('open').panel('resize');
			}
			$("#unCheckeddatagrid").datagrid("search",{'param':param});
		}else if(param.dataType=='i8'){
			$("#unCheckeddatagrid_i8").datagrid('options').url = holly.getPath()+'/rest/qualitymanagement/getQualitymanagementList';
			if($("#unCheckeddatagrid_i8").datagrid('getPanel').panel('options').closed){
				$("#unCheckeddatagrid").datagrid('getPanel').panel('close');
				$("#unCheckeddatagrid_i8").datagrid('getPanel').panel('open').panel('resize');
			}
			$("#unCheckeddatagrid_i8").datagrid("search",{'param':param});
		}
	},
	formatPlanName : function(val,row,index){//质检计划名称
        if(val == undefined || val =='' || val == null || val=='null') {
            val = '全文检索';
        }
        return '<span title='+val+'>'+val+'</span>';
	},
	// 质检来源
	formatDataTyp:function(val, row, index) {
		var res =dataTypeMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	},
	formatCloseType:function(val, row, index){
		var res = closeTypeMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	},
	formatSessionType:function(val, row, index){
		var res = sessionTypeMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	},
	formatAppealStatus:function(val, row, index){
		var res = appealstatusMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	},

	// 操作
	formatOption:function(val,row,index){
		var	content	= "";
		if(row.appealStatus =="3" && loginUser.userCode == row.executeCode){//申诉状态为待处理，申诉处理人为当前登陆人
			content = "<a href='javascript:void(0)' onclick=\"unChecked.jumpUrl('"+row.paperId+"','"+row.paperDetailId+"','"+row.appealId+"','复核')\" title='复核'>复核</a>";
		}else{
			content = "<a href='javascript:void(0)' onclick=\"unChecked.jumpUrl('"+row.paperId+"','"+row.paperDetailId+"','"+row.appealId+"','查看')\" title='查看'>查看</a>";
		}
		return content;
	},	
	// 坐席
	formatModifier:function(val,row,index){
		//return val?"<span title='"+row.modifierName + "(" +val+ ")"+"'>"+row.modifierName + "(" + val+ ")"+"</span>":"";
		
		if(row.agentName == null || row.userCode == null) {
			return "";
		} else {
			return "<span>" + row.agentName + "(" + row.userCode + ")</span>";
		}
	},
	// 质检员
	formatChecker:function(val,row,index){
		//return val?"<span title='"+row.checkerName + "(" +val+ ")"+"'>"+row.checkerName + "(" + val+ ")"+"</span>":"";
		
		if(row.checkerName == null || row.checkerCode == null) {
			return "";
		} else {
			return "<span>" + row.checkerName + "(" + row.checkerCode + ")</span>";
		}
	},
	
	jumpUrl:function(paperId,paperDetailId,appealId,title){
		var param = holly.form2json($("#searchunChecked"));
		var frameId = window.frameElement && window.frameElement.id || '';
		var url = holly.getPath()+'/hollysqm/qualitymanagement/page/check.jsp';
		url += '?dataType='+param.dataType;
		url += '&agent='+param.agent;
		//url += '&checker='+param.checker;
		url += '&endTime='+param.endTime;
		url += '&planId='+param.planId;
		url += '&startTime='+param.startTime;
		url += '&paperId='+paperId;
		url += '&contactId='+paperDetailId;
		url += '&appealId='+appealId;
		url += '&status='+param.status;
		url += '&frameId=' + frameId;
		// parent.main.addTab('iframequalitymanagement',url,title);
		window.location = url;
	},
	exportPaper : function(toPage){
		var formDataType = $("#formDataType").combobox("getValue");
		var grid = $('#unCheckeddatagrid');  
		if(formDataType == "i8")
			grid = $('#unCheckeddatagrid_i8');  
		var options = grid.datagrid('getPager').data("pagination").options;  
		var total = options.total;
		if(total > export_total){
			$.messager.alert("提示","导出数据不能大于"+export_total+"，请继续筛选数据！");
			return false;
		}
		if(toPage){
			var url =holly.getPath()+"/rest/csv/wizard/sqlexport?tableId=csv.score.queryQuality_"+formDataType+"&sqlKey=velocity.score.queryQuality&mediaId=1";
			var targetIframe ="iframe-export";
			$('#searchunChecked').attr("action",url);
			$('#searchunChecked').attr("target",targetIframe);
			$('#searchunChecked').submit();
			$('#dlg-export').dialog('open');
		}else{
			$("#iframe-export")[0].contentWindow.exportData();
			$('#dlg-export').dialog('close');
		}
	},
	resetForm : function(){
		$('#searchunChecked').form('reset');
		$('#formDataType').combobox('select',unChecked.dataTypeData[0].value);//默认选中第一个
	}

};
  