var isShow = false;
var check = {
    /**
     * 初始化加载数据
     */
    paramgetUnscoredPaperMsg: {
        "dataType": holly.getUrlParams("dataType") ? holly.getUrlParams("dataType") : "",//质检来源
        "agent": holly.getUrlParams("agent") ? holly.getUrlParams("agent") : "",//坐席
        //"checker":holly.getUrlParams("checker")?holly.getUrlParams("checker"):"",
        "endTime": holly.getUrlParams("endTime") ? holly.getUrlParams("endTime") : "",//结束时间
        "planId": holly.getUrlParams("planId") ? holly.getUrlParams("planId") : "",//质检计划ID
        "startTime": holly.getUrlParams("startTime") ? holly.getUrlParams("startTime") : "",//开始时间
        "paperId": holly.getUrlParams("paperId") ? holly.getUrlParams("paperId") : "",//质检单ID
        "appealId": holly.getUrlParams("appealId") ? holly.getUrlParams("appealId") : "",//申诉ID
        "status": holly.getUrlParams("status") ? holly.getUrlParams("status") : ""//申诉状态
    },
    contactId: holly.getUrlParams("contactId") ? holly.getUrlParams("contactId") : "",//接触ID
    frameId: holly.getUrlParams("frameId") ? holly.getUrlParams("frameId") : "",//上一frameID
    thisIframeId: window.frameElement && window.frameElement.id || '',// 获取该页面iframe id
    totalScoreReal: 0,
    init: function () {
        $("#caseItem").combobox("init", {'codeType': 'CASE_ITEM'});
        $("#errorType").combobox("init", {'codeType': 'ERROR_TYPE'});
        $("#formserviceType").combobox("init", {'codeType': 'PAPER_SERVICE_TYPE'});
        //check.servicetypeOption();
        var param = {
            "dataType": holly.getUrlParams("dataType") ? holly.getUrlParams("dataType") : "",
            "paperId": holly.getUrlParams("paperId") ? holly.getUrlParams("paperId") : ""
        };
        check.getItemScoreList("1", "#scoreitemlistText");//加载评分信息
        // 查询申诉信息
        holly.get(holly.getPath() + "/rest/qualitymanagement/getQualitymanagement", check.paramgetUnscoredPaperMsg, function (e) {
            if (e.success) {
                with (e.content) {
                    if (loginUser.userCode == executeCode && executeime == null) {
                        isShow = true;
                        $("#reshenhePaperDiv1").show();
                        check.initCheck();
                        holly.renderEasyUI($("#scoreForm").children());
                        /*$("#caseItem").combobox();
                         $("#comments").textbox();
                         $("#errorType").combobox();
                         $("#formserviceType").combobox();*/
                    } else if (status == "1") {
                        $("#reviewContainer").show();
                        check.getItemScoreList("2", "#scoreitemlistText2");
                    }
                }
            }
        }, true);
        if (check.thisIframeId == 'iframequalitymanagement' && isShow) {//如果该页面从待复核列表打开
            // 其他待复核
            holly.post(holly.getPath() + "/rest/qualitymanagement/nextQualitymanagement", check.paramgetUnscoredPaperMsg, function (e) {
                if (e.success) {
                    if (e.content) {
                        $('#paperCount').html(e.content.paperCount + '条');
                        $('#paperCountText').html('提交后自动显示下一质检单');
                        var vals = e.content.nextPaperId.split(",");
                        check.nextPaperId = vals[0];
                        check.nextAppealId = vals[1];
                    }
                }
            });
        }
        //开始加载申诉列表
        holly.post(holly.getPath() + "/rest/qualitymanagement/getQualitymanagementPaper", check.paramgetUnscoredPaperMsg, function (e) {
            if (e.success) {
                if (e.content) {
                    var html = "";
                    var length = e.content.length;
                    $.each(e.content, function (index, val) {
                        with (val) {
                            html += "<div class='icontrol-group' id='icontrol-group" + (index + 1) + "'>" +
                                "<div class='icontrol-label' style='width:90px;'>" + check.getSysUser(executeCode) + "(" + executeCode + ")" + "</div>" +
                                "<div class='icontrols icontrols-text'>&nbsp;&nbsp;" + (explain == null ? "" : explain) + "【" + appealStatusMap[status] + "】" + "<div style='text-align: right'>" + (executeime == null ? "&nbsp;" : executeime) + "</div>" + "</div>" +
                                "</div>";
                        }
                    });
                    $("#appealPaperDiv").html(html);
                    if (isShow) $("#appealPaperDiv #icontrol-group" + length).remove();

                }
            }
        });
        //结束加载申诉列表
        // 质检单评分查询
        holly.post(holly.getPath() + "/rest/commonRest/getPaperScoreResult", {"paperId": param.paperId}, function (e) {
            if (e.success) {
                if (e.content) {
                    $('#totalScoreDiv').html(e.content.totalScore + '分');//总分
                    $('#checkerInfoDiv').html(e.content.scorerName + '(' + e.content.scorerCode + ')');//质检人
                    $('#scoreTimeDiv').html(e.content.scoreTime);//评分时间
                    if (isShow) {
                        $('#comments').textbox('setValue', e.content.comments);//问题描述

                        if (e.content.needCorrect == "1") {//是否纠错
                            $('#needCorrect1').attr('checked', 'checked');
                        } else {
                            $('#needCorrect0').attr('checked', 'checked');
                        }

                        if (e.content.isFatal == "1") {//是否致命
                            $('#isFatal1').attr('checked', 'checked');
                        } else {
                            $('#isFatal0').attr('checked', 'checked');
                        }

                        if (e.content.errorType) {//差错类型
                            $("#errorType").combobox();
                            $("#errorType").combobox('setValue', e.content.errorType);
                        }

                        if (e.content.serviceType) {//业务类型
                            $('#formserviceType').combobox();
                            $("#formserviceType").combobox('setValue', e.content.serviceType);
                        }
                    } else {
                        $('#reviewScoreDiv2').html(e.content.reviewScore);
                        $('#checkerInfoDiv2').html(e.content.checkerName + '(' + e.content.checkerCode + ')');
                        $('#reviewTimeDiv').html(e.content.reviewTime);
                        $('#commentsDiv2').html(holly.replace_str(e.content.comments, false));
                        $('#errorTypeDiv2').html(errorTypeMap[e.content.errorType]);
                        $('#needCorrectDiv2').html(e.content.needCorrect == '0' ? '否' : '是');
                        $('#isFatalDiv2').html(e.content.isFatal == '0' ? '否' : '是');
                        $('#serviceTypeDiv').html(paperServiceTypeMap[e.content.serviceType]);
                        // 获取当前质检单绑定的典型案例
                        holly.post(holly.getPath() + "/rest/commonRest/getPaperCase", {"paperId": param.paperId}, function (e) {
                            if (e.success) {
                                if (e.content) {
                                    if (e.content.caseItem) {
                                        $('#caseItemDiv2').html(e.content.caseItem);//caseItemMap[]
                                        if (e.content.recordType == "1") {
                                            $('#recordTypeDiv2').html("是");
                                            $('#caseItemTip2').show();
                                        } else if (e.content.recordType == "2") {
                                            $('#recordTypeDiv2').html("否");
                                            $('#caseItemTip2').hide();
                                        }
                                    } else {
                                        $('#recordTypeDiv2').html("否");
                                        $('#caseItemTip2').hide();
                                    }
                                }
                            }
                        });
                        //加载业务类别 PAPER_SERVICE_TYPE
                        /*if(e.content.serviceType) {
                         holly.post(holly.getPath()+"/rest/commonRest/getServiceTypeNames", {serviceTypeId:e.content.serviceType},function(e1){
                         if(e1.success) {
                         if(e1.content) {
                         var content = e1.content;
                         var serviceType = content[0].text;
                         $('#serviceTypeDiv').html(serviceType);
                         }
                         }
                         });
                         }*/
                    }
                }
            }
        });

        holly.post(holly.getPath() + "/rest/commonRest/getRecord", param, function (e) {
            if (e.success) {
                var content = e.content;
                var html = '';
                var chatHtml = '';
                var arrFlag = [];
                var arrText = [];
                check.contactId = content.contactId;
                check.acceptTime = content.acceptTime;
                check.recordFile = content.recordFile;
                check.planId = content.planId;
                check.dataType = content.dataType;
                //显示接触记录绑定的质检计划信息
                if (content.planId) {
                    if (content.planId == 'system') {
                        $('#planDetail').html('所属质检计划:' + content.planName);
                    } else {
                        $('#planDetail').html('<a class="sublink" href="javascript:;" onclick="check.planinfo()">所属质检计划:' + content.planName + '</a>');
                    }
                }
                if (check.paramgetUnscoredPaperMsg.dataType == 'v8') {
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">来电号码</div>';
                    if (content.caller != null) {
                        html += '<div class="icontrols">' + content.caller + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">坐席</div>';
                    if (content.user != null) {
                        html += '<div class="icontrols">' + content.user.username + '(' + content.user.userCode + ')' + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">归属地</div>';
                    if (content.custArea != null) {
                        html += '<div class="icontrols">' + check.paramJson(content.custArea, 'CUST_AREA') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">品牌</div>';
                    if (content.custBand != null) {
                        html += '<div class="icontrols">' + check.paramJson(content.custBand, 'CUST_BAND') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">满意度</div>';
                    if (content.satisfaction != null) {
                        html += '<div class="icontrols">' + check.paramJson(content.satisfaction, 'V8SATISFACTION') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">业务类型</div>';
                    if (content.serviceType != null) {
                        html += '<div class="icontrols">' + check.paramJson(content.bussinessType, 'BUSINESS_TYPE') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">开始时间</div>';
                    if (content.startTime != null) {
                        html += '<div class="icontrols">' + check.formatTime(content.startTime) + '</div>';
                    }
                    html += '</div>';

                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">通话时长</div>';
                    if (content.length != null) {
                        html += '<div class="icontrols">' + content.length + '秒</div>';
                    }
                    html += '</div>';

                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">用户级别</div>';
                    if (content.custLevel != null) {
                        html += '<div class="icontrols">' + check.paramJson(content.custLevel, 'CUST_LEVEL') + '</div>';
                    }
                    html += '</div>';

                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">静音时长</div>';
                    if (content.custLevel != null) {
                        html += '<div class="icontrols">' + content.silenceLength + '秒</div>';
                    }
                    html += '</div>';

                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">服务请求</div>';
                    if (content.serviceType != null) {
                        //html += '<div class="icontrols">'+content.serviceType+'</div>';
                        holly.post(holly.getPath() + "/rest/commonRest/getServiceTypeNames", {"serviceTypeId": content.serviceType}, function (e1) {
                            if (e1.content) {
                                var array = e1.content;
                                if (array.length > 0) {
                                    html += '<div class="icontrols">' + array[0].text + '</div>';
                                }
                            }
                            html += '</div>';
                            $('#getRecordDiv').html(html);
                        });
                    } else {
                        html += '</div>';
                        $('#getRecordDiv').html(html);
                    }

                    $('#voicechatDiv').show();
                    // 接触记录图片音频地址查询
                    holly.post(holly.getPath() + "/rest/commonRest/getRecordDir", {
                        "contactId": check.contactId,
                        "acceptTime": check.acceptTime,
                        "recordFile": check.recordFile
                    }, function (e) {
                        if (e.success) {
                            listenTape.init(e.content.wavDir, e.content.imgDir);
                        } else {
                            toastr.error(e.errorMessage);
                        }
                    });
                } else if (check.paramgetUnscoredPaperMsg.dataType == 'i8') {
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">来话号码</div>';
                    if (content.caller != null) {
                        html += '<div class="icontrols">' + content.caller + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">坐席</div>';
                    if (content.user != null) {
                        html += '<div class="icontrols">' + content.user.username + '(' + content.user.userCode + ')' + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">关闭方式</div>';
                    if (content.closeType != null) {
                        html += '<div class="icontrols">' + check.paramJson(content.closeType, 'CLOSE_TYPE') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">会话等级</div>';
                    if (content.sessionType != null) {
                        html += '<div class="icontrols">' + check.paramJson(content.sessionType, 'SESSION_TYPE') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">满意度</div>';
                    if (content.satisfaction != null) {
                        html += '<div class="icontrols">' + check.paramJson(content.satisfaction, 'I8SATISFACTION') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">服务类型</div>';
                    if (content.serviceType != null) {
                        html += '<div class="icontrols">' + content.serviceType + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">开始时间</div>';
                    if (content.startTime != null) {
                        html += '<div class="icontrols">' + content.startTime + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">结束时间</div>';
                    if (content.endTime != null) {
                        html += '<div class="icontrols">' + content.endTime + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">会话时长</div>';
                    if (content.length != null) {
                        html += '<div class="icontrols">' + content.length + '秒</div>';
                    }
                    html += '</div>';
                }
                $('#getRecordDiv').html(html);

                arrFlag = content.txtContent.match(/n\d#/ig);
                arrText = content.txtContent.split(/n\d#/ig);
                for (var i = 0; i < arrFlag.length; i++) {
                    if (arrFlag[i] == 'n0#') {
                        chatHtml += '<div class="text worker"><span style="text-align:right;">' + (i + 1) + '&nbsp;&nbsp;</span>坐席：' + (arrText[i + 1]) + '</div>';
                    } else if (arrFlag[i] == 'n1#') {
                        chatHtml += '<div class="text visiter"><span style="text-align:right;">' + (i + 1) + '&nbsp;&nbsp;</span>客户：' + (arrText[i + 1]) + '</div>';
                    }
                }
                $('#chatqa').html(chatHtml);
            } else {
                toastr.error(e.errorMessage);
            }
        });
    },
    paramJson: function (param, getdictname) {
        if (param) {
            var map = $("#planlistdatagrid").datagrid("getdictnames", {'param': {'codeType': getdictname}});
            var text = '';
            $.each(param.split(","), function (index, item) {
                text += (index == 0 ? '' : ',') + (map[item] ? map[item] : '');
            });
            return text;
        }
    },
    getItemScoreList: function (orderNum, el) {
        holly.post(holly.getPath() + "/rest/commonRest/getItemScoreList", {
            "orderNum": orderNum,
            "paperId": check.paramgetUnscoredPaperMsg.paperId
        }, function (e) {
            if (e.success) {
                if (e.content) {
                    var content = e.content;
                    var html = "";
                    if (orderNum == "0") {
                        for (var i = 0; i < content.length; i++) {
                            html += '<li><label title="' + content[i].scoreItemName + '"><input type="checkbox" onchange="check.scoreItemCheck(this,' + (content[i].scoreType == "0" ? "+" : "-") + content[i].score + ')" value="' + content[i].scoreItemId + '" class="chk" name="scoreItems"/><span class="text">' + content[i].scoreItemName + '</span><span class="point">' + (content[i].scoreType == "0" ? "+" : "-") + content[i].score + '</span></label></li>';
                        }
                    } else {
                        if (content.length > 0) {
                            for (var i = 0; i < content.length; i++) {
                                html += '<li title="' + content[i].scoreItemName + '"><span class="text" title="' + content[i].scoreItemName + '">' + content[i].scoreItemName + '</span><span class="point">' + (content[i].scoreType == "0" ? "+" : "-") + content[i].score + '</span></li>';
                            }
                        } else {
                            html += "无扣分项";
                        }
                    }
                    $(el).html(html);
                }
            } else {
                toastr.error(e.errorMessage);
            }
        });
    },
    scoreItemCheck: function (obj, point) {
        var val = 100;
        if (obj.checked) {
            check.totalScoreReal += point;
        } else {
            check.totalScoreReal -= point;
        }
        val += check.totalScoreReal;
        if (val > 100) {
            val = 100;
        } else if (val < 0) {
            val = 0;
        }
        $('#reviewScore').val(val);
        $('#reviewScoreDiv').html(val + '分');
    },
    saveScore: function () {
        var qualityStatus = $("#actionType").combobox("getValue");
        if (qualityStatus == "未选择") {
            holly.showError('请选择操作类型');
            return false;
        }
        $('#qualityStatus').val(qualityStatus);
        check.formSubmit(qualityStatus);

    },
    formSubmit: function (qualityStatus) {
        var param = {};
        var formJson = holly.form2json($("#scoreForm"));
        if (qualityStatus == "3") {
            var scoreItem = [];
            $('input[name="scoreItems"]:checked').each(function () {
                scoreItem.push($(this).val());
            });
            var caseItem = $("#caseItem").combobox("getValue");
            var recordType = $('input[name="recordType"]:checked').val();
            if ((recordType == '1') && (caseItem == "")) {
                holly.showError('请选择质检案例');
                return false;
            }

            var needCorrect = $('input:radio[name="needCorrect"]:checked').val();//是否纠错
            if (!needCorrect) {
                holly.showError('请选择是否纠错');
                return false;
            }

            var serviceType = $("#formserviceType").combobox("getValue");//业务类型
            if (!serviceType) {
                holly.showError('请选择业务类型');
                return false;
            }

            var answer = formJson.comments;
            if (holly.getLength(answer) > 500) {
                holly.showError('问题描述不能超过500字');
                return false;
            }
            $("#itemId").val(scoreItem);
            param = {
                "caseItem": formJson.caseItem,
                "answer": (answer == null || answer == "") ? "已复核" : answer,
                "contactId": check.contactId,
                "paperId": check.paramgetUnscoredPaperMsg.paperId,
                "isReview": formJson.isReview,
                "itemId": formJson.itemId,
                "qualityStatus": qualityStatus,
                "recordType": formJson.recordType,
                "reviewScore": formJson.reviewScore,
                "appealId": check.appealId,
                "status": "1",
                "isFatal": formJson.isFatal,
                "errorType": formJson.errorType,
                "needCorrect": formJson.needCorrect,
                "serviceType": formJson.serviceType,
                "appealId": check.paramgetUnscoredPaperMsg.appealId
            };
            $.messager.confirm('提醒', '您正在提交质检单，评分为' + $('#reviewScore').val() + '分,请确认评分和问题描述是否已输入完整?', function (r) {
                if (r) {
                    check.formPost(param);
                }
            });
        } else if (qualityStatus == "4") {
            var answer = $('#formanswer4').textbox('getValue');
            if ($.trim(answer).length == 0) {
                holly.showError('请填写驳回理由');
                return false;
            }
            if (holly.getLength(answer) > 500) {
                holly.showError('驳回理由不能超过500字');
                return false;
            }
            param = {
                "paperId": check.paramgetUnscoredPaperMsg.paperId,
                "qualityStatus": qualityStatus,
                "appealId": check.paramgetUnscoredPaperMsg.appealId,
                "answer": answer,
            };
            $.messager.confirm("提醒", "是否确认驳回该质检单?", function (r) {
                if (r) {
                    check.formPost(param);
                }
            });
        } else if (qualityStatus == "5") {
            var auditorCode = formJson.auditorCode;
            if (!auditorCode) {
                holly.showError('请选择审批处理人');
                return false;
            }
            var answer = $('#formanswer5').textbox('getValue');
            if ($.trim(answer).length == 0) {
                holly.showError('请填写申诉理由');
                return false;
            }
            if (holly.getLength(answer) > 500) {
                holly.showError('申诉理由不能超过500字');
                return false;
            }
            param = {
                "qualityStatus": qualityStatus,
                "answer": answer,
                "appealId": check.paramgetUnscoredPaperMsg.appealId,
                "auditorCode": auditorCode
            };
            $.messager.confirm("提醒", "是否确认继续申诉该质检单?", function (r) {
                if (r) {
                    check.formPost(param);
                }
            });
        } else {
            holly.showError('请选择操作类型');
            return false;
        }
    },
    turnedDown: function () {
    },
    formPost: function (param) {
        $.messager.progress({
            text: '正在提交...'
        });
        holly.post(holly.getPath() + "/rest/commonRest/doAppealPaper", param, function (e) {
            if (e.success) {
                if (param.qualityStatus == "3") {
                    holly.showSuccess('复核评分成功');
                } else if (param.qualityStatus == "4") {
                    holly.showSuccess('驳回成功');
                } else if (param.qualityStatus == "5") {
                    holly.showSuccess('申诉成功');
                }

                var nextParam = check.paramgetUnscoredPaperMsg;
                // var url = holly.getPath()+'/hollysqm/qualitymanagement/page/check.jsp';
                // url += '?dataType='+nextParam.dataType;
                // url += '&agent='+nextParam.agent;
                // url += '&endTime='+nextParam.endTime;
                // url += '&planId='+nextParam.planId;
                // url += '&startTime='+nextParam.startTime;
                // url += '&paperId='+check.nextPaperId;
                // url += '&contactId='+check.contactId;
                // url += '&appealId='+check.nextAppealId;
                // url += '&status='+nextParam.status;
                // url += '&frameId='+check.frameId;

                // 如果待复核列表页面没有关闭，并且该页面是从待复核列表打开的
                // if((parent.$("#"+check.frameId).length>0) && (check.thisIframeId=='iframequalitymanagement')){
                // 	var $obj = parent.$("#"+check.frameId)[0].contentWindow;
                // 	$obj.unChecked.search();//重新搜索列表
                // }
                // if(check.thisIframeId=='iframequalitymanagement'){
                // 	if((check.nextPaperId) && (check.nextPaperId!="0")){
                // 		window.location.href=url;
                // 	}else{
                // 		alert('已完成最后一条复核，待复核页面将会关闭');
                // 		var $tabs = parent.$('#tabs');
                // 		var curTab = $tabs.tabs('getSelected');
                // 		var tabIndex = $tabs.tabs('getTabIndex',curTab);
                // 		$tabs.tabs('close',tabIndex);
                // 	}
                // }else{
                // 	var $tabs = parent.$('#tabs');
                // 	var curTab = $tabs.tabs('getSelected');
                // 	var tabIndex = $tabs.tabs('getTabIndex',curTab);
                // 	$tabs.tabs('close',tabIndex);
                // }

                //跳转回待复核页面
                var url = holly.getPath() + '/hollysqm/qualitymanagement/page/qualitymanagement.jsp?frameId=' + check.frameId;
                window.location = url;
            } else {
                holly.showError(e.errorMessage);
            }
            $.messager.progress('close');
        });
    },
    getSysUser: function (userCode) {
        var userName = "";
        holly.post(holly.getPath() + "/rest/user/findUser", {"userCode": userCode}, function (e) {
            if (e.success) {
                userName = e.content[0].userName;
            } else {
                holly.showError(e.errorMessage);
            }
        }, true);
        return userName;
    },

    formatTime: function (time) {
        var year = time.substr(0, 4);
        var month = time.substr(4, 2);
        var day = time.substr(6, 2);
        var hour = time.substr(8, 2);
        var minute = time.substr(10, 2);
        var second = time.substr(12, 2);
        return year + '-' + month + '-' + day + ' ' + hour + ":" + minute + ':' + second;
    },
    planinfo: function () {
        var url = holly.getPath() + "/hollysqm/planmanager/page/dlg-planinfo.jsp";
        url += "?planId=" + check.planId;
        url += "&dataType=" + check.dataType;
        $('#dlg-planinfo').dialog('open').dialog('refresh', url);
    },
    recordTypeEvent: function (val) {
        if (val == "1") {
            $('#caseItemRequired').show();
            $('#caseItem').combobox('enable');
        } else if (val == "2") {
            $('#caseItemRequired').hide();
            $('#caseItem').combobox('setValue', '').combobox('disable');
        }
    },
    initCheck: function () {

        check.getItemScoreList("1", "#scoreitemlistText");//加载评分信息
        check.getItemScoreList("0", "#scoreitemlist");//加载复核项

        // 获取当前质检单绑定的典型案例
        holly.post(holly.getPath() + "/rest/commonRest/getPaperCase", check.paramgetUnscoredPaperMsg, function (e) {
            if (e.success) {
                if (e.content) {
                    if (e.content.caseItem) {
                        $("#caseItem").combobox('setValue', e.content.caseItem);//质检案例
                        if (e.content.recordType == "1") {//典型案例
                            $('#recordType1').attr('checked', 'checked');
                        } else {
                            $('#recordType2').attr('checked', 'checked');
                        }
                        check.recordTypeEvent(e.content.recordType);
                    } else {
                        $('#recordType2').attr('checked', 'checked');
                    }

                }
            }
        });
    },
    servicetypeOption: function () {
        $('#formserviceType').combobox({
            url: holly.getPath() + '/rest/commonRest/queryServiceTypeTopList',
            valueField: 'value',
            textField: 'name',
            loadFilter: function (data) {
                var jsonArray = new Array();
                for (var o in data.content) {
                    jsonArray.push(data.content[o]);
                }
                return jsonArray;
            }, onHidePanel: function () {
                var el = $(this);
                var value = el.combobox("getValue");
                if (!value)//如果没有匹配到值，则默认设置成空
                    el.combobox("setValue", "");
            }
        });
    },
    actionType: function (data) {
        var value = "";
        try {
            value = data.value;
        } catch (e) {
        }
        if (value == "3") {//复核
            $("#shenhe3").show();
            holly.renderEasyUI($("#shenhe3").children());
            $("#shenhe4").hide();
            $("#shenhe5").hide();
        } else if (value == "4") {
            $("#shenhe3").hide();
            $("#shenhe4").show();
            holly.renderEasyUI($("#shenhe4").children());
            $("#shenhe5").hide();
        } else if (value == "5") {//下一个审批人
            $("#shenhe3").hide();
            $("#shenhe4").hide();
            $("#shenhe5").show();
            holly.renderEasyUI($("#shenhe5").children());
        } else {
            $("#shenhe3").hide();
            $("#shenhe4").hide();
            $("#shenhe5").hide();
        }
    },
    selectUserCode: function () {
        assignUser.loadBind();
        $("#selectUserCode").dialog().dialog("open");
    }

};

function setUser(userCode, userName) {
    $("#auditorCode").val(userCode);
    $("#examineName").textbox("setValue", userName);
}

var listenTape = {
    init: function (voicePath, imgPath) {
        var player,//播放器
            intever = 0;//定时器 返回number类型
        player = new _mu.Player({
            baseDir: holly.getPath() + '/hollysqm/common/muplayer/dist'
        });//初始播放器
        //setSong(contextPath+path);//设置播放录音
        //音量调节
        $(document).on('sliderchange', "#voice", function (e, result) {
            player.setVolume(result.value);
        });
        $('.myautoplayer-play').on('click', function () {
            if ($(this).hasClass('icon-play')) {
                player.play();//播放事件
                $(this).removeClass("icon-play").addClass("icon-pause");
                intever = scrollImg();
            } else if ($(this).hasClass('icon-pause')) {
                player.pause();//暂停
                $(this).removeClass("icon-pause").addClass("icon-play");
                clearInterval(intever);//清除事件
            }
        });
        //图片点击播放事件
        $("#divScroll").click(function (e) {
            var scrollLeft = $("#divScroll").scrollLeft();
            var offset = $(this).offset();
            var relativeX = (e.pageX - offset.left) + scrollLeft;
            var len = parseInt(relativeX / $("#imgVoice").width() * player.duration() * 1000);
            player.play(len);
            intever = scrollImg();
            $(".icon-play").addClass("icon-pause").removeClass("icon-play");
        });
        //滚动图片
        function scrollImg() {
            return setInterval(function () {//改变滑动块
                var l = player.curPos() / player.duration();
                var left = $("#imgVoice").width();//图片宽度
                if (l > (1 - 900 / left)) {
                    clearInterval(intever);//清除事件
                    $(".icon-pause").addClass("icon-play").removeClass("icon-pause");
                    return;
                }
                ;
                $("#divScroll").animate({scrollLeft: left * l}, 1000);
            }, 1000);
        }

        //滚动条拖动事件
        $("#divScroll").mousedown(function () {
            var state = player.getState();
            if (state == 'playing') {
                player.pause();//暂停
                $(".icon-pause").addClass("icon-play").removeClass("icon-pause");
                clearInterval(intever);//清除事件
            }
        });
        player.setUrl(holly.getPath() + "/" + voicePath);//添加录音文件
        $("#imgVoice").attr("src", holly.getPath() + "/" + imgPath).removeClass('voiceloading').addClass('voicechatimg');
    }
};