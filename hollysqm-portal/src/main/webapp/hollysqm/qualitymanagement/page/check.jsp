<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/hollybeacon/common/taglibs.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<%@ include file="/hollybeacon/common/meta.jsp" %>
<title>复核</title>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type="text/javascript" src="${ctx}/hollysqm/common/muplayer/dist/player.min.js"></script>
<script type="text/javascript" src="${ctx}/hollysqm/common/jquery-ui/jquery-ui-1.10.3.mouse_core.js"></script>
<script type="text/javascript" src="${ctx}/hollysqm/common/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="${ctx}/hollysqm/common/bootstrap/js/bootstrapslider.min.js"></script>
<script src="${ctx}/hollysqm/qualitymanagement/js/check.js"></script>
<script>
    var loginUser = ${loginUser};

    // 获取必要的数据字典MAP
var satisfactionMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'SATISFACTION'}});
var serviceTypeMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'BUSINESS_TYPE'}});
var caseItemMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'CASE_ITEM'}});
var errorTypeMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'ERROR_TYPE'}});
var dataTypeMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'DATATYPE'}});
var stateMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'STATUS'}});
var taskTimerMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'TASK_TIMER'}});
var appealtatusMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'APPEAL_STATUS'}});//申诉状态
var jobypeMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'JOB_TYPE'}});//质检角色
var paperServiceTypeMap = $("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'PAPER_SERVICE_TYPE'}});
var appealStatusMap = {0:"已申诉",1:"已通过",2:"已驳回",3:"待处理"};
$(function(){
	check.init();
});
</script>
</head>
<body class="easyui-layout">
<div data-options="region:'center',border:false">

	<div class="container-sqm cfit">  
		<div class="heading">
			<img class="pic" src="${ctx}/hollysqm/common/images/add.png" />
			<div class="title">复核</div>
			<div class="subtitle"><span class="maincolor" id="paperCount"></span> <span id="paperCountText"></span>
			<span class="sublinkpane" id="planDetail"></span>
			</div>
		</div>
		<div class="contentpane">
			<div class="scorepanel">
				<div class="scoreleft">
					<div class="container-sqm1">
						<div class="heading">
							<i class="icon"></i>
							<div class="title">接触记录</div>
						</div>
						<div class="contentpane" style="padding:15px;">
							<div class="iform-horizontal column-3 labelmin iform-text" id="getRecordDiv">
							</div>
							<div class="container-voicechat" id="voicechatDiv" style="margin-bottom:20px;display:none;">
								<!-- 语音波形图 -->
								<div class="voicechatbox" id="divScroll">
									<img src="${ctx}/hollysqm/common/images/loading-mask.gif" class="voiceloading" id="imgVoice">
								</div>
								<div class="myautoplayer">
									<span class="myautoplayer-play icon-play"></span>
									<span class="myautoplayer-volume icon-volume-up"></span>
									<div id="voice" class="progress slider">
										<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
									</div>
								</div>
							</div>
							<div class="chatqa" id="chatqa">
							</div>
						</div>
						<%-- <jspinclude page="../../common/weigui.jsp" flush="true"/> --%>
					</div>
				</div>

				<div class="scoreright">
					<div class="container-sqm1">
						<div class="heading">
							<i class="icon"></i>
							<div class="title">评分</div>
						</div>
						<div class="contentpane">
								<div class="iform-horizontal labelmin iform-text">
									<div class="icontrol-group">
										<div class="icontrol-label">评分</div>
										<div class="icontrols icontrols-text">
											<ul class="scoreitemlist" id="scoreitemlistText">
											</ul>
										</div>
									</div>
									<div class="icontrol-group">
										<div class="icontrol-label">得分</div>
										<div class="icontrols icontrols-text"><span style="color:#ff0000;" id="totalScoreDiv">100分</span></div>
									</div>
									<div class="icontrol-group">
										<div class="icontrol-label">质检员</div>
										<div class="icontrols" id="checkerInfoDiv">
										</div>
									</div>
									<div class="icontrol-group">
										<div class="icontrol-label">评分时间</div>
										<div class="icontrols" id="scoreTimeDiv">
										</div>
									</div>
									
								</div>
						</div>
					</div>
					
					<div class="container-sqm1" style="display:none;" id="reviewContainer">
					<div class="heading">
						<i class="icon"></i>
						<div class="title">复核</div>
					</div>
					<div class="contentpane">
						<div class="iform-horizontal labelmin iform-text">
							
							<div class="icontrol-group" id="scoreitemlistTip2">
								<div class="icontrol-label">复核</div>
								<div class="icontrols icontrols-text">
									<ul class="scoreitemlist" id="scoreitemlistText2">
									</ul>
								</div>
							</div>
							<div class="icontrol-group" id="reviewScoreTip">
								<div class="icontrol-label">得分</div>
								<div class="icontrols icontrols-text"><span style="color:#ff0000;" id="reviewScoreDiv2"></span></div>
							</div>
							<div class="icontrol-group">
								<div class="icontrol-label">复核人</div>
								<div class="icontrols icontrols-text" id="checkerInfoDiv2"></div>
							</div>
							<div class="icontrol-group">
								<div class="icontrol-label">复核时间</div>
								<div class="icontrols icontrols-text" id="reviewTimeDiv"></div>
							</div>
							<div class="icontrol-group">
								<div class="icontrol-label">典型案例</div>
								<div class="icontrols icontrols-text" id="recordTypeDiv2">
								</div>
							</div>
							<div class="icontrol-group" id="caseItemTip2">
								<div class="icontrol-label">质检案例</div>
								<div class="icontrols" id="caseItemDiv2">
								</div>
							</div>
							
							<div class="icontrol-group">
							<div class="icontrol-label">差错类型</div>
							<div class="icontrols" id="errorTypeDiv2">
							</div>
						</div>
						
						<div class="icontrol-group">
							<div class="icontrol-label">是否纠错</div>
							<div class="icontrols icontrols-text" id="needCorrectDiv2">
							</div>
						</div>
						
						<div class="icontrol-group">
							<div class="icontrol-label">是否致命</div>
							<div class="icontrols icontrols-text" id="isFatalDiv2">
							</div>
						</div>

						<div class="icontrol-group">
							<div class="icontrol-label">业务类型</div>
							<div class="icontrols icontrols-text" id="serviceTypeDiv">
							</div>
						</div>
							
						<div class="icontrol-group">
							<div class="icontrol-label">问题描述</div>
							<div class="icontrols" id="commentsDiv2">
							</div>
						</div>
							
						</div>
					</div>
				</div>
					
					<div class="container-sqm1">
						<div class="heading">
							<i class="icon"></i>
							<div class="title">申诉记录</div>
						</div>
						<div class="contentpane">
							<div class="iform-horizontal labelmin">
								<div id="appealPaperDiv">
								</div>
							</div>
						</div>
					</div>
					
					<div class="container-sqm1" id="reshenhePaperDiv1" style="display: none;">
						<div class="heading">
							<i class="icon"></i>
							<div class="title">复核</div>
						</div>
						<div class="contentpane">
							<form id="scoreForm" class="easyui-form" method="post">
								<div class="iform-horizontal labelmin">
									<div class="icontrol-group">
										<div class="icontrol-label"><i class="required">*</i>操作类型</div>
										<div class="icontrols icontrols-text">
											<select id="actionType" name="actionType" class="easyui-combobox" data-options="editable:false,width:'100%',onSelect:check.actionType">
												<option value="未选择">未选择</option>
												<option value="3">复核</option>
												<option value="4">驳回</option>
												<option value="5">流转下一审批人</option>
											</select>
										</div>
									</div>
									<div id="shenhe3" style="display: none;">
										<div class="icontrol-group">
											<div class="icontrol-label">复核</div>
											<div class="icontrols icontrols-text">
												<ul class="scoreitemlist" id="scoreitemlist">
												</ul>
											</div>
										</div>
										<div class="icontrol-group">
											<div class="icontrol-label">得分</div>
											<div class="icontrols icontrols-text"><span style="color:#ff0000;" id="reviewScoreDiv">100分</span></div>
										</div>
										
										<div class="icontrol-group">
											<div class="icontrol-label">典型案例</div>
											<div class="icontrols icontrols-text">
												<label><input id="recordType1" type="radio" name="recordType" value="1" onclick="check.recordTypeEvent('1')" /> 是</label>&nbsp;&nbsp;
												<label><input id="recordType2" type="radio" name="recordType" value="2" onclick="check.recordTypeEvent('2')" checked /> 否</label>
											</div>
										</div>
										
										<div class="icontrol-group">
											<div class="icontrol-label"><i class="required" id="caseItemRequired" style="display:none;">*</i>质检案例</div>
											<div class="icontrols">
												<select id="caseItem" name="caseItem" data-options="disabled:true,editable:false,width:'100%'"></select>
											</div>
										</div>
										
										<div class="icontrol-group">
											<div class="icontrol-label"><i class="required">*</i>是否纠错</div>
											<div class="icontrols icontrols-text">
												<label><input type="radio" id="needCorrect1" name="needCorrect" value="1" /> 是</label>&nbsp;&nbsp;
												<label><input type="radio" id="needCorrect0" name="needCorrect" value="0" checked /> 否</label>
											</div>
										</div>
										
										<div class="icontrol-group">
											<div class="icontrol-label">是否致命</div>
											<div class="icontrols icontrols-text">
												<label><input type="radio" name="isFatal" id="isFatal1" value="1" /> 是</label>&nbsp;&nbsp;
												<label><input type="radio" name="isFatal" id="isFatal0" value="0" checked /> 否</label>
											</div>
										</div>
										
										
										<div class="icontrol-group">
											<div class="icontrol-label"><i class="required" id="errorTypeRequired">*</i>差错类型</div>
											<div class="icontrols">
												<select id="errorType" name="errorType" data-options="disabled:false,editable:false,width:'100%'"></select>
											</div>
										</div>
										
										<div class="icontrol-group">
											<div class="icontrol-label"><i class="required">*</i>业务类型</div>
											<div class="icontrols">
												<select name="serviceType" id="formserviceType" class="easyui-combobox" data-options="width:'100%'"></select>	
											</div>
										</div>
										<div class="icontrol-group">
											<div class="icontrol-label">问题描述</div>
											<div class="icontrols">
												<input name="comments" id="comments" class="easyui-textbox" data-options="multiline:true,width:'100%',height:'60px',validType:['length[0,500]']" />
											</div>
										</div>
									</div>
									<div id="shenhe4" style="display: none;">
										<div class="icontrol-group">
											<div class="icontrol-label"><i class="required">*</i>驳回理由</div>
											<div class="icontrols"><!-- name="answer" -->
												<input  id="formanswer4" class="easyui-textbox" data-options="width:'100%',height:60,required: true,multiline:true,novalidate:true,validType:['length[0,500]']" />
											</div>
										</div>
									</div>
									<div id="shenhe5" style="display: none;">
										<div class="icontrol-group">
											<div class="icontrol-label"><i class="required">*</i>审批人</div>
											<div class="icontrols">
												<input name="examineName" class="easyui-textbox" id="examineName" data-options="onClickButton:check.selectUserCode,width:'100%',buttonText:'选择',prompt:'请选择审批人...',editable:false" />
											</div>
										</div>
										<div class="icontrol-group">
											<div class="icontrol-label"><i class="required">*</i>申诉理由</div>
											<div class="icontrols"><!-- name="answer" -->
												<input  id="formanswer5" class="easyui-textbox" data-options="width:'100%',height:60,required: true,multiline:true,novalidate:true,validType:['length[0,500]']" />
											</div>
										</div>
									</div>
									<div class="icontrol-group">
										<div class="icontrol-label"></div>
										<div class="icontrols">
											<button class="easyui-linkbutton l-btn-main" type="button" onclick="check.saveScore()">提交</button>
											<button class="easyui-linkbutton" type="button" onclick="$('#scoreForm').form('reset');check.actionType();">重置</button>
										</div>
									</div>
								</div>
								<input id="reviewScore" type="hidden" name="reviewScore" value="100" />
								<input id="qualityStatus" type="hidden" name="qualityStatus" value="" />
								<input id="itemId" type="hidden" name="itemId" value="" />
								<input id="isReview" type="hidden" name="isReview" value="1">
								<input id="contactId" type="hidden" name="contactId" value="" />
								<input id="isPublicResult" type="hidden" name="isPublicResult" value="" />
								<input id="auditorCode" type="hidden" name="auditorCode" /><!-- 申诉处理人 -->
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<div id="dlg-planinfo" class="easyui-dialog" style="padding:10px;"
		data-options="
			title:'查看计划详情',
			closed:true,
			width:750,
			height:'95%'
		">
</div>

<jsp:include page="../../agentscore/page/asignUser.jsp" flush="true"/>
</body>
</html>