<%@include file="/hollybeacon/common/taglibs.jsp"%>
<script type="text/javascript">
var loginUser = ${loginUser};
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/hollysqm/agentscore/js/asignUser.js"></script>
<link rel="stylesheet" href="<%=request.getContextPath() %>/hollysqm/agentscore/css/asignUser.css">
<div id="selectUserCode" class="easyui-dialog" title="选择审批人"
		data-options="
			closed:true,
			width:'45%',
			height:'95%',
			draggable:true,buttons:'#dlg-buttons1'
		">
		<div class="contain">
		<div class="easyui-layout" data-options="fit:true">
			<div data-options="region:'west',border:false">
				<div class="easyui-panel" title="可选人员" data-options="width:480,height:'100%'" >
					<div class="easyui-layout" data-options="fit:true">
						<div data-options="region:'west',border:false,width:'50%'">
							<div class="treecontentpane">
								<label class="easyui-radio"><input name="showStopDic" type="checkbox" value=""> 显示停用的部门</label>
								<ul id="personTree"></ul>
							</div>
						</div>
						<div data-options="region:'center',border:false" style="padding:10px; border-left:1px solid #ddd;">
							<div class="easyui-layout" data-options="fit:true">
								<div data-options="region:'north',border:false" style="height:50px; padding:8px 5px;">
									<div><input id="searchName" data-options="width:'100%'"></div>
								</div>
								<div data-options="region:'center',border:false">
									<div class="select-panel out">
										<ul id="srcList" class="select-list">
										</ul>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	    </div>
    </div>
    <div id="dlg-buttons1">
			<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red"onclick="assignUser.getCheckUserCode()">确定</a> 
			<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#selectUserCode').dialog('close')">取消</a>
	</div>