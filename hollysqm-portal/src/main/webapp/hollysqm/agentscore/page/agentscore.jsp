<%@page import="com.hollycrm.hollybeacon.basic.util.AppConfigUtils"%>
<%@page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>坐席成绩</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type="text/javascript" src="../js/agentscore.js" ></script>
<%
Integer export_total = AppConfigUtils.getConfig().getInteger("system.export.total", 100000);
%>
<script type="text/javascript">
    var loginUser = ${loginUser};
    var export_total = <%=export_total%>;
// 获取必要的数据字典MAP		
//var dataTypeMap=$("#agentScoredatagrid").datagrid("getdictnames",{'param':{'codeType':'DATATYPE'}});
//var planNameMap=$("#agentScoredatagrid").datagrid("getdictnames",{'param':{'codeType':'PLAN_NAME'}});
//var qualityStatusMap=$("#agentScoredatagrid").datagrid("getdictnames",{'param':{'codeType':'STATUS'}});
var closeTypeMap=$("#unCheckeddatagrid").datagrid("getdictnames",{'param':{'codeType':'CLOSE_TYPE'}});
var sessionTypeMap=$("#unCheckeddatagrid").datagrid("getdictnames",{'param':{'codeType':'SESSION_TYPE'}});
$(function() {
	agentScore.init();
	$("#agentCode").val(loginUser.userCode);
	$("#searchagentScore").keyup(function(event){
		if(event.keyCode==13){
			agentScore.search();
		}
	});
});
</script>

<style type="text/css">
.review_score{
	color: red;
}
</style>
</head>
<body class="easyui-layout">
	<div data-options="region:'center',border:false" style="padding:10px;">
		<div class="easyui-layout" data-options="fit:true">
			<!-- 搜索区域 -->
			<div data-options="region:'north',border:false" style="padding-bottom:10px;">
				<form id="searchagentScore" method="post" class="search-form">
					<input type="hidden" name="agentCode" id="agentCode"/>
					<div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">质检来源</span>
							<div class="valueGroup">
								<select name="paperType" id="formDataType" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">质检计划</span>
							<div class="valueGroup">
								<select name="planName" id="formplanName" class="easyui-combobox" data-options="width:'100%'"></select>	
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">质检时间</span>
							<div class="valueGroup">
								<input type="text" class="easyui-datebox" name="createStartTime" data-options="editable:false,width:'100%'">
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">至</span>
							<div class="valueGroup">
								<input type="text" class="easyui-datebox" name="createEndTime" data-options="editable:false,width:'100%'">
							</div>
						</span>
					</div>
					<div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">质检状态</span>
							<div class="valueGroup">
								<select name="qualityStatus" id="formappealState" class="easyui-combobox" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>	
					</div>
					<div class="btn-group">
						<a href="javascript:;" class="operation search-btn" onclick="agentScore.search()" title="搜索"></a>
						<a href="javascript:;" class="operation reset-btn" onclick="agentScore.resetForm()" title="重置"></a>
					</div>
				</form>
			</div>
			<!-- 搜索区域 end -->	
			<div data-options="region:'center',border:false">
				<div id="tt">
					<a href="javascript:void(0);" class="easyui-linkbutton" id="exportTable" onclick="agentScore.exportPaper(true)">导出</a>
				</div>
				<div class="easyui-panel" title="坐席成绩" data-options="tools:'#tt',fit:true">
					<table id="agentScoredatagrid"></table>
					<table id="agentScoredatagrid_i8"></table>
				</div>
			</div>
		</div>
	</div>
	
	<div id="dlg-export" class="easyui-dialog" title="导出坐席成绩"
		data-options="
			closed:true,
			buttons: '#dlg-export-buttons',
			width:650,
			height:'95%'
		">
		<iframe id="iframe-export" name="iframe-export" src="" frameborder="0" style="width:100%;height:100%;display:block;"></iframe>
    </div>
    
	<div id="dlg-export-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red" onclick="agentScore.exportPaper(false)">确定</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#dlg-export').dialog('close')">取消</a>
	</div>
	
	<div id="dlg-sqminfo" class="easyui-dialog" title="质检单信息"
		data-options="
			closed:true,
			width:'95%',
			height:'95%',
			draggable:false
		">
	</div>
</body>
</html>