/**
 * 分配用户
 * @type
 */
var assignUser = {
	/**
	 * 加载部门,工作组,个人dialog等初始化操作
	 */
	loadBind:function(){
		// 加载个人树
		$("#personTree").tree({
			queryParams:{enabled:true},
			url: holly.getPath() + "/rest/org/findTree",
			method:"get",dnd:false,
			loadFilter:function(data){return (data.content)?data.content:data;},
			onClick:function(node){
				assignUser.searchUser();
			},
			onLoadSuccess:function(node,data){
				assignUser.expandAll($("#personTree"));
			}
		});
		$("input[name='showStopDic']").change(assignUser.searchDept);
		$("#searchName").searchbox({prompt:'按姓名搜索',iconWidth:22,searcher:assignUser.searchUser}).textbox('addClearBtn', {
			iconCls:'icon-clear',
			handler: function(){
				assignUser.searchUser();
			}
		});
	},
	/**
	 * 展开根节点或树的所有节点
	 * @param {} ele 如果存在表示指定树展开根节点
	 */
	expandAll:function(ele){
		if(ele){//展开指定的树
			var root=ele.tree("getRoot");
			if(root){ele.tree("expand",root.target);}
		}
	},
	/**
	 * 显示停用部门
	 */
	searchDept:function(){
		var params=$("input[name='showStopDic']").is(':checked')?{}:{enabled:true};
		$("#personTree").tree('options').queryParams=params;
		$("#personTree").tree('reload');
	},
	/**
	 * 查找部门下的用户
	 */
	searchUser:function(){
		var param={enabled:1};
		var orgNode=$("#personTree").tree("getSelected");
		if(orgNode){//将当前选中的部门或机构作为查询参数
			if(orgNode.attributes && orgNode.attributes.isOrg){
				param.orgId=orgNode.id;
			}else{
				param.deptId=orgNode.id;
				param.orgId=orgNode.attributes.rootId;
			}
		}
		param.orgId=(param.orgId)?param.orgId:loginUser.orgId;
		param.userName=$("#searchName").val();
		holly.post(holly.getPath()+"/rest/user/findUser",param,function(e){
			if(e.success){
				var html = '';
				$("#srcList").empty();
				$("#srcList").append("");
				$.each(e.content, function (index, v) {
					if(v.id && v.userCode){
					  var toolsTip={deptName:v.deptName,orgName:v.orgName};
					  var tip="<div id='tip"+v.userCode+"' class='opt-dropdown-noline assign' data-options='minWidth:100'><div>所属部门：{deptName}</div></div>".replace(/{deptName}/g,toolsTip.deptName||"").replace(/{orgName}/g,toolsTip.orgName||"");
					  
					  html+='<li><label class="select-item" userId="'+v.userCode+'" data-userCode="'+v.userCode+'">'+'<input '+(loginUser.userCode == v.userCode?'disabled= "true"':'')+' type="checkbox" userId="'+v.userCode+'" class="select-check" onChange="assignUser.assign(this);" >'+
							'<span class="name">'+v.userName+'</span><span class="userCode">（'+v.userCode+'）</span>'+
							'</label><span class="opr-wrp"><i class="easyui-menubutton opr-icon icon-info-sign" data-options="menu:\'#tip'+v.userCode+'\'"></i>'+tip+'</span></li>';
						
					}
				});
				$("#srcList").append(html);
				$('.easyui-menubutton').menubutton();
			}
		});
	},
	/**
	 * 选中人员时触发移动事件
	 */
	assign:function(obj){
		$(".select-check").removeAttr("checked");
		$(obj).attr('checked','checked');
	},
	getCheckUserCode : function(){
		var userCode = "",userName = "";
		
		$(".select-check:checked").each(function(){
			userCode = $(this).attr("userid");
			userName = $(this).next().filter(".name").html();
		});
		if(userCode == ""){
			holly.showError('未选择人员');
			return false;
		}
		
		setUser(userCode,userName);
		$("#selectUserCode").dialog("close");
	}
};