var agentScore = {
	/**
	 * 初始化加载数据
	 */
	init:function(){
		$("#formDataType").combobox("init_extend",{'codeType':'DATATYPE','hasAllOption':false,event:{
			onLoadSuccess:function(){
				agentScore.dataTypeData = $(this).combobox('getData');
				$(this).combobox('setValue',agentScore.dataTypeData[0].value);
			}
		}});
		$("#formappealState").combobox("init_filter",{'codeType':'QUALITY_STATUS',"filterValue":"0"});
		agentScore.planNameOption();
		// 初始化质检单信息dialog
		$('#dlg-sqminfo').dialog({
			onClose:function(){
				// 关闭dialog停止播放语音
				if(listenTape.player.pause){
					listenTape.player.pause();
				}
			}
		});
		$("#agentScoredatagrid").datagrid({
			fit:true,
			url:holly.getPath()+'/rest/agentscore/queryAgentscore',
			method:'get',
			singleSelect : true,
			border:false,
			fitColumns:true,
			queryParams:{'paperType':'v8'},
			pagination: true,
			loadFilter: function(data){return (data.content)?data.content:data;},
			columns:[[
				{field:'paperTypeName',title:'质检来源',width:60},
				{field:'planName',title:'质检计划',width:100,formatter:agentScore.formatPlanName},
				{field:'caller',title:'来电号码',width:60},
				{field:'startTime',title:'来电时间',width:80},
				{field:'totalScore',title:'评分得分',width:60},
				{field:'modifyTime',title:'质检时间',width:80,formatter:agentScore.formatPlanName},
				{field:'qualityStatus',title:'质检状态',width:50},
				{field:'reviewScore',title:'复核得分',width:60,formatter:agentScore.formatReviewScore},
				/*{field:'createCodeName',title:'质检员'},*/
				{field:'option',title:'操作',width:60,formatter:agentScore.formatOption}
			]],
			loadMsg:'数据加载中请稍后……',
			onLoadSuccess: function (data)  {
				if (data.rows.length == 0) {
					$("#exportTable").hide();//没有数据时隐藏导出按钮
					var body = $(this).data().datagrid.dc.body2;
					body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
				}else $("#exportTable").show();
			}
		});
		$("#agentScoredatagrid_i8").datagrid({
			fit:true,
			method:'get',
			singleSelect : true,
			border:false,
			fitColumns:true,
			pagination: true,
			closed:true,
			loadFilter: function(data){return (data.content)?data.content:data;},
			columns:[[
				{field:'paperTypeName',title:'质检来源',width:60},
				{field:'planName',title:'质检计划',width:100,formatter:agentScore.formatPlanName},
				{field:'caller',title:'来电号码',width:60},
				{field:'startTime',title:'来电时间',width:80},
				{field:'totalScore',title:'评分得分',width:50},
				{field:'modifyTime',title:'质检时间',width:80},
				{field:'qualityStatus',title:'质检状态',width:50},
				{field:'reviewScore',title:'复核得分',width:50,formatter:agentScore.formatReviewScore},
				{field:'createCodeName',title:'质检员'},
				{field:'closeType',title:'关闭方式',width:60,formatter:agentScore.formatCloseType},
				{field:'sessionLevel',title:'会话等级',width:60,formatter:agentScore.formatSessionType},
				{field:'option',title:'操作',width:60,formatter:agentScore.formatOption}
			]],
			loadMsg:'数据加载中请稍后……',
			onLoadSuccess: function (data)  {
				if (data.rows.length == 0) {
					$("#exportTable").hide();//没有数据时隐藏导出按钮
					var body = $(this).data().datagrid.dc.body2;
					body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
				}else $("#exportTable").show();
			}
		});
	},

	
	/**
	 * 搜索
	 */
	search:function(){
		var param = holly.form2json($("#searchagentScore"));
		if(agentScore.validateTime(param)){
			if(param.paperType=='v8'){
				if($("#agentScoredatagrid").datagrid('getPanel').panel('options').closed){
					$("#agentScoredatagrid_i8").datagrid('getPanel').panel('close');
					$("#agentScoredatagrid").datagrid('getPanel').panel('open').panel('resize');
				}
				$("#agentScoredatagrid").datagrid("search",{'param':param});
			}else if(param.paperType=='i8'){
				$("#agentScoredatagrid_i8").datagrid('options').url = holly.getPath()+'/rest/agentscore/queryAgentscore';
				if($("#agentScoredatagrid_i8").datagrid('getPanel').panel('options').closed){
					$("#agentScoredatagrid").datagrid('getPanel').panel('close');
					$("#agentScoredatagrid_i8").datagrid('getPanel').panel('open').panel('resize');
				}
				$("#agentScoredatagrid_i8").datagrid("search",{'param':param});
			}
		}
	},
	formatPlanName : function(val,row,index){//质检计划名称
        if(val == undefined || val =='' || val == null || val=='null') {
            val = '全文检索';
        }
        return '<span title='+val+'>'+val+'</span>';
	},
	// 质检来源
	formatDataTyp:function(val, row, index) {
		//var res =dataTypeMap[val]; //获取对应的数据
		//return "<span title='"+res+"'>"+res+"</span>";
	},
	formatCloseType:function(val, row, index){
		var res = closeTypeMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	},
	formatSessionType:function(val, row, index){
		var res = sessionTypeMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	},
	// 申诉状态
	formatState:function(val, row, index) {
		//var res =qualityStatusMap[val]; //获取对应的数据
		//return res;
	},
	formatReviewScore : function(val ,row ,index){
		return val ?"<span class = 'review_score'>"+ val +"</span>":val; 
	},
	// 质检员
	formatCheckerName:function(val,row,index){
		return val?"<span title='"+row.checker + "(" +val+ ")"+"'>"+row.checker + "(" + val+ ")"+"</span>":"";
	},
	planNameOption : function(){
		$('#formplanName').combobox({
		    url : holly.getPath() + '/rest/commonRest/queryPlan',
		    valueField : 'value', 
			textField : 'name',
			loadFilter: function(data){
				var jsonArray=new Array();
				for(var o in data.content){
					jsonArray.push(data.content[o]);
				}
				return jsonArray;
			},onHidePanel : function(){
				var el = $(this);
				var value = el.combobox("getValue");
				if(!value)//如果没有匹配到值，则默认设置成空
					 el.combobox("setValue","");
			}
		});  
	},
	sqminfo : function(paperId,contactId){
		var url = holly.getPath()+"/hollysqm/agentscore/page/dlg-sqminfo.jsp";
		url += "?paperId=" + paperId+"&contactId="+contactId;
		$('#dlg-sqminfo').dialog('open').dialog('refresh',url);
	},
	// 操作
	formatOption:function(val,row,index){
		var content = '';
			content	+= "<a href='javascript:void(0)' onclick=\"agentScore.sqminfo('"+row.paperId+"','"+row.planId+"')\" class='grid-optcon icon-eye-open' title='查看'></a>";
		return content;
	},
	exportPaper : function(toPage){//导出质检单
		var formDataType = $("#formDataType").combobox("getValue");
		var grid = $('#agentScoredatagrid');  
		if(formDataType == "i8")
			grid = $('#agentScoredatagrid_i8');  
		var options = grid.datagrid('getPager').data("pagination").options;  
		var total = options.total;
		if(total > export_total){
			$.messager.alert("提示","导出数据不能大于"+export_total+"，请继续筛选数据！");
			return false;
		}
		if(toPage){
			var url =holly.getPath()+"/rest/csv/wizard/sqlexport?tableId=csv.score.queryAgentScore_"+formDataType+"&sqlKey=velocity.score.queryAgentScore&mediaId=1";
			var targetIframe ="iframe-export";
			$('#searchagentScore').attr("action",url);
			$('#searchagentScore').attr("target",targetIframe);
			$('#searchagentScore').submit();
			$('#dlg-export').dialog('open');
		}else{
			$("#iframe-export")[0].contentWindow.exportData();
			$('#dlg-export').dialog('close');
		}
	},
	resetForm : function(){
		$('#searchagentScore').form('reset');
		
		var data = $('#formDataType').combobox('getData');
		 $('#formDataType').combobox('select',data[0].value);//默认选中第一个
	
	},
	validateTime : function (params){
		var startTime = params.createStartTime;
		var endTime = params.createEndTime;
		
		if(startTime && endTime && startTime > endTime){
			$.messager.alert('提示','评分开始时间不能大于结束时间!');
			return false;
		}
		/*
		var startTimeLon = new Date(startTime).getTime();
		var lastMonthLon = new Date(endTime).getTime();
		var timeLon = (lastMonthLon - startTimeLon) / (1000*60*60*24);
		if(timeLon > 31){
			$.messager.alert('提示','评分开始时间和结束时间不能超过31天!');
			return false;
		}*/
		return true;
	}
	
};
 