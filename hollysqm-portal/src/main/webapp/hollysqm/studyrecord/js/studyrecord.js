var studyrecord = {
	/**
	 * 初始化加载数据
	 */
	init:function(){
		var param = holly.form2json($("#searchstudyrecord"));
		$("#studyrecorddatagrid").datagrid({
			fit:true,
			url:holly.getPath()+'/rest/studyrecordcount/recordCount',
			method:'get',
			singleSelect : true,
			border:false,
			fitColumns:true,
			queryParams:param,
			pagination: true,
			loadFilter: function(data){return (data.content)?data.content:data;},
			columns:[[
				{field:'userCode',title:'坐席',width:60,sortable:true},
				{field:'deptName',title:'所属部门',width:100},
				{field:'paperTotal',title:'质检单量',width:40,sortable:true},
				{field:'paperedTotal',title:'已质检量',width:40,sortable:true},
				{field:'attentionedTotal',title:'已关注量',width:40,sortable:true},
				{field:'paperedRate',title:'质检率',width:40,sortable:false},
				{field:'attentionedRate',title:'关注率',width:40,sortable:false}
			]],
			loadMsg:'数据加载中请稍后……',
			onLoadSuccess: function (data)  {
				if (data.rows.length == 0) {
					$("#exportTable").hide();//没有数据时隐藏导出按钮
					var body = $(this).data().datagrid.dc.body2;
					body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
				}else $("#exportTable").show();
			}
		});
		
		// 加载部门树
		$("#deptTree").tree({
			queryParams:{enabled:true},
			url: holly.getPath()+"/rest/org/findTree",
			method:"get",dnd:false,checkbox:true,
			loadFilter:function(data){return (data.content)?data.content:data;},
			onLoadSuccess:function(node,data){
					var arr=$("#deptTree").find("span.tree-checkbox");
						var root=$("#deptTree").tree("getRoot").target;
						$(root).find("span.tree-checkbox").remove();
						$.each(arr, function (index, v){
							var node=$("#deptTree").tree("getNode",$(this).parent());
							//选部门时如果节点是机构。则移除checkbox
							if(node && node.attributes && node.attributes.isOrg){
								$(this).remove();
							}
						});
						studyrecord.expandAll($("#deptTree"));
           		}
		});
	},

	
	/**
	 * 搜索
	 */
	search:function(){
		strReplace("agentCode");//,
		var param = holly.form2json($("#searchstudyrecord"));
		if(studyrecord.validateTime(param)){
			$("#studyrecorddatagrid").datagrid("search",{'param':param});
		}
	},
	exportPaper : function(toPage){
		var grid = $('#studyrecorddatagrid');  
		var options = grid.datagrid('getPager').data("pagination").options;  
		var total = options.total;
		if(total > export_total){
			$.messager.alert("提示","导出数据不能大于"+export_total+"，请继续筛选数据！");
			return false;
		}
		if(toPage){
			var url =holly.getPath()+"/rest/csv/wizard/sqlexport?tableId=csv.count.studyrecordcount&sqlKey=velocity.count.studyrecordcount&mediaId=1";
			var targetIframe ="iframe-export";
			$('#searchstudyrecord').attr("action",url);
			$('#searchstudyrecord').attr("target",targetIframe);
			$('#searchstudyrecord').submit();
			$('#dlg-export').dialog('open');
		}else{
			$("#iframe-export")[0].contentWindow.exportData();
			$('#dlg-export').dialog('close');
		}
	},
	resetForm : function() {
		$('#searchstudyrecord').form('reset');
	},
	validateTime : function (params){
		var startTime = params.createStartTime;
		var endTime = params.createEndTime;
		
		if(startTime && endTime && startTime > endTime){
			$.messager.alert('提示','评分开始时间不能大于结束时间!');
			return false;
		}
		/*
		var startTimeLon = new Date(startTime).getTime();
		var lastMonthLon = new Date(endTime).getTime();
		var timeLon = (lastMonthLon - startTimeLon) / (1000*60*60*24);
		if(timeLon > 31){
			$.messager.alert('提示','评分开始时间和结束时间不能超过31天!');
			return false;
		}*/
		return true;
	},
	/**
	 * 清空部门、工作组、个人dialog的选择条件或查询条件
	 */
	clearChoose:function(type){
		var nodes = $('#deptTree').tree("getChecked");
		for(var i=0;i<nodes.length;i++){
			$('#deptTree').tree('uncheck',nodes[i].target);
		}
	},
	/**
	 * 展开根节点或树的所有节点
	 * @param {} ele 如果存在表示指定树展开根节点
	 */
	expandAll:function(ele){
		if(ele){//展开指定的树
			var root=ele.tree("getRoot");
			if(root){ele.tree("expand",root.target);}
		}
	},

	/**
	 * 清空发送范围
	 */
	clear:function() {
		
		$("#departmentIds").val("");
		$("#deptTree").tree('uncheck',$("#deptTree").tree('check')).tree('collapseAll');
	},
	/**
	 * 保存所选部门、机构
	 */
	saveDepartment:function() {
		var nodes = $('#deptTree').tree('getChecked');
		departmentNames = '';
		departmentIds = "";
		for(var i=0; i<nodes.length; i++){
			if(!nodes[i].attributes.isOrg){
			if (departmentNames != '') {
			departmentNames += ',';
			}
			departmentNames += nodes[i].text;
			if(departmentIds != ''){
				departmentIds += ',';
			}
				departmentIds += "'"+nodes[i].id+"'";
			}
		}
		
		$("#sendeeNames").textbox('setValue',departmentNames);
		$("#deptId").val(departmentIds);
		$("#dlg-department").dialog('close');
	},
	clearDept : function(e){
		$(e.data.target).textbox('clear');
		$('#deptId').val("");
	}
};
/**
*	过滤特殊字符
*	str：要进行过滤的字符串
*/
function strReplace(id){
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
	var rs = "";
	var str = $("#"+id).textbox("getValue");
	if($.trim(str).length > 0 ){
		for (var i = 0; i < str.length; i++) {
			rs = rs+str.substr(i, 1).replace(pattern, '');
		}
		$("#"+id).textbox("setValue",rs);
	}
}
