<%@page import="com.hollycrm.hollybeacon.basic.util.AppConfigUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>员工学习记录</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type="text/javascript" src="${ctx}/hollysqm/studyrecord/js/studyrecord.js" ></script>
<%
Integer export_total = AppConfigUtils.getConfig().getInteger("system.export.total", 100000);
%>
<script type="text/javascript">
  var export_total = <%=export_total%>;
$(function() {
	studyrecord.init();
	$("#searchstudyrecord").keyup(function(event){
		if(event.keyCode==13){
			studyrecord.search();
		}
	});
});

</script> 

</head>
<body class="easyui-layout">
	<div data-options="region:'center',border:false" style="padding:10px;">
		<div class="easyui-layout" data-options="fit:true">
			<!-- 搜索区域 -->
			<div data-options="region:'north',border:false" style="padding-bottom:10px;">
				<form id="searchstudyrecord" method="post" class="search-form">
					<div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">创建时间</span>
							<div class="valueGroup">
								<input type="text" class="easyui-datebox" name="createStartTime" data-options="editable:false,width:'100%'">
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">至</span>
							<div class="valueGroup">
								<input type="text" class="easyui-datebox" name="createEndTime" data-options="editable:false,width:'100%'">
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">坐席</span>
							<div class="valueGroup">
								<input type="text" class="easyui-textbox" name="agentCode" id="agentCode" data-options="prompt:'请输入姓名或工号',editable:true,width:'100%'">
							</div>
						</span>
						
						<span class="screeningbar-item">
							<span class="label">&nbsp;</span>
							<div class="valueGroup">
								&nbsp;
							</div>
						</span>	
					</div>
					<div class="screeningbar">
						<span class="screeningbar-item" style="width: 75%">
							<span class="label">所属部门</span>
							<div class="valueGroup">
								<input class="easyui-textbox" id="sendeeNames" data-options="buttonText:'选择',prompt:'请选择部门...',editable:false,width:'100%',
								iconWidth: 22,
								icons: [{
									iconCls:'icon-remove',
									handler: studyrecord.clearDept
								}]
								">
								<input type="hidden" id="deptId" name="deptId" value="" />
							</div>
						</span>	
					</div>
					<div class="btn-group">
						<a href="javascript:;" class="operation search-btn" onclick="studyrecord.search()" title="搜索"></a>
						<a href="javascript:;" class="operation reset-btn" onclick="studyrecord.resetForm();" title="重置"></a>
					</div>
				</form>
			</div>
			<!-- 搜索区域 end -->	
			<div data-options="region:'center',border:false">
				<div id="tt">
					<a href="javascript:void(0);" class="easyui-linkbutton" id="exportTable" onclick="studyrecord.exportPaper(true)">导出</a>
				</div>
				<div class="easyui-panel" title="员工学习记录" data-options="tools:'#tt',fit:true">
					<table id="studyrecorddatagrid"></table>
				</div>
			</div>
		</div>
	</div>
	<!-- 导出 -->
	<div id="dlg-export" class="easyui-dialog" title="导出员工学习记录"
		data-options="
			closed:true,
			buttons: '#dlg-export-buttons',
			width:650,
			height:'95%'
		">
		<iframe id="iframe-export" name="iframe-export" src="" frameborder="0" style="width:100%;height:100%;display:block;"></iframe>
    </div>
	<div id="dlg-export-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red" onclick="studyrecord.exportPaper(false)">确定</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#dlg-export').dialog('close')">取消</a>
	</div>
	
	<!-- 选择部门 -->
	<div id="dlg-department" class="easyui-dialog" title="部门" style="padding:10px"
		data-options="
			width:500,
			height:'95%',
			closed:true,
			onClose:function(){studyrecord.clearChoose();},
			buttons: '#dlg-department-buttons'
		">
		<ul id="deptTree"></ul>
	</div>
	<div id="dlg-department-buttons">
		<a href="javascript:void(0)" onClick="studyrecord.saveDepartment();" class="easyui-linkbutton l-btn-red">确定</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#dlg-department').dialog('close')">取消</a>
	</div>
</body>
<script>
	$(function(){
		   $("#sendeeNames").textbox({onClickButton:function(){
		        $("#dlg-department").dialog("open");
		    }});
	});
</script>
</html>