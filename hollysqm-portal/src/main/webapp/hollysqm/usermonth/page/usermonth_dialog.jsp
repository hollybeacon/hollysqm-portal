<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>员工月度</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
<link rel="stylesheet" href="${ctx}/hollysqm/usermonth/css/usermonth.css">
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type= "text/javascript" src="${ctx}/hollysqm/usermonth/js/usermonth_dialog.js" ></script>
<script src="${ctx}/hollysqm/common/echarts/echarts.min.js"></script> 
<script type="text/javascript">
$(function() {
	
	var params = eval("("+holly.getUrlParams("params")+")");
	for(var p in params){
		$("#dialogform").append("<input type='hidden'  name='"+p+"' value='"+params[p]+"'/>");
	}
	eval(holly.getUrlParams("function")+"()");

});
</script>
</head>
<body>
	<form id="dialogform">
		
	</form>
	<div style="display: inline-block;width: 95%;">
			<div class="echarts2" id="ech3"></div>
	</div>
</body>
</html>