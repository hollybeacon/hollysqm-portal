<%@page import="org.apache.shiro.SecurityUtils"%>
<%@page import="com.alibaba.fastjson.JSONObject"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>员工月度</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
<link rel="stylesheet" href="${ctx}/hollysqm/usermonth/css/usermonth.css">
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type= "text/javascript" src="${ctx}/hollysqm/usermonth/js/usermonth.js" ></script>
<script src="${ctx}/hollysqm/common/echarts/echarts.min.js"></script> 
<c:set var="user" value="<%=JSONObject.toJSON(SecurityUtils.getSubject().getPrincipal())%>" />
<script type="text/javascript">

$(function() {
	usermonth.init();
	$("#searchusermonth").keyup(function(event){
		if(event.keyCode==13){
			usermonth.search();
		}
	});
});
</script>
</head>
<body>
	<div class="banner">
		<form id="searchusermonth" method="post" class="search-form">
			<div class="screeningbar">
				<span class="screeningbar-item">
							<span class="label">质检来源</span>
							<div class="valueGroup">
								<select name="dataType" id="formDataType" data-options="editable:false,width:'100%'"></select>	
							</div>
				</span>
				<span class="screeningbar-item">
							<span class="label">评分模板</span>
							<div class="valueGroup">
								<select name="standardId" id="formStandardId" data-options="editable:false,width:'100%'"></select>	
							</div>
				</span>
				<span class="screeningbar-item">
					<span class="label">坐席</span>
					<div class="valueGroup">
						<input name="agentCode" class="easyui-textbox" id="agentCode" value="${user.userName }" data-options="width:'100%',prompt:'请输入坐席姓名、工号或帐号'">
					</div>
				</span>
				
			</div>
			<div class="btn-group">
				<a href="javascript:;" class="operation search-btn" onclick="usermonth.search()" title="搜索"></a>
				<a href="javascript:;" class="operation reset-btn" onclick="usermonth.restForm();" title="重置"></a>
			</div>
		</form>
	</div>
	<div style="display: inline-block;width: 90%;">
		 <div class="chart">
			<div class="displayDiv">请查询加载满意度调查统计</div>
			<div class="echarts1" id="ech0"></div>
		</div>
		 <div class="chart">
			<div class="displayDiv">请查询加载评分项统计</div>
			<div class="echarts2" id="ech1"></div>
		</div>
		<div class="chart">
			<div class="displayDiv">请查询加载坐席成绩统计</div>
			<div class="echarts1" id="ech2"></div>
		</div>
	</div>
	
	<!-- 查看统计详情 -->
	<div id="dlg-statistics" class="easyui-dialog" title="提交差错"
		data-options="
			closed:true,
			buttons: '#dlg-statistics-buttons',
			width:'90%',
			height:'80%'
		">
			<iframe id="iframe-statistics" name="iframe-statistics" src="" frameborder="0" style="width:100%;height:100%;display:block;"></iframe>
    </div>
	<!-- <div id="dlg-statistics-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#dlg-statistics').dialog('close')">关闭</a>
	</div> -->
</body>
</html>