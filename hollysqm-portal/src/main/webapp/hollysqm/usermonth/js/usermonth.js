var usermonth = {
	ajaxCount: 2, //8条数据字典请求	
	init:function(){
		$("#formDataType").combobox("init_extend",{'codeType':'DATATYPE','hasAllOption':false,event:{
			onLoadSuccess:function(data){
				$(this).combobox('setValue',data[0].value);
				usermonth.ajaxFinished();
			}
		}});
		$("#formStandardId").combobox({
			url:holly.getPath()+'/rest/commonRest/queryStandardList',
			valueField:'value',
			textField:'name',
			method:"post",
			loadFilter:function(data){
				return data.content;
			},
			onLoadSuccess:function(data){
				$(this).combobox('setValue',data[0].value);
				usermonth.ajaxFinished();
			}
		});
	},
	initChart0 : function(params){
		var echartData = usermonth.initChartsData("/rest/userMonth/satisfactionStatistics","#ech0",params);
		if(!echartData) return false;
		var chart0 = echarts.init(document.getElementById('ech0'));  
		chart0.setOption( {  
		    title : {  
		        text: '满意度调查',
		        x:'left'  
		    },  
		    tooltip : {  
		        trigger: 'item',  
		        formatter: "{a} <br/>{b} : {c} ({d}%)"  
		    },  
		    legend: {  
		        orient : 'horizontal',  //布局方式,horizontal=水平，vertical=垂直
		        x : 'center',  
		        data:["未转接满意度", "满意", "一般", "不满意", "转接未参与调查"]//usermonth.XAxis(echartData)
		    },  
		    toolbox: {  
		        show : true,
		        padding:[5, 20, 5, 5],
		        feature : {  
		            mark : {show: true},  
		            dataView : {show: true, readOnly: false},  
		            restore : {show: true},  
		            saveAsImage : {show: true}  
		        }  
		    },  
		    calculable : true,  
		    series : [  
		        {  
		            name:'满意度调查',
		            clickable:false,//数据图形是否可点击，默认开启
		            type:'pie',  
		            radius : '55%',  //半径，支持绝对值（px）和百分比
		            center: ['50%', '60%'],  //圆心坐标，支持绝对值（px）和百分比
		            data: echartData,
		            itemStyle:{ 
		                normal:{ 
		                      label:{ 
		                        show: true, 
		                        formatter: '{b} : {c} ({d}%)' 
		                      }, 
		                      labelLine :{show:true} 
		                    } 
		            } 
		        }  
		    ]  
		}); 
	},
	/*
	initChart1 : function(params){
		var echartData = usermonth.initChartsData("/rest/userMonth/standartStatistics","#ech1",params);
		if(!echartData) return false;
		var chart1 = echarts.init(document.getElementById('ech1'));
		chart1.on('click', function (params) {
		 });
		chart1.setOption({
			 title: {
				text: '评分项明细'
			},
			tooltip: {
				trigger: 'item'
			},
		    toolbox: {
		        show : true,
		        padding:[5, 20, 5, 5],
		        feature : {
		        	myTool :{//自定义方法名必须以my开头
		                   show:true,//是否显示    
		                   title:'评分项年度统计',
		                   icon: 'image://../css/echart.png',
		                   option:{},    
		                   onclick:function(option1) {//点击事件,这里的option1是chart的option信息    
		                        usermonth.openScoreListStatistics(); 
		                   }    
		            },
		            mark : {show: true},//开启辅助线
		            dataView : {show: true, readOnly: false},//数据视图
		            //magicType : {show: true, type: ['line', 'bar']},//切换线形图、柱状图
		            restore : {show: true},//还原
		            saveAsImage : {show: true}//保存为图片
		            
		        }
		    },
			calculable : true,//是否启用拖拽重计算特性，默认关闭
			xAxis:  {
				type: 'category',//坐标轴类型，横轴默认为类目型'category'，纵轴默认为数值型'value'
				data: usermonth.XAxis(echartData),
				axisLabel:{
					//interval: '0',
					interval: function(index,data){
						return true;
					},
					rotate: 30,
					margin: 2,
					formatter:function(data){
						if(data.length>7){
							return data.substr(0,7)+"...";
						}
						return data;
					}
				}
			},
			yAxis: {
				type: 'value',
				axisLabel: {
					//formatter: '{value} °C' 纵坐标数量单位
				}
			},
			series: [
				{
					name:'数量',
					type:'bar',
					data:usermonth.YAxis(echartData),
				    itemStyle: {
						normal: {
							color: '#4bb77d',
							label : {show: true}
						}
					},
					markPoint : {
		                data : [
		                    {type : 'max', name: '最大值'},
		                    {type : 'min', name: '最小值'}
		                ]
		            }
			    }
			]
		});
	},*/
	initChart1 : function(params){
		/*var option = {
			    tooltip : {
			        trigger: 'axis',
			        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
			            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
			        }
			    },
			    legend: {
			        data:["2017-01", "2017-02", "2017-03", "2017-04", "2017-05", "2017-06", "2017-07", "2017-08", "2017-09", "2017-10", "2017-11", "2017-12"],
			        formatter:function(data){
			    		 return data.split("-")[1]+"月";
			    	 }
			    },
			    toolbox: {
			        show : true,
			        feature : {
			            mark : {show: true},
			            dataView : {show: true, readOnly: false},
			            magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
			            restore : {show: true},
			            saveAsImage : {show: true}
			        }
			    },
			    calculable : true,
			    xAxis : [
			        {
			            type : 'value'
			        }
			    ],
			    yAxis : [
			        {
			            type : 'category',
			            data : ["亲和力强、耐心、主动", "信息提供主动性与解决问题的能力", "正确登记服务请求", "注意聆听并适当回应", "吐字清晰、音量、语速适中", "信息提供或记录全面准确", "使用标准化的规范用语和礼貌用语", "知识、技能熟练程度", "尊重客户、适时安抚客户情绪", "有效确认客户理解", "灵活运用语言技巧", "有效提问、正确理解客户需求", "语言条理清晰、精练易懂"]
			        }
			    ],
			    series : [
			        {name:'2017-01',type:'bar',stack: '总量',itemStyle : { normal: {label : {show: true,formatter:function(data){return data.value==0?"":data.value;}}}},data:[1,0,0,0,0,0,0,0,0,0,0,0,12]},
			        {name:'2017-02',type:'bar',stack: '总量',itemStyle : { normal: {label : {show: true,formatter:function(data){return data.value==0?"":data.value;}}}},data:[2,0,0,0,0,0,0,0,0,0,0,0,11]},
			        {name:'2017-03',type:'bar',stack: '总量',itemStyle : { normal: {label : {show: true,formatter:function(data){return data.value==0?"":data.value;}}}},data:[3,0,0,0,0,0,0,0,0,0,0,0,10]},
			        {name:'2017-04',type:'bar',stack: '总量',itemStyle : { normal: {label : {show: true,formatter:function(data){return data.value==0?"":data.value;}}}},data:[4,0,0,0,0,0,0,0,0,0,0,0,9]},
			        {name:'2017-05',type:'bar',stack: '总量',itemStyle : { normal: {label : {show: true,formatter:function(data){return data.value==0?"":data.value;}}}},data:[5,0,0,0,0,0,0,0,0,0,0,0,8]},
			        {name:'2017-06',type:'bar',stack: '总量',itemStyle : { normal: {label : {show: true,formatter:function(data){return data.value==0?"":data.value;}}}},data:[6,0,0,0,0,0,0,0,0,55,0,0,7]},
			        {name:'2017-07',type:'bar',stack: '总量',itemStyle : { normal: {label : {show: true,formatter:function(data){return data.value==0?"":data.value;}}}},data:[7,0,0,1,1,1,0,3,0,2,0,0,6]},
			        {name:'2017-08',type:'bar',stack: '总量',itemStyle : { normal: {label : {show: true,formatter:function(data){return data.value==0?"":data.value;}}}},data:[8,0,0,0,0,0,0,0,0,10,0,0,5]},
			        {name:'2017-09',type:'bar',stack: '总量',itemStyle : { normal: {label : {show: true,formatter:function(data){return data.value==0?"":data.value;}}}},data:[9,0,0,0,0,0,0,0,0,12,0,0,4]},
			        {name:'2017-10',type:'bar',stack: '总量',itemStyle : { normal: {label : {show: true,formatter:function(data){return data.value==0?"":data.value;}}}},data:[10,0,0,0,0,0,0,0,0,0,0,0,3]},
			        {name:'2017-11',type:'bar',stack: '总量',itemStyle : { normal: {label : {show: true,formatter:function(data){return data.value==0?"":data.value;}}}},data:[11,0,0,0,0,0,0,0,0,0,0,0,2]},
			        {name:'2017-12',type:'bar',stack: '总量',itemStyle : { normal: {label : {show: true,formatter:function(data){return data.value==0?"":data.value;}}}},data:[12,0,0,0,0,0,0,0,0,0,0,0,1]}
			    ]
			};
		var chart1 = echarts.init(document.getElementById('ech1'));	   
		chart1.setOption(option);*/
		var echartData = usermonth.initChartsData("/rest/userMonth/standardDetailStatistics","#ech1",params);
		if(!echartData) return false;
		var chart1 = echarts.init(document.getElementById('ech1'));
		chart1.setOption({
			title: {
					text: '评分项明细'
			},
		    tooltip : {
		        trigger: 'axis',
		        axisPointer : {            // 坐标轴指示器，坐标轴触发有效
		            type : 'shadow'        // 默认为直线，可选为：'line' | 'shadow'
		        },
		        formatter: function(data){
		        	if(data.length>0){
		        		var a = "",b="",c=0,d=0;
		        		$.each(data,function(index,e){
		        			a = e.name;
		        			d = !e.value?0:e.value;
		        			if(e.seriesName)
		        				b += "<br/>"+e.seriesName+":"+d;
		        			c += d;
		        		});
		        		return a+":合计("+c+")"+b;
		        	}
		        }
		    },
		    legend: {
		    	 data:usermonth.legendData(echartData),
		    	 formatter:function(data){
		    		 return data.split("-")[1]+"月";
		    	 }
		    },
		    toolbox: {
		        show : true,
		        feature : {
		            mark : {show: true},
		            dataView : {show: true, readOnly: false},
		            //magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
		            restore : {show: true},
		            saveAsImage : {show: true}
		        }
		    },
		    calculable : true,
		    xAxis : [
				{
				    type : 'value'
				}
		    ],
		    yAxis : [
				{
		            type : 'category',
		            data : usermonth.XAxis(echartData,"id"),
		            axisLabel:{
						interval: '0',
						margin:8,
						formatter : function(data){
							if(data.length>7){
								return data.substr(0,7)+"...";
							}
							return data;
						}
		            },
		            textStyle :{
		            	align:"left"
		            }
				}
		    ],
		    series : usermonth.seriesData(echartData)
		});
	},
	initChart2 : function(params){
		var echartData = usermonth.initChartsData("/rest/userMonth/scoreStatistics","#ech2",params);
		if(!echartData) return false;
		var chart2 = echarts.init(document.getElementById('ech2'));
		 chart2.on('click', function (params) {
		 });
		chart2.setOption({
			 title: {
				text: '成绩趋势'
			},
			tooltip: {
				trigger: 'item'
			},
		    toolbox: {
		        show : true,
		        padding:[5, 20, 5, 5],
		        feature : {
		            mark : {show: true},//开启辅助线
		            dataView : {show: true, readOnly: false},//数据视图
		            //magicType : {show: true, type: ['line', 'bar']},//切换线形图、柱状图
		            restore : {show: true},//还原
		            saveAsImage : {show: true}//保存为图片
		        }
		    },
			calculable : true,//是否启用拖拽重计算特性，默认关闭
			xAxis:  {
				type: 'category',//坐标轴类型，横轴默认为类目型'category'，纵轴默认为数值型'value'
				data: usermonth.XAxis(echartData),
				axisLabel:{
					interval:0//显示全部横坐标,auto 为自动
				}
			},
			yAxis: {
				type: 'value',
				axisLabel: {
					//formatter: '{value} °C' 纵坐标数量单位
				}
			},
			series: [
				{
					name:'数量',
					type:'line',
					data:usermonth.YAxis(echartData),
				    itemStyle: {
						normal: {
							color: '#4bb77d',
							label : {show: true}
						}
					},
					markPoint : {
		                data : [
		                    {type : 'max', name: '最大值'},
		                    {type : 'min', name: '最小值'}
		                ]
		            },
		            markLine : {
		                data : [
		                    {type : 'average', name: '平均值'}
		                ]
		            }
			    }
			]
		});
	},
	ajaxFinished:function(){
		usermonth.ajaxCount --;
		if(usermonth.ajaxCount <= 0){// 所有数据字典请求全部完成，做该做的事情
			usermonth.search();
		}
	},
	search : function(){
		strReplace("agentCode");
		var params = holly.form2json($("#searchusermonth"));//获取form表单查询条件
		if(!$.trim(params['agentCode'])){
			$.messager.alert('提示','坐席不能为空!');
			return false;
		}
		$(".displayDiv").hide();
		usermonth.initChart0(params);
		usermonth.initChart1(params);
		usermonth.initChart2(params);
	},
	initChartsData : function(url,targetDiv,params){
		
		holly.showLoading(false,targetDiv);
		
		var content = null;
		holly.get(holly.getPath() + url,params,function (e){
			if(e.success)
				content = e.content;
			else{
				holly.showError("错误，原因:"+e.errorMessage);
			}
			holly.hideLoading(targetDiv);
		},true);
		return content;
	},
	XAxis : function(data,i){
		var a = new Array();
		$.each(data,function(index,e){
			var b = eval("e."+(!i?"name":i));
			if(a.indexOf(b) == -1)//去重复
				a.push( b);
					
		});
		return a;
	},
	YAxis : function(data){
		var a = new Array();
		$.each(data,function(index,e){
			a.push( e.value);
		});
		return a;
	},
	getId : function(data,name){
		var id = "";
		$.each(data,function(index,e){
			if(e.name == name){
				id =  e.id;
				return false;
			}
		});
		return id;
	},
	legendData : function(data){
		var a = new Array();
		$.each(data,function(index,e){
			if(a.indexOf(e.name) == -1)//去重复
				a.push( e.name);
		});
		return a;
	},
	seriesData : function(data){
		
		var a = new Array();
		var b = usermonth.legendData(data);
		var itemStyle = "itemStyle : { normal: {label : {show: true, position: 'insideRight',formatter:function(data){return data.value==0?'':data.value;}}}}";//柱状图样式
		$.each(b,function(i,d){
			var c = new Array();
			$.each(data,function(index,e){
				if(d == e.name){
					c.push(e.value);
				}
			});
			a.push(eval("({name:'" +d+"',type:'bar',stack: '总量',"+itemStyle+",data:["+c+"]})"));//stack多组数据的堆积图时使用
		});
		return a;
	},
	openScoreListStatistics:function(){//打开评分项和全年月份统计
		var params = holly.form2json($("#searchusermonth"));
		
		var url =holly.getPath()+"/hollysqm/usermonth/page/usermonth_dialog.jsp?function=usermonth.initChart3&params="+JSON.stringify(params);
		var targetIframe ="iframe-statistics";
		$('#searchusermonth').attr("action",url);
		$('#searchusermonth').attr("target",targetIframe);
		$('#searchusermonth').submit();
		$('#dlg-statistics').dialog({"title":"评分项"}).dialog('open');
	},
	restForm:function(){
		$("#searchusermonth").form('reset');
		var data = $("#formDataType").combobox('getData');
		$("#formDataType").combobox('setValue',data[0].value);
		data = $("#formStandardId").combobox('getData');
		$("#formStandardId").combobox('setValue',data[0].value);
	}
};

/**
*	过滤特殊字符
*	str：要进行过滤的字符串
*/
function strReplace(id){
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
	var rs = "";
	var str = $("#"+id).textbox("getValue");
	if($.trim(str).length > 0 ){
		for (var i = 0; i < str.length; i++) {
			rs = rs+str.substr(i, 1).replace(pattern, '');
		}
		$("#"+id).textbox("setValue",rs);
	}
}

