var usermonth = {
	
	initChart3 : function(){
		var echartData = usermonth.initChartsData("/rest/userMonth/standardDetailStatistics","#ech3");
		if(!echartData) return false;
		var chart3 = echarts.init(document.getElementById('ech3'));
		chart3.setOption({
		    tooltip : {
		        trigger: 'axis'
		    },
		    legend: {
		        data:usermonth.legendData(echartData)
		    },
		    toolbox: {
		        show : true,
		        feature : {
		            mark : {show: true},
		            dataView : {show: true, readOnly: false},
		            //magicType : {show: true, type: ['line', 'bar', 'stack', 'tiled']},
		            restore : {show: true},
		            saveAsImage : {show: true}
		        }
		    },
		    calculable : true,
		    xAxis : [
		        {
		            type : 'category',
		            boundaryGap : false,
		            data : usermonth.XAxis(echartData)
		        }
		    ],
		    yAxis : [
		        {
		            type : 'value'
		        }
		    ],
		    series : usermonth.seriesData(echartData)
		});
	},
	initChartsData : function(url,targetDiv){
		strReplace("agentCode");
		var params = holly.form2json($("#dialogform"));//获取form表单查询条件
		if(!$.trim(params['agentCode'])){
			$.messager.alert('提示','坐席不能为空!');
			return false;
		}
		$(".displayDiv").hide();
		holly.showLoading(false,targetDiv);
		
		var content = null;
		holly.get(holly.getPath() + url,params,function (e){
			if(e.success)
				content = e.content;
			else{
				holly.showError("错误，原因:"+e.errorMessage);
			}
			holly.hideLoading(targetDiv);
		},true);
		return content;
	},
	XAxis : function(data){
		var a = new Array();
		$.each(data,function(index,e){
			if(a.indexOf() == -1)//去重复
				a.push( e.name);
		});
		return a;
	},
	YAxis : function(data){
		var a = new Array();
		$.each(data,function(index,e){
			a.push( e.value);
		});
		return a;
	},
	legendData : function(data){
		var a = new Array();
		$.each(data,function(index,e){
			if(a.indexOf(e.id) == -1)//去重复
				a.push( e.id);
		});
		return a;
	},
	seriesData : function(data){
		var a = new Array();
		var b = usermonth.legendData(data);
		$.each(b,function(i,d){
			var c = new Array();
			$.each(data,function(index,e){
				if(d == e.id){
					c.push(e.value);
				}
			});
			a.push(eval("({name:'" +d+"',type:'line',stack: '总量',data:["+c+"]})"));
		});
		return a;
	}
};

/**
*	过滤特殊字符
*	str：要进行过滤的字符串
*/
function strReplace(id){
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
	var rs = "";
	var str = holly.$E(id).val();
	if($.trim(str).length > 0 ){
		for (var i = 0; i < str.length; i++) {
			rs = rs+str.substr(i, 1).replace(pattern, '');
		}
		holly.$E(id).val(rs);
	}
}

