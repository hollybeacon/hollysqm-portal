<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>质检员工作统计</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
<link rel="stylesheet" href="${ctx }/hollysqm/workcount/css/workcount.css">
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type="text/javascript" src="${ctx }/hollysqm/workcount/js/workcount.js" ></script>
<script type="text/javascript">
    var loginUser = ${loginUser};
	$(function() {
		workcount.init({"flag": 0});
		workcount.initInfo();
	});
</script>
</head>
<body>
	<div class="tab_item">
		<ul class="tab_ul clearfix">
			<li><a href="javascript:;"  class="tab_a" onclick="workcount.init({'flag':0});">本日</a></li>
			<li><a href="javascript:;"  class="tab_a" onclick="workcount.init({'flag':1});">本周</a></li>
			<li><a href="javascript:;"  class="tab_a" onclick="workcount.init({'flag':2});">本月</a></li>
			<li><a href="javascript:;"  class="tab_a" onclick="$('#dlg1').dialog('open');">自定义</a></li>
		</ul>
	</div>
	<div style="display: inline-block;width: 60%;">
		<div class="personal_info clearfix">
			<div class="header_info">
				<div class="person_header"></div>
				<i class="person_sex"></i>
				<span class="person_name"></span>
			</div>
		</div>
		<div class="score_box">
			<div class="score_title clearfix" >
				<div class="green_item"></div>
				<span class="item_text">我</span>
			</div>
			<div class="score_info clearfix" id="score_info">
			</div>
			<div class="score_title clearfix">
				<div class="green_item"></div>
				<span class="item_text">所有质检员平均值</span>
			</div>
			<div class="score_info clearfix" id="score_info1"> 
			</div>
		</div>
	</div>
	<div class="work_rank">
		<div class="chart_title clearfix" style="padding-left: 20px;padding-bottom: 20px">
			<div class="green_item"></div>
			<span class="item_text" >工作排名</span>
			<span class="rankbyscore">按质检量排名</span>
		</div>
		<div class="rank_info" id="rank_info"></div>
	</div>
	<div id="dlg1" class="easyui-dialog" title="自定义时间" style="width: 400px;height: 200px; padding: 10px" data-options="closed:true,buttons:'#dlg-buttons1'">
		<div class="iform-horizontal">
			<div class="screeningbar">
				<div class="icontrol-group">
					<span class="icontrol-label">开始时间</span>
					<div class="icontrols">
						<input type="text" id="startTime" class="easyui-datebox" data-options="editable:false,width:'100%'">
					</div>
				</div>
				<div class="icontrol-group">
					<span class="icontrol-label">至</span>
					<div class="icontrols">
						<input type="text" id= "endTime" class="easyui-datebox" data-options="editable:false,width:'100%'">
					</div>
				</div>
			</div>		
		</div>
		<div id="dlg-buttons1">
			<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red"onclick="workcount.custom();">确定</a> 
			<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#dlg1').dialog('close')">取消</a>
		</div>
</body>
<script>
	$(function(){
		var class_value = "1px solid #4bb77d";
		var class_id = "border-bottom";
		$(".tab_a").click(function(){
			$(this).addClass("a_active").css(class_id,class_value);
			$($(this).parent().siblings().children()).removeClass("a_active").css(class_id,"");//移除其他节点的样式
		});
		$($(".tab_a")[0]).addClass("a_active").css(class_id,class_value);//默认第一个选中样式
	});
</script>
</html>