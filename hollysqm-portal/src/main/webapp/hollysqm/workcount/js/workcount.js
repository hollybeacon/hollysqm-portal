var workcount ={
	init:function(param){
		holly.get(holly.getPath()+ "/rest/workcount/workRank",
				param,function(e){
	    		if(e.success){
					var rank_infoHtml = '';
					var rows = e.content;
					for(var i in rows){
						rank_infoHtml += '<div class="score_rank clearfix">';
						rank_infoHtml += '<i class="num no'+(e.content[i].rank>3?'4':e.content[i].rank)+'">';//前1、2、3名对应不能的样式
						rank_infoHtml +='<span class="rank_num">'+e.content[i].rank+'</span>';
						rank_infoHtml += '</i>';
						rank_infoHtml += '<i class="header" '+(e.content[i].photo ? 'style="background-image:url('+holly.getResource(e.content[i].photo,true)+')"' : '')+'></i>';
						rank_infoHtml += '<span class="CS_name">'+e.content[i].userName+'</span>';
						rank_infoHtml += '<span class="CS_score">'+e.content[i].total+'</span>';
						rank_infoHtml += '</div>';
					}
					
					$('#rank_info').html(rank_infoHtml);
				}else{
					holly.showError(e.errorMessage);
				}
		});
		holly.get(holly.getPath()+"/rest/workcount/workCount",
				param,function(e){
	    		if(e.success){
	    			var rows = e.content;
					var score_infoHtml = '';
					
					for(var i in rows){
						var statusName = statusMap[i.replace("status","")];
						score_infoHtml += '<div class="score_combo">';
						score_infoHtml += '<div class="combo_score">'+ rows[i]+'</div>';
						score_infoHtml +='<div class="combo_text">'+(statusName?statusName:"总计")+'</div>';
						score_infoHtml += '</div>';
					}
					
					$('#score_info').html(score_infoHtml);
				}else{
					holly.showError(e.errorMessage);
				}
		});
		holly.get(holly.getPath()+"/rest/workcount/averWorkCount",param,function(e){
	    		if(e.success){
	    			var rows = e.content;
					var score_info1Html = '';
					
					for(var i in rows){
						var statusName = statusMap[i.replace("status","")];
						score_info1Html += '<div class="score_combo">';
						score_info1Html += '<div class="combo_score">'+ rows[i]+'</div>';
						score_info1Html +='<div class="combo_text">'+(statusName?statusName:"总计")+'</div>';
						score_info1Html += '</div>';
					}
					$('#score_info1').html(score_info1Html);
				}else{
					holly.showError(e.errorMessage);
				}
		});
	},
	custom : function(){//自定义
		var startTime = $("#startTime").datebox("getValue");
		var endTime = $("#endTime").datebox("getValue");
		if(!startTime){
			$.messager.alert('提示','开始时间不能为空!');
			return false;
		}
		if(!endTime){
			$.messager.alert('提示','结束时间不能为空!');
			return false;
		}
		
		if(startTime > endTime){
			$.messager.alert('提示','开始时间不能大于结束时间!');
			return false;
		}
		var startTimeLon = new Date(startTime).getTime();
		var lastMonthLon = new Date(endTime).getTime();
		var timeLon = (lastMonthLon - startTimeLon) / (1000*60*60*24);

		if(timeLon > 90){
			$.messager.alert('提示','开始时间和结束时间不能超过90天!');
			return false;
		}
		
		workcount.init({"startTime":startTime,"endTime":endTime,"flag":3});
		$('#dlg1').dialog('close');
	},
	getdictnames : function(param){
		var res=null;
		holly.get(holly.getPath()+"/rest/dictionary/getDictionaryNamesRest",param,function(e){
			if(e.success){//先确认检查通过！
				res = e.content;
			}else{
				holly.showError("错误，原因:"+e.errorMessage);
			}
		},true);
		return res;
	},
	initInfo : function(){
		holly.get(holly.getPath()+"/rest/user/showUpdateRest",{id: loginUser.id},function(e){
			if(e.success && e.content){
				with(e.content){
					$(".person_name").text(userName);//sex,photo
					if(photo){
						$(".person_header").css({"background-image":"url("+holly.getResource(photo,true)+")"});
					}else{
						$(".person_header").css("background-image","url("+holly.getPath()+"/hollysqm/common/images/header.jpg)");
					}
					if(sex == 0){
						$(".person_sex").addClass("male");
					}else{
						$(".person_sex").addClass("female");
					}
				}
			}
		});
	}
};
var statusMap = workcount.getdictnames({'codeType':'QUALITY_STATUS'});