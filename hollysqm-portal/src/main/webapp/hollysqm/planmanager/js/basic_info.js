var basic_info = {
	/**
	 * 初始化加载数据
	 */
	planId:holly.getUrlParams('planId'),
	frameId:holly.getUrlParams("frameId")?holly.getUrlParams("frameId"):"",
	init:function(){
		$("#formDataType").combobox("init_extend",{'codeType':'DATATYPE','hasAllOption':false,"event":{
			onLoadSuccess:function(){
				$(this).combobox('setValue','v8');
			}
		}});
		$("#formExport").combobox({
			url:holly.getPath() + "/rest/dictionary/getDictionaryByCodeType?codeType=IS_EXPORT",
			valueField:'value',
			textField:'name',
			method:"get",
			loadFilter:function(data){
				return data.content;
			},
			onLoadSuccess:function(){
				var data = $(this).combobox('getData');
				$(this).combobox('setValue',data[0].value);
			},
			onChange:function(newValue,oldValue){
				if(newValue == '1'){
					$('#checkeritem').hide();
				}else if(newValue == '0'){
					$('#checkeritem').show();
				}
			}
		});
		$("#formTimer").combobox("init",{'codeType':'TASK_TIMER'});
		$("#formStandardId").combobox({
			url:holly.getPath()+'/rest/commonRest/queryStandardList?status=1',
			valueField:'value',
			textField:'name',
			method:"post",
			loadFilter:function(data){
				var jsonArray=new Array();
				var defaultVal = {"value":"","name":'请选择'};
				jsonArray.push(defaultVal);
				for(var o in data.content){
					jsonArray.push(data.content[o]);
				}
				return jsonArray;
			},
			onLoadSuccess:function(){
				basic_info.standardIdData = $(this).combobox('getData');
			}
		});
		$("#formState").combobox("init_extend",{'codeType':'STATUS','hasAllOption':false});
		holly.get(holly.getPath() + "/rest/dictionary/getDictionaryByCodeType?codeType="+"EXEMPTION",{},function(e){
			if(e.success){//先确认检查通过！
				var res = e.content;
				var content = "";
				for(var o in res){
					content += "<label class='extend-input radio'><input type='radio' name='exemption' value="+res[o].value+" "+(res[o].value=='0'?'checked':'')+"> "+res[o].name+"</label>";
				}
				$('#exemptionDiv').html(content);
			}else{
				holly.showError("错误，原因:"+e.errorMessage);
			}
		},true);
		holly.post(holly.getPath()+"/rest/planRest/addOrEditBaseInformation",{"planId":basic_info.planId},function(e){
			if(e.success){
				if(e.content){
					if(e.content.planId){
						$('#planEditTitle').html('编辑质检计划');
					}else{
						$('#planEditTitle').html('新建质检计划');
						$('#formDataType').combobox('readonly',false);
						$('#formExport').combobox('readonly',false);
					}
					if(e.content.dataType != null){
						$('#formExport').combobox('setValue',e.content.isExport);
						$('#basic_infoform').form('load',e.content);
						var flag = false;
						$.each(basic_info.standardIdData,function(index,item){
							if(e.content.standardId==item.value){
								flag = true;
							}
						});
						if(flag==false){
							$("#formStandardId").combobox('setValue','');
						}
					}
				}
			}else{ 
				holly.showError(e.errorMessage);
			}
		});
	},
	
	
	
	formSubmit:function(){
		if($('#basic_infoform').form('enableValidation').form("validate")){
			var dataType = $("#formDataType").combobox('getValue');
			$('#basic_infoform').form('submit',{
				url:holly.getPath()+"/rest/planRest/saveBaseInfomation",
				success : function(e) {
					var d=($.type(e)=="string")?$.parseJSON(e):e;
					if(d.success){
						if(dataType=='v8'){
							if($("#formExport").combobox('getValue') == '0') {
								var url = holly.getPath()+'/hollysqm/planmanager/page/set_rules_v8.jsp';
							} else {
								var url = holly.getPath()+'/hollysqm/planmanager/page/set_rules_export_v8.jsp';
							}
		 					
						}else if(dataType=='i8'){
		 					var url = holly.getPath()+'/hollysqm/planmanager/page/set_rules_i8.jsp';
						}
						url += '?frameId=' + basic_info.frameId;
						window.location = url;
					}else{
						holly.showError(d.errorMessage);
					}
				}
			});
		}
	}

} 
	 