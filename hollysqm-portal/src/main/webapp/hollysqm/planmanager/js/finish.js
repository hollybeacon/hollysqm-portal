var finish = {
	content:"", //计划详情数据
	frameId:holly.getUrlParams("frameId")?holly.getUrlParams("frameId"):"",
	
	/**
	 * 初始化加载数据
	 */
	
	viewParam:{},
	init:function(){
		// 初始化预览dialog
		$('#dlg-view').dialog({
			href:holly.getPath()+"/hollysqm/planmanager/page/dlg-view.jsp",
			onOpen:function(){
				$('body').css('overflow','hidden');
			},
			onClose:function(){
				$('body').css('overflow','auto');
				// 关闭dialog停止播放语音
				if(listenTape.player.pause){
					listenTape.player.pause();
				}
			}
		});
		$('#cxsearch').searchbox({
			searcher:function(value,name){
				value = $.trim(value);
				finish.workerSearch(value,finish.content.agents,'#cxgroplistpane');
			},
			height:30,
			width:228,
			prompt:'按姓名或账号搜索'
		}).textbox('addClearBtn', {
			iconCls:'icon-clear',
			handler: function(){
				finish.workerSearch('',finish.content.agents,'#cxgroplistpane');
			}
		});
		$("#zjysearch").searchbox({
			searcher:function(value,name){
				value = $.trim(value);
				finish.workerSearch(value,finish.content.checkers,'#zjygroplistpane');
			},
			height:30,
			width:228,
			prompt:'按姓名或账号搜索'
		}).textbox('addClearBtn', {
			iconCls:'icon-clear',
			handler: function(){
				finish.workerSearch('',finish.content.checkers,'#zjygroplistpane');
			}
		});

		holly.post(holly.getPath()+"/rest/planRest/showPlanDetail",{},function(e){
			if(e.success){
				if(e.content.planId){
					$('#planEditTitle').html('编辑质检计划');
				}else{
					$('#planEditTitle').html('新建质检计划');
				}
				// 修改数据结构
				finish.content = e.content;
				$.each(e.content.agents,function(index,item){
					if(index==0){
						finish.viewParam.agent=item.userCode;
					}else{
						finish.viewParam.agent+=","+item.userCode;
					}
				});
				finish.viewParam.dataType=e.content.dataType;
				var extractMethod = e.content.extractMethod.split('-');
				$('#extractMethod').html('每个坐席' + finish.paramJson(extractMethod[0],'EXTRACT_PERIOD') + '抽取' + extractMethod[1] + '条');
				if(e.content) {
					holly.post(holly.getPath()+"/rest/planRest/getPlanExtractDetail",{planId:e.content.planId},function(e1){
						if(e1.success) {
							if(e1.content) {
								$('#extractDetail').html('抽检人数:' + e1.content.EXTRACTUSERNUM + ',' + '抽检组数:' + e1.content.EXTRACTDEPTNUM);
							}
						}
						
					});
				} 
				
				
				var paramJson = $.parseJSON(e.content.paramJson);
				finish.paramJsonHtml(paramJson,e.content.textItemId);

				var agents_departmentArr = new Array();
				var agentsArr = new Array();
				var agents_map = new Array();
				var cxgropHtml = "";//坐席列表html
				for(var i in e.content.agents){
					//检索department在该数组中是否存在，不存在才会允许添加到departmentArr
					if(agents_departmentArr.indexOf(e.content.agents[i].department)==-1){
						agents_departmentArr.push(e.content.agents[i].department);
					}
				}
				for(var i in agents_departmentArr){
					// 从e.content.agents中过滤出含有departmentArr的数据
					var dept = e.content.agents.filter(function(n){
						return n.department==agents_departmentArr[i];
					});
					agentsArr.push(dept);
				}
				agents_map = $.map(agentsArr,function(item,index){
					return{
						department:agentsArr[index][0].department,
						deptName:agentsArr[index][0].deptName,
						children:item
					}
				});

				cxgropHtml = finish.workerHtmlList(agents_map);
				$('#cxgroplistpane').html(cxgropHtml);


				var checkers_departmentArr = new Array();
				var checkersArr = new Array();
				var checkers_map = new Array();
				var zjygropHtml = "";//坐席列表html
				for(var i in e.content.checkers){
					//检索department在该数组中是否存在，不存在才会允许添加到departmentArr
					if(checkers_departmentArr.indexOf(e.content.checkers[i].department)==-1){
						checkers_departmentArr.push(e.content.checkers[i].department);
					}
				}
				for(var i in checkers_departmentArr){
					// 从e.content.agents中过滤出含有departmentArr的数据
					var dept = e.content.checkers.filter(function(n){
						return n.department==checkers_departmentArr[i];
					});
					checkersArr.push(dept);
				}
				checkers_map = $.map(checkersArr,function(item,index){
					return{
						department:checkersArr[index][0].department,
						deptName:checkersArr[index][0].deptName,
						children:item
					}
				});

				zjygropHtml = finish.workerHtmlList(checkers_map);
				$('#zjygroplistpane').html(zjygropHtml);

				var jibenxinxiHtml = '';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">质检来源</div>';
				jibenxinxiHtml += '<div class="icontrols">'+dataTypeMap[e.content.dataType]+'</div>';
				jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">质检计划</div>';
				jibenxinxiHtml += '<div class="icontrols">'+e.content.planName+'</div>';
				jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">执行周期</div>';
				jibenxinxiHtml += '<div class="icontrols">'+(e.content.taskTimer?taskTimerMap[e.content.taskTimer]:"")+'</div>';
				jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">评分模板</div>';
				jibenxinxiHtml += '<div class="icontrols">';
				$.each(standardMap,function(index,item){
					if(item.value == e.content.standardId){
						jibenxinxiHtml += item.name;
					}
				});
				jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '</div>';
				// jibenxinxiHtml += '<div class="icontrol-group">';
				// jibenxinxiHtml += '<div class="icontrol-label">优秀免检</div>';
				// jibenxinxiHtml += '<div class="icontrols">';
				// if(e.content.exemption){
				// 	jibenxinxiHtml += e.content.exemption=='0'?'否':'是';
				// }
				// jibenxinxiHtml += '</div>';
				// jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">启用状态</div>';
				jibenxinxiHtml += '<div class="icontrols">'+stateMap[e.content.status]+'</div>';
				jibenxinxiHtml += '</div>';
				$('#jibenxinxi').html(jibenxinxiHtml);
			}else{
				holly.showError(e.errorMessage);
			}
		});
	},
	/**
	 * 返回坐席列表html
	 * @param map 数据 { array }
	**/
	workerHtmlList:function(map){
		var html = "";
		$.each(map, function(index, item){
			html += '<div class="groupitem">';
			html += '<div class="title">'+item.deptName+'</div>';
			html += '<ul class="list">';
			$.each(item.children, function(index1, item1){
				html += '<li title="'+item1.username+'('+item1.userCode+')">'+item1.username+'('+item1.userCode+')</li>';
			});
			html += '</ul>';
			html += '</div>';
		});
		return html;
	},
	paramJson:function(param,getdictname){
		var text = '';
		if(param){
			var map = $("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':getdictname}});
			$.each(param.split(","), function(index, item){
				var itemText = map[item.replace(/\'/g,'')];
				if(itemText){
					text += (index==0?'':',')+itemText;
				}
			});
		}
		return text?text:"";
	},
	paramJsonHtml:function(paramJson,textItemId){
		if(finish.viewParam.dataType=='v8'){
			holly.post(holly.getPath()+'/rest/commonRest/getServiceTypeNames',{"serviceTypeId":paramJson.serviceType},function(e1){
				if(e1.success){
					if(e1.content){
						var serviceTypeArr = [];
						$.each(e1.content,function(index,item){
							serviceTypeArr.push(item.text);
						});
						finish.serviceTypeText = serviceTypeArr.join('<br/>');
					}
				}
			},true);
			var paramJsonHtml = '<div class="icontrol-group"><div class="icontrol-label maincolor">关键词</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.words+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">通话时长</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.recoinfoLength+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">静音时长</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.silenceLength+'秒</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">关键词适配对象</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.wordType,'V8_WORDTYPE')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">业务类型</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.businessType,'BUSINESS_TYPE')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">服务类型</div>';
			paramJsonHtml += '<div class="icontrols">'+(finish.serviceTypeText?finish.serviceTypeText:"")+'</div>';
			paramJsonHtml += '</div>';
			// paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">分配策略</div>';
			// paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.allotMethod,'V8ALLOTMETHOD')+'</div>';
			// paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">满意度</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.satisfication,'V8SATISFACTION')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">用户级别</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.custLevel,'CUST_LEVEL')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">号码归属地</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.custArea,'CUST_AREA')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">用户品牌</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.custBand,'CUST_BAND')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">日期规则</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.dateRule,'DATE_RULE')+'</div>';
			paramJsonHtml += '</div>';
			if(paramJson.dateRule == '2') {
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">时间范围</div>';
				paramJsonHtml += '<div class="icontrols">'+paramJson.dayNumber+'</div>';
				paramJsonHtml += '</div>';
			} else {
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">人工来电开始时间</div>';
				paramJsonHtml += '<div class="icontrols">'+finish.formatTime(paramJson.startTime)+'</div>';
				paramJsonHtml += '</div>';
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">人工来电结束时间</div>';
				paramJsonHtml += '<div class="icontrols">'+finish.formatTime(paramJson.endTime)+'</div>';
				paramJsonHtml += '</div>';
			}
			
			finish.viewParam.businessType = paramJson.businessType;
			finish.viewParam.custArea = paramJson.custArea;
			finish.viewParam.custBand = paramJson.custBand;
			finish.viewParam.custLevel = paramJson.custLevel;
			finish.viewParam.endTime = paramJson.endTime;
			finish.viewParam.recoinfoLength = paramJson.recoinfoLength;
			finish.viewParam.satisfaction = paramJson.satisfication;
			finish.viewParam.startTime = paramJson.startTime;
			finish.viewParam.wordType = paramJson.wordType;
			finish.viewParam.words = paramJson.words;
			finish.viewParam.silenceLength = paramJson.silenceLength;
		}else if(finish.viewParam.dataType=='i8'){
			var paramJsonHtml = '<div class="icontrol-group"><div class="icontrol-label maincolor">关键词</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.words+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">会话时长</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.sessionLength+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">关键词适配对象</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.wordType,'I8_WORDTYPE')+'</div>';
			paramJsonHtml += '</div>';
			/*paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">服务类型</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.serviceType,'I8_SERVICE_TYPE')+'</div>';
			paramJsonHtml += '</div>';*/
			/*paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">分配策略</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.allotMethod,'I8ALLOTMETHOD')+'</div>';
			paramJsonHtml += '</div>';*/
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">满意度</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.satisfaction,'I8SATISFACTION')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">关闭方式</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.closeType,'CLOSE_TYPE')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">会话等级</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.sessionType,'SESSION_TYPE')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">日期规则</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.dateRule,'DATE_RULE')+'</div>';
			paramJsonHtml += '</div>';
			if(paramJson.dateRule == '2') {
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">时间范围</div>';
				paramJsonHtml += '<div class="icontrols">'+paramJson.dayNumber+'</div>';
				paramJsonHtml += '</div>';
			} else {
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">会话开始时间</div>';
				paramJsonHtml += '<div class="icontrols">'+finish.formatTime(paramJson.startTime)+'</div>';
				paramJsonHtml += '</div>';
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">会话结束时间</div>';
				paramJsonHtml += '<div class="icontrols">'+finish.formatTime(paramJson.endTime)+'</div>';
				paramJsonHtml += '</div>';
			}
		
			finish.viewParam.closeType = paramJson.closeType;
			finish.viewParam.endTime = paramJson.endTime;
			finish.viewParam.satisfaction = paramJson.satisfaction;
			//finish.viewParam.serviceType = paramJson.serviceType;
			finish.viewParam.sessionLength = paramJson.sessionLength;
			finish.viewParam.sessionType = paramJson.sessionType;
			finish.viewParam.startTime = paramJson.startTime;
			finish.viewParam.wordType = paramJson.wordType;
			finish.viewParam.words = paramJson.words;
		}
		paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">标签</div>';
		paramJsonHtml += '<div class="icontrols" id="textItemName"></div>';
		paramJsonHtml += '</div>';
		$('#paramJsonDiv').html(paramJsonHtml);
		if(textItemId){
			finish.viewParam.textItemId = textItemId;
			holly.get(holly.getPath()+"/rest/commonRest/queryTextItem",{},function(e){
				if(e.success){
					var item_contentHtml = "";
					$.each(e.content,function(index,item){
						if(textItemId==item.textItemId){
							item_contentHtml += ' <div class="item_combo">';
							item_contentHtml += '<label class="label"><span class="text">'+item.itemName+'</span><a href="javascript:void(0)" class="item_icon" onclick="finish.displaysubMenu(this,event)"></a></label>';
							item_contentHtml += '<div class="ul_list" onclick="event.stopPropagation()">';
							item_contentHtml += '<div class="ul_inside">';  
							item_contentHtml += '<div class="item-group clearfix">';
							item_contentHtml += '<div class="item-label">'+item.itemName+'匹配规则</div>';
							item_contentHtml += '<div class="item-val">'+item.content+'</div>';
							item_contentHtml += '</div>';
							item_contentHtml += '</div>';
							item_contentHtml +=  '<div class="item-angle"></div>'
							item_contentHtml += '</div>';
							item_contentHtml += '</div>';
							//$('#textItemName').html(item.itemName);
							$('#textItemName').html(item_contentHtml);
						}
					});
				}
			});
		}
	},
	/**
	 * 搜索坐席
	 * @param keyword 关键词 { string }
	 * @param content 数据 { array }
	 * @param id 要填充的div id名 { string }
	**/
	workerSearch:function(keyword,content,id){
		var departmentArr = new Array();
		var searchArr = new Array();
		var searchMap = new Array();
		var html = "";//坐席列表html
		for(var i in content){
			if(departmentArr.indexOf(content[i].department)==-1){
				if(content[i].username.indexOf(keyword)>=0 || content[i].userCode.indexOf(keyword)>=0){
					departmentArr.push(content[i].department);
				}
			}
		}
		if(departmentArr.length > 0){
			for(var i in departmentArr){
				var dept = content.filter(function(n){
					if(n.username.indexOf(keyword)>=0 || n.userCode.indexOf(keyword)>=0){
						return n.department==departmentArr[i];
					}
				});
				searchArr.push(dept);
			}
			searchMap = $.map(searchArr,function(item,index){
				return{
					department:searchArr[index][0].department,
					deptName:searchArr[index][0].deptName,
					children:item
				}
			});
			html = finish.workerHtmlList(searchMap);
			$(id).html(html);
		}else{
			$(id).html('<div class="nodata">暂无搜索结果</div>');
		}
	},
	jumpBasicPage:function(){
		var url = holly.getPath()+'/hollysqm/planmanager/page/basic_info.jsp';
		url += '?frameId=' + finish.frameId;
		window.location = url;
	},
	
	displaysubMenu:function(obj,event) {
		$(".ul_list").hide();
		event.stopPropagation();
		var $this = $(obj);
		var ul_listDiv = $(obj).closest('.item_combo').children('.ul_list');
		// var x = $this.offset().left;
		// var y = $this.offset().top; 
		// var z = document.body.clientWidth;
		// var w = ul_listDiv.outerWidth();
		
		var left = $(obj).closest('.label').outerWidth()+110;
		ul_listDiv.toggle().css({'left':left});
		$(document).on('click',finish.docDisplaysubMenu);
	},
	
	docDisplaysubMenu:function(){
		$(".ul_list").hide();
		$(document).off('click',finish.docDisplaysubMenu);
	},
	
	isContains:function(array,needle) {
		 for (i in array) {
			    if (array[i] == needle) return true;
			  }
			  return false;
	},
	
	formatTime:function(time) {
		var year = time.substr(0,4);
		var month = time.substr(4,2);
		var day = time.substr(6,2);
		var hour = time.substr(8,2);
		var minute = time.substr(10,2);
		var second = time.substr(12,2);
		return year + '-' + month + '-' + day + ' ' + hour + ":" + minute + ':' + second;
	},
	formSubmit:function(){
		holly.post(holly.getPath()+'/rest/planRest/saveOrUpdatePlan',{},function(e){
			if(e.success){
				holly.showSuccess('保存成功');
				// 如果待复核列表页面没有关闭，并且该页面是从待复核列表打开的
				// if(parent.$("#"+finish.frameId).length>0){
				// 	var $obj = parent.$("#"+finish.frameId)[0].contentWindow;
				// 	$obj.planlist.search();//重新搜索列表
				// }
				// var $tabs = parent.$('#tabs');
				// var curTab = $tabs.tabs('getSelected');
				// var tabIndex = $tabs.tabs('getTabIndex',curTab);
				// $tabs.tabs('close',tabIndex);

				//回跳质检计划页面
                var url = holly.getPath() + "/hollysqm/planmanager/page/planlist.jsp?frameId=" + finish.frameId;
                window.location = url;
			}else{
				holly.showError(e.errorMessage);
			}
		});
	}
}