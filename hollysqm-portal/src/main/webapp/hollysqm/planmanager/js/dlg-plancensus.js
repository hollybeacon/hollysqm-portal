var dlgPlancensus = {
	init:function() {
		
		$("#censusDiv").datagrid({
			fit:true,
			url:holly.getPath()+'/rest/planRest/getPlanCensus',
			method:'get',
			singleSelect : true,
			border:false,
			fitColumns:true,
			queryParams:{'planId':planId},
			pagination: true,
			loadFilter: function(data){return (data.content)?data.content:data;},
			columns:[[
				{field:'planName',title:'计划名称',width:120},
				{field:'isExport',title:'抽取类型',width:80,formatter:dlgPlancensus.extractType},
				{field:'agentCode',title:'坐席工号',width:60},
				{field:'checkerAgentCode',title:'质检员工号',width:70},
				{field:'planExtractNum',title:'计划抽取量',width:60},
				{field:'hasExtractNum',title:'实际抽取量',width:80},
				{field:'lastExtractNum',title:'未抽取的量',width:60}
			]],
			loadMsg:'数据加载中请稍后……',
			onLoadSuccess: function (data)  {
				if (data.rows.length == 0) {
					var body = $(this).data().datagrid.dc.body2;
					body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
				}
			}
		});	
	},
	
	extractType:function extractType(val, row, index) {
		if(val == 0 || val == '0') {
			return '非导入模式';
		} else {
			return '导入模式';
		}
	}
}