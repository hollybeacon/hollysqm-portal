var checker = {
	/**
	 * 初始化加载数据 
	 */
	dataType:holly.getUrlParams("dataType")?holly.getUrlParams("dataType"):"",
	frameId:holly.getUrlParams("frameId")?holly.getUrlParams("frameId"):"",
	init:function(){
		holly.post(holly.getPath()+"/rest/planRest/setPlanChekers",{},function(e){
			if(e.success){
				if(e.content){
					if(e.content.planId){
						$('#planEditTitle').html('编辑质检计划');
					}else{
						$('#planEditTitle').html('新建质检计划');
					}
					if(e.content.checkers != null){
						assignUser.moveToUser(e.content.checkers, "checker");
					}else{
						assignUser.moveToUser('', "checker");
					}
				}
			}
		});
	},
	formSubmit:function(page){
		var param = {
			checker: $('input[name="checker"]').val()
		}
		if(page=="next"){
			if($('#targetList > li').length==0){
				holly.showError('请选择质检员');
			}else{
				holly.post(holly.getPath()+'/rest/planRest/savePlanCheckers',param,function(e){
					if(e.success){
						var url = holly.getPath()+'/hollysqm/planmanager/page/finish.jsp';
						url += '?frameId=' + checker.frameId;
						window.location = url;
					}else{
						holly.showError(e.errorMessage);
					}
				});
			}
		}else if(page=="last"){
			holly.post(holly.getPath()+'/rest/planRest/savePlanCheckers',param,function(e){
				if(e.success){
					if(checker.dataType=='v8'){
						var url = holly.getPath()+'/hollysqm/planmanager/page/set_rules_v8.jsp';
					}else if(checker.dataType=='i8'){
						var url = holly.getPath()+'/hollysqm/planmanager/page/set_rules_i8.jsp';
					}
					url += '?frameId=' + checker.frameId;
					window.location = url;
				}else{
					holly.showError(e.errorMessage);
				}
			});
		}
	}
}
 
 