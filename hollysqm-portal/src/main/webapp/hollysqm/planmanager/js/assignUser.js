/**
 * 分配用户
 * @type
 */
var assignUser = {
	eleName:'',
	/**
	 * 加载部门,工作组,个人dialog等初始化操作
	 */
	loadBind:function(){
		// 加载个人树
		$("#personTree").tree({
			queryParams:{enabled:true},
			url: holly.getPath() + "/rest/org/findTree",
			method:"get",dnd:false,
			loadFilter:function(data){return (data.content)?data.content:data;},
			onClick:function(node){
				assignUser.searchUser();
			},
			onLoadSuccess:function(node,data){
				assignUser.expandAll($("#personTree"));
			}
		});
		$("input[name='showStopDic']").change(assignUser.searchDept);
		$("#searchName").searchbox({prompt:'按姓名搜索',iconWidth:22,searcher:assignUser.searchUser}).textbox('addClearBtn', {
			iconCls:'icon-clear',
			handler: function(){
				assignUser.searchUser();
			}
		});
	},
	moveToUser:function(agents,eleName){
		assignUser.eleName=eleName;
		var html = '';
		$.each(agents, function (index, v) {
		 html+='<li><label class="select-item" userId="'+v.userCode+'" data-userCode="'+v.userCode+'">'+
				'<span class="name">'+v.username+'</span><span class="userCode">（'+v.userCode+'）</span>'+
				'</label><span class="opr-wrp"><i userId= "'+v.userCode+'" class="opr-icon icon-remove" onclick="assignUser.removeTarget(this);"></i></span></li>';
		});
		$("#targetList").html(html);
		assignUser.getUserCode();
	},
	/**
	 * 展开根节点或树的所有节点
	 * @param {} ele 如果存在表示指定树展开根节点
	 */
	expandAll:function(ele){
		if(ele){//展开指定的树
			var root=ele.tree("getRoot");
			if(root){ele.tree("expand",root.target);}
		}
	},
	/**
	 * 显示停用部门
	 */
	searchDept:function(){
		var params=$("input[name='showStopDic']").is(':checked')?{}:{enabled:true};
		$("#personTree").tree('options').queryParams=params;
		$("#personTree").tree('reload');
	},
	/**
	 * 查找部门下的用户
	 */
	searchUser:function(){
		var param={enabled:1};
		var orgNode=$("#personTree").tree("getSelected");
		if(orgNode){//将当前选中的部门或机构作为查询参数
			if(orgNode.attributes && orgNode.attributes.isOrg){
				param.orgId=orgNode.id;
			}else{
				param.deptId=orgNode.id;
				param.orgId=orgNode.attributes.rootId;
			}
		}
		param.orgId=(param.orgId)?param.orgId:loginUser.orgId;
		param.userName=$("#searchName").val();
		holly.post(holly.getPath()+"/rest/user/findUser",param,function(e){
			if(e.success){
				var html = '';
				$("#srcList").empty();
				$("#srcList").append("<li><label class='select-item selsrcAll'><input type='checkbox' class='select-check' onChange='assignUser.selAll(this);'><span>全选</span></label></li>");
				$.each(e.content, function (index, v) {
					if(v.id && v.userCode){
					  var toolsTip={deptName:v.deptName,orgName:v.orgName};
					  var tip="<div id='tip"+v.userCode+"' class='opt-dropdown-noline assign' data-options='minWidth:100'><div>所属部门：{deptName}</div></div>".replace(/{deptName}/g,toolsTip.deptName||"").replace(/{orgName}/g,toolsTip.orgName||"");
					  
					  html+='<li><label class="select-item" userId="'+v.userCode+'" data-userCode="'+v.userCode+'">'+'<input type="checkbox" userId="'+v.userCode+'" class="select-check" onChange="assignUser.assign(this);" >'+
							'<span class="name">'+v.userName+'</span><span class="userCode">（'+v.userCode+'）</span>'+
							'</label><span class="opr-wrp"><i class="easyui-menubutton opr-icon icon-info-sign" data-options="menu:\'#tip'+v.userCode+'\'"></i>'+tip+'</span></li>';
						
					}
				});
				$("#srcList").append(html);
				$('.easyui-menubutton').menubutton();
			}
		});
	},

	/**
	 * 选择人员时全选/取消全选
	 */
	selAll:function(obj) {
		if($(obj).is(':checked')==true){
			$.each($("#srcList > li:gt(0)"),function(i,v){
					var userId = $(v).find("label[userId]").attr("userId");
					$("#srcList .select-check").prop('checked',true);
					assignUser.moveToTarget(v, userId);
				});
		}
		else{
			$.each($("#srcList > li:gt(0)"),function(i,v){
					var userId = $(v).find("label[userId]").attr("userId");
					$("#srcList .select-check").prop('checked',false);
					assignUser.removeTarget_(v, userId);
				});
		}
		$('.easyui-menubutton').menubutton();
	},

	/**
	 * 选中人员时触发移动事件
	 */
	assign:function(obj){
		var src = $(obj).parents(".select-item").parent();
		var userId = $(obj).attr("userId");
		if($(obj).is(':checked')==true){
			assignUser.moveToTarget(src, userId);
			// 如果全部勾上了,就把全选勾上
			if($("#srcList li").length -1 == $("#srcList .select-check:checked").length){
				$("#srcList .select-item.selsrcAll .select-check").prop('checked',true);
			}
		}
		else{
			assignUser.removeTarget_(src, userId);
			// 取消全选
			$("#srcList .select-item.selsrcAll .select-check").prop('checked',false);
		}
	},
	getUserCode:function(){
		var userNames = '';
		userNames = $("#targetList .select-item").map(function(index) {
			return $(this).data().usercode;
		}).get();
		$('input[name="'+assignUser.eleName+'"]').val(userNames);
	},

	/**
	 * 将选中人员移动到右侧
	 */
	moveToTarget:function(src, userId){
		var srcId=$(src).find("label[userId]").attr("userId");
		if($("#targetList .select-item[userId='"+srcId+"']").length==0){
			var $target=$(src).clone();
			$("#targetList").append($target);
			$("#targetList").find(".select-check").remove();
			$target.find(".opr-wrp").html("<i userId= '"+userId+"' class='opr-icon icon-remove' onclick='assignUser.removeTarget(this);'></i>");
		}
		assignUser.getUserCode();
	},

	/**
	 * 点击右侧X按钮移除人员
	 * @param {} obj
	 * @param {} userId
	 */
	removeTarget:function(obj){
		 var userId = $(obj).attr("userId");
		 $(obj).parent().parent().remove();
		 $("#srcList .select-check[userId='"+userId+"']").prop('checked',false);
		 // 去掉全选
		 $("#srcList .select-item.selsrcAll .select-check").prop('checked',false);
		assignUser.getUserCode();
	},

	/**
	 * 取消勾选时移除人员
	 * @param {} src
	 * @param {} userId
	 */
	removeTarget_:function(src, userId){
		$("#targetList .select-item[userId='"+userId+"']").parent().remove();
		assignUser.getUserCode();
	}
};
