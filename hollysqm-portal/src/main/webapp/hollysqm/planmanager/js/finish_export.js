var finish = {
	content:"", //计划详情数据
	frameId:holly.getUrlParams("frameId")?holly.getUrlParams("frameId"):"",
	
	/**
	 * 初始化加载数据
	 */
	
	viewParam:{},
	init:function(){
		// 初始化预览dialog
		$('#dlg-view').dialog({
			href:holly.getPath()+"/hollysqm/planmanager/page/dlg-view.jsp",
			onOpen:function(){
				$('body').css('overflow','hidden');
			},
			onClose:function(){
				$('body').css('overflow','auto');
				// 关闭dialog停止播放语音
				if(listenTape.player.pause){
					listenTape.player.pause();
				}
			}
		});
		

		holly.post(holly.getPath()+"/rest/planRest/showPlanDetail",{},function(e){
			if(e.success){
				if(e.content.planId){
					$('#planEditTitle').html('编辑质检计划');
				}else{
					$('#planEditTitle').html('新建质检计划');
				}
				// 修改数据结构
				finish.content = e.content;
				var userMsg = e.content.userMsg;
				
				for(obj in userMsg) {
					if(obj == 0) {
						finish.viewParam.agent = userMsg[obj].agentCode;
					}
					
					finish.viewParam.agent += ',' + userMsg[obj].agentCode;
					
				}
				if(userMsg){
					finish.importDatagrid(userMsg, 1);
				}
				
				finish.viewParam.dataType=e.content.dataType;
				$('#extractMethod').html(finish.paramJson(e.content.extractMethod,'EXTRACT_PERIOD'));
				if(e.content) {
					holly.post(holly.getPath()+"/rest/planRest/getPlanExtractDetail",{planId:e.content.planId},function(e1){
						if(e1.success) {
							if(e1.content) {
								$('#extractDetail').html('抽检人数:' + e1.content.EXTRACTUSERNUM + ',' + '抽检组数:' + e1.content.EXTRACTDEPTNUM + ',' + '抽检数目:' + e1.content.EXTRACTNUM);
							}
						}
						
					});
				}
				
				

				var paramJson = $.parseJSON(e.content.paramJson);
				finish.paramJsonHtml(paramJson,e.content.textItemId);

				

				var jibenxinxiHtml = '';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">质检来源</div>';
				jibenxinxiHtml += '<div class="icontrols">'+dataTypeMap[e.content.dataType]+'</div>';
				jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">质检计划</div>';
				jibenxinxiHtml += '<div class="icontrols">'+e.content.planName+'</div>';
				jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">执行周期</div>';
				jibenxinxiHtml += '<div class="icontrols">'+(e.content.taskTimer?taskTimerMap[e.content.taskTimer]:"")+'</div>';
				jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">评分模板</div>';
				jibenxinxiHtml += '<div class="icontrols">';
				$.each(standardMap,function(index,item){
					if(item.value == e.content.standardId){
						jibenxinxiHtml += item.name;
					}
				});
				jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '</div>';
				
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">启用状态</div>';
				jibenxinxiHtml += '<div class="icontrols">'+stateMap[e.content.status]+'</div>';
				jibenxinxiHtml += '</div>';
				$('#jibenxinxi').html(jibenxinxiHtml);
			}else{
				holly.showError(e.errorMessage);
			}
		});
	},
	
	paramJson:function(param,getdictname){
		var text = '';
		if(param){
			var map = $("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':getdictname}});
			$.each(param.split(","), function(index, item){
				var itemText = map[item.replace(/\'/g,'')];
				if(itemText){
					text += (index==0?'':',')+itemText;
				}
			});
		}
		return text?text:"";
	},
	paramJsonHtml:function(paramJson,textItemId){
		if(finish.viewParam.dataType=='v8'){
			holly.post(holly.getPath()+'/rest/commonRest/getServiceTypeNames',{"serviceTypeId":paramJson.serviceType},function(e1){
				if(e1.success){
					if(e1.content){
						var serviceTypeArr = [];
						$.each(e1.content,function(index,item){
							serviceTypeArr.push(item.text);
						});
						finish.serviceTypeText = serviceTypeArr.join('<br/>');
					}
				}
			},true);
			var paramJsonHtml = '<div class="icontrol-group"><div class="icontrol-label maincolor">关键词</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.words+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">通话时长</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.recoinfoLength+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">静音时长</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.silenceLength+'秒</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">关键词适配对象</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.wordType,'V8_WORDTYPE')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">业务类型</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.businessType,'BUSINESS_TYPE')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">服务类型</div>';
			paramJsonHtml += '<div class="icontrols">'+(finish.serviceTypeText?finish.serviceTypeText:"")+'</div>';
			paramJsonHtml += '</div>';
			// paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">分配策略</div>';
			// paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.allotMethod,'V8ALLOTMETHOD')+'</div>';
			// paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">满意度</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.satisfication,'V8SATISFACTION')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">用户级别</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.custLevel,'CUST_LEVEL')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">号码归属地</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.custArea,'CUST_AREA')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">用户品牌</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.custBand,'CUST_BAND')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">日期规则</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.dateRule,'DATE_RULE')+'</div>';
			paramJsonHtml += '</div>';
			if(paramJson.dateRule == '2') {
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">时间范围</div>';
				paramJsonHtml += '<div class="icontrols">'+paramJson.dayNumber+'</div>';
				paramJsonHtml += '</div>';
			} else {
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">人工来电开始时间</div>';
				paramJsonHtml += '<div class="icontrols">'+finish.formatTime(paramJson.startTime)+'</div>';
				paramJsonHtml += '</div>';
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">人工来电结束时间</div>';
				paramJsonHtml += '<div class="icontrols">'+finish.formatTime(paramJson.endTime)+'</div>';
				paramJsonHtml += '</div>';
			}
			
			finish.viewParam.businessType = paramJson.businessType;
			finish.viewParam.serviceType = paramJson.serviceType;
			finish.viewParam.custArea = paramJson.custArea;
			finish.viewParam.custBand = paramJson.custBand;
			finish.viewParam.custLevel = paramJson.custLevel;
			finish.viewParam.endTime = paramJson.endTime;
			finish.viewParam.recoinfoLength = paramJson.recoinfoLength;
			finish.viewParam.satisfaction = paramJson.satisfication;
			finish.viewParam.startTime = paramJson.startTime;
			finish.viewParam.wordType = paramJson.wordType;
			finish.viewParam.words = paramJson.words;
			finish.viewParam.silenceLength = paramJson.silenceLength;
		}else if(finish.viewParam.dataType=='i8'){
			var paramJsonHtml = '<div class="icontrol-group"><div class="icontrol-label maincolor">关键词</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.words+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">会话时长</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.sessionLength+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">关键词适配对象</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.wordType,'I8_WORDTYPE')+'</div>';
			paramJsonHtml += '</div>';
			/*paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">服务类型</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.serviceType,'I8_SERVICE_TYPE')+'</div>';
			paramJsonHtml += '</div>';*/
			/*paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">分配策略</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.allotMethod,'I8ALLOTMETHOD')+'</div>';
			paramJsonHtml += '</div>';*/
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">满意度</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.satisfaction,'I8SATISFACTION')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">关闭方式</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.closeType,'CLOSE_TYPE')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">会话等级</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.sessionType,'SESSION_TYPE')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">日期规则</div>';
			paramJsonHtml += '<div class="icontrols">'+finish.paramJson(paramJson.dateRule,'DATE_RULE')+'</div>';
			paramJsonHtml += '</div>';
			if(paramJson.dateRule == '2') {
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">时间范围</div>';
				paramJsonHtml += '<div class="icontrols">'+paramJson.dayNumber+'</div>';
				paramJsonHtml += '</div>';
			} else {
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">会话开始时间</div>';
				paramJsonHtml += '<div class="icontrols">'+finish.formatTime(paramJson.startTime)+'</div>';
				paramJsonHtml += '</div>';
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">会话结束时间</div>';
				paramJsonHtml += '<div class="icontrols">'+finish.formatTime(paramJson.endTime)+'</div>';
				paramJsonHtml += '</div>';
			}
		
			finish.viewParam.closeType = paramJson.closeType;
			finish.viewParam.endTime = paramJson.endTime;
			finish.viewParam.satisfaction = paramJson.satisfaction;
			//finish.viewParam.serviceType = paramJson.serviceType;
			finish.viewParam.sessionLength = paramJson.sessionLength;
			finish.viewParam.sessionType = paramJson.sessionType;
			finish.viewParam.startTime = paramJson.startTime;
			finish.viewParam.wordType = paramJson.wordType;
			finish.viewParam.words = paramJson.words;
		}
		paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">标签</div>';
		paramJsonHtml += '<div class="icontrols" id="textItemName"></div>';
		paramJsonHtml += '</div>';
		$('#paramJsonDiv').html(paramJsonHtml);
		if(textItemId){
			finish.viewParam.textItemId = textItemId;
			holly.get(holly.getPath()+"/rest/commonRest/queryTextItem",{},function(e){
				if(e.success){
					var item_contentHtml = "";
					$.each(e.content,function(index,item){
						if(textItemId==item.textItemId){
							item_contentHtml += ' <div class="item_combo">';
							item_contentHtml += '<label class="label"><span class="text">'+item.itemName+'</span><a href="javascript:void(0)" class="item_icon" onclick="finish.displaysubMenu(this,event)"></a></label>';
							item_contentHtml += '<div class="ul_list" onclick="event.stopPropagation()">';
							item_contentHtml += '<div class="ul_inside">';  
							item_contentHtml += '<div class="item-group clearfix">';
							item_contentHtml += '<div class="item-label">'+item.itemName+'匹配规则</div>';
							item_contentHtml += '<div class="item-val">'+item.content+'</div>';
							item_contentHtml += '</div>';
							item_contentHtml += '</div>';
							item_contentHtml +=  '<div class="item-angle"></div>'
							item_contentHtml += '</div>';
							item_contentHtml += '</div>';
							//$('#textItemName').html(item.itemName);
							$('#textItemName').html(item_contentHtml);
						}
					});
				}
			});
		}
	},
	
	jumpBasicPage:function(){
		var url = holly.getPath()+'/hollysqm/planmanager/page/basic_info.jsp';
		url += '?frameId=' + finish.frameId;
		window.location = url;
	},
	
	displaysubMenu:function(obj,event) {
		$(".ul_list").hide();
		event.stopPropagation();
		var $this = $(obj);
		var ul_listDiv = $(obj).closest('.item_combo').children('.ul_list');
		// var x = $this.offset().left;
		// var y = $this.offset().top; 
		// var z = document.body.clientWidth;
		// var w = ul_listDiv.outerWidth();
		
		var left = $(obj).closest('.label').outerWidth()+110;
		ul_listDiv.toggle().css({'left':left});
		$(document).on('click',finish.docDisplaysubMenu);
	},
	
	docDisplaysubMenu:function(){
		$(".ul_list").hide();
		$(document).off('click',finish.docDisplaysubMenu);
	},
	formSubmit:function(){
		holly.post(holly.getPath()+'/rest/planRest/saveOrUpdatePlan',{},function(e){
			if(e.success){
				holly.showSuccess('保存成功');
				// 如果待复核列表页面没有关闭，并且该页面是从待复核列表打开的
				// if(parent.$("#"+finish.frameId).length>0){
				// 	var $obj = parent.$("#"+finish.frameId)[0].contentWindow;
				// 	$obj.planlist.search();//重新搜索列表
				// }
				// var $tabs = parent.$('#tabs');
				// var curTab = $tabs.tabs('getSelected');
				// var tabIndex = $tabs.tabs('getTabIndex',curTab);
				// $tabs.tabs('close',tabIndex);

                //回跳质检计划页面
                var url = holly.getPath() + "/hollysqm/planmanager/page/planlist.jsp?frameId=" + finish.frameId;
                window.location = url;
			}else{
				holly.showError(e.errorMessage);
			}
		});
	},
	importDatagrid:function(data, page){
		finish.importTableHtml(data, page);
		$('#importDatagridPage').remove();
		if(data.length > 10){
			$('#importDatagridJsDiv').append('<div id="importDatagridPage"></div>');
			$('#importDatagridPage').pagination({
				total: data.length,
				pageSize:10,
				pageNumber:1,
				onSelectPage:function(pageNumber, pageSize){
					finish.importTableHtml(data, pageNumber);
				}
			});
		}
	},
	
	formatTime:function(time) {
		var year = time.substr(0,4);
		var month = time.substr(4,2);
		var day = time.substr(6,2);
		var hour = time.substr(8,2);
		var minute = time.substr(10,2);
		var second = time.substr(12,2);
		return year + '-' + month + '-' + day + ' ' + hour + ":" + minute + ':' + second;
	},
	
	isContains:function(array,needle) {
		 for (var i in array) {
			    if (array[i] == needle) return true;
			  }
			  return false;
	},
	importTableHtml:function(data, page){
		var html = '';
		html += '<thead><tr><th>坐席工号</th><th>质检员工号</th><th>组别</th><th>坐席姓名</th><th>质检员姓名</th><th>分层类型</th><th>抽取条数</th></tr></thead>';
		html += '<tbody>';
		var arr = [];
		if(data.length > 10){
			arr = data.slice((page-1)*10, (page*10));
		}else{
			arr = data;
		}
		$.each(arr,function(index,item){
			html += '<tr>';
			html += '<td>'+item.agentCode+'</td>';
			html += '<td>'+item.checkerAgentCode+'</td>';
			html += '<td>'+item.agentDeptName+'</td>';
			html += '<td>'+item.agentName+'</td>';
			html += '<td>'+item.checkerName+'</td>';
			html += '<td>'+item.rank+'</td>';
			html += '<td>'+item.extractNumber+'</td>';
			html += '</tr>';
		});
		html += '</tbody>';
		$('#importDatagrid').html(html);
	}
}