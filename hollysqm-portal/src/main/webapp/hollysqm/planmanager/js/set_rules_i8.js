var set_rules = {
	ajaxCount: 6, //6条数据字典请求
	frameId:holly.getUrlParams("frameId")?holly.getUrlParams("frameId"):"",
	/**
	 * 初始化加载数据 
	 */
	init:function(){

		$("#formextractMethod").combobox("init",{'codeType':'EXTRACT_METHOD','event':{
			onLoadSuccess:function(){
				set_rules.ajaxFinished();
			}
		}});
		$("#formdateRule").combobox({
			valueField:'value',
			textField:'name',
			url:holly.getPath() + "/rest/dictionary/getDictionaryByCodeType?codeType=DATE_RULE",
			method:"get",
			onLoadSuccess:function(){
				var data = $(this).combobox('getData');
				$(this).combobox('setValue',data[0].value);
				set_rules.ajaxFinished();
			},
			onChange:function(n,o){
				if((n==1)||(n==3)){
					$('#dateRule1and3').show();
					$('#dateRule2').hide();
					$('#formstartTime,#formendTime').datebox('resize');
					$('#formdayNumber').numberspinner({
						required:false
					});
				}else if(n==2){
					$('#dateRule2').show();
					$('#dateRule1and3').hide();
					$('#formstartTime,#formendTime').datebox('resize');
					$('#formdayNumber').numberspinner({
						required:true
					});
				}
			},
			loadFilter:function(data){
				var jsonArray=new Array(); 
				for(var o in data.content){
					jsonArray.push(data.content[o]);
				}
				return jsonArray;
			}
		});
		$("#formwordType").combobox("init_extend",{'codeType':'I8_WORDTYPE','hasAllOption':false,'event':{
			onLoadSuccess:function(){
				set_rules.ajaxFinished();
			}
		}});
		$("#formsessionType").combobox("init_extend",{'codeType':'SESSION_TYPE','hasAllOption':false,'event':{
			onLoadSuccess:function(){
				set_rules.ajaxFinished();
			}
		}});
		$("#formSatisfication").combobox("init_extend",{'codeType':'I8SATISFACTION','hasAllOption':false,'event':{
			onLoadSuccess:function(){
				set_rules.ajaxFinished();
			}
		}});
		$("#formcloseType").combobox("init_extend",{'codeType':'CLOSE_TYPE','hasAllOption':false,'event':{
			onLoadSuccess:function(){
				set_rules.ajaxFinished();
			}
		}});
		//$("#formbusinessType").combobox("init_extend",{'codeType':'I8_SERVICE_TYPE','hasAllOption':false,'event':{
		// 	onLoadSuccess:function(){
		// 		set_rules.ajaxFinished();
		// 	}
		// }});

	},
	ajaxFinished:function(){
		set_rules.ajaxCount --;
		if(set_rules.ajaxCount <= 0){// 所有数据字典请求全部完成，做该做的事情
			holly.post(holly.getPath()+"/rest/planRest/setPlanExtractRule",{},function(e){
				if(e.success){
					if(e.content.planId){
						$('#planEditTitle').html('编辑质检计划');
					}else{
						$('#planEditTitle').html('新建质检计划');
					}
					if(e.content.extractMethod != null){
						$('#formextractMethod').combobox('setValue',e.content.extractMethod);
						assignUser.moveToUser(e.content.agents, "agent");
						var paramJson = $.parseJSON(e.content.paramJson);
						if(paramJson.closeType != undefined){//I8独有字段，避免从V8切换到I8的时候没有该字段出现报错
							var satisfaction = paramJson.satisfaction.replace(/\'/g,'');
							if(satisfaction){
								$("#formSatisfication").combobox('setValues',satisfaction);
							}
							/*var serviceType = paramJson.serviceType.replace(/\'/g,'');
							if(serviceType){
								$("#formbusinessType").combobox('setValues',serviceType);
							}*/
							$("#formdateRule").combobox('setValue',paramJson.dateRule);
							$("#formdayNumber").numberspinner('setValue',paramJson.dayNumber);
							var startTime = paramJson.startTime;
							if(startTime){
								startTime = startTime.substring(0,4)+'-'+startTime.substring(4,6)+'-'+startTime.substring(6,8);
								$("#formstartTime").textbox('setValue',startTime);
							}
							var endTime = paramJson.endTime;
							if(endTime){
								endTime = endTime.substring(0,4)+'-'+endTime.substring(4,6)+'-'+endTime.substring(6,8);
								$("#formendTime").textbox('setValue',endTime);
							}
							$("#formwords").textbox('setValue',paramJson.words);
							$("#formwordType").combobox('setValue',paramJson.wordType);
							var sessionLength = paramJson.sessionLength.split('-');
							$("#sessionLengthstart").numberspinner('setValue',sessionLength[0]);
							$("#sessionLengthend").numberspinner('setValue',sessionLength[1]);
							var closeType = paramJson.closeType.replace(/\'/g,'');
							if(closeType){
								$("#formcloseType").combobox('setValues',closeType);
							}
							var sessionType = paramJson.sessionType.replace(/\'/g,'');
							if(sessionType){
								$("#formsessionType").combobox('setValues',sessionType);
							}
						}
						set_rules.textItemIdHtml(e.content.textItemId);
					}else{
						assignUser.moveToUser('', "agent");
						set_rules.textItemIdHtml('');
					}
				}else{ 
					holly.showError(e.errorMessage);
				}
			});
		}
	},
	textItemIdHtml:function(id){
		holly.get(holly.getPath()+"/rest/commonRest/queryTextItem",{},function(e){
			if(e.success){
				var item_contentHtml  = '';
				$.each(e.content,function(index,item){
					item_contentHtml += ' <div class="item_combo">';
					item_contentHtml += '<label class="label" title="'+item.itemName+'"><input type="checkbox" value="'+item.textItemId+'" name="textItemIds" '+(id==item.textItemId?"checked":"")+' onclick="set_rules.itemcheck(this)" /><span class="text">'+item.itemName+'</span><a href="javascript:void(0)" class="item_icon" onclick="set_rules.displaysubMenu(this,event)"></a></label>';
					item_contentHtml += '<div class="ul_list" onclick="event.stopPropagation()">';
					item_contentHtml += '<div class="ul_inside">';  
					item_contentHtml += '<div class="item-group clearfix">';
					item_contentHtml += '<div class="item-label">'+item.itemName+'匹配规则</div>';
					item_contentHtml += '<div class="item-val">'+item.content+'</div>';
					item_contentHtml += '</div>';
					item_contentHtml += '</div>';
					item_contentHtml +=  '<div class="item-angle"></div>';
					item_contentHtml += '</div>';
					item_contentHtml += '</div>';
					if(id==item.textItemId){
						$('#formwords').textbox('disable');
						$('#formwordType').textbox('disable');
					}
				});
				$('#item_content').html(item_contentHtml);
			}
			else{
				holly.showError(e.errorMessage);
			}
		});
	},
	displaysubMenu:function(obj,event) {
		$(".ul_list").hide();
		event.stopPropagation();
		var $this = $(obj);
		var ul_listDiv = $(obj).closest('.item_combo').children('.ul_list');
		var x = $this.offset().left;
		var y = $this.offset().top; 
		//var z = document.body.clientWidth;
		//var w = ul_listDiv.outerWidth();
		ul_listDiv.toggle().css({'top':y-160,'left':x-10});
		$(document).on('click',set_rules.docDisplaysubMenu);
	},
	docDisplaysubMenu:function(){
		$(".ul_list").hide();
		$(document).off('click',set_rules.docDisplaysubMenu);
	},
	itemcheck:function(el){
		$this = $(el);
		if($this.prop('checked')){
			$this.closest('.item_combo').siblings().find('input[type="checkbox"]').prop('checked',false);
			$('#formwords').textbox('disable');
			$('#formwordType').textbox('disable');
		}else{
			$('#formwords').textbox('enable');
			$('#formwordType').textbox('enable');
		}
	},
	returnStrArr:function(arr){
		var str = "";
		$.each(arr,function(index,item){
			str+=index==0?'':',';
			str+="'"+item+"'";
		});
		return str;
	},
	formSubmit:function(page){
		// var paramJson='{';
		// paramJson+='\\"words\\":\\"'+$('#formwords').textbox('getValue')+'\\",';
		// paramJson+='\\"sessionLength\\":\\"'+$("#sessionLengthstart").numberspinner('getValue')+$("#sessionLengthend").numberspinner('getValue')+'\\",';
		// paramJson+='\\"wordType\\":\\"'+$('#formwordType').textbox('getValue')+'\\",';
		// paramJson+='\\"serviceType\\":\\"'+set_rules.returnStrArr($('#formbusinessType').combobox('getValues'))+'\\",';
		// paramJson+='\\"allotMethod\\":\\"\\",';
		// paramJson+='\\"satisfication\\":\\"'+set_rules.returnStrArr($('#formSatisfication').combobox('getValues'))+'\\",';
		// paramJson+='\\"endTime\\":\\"'+$('#formendTime').combobox('getValue').split('-').join("")+'\\",';
		// paramJson+='\\"startTime\\":\\"'+$('#formstartTime').combobox('getValue').split('-').join("")+'\\"';
		// paramJson+='\\"closeType\\":\\"'+set_rules.returnStrArr($('#formcloseType').combobox('getValues'))+'\\",';
		// paramJson+='\\"sessionType\\":\\"'+set_rules.returnStrArr($('#formsessionType').combobox('getValues'))+'\\",';
		// paramJson+='}';
		var paramJson='{';
		paramJson+='"words":"'+$('#formwords').textbox('getValue')+'",';
		paramJson+='"dateRule":"'+$('#formdateRule').combobox('getValue')+'",';
		var dayNumberValue = $('#formdayNumber').numberspinner('getValue');
		paramJson+='"dayNumber":"'+(dayNumberValue?dayNumberValue:"0")+'",';
		if($("#sessionLengthstart").numberspinner('getValue')&&$("#sessionLengthend").numberspinner('getValue')){
			paramJson+='"sessionLength":"'+$("#sessionLengthstart").numberspinner('getValue')+'-'+$("#sessionLengthend").numberspinner('getValue')+'",';
		}
		paramJson+='"wordType":"'+$('#formwordType').textbox('getValue')+'",';
		//paramJson+='"serviceType":"'+set_rules.returnStrArr($('#formbusinessType').combobox('getValues'))+'",';
		paramJson+='"allotMethod":"0",';
		paramJson+='"satisfaction":"'+set_rules.returnStrArr($('#formSatisfication').combobox('getValues'))+'",';
		paramJson+='"closeType":"'+set_rules.returnStrArr($('#formcloseType').combobox('getValues'))+'",';
		paramJson+='"sessionType":"'+set_rules.returnStrArr($('#formsessionType').combobox('getValues'))+'",';
		paramJson+='"endTime":"'+$('#formendTime').datebox('getValue').split('-').join("")+'",';
		paramJson+='"startTime":"'+$('#formstartTime').datebox('getValue').split('-').join("")+'"';
		paramJson+='}';
		var json = eval('(' + paramJson + ')'); 
		var textItemIds = [];
		$('input[name="textItemIds"]:checked').each(function(){
			textItemIds.push($(this).val());
		});
		var param = {
			agent: $('input[name="agent"]').val(),
			extractMethod: $('#formextractMethod').combobox('getValue'),
			paramJson: paramJson,
			textItemId: textItemIds.join(',')
		};
		if(page=="next"){
			var startTime = $('#formstartTime').datebox('getValue');
			var endTime = $('#formendTime').datebox('getValue');
			var startTimeLon = new Date(startTime).getTime();
			var endTimeLon = new Date(endTime).getTime();
			var timeLon = (endTimeLon - startTimeLon) / (1000*60*60*24);
			if($('#targetList > li').length==0){
				holly.showError('请选择抽取坐席');
			}
			if(json.dateRule == "1" || json.dateRule == "3"){//固定天数或是固定周期
				if(startTime==""){
					holly.showError('请选择开始时间');
					return false;
				}
				if(endTime==""){
					holly.showError('请选择结束时间');
					return false;
				}
				if(startTimeLon > endTimeLon){
					holly.showError('开始时间不能大于结束时间');
					return false;
				}if(timeLon >= 90){
					holly.showError('开始时间和结束时间不能超过90天');
					return false;
				}
			}else if(json.dateRule == "2") {//固定天数
				if(!$.trim(dayNumberValue)){
					holly.showError('时间范围不能为空');
					return false;
				}
				
				if(parseInt(dayNumberValue) > 90) {
					holly.showError('时间范围不能大于90天');
					return false;
				}
			}else{
				holly.showError('时间规则不能为空');
				return false;
			}
			var sessionstart = $('#sessionLengthstart').numberspinner('getValue');
			var sessionend = $('#sessionLengthend').numberspinner('getValue');
			if(sessionstart > sessionend){
				holly.showError('会话时长开始时间不能大于结束时间');
				return false;
			}
			if($('#set_rulesform').form('enableValidation').form("validate")){
				holly.post(holly.getPath()+'/rest/planRest/savePlanExtractRule',param,function(e){
					if(e.success){
						var url = holly.getPath()+'/hollysqm/planmanager/page/checker.jsp';
						url += '?dataType=i8';
						url += '&frameId=' + set_rules.frameId;
						window.location = url;
					}else{
						holly.showError(e.errorMessage);
					}
				});
			}
		}else if(page=="last"){
			holly.post(holly.getPath()+'/rest/planRest/savePlanExtractRule',param,function(e){
				if(e.success){
					var url = holly.getPath()+'/hollysqm/planmanager/page/basic_info.jsp';
					url += '?frameId=' + set_rules.frameId;
					window.location = url;
				}else{
					holly.showError(e.errorMessage);
				}
			});
		}
	}
};