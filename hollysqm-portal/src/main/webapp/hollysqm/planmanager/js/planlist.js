var planlist = {
	/**
	 * 初始化加载数据
	 */
	init:function(){

		$("#formDataType").combobox("init_extend",{'codeType':'DATATYPE',"hasAllOption":false,"event":{
			onLoadSuccess:function(){
				planlist.dataTypeData = $(this).combobox('getData');
				$(this).combobox('setValue',planlist.dataTypeData[0].value);
			}
		}});
		$("#formState").combobox("init_extend",{'codeType':'STATUS'});
		$("#formPlanName").combobox("init_extend",{'codeType':'PLAN_NAME'});
		$("#searchplanlist").keyup(function(event){
			if(event.keyCode==13){
				planlist.search();
			}
		});
		$("#planlistdatagrid").datagrid({
				fit:true,
				url:holly.getPath()+'/rest/planRest/getPlanList',
				method:'get',
				singleSelect : true,
				border:false,
				fitColumns:true,
				queryParams:{'dataType':'v8'},
				pagination: true,
				loadFilter: function(data){return (data.content)?data.content:data;},
				columns:[[
					{field:'dataType',title:'质检来源',width:60,formatter:planlist.formatDataTyp},
					{field:'planName',title:'质检计划',width:100,formatter:planlist.formatLink},
					{field:'status',title:'启用状态',width:50,formatter:planlist.formatState},
					{field:'modifier',title:'最后修改人',width:60,formatter:planlist.formatModifier},
					{field:'modifyTime',title:'最后修改时间',width:80},
					{field:'option',title:'操作',width:60,formatter:planlist.formatOption}
				]],
				loadMsg:'数据加载中请稍后……',
				onLoadSuccess: function (data)  {
					if (data.rows.length == 0) {
						var body = $(this).data().datagrid.dc.body2;
						body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
					}
				}
			});	
		
		//初始化质检计划列表
		$('#planId').combobox({
			url : holly.getPath() + '/rest/commonRest/queryPlan',
			valueField : 'value', 
			textField : 'name',
			loadFilter: function(data){
				var jsonArray=new Array();
				var defaultVal = {"value":"","name":'请选择'};
				jsonArray.push(defaultVal);
				for(var o in data.content){
					jsonArray.push(data.content[o]);
				}
				return jsonArray;
			}
		}); 
		
	},

	
	/**
	 * 搜索
	 */
	search:function(){
		var param = holly.form2json($("#searchplanlist"));
		$("#planlistdatagrid").datagrid("search",{'param':param});
	},
	resetForm : function(){
		$('#searchplanlist').form('reset');
		$('#formDataType').combobox('select',planlist.dataTypeData[0].value);//默认选中第一个
	},
	
	// 质检来源
	formatDataTyp:function(val, row, index) {
		var res =dataTypeMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	},
	// 启用状态
	formatState:function(val, row, index) {
		var res =stateMap[val]; //获取对应的数据
		return res;
	},
	// 操作
	formatOption:function(val,row,index){
		var content = '';
			content += "<span class='grid-opt-wrap'>";
			content += "<div class='switch "+(row.status=="1"?'switch-on':'')+"' onclick=\"planlist.switchBtn(this,'"+row.status+"','"+row.planId+"','"+index+"')\"></div>";
			content	+= "<a href='javascript:void(0)' onclick=\"planlist.editPlan('"+row.planId+"')\" class='grid-optcon icon-edit' title='编辑'></a>";
			content += "<a href='javascript:void(0)' onclick=\"planlist.planrecord('"+row.planId+"')\" class='grid-optcon icon-pushpin' title='执行记录'></a>";
			content += "<a href='javascript:void(0)' onclick=\"planlist.plancensus('"+row.planId+"')\" class='grid-optcon icon-bar-chart' title='抽取统计'></a>";
			if(row.status=="1"){
				content += "<a href='javascript:void(0)' onclick=\"planlist.planactivation('"+row.planId+"')\" class='grid-optcon icon-magic' title='执行计划'></a>";
			}
			content += "</span>";
		return content;
	},
	// 质检计划
	formatLink:function(val,row,index){
		return val?"<a href='javascript:;' title='"+val+"' onclick=\"planlist.planinfo('"+row.planId+"','"+row.dataType+"')\">"+val+"</a>":"";
	},
	// 最后修改人
	formatModifier:function(val,row,index){
		return val?"<span title='"+row.modifierName + "(" +val+ ")"+"'>"+row.modifierName + "(" + val+ ")"+"</span>":"";
	},

	/**
	 * 修改计划
	 * @param planId 计划id
	**/
	editPlan:function(planId){
		var url = holly.getPath() + "/hollysqm/planmanager/page/basic_info.jsp";
		if(planId){
			url += "?planId=" + planId;
		}
		var frameId = window.frameElement && window.frameElement.id || '';
		url += '?frameId=' + frameId;
		// parent.main.addTab('editPlan',url,'修改计划');
		window.location = url;
	},
	addPlan:function(){
		holly.post(holly.getPath() + "/rest/planRest/clearChachePlan",{},function(e){
			if(e.success){
				var url = holly.getPath() + "/hollysqm/planmanager/page/basic_info.jsp";
				var frameId = window.frameElement && window.frameElement.id || '';
				url += '?frameId=' + frameId;
				// parent.main.addTab('addPlan',url,'新建计划');
                window.location = url;
            }
		});
	},

	/**
	 * 执行记录
	 * @param planId 计划id
	**/
	planrecord:function(planId){
		var url = holly.getPath() + "/hollysqm/planmanager/page/planrecord.jsp";
		if(planId){
			url += "?planId=" + planId;
		}
		// parent.main.addTab('planrecord',url,'执行记录');
        window.location = url;
    },


	/**
	 * switch切换状态方法
	 * @param obj this切换按钮
	 * @param status 状态 0 或 1
	 * @param planId 计划id
	 * @param index 行数据索引
	**/
	switchBtn:function(obj,status,planId,index){
		var enable = status=="1"?stateMap["0"]:stateMap["1"];
		var json={
			title:enable,
			content:"确定修改该计划状态为"+enable+"吗？",
			message:"该计划已成功"+enable
		};
		$.messager.confirm(json.title,json.content,function(r){
			if(r){
				holly.post(holly.getPath()+"/rest/planRest/switchPlan",{"planId":planId,"status":status=="1"?"0":"1"},function(e){
					if(e.success){
						if(status=="1"){
							$("#planlistdatagrid").datagrid("getData").rows[index].status="0";
						}else{
							$("#planlistdatagrid").datagrid("getData").rows[index].status="1";
						}
						$("#planlistdatagrid").datagrid('refreshRow',index);
						holly.showSuccess(json.message);
					}else{
						holly.showError(e.errorMessage);
					}
				});
			}
		});
	},
	planinfo:function(planId,dataType){
		var url = holly.getPath()+"/hollysqm/planmanager/page/dlg-planinfo.jsp";
		url += "?planId=" + planId;
		url += "&dataType=" + dataType;
		$('#dlg-planinfo').dialog('open').dialog('refresh',url);
	},
	
	/**
	 * 抽取统计
	 * @param planId 计划id
	**/
	plancensus:function(planId){
		var url = holly.getPath()+"/hollysqm/planmanager/page/dlg-plancensus.jsp";
		url += "?planId=" + planId;
		$('#dlg-plancensus').dialog('open').dialog('refresh',url);
	},
	
	planactivation:function(planId){
		$.messager.confirm('执行计划', '是否确定执行计划?', function(r){
			if (r){
				holly.showSuccess("执行成功");
				holly.post(holly.getPath()+"/rest/planRest/executePlan",{"planId":planId},function(e){
					if(e.success){
					}else{
						holly.showError("错误，原因:"+e.errorMessage);
					}
				});
			}
		});
	},
	
	//评分跳转页面
	copyPlan:function jumptoscore(contactId,dataType,agentCode){
		//初始化质检计划列表
		$('#planId').combobox({
			url : holly.getPath() + '/rest/commonRest/queryPlan',
			valueField : 'value', 
			textField : 'name',
			loadFilter: function(data){
				var jsonArray=new Array();
				var defaultVal = {"value":"","name":'请选择'};
				jsonArray.push(defaultVal);
				for(var o in data.content){
					jsonArray.push(data.content[o]);
				}
				return jsonArray;
			}
		}); 
		$('#dlg-templet').dialog('open');
		
	},
	
	submit:function() {
		var vaild = $("#formtemplet").form('enableValidation').form('validate');
		if(vaild){
			$.messager.progress({
				text:'正在提交...'
			});
			$('#formtemplet').form("submit",{
				url:holly.getPath()+"/rest/planRest/copyPlan",
				success : function(e) {
					var d=($.type(e)=="string")?$.parseJSON(e):e;
					if(d.success){
						holly.showSuccess('复制计划成功');
						
					}else{
						holly.showError('复制计划失败');
					}
					$.messager.progress('close');
					$('#dlg-templet').dialog('close');
					planlist.search();
				}
			});
		}	
	},
	
	reset:function() {
		$('#formtemplet').form('reset');
		$('#planId').combobox('select',"");//默认选中第一个
	}
}