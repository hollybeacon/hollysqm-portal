var planRecord = {
	/**
	 * 初始化加载数据
	 */
	init:function(){		
		$("#PlanRecorddatagrid").datagrid({
			fit:true,
			url:holly.getPath()+'/rest/planRest/getPlanLog',
			method:'get',
			singleSelect : true,
			border:false,
			fitColumns:true,
			pagination: true,
			queryParams:holly.form2json($("#searchPlanRecord")),
			loadFilter: function(data){return (data.content)?data.content:data;},
			columns:[[
				{field:'date',title:'日期',width:80,formatter:planRecord.formatTime},
				{field:'startTime',title:'执行开始时间',width:80},
				{field:'endTime',title:'执行结束时间',width:80},
				{field:'paperCount',title:'生成质检单',width:60}
			]],
			loadMsg:'数据加载中请稍后……',
			onLoadSuccess: function (data)  {
				if (data.rows.length == 0) {
					var body = $(this).data().datagrid.dc.body2;
					body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
				}
			}
		});
	},	
	/**
	 * 搜索
	 */
	search:function(){
		var param = holly.form2json($("#searchPlanRecord"));
		$("#PlanRecorddatagrid").datagrid("search",{'param':param});
	},
	/**
	 * 日期：由开始时间截取而来
	 */
	formatTime:function(val,row,index){
		var time = row.startTime;
		return time.substring(0,10);
	}
}