var dlgView = {
	/**
	 * 初始化加载数据  
	 */
	loading:'<div class="panel-loading dlgView-loading">Loading...</div>',
	init:function(){

		holly.post(holly.getPath()+"/rest/planRest/viewPlanPaper",finish.viewParam,function(e){
			if(e.success){
				if(e.content.length>0){
					dlgView.contactIdArr = e.content;
					var html = '';
					html += '系统提供'+dlgView.contactIdArr.length+'条数据供您预览，';
					html += '<a href="javascript:;" onclick="dlgView.prePage('+dlgView.contactIdArr.length+')">上一个</a>';
					html += '（<span id="viewpage" data-page="1">1</span>/'+dlgView.contactIdArr.length+'）';
					html += '<a href="javascript:;" onclick="dlgView.nextPage('+dlgView.contactIdArr.length+')">下一个</a>';
					$('#viewpagetion').html(html);
					dlgView.getPlanRecord(dlgView.contactIdArr[0]);
				}else{
					$('#dlg-view').html('没有符合要求的质检单');
				}
			}
		});
	},
	prePage:function(itemNum){
		var page = parseInt($("#viewpage").text());
		if(page<=1){
			holly.showError('已经是第一页了');
			return false;
		}else{
			$('#dlg-view').append(dlgView.loading);
			dlgView.getPlanRecord(dlgView.contactIdArr[page-2]);
			dlgView.curPage = page-1;
		}
	},
	nextPage:function(itemNum){
		var page = parseInt($("#viewpage").text());
		if(page>=itemNum){
			holly.showError('已经是最后一页了');
			return false;
		}else{
			$('#dlg-view').append(dlgView.loading);
			dlgView.getPlanRecord(dlgView.contactIdArr[page]);
			dlgView.curPage = page+1;
		}
	},
	getPlanRecord:function(contactId){
		//init_weigui(contactId);
		listenTape.resetListen();
		holly.post(holly.getPath()+"/rest/commonRest/getHighlightRecord",{"contactId":contactId,"dataType":finish.viewParam.dataType, "textItemId":finish.viewParam.textItemId, "words":finish.viewParam.words, "wordType":finish.viewParam.wordType},function(e){
			if(e.success){
				$('#dlg-view .dlgView-loading').remove();
				var content = e.content;
				var html = '';
				var chatHtml = '';
				var arrFlag = [];
				var arrText = [];
				$("#viewpage").html(dlgView.curPage);
				dlgView.contactId = content.contactId;
				dlgView.acceptTime = content.acceptTime;
				dlgView.recordFile = content.recordFile;
				
				arrFlag= content.txtContent.match(/n\d#/ig);
				arrText= content.txtContent.split(/n\d#/ig);
				for(var i=0; i<arrFlag.length; i++){
					if (arrFlag[i]=='n0#') {
						chatHtml += '<div class="text worker"><span style="text-align:right;">'+(i+1)+'&nbsp;&nbsp;</span>坐席：'+(arrText[i+1])+'</div>';
					}else if(arrFlag[i]=='n1#'){
						chatHtml += '<div class="text visiter"><span style="text-align:right;">'+(i+1)+'&nbsp;&nbsp;</span>客户：'+(arrText[i+1])+'</div>';
					}
				}
				$('#chatqa').html(chatHtml);
				
				if(e.content.dataType=='v8'){
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">来电号码</div>';
					if(content.caller != null) {
						html += '<div class="icontrols">'+content.caller+'</div>';
					} 
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">坐席</div>';
					if(content.user != null){
						html += '<div class="icontrols">'+content.user.username+'('+content.user.agentCode+')'+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">归属地</div>';
					if(content.custArea != null) {
						html += '<div class="icontrols">'+dlgView.paramJson(content.custArea,'CUST_AREA')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">品牌</div>';
					if(content.custBand != null) {
						html += '<div class="icontrols">'+dlgView.paramJson(content.custBand,'CUST_BAND')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">满意度</div>';
					if(content.satisfaction != null) {
						html += '<div class="icontrols">'+dlgView.paramJson(content.satisfaction,'V8SATISFACTION')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">业务类型</div>';
					if(content.serviceType != null) {
						html += '<div class="icontrols">'+dlgView.paramJson(content.bussinessType,'BUSINESS_TYPE')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">开始时间</div>';
					if(content.startTime != null) {
						html += '<div class="icontrols">'+dlgView.formatTime(content.startTime)+'</div>';
					}
					html += '</div>';
				
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">通话时长</div>';
					if(content.length != null) {
						html += '<div class="icontrols">'+content.length+'秒</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">用户级别</div>';
					if(content.custLevel != null) {
						html += '<div class="icontrols">'+dlgView.paramJson(content.custLevel,'CUST_LEVEL')+'</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">静音时长</div>';
					if(content.custLevel != null) {
						html += '<div class="icontrols">'+ content.silenceLength +'秒</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">服务请求</div>';
					if(content.serviceType != null) {
						//html += '<div class="icontrols">'+content.serviceType+'</div>';
						holly.post(holly.getPath()+"/rest/commonRest/getServiceTypeNames",{"serviceTypeId":content.serviceType},function(e1){
							if(e1.content) {
								var array = e1.content;
								if(array.length > 0) {
									html += '<div class="icontrols">'+array[0].text+'</div>';
								}
							}
							html += '</div>';
							$('#getRecordDiv').html(html);
						});
					}else {
						html += '</div>';
						$('#getRecordDiv').html(html);
					}

					$('#voicechatDiv').show();
					// 接触记录图片音频地址查询
					holly.post(holly.getPath()+"/rest/commonRest/getRecordDir",
							{"contactId":dlgView.contactId, "acceptTime":dlgView.acceptTime, "recordFile":dlgView.recordFile},function(e){
						if(e.success){
							listenTape.init(e.content.wavDir,e.content.imgDir);
						}else{
							toastr.error(e.errorMessage);
						}
					});
				}else if(e.content.dataType=='i8'){
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">来话号码</div>';
					if(content.caller != null) {
						html += '<div class="icontrols">'+content.caller+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">坐席</div>';
					if(content.user != null){
						html += '<div class="icontrols">'+content.user.username+'('+content.user.agentCode+')'+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">关闭方式</div>';
					if(content.closeType != null) {
						html += '<div class="icontrols">'+dlgView.paramJson(content.closeType,'CLOSE_TYPE')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">会话等级</div>';
					if(content.sessionType != null) {
						html += '<div class="icontrols">'+dlgView.paramJson(content.sessionType,'SESSION_TYPE')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">满意度</div>';
					if(content.satisfaction != null) {
						html += '<div class="icontrols">'+dlgView.paramJson(content.satisfaction,'I8SATISFACTION')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">服务类型</div>';
					if(content.serviceType != null) {
						html += '<div class="icontrols">'+content.serviceType+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">开始时间</div>';
					if(content.startTime != null) {
						html += '<div class="icontrols">'+content.startTime+'</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">会话时长</div>';
					if(content.length != null) {
						html += '<div class="icontrols">'+content.length+'秒</div>';
					}
					html += '</div>';					
				}
				//$('#getRecordDiv').html(html);

				
			}else{
				toastr.error(e.errorMessage);
			}
		});

	},
	
	formatTime:function(time) {
		var year = time.substr(0,4);
		var month = time.substr(4,2);
		var day = time.substr(6,2);
		var hour = time.substr(8,2);
		var minute = time.substr(10,2);
		var second = time.substr(12,2);
		return year + '-' + month + '-' + day + ' ' + hour + ":" + minute + ':' + second;
	},
	paramJson:function(param,getdictname){
		if(param){
			var map = $("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':getdictname}});
			var text = '';
			$.each(param.split(","), function(index, item){
				text += (index==0?'':',')+(map[item]?map[item]:'');
			});
			return text;
		}
	}

};


var listenTape = {
	player: new Object(),
	intever : null,
	init:function(voicePath,imgPath){
		listenTape.player = new _mu.Player({
			baseDir: holly.getPath() + '/hollysqm/common/muplayer/dist'
		});//初始播放器
		//setSong(contextPath+path);//设置播放录音
		//音量调节
		$("#voice").slider();
		$(document).on('sliderchange', "#voice", function (e, result) {
			listenTape.player.setVolume(result.value);
		});

		$('.myautoplayer-play').on('click',function(){
			if($(this).hasClass('icon-play')){
				listenTape.player.play();//播放事件
				$(this).removeClass("icon-play").addClass("icon-pause");
				listenTape.intever = scrollImg();
			}else if($(this).hasClass('icon-pause')){
				listenTape.player.pause();//暂停
				$(this).removeClass("icon-pause").addClass("icon-play");
				clearInterval(listenTape.intever);//清除事件
			}
			$('#spanDuration').html(countPlayDate(listenTape.player.duration()));
		});
		//图片点击播放事件
		$("#divScroll").click(function (e) {
			var scrollLeft = $("#divScroll").scrollLeft();
			var offset = $(this).offset();
			var relativeX = (e.pageX - offset.left) + scrollLeft;
			var len = parseInt(relativeX / $("#imgVoice").width() * listenTape.player.duration() * 1000);
			listenTape.player.play(len);
			listenTape.intever = scrollImg();
			$(".icon-play").addClass("icon-pause").removeClass("icon-play");
			$('#spanDuration').html(countPlayDate(listenTape.player.duration()));
		});
		//滚动图片
		function scrollImg() {
			return setInterval(function () {//改变滑动块
				var l = listenTape.player.curPos() / listenTape.player.duration();
				var left = $("#imgVoice").width();//图片宽度
				if(l >(1-900/left)){
					clearInterval(listenTape.intever);//清除事件
					$(".icon-pause").addClass("icon-play").removeClass("icon-pause");
					return ;
				}
				$("#divScroll").animate({scrollLeft: left * l}, 1000);
				$('#spanCurPos').html(countPlayDate(listenTape.player.curPos()));
			}, 1000);
		}

		//滚动条拖动事件
		$("#divScroll").mousedown(function () {
			var state = listenTape.player.getState();
			if (state == 'playing') {
				listenTape.player.pause();//暂停
				$(".icon-pause").addClass("icon-play").removeClass("icon-pause");
				clearInterval(listenTape.intever);//清除事件
			}
			$('#spanCurPos').html(countPlayDate(listenTape.player.curPos()));
		});
		listenTape.player.setUrl(holly.getPath() + "/" + voicePath );//添加录音文件
		$("#imgVoice").attr("src",holly.getPath() + "/" + imgPath).removeClass('voiceloading').addClass('voicechatimg');
		//输出标准时间格式
		function countPlayDate(pos){
			pos = Math.ceil(pos);
			if (pos>60){
				var m = Math.ceil(pos/60);
				var s = Math.ceil(pos%60);
				return (m>9?m:"0"+m)+":"+ (s>9?s:"0"+s);
			}else {
				return "00:"+(pos>9?pos:"0"+pos);
			}
		}
	},
	resetListen:function(){
		if(listenTape.player.pause){
			listenTape.player.pause();
		}
		clearInterval(listenTape.intever);//清除事件
		$(".myautoplayer-play").off('click');
		$(".myautoplayer-play").removeClass("icon-pause").addClass("icon-play");
		$("#divScroll").off('click');
		$("#divScroll").off('mousedown');
		$("#imgVoice").attr("src",holly.getPath() + "/hollysqm/common/images/loading-mask.gif").removeClass('voicechatimg').addClass('voiceloading');
	}
};