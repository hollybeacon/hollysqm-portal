var standardMap = [];
holly.post(holly.getPath() + '/rest/commonRest/queryStandardList',{},function(e){
	if(e.success){
		standardMap = e.content;
	}else{
		holly.showError(e.errorMessage);
	}
});
var taskTimerMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'TASK_TIMER'}});
var dlgPlaninfo = {
	content:"", //计划详情数据
	
	/**
	 * 初始化加载数据
	 */
	init:function(){

		holly.post(holly.getPath()+"/rest/planRest/showPlanDetail",{"planId":planId},function(e){
			if(e.success){
				// 修改数据结构
				dlgPlaninfo.content = e.content;
				if(e.content.isExport == '1'){
					$('#importDatagridDiv').show();
					$('#extractMethod').html(dlgPlaninfo.paramJson(e.content.extractMethod,'EXTRACT_PERIOD'));
					if(e.content.userMsg){
						dlgPlaninfo.importDatagrid(e.content.userMsg, 1);
					}
					
					holly.post(holly.getPath()+"/rest/planRest/getPlanExtractDetail",{planId:e.content.planId},function(e1){
						if(e1.success) {
							if(e1.content) {
								$('#extractDetail').html('抽检人数:' + e1.content.EXTRACTUSERNUM + ',' + '抽检组数:' + e1.content.EXTRACTDEPTNUM + ',' + '抽检数目:' + e1.content.EXTRACTNUM);
							}
						}
						
					});
				}else if(e.content.isExport == '0'){
					$('#cxgroplistpaneDiv').show();
					$('#zjygroplistpaneDiv').show();
					$('#cxsearch').searchbox({
						searcher:function(value,name){
							var value = $.trim(value);
							dlgPlaninfo.workerSearch(value,dlgPlaninfo.content.agents,'#cxgroplistpane');
						},
						height:30,
						width:228,
						prompt:'按姓名或账号搜索'
					}).textbox('addClearBtn', {
						iconCls:'icon-clear',
						handler: function(){
							dlgPlaninfo.workerSearch('',dlgPlaninfo.content.agents,'#cxgroplistpane');
						}
					});
					$("#zjysearch").searchbox({
						searcher:function(value,name){
							value = $.trim(value);
							dlgPlaninfo.workerSearch(value,dlgPlaninfo.content.checkers,'#zjygroplistpane');
						},
						height:30,
						width:228,
						prompt:'按姓名或账号搜索'
					}).textbox('addClearBtn', {
						iconCls:'icon-clear',
						handler: function(){
							dlgPlaninfo.workerSearch('',dlgPlaninfo.content.checkers,'#zjygroplistpane');
						}
					});
					var extractMethod = e.content.extractMethod.split('-');
					$('#extractMethod').html('每个坐席' + dlgPlaninfo.paramJson(extractMethod[0],'EXTRACT_PERIOD') + '抽取' + extractMethod[1] + '条');
					holly.post(holly.getPath()+"/rest/planRest/getPlanExtractDetail",{planId:e.content.planId},function(e1){
						if(e1.success) {
							if(e1.content) {
								$('#extractDetail').html('抽检人数:' + e1.content.EXTRACTUSERNUM + ',' + '抽检组数:' + e1.content.EXTRACTDEPTNUM);
							}
						}
						
					});
				}

				var paramJson = $.parseJSON(e.content.paramJson);
				dlgPlaninfo.paramJsonHtml(paramJson,e.content.textItemId);

				var agents_departmentArr = new Array();
				var agentsArr = new Array();
				var agents_map = new Array();
				var cxgropHtml = "";//坐席列表html
				for(var i in e.content.agents){
					//检索department在该数组中是否存在，不存在才会允许添加到departmentArr
					// if(agents_departmentArr.indexOf(e.content.agents[i].department)==-1){
					// 	agents_departmentArr.push(e.content.agents[i].department);
					// }
					if($.inArray(e.content.agents[i].department, agents_departmentArr)==-1){
						agents_departmentArr.push(e.content.agents[i].department);
					}
				}
				for(var i in agents_departmentArr){
					// 从e.content.agents中过滤出含有departmentArr的数据
					// var dept = e.content.agents.filter(function(n){
					// 	debugger;
					// 	return n.department==agents_departmentArr[i];
					// });
					var dept = new Array();
					$.each(e.content.agents,function(index,item){
						if(item.department==agents_departmentArr[i]){
							dept.push(item);
						}
					});
					agentsArr.push(dept);
				}
				agents_map = $.map(agentsArr,function(item,index){
					return{
						department:agentsArr[index][0].department,
						deptName:agentsArr[index][0].deptName,
						children:item
					}
				});

				cxgropHtml = dlgPlaninfo.workerHtmlList(agents_map);
				$('#cxgroplistpane').html(cxgropHtml);


				var checkers_departmentArr = new Array();
				var checkersArr = new Array();
				var checkers_map = new Array();
				var zjygropHtml = "";//坐席列表html
				for(var i in e.content.checkers){
					//检索department在该数组中是否存在，不存在才会允许添加到departmentArr
					// if(checkers_departmentArr.indexOf(e.content.checkers[i].department)==-1){
					// 	checkers_departmentArr.push(e.content.checkers[i].department);
					// }
					if($.inArray(e.content.checkers[i].department, checkers_departmentArr)==-1){
						checkers_departmentArr.push(e.content.checkers[i].department);
					}
				}
				for(var i in checkers_departmentArr){
					// 从e.content.agents中过滤出含有departmentArr的数据
					// var dept = e.content.checkers.filter(function(n){
					// 	return n.department==checkers_departmentArr[i];
					// });
					// checkersArr.push(dept);
					var dept = new Array();
					$.each(e.content.checkers,function(index,item){
						if(item.department==checkers_departmentArr[i]){
							dept.push(item);
						}
					});
					checkersArr.push(dept);
				}
				checkers_map = $.map(checkersArr,function(item,index){
					return{
						department:checkersArr[index][0].department,
						deptName:checkersArr[index][0].deptName,
						children:item
					}
				});

				zjygropHtml = dlgPlaninfo.workerHtmlList(checkers_map);
				$('#zjygroplistpane').html(zjygropHtml);
				var jibenxinxiHtml = '';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">质检来源</div>';
				jibenxinxiHtml += '<div class="icontrols">'+dataTypeMap[e.content.dataType]+'</div>';
				jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">质检计划</div>';
				jibenxinxiHtml += '<div class="icontrols">'+(e.content.planName?e.content.planName:'')+'</div>';
				jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">执行周期</div>';
				jibenxinxiHtml += '<div class="icontrols">'+(e.content.taskTimer?taskTimerMap[e.content.taskTimer]:'')+'</div>';
				jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">评分模板</div>';
				jibenxinxiHtml += '<div class="icontrols">';
				$.each(standardMap,function(index,item){
					if(item.value == e.content.standardId){
						jibenxinxiHtml += item.name;
					}
				});
				jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '</div>';
				// jibenxinxiHtml += '<div class="icontrol-group">';
				// jibenxinxiHtml += '<div class="icontrol-label">优秀免检</div>';
				// jibenxinxiHtml += '<div class="icontrols">';
				// if(e.content.exemption){
				// 	jibenxinxiHtml += e.content.exemption=='0'?'否':'是';
				// }
				// jibenxinxiHtml += '</div>';
				// jibenxinxiHtml += '</div>';
				jibenxinxiHtml += '<div class="icontrol-group">';
				jibenxinxiHtml += '<div class="icontrol-label">启用状态</div>';
				jibenxinxiHtml += '<div class="icontrols">'+stateMap[e.content.status]+'</div>';
				jibenxinxiHtml += '</div>';
				$('#jibenxinxi').html(jibenxinxiHtml);
			}else{
				holly.showError(e.errorMessage);
			}
		});
	},
	returnStandard:function(param){
		holly.post(holly.getPath() + '/rest/commonRest/queryStandardList',{},function(a){
			if(a.success){
				$.each(a.content,function(index,item){
					if(item.value==param){
						return item.name;
					}
				});
			}
		});

	},
	/**
	 * 返回坐席列表html
	 * @param map 数据 { array }
	**/
	workerHtmlList:function(map){
		var html = "";
		$.each(map, function(index, item){
			html += '<div class="groupitem">';
			html += '<div class="title">'+item.deptName+'</div>';
			html += '<ul class="list">';
			$.each(item.children, function(index1, item1){
				html += '<li title="'+item1.username+'('+item1.userCode+')">'+item1.username+'('+item1.userCode+')</li>';
			});
			html += '</ul>';
			html += '</div>';
		});
		return html;
	},
	paramJson:function(param,getdictname){
		if(param){
			var map = $("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':getdictname}});
			var text = '';
			$.each(param.split(","), function(index, item){
				text += (index==0?'':',')+(map[item.replace(/\'/g,'')]?map[item.replace(/\'/g,'')]:'');
			});
			return text;
		}else{
			return "";
		}
	},
	paramJsonHtml:function(paramJson,textItemId){
		if(dataType=='v8'){
			holly.post(holly.getPath()+'/rest/commonRest/getServiceTypeNames',{"serviceTypeId":paramJson.serviceType},function(e1){
				if(e1.success){
					if(e1.content){
						var serviceTypeArr = [];
						$.each(e1.content,function(index,item){
							serviceTypeArr.push(item.text);
						});
						dlgPlaninfo.serviceTypeText = serviceTypeArr.join('<br/>');
					}
				}
			},true);
			var paramJsonHtml = '<div class="icontrol-group"><div class="icontrol-label maincolor">关键词</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.words+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">通话时长</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.recoinfoLength+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">静音时长</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.silenceLength+'秒</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">关键词适配对象</div>';
			paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.paramJson(paramJson.wordType,'V8_WORDTYPE')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">业务类型</div>';
			paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.paramJson(paramJson.businessType,'BUSINESS_TYPE')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">服务类型</div>';
			paramJsonHtml += '<div class="icontrols">'+(dlgPlaninfo.serviceTypeText?dlgPlaninfo.serviceTypeText:"")+'</div>';
			paramJsonHtml += '</div>';
			// paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">分配策略</div>';
			// paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.paramJson(paramJson.allotMethod,'V8ALLOTMETHOD')+'</div>';
			// paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">满意度</div>';
			paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.paramJson(paramJson.satisfication,'V8SATISFACTION')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">用户级别</div>';
			paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.paramJson(paramJson.custLevel,'CUST_LEVEL')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">号码归属地</div>';
			paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.paramJson(paramJson.custArea,'CUST_AREA')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">用户品牌</div>';
			paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.paramJson(paramJson.custBand,'CUST_BAND')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">日期规则</div>';
			paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.paramJson(paramJson.dateRule,'DATE_RULE')+'</div>';
			paramJsonHtml += '</div>';
			if(paramJson.dateRule == '2') {
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">时间范围</div>';
				paramJsonHtml += '<div class="icontrols">'+paramJson.dayNumber+'</div>';
				paramJsonHtml += '</div>';
				
			} else {
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">人工来电开始时间</div>';
				paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.formatTime(paramJson.startTime)+'</div>';
				paramJsonHtml += '</div>';
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">人工来电结束时间</div>';
				paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.formatTime(paramJson.endTime)+'</div>';
				paramJsonHtml += '</div>';
			}
			
		}else if(dataType=='i8'){
			var paramJsonHtml = '<div class="icontrol-group"><div class="icontrol-label maincolor">关键词</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.words+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">会话时长</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.sessionLength+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">关键词适配对象</div>';
			paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.paramJson(paramJson.wordType,'I8_WORDTYPE')+'</div>';
			paramJsonHtml += '</div>';
			/*paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">服务类型</div>';
			paramJsonHtml += '<div class="icontrols">'+paramJson.serviceType+'</div>';
			paramJsonHtml += '</div>';*/
			/*paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">分配策略</div>';
			paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.paramJson(paramJson.allotMethod,'I8ALLOTMETHOD')+'</div>';
			paramJsonHtml += '</div>';*/
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">满意度</div>';
			paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.paramJson(paramJson.satisfaction,'I8SATISFACTION')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">关闭方式</div>';
			paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.paramJson(paramJson.closeType,'CLOSE_TYPE')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">会话等级</div>';
			paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.paramJson(paramJson.sessionType,'SESSION_TYPE')+'</div>';
			paramJsonHtml += '</div>';
			paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">日期规则</div>';
			paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.paramJson(paramJson.dateRule,'DATE_RULE')+'</div>';
			paramJsonHtml += '</div>';
			if(paramJson.dateRule == '2') {
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">时间范围</div>';
				paramJsonHtml += '<div class="icontrols">'+paramJson.dayNumber+'</div>';
				paramJsonHtml += '</div>';
				
			} else {
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">人工来电开始时间</div>';
				paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.formatTime(paramJson.startTime)+'</div>';
				paramJsonHtml += '</div>';
				paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">人工来电结束时间</div>';
				paramJsonHtml += '<div class="icontrols">'+dlgPlaninfo.formatTime(paramJson.endTime)+'</div>';
				paramJsonHtml += '</div>';
			}
			
		}
		paramJsonHtml += '<div class="icontrol-group"><div class="icontrol-label maincolor">标签</div>';
		paramJsonHtml += '<div class="icontrols" id="textItemName"></div>';
		paramJsonHtml += '</div>';
		$('#paramJsonDiv').html(paramJsonHtml);
		if(textItemId){
			holly.get(holly.getPath()+"/rest/commonRest/queryTextItem",{},function(e){
				if(e.success){
					var item_contentHtml = "";
					$.each(e.content,function(index,item){
						if(textItemId==item.textItemId){
							item_contentHtml += ' <div class="item_combo">';
							item_contentHtml += '<label class="label"><span class="text">'+item.itemName+'</span><a href="javascript:void(0)" class="item_icon" onclick="dlgPlaninfo.displaysubMenu(this,event)"></a></label>';
							item_contentHtml += '<div class="ul_list" onclick="event.stopPropagation()">';
							item_contentHtml += '<div class="ul_inside">';  
							item_contentHtml += '<div class="item-group clearfix">';
							item_contentHtml += '<div class="item-label">'+item.itemName+'匹配规则</div>';
							item_contentHtml += '<div class="item-val">'+item.content+'</div>';
							item_contentHtml += '</div>';
							item_contentHtml += '</div>';
							item_contentHtml +=  '<div class="item-angle"></div>'
							item_contentHtml += '</div>';
							item_contentHtml += '</div>';
							//$('#textItemName').html(item.itemName);
							$('#textItemName').html(item_contentHtml);
						}
					});
				}
			});
		}
	},
	
	displaysubMenu:function(obj,event) {
		$(".ul_list").hide();
		event.stopPropagation();
		var $this = $(obj);
		var ul_listDiv = $(obj).closest('.item_combo').children('.ul_list');
		// var x = $this.offset().left;
		// var y = $this.offset().top; 
		// var z = document.body.clientWidth;
		// var w = ul_listDiv.outerWidth();
		
		var left = $(obj).closest('.label').outerWidth()+110;
		ul_listDiv.toggle().css({'left':left});
		$(document).on('click',dlgPlaninfo.docDisplaysubMenu);
	},
	
	docDisplaysubMenu:function(){
		$(".ul_list").hide();
		$(document).off('click',dlgPlaninfo.docDisplaysubMenu);
	},
	/**
	 * 搜索坐席
	 * @param keyword 关键词 { string }
	 * @param content 数据 { array }
	 * @param id 要填充的div id名 { string }
	**/
	workerSearch:function(keyword,content,id){
		var departmentArr = new Array();
		var searchArr = new Array();
		var searchMap = new Array();
		var html = "";//坐席列表html
		for(var i in content){
			// if(departmentArr.indexOf(content[i].department)==-1){
			// 	if(content[i].username.indexOf(keyword)>=0 || content[i].userCode.indexOf(keyword)>=0){
			// 		departmentArr.push(content[i].department);
			// 	}
			// }
			if($.inArray(content[i].department, departmentArr)==-1){
				if(content[i].username.indexOf(keyword)>=0 || content[i].userCode.indexOf(keyword)>=0){
					departmentArr.push(content[i].department);
				}
			}
		}
		if(departmentArr.length > 0){
			for(var i in departmentArr){
				// var dept = content.filter(function(n){
				// 	if(n.username.indexOf(keyword)>=0 || n.userCode.indexOf(keyword)>=0){
				// 		return n.department==departmentArr[i];
				// 	}
				// });
				// searchArr.push(dept);
				var dept = new Array();
				$.each(content,function(index,item){
					if(item.username.indexOf(keyword)>=0 || item.userCode.indexOf(keyword)>=0){
						if(item.department==departmentArr[i]){
							dept.push(item);
						}
					}
				});
				searchArr.push(dept);
			}
			searchMap = $.map(searchArr,function(item,index){
				return{
					department:searchArr[index][0].department,
					deptName:searchArr[index][0].deptName,
					children:item
				}
			});
			html = dlgPlaninfo.workerHtmlList(searchMap);
			$(id).html(html);
		}else{
			$(id).html('<div class="nodata">暂无搜索结果</div>');
		}
	},
	importDatagrid:function(data, page){
		dlgPlaninfo.importTableHtml(data, page);
		$('#importDatagridPage').remove();
		if(data.length > 10){
			$('#importDatagridJsDiv').append('<div id="importDatagridPage"></div>');
			$('#importDatagridPage').pagination({
				total: data.length,
				pageSize:10,
				pageNumber:1,
				onSelectPage:function(pageNumber, pageSize){
					dlgPlaninfo.importTableHtml(data, pageNumber);
				}
			});
		}
	},
	
	formatTime:function(time) {
		var year = time.substr(0,4);
		var month = time.substr(4,2);
		var day = time.substr(6,2);
		var hour = time.substr(8,2);
		var minute = time.substr(10,2);
		var second = time.substr(12,2);
		return year + '-' + month + '-' + day + ' ' + hour + ":" + minute + ':' + second;
	},
	importTableHtml:function(data, page){
		var html = '';
		html += '<thead><tr><th>坐席工号</th><th>质检员工号</th><th>组别</th><th>坐席姓名</th><th>质检员姓名</th><th>分层类型</th><th>抽取条数</th></tr></thead>';
		html += '<tbody>';
		var arr = [];
		if(data.length > 10){
			arr = data.slice((page-1)*10, (page*10));
		}else{
			arr = data;
		}
		$.each(arr,function(index,item){
			html += '<tr>';
			html += '<td>'+item.agentCode+'</td>';
			html += '<td>'+item.checkerAgentCode+'</td>';
			html += '<td>'+item.agentDeptName+'</td>';
			html += '<td>'+item.agentName+'</td>';
			html += '<td>'+item.checkerName+'</td>';
			html += '<td>'+item.rank+'</td>';
			html += '<td>'+item.extractNumber+'</td>';
			html += '</tr>';
		});
		html += '</tbody>';
		$('#importDatagrid').html(html);
	}
}