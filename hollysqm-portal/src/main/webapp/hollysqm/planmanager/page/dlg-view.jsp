<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<script type="text/javascript" src="<%=request.getContextPath() %>/hollysqm/common/muplayer/dist/player.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/hollysqm/common/jquery-ui/jquery-ui-1.10.3.mouse_core.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/hollysqm/common/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/hollysqm/common/bootstrap/js/bootstrapslider.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/hollysqm/planmanager/js/dlg-view.js"></script>
<script type="text/javascript">
// 获取必要的数据字典MAP		
var satisfactionMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'SATISFACTION'}});
var serviceTypeMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'BUSINESS_TYPE'}});
var caseItemMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'CASE_ITEM'}});
$(function(){
	dlgView.init();
});
</script>
<div id="viewpagetion" class="viewpagetion"></div>
<div class="container-sqm1">
	<div class="heading">
		<i class="icon"></i>
		<div class="title">接触记录</div>
	</div>
	<div class="contentpane" style="padding:15px;">
		<div class="iform-horizontal column-3 labelmin iform-text" id="getRecordDiv">
		</div>
		<div class="container-voicechat" id="voicechatDiv" style="margin-bottom:20px;display:none;">
			<!-- 语音波形图 -->
			<div class="voicechatbox" id="divScroll">
				<img src="<%=request.getContextPath() %>/hollysqm/common/images/loading-mask.gif" class="voiceloading" id="imgVoice">
			</div>
			<div class="myautoplayer">
				<span class="myautoplayer-play icon-play"></span>
				<span class="myautoplayer-volume icon-volume-up"></span>
				<div id="voice" class="progress slider">
					<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
				</div>
				<span id="spanCurPos" style="padding-left: 15px; font-size: 11px;">00:00</span>
				/<span id="spanDuration" style="padding-left: 5px; font-size: 11px;">00:00</span>
			</div>
		</div>
		<div class="chatqa" id="chatqa" style="max-height:none;">
		</div>
	</div>
	<!-- 
	<jspinclude page="../../common/weigui.jsp" flush="true"/>
	 -->
</div>