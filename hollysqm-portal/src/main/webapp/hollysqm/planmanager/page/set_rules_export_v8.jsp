<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<%@ include file="/hollybeacon/common/meta.jsp"%>
	<title>设置抽取规则</title>
	<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
	<script src="../js/assignUser.js"></script>
	<script src="../js/set_rules_export_v8.js"></script>
	<link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
	<link rel="stylesheet" href="../css/set_rules.css">
	<script type="text/javascript">
		$(function() {
			assignUser.loadBind();
			set_rules.init();
			
			//开始时间
			$("#formstartTime").datetimebox({
			    value: holly.currentTime("yyyy-MM-dd")+" 00:00:00"
			});
			//结束时间
			$("#formendTime").datetimebox({
			    value: holly.currentTime("yyyy-MM-dd")+" 23:59:59"
			});


		});
	</script>
</head>
<body>
<form id="planDetail" action="" target="" method="post">
</form>
	<div class="container-sqm">
		<div class="heading clearfix">
			<i class="pic"></i>
			<div class="title" id="planEditTitle">新建质检计划</div>
		</div>
		<div class="contentpane">
			<div class="main_box">
			   <div class="tbl">
					<ul class="procee clearfix">
						<li class="item active">
							<i class="circle"></i>
							<i class="line"></i>
							<div class="text">填写基本信息</div>
						</li>
						<li class="item active">
							<i class="circle"></i>
							<i class="line"></i>
							<div class="text">设置抽取规则</div>
						</li>
						<li class="item" style="display:none;">
							<i class="circle"></i>
							<i class="line"></i>
							<div class="text">选择质检员</div>
						</li>
						<li class="item">
							<i class="circle"></i>
							<i class="line"></i>
							<div class="text">完成</div>
						</li>
					</ul>
				</div>
				<div class="mid_box">
					<form id="set_rulesform" method="post" class="easyui-form" data-options="novalidate:true">                        
						<div class="midbox_bar clearfix">
						<div class="bar_item">
								<span class="nam"><span>*</span>抽取范围</span>
								<div class="bar_group">
									<select name="extractPeriod" id="formextractPeriod" data-options="editable:false,width:'100%',required: true,validType:'mustSelect',novalidate:true"></select>
								</div>   
							</div>
							
							<span class="midbar-item clearfix">
								<a href="javascript:void(0)" class="easyui-linkbutton" onclick="set_rules.importPlanDetail(true);">导入</a>
								
							</span>
						</div>
						<%-- <div class="midbox_bar clearfix" style="margin-bottom: 20px;">
						<div class="midbox_bar clearfix">
							<div class="bar_item">
								<span class="nam">日期规则</span>
								<div class="bar_group">
									<input name="dateRule" id="formdateRule" data-options="width:'100%',editable:false,required: true,validType:'mustSelect',novalidate:true" >
									<div class="easyui-tooltip iconqus" title="固定时间：如每次执行均固定抽1月1号~1月30号；<br />固定天数：抽取执行计划开始时间往前多少天，如：90天；<br />固定周期：上次一次计划执行时间长度顺延，如：第一次1月1号~1月31号，第二次2月1号~2月28号，以此累推；
" data-options="position:'right',onShow:function(){
					$(this).tooltip('tip').css({
						backgroundColor: '#3E4750',
						borderColor: '#3E4750',
						color:'#fff'
					});
				}"><i class="icon-question-sign icon"></i></div>
								</div>   
							</div>
						</div> --%>
						<div class="midbox_bar clearfix" id="dateRule1and3" style="display:block;">
							<div class="bar_item">
								<span class="nam"><span>*</span>开始时间</span>
								<div class="bar_group">
									<input name="startTime" id="formstartTime" class="easyui-datetimebox" data-options="width:'100%',editable:false,novalidate:true" >
								</div>   
							</div>
							<div class="bar_item">
								<span class="nam"><span>*</span>结束时间</span>
								<div class="bar_group">
									<input name="endTime" id="formendTime" class="easyui-datetimebox" data-options="width:'100%',editable:false,novalidate:true" >
								</div>   
							</div>
						</div>
						<div class="midbox_bar clearfix" id="importDatagridDiv" style="display:none;">
							<div class="bar_item" style="width:60%;">
								<span class="nam" style="display: none">计划人员配置</span>
								<div class="bar_group" id="importDatagridJsDiv">
									<table id="importDatagrid" class="table table-bordered" style="width:100%;"></table>
								</div>
								<span class="nam" style="display: none;color: red">导入失败</span>
								<div class="bar_group" id="importErrorDatagridJsDiv">
									<table id="importErrorDatagrid" class="table table-bordered" style="width:100%;"></table>
								</div>   
							</div>
						</div>
						<!-- <div class="midbox_bar clearfix" id="dateRule2" style="display:none;">
							<div class="bar_item">
								<span class="nam"><span>*</span>时间范围</span>
								<div class="bar_group">
									<input name="dayNumber" id="formdayNumber" class="easyui-numberspinner" data-options="width:'100',novalidate:true,min:0,max:90">
								</div>   
							</div>
						</div> -->
						<div class="midbox_bar clearfix">
							<div class="bar_item" style="width: 60%;">
								<span class="nam">关键词</span>
								<div class="bar_group">
									<input name="words" id="formwords" class="easyui-textbox" data-options="width:'100%',height:70,multiline:true,validType:['length[0,200]']" >
									<div style="color:#999;font-size:12px;">注：(控制逻辑AND【并且】,OR【或】,NOT【排除】三种关系，*【通配符】可以匹配包含词，如：*套餐*，表示任意词中包含“套餐”即符合条件，词与词之间用空格隔开)</div>
								</div>   
							</div>
						</div>
						<div class="midbox_bar clearfix">
							<div class="bar_item">
								<span class="nam">关键词适配对象</span>
								<div class="bar_group">
									<select name="wordType" id="formwordType" data-options="editable:false,width:'100%'"></select>
								</div>   
							</div>
							<div class="bar_item clearfix">
								<div class="inner_item clearfix">
									<span class="nam"><span>*</span>通话时长(秒)</span>
									<div class="bar_group">
										<input class="easyui-numberspinner" name="recoinfoLengthstart" id="recoinfoLengthstart" data-options="width:'100%',required: true,novalidate:true,value:'1',min:1,max:6000">
									</div>
								</div>
								<div class="inner_item clearfix" style="width: 39%;">
									<span class="nam small">至</span>
									<div class="inner_input">
										<input class="easyui-numberspinner" name="recoinfoLengthend" id="recoinfoLengthend" data-options="width:'100%',required: true,novalidate:true,value:'6000',min:1,max:6000">
									</div>
								</div>
							</div> 
							<div class="bar_item">
								<span class="nam">品牌</span>
								<div class="bar_group">
									<select name="custBand" id="formcustBand" data-options="editable:false,width:'100%',multiple:true"></select>
								</div>   
							</div>
						</div>
						<div class="midbox_bar clearfix">
							<div class="bar_item">
								<span class="nam">城市</span>
								<div class="bar_group">
									<select name="custArea" id="formcustArea" data-options="editable:false,multiple:true,width:'100%'"></select>
								</div>   
							</div>
							<div class="bar_item clearfix">
								<div class="inner_item clearfix">
									<span class="nam"><span>*</span>静音时长(秒)</span>
									<div class="bar_group">
										<input class="easyui-numberspinner" name="silenceLengthstart" id="silenceLengthstart" data-options="width:'100%',required: true,novalidate:true,value:'0',min:0,max:6000">
									</div>
								</div>
								<div class="inner_item clearfix" style="width: 39%;">
									<span class="nam small">至</span>
									<div class="inner_input">
										<input class="easyui-numberspinner" name="silenceLengthend" id="silenceLengthend" data-options="width:'100%',required: true,novalidate:true,value:'6000',min:0,max:6000">
									</div>
								</div>
							</div>
						   <div class="bar_item">
								<span class="nam">级别</span>
								<div class="bar_group">
									<select name="custLevel" id="formcustLevel" data-options="editable:false,multiple:true,width:'100%'"></select>
								</div>   
							</div>
						</div>
						<div class="midbox_bar clearfix">
							<div class="bar_item">
								<span class="nam">满意度</span>
								<div class="bar_group">
									<select id="formSatisfication" name="satisfication" data-options="editable:false,multiple:true,width:'100%'"></select>
								</div>   
							</div>
							<div class="bar_item">
								<span class="nam">业务类型</span>
								<div class="bar_group">
									<select id="formbusinessType" name="businessType" data-options="editable:false,multiple:true,width:'100%'"></select>
								</div>   
							</div>							
							<div class="bar_item">
								<span class="nam">服务类型</span>
								<div class="bar_group">
									<select id="formserviceType" name="serviceType" data-options="editable:false,width:'100%',multiple:true"></select>
								</div>   
							</div>
						</div>
						<div class="midbox_bar clearfix">
							<span class="nam">标签</span>
							<div class="outside clearfix" style="width: auto" id="item_content">
							</div>
						</div>
						<input type="hidden" name="agent" />
					</form>
				</div>
				<div class="btn_group">
					<button class="easyui-linkbutton l-btn-main" type="button" style="width: 115px;" onclick="set_rules.formSubmit('last')">上一步</button>
					<button class="easyui-linkbutton l-btn-main" type="button" style="background-color:  #3E4750;border-color: #3E4750;width: 115px;" onclick="set_rules.formSubmit('next')">下一步</button>
				</div> 
			</div>
		</div>
		
	</div>
	
	<div id="dlg-import" class="easyui-dialog" title="导入人员" style="width: 650px; height: 500px;"data-options="closed:true,buttons: '#dlg-import-buttons'">
	<iframe id="iframe-import" name="iframe-import" frameborder="0" style="width: 100%; height: 100%; display: block;"></iframe>
</div>
<div id="dlg-import-buttons">
	<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red" onclick="set_rules.importPlanDetail(false);">导入</a>
	<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#dlg-import').dialog('close')">取消</a>
</div>
</body>
</html>