<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/hollybeacon/common/taglibs.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <%@ include file="/hollybeacon/common/meta.jsp" %>
    <title>选择质检员</title>
    <link rel="stylesheet" href="../css/set_rules.css">
    <script src="../js/assignUser.js"></script>
    <script src="${ctx}/hollysqm/planmanager/js/checker.js"></script>
    <script type="text/javascript">
        var loginUser = ${loginUser};

        $(function() {
            assignUser.loadBind();
            checker.init();
        });
    </script>
</head>
<body>
    <div class="container-sqm">
        <div class="heading clearfix">
            <i class="pic"></i>
            <div class="title" id="planEditTitle">新建质检计划</div>
        </div>
        <div class="contentpane">
            <div class="main_box">
               <div class="tbl">
                    <ul class="procee clearfix">
                        <li class="item active">
                            <i class="circle"></i>
                            <i class="line"></i>
                            <div class="text">填写基本信息</div>
                        </li>
                        <li class="item active">
                            <i class="circle"></i>
                            <i class="line"></i>
                            <div class="text">设置抽取规则</div>
                        </li>
                        <li class="item active">
                            <i class="circle"></i>
                            <i class="line"></i>
                            <div class="text">选择质检员</div>
                        </li>
                        <li class="item">
                            <i class="circle"></i>
                            <i class="line"></i>
                            <div class="text">完成</div>
                        </li>
                    </ul>
                </div>
                <div class="mid_box">
                    <div class="midbox_bar clearfix">
                        <span class="nam"><span>*</span>质检员</span>
                        <div class="contain">
                            <div class="easyui-layout" data-options="fit:true">
                                <div data-options="region:'west',border:false" style="width:500px;">
                                    <div class="easyui-panel" title="可选人员" data-options="width:480,height:'100%'" >
                                        <div class="easyui-layout" data-options="fit:true">
                                            <div data-options="region:'west',border:false,width:'50%'">
                                                <div class="treecontentpane">
                                                    <label class="easyui-radio"><input name="showStopDic" type="checkbox" value=""> 显示停用的部门</label>
                                                    <ul id="personTree"></ul>
                                                </div>
                                            </div>
                                            <div data-options="region:'center',border:false" style="padding:10px; border-left:1px solid #ddd;">
                                                <div class="easyui-layout" data-options="fit:true">
                                                    <div data-options="region:'north',border:false" style="height:50px; padding:8px 5px;">
                                                        <div><input id="searchName" data-options="width:'100%'"></div>
                                                    </div>
                                                    <div data-options="region:'center',border:false">
                                                        <div class="select-panel out">
                                                            <ul id="srcList" class="select-list">
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div data-options="region:'center',border:false">
                                    <div class="easyui-panel" title="已选人员" data-options="fit:true" >
                                        <div class="select-panel in">
                                            <ul id="targetList" class="select-list">
                                            </ul> 
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </div>
                        <input type="hidden" name="checker" />
                    </div>
                </div>
                <div class="btn_group">
                    <button class="easyui-linkbutton l-btn-main" type="button" style="width: 115px;" onclick="checker.formSubmit('last')">上一步</button>
                    <button class="easyui-linkbutton l-btn-main" type="button" style="background-color:  #3E4750;border-color: #3E4750;width: 115px;" onclick="checker.formSubmit('next')">下一步</button>
                </div> 
            </div>
        </div>
    </div>   
</body>
</html>