<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>质检计划</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/switch.css">
<script type="text/javascript" src="${ctx}/hollysqm/planmanager/js/planlist.js"></script>
<script type="text/javascript">
// 获取必要的数据字典MAP		
var dataTypeMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'DATATYPE'}});
var stateMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'STATUS'}});
var planNameMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'PLAN_NAME'}});
$(function() {
	planlist.init();
});
</script>
</head>
<body class="easyui-layout">
	<div data-options="region:'center',border:false" style="padding:10px;">
		<div class="easyui-layout" data-options="fit:true">
			<!-- 搜索区域 -->
			<div data-options="region:'north',border:false" style="padding-bottom:10px;">
				<form id="searchplanlist" method="post" class="search-form">
					<div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">质检来源</span>
							<div class="valueGroup">
								<select name="dataType" id="formDataType" data-options="editable:false,width:'100%'"></select>
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">质检计划</span>
							<div class="valueGroup">
								<input name="planName" class="easyui-textbox" data-options="width:'100%'" />
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">启用状态</span>
							<div class="valueGroup">
								<select name="status" id="formState" class="easyui-combobox" data-options="editable:false,width:'100%'"></select>
							</div>
						</span>
					</div>
					<div class="btn-group">
						<a href="javascript:;" class="operation search-btn" onclick="planlist.search()" title="搜索"></a>
						<a href="javascript:;" class="operation reset-btn" onclick="planlist.resetForm()" title="重置"></a>
					</div>
				</form>
			</div>
			<!-- 搜索区域 end -->	
			<div data-options="region:'center',border:false">
				<div id="tt">
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="planlist.addPlan()">新建</a> 
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="planlist.copyPlan()">复制计划</a> 
				</div>
				<div class="easyui-panel" title="质检计划" data-options="tools:'#tt',fit:true">
					<table id="planlistdatagrid" ></table>
				</div>
			</div>
		</div>
	</div>
	
<div class="easyui-dialog" id="dlg-templet" title="选择质检计划" style="width: 400px; height:200px;padding: 10px" data-options="closed:true,buttons:'#dlg-buttons-templet'">
		<form id="formtemplet" class="easyui-form iform-horizontal" method="post" data-options="novalidate:true">
			<div class="icontrol-group">
				<div class="icontrol-label">选择质检计划</div>
				<div class="icontrols">
					<select name="planId" id="planId" data-options="editable:false,width:'100%',required: true,validType:'mustSelect',novalidate:true"></select>
				</div>
				<br/>
				<div class="icontrol-label">质检计划名</div>
				<div class="icontrols">
					<input name="planName" class="easyui-textbox" data-options="width:'100%'" />
				</div>
				
				<div class="btn-group" style="margin-top:15px">
					<a href="javascript:void(0)" style="margin-left:140px" class="easyui-linkbutton" onclick="planlist.submit()">提交</a> 
					<a href="javascript:void(0)" class="easyui-linkbutton" onclick="planlist.reset()">重置</a> 	
				</div>
				
			</div>
			
		</form>
		
	</div>
<div id="dlg-planinfo" class="easyui-dialog" style="padding:10px;"
		data-options="
			title:'查看计划详情',
			closed:true,
			width:750,
			height:'95%'
		">
</div>

<div id="dlg-plancensus" class="easyui-dialog" style="padding:10px;"
		data-options="
			title:'计划抽取情况',
			closed:true,
			width:750,
			height:'95%'
		">
</div>

</body>
</html>