<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/hollybeacon/common/taglibs.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
    <%@ include file="/hollybeacon/common/meta.jsp" %>
    <title>完成</title>
    <link rel="stylesheet" href="../css/finish.css">
    <link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
    <script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
    <script src="${ctx}/hollysqm/planmanager/js/finish.js"></script>
<script>
// 获取必要的数据字典MAP
var dataTypeMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'DATATYPE'}});
var stateMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'STATUS'}});
var standardMap = [];
holly.post(holly.getPath() + '/rest/commonRest/queryStandardList',{},function(e){
    if(e.success){
        standardMap = e.content;
    }else{
        holly.showError(e.errorMessage);
    }
});
var taskTimerMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'TASK_TIMER'}});

$(function() {
    finish.init();
});
</script>
<link rel="stylesheet" href="../css/dlg-planinfo.css">
</head>
<body>
    <div class="container-sqm">
        <div class="heading clearfix">
            <i class="pic"></i>
            <div class="title" id="planEditTitle">新建质检计划</div>
        </div>
        <div class="main_box" style="padding: 20px;">
            <div class="tbl">
                <ul class="procee clearfix">
                    <li class="item active">
                        <i class="circle"></i>
                        <i class="line"></i>
                        <div class="text">填写基本信息</div>
                    </li>
                    <li class="item active">
                        <i class="circle"></i>
                        <i class="line"></i>
                        <div class="text">设置抽取规则</div>
                    </li>
                    <li class="item active">
                        <i class="circle"></i>
                        <i class="line"></i>
                        <div class="text">选择质检员</div>
                    </li>
                    <li class="item active">
                        <i class="circle"></i>
                        <i class="line"></i>
                        <div class="text">完成</div>
                    </li>
                </ul>
                <div class="finish_icon">
                    <i class="finish"></i>
                    <div class="text">您已完成了全部设置，请核对计划信息后保存</div>
                </div>
            </div>    
<!-- 基本信息 -->
            <div class="container-sqm1">
                <div class="heading">
                    <i class="icon"></i>
                    <div class="title">基本信息</div>
                </div>
                <div class="contentpane">
                    <div class="iform-horizontal iform-text" id="jibenxinxi">
                    </div>
                </div>
            </div>
            <!-- 抽取规则 -->
            <div class="container-sqm1">
                <div class="heading">
                    <i class="icon"></i>
                    <div class="title">抽取规则</div>
                </div>
                <div class="contentpane">
                    <div class="iform-horizontal iform-text">
                        <div class="icontrol-group">
                            <div class="icontrol-label">抽取策略</div>
                            <div class="icontrols" id="extractMethod"></div>
                            <div class="icontrols" id="extractDetail"></div>
                        </div>
                        <div class="icontrol-group">
                            <div class="icontrol-label">抽取条件</div>
                            <div class="icontrols" id="paramJsonDiv"></div>
                        </div>
                        <div class="icontrol-group">
                            <div class="icontrol-label">抽取坐席</div>
                            <div class="icontrols">
                                <div style="margin-bottom:15px;"><input id="cxsearch"></input></div>
                                <div class="cxgroplistpane" id="cxgroplistpane">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- 质检员 -->
            <div class="container-sqm1">
                <div class="heading">
                    <i class="icon"></i>
                    <div class="title">质检员</div>
                </div>
                <div class="contentpane">
                    <div class="iform-horizontal iform-text">
                        <div class="icontrol-group">
                            <div class="icontrol-label">质检员</div>
                            <div class="icontrols">
                                <div style="margin-bottom:15px;"><input id="zjysearch"></input></div>
                                <div class="cxgroplistpane" id="zjygroplistpane">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="btn_group">
                <button class="easyui-linkbutton l-btn-main" type="button" style="width: 115px;" onclick="finish.formSubmit()">保存</button>
                <button class="easyui-linkbutton l-btn-main" type="button" style="background-color:  #3E4750;border-color: #3E4750;width: 115px;" onclick="$('#dlg-view').dialog('open')">预览</button>
                <a href="javascript:;" onclick="finish.jumpBasicPage()">修改</a>
            </div>
        </div>
    </div>
    <div id="dlg-view" class="easyui-dialog" title="预览" style="padding:10px" data-options="closed:true,width:800,height:450">  
    </div>
</body>
</html>