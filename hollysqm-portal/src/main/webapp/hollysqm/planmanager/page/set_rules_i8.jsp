<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<%@ include file="/hollybeacon/common/meta.jsp"%>
	<title>设置抽取规则</title>
	<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
	<script src="../js/assignUser.js"></script>
	<script src="../js/set_rules_i8.js"></script> 
	<link rel="stylesheet" href="../css/set_rules.css">
	<script type="text/javascript">
        var loginUser = ${loginUser};

        $(function() {
			assignUser.loadBind();
			set_rules.init();
		});
	</script>
</head>
<body>
	<div class="container-sqm">
		<div class="heading clearfix">
			<i class="pic"></i>
			<div class="title" id="planEditTitle">新建质检计划</div>
		</div>
		<div class="contentpane">
			<div class="main_box">
			   <div class="tbl">
					<ul class="procee clearfix">
						<li class="item active">
							<i class="circle"></i>
							<i class="line"></i>
							<div class="text">填写基本信息</div>
						</li>
						<li class="item active">
							<i class="circle"></i>
							<i class="line"></i>
							<div class="text">设置抽取规则</div>
						</li>
						<li class="item">
							<i class="circle"></i>
							<i class="line"></i>
							<div class="text">选择质检员</div>
						</li>
						<li class="item">
							<i class="circle"></i>
							<i class="line"></i>
							<div class="text">完成</div>
						</li>
					</ul>
				</div>
				<div class="mid_box">
					<form id="set_rulesform" method="post" class="easyui-form" data-options="novalidate:true">                        
						<div class="midbox_bar clearfix">
							<span class="nam"><span>*</span>抽取策略</span>
							<div class="outside">
								<select name="extractMethod" id="formextractMethod" data-options="editable:false,width:'100%',required: true,validType:'mustSelect',novalidate:true"></select>
							</div>
						</div>
						<div class="midbox_bar clearfix" style="margin-bottom: 20px;">
							<span class="nam"><span>*</span>抽取坐席</span>
							<div class="contain">
								<div class="easyui-layout" data-options="fit:true">
									<div data-options="region:'west',border:false" style="width:500px;">
										<div class="easyui-panel" title="可选人员" data-options="width:480,height:'100%'" >
											<div class="easyui-layout" data-options="fit:true">
												<div data-options="region:'west',border:false,width:'50%'">
													<div class="treecontentpane">
														<label class="easyui-radio"><input name="showStopDic" type="checkbox" value=""> 显示停用的部门</label>
														<ul id="personTree"></ul>
													</div>
												</div>
												<div data-options="region:'center',border:false" style="padding:10px; border-left:1px solid #ddd;">
													<div class="easyui-layout" data-options="fit:true">
														<div data-options="region:'north',border:false" style="height:50px; padding:8px 5px;">
															<div><input id="searchName" data-options="width:'100%'"></div>
														</div>
														<div data-options="region:'center',border:false">
															<div class="select-panel out">
																<ul id="srcList" class="select-list">
																</ul>
															</div>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
									<div data-options="region:'center',border:false">
										<div class="easyui-panel" title="已选人员" data-options="fit:true" >
											<div class="select-panel in">
												<ul id="targetList" class="select-list">
												</ul> 
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
						<div class="midbox_bar clearfix">
							<div class="bar_item">
								<span class="nam">日期规则</span>
								<div class="bar_group">
									<input name="dateRule" id="formdateRule" data-options="width:'100%',editable:false,required: true,validType:'mustSelect',novalidate:true" >
									<div class="easyui-tooltip iconqus" title="固定时间：如每次执行均固定抽1月1号~1月30号；<br />固定天数：抽取执行计划开始时间往前多少天，如：90天；<br />固定周期：上次一次计划执行时间长度顺延，如：第一次1月1号~1月31号，第二次2月1号~2月28号，以此累推；
" data-options="position:'right',onShow:function(){
					$(this).tooltip('tip').css({
						backgroundColor: '#3E4750',
						borderColor: '#3E4750',
						color:'#fff'
					});
				}"><i class="icon-question-sign icon"></i></div>
								</div>   
							</div>
						</div>
						<div class="midbox_bar clearfix" id="dateRule1and3" style="display:block;">
							<div class="bar_item">
								<span class="nam"><span>*</span>开始时间</span>
								<div class="bar_group">
									<input name="startTime" id="formstartTime" class="easyui-datebox" data-options="width:'100%',editable:false,novalidate:true" >
								</div>   
							</div>
							<div class="bar_item">
								<span class="nam"><span>*</span>结束时间</span>
								<div class="bar_group">
									<input name="endTime" id="formendTime" class="easyui-datebox" data-options="width:'100%',editable:false,novalidate:true" >
								</div>   
							</div>
						</div>
						<div class="midbox_bar clearfix" id="dateRule2" style="display:none;">
							<div class="bar_item">
								<span class="nam"><span>*</span>时间范围</span>
								<div class="bar_group">
									<input name="dayNumber" id="formdayNumber" class="easyui-numberspinner" data-options="width:'100',novalidate:true,min:0,max:90">
								</div>   
							</div>
						</div>
						<div class="midbox_bar clearfix">
							<div class="bar_item" style="width: 60%;">
								<span class="nam">关键词</span>
								<div class="bar_group">
									<input name="words" id="formwords" class="easyui-textbox" data-options="width:'100%',height:70,multiline:true,validType:['length[0,200]']" >
									<div style="color:#999;font-size:12px;">注：(控制逻辑AND【并且】,OR【或】,NOT【排除】三种关系，*【通配符】可以匹配包含词，如：*套餐*，表示任意词中包含“套餐”即符合条件，词与词之间用空格隔开)</div>
								</div>   
							</div>
						</div>
						<div class="midbox_bar clearfix">
							<div class="bar_item">
								<span class="nam">关键词适配对象</span>
								<div class="bar_group">
									<select name="wordType" id="formwordType" data-options="editable:false,width:'100%'"></select>
								</div>   
							</div>
							<div class="bar_item clearfix">
								<div class="inner_item clearfix">
									<span class="nam"><span>*</span>会话时长(秒)</span>
									<div class="bar_group">
										<input class="easyui-numberspinner" name="sessionLengthstart" id="sessionLengthstart" data-options="width:'100%',required: true,novalidate:true,value:'1',min:1,max:6000">
									</div>
								</div>
								<div class="inner_item clearfix" style="width: 39%;">
									<span class="nam small">至</span>
									<div class="inner_input">
										<input class="easyui-numberspinner" name="sessionLengthend" id="sessionLengthend" data-options="width:'100%',required: true,novalidate:true,value:'6000',min:1,max:6000">
									</div>
								</div>
							</div> 
							<div class="bar_item">
								<span class="nam">关闭方式</span>
								<div class="bar_group">
									<select name="closeType" id="formcloseType" data-options="editable:false,multiple:true,width:'100%'"></select>
								</div>    
							</div>
						</div>
						<div class="midbox_bar clearfix">
							<div class="bar_item">
								<span class="nam">会话等级</span>
								<div class="bar_group">
									<select name="sessionType" id="formsessionType" data-options="editable:false,multiple:true,width:'100%'"></select>
								</div>   
							</div>
							<div class="bar_item">
								<span class="nam">满意度</span>
								<div class="bar_group">
									<select id="formSatisfication" name="satisfaction" data-options="editable:false,multiple:true,width:'100%'"></select>
								</div>   
							</div>
							<!-- <div class="bar_item">
								<span class="nam">服务类型</span>
								<div class="bar_group">
									<select id="formbusinessType" name="businessType" data-options="editable:false,multiple:true,width:'100%'"></select>
								</div>   
							</div> -->
						</div>
						<div class="midbox_bar clearfix">
							<span class="nam">标签</span>
							<div class="outside clearfix" style="width: auto" id="item_content">
							</div>
						</div>
						<input type="hidden" name="agent" />
					</form>
				</div>
				<div class="btn_group">
					<button class="easyui-linkbutton l-btn-main" type="button" style="width: 115px;" onclick="set_rules.formSubmit('last')">上一步</button>
					<button class="easyui-linkbutton l-btn-main" type="button" style="background-color:  #3E4750;border-color: #3E4750;width: 115px;" onclick="set_rules.formSubmit('next')">下一步</button>
				</div> 
			</div>
		</div>
		
	</div>
</body>
</html>