<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>执行记录</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<script type="text/javascript" src="../js/planrecord.js" ></script>
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type="text/javascript">
$(function() {
	planRecord.init();
});
</script>
</head>
<body class="easyui-layout">
	<div data-options="region:'center',border:false" style="padding:10px;">
		<div class="easyui-layout" data-options="fit:true">
			<!-- 搜索区域 -->
			<div data-options="region:'north',border:false" style="padding-bottom:10px;">
				<form id="searchPlanRecord" method="post" class="search-form">
					<input type="hidden" id="planId" name="planId" value= "${param.planId}" />
					<div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">日期</span>
							<div class="valueGroup">
								<input type="text" name="startTime" class="easyui-datebox" data-options="width:'100%',editable:false">
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">至</span>
							<div class="valueGroup">
								<input type="text" name = "endTime" class="easyui-datebox" data-options="width:'100%',editable:false">
							</div>
						</span>				
					</div>
					<div class="btn-group">
						<a href="javascript:;" class="operation search-btn" onclick="planRecord.search()" title="搜索"></a>
						<a href="javascript:;" class="operation reset-btn" onclick="$('#searchPlanRecord').form('reset');" title="重置"></a>
					</div>
				</form>
			</div>
			<!-- 搜索区域 end -->	
			<div data-options="region:'center',border:false">
				<div class="easyui-panel" title="执行记录" data-options="tools:'#tt',fit:true">
					<table id="PlanRecorddatagrid" ></table>
				</div>
			</div>
		</div>
	</div>	
</body>
</html>