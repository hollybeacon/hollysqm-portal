<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/hollybeacon/common/taglibs.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
	<%@ include file="/hollybeacon/common/meta.jsp" %>
	<title>填写基本信息</title>
	<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
	<link rel="stylesheet" href="../css/basic_info.css">
	<script src="${ctx}/hollysqm/planmanager/js/basic_info.js"></script>
	<script type="text/javascript">
	// 获取必要的数据字典MAP    
	$(function() {
		basic_info.init();
	});
	</script>
</head>
<body>

	<div class="container-sqm">
		<div class="heading clearfix">
			<i class="pic"></i>
			<div class="title" id="planEditTitle">新建质检计划</div>
		</div>
		<div class="contentpane">
			<div class="main_box">
			   <div class="tbl">
					<ul class="procee clearfix">
						<li class="item active">
							<i class="circle"></i>
							<i class="line"></i>
							<div class="text">填写基本信息</div>
						</li>
						<li class="item">
							<i class="circle"></i>
							<i class="line"></i>
							<div class="text">设置抽取规则</div>
						</li>
						<li class="item" id="checkeritem">
							<i class="circle"></i>
							<i class="line"></i>
							<div class="text">选择质检员</div>
						</li>
						<li class="item">
							<i class="circle"></i>
							<i class="line"></i>
							<div class="text">完成</div>
						</li>
					</ul>
				</div>
				<div class="mid_box clearfix">
					<form id="basic_infoform" method="post" class="easyui-form" data-options="novalidate:true">
						<div class="midbar clearfix">
							<span class="midbar-item clearfix">
								<span class="label"><span class="red">*</span>质检来源</span>
								<div class="valueGroup">
									<select name="dataType" id="formDataType" data-options="editable:false,readonly:true,width:'100%'"></select>
								</div>
							</span>
							<span class="midbar-item clearfix">
								<span class="label"><span class="red">*</span>质检计划</span>
								<div class="valueGroup">
								   <input name="planName" class="easyui-textbox " data-options="width:'100%',required: true,novalidate:true,validType:['length[1,20]']">
								</div>
							</span>
							<span class="midbar-item clearfix">
								<span class="label"><span class="red">*</span>执行周期</span>
								<div class="valueGroup">
									<select name="taskTimer" id="formTimer" data-options="editable:false,width:'100%',required: true,validType:'mustSelect',novalidate:true"></select>
								</div>
							</span>
						</div>
						<div class="midbar clearfix">
							<span class="midbar-item clearfix">
								<span class="label"><span class="red">*</span>评分模板</span>
								<div class="valueGroup">
									<select name="standardId" id="formStandardId" data-options="editable:false,width:'100%',required: true,validType:'mustSelect',novalidate:true"></select>
								</div>
							</span>
							<span class="midbar-item clearfix" style="display:none;">
								<span class="label">优秀免检</span>
								<div class="valueGroup" id="exemptionDiv" style="margin-top: 5px;">
								</div>
							</span>
							<span class="midbar-item clearfix">
								<span class="label">启用状态</span>
								<div class="valueGroup">
									<input name="status" id="formState" data-options="width:'100%',editable:false" readonly="readonly" value="0">
								</div>
							</span>
							
							<span class="midbar-item clearfix">
								<span class="label">导入人员</span>
								<div class="valueGroup">
									<select name="isExport" id="formExport" data-options="width:'100%',editable:false,readonly:true"></select>
								</div>
							</span>
			
						</div>
					</form>
				</div>
				<div class="bott">
					<button class="easyui-linkbutton l-btn-main" style="width: 115px;" type="button" onclick="basic_info.formSubmit()">下一步</button>
				</div>
			</div>
		</div>
	</div>


</body>
</html>