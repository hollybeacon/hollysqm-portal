<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%
	String planId = request.getParameter("planId");
	String dataType = request.getParameter("dataType");
%>
<script type="text/javascript">
var planId = "<%=planId%>";
var dataType = "<%=dataType%>";
</script>
<script type="text/javascript" src="<%=request.getContextPath() %>/hollysqm/planmanager/js/dlg-planinfo.js"></script>
<script type="text/javascript">
$(function(){
	dlgPlaninfo.init();
});
</script>
<link rel="stylesheet" href="<%=request.getContextPath() %>/hollysqm/planmanager/css/dlg-planinfo.css">
<!-- 基本信息 -->
<div class="container-sqm1">
	<div class="heading">
		<i class="icon"></i>
		<div class="title">基本信息</div>
	</div>
	<div class="contentpane">
		<div class="iform-horizontal iform-text" id="jibenxinxi">
		</div>
	</div>
</div>
<!-- 抽取规则 -->
<div class="container-sqm1">
	<div class="heading">
		<i class="icon"></i>
		<div class="title">抽取规则</div>
	</div>
	<div class="contentpane">
		<div class="iform-horizontal iform-text">
			<div class="icontrol-group">
				<div class="icontrol-label">抽取策略</div>
				<div class="icontrols" id="extractMethod"></div>
				<div class="icontrols" id="extractDetail"></div>
			</div>
			<div class="icontrol-group">
				<div class="icontrol-label">抽取条件</div>
				<div class="icontrols" id="paramJsonDiv"></div>
			</div>
			<div class="icontrol-group" id="cxgroplistpaneDiv" style="display:none;">
				<div class="icontrol-label">抽取坐席</div>
				<div class="icontrols">
					<div style="margin-bottom:15px;"><input id="cxsearch"></input></div>
					<div class="cxgroplistpane" id="cxgroplistpane">
					</div>
				</div>
			</div>
			<div class="icontrol-group" id="importDatagridDiv" style="display:none;">
				<div class="icontrol-label">导入人员配置</div>
				<div class="icontrols" id="importDatagridJsDiv" style="width:500px;">
					<table id="importDatagrid" class="table table-bordered" style="width:100%;"></table>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- 质检员 -->
<div class="container-sqm1" id="zjygroplistpaneDiv" style="display:none;">
	<div class="heading">
		<i class="icon"></i>
		<div class="title">质检员</div>
	</div>
	<div class="contentpane">
		<div class="iform-horizontal iform-text">
			<div class="icontrol-group">
				<div class="icontrol-label">质检员</div>
				<div class="icontrols">
					<div style="margin-bottom:15px;"><input id="zjysearch"></input></div>
					<div class="cxgroplistpane" id="zjygroplistpane">
					</div>
				</div>
			</div>
		</div>
	</div>
</div>