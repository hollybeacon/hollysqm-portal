var check = {
	/**
	 * 初始化加载数据  
	 */
	paramgetUnscoredPaperMsg:{
		"dataType":holly.getUrlParams("dataType")?holly.getUrlParams("dataType"):"",
		"agent":holly.getUrlParams("agent")?holly.getUrlParams("agent"):"",
		"checker":holly.getUrlParams("checker")?holly.getUrlParams("checker"):"",
		"endTime":holly.getUrlParams("endTime")?holly.getUrlParams("endTime"):"",
		"planId":holly.getUrlParams("planId")?holly.getUrlParams("planId"):"",
		"startTime":holly.getUrlParams("startTime")?holly.getUrlParams("startTime"):"",
		"paperId":holly.getUrlParams("paperId")?holly.getUrlParams("paperId"):""
	},
	contactId:holly.getUrlParams("contactId")?holly.getUrlParams("contactId"):"",
	frameId:holly.getUrlParams("frameId")?holly.getUrlParams("frameId"):"",
	thisIframeId : window.frameElement && window.frameElement.id || '',// 获取该页面iframe id
	totalScoreReal:0,
	init:function(){
		$("#caseItem").combobox("init",{'codeType':'CASE_ITEM'});

		var param = {
			"dataType":holly.getUrlParams("dataType")?holly.getUrlParams("dataType"):"",
			"paperId":holly.getUrlParams("paperId")?holly.getUrlParams("paperId"):""
		};

		if(check.thisIframeId=='iframecheck'){//如果该页面从待复核列表打开
			// 其他待复核
			holly.post(holly.getPath()+"/rest/uncheckedRest/getUncheckedPaperMsg",check.paramgetUnscoredPaperMsg,function(e){
				if(e.success){
					if(e.content){
						$('#paperCount').html(e.content.paperCount+'条');
						$('#paperCountText').html('提交后自动显示下一质检单');
						check.nextPaperId=e.content.nextPaperId;
					}
				}
			});
		}
		// 质检单评分查询
		holly.post(holly.getPath()+"/rest/commonRest/getPaperScoreResult",{"paperId":param.paperId},function(e){
			if(e.success){
				if(e.content){
					$('#totalScoreDiv').html(e.content.totalScore+'分');
					$('#checkerInfoDiv').html(e.content.scorerName+'('+e.content.scorerCode+')');
					$('#scoreTimeDiv').html(e.content.scoreTime);
					$('#comments').textbox('setValue', e.content.comments);
					//$('#commentsDiv').html(e.content.comments);
					
 				}
			}
		});
		// 获取当前质检单绑定的典型案例
		holly.post(holly.getPath()+"/rest/commonRest/getPaperCase",{"paperId":param.paperId},function(e){
			if(e.success){
				if(e.content){
					//$('#caseItemDiv').html(caseItemMap[e.content.caseItem]);
					if(e.content.caseItem){
						$("#caseItem").combobox('setValue',e.content.caseItem);
						//$('#recordTypeDiv').html(e.content.recordType=="1"?"是":"否");
						if(e.content.recordType == "1") {//优秀录音
							$('#recordType1').attr('checked', 'checked');
						} else {//问题录音
							$('#recordType2').attr('checked', 'checked');
						}
						check.recordTypeEvent(e.content.recordType);
					}else{
						$('#recordType2').attr('checked', 'checked');
					}
					
				}
			}
		});
		// 查询申诉信息
		holly.get(holly.getPath()+"/rest/commonRest/queryAppealPaper",{"paperId":param.paperId},function(e){
			if(e.success){
				if(e.content){
					$('#appealReasonDiv').html(holly.replace_str(e.content.appealReason,false));
					//$('#recordTypeDiv').html(e.content.recordType);
					check.appealId = e.content.appealId?e.content.appealId:"";
				}
			}
		});

		holly.post(holly.getPath()+"/rest/commonRest/getRecord",param,function(e){
			if(e.success){
				var content = e.content;
				var html = '';
				var chatHtml = '';
				var arrFlag = [];
				var arrText = [];
				check.contactId = content.contactId;
				check.acceptTime = content.acceptTime;
				check.recordFile = content.recordFile;
				check.planId = content.planId;
				check.dataType = content.dataType;
				//显示接触记录绑定的质检计划信息
				if(content.planId) {
					if(content.planId=='system'){
						$('#planDetail').html('所属质检计划:' + content.planName);
					}else{
						$('#planDetail').html('<a class="sublink" href="javascript:;" onclick="check.planinfo()">所属质检计划:' + content.planName +'</a>');
					}
				}
				if(check.paramgetUnscoredPaperMsg.dataType=='v8'){
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">来电号码</div>';
					if(content.caller != null) {
						html += '<div class="icontrols">'+content.caller+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">坐席</div>';
					if(content.user != null){
						html += '<div class="icontrols">'+content.user.username+'('+content.user.userCode+')'+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">归属地</div>';
					if(content.custArea != null) {
						html += '<div class="icontrols">'+check.paramJson(content.custArea,'CUST_AREA')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">品牌</div>';
					if(content.custBand != null) {
						html += '<div class="icontrols">'+check.paramJson(content.custBand,'CUST_BAND')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">满意度</div>';
					if(content.satisfaction != null) {
						html += '<div class="icontrols">'+check.paramJson(content.satisfaction,'V8SATISFACTION')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">业务类型</div>';
					if(content.serviceType != null) {
						html += '<div class="icontrols">'+check.paramJson(content.bussinessType,'BUSINESS_TYPE')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">开始时间</div>';
					if(content.startTime != null) {
						html += '<div class="icontrols">'+content.startTime+'</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">通话时长</div>';
					if(content.length != null) {
						html += '<div class="icontrols">'+content.length+'秒</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">用户级别</div>';
					if(content.custLevel != null) {
						html += '<div class="icontrols">'+check.paramJson(content.custLevel,'CUST_LEVEL')+'</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">静音时长</div>';
					if(content.custLevel != null) {
						html += '<div class="icontrols">'+ content.silenceLength +'秒</div>';
					}
					html += '</div>';
					
					$('#voicechatDiv').show();
					// 接触记录图片音频地址查询
					holly.post(holly.getPath()+"/rest/commonRest/getRecordDir",{"contactId":check.contactId, "acceptTime":check.acceptTime, "recordFile":check.recordFile},function(e){
						if(e.success){
							listenTape.init(e.content.wavDir,e.content.imgDir);
						}else{
							toastr.error(e.errorMessage);
						}
					});
				}else if(check.paramgetUnscoredPaperMsg.dataType=='i8'){
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">来话号码</div>';
					if(content.caller != null) {
						html += '<div class="icontrols">'+content.caller+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">坐席</div>';
					if(content.user != null){
						html += '<div class="icontrols">'+content.user.username+'('+content.user.userCode+')'+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">关闭方式</div>';
					if(content.closeType != null) {
						html += '<div class="icontrols">'+check.paramJson(content.closeType,'CLOSE_TYPE')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">会话等级</div>';
					if(content.sessionType != null) {
						html += '<div class="icontrols">'+check.paramJson(content.sessionType,'SESSION_TYPE')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">满意度</div>';
					if(content.satisfaction != null) {
						html += '<div class="icontrols">'+check.paramJson(content.satisfaction,'I8SATISFACTION')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">服务类型</div>';
					if(content.serviceType != null) {
						html += '<div class="icontrols">'+content.serviceType+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">开始时间</div>';
					if(content.startTime != null) {
						html += '<div class="icontrols">'+content.startTime+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">结束时间</div>';
					if(content.endTime != null) {
						html += '<div class="icontrols">'+content.endTime+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">会话时长</div>';
					if(content.length != null) {
						html += '<div class="icontrols">'+content.length+'秒</div>';
					}
					html += '</div>';					
				}
				$('#getRecordDiv').html(html);

				arrFlag= content.txtContent.match(/n\d#/ig);
				arrText= content.txtContent.split(/n\d#/ig);
				for(var i=0; i<arrFlag.length; i++){
					if (arrFlag[i]=='n0#') {
						chatHtml += '<div class="text worker"><span style="text-align:right;">'+(i+1)+'&nbsp;&nbsp;</span>坐席：'+(arrText[i+1])+'</div>';
					}else if(arrFlag[i]=='n1#'){
						chatHtml += '<div class="text visiter"><span style="text-align:right;">'+(i+1)+'&nbsp;&nbsp;</span>客户：'+(arrText[i+1])+'</div>';
					}
				}
				$('#chatqa').html(chatHtml);
			}else{
				toastr.error(e.errorMessage);
			}
		});

		
		check.getItemScoreList("1","#scoreitemlistText");
		check.getItemScoreList("0","#scoreitemlist");
	},
	paramJson:function(param,getdictname){
		if(param){
			var map = $("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':getdictname}});
			var text = '';
			$.each(param.split(","), function(index, item){
				text += (index==0?'':',')+(map[item]?map[item]:'');
			});
			return text;
		}
	},
	getItemScoreList:function(orderNum,el){
		holly.post(holly.getPath()+"/rest/commonRest/getItemScoreList",{"orderNum":orderNum, "paperId":check.paramgetUnscoredPaperMsg.paperId},function(e){
			if(e.success){
				if(e.content){
					var content = e.content;
					var html = "";
					if(orderNum=="0"){
						for(var i=0; i<content.length; i++){
							html += '<li><label title="'+content[i].scoreItemName+'"><input type="checkbox" onchange="check.scoreItemCheck(this,'+(content[i].scoreType=="0"?"+":"-")+content[i].score+')" value="'+content[i].scoreItemId+'" class="chk" name="scoreItems"/><span class="text">'+content[i].scoreItemName+'</span><span class="point">'+(content[i].scoreType=="0"?"+":"-")+content[i].score+'</span></label></li>';
						}
					}else{
						if(content.length > 0){
							for(var i=0; i<content.length; i++){
								html += '<li title="'+content[i].scoreItemName+'"><span class="text" title="'+content[i].scoreItemName+'">'+content[i].scoreItemName+'</span><span class="point">'+(content[i].scoreType=="0"?"+":"-")+content[i].score+'</span></li>';
							}
						}else{
							html += "无扣分项";
						}
					}
					$(el).html(html);
				}
			}else{
				toastr.error(e.errorMessage);
			}
		});
	},
	scoreItemCheck:function(obj,point){
		var val = 100;
		if(obj.checked){
			check.totalScoreReal += point;
		}else{
			check.totalScoreReal -= point;
		}
		val += check.totalScoreReal;
		if(val > 100){
			val = 100;
		}else if(val < 0){
			val = 0;
		}
		$('#reviewScore').val(val);
		$('#reviewScoreDiv').html(val+'分');
	},
	saveScore:function(qualityStatus){
		$('#qualityStatus').val(qualityStatus);
		var scoreItem =[];
		$('input[name="scoreItems"]:checked').each(function() {
			scoreItem.push($(this).val());
		});
		var caseItem = $("#caseItem").combobox("getValue");
		var recordType = $('input[name="recordType"]:checked').val();
		if((recordType=='1') && (caseItem=="")){
			holly.showError('请选择质检案例');
			return false;
		}
		$("#itemId").val(scoreItem);
		
		$.messager.confirm('提醒', '您正在提交质检单，评分为' + $('#reviewScore').val() + '分,请确认评分和问题描述是否已输入完整?', function(r){
			if (r){
				check.formSubmit(qualityStatus);
			}
		});
		
	},
	formSubmit:function(qualityStatus){
		var formJson = holly.form2json($("#scoreForm"));
		var param = {};
		if(qualityStatus=="3"){
			param = {
				"caseItem":formJson.caseItem,
				"comments":formJson.comments,
				"contactId":check.contactId,
				"paperId":check.paramgetUnscoredPaperMsg.paperId,
				"isReview":formJson.isReview,
				"itemId":formJson.itemId,
				"qualityStatus":formJson.qualityStatus,
				"recordType":formJson.recordType,
				"reviewScore":formJson.reviewScore,
				"appealId":check.appealId,
				"status":"1"
			};
			check.formPost(param);
		}else if(qualityStatus=="4"){
			$('#dlg-turnedDown').dialog('open');
		}	
	},
	turnedDown:function(){
		if($('#formturnedDown').form('enableValidation').form("validate")){
			var formJson = holly.form2json($("#scoreForm"));
			var answer = $('#formanswer').textbox('getValue');
			param = {
				"paperId":check.paramgetUnscoredPaperMsg.paperId,
				"qualityStatus":formJson.qualityStatus,
				"appealId":check.appealId,
				"status":"2",
				"answer":answer
			};
			check.formPost(param);
		}
	},
	formPost:function(param){
		$.messager.progress({
			text:'正在提交...'
		});
		holly.post(holly.getPath()+"/rest/commonRest/scorePaper",param,function(e){
			if(e.success){
				if(param.qualityStatus == "3") {
					holly.showSuccess('复核评分成功');
				} else {
					$('#dlg-turnedDown').dialog('close');
					holly.showSuccess('驳回成功');
				}
				
				var nextParam = check.paramgetUnscoredPaperMsg;
				var url = holly.getPath()+'/hollysqm/unchecked/page/check.jsp';
				url += '?dataType='+nextParam.dataType;
				url += '&agent='+nextParam.agent;
				url += '&checker='+nextParam.checker;
				url += '&endTime='+nextParam.endTime;
				url += '&planId='+nextParam.planId;
				url += '&startTime='+nextParam.startTime;
				url += '&paperId='+check.nextPaperId;
				url += '&contactId='+check.contactId;
				url += '&frameId='+check.frameId;

				// 如果待复核列表页面没有关闭，并且该页面是从待复核列表打开的
				if((parent.$("#"+check.frameId).length>0) && (check.thisIframeId=='iframecheck')){
					var $obj = parent.$("#"+check.frameId)[0].contentWindow;
					$obj.unChecked.search();//重新搜索列表
				}
				if(check.thisIframeId=='iframecheck'){
					if((check.nextPaperId) && (check.nextPaperId!="0")){
						window.location.href=url;
					}else{
						alert('已完成最后一条复核，待复核页面将会关闭');
						var $tabs = parent.$('#tabs');
						var curTab = $tabs.tabs('getSelected');
						var tabIndex = $tabs.tabs('getTabIndex',curTab);
						$tabs.tabs('close',tabIndex);
					}
				}else{
					var $tabs = parent.$('#tabs');
					var curTab = $tabs.tabs('getSelected');
					var tabIndex = $tabs.tabs('getTabIndex',curTab);
					$tabs.tabs('close',tabIndex);
				}
			}else{
				holly.showError(e.errorMessage);
			}
			$.messager.progress('close');
		});
	},
	
	planinfo:function(){
		var url = holly.getPath()+"/hollysqm/planmanager/page/dlg-planinfo.jsp";
		url += "?planId=" + check.planId;
		url += "&dataType=" + check.dataType;
		$('#dlg-planinfo').dialog('open').dialog('refresh',url);
	},
	recordTypeEvent:function(val){
		if(val=="1"){
			$('#caseItemRequired').show();
			$('#caseItem').combobox('enable');
		}else if(val=="2"){
			$('#caseItemRequired').hide();
			$('#caseItem').combobox('setValue','').combobox('disable');
		}
	}

};


var listenTape = {
	init:function(voicePath,imgPath){
		var player,//播放器
			intever = 0;//定时器 返回number类型
		player = new _mu.Player({
			baseDir: holly.getPath() + '/hollysqm/common/muplayer/dist'
		});//初始播放器
		//setSong(contextPath+path);//设置播放录音
		//音量调节
		$(document).on('sliderchange', "#voice", function (e, result) {
			player.setVolume(result.value);
		});
		$('.myautoplayer-play').on('click',function(){
			if($(this).hasClass('icon-play')){
				player.play();//播放事件
				$(this).removeClass("icon-play").addClass("icon-pause");
				intever = scrollImg();
			}else if($(this).hasClass('icon-pause')){
				player.pause();//暂停
				$(this).removeClass("icon-pause").addClass("icon-play");
				clearInterval(intever);//清除事件
			}
		});
		//图片点击播放事件
		$("#divScroll").click(function (e) {
			var scrollLeft = $("#divScroll").scrollLeft();
			var offset = $(this).offset();
			var relativeX = (e.pageX - offset.left) + scrollLeft;
			var len = parseInt(relativeX / $("#imgVoice").width() * player.duration() * 1000);
			player.play(len);
			intever = scrollImg();
			$(".icon-play").addClass("icon-pause").removeClass("icon-play");
		});
		//滚动图片
		function scrollImg() {
			return setInterval(function () {//改变滑动块
				var l = player.curPos() / player.duration();
				var left = $("#imgVoice").width();//图片宽度
				if(l >(1-900/left)){
					clearInterval(intever);//清除事件
					$(".icon-pause").addClass("icon-play").removeClass("icon-pause");
					return ;
				};
				$("#divScroll").animate({scrollLeft: left * l}, 1000);
			}, 1000);
		}

		//滚动条拖动事件
		$("#divScroll").mousedown(function () {
			var state = player.getState();
			if (state == 'playing') {
				player.pause();//暂停
				$(".icon-pause").addClass("icon-play").removeClass("icon-pause");
				clearInterval(intever);//清除事件
			}
		});
		player.setUrl(holly.getPath() + "/" + voicePath );//添加录音文件
		$("#imgVoice").attr("src",holly.getPath() + "/" + imgPath).removeClass('voiceloading').addClass('voicechatimg');
	}
};