<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>质检完成情况</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
<link rel="stylesheet" href="${ctx}/hollysqm/executioninfo/css/executioninfo.css">
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type= "text/javascript" src="${ctx}/hollysqm/executioninfo/js/executioninfo.js" ></script>
<script src="${ctx}/hollysqm/common/echarts/echarts.min.js"></script> 
<script type="text/javascript">

$(function() {
	$("#formDataType").combobox("init_extend",{'codeType':'DATATYPE'});
	executioninfo.planNameOption();
	
	executioninfo.init();
	
	$("#searchexecutioninfo").keyup(function(event){
		if(event.keyCode==13){
			executioninfo.search();
		}
	});
});
</script>
</head>
<body>
	<div class="banner">
		<form id="searchexecutioninfo" method="post" class="search-form">
			<input type="hidden" value="3" name="flag"/><!-- 自定义查询条件 -->
			<div class="screeningbar">
				<span class="screeningbar-item">
					<span class="label">日期</span>
					<div class="valueGroup">
						<input type="text" name="startTime" class="easyui-datebox" value="${startTime }" data-options="editable:false,width:'100%'">
					</div>
				</span>
				<span class="screeningbar-item">
					<span class="label">至</span>
					<div class="valueGroup">
						<input type="text" name="endTime" class="easyui-datebox" value="${endTime }" data-options="editable:false,width:'100%'">
					</div>
				</span>
				<span class="screeningbar-item">
					<span class="label">质检来源</span>
					<div class="valueGroup">
						<select name="paperType" id="formDataType" data-options="editable:false,width:'100%'"></select>
					</div>
				</span>
				<span class="screeningbar-item">
					<span class="label">质检计划</span>
					<div class="valueGroup">
						<select name="planName" id="formplanName" class="easyui-combobox" data-options="width:'100%'"></select>	
					</div>
				</span>
			</div>
			<div class="btn-group">
				<a href="javascript:;" class="operation search-btn" onclick="executioninfo.search()" title="搜索"></a>
				<a href="javascript:;" class="operation reset-btn" onclick="$('#searchexecutioninfo').form('reset');" title="重置"></a>
			</div>
			<div class="screeningbar">
				<span class="screeningbar-item">
					<span class="label">质检员</span>
					<div class="valueGroup">
						<input name="createCode" class="easyui-textbox" id="createCode" data-options="width:'100%'">
					</div>
				</span>
			</div>
		</form>
	</div>
	<div style="display: inline-block;width: 60%;">
		<div class="chart">
			<div class="chart_title clearfix">
				<div class="green_item"></div>
				<span class="item_text">完成情况</span>
				<div class="title_button">
					<button class="easyui-linkbutton l-btn-main" id="urgePaper" onclick="executioninfo.urgePaper()">催一下</button>
				</div>
			</div>
			<div class="echarts1" id="ech1"></div>
		</div>
		<div class="chart">
			<div class="chart_title clearfix">
				<div class="green_item"></div>
				<span class="item_text">时间分布(质检量)</span>
			</div> 
			<div class="echarts1" id="ech2"></div>
		</div>
	</div>
	<div class="work_rank">
		<!-- 
		<div class="tab_item">
			<div class="tab_green">工作排名</div>
			<span class="tab_text">质检量</span>
		</div>
		 -->
		<div class="chart_title clearfix" style="padding: 20px 50px">
			<div class="green_item"></div>
			<span class="item_text" >工作排名</span>
			<span class="rankbyscore">按质检量排名</span>
		</div>
		<div class="rank_info" id="rank_info"></div>
	</div>
</body>
</html>