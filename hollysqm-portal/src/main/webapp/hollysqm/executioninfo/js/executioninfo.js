var url1 = "/rest/executioninfo/executionInfo";
var url2 = "/rest/executioninfo/timeDistribution";
var executioninfo = {

	init:function(){
		var complete = executioninfo.initChartsData(url1);//完成情况
		if(!complete) return false;//防止二次验证
		var timeData = executioninfo.initChartsData(url2);//时间分布
		var chart1 = echarts.init(document.getElementById('ech1'));
		chart1.setOption({
			title: {
				/*text: '某展会类别分布图',
				subtext: 'By sunweizy'*/
			},
			tooltip: {
				trigger: 'axis',
				axisPointer: {
					type: 'shadow'
				},
				formatter: "{b} : {c}"
			},
			legend: {
				//data: ['2016年']
			},
			grid: {
				left: '3%',
				right: '4%',
				bottom: '3%',
				containLabel: true
			},
			xAxis: {
				type: 'value',
				boundaryGap: [0, 0.01],
				axisLabel: {
					"interval": 0,//强制显示所有标签
					/*formatter: '{value}%',*/
				}
			},
			yAxis: {
				type: 'category',
				data: executioninfo.completeYAxis(complete)
			},
			series: [{
			   /* name: '2016年占比',*/
				type: 'bar',
				data: executioninfo.completeXAxis(complete),
				itemStyle: {
					normal: {
						color: '#4bb77d',
						label : {show: true,position: 'right'}
					}
				}
			}]
		});

		var chart2 = echarts.init(document.getElementById('ech2'));
		chart2.setOption({
			 title: {
				/*text: '未来一周气温变化',
				subtext: '纯属虚构'*/
			},
			tooltip: {
				trigger: 'axis'
			},
			legend: {
				/*data:['最高气温','最低气温']*/
			},
		  
			xAxis:  {
				type: 'category',
				boundaryGap: false,
				data: executioninfo.TimeXAxis(timeData)
			},
			yAxis: {
				type: 'value',
				axisLabel: {
					/*formatter: '{value} °C'*/
				}
			},
			series: [
				{
					name:'数量',
					type:'line',
					data:executioninfo.TimeYAxis(timeData),
				    itemStyle: {
						normal: {
							color: '#4bb77d',
							label : {show: true}
						}
					}
			    }
			]
		});
	    	holly.get(holly.getPath()+"/rest/workcount/workRank",holly.form2json($("#searchexecutioninfo")),function(e){
	    		if(e.success){
					var rank_infoHtml = '';
					var rows = e.content;
					for(var i in rows){
						rank_infoHtml += '<div class="score_rank clearfix">';
						rank_infoHtml += '<i class="num no'+(rows[i].rank>3?'4':rows[i].rank)+'">';
						rank_infoHtml +='<span class="rank_num">'+rows[i].rank+'</span>';
						rank_infoHtml += '</i>';
						rank_infoHtml += '<i class="header" '+(rows[i].photo ? 'style="background-image:url('+holly.getResource(rows[i].photo,true)+')"' : '')+'></i>';
						rank_infoHtml += '<span class="CS_name">'+rows[i].userName+'</span>';
						rank_infoHtml += '<span class="CS_score">'+rows[i].total+'</span>';
						rank_infoHtml += '</div>';
					}
					
					$('#rank_info').html(rank_infoHtml);
				}else{
					holly.showError(e.errorMessage);
				}
		});
	},
	search : function(){
		executioninfo.init();
	},
	planNameOption : function(){//加载质检计划下拉数据
		$('#formplanName').combobox({
		    url : holly.getPath() + '/rest/commonRest/queryPlan',
		    valueField : 'value', 
			textField : 'name',
			loadFilter: function(data){
				var jsonArray=new Array();
				for(var o in data.content){
					jsonArray.push(data.content[o]);
				}
				return jsonArray;
			},onHidePanel : function(){
				var el = $(this);
				var value = el.combobox("getValue");
				if(!value)//如果没有匹配到值，则默认设置成空
					 el.combobox("setValue","");
			}
		});  
	},
	initChartsData : function(url){
		var params = holly.form2json($("#searchexecutioninfo"));//获取form表单查询条件
		strReplace("createCode");
		var startTime = params.startTime;
		var endTime = params.endTime;
		
		if(!startTime){
			$.messager.alert('提示','开始时间不能为空!');
			return false;
		}
		if(!endTime){
			$.messager.alert('提示','结束时间不能为空!');
			return false;
		}
		var startTimeLon = new Date(startTime).getTime();
		var lastMonthLon = new Date(endTime).getTime();//executioninfo.getLastMonthDateStr(endTime).getTime();
		
		if(startTime > endTime){
			$.messager.alert('提示','开始时间不能大于结束时间!');
			return false;
		}
		var timeLon = (lastMonthLon - startTimeLon) / (1000*60*60*24);
		if(timeLon > 31){
			$.messager.alert('提示','开始时间和结束时间不能超过31天!');
			return false;
		}
		
		var content = null;
		holly.get(holly.getPath() + url,params,function (e){
			if(e.success)
				content = e.content;
			else{
				holly.showError("错误，原因:"+e.errorMessage);
			}
		},true);
		return content;
	},
	completeYAxis : function(data){//完成情况纵坐标数据
		var a = new Array();
		if(data["status0"] == 0 && data["status2"] == 0 ){//未质检和已申诉统计量都为0时，隐藏催一下按钮
			$("#urgePaper").hide();
		}else{
			$("#urgePaper").show();
		}
		for(var o in data){
			var status = statusMap[o.replace("status","")];
			if(status)
			   a.push(status);
		}
		return a;
	},
	completeXAxis : function(data){//完成情况横坐标数据
		var a = new Array();
		for(var o in data){
			if( o != "total")
			 a.push(data[o]);
		}
		return a;
	},
	TimeXAxis : function(data){//时间分布横坐标数据
		var a = new Array();
		for(var o in data){
			a.push(data[o].abscissa);
		}
		return a;
	},
	TimeYAxis : function(data){//时间分布横坐标数据
		var a = new Array();
		for(var o in data){
			a.push(data[o].ordinate);
		}
		return a;
	},
	getdictnames : function(param){
		var res=null;
		holly.get(holly.getPath()+"/rest/dictionary/getDictionaryNamesRest",param,function(e){
			if(e.success){//先确认检查通过！
				res = e.content;
			}else{
				holly.showError("错误，原因:"+e.errorMessage);
			}
		},true);
		return res;
	},urgePaper : function(){//催一下
		holly.post(holly.getPath()+"/rest/executioninfo/urgePaper",holly.form2json($("#searchexecutioninfo")),function(e){
    		if(e.success){
				with(e.content){
					holly.showSuccess("一共发送通知请求"+total+"单，成功发送"+send+"单，已过滤"+resend+"单");
				}
			}else{
				holly.showError(e.errorMessage);
			}
		});
	}
};
var statusMap = executioninfo.getdictnames({'codeType':'QUALITY_STATUS'});

/**
*	过滤特殊字符
*	str：要进行过滤的字符串
*/
function strReplace(id){
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
	var rs = "";
	var str = $("#"+id).textbox("getValue");
	if($.trim(str).length > 0 ){
		for (var i = 0; i < str.length; i++) {
			rs = rs+str.substr(i, 1).replace(pattern, '');
		}
		$("#"+id).textbox("setValue",rs);
	}
}

