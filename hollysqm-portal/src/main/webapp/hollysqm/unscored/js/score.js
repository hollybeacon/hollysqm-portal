var score = {
    /**
     * 初始化加载数据
     */
    paramgetUnscoredPaperMsg: {
        "dataType": holly.getUrlParams("dataType") ? holly.getUrlParams("dataType") : "",
        "agent": holly.getUrlParams("agent") ? holly.getUrlParams("agent") : "",
        "checker": holly.getUrlParams("checker") ? holly.getUrlParams("checker") : "",
        "endTime": holly.getUrlParams("endTime") ? holly.getUrlParams("endTime") : "",
        "planId": holly.getUrlParams("planId") ? holly.getUrlParams("planId") : "",
        "startTime": holly.getUrlParams("startTime") ? holly.getUrlParams("startTime") : "",
        "paperId": holly.getUrlParams("paperId") ? holly.getUrlParams("paperId") : ""
    },
    contactId: holly.getUrlParams("contactId") ? holly.getUrlParams("contactId") : "",
    frameId: holly.getUrlParams("frameId") ? holly.getUrlParams("frameId") : "",
    totalScoreReal: 0,
    thisIframeId: window.frameElement && window.frameElement.id || '',//holly.getUrlParams("iframescore") ? holly.getUrlParams("iframescore") : "0",// 获取该页面iframe id
    init: function () {
        $("#caseItem").combobox("init", {'codeType': 'CASE_ITEM'});

        var param = {
            "dataType": holly.getUrlParams("dataType") ? holly.getUrlParams("dataType") : "",
            "paperId": holly.getUrlParams("paperId") ? holly.getUrlParams("paperId") : ""
        };

        // if (score.thisIframeId == 'iframescore') {//如果该页面从待评分列表打开
        // if (score.thisIframeId == '1') {//如果该页面从待评分列表打开
            holly.post(holly.getPath() + "/rest/unscoredRest/getUnscoredPaperMsg", score.paramgetUnscoredPaperMsg, function (e) {
                if (e.success && e.content != null) {
                    if (e.content.paperCount == 1) {
                        $('#paperCountText').html('已是最后一条');
                    } else {
                        $('#paperCount').html('共 ' + e.content.paperCount + ' 条');
                        $('#paperCountText').html('提交后自动显示下一质检单');
                        score.nextPaperId = e.content.nextPaperId;
                    }
                }
            });
        // }

        holly.post(holly.getPath() + "/rest/commonRest/getRecord", param, function (e) {
            if (e.success) {
                var content = e.content;
                var html = '';
                var chatHtml = '';
                var arrFlag = [];
                var arrText = [];
                score.contactId = content.contactId;
                score.acceptTime = content.acceptTime;
                score.recordFile = content.recordFile;
                score.planId = content.planId;
                score.dataType = content.dataType;
                //显示接触记录绑定的质检计划信息
                if (content.planId) {
                    if (content.planId == 'system') {
                        $('#planDetail').html('所属质检计划:' + content.planName);
                    } else {
                        $('#planDetail').html('<a class="sublink" href="javascript:;" onclick="score.planinfo()">所属质检计划:' + content.planName + '</a>');
                    }
                }
                if (score.paramgetUnscoredPaperMsg.dataType == 'v8') {
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">来电号码</div>';
                    if (content.caller != null) {
                        html += '<div class="icontrols">' + content.caller + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">坐席</div>';
                    if (content.user != null) {
                        html += '<div class="icontrols">' + content.user.username + '(' + content.user.agentCode + ')' + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">归属地</div>';
                    if (content.custArea != null) {
                        html += '<div class="icontrols">' + score.paramJson(content.custArea, 'CUST_AREA') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">品牌</div>';
                    if (content.custBand != null) {
                        html += '<div class="icontrols">' + score.paramJson(content.custBand, 'CUST_BAND') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">满意度</div>';
                    if (content.satisfaction != null) {
                        html += '<div class="icontrols">' + score.paramJson(content.satisfaction, 'V8SATISFACTION') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">业务类型</div>';
                    if (content.serviceType != null) {
                        html += '<div class="icontrols">' + score.paramJson(content.bussinessType, 'BUSINESS_TYPE') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">开始时间</div>';
                    if (content.startTime != null) {
                        html += '<div class="icontrols">' + content.startTime + '</div>';
                    }
                    html += '</div>';

                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">通话时长</div>';
                    if (content.length != null) {
                        html += '<div class="icontrols">' + content.length + '秒</div>';
                    }
                    html += '</div>';

                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">用户级别</div>';
                    if (content.custLevel != null) {
                        html += '<div class="icontrols">' + score.paramJson(content.custLevel, 'CUST_LEVEL') + '</div>';
                    }
                    html += '</div>';

                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">静音时长</div>';
                    if (content.custLevel != null) {
                        html += '<div class="icontrols">' + content.silenceLength + '秒</div>';
                    }
                    html += '</div>';

                    $('#voicechatDiv').show();
                    // 接触记录图片音频地址查询
                    holly.post(holly.getPath() + "/rest/commonRest/getRecordDir",
                        {
                            "contactId": score.contactId,
                            "acceptTime": score.acceptTime,
                            "recordFile": score.recordFile
                        }, function (e) {
                            if (e.success) {
                                listenTape.init(e.content.wavDir, e.content.imgDir);
                            } else {
                                toastr.error(e.errorMessage);
                            }
                        });
                } else if (score.paramgetUnscoredPaperMsg.dataType == 'i8') {
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">来话号码</div>';
                    if (content.caller != null) {
                        html += '<div class="icontrols">' + content.caller + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">坐席</div>';
                    if (content.user != null) {
                        html += '<div class="icontrols">' + content.user.username + '(' + content.user.agentCode + ')' + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">关闭方式</div>';
                    if (content.closeType != null) {
                        html += '<div class="icontrols">' + score.paramJson(content.closeType, 'CLOSE_TYPE') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">会话等级</div>';
                    if (content.sessionType != null) {
                        html += '<div class="icontrols">' + score.paramJson(content.sessionType, 'SESSION_TYPE') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">满意度</div>';
                    if (content.satisfaction != null) {
                        html += '<div class="icontrols">' + score.paramJson(content.satisfaction, 'I8SATISFACTION') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">服务类型</div>';
                    if (content.serviceType != null) {
                        html += '<div class="icontrols">' + content.serviceType + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">开始时间</div>';
                    if (content.startTime != null) {
                        html += '<div class="icontrols">' + content.startTime + '</div>';
                    }
                    html += '</div>';

                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">会话时长</div>';
                    if (content.length != null) {
                        html += '<div class="icontrols">' + content.length + '秒</div>';
                    }
                    html += '</div>';
                }
                $('#getRecordDiv').html(html);

                arrFlag = content.txtContent.match(/n\d#/ig);
                arrText = content.txtContent.split(/n\d#/ig);
                for (var i = 0; i < arrFlag.length; i++) {
                    if (arrFlag[i] == 'n0#') {
                        chatHtml += '<div class="text worker"><span style="text-align:right;">' + (i + 1) + '&nbsp;&nbsp;</span>坐席：' + (arrText[i + 1]) + '</div>';
                    } else if (arrFlag[i] == 'n1#') {
                        chatHtml += '<div class="text visiter"><span style="text-align:right;">' + (i + 1) + '&nbsp;&nbsp;</span>客户：' + (arrText[i + 1]) + '</div>';
                    }
                }
                $('#chatqa').html(chatHtml);

            } else {
                toastr.error(e.errorMessage);
            }
        });

        holly.post(holly.getPath() + "/rest/commonRest/getItemScoreList", {
            "orderNum": "0",
            "paperId": score.paramgetUnscoredPaperMsg.paperId
        }, function (e) {
            if (e.success) {
                var content = e.content;
                var html = "";
                for (var i = 0; i < content.length; i++) {
                    html += '<li><label title="' + content[i].scoreItemName + '"><input type="checkbox" onchange="score.scoreItemCheck(this,' + (content[i].scoreType == "0" ? "+" : "-") + content[i].score + ')" value="' + content[i].scoreItemId + '" class="chk" name="scoreItems"/><span class="text">' + content[i].scoreItemName + '</span><span class="point">' + (content[i].scoreType == "0" ? "+" : "-") + content[i].score + '</span></label></li>';
                }
                $('#scoreitemlist').html(html);
            } else {
                toastr.error(e.errorMessage);
            }
        });
    },
    paramJson: function (param, getdictname) {
        if (param) {
            var map = $("#planlistdatagrid").datagrid("getdictnames", {'param': {'codeType': getdictname}});
            var text = '';
            $.each(param.split(","), function (index, item) {
                text += (index == 0 ? '' : ',') + (map[item] ? map[item] : '');
            });
            return text;
        }
    },
    scoreItemCheck: function (obj, point) {
        var val = 100;
        if (obj.checked) {
            score.totalScoreReal += point;
        } else {
            score.totalScoreReal -= point;
        }
        val += score.totalScoreReal;
        if (val > 100) {
            val = 100;
        } else if (val < 0) {
            val = 0;
        }
        $('#totalScore').val(val);
        $('#totalScoreDiv').html(val + '分');
    },
    saveScore: function () {
        var scoreItem = [];
        $('input[name="scoreItems"]:checked').each(function () {
            scoreItem.push($(this).val());
        });
        var caseItem = $("#caseItem").combobox("getValue");
        var recordType = $('input[name="recordType"]:checked').val();
        if ((recordType == '1') && (caseItem == "")) {
            holly.showError('请选择质检案例');
            return false;
        }

        $("#itemId").val(scoreItem);
        $("#contactId").val(score.contactId);

        $.messager.confirm('提醒', '您正在提交质检单，评分为' + $('#totalScore').val() + '分,请确认评分和问题描述是否已输入完整?', function (r) {
            if (r) {
                score.formSubmit();
            }
        });


    },
    formSubmit: function () {
        var vaild = $("#scoreForm").form('enableValidation').form('validate');
        if (vaild) {
            $.messager.progress({
                text: '正在提交...'
            });
            $('#scoreForm').form("submit", {
                url: holly.getPath() + "/rest/commonRest/scorePaper?paperId=" + score.paramgetUnscoredPaperMsg.paperId,
                success: function (e) {
                    var d = ($.type(e) == "string") ? $.parseJSON(e) : e;
                    if (d.success) {
                        holly.showSuccess('评分成功');

                        // var param = score.paramgetUnscoredPaperMsg;
                        // var url = holly.getPath() + '/hollysqm/unscored/page/score.jsp';
                        // url += '?dataType=' + param.dataType;
                        // url += '&agent=' + param.agent;
                        // url += '&checker=' + param.checker;
                        // url += '&endTime=' + param.endTime;
                        // url += '&planId=' + param.planId;
                        // url += '&startTime=' + param.startTime;
                        // url += '&paperId=' + score.nextPaperId;
                        // url += '&contactId=' + score.contactId;
                        // url += '&frameId=' + score.frameId;

                        // 如果待评分列表页面没有关闭，并且该页面是从待评分列表打开的
                        // if((parent.$("#"+score.frameId).length>0)){
                        // 	var $obj = parent.$("#"+score.frameId)[0].contentWindow;
                        // 	$obj.search();//重新搜索列表
                        // }
                        // if(score.thisIframeId=='iframescore'){
                        // 	if((score.nextPaperId) && (score.nextPaperId!="0")){
                        // 		window.location.href=url;
                        // 	}else{
                        // 		alert('已完成最后一条评分，待评分页面将会关闭');
                        // 		var $tabs = parent.$('#tabs');
                        // 		var curTab = $tabs.tabs('getSelected');
                        // 		var tabIndex = $tabs.tabs('getTabIndex',curTab);
                        // 		$tabs.tabs('close',tabIndex);
                        // 	}
                        // }else{
                        // 	var $tabs = parent.$('#tabs');
                        // 	var curTab = $tabs.tabs('getSelected');
                        // 	var tabIndex = $tabs.tabs('getTabIndex',curTab);
                        // 	$tabs.tabs('close',tabIndex);
                        // }

                        if(score.nextPaperId) {
                            //存在有下一条数据
                            var param = score.paramgetUnscoredPaperMsg;
                            var url = holly.getPath() + '/hollysqm/unscored/page/score.jsp';
                            url += '?dataType=' + param.dataType;
                            url += '&agent=' + param.agent;
                            url += '&checker=' + param.checker;
                            url += '&endTime=' + param.endTime;
                            url += '&planId=' + param.planId;
                            url += '&startTime=' + param.startTime;
                            url += '&paperId=' + score.nextPaperId;
                            url += '&contactId=' + score.contactId;
                            url += '&frameId=' + score.frameId;
                            window.location = url;
                        } else {
                            //重新跳转待评分页面
                            var url = holly.getPath() + "/hollysqm/unscored/page/unscored.jsp?frameId=" + score.frameId;
                            window.location = url;
                        }
                    } else {
                        holly.showError('提交失败');
                    }
                    $.messager.progress('close');
                }
            });
        }
    },

    planinfo: function () {
        var url = holly.getPath() + "/hollysqm/planmanager/page/dlg-planinfo.jsp";
        url += "?planId=" + score.planId;
        url += "&dataType=" + score.dataType;
        $('#dlg-planinfo').dialog('open').dialog('refresh', url);
    },
    recordTypeEvent: function (val) {
        if (val == "1") {
            $('#caseItemRequired').show();
            $('#caseItem').combobox('enable');
        } else if (val == "2") {
            $('#caseItemRequired').hide();
            $('#caseItem').combobox('setValue', '').combobox('disable');
        }
    }

};


var listenTape = {
    init: function (voicePath, imgPath) {
        var player,//播放器
            intever = 0;//定时器
        player = new _mu.Player({
            baseDir: holly.getPath() + '/hollysqm/common/muplayer/dist'
        });//初始播放器
        //setSong(contextPath+path);//设置播放录音
        //音量调节
        $(document).on('sliderchange', "#voice", function (e, result) {
            player.setVolume(result.value);
        });
        $('.myautoplayer-play').on('click', function () {
            if ($(this).hasClass('icon-play')) {
                player.play();//播放事件
                $(this).removeClass("icon-play").addClass("icon-pause");
                intever = scrollImg();
            } else if ($(this).hasClass('icon-pause')) {
                player.pause();//暂停
                $(this).removeClass("icon-pause").addClass("icon-play");
                clearInterval(intever);//清除事件
            }
        });
        //图片点击播放事件
        $("#divScroll").click(function (e) {
            var scrollLeft = $("#divScroll").scrollLeft();
            var offset = $(this).offset();
            var relativeX = (e.pageX - offset.left) + scrollLeft;
            var len = parseInt(relativeX / $("#imgVoice").width() * player.duration() * 1000);
            player.play(len);
            intever = scrollImg();
            $(".icon-play").addClass("icon-pause").removeClass("icon-play");
        });
        //滚动图片
        function scrollImg() {
            return setInterval(function () {//改变滑动块
                var l = player.curPos() / player.duration();
                var left = $("#imgVoice").width();//图片宽度
                if (l > (1 - 900 / left)) {
                    clearInterval(intever);//清除事件
                    $(".icon-pause").addClass("icon-play").removeClass("icon-pause");
                    return;
                }
                $("#divScroll").animate({scrollLeft: left * l}, 1000);
            }, 1000);
        }

        //滚动条拖动事件
        $("#divScroll").mousedown(function () {
            var state = player.getState();
            if (state == 'playing') {
                player.pause();//暂停
                $(".icon-pause").addClass("icon-play").removeClass("icon-pause");
                clearInterval(intever);//清除事件
            }
        });
        player.setUrl(holly.getPath() + "/" + voicePath);//添加录音文件
        $("#imgVoice").attr("src", holly.getPath() + "/" + imgPath).removeClass('voiceloading').addClass('voicechatimg');
    }
};