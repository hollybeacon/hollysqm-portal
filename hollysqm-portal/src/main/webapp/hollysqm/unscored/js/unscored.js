var unScored = {
	/**
	 * 初始化加载数据  
	 */
	init:function(){

		$("#formDataType").combobox("init_extend",{'codeType':'DATATYPE','hasAllOption':false,"event":{
			onLoadSuccess:function(){
				unScored.dataTypeData = $(this).combobox('getData');
				$(this).combobox('setValue',unScored.dataTypeData[0].value);
			}
		}});
		
		$("#formPlanId").combobox({
			url:holly.getPath()+'/rest/commonRest/queryPlan',
			valueField:'value',
			textField:'name',
			method:"post",
			loadFilter:function(data){
				var jsonArray=new Array();
				//var defaultVal = {"value":"","name":'全部'};
				//jsonArray.push(defaultVal);
				for(var o in data.content){
					jsonArray.push(data.content[o]);
				}
				return jsonArray;
			},onHidePanel : function(){
				var el = $(this);
				var value = el.combobox("getValue");
				if(!value)//如果没有匹配到值，则默认设置成空
					 el.combobox("setValue","");
			}
		});
		$("#unScoreddatagrid").datagrid({
			fit:true,
			url:holly.getPath()+'/rest/unscoredRest/getUnscoredPaperList',
            method:'get',
            singleSelect : true,
            border:false,
            fitColumns:true,
			queryParams:{'dataType':'v8'},
			pagination: true,
			loadFilter: function(data){
				return (data.content)?data.content:data;
			},
			columns:[[
				{field:'dataType',title:'质检来源',width:60,formatter:unScored.formatDataTyp},
				{field:'planName',title:'质检计划',width:100,formatter:unScored.formatPlanName},
				{field:'caller',title:'来电号码',width:80},
				{field:'startTime',title:'来电时间',width:80},
				{field:'agentName',title:'坐席',width:80},
				{field:'createTime',title:'分配时间',width:80},
				{field:'option',title:'操作',width:60,formatter:unScored.formatOption}
			]],
			loadMsg:'数据加载中请稍后……',
			onLoadSuccess: function (data)  {
				if (data.rows.length == 0) {
					$("#exportTable").hide();//没有数据时隐藏导出按钮
					var body = $(this).data().datagrid.dc.body2;
					body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
				}else $("#exportTable").show();
			}
		});
		$("#unScoreddatagrid_i8").datagrid({
			fit:true,
			method:'get',
			singleSelect : true,
			border:false,
			fitColumns:true,
			pagination: true,
			closed:true,
			loadFilter: function(data){return (data.content)?data.content:data;},
			columns:[[
				{field:'dataType',title:'质检来源',width:60,formatter:unScored.formatDataTyp},
				{field:'planName',title:'质检计划',width:100,formatter:unScored.formatPlanName},
				{field:'caller',title:'来电号码',width:80},
				{field:'startTime',title:'来电时间',width:80},
				{field:'agentName',title:'坐席',width:80},
				{field:'createTime',title:'分配时间',width:80},
				{field:'closeType',title:'关闭方式',width:80,formatter:unScored.formatCloseType},
				{field:'sessionType',title:'会话等级',width:80,formatter:unScored.formatSessionType},
				{field:'option',title:'操作',width:60,formatter:unScored.formatOption}
			]],
			loadMsg:'数据加载中请稍后……',
			onLoadSuccess: function (data)  {
				if (data.rows.length == 0) {
					$("#exportTable").hide();//没有数据时隐藏导出按钮
					var body = $(this).data().datagrid.dc.body2;
					body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
				}else $("#exportTable").show();
			}
		});
	},

	
	/**
	 * 搜索
	 */
	search:function(){
		var param = holly.form2json($("#searchunScored"));
		if(param.dataType=='v8'){
			if($("#unScoreddatagrid").datagrid('getPanel').panel('options').closed){
				$("#unScoreddatagrid_i8").datagrid('getPanel').panel('close');
				$("#unScoreddatagrid").datagrid('getPanel').panel('open').panel('resize');
			}
			$("#unScoreddatagrid").datagrid("search",{'param':param});
		}else if(param.dataType=='i8'){
			$("#unScoreddatagrid_i8").datagrid('options').url = holly.getPath()+'/rest/unscoredRest/getUnscoredPaperList';
			if($("#unScoreddatagrid_i8").datagrid('getPanel').panel('options').closed){
				$("#unScoreddatagrid").datagrid('getPanel').panel('close');
				$("#unScoreddatagrid_i8").datagrid('getPanel').panel('open').panel('resize');
			}
			$("#unScoreddatagrid_i8").datagrid("search",{'param':param});
		}
	},
	formatPlanName : function(val,row,index){//质检计划名称
        if(val == undefined || val =='' || val == null || val=='null') {
            val = '全文检索';
        }
		return '<span title='+val+'>'+val+'</span>';
	},
	// 质检来源
	formatDataTyp:function(val, row, index) {
		var res = dataTypeMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	},
	formatCloseType:function(val, row, index){
		var res = closeTypeMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	},
	formatSessionType:function(val, row, index){
		var res = sessionTypeMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	},
 
	// 操作
	formatOption:function(val,row,index){
		var	content	= "<a href='javascript:void(0)' onclick=\"unScored.jumpUrl('"+row.paperId+"','"+row.paperDetailId+"')\" title='评分'>评分</a>";
		return content;
	}, 
	// 坐席
	formatModifier:function(val,row,index){
		//return val?"<span title='"+row.agentName + "(" +val+ ")"+"'>"+row.agentCode + "(" + val+ ")"+"</span>":"";
		if(row.agentName == null || row.userCode == null) {
			return "";
		} else {
			return "<span>" + row.agentName + "(" + row.userCode + ")</span>";
		}
		
	},
	jumpUrl:function(paperId,paperDetailId){
		var param = holly.form2json($("#searchunScored"));
		var frameId = window.frameElement && window.frameElement.id || '';
		var url = holly.getPath()+'/hollysqm/unscored/page/score.jsp';
		url += '?dataType='+param.dataType;
		url += '&agent='+param.agent;
		url += '&checker='+param.checker;
		url += '&endTime='+param.endTime;
		url += '&planId='+param.planId;
		url += '&startTime='+param.startTime;
		url += '&paperId='+paperId;
		url += '&contactId='+paperDetailId;
		url += '&frameId=' + frameId;
		// parent.main.addTab('iframescore',url,'评分');
		window.location=url;
	},
	exportPaper : function(toPage){
		var formDataType = $("#formDataType").combobox("getValue");
		var grid = $('#unScoreddatagrid');  
		if(formDataType == "i8")
			grid = $('#unScoreddatagrid_i8');  
		var options = grid.datagrid('getPager').data("pagination").options;  
		var total = options.total;
		if(total > export_total){
			$.messager.alert("提示","导出数据不能大于"+export_total+"，请继续筛选数据！");
			return false;
		}
		if(toPage){
			var url =holly.getPath()+"/rest/csv/wizard/sqlexport?tableId=csv.score.queryUnScore_"+formDataType+"&sqlKey=velocity.score.queryUnScore&mediaId=1";
			var targetIframe ="iframe-export";
			$('#searchunScored').attr("action",url);
			$('#searchunScored').attr("target",targetIframe);
			$('#searchunScored').submit();
			$('#dlg-export').dialog('open');
		}else{
			$("#iframe-export")[0].contentWindow.exportData();
			$('#dlg-export').dialog('close');
		}
	},
	resetForm : function(){
		$('#searchunScored').form('reset');
		$('#formDataType').combobox('select',unScored.dataTypeData[0].value);//默认选中第一个
	}

};
  