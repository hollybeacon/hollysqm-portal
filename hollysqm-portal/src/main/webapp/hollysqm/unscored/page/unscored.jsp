<%@page import="com.hollycrm.hollybeacon.basic.util.AppConfigUtils" %>
<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp"
         pageEncoding="UTF-8" %>
<%@ include file="/hollybeacon/common/taglibs.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>待评分</title>
    <%@ include file="/hollybeacon/common/meta.jsp" %>
    <script type="text/javascript" src="../js/unscored.js"></script>
    <script type="text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
    <%
        Integer export_total = AppConfigUtils.getConfig().getInteger("system.export.total", 100000);
    %>
    <script type="text/javascript">
        var loginUser = ${loginUser};
        var export_total = <%=export_total%>;
        // 获取必要的数据字典MAP
        var dataTypeMap = $("#unScoreddatagrid").datagrid("getdictnames", {'param': {'codeType': 'DATATYPE'}});
        var closeTypeMap = $("#unScoreddatagrid").datagrid("getdictnames", {'param': {'codeType': 'CLOSE_TYPE'}});
        var sessionTypeMap = $("#unScoreddatagrid").datagrid("getdictnames", {'param': {'codeType': 'SESSION_TYPE'}});
        $(function () {
            $("#checkerCode").val(loginUser.userCode);
            unScored.init();
            $("#searchunScored").keyup(function (event) {
                if (event.keyCode == 13) {
                    unScored.search();
                }
            });
        });
        function search() {
            unScored.search();
        }
    </script>
</head>
<body class="easyui-layout">
<div data-options="region:'center',border:false" style="padding:10px;">
    <div class="easyui-layout" data-options="fit:true">
        <!-- 搜索区域 -->
        <div data-options="region:'north',border:false" style="padding-bottom:10px;">
            <form id="searchunScored" method="post" class="search-form">
                <input type="hidden" name="checkerCode" id="checkerCode"/>
                <div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">质检来源</span>
							<div class="valueGroup">
								<select name="dataType" id="formDataType"
                                        data-options="editable:false,width:'100%'"></select>
							</div>
						</span>
                    <span class="screeningbar-item">
							<span class="label">质检计划</span>
							<div class="valueGroup">
									<select name="planId" id="formPlanId" data-options="width:'100%'"></select>
							</div>
						</span>
                    <span class="screeningbar-item">
							<span class="label">分配时间</span>
							<div class="valueGroup">
								<input name="startTime" type="text" class="easyui-datebox"
                                       data-options="editable:false,width:'100%'">
							</div>
						</span>
                    <span class="screeningbar-item">
							<span class="label">至</span>
							<div class="valueGroup">
								<input name="endTime" type="text" class="easyui-datebox"
                                       data-options="editable:false,width:'100%'">
							</div>
						</span>
                    <span class="screeningbar-item">
							<span class="label">坐席</span>
							<div class="valueGroup">
								<input name="agent" type="text" class="easyui-textbox"
                                       data-options="editable:true,width:'100%'">
							</div>
						</span>
                    <!-- -
                    <span class="screeningbar-item">
                        <span class="label">质检员</span>
                        <div class="valueGroup">
                            <input name="checker" type="text" class="easyui-textbox" data-options="editable:true,width:'100%'">
                        </div>
                    </span>
                     -->
                </div>
                <div class="btn-group">
                    <a href="javascript:;" class="operation search-btn" onclick="unScored.search()" title="搜索"></a>
                    <a href="javascript:;" class="operation reset-btn" onclick="unScored.resetForm()" title="重置"></a>
                </div>
            </form>
        </div>
        <!-- 搜索区域 end -->
        <div data-options="region:'center',border:false">
            <div id="tt">
                <a href="javascript:void(0);" class="easyui-linkbutton l-btn-main" id="exportTable"
                   onclick="unScored.exportPaper(true)">导出</a>
            </div>
            <div class="easyui-panel" title="待评分" data-options="tools:'#tt',fit:true">
                <table id="unScoreddatagrid"></table>
                <table id="unScoreddatagrid_i8"></table>
            </div>
        </div>
    </div>
</div>

<div id="dlg-export" class="easyui-dialog" title="导出待评分"
     data-options="
			closed:true,
			buttons: '#dlg-export-buttons',
			width:650,
			height:'95%'
		">
    <iframe id="iframe-export" name="iframe-export" src="" frameborder="0"
            style="width:100%;height:100%;display:block;"></iframe>
</div>
<div id="dlg-export-buttons">
    <a href="javascript:void(0)" class="easyui-linkbutton l-btn-red" onclick="unScored.exportPaper(false)">确定</a>
    <a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#dlg-export').dialog('close')">取消</a>
</div>
</body>
</html>