var checkscore = {
	/**
	 * 初始化加载数据  
	 */
	param : {
		"paperId":holly.getUrlParams("paperId")?holly.getUrlParams("paperId"):"",
		"frameId":holly.getUrlParams("frameId")?holly.getUrlParams("frameId"):""
	},
	init:function(){
		$("#caseItem").combobox("init",{'codeType':'CASE_ITEM'});

		// 质检单评分查询
		holly.post(holly.getPath()+"/rest/commonRest/getPaperScoreResult",{"paperId":checkscore.param.paperId},function(e){
			if(e.success){
				if(e.content){
					checkscore.qualityStatus = e.content.qualityStatus;
					if(e.content.qualityStatus <= 2){
						//评分阶段只显示评分div，其他所有div隐藏
						$('#totalScoreDiv').html(e.content.totalScore + '分');
						$('#checkerInfoDiv').html(e.content.scorerName+'('+e.content.scorerCode+')');
						$('#scoreTimeDiv').html(e.content.scoreTime);
						$('#commentsDiv').html(holly.replace_str(e.content.comments,false));
						
						
					} else {
						$('#reviewContainer').show();
						$('#caseItemTip').hide();
						$('#recordTypeTip').hide();
						$('#commonsTip').hide();
						$('#totalScoreDiv').html(e.content.totalScore + '分');
						$('#checkerInfoDiv').html(e.content.scorerName+'('+e.content.scorerCode+')');
						$('#scoreTimeDiv').html(e.content.scoreTime);
						$('#reviewScoreDiv').html(e.content.reviewScore);
						$('#checkerInfoDiv2').html(e.content.checkerName+'('+e.content.checkerCode+')');
						$('#reviewTimeDiv').html(e.content.reviewTime);
						$('#commentsDiv2').html(holly.replace_str(e.content.comments,false));
						
							
					}
					
					if(e.content.qualityStatus > 2) {
						$('#reviewContainer').show();
						
					}
					
					if(e.content.qualityStatus >= 2) {
						$('#appealContainer').show();
					}
					
					if(e.content.qualityStatus == 4) {
						$('#scoreitemlistTip2').hide();
						$('#reviewScoreTip').hide();
					}
					
					// 查询申诉信息
					holly.get(holly.getPath()+"/rest/commonRest/queryAppealPaper",{"paperId":checkscore.param.paperId},function(e){
						if(e.success){
							if(e.content){
								var html = "";
								var length = e.content.length;
								$.each(e.content,function(index,val){
									with(val){
										html +="<div class='icontrol-group' id='icontrol-group"+(index+1)+"'>"+
													"<div class='icontrol-label' style='width:90px;'>"+checkscore.getSysUser(executeCode)+"("+executeCode+")"+"</div>"+
													"<div class='icontrols icontrols-text'>&nbsp;&nbsp;"+(explain == null ?"":explain)+"【"+appealStatusMap[status]+"】"+"<div style='text-align: right'>"+(executeime == null?"":executeime)+"</div>"+"</div>"+
												"</div>";
									}
								});
								$("#appealPaperDiv").html(html);
								if(length == 0)
									$("#appealPaperDiv").parent().parent().parent().hide();
								else{
									$("#appealPaperDiv").parent().parent().parent().show();
									
								}
							}
						}
					});
					
					// 获取当前质检单绑定的典型案例
					holly.post(holly.getPath()+"/rest/commonRest/getPaperCase",{"paperId":checkscore.param.paperId},function(e){
						if(e.success){
							if(e.content){
								if(checkscore.qualityStatus <= 2) {
									if(e.content.caseItem){
										$('#caseItemDiv').html(caseItemMap[e.content.caseItem]);
										if(e.content.recordType=="1"){
											$('#recordTypeDiv').html("是");
											$('#caseItemTip').show();
										}else if(e.content.recordType=="2"){
											$('#recordTypeDiv').html("否");
											$('#caseItemTip').hide();
										}
									}else{
										$('#recordTypeDiv').html("否");
										$('#caseItemTip').hide();
									}
								} else {
									if(e.content.caseItem){
										$('#caseItemDiv2').html(caseItemMap[e.content.caseItem]);
										if(e.content.recordType=="1"){
											$('#recordTypeDiv2').html("是");
											$('#caseItemTip2').show();
										}else if(e.content.recordType=="2"){
											$('#recordTypeDiv2').html("否");
											$('#caseItemTip2').hide();
										}
									}else{
										$('#recordTypeDiv2').html("否");
										$('#caseItemTip2').hide();
									}
								}
								
							}
						}
					});
					
					
				}
			}
		});
		
		

		holly.post(holly.getPath()+"/rest/commonRest/getRecord",checkscore.param,function(e){
			if(e.success){
				var content = e.content;
				var html = '';
				var chatHtml = '';
				var arrFlag = [];
				var arrText = [];
				checkscore.contactId = content.contactId;
				checkscore.acceptTime = content.acceptTime;
				checkscore.recordFile = content.recordFile;
				if(e.content.dataType=='v8'){
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">来电号码</div>';
					if(content.caller != null) {
						html += '<div class="icontrols">'+content.caller+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">坐席</div>';
					if(content.user != null){
						html += '<div class="icontrols">'+content.user.username+'('+content.user.agentCode+')'+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">归属地</div>';
					if(content.custArea != null) {
						html += '<div class="icontrols">'+checkscore.paramJson(content.custArea,'CUST_AREA')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">品牌</div>';
					if(content.custBand != null) {
						html += '<div class="icontrols">'+checkscore.paramJson(content.custBand,'CUST_BAND')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">满意度</div>';
					if(content.satisfaction != null) {
						html += '<div class="icontrols">'+checkscore.paramJson(content.satisfaction,'V8SATISFACTION')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">业务类型</div>';
					if(content.serviceType != null) {
						html += '<div class="icontrols">'+checkscore.paramJson(content.bussinessType,'BUSINESS_TYPE')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">开始时间</div>';
					if(content.startTime != null) {
						html += '<div class="icontrols">'+content.startTime+'</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">通话时长</div>';
					if(content.length != null) {
						html += '<div class="icontrols">'+content.length+'秒</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">用户级别</div>';
					if(content.custLevel != null) {
						html += '<div class="icontrols">'+checkscore.paramJson(content.custLevel,'CUST_LEVEL')+'</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">静音时长</div>';
					if(content.custLevel != null) {
						html += '<div class="icontrols">'+ content.silenceLength +'秒</div>';
					}
					html += '</div>';
					
					$('#voicechatDiv').show();
					// 接触记录图片音频地址查询
					holly.post(holly.getPath()+"/rest/commonRest/getRecordDir",{"contactId":checkscore.contactId, "acceptTime":checkscore.acceptTime, "recordFile":checkscore.recordFile},function(e){
						if(e.success){
							listenTape.init(e.content.wavDir,e.content.imgDir);
						}else{
							toastr.error(e.errorMessage);
						}
					});
				}else if(e.content.dataType=='i8'){
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">来话号码</div>';
					if(content.caller != null) {
						html += '<div class="icontrols">'+content.caller+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">坐席</div>';
					if(content.user != null){
						html += '<div class="icontrols">'+content.user.username+'('+content.user.agentCode+')'+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">关闭方式</div>';
					if(content.closeType != null) {
						html += '<div class="icontrols">'+checkscore.paramJson(content.closeType,'CLOSE_TYPE')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">会话等级</div>';
					if(content.sessionType != null) {
						html += '<div class="icontrols">'+checkscore.paramJson(content.sessionType,'SESSION_TYPE')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">满意度</div>';
					if(content.satisfaction != null) {
						html += '<div class="icontrols">'+checkscore.paramJson(content.satisfaction,'I8SATISFACTION')+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">服务类型</div>';
					if(content.serviceType != null) {
						html += '<div class="icontrols">'+content.serviceType+'</div>';
					}
					html += '</div>';
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">开始时间</div>';
					if(content.startTime != null) {
						html += '<div class="icontrols">'+content.startTime+'</div>';
					}
					html += '</div>';
					
					html += '<div class="icontrol-group">';
					html += '<div class="icontrol-label">会话时长</div>';
					if(content.length != null) {
						html += '<div class="icontrols">'+content.length+'秒</div>';
					}
					html += '</div>';					
				}
				$('#getRecordDiv').html(html);

				arrFlag= content.txtContent.match(/n\d#/ig);
				arrText= content.txtContent.split(/n\d#/ig);
				for(var i=0; i<arrFlag.length; i++){
					if (arrFlag[i]=='n0#') {
						chatHtml += '<div class="text worker"><span style="text-align:right;">'+(i+1)+'&nbsp;&nbsp;</span>坐席：'+(arrText[i+1])+'</div>';
					}else if(arrFlag[i]=='n1#'){
						chatHtml += '<div class="text visiter"><span style="text-align:right;">'+(i+1)+'&nbsp;&nbsp;</span>客户：'+(arrText[i+1])+'</div>';
					}
				}
				$('#chatqa').html(chatHtml);
			}else{
				toastr.error(e.errorMessage);
			}
		});

		checkscore.getItemScoreList("1","#scoreitemlistText");
		checkscore.getItemScoreList("2","#scoreitemlistText2");
		
		holly.post(holly.getPath()+"/rest/judgeCheckerRest/getEvaluateResult",{"paperId":checkscore.param.paperId},function(e){
			
			if(e.success){
				if(!e.content){
					$('#checkContainer').show();
					$('#checkInfo').textbox();
				}else{
					if(e.content.checkStatus == 0) {
						$('#checkContainer').show();
						$('#checkInfo').textbox();
					}
				}
			}else{
				toastr.error(e.errorMessage);
			}
		});
	},
	getSysUser:function(userCode){
		var userName = "";
		holly.post(holly.getPath()+"/rest/user/findUser",{"userCode":userCode},function(e){
			if(e.success){
				userName = e.content[0].userName;
			}else{
				holly.showError(e.errorMessage);
			}
		},true);
		return userName;
	},
	paramJson:function(param,getdictname){
		if(param){
			var map = $("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':getdictname}});
			var text = '';
			$.each(param.split(","), function(index, item){
				text += (index==0?'':',')+(map[item]?map[item]:'');
			});
			return text;
		}
	},
	getItemScoreList:function(orderNum,el){
		holly.post(holly.getPath()+"/rest/commonRest/getItemScoreList",{"orderNum":orderNum, "paperId":checkscore.param.paperId},function(e){
			if(e.success){
				if(e.content){
					var content = e.content;
					var html = "";
					if(orderNum=="0"){
						for(var i=0; i<content.length; i++){
							html += '<li><label><input type="checkbox" onchange="check.scoreItemCheck(this,'+(content[i].scoreType=="0"?"+":"-")+content[i].score+')" value="'+content[i].scoreItemId+'" class="chk" name="scoreItems"/><span class="text" title="'+content[i].scoreItemName+'">'+content[i].scoreItemName+'</span><span class="point">'+(content[i].scoreType=="0"?"+":"-")+content[i].score+'</span></label></li>';
						}
					}else{
						if(content.length > 0){
							for(var i=0; i<content.length; i++){
								html += '<li><span class="text" title="'+content[i].scoreItemName+'">'+content[i].scoreItemName+'</span><span class="point">'+(content[i].scoreType=="0"?"+":"-")+content[i].score+'</span></li>';
							}
						}else{
							html += "无扣分项";
						}
					}
					$(el).html(html);
				}
			}else{
				toastr.error(e.errorMessage);
			}
		});
	},
	scoreItemCheck:function(obj,score){
		var val = parseInt($('#reviewScore').val());
		if(obj.checked){
			val = val+score;
			$('#reviewScore').val(val);
		}else{
			val = val-score;
			$('#reviewScore').val(val);
		}
		$('#reviewScoreDiv').html(val+'分');
	},
	saveScore:function(){
		var formJson = holly.form2json($("#scoreForm"));
		if(!formJson.checkScore){
			holly.showError('请选择星级');
		}else if(!formJson.checkInfo){
			holly.showError('请填写评语');
		}else{
			var param = {
				"checkInfo":formJson.checkInfo,
				"checkScore":formJson.checkScore,
				"checkStatus":'1',
				"paperId":checkscore.param.paperId
			};
			checkscore.formSubmit(param);
		}
	},
	formSubmit:function(param){
		var vaild = $("#scoreForm").form('enableValidation').form('validate');
		if(vaild){
			$.messager.progress({
				text:'正在提交'
			});
			holly.post(holly.getPath()+"/rest/judgeCheckerRest/evaluatePaper",param,function(e){
				if(e.success){
					// if(parent.$("#"+checkscore.param.frameId).length>0){
					// 	var $obj = parent.$("#"+checkscore.param.frameId)[0].contentWindow;//刷新列表
					// 	$obj.search();
					// }
					
					holly.showSuccess('提交成功');
					
					// checkscore.closeCurrentIframe();
					//parent.main.closeIframe(window.frameElement && window.frameElement.id || '');//关闭当前页面
					// var url = holly.getPath()+'/hollysqm/unchecked/page/check.jsp';
					// window.location.href=url;

                    //跳转质检成绩页面
					var frameId = window.frameElement && window.frameElement.id || '';
                    var url = holly.getPath() + "/hollysqm/paperscore/page/paperscore.jsp?frameId=" + frameId ;
                    window.location = url;
				}else{
					holly.showError(e.errorMessage);
				}
				$.messager.progress('close');
			});
		}
	},
	closeCurrentIframe : function(){//关闭当前页面
		var $tabs = parent.$('#tabs');
		var curTab = $tabs.tabs('getSelected');
		var tabIndex = $tabs.tabs('getTabIndex',curTab);
		$tabs.tabs('close',tabIndex);
		return false;
	}

};


var listenTape = {
	init:function(voicePath,imgPath){
		var player,//播放器
			intever = null;//定时器
		player = new _mu.Player({
			baseDir: holly.getPath() + '/hollysqm/common/muplayer/dist'
		});//初始播放器
		//setSong(contextPath+path);//设置播放录音
		//音量调节
		$(document).on('sliderchange', "#voice", function (e, result) {
			player.setVolume(result.value);
		});
		$('.myautoplayer-play').on('click',function(){
			if($(this).hasClass('icon-play')){
				player.play();//播放事件
				$(this).removeClass("icon-play").addClass("icon-pause");
				intever = scrollImg();
			}else if($(this).hasClass('icon-pause')){
				player.pause();//暂停
				$(this).removeClass("icon-pause").addClass("icon-play");
				clearInterval(intever);//清除事件
			}
		});
		//图片点击播放事件
		$("#divScroll").click(function (e) {
			var scrollLeft = $("#divScroll").scrollLeft();
			var offset = $(this).offset();
			var relativeX = (e.pageX - offset.left) + scrollLeft;
			var len = parseInt(relativeX / $("#imgVoice").width() * player.duration() * 1000);
			player.play(len);
			intever = scrollImg();
			$(".icon-play").addClass("icon-pause").removeClass("icon-play");
		});
		//滚动图片
		function scrollImg() {
			return setInterval(function () {//改变滑动块
				var l = player.curPos() / player.duration();
				var left = $("#imgVoice").width();//图片宽度
				if(l >(1-900/left)){
					clearInterval(intever);//清除事件
					$(".icon-pause").addClass("icon-play").removeClass("icon-pause");
					return ;
				}
				$("#divScroll").animate({scrollLeft: left * l}, 1000);
			}, 1000);
		}

		//滚动条拖动事件
		$("#divScroll").mousedown(function () {
			var state = player.getState();
			if (state == 'playing') {
				player.pause();//暂停
				$(".icon-pause").addClass("icon-play").removeClass("icon-pause");
				clearInterval(intever);//清除事件
			}
		});
		player.setUrl(holly.getPath() + "/" + voicePath );//添加录音文件
		$("#imgVoice").attr("src",holly.getPath() + "/" + imgPath).removeClass('voiceloading').addClass('voicechatimg');
	}
};