/**
 *质检管理员修改评分
 */
var editscore = {
    //接收前台传来的参数
    frameId: holly.getUrlParams("frameId") ? holly.getUrlParams("frameId") : "",
    dataType: holly.getUrlParams("dataType") ? holly.getUrlParams("dataType") : "",
    paperId: holly.getUrlParams("paperId") ? holly.getUrlParams("paperId") : "",
    totalScoreReal: 0,//页面分时初始化
    thisIframeId: window.frameElement && window.frameElement.id || '',// 获取该页面iframe id

    //初始化页面
    init: function () {
        $("#caseItem").combobox("init", {'codeType': 'CASE_ITEM'});

        //查询接触记录相关信息所需的参数
        var param = {
            "dataType": holly.getUrlParams("dataType") ? holly.getUrlParams("dataType") : "",
            "paperId": holly.getUrlParams("paperId") ? holly.getUrlParams("paperId") : ""
        };
        //查询接触记录相关信息
        holly.post(holly.getPath() + "/rest/commonRest/getRecord", param, function (e) {
            if (e.success) {
                var content = e.content;
                var html = '';
                var chatHtml = '';
                var arrFlag = [];
                var arrText = [];
                editscore.contactId = content.contactId;
                editscore.acceptTime = content.acceptTime;
                editscore.recordFile = content.recordFile;
                editscore.planId = content.planId;
                editscore.dataType = content.dataType;
                //显示接触记录绑定的质检计划信息
                if (content.planId) {
                    if (content.planId == 'system') {
                        $('#planDetail').html('所属质检计划:' + content.planName);
                    } else {
                        $('#planDetail').html('<a class="sublink" href="javascript:;" onclick="editscore.planinfo()">所属质检计划:' + content.planName + '</a>');
                    }
                }
                if (param.dataType == 'v8') {
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">来电号码</div>';
                    if (content.caller != null) {
                        html += '<div class="icontrols">' + content.caller + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">坐席</div>';
                    if (content.user != null) {
                        html += '<div class="icontrols">' + content.user.username + '(' + content.user.agentCode + ')' + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">归属地</div>';
                    if (content.custArea != null) {
                        html += '<div class="icontrols">' + editscore.paramJson(content.custArea, 'CUST_AREA') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">品牌</div>';
                    if (content.custBand != null) {
                        html += '<div class="icontrols">' + editscore.paramJson(content.custBand, 'CUST_BAND') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">满意度</div>';
                    if (content.satisfaction != null) {
                        html += '<div class="icontrols">' + editscore.paramJson(content.satisfaction, 'V8SATISFACTION') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">业务类型</div>';
                    if (content.serviceType != null) {
                        html += '<div class="icontrols">' + editscore.paramJson(content.bussinessType, 'BUSINESS_TYPE') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">开始时间</div>';
                    if (content.startTime != null) {
                        html += '<div class="icontrols">' + content.startTime + '</div>';
                    }
                    html += '</div>';

                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">通话时长</div>';
                    if (content.length != null) {
                        html += '<div class="icontrols">' + content.length + '秒</div>';
                    }
                    html += '</div>';

                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">用户级别</div>';
                    if (content.custLevel != null) {
                        html += '<div class="icontrols">' + editscore.paramJson(content.custLevel, 'CUST_LEVEL') + '</div>';
                    }
                    html += '</div>';

                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">静音时长</div>';
                    if (content.custLevel != null) {
                        html += '<div class="icontrols">' + content.silenceLength + '秒</div>';
                    }
                    html += '</div>';

                    $('#voicechatDiv').show();
                    // 接触记录图片音频地址查询
                    holly.post(holly.getPath() + "/rest/commonRest/getRecordDir",
                        {
                            "contactId": editscore.contactId,
                            "acceptTime": editscore.acceptTime,
                            "recordFile": editscore.recordFile
                        }, function (e) {
                            if (e.success) {
                                listenTape.init(e.content.wavDir, e.content.imgDir);
                            } else {
                                toastr.error(e.errorMessage);
                            }
                        });
                } else if (param.dataType == 'i8') {
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">来话号码</div>';
                    if (content.caller != null) {
                        html += '<div class="icontrols">' + content.caller + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">坐席</div>';
                    if (content.user != null) {
                        html += '<div class="icontrols">' + content.user.username + '(' + content.user.agentCode + ')' + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">关闭方式</div>';
                    if (content.closeType != null) {
                        html += '<div class="icontrols">' + editscore.paramJson(content.closeType, 'CLOSE_TYPE') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">会话等级</div>';
                    if (content.sessionType != null) {
                        html += '<div class="icontrols">' + editscore.paramJson(content.sessionType, 'SESSION_TYPE') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">满意度</div>';
                    if (content.satisfaction != null) {
                        html += '<div class="icontrols">' + editscore.paramJson(content.satisfaction, 'I8SATISFACTION') + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">服务类型</div>';
                    if (content.serviceType != null) {
                        html += '<div class="icontrols">' + content.serviceType + '</div>';
                    }
                    html += '</div>';
                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">开始时间</div>';
                    if (content.startTime != null) {
                        html += '<div class="icontrols">' + content.startTime + '</div>';
                    }
                    html += '</div>';

                    html += '<div class="icontrol-group">';
                    html += '<div class="icontrol-label">会话时长</div>';
                    if (content.length != null) {
                        html += '<div class="icontrols">' + content.length + '秒</div>';
                    }
                    html += '</div>';
                }
                $('#getRecordDiv').html(html);

                arrFlag = content.txtContent.match(/n\d#/ig);
                arrText = content.txtContent.split(/n\d#/ig);
                for (var i = 0; i < arrFlag.length; i++) {
                    if (arrFlag[i] == 'n0#') {
                        chatHtml += '<div class="text worker"><span style="text-align:right;">' + (i + 1) + '&nbsp;&nbsp;</span>坐席：' + (arrText[i + 1]) + '</div>';
                    } else if (arrFlag[i] == 'n1#') {
                        chatHtml += '<div class="text visiter"><span style="text-align:right;">' + (i + 1) + '&nbsp;&nbsp;</span>客户：' + (arrText[i + 1]) + '</div>';
                    }
                }
                $('#chatqa').html(chatHtml);

            } else {
                toastr.error(e.errorMessage);
            }
        });

        //查询打分标签并根据成绩自动勾选
        holly.post(holly.getPath() + "/rest/commonRest/getItemScoreList", {
            "orderNum": "0",
            "paperId": param.paperId
        }, function (e) {
            if (e.success) {
                var content = e.content;
                var html = "";
                var hasScore = new Array();
                holly.post(holly.getPath() + "/rest/commonRest/getItemScoreList", {
                    "orderNum": "1",
                    "paperId": param.paperId
                }, function (e) {
                    if (e.success) {
                        var innerContent = e.content;
                        for (var i = 0; i < innerContent.length; i++) {
                            hasScore.push(innerContent[i].scoreItemId);
                        }
                        for (var i = 0; i < content.length; i++) {
                            html += '<li><label title="' + content[i].scoreItemName + '"><input type="checkbox"';
                            if (editscore.contains(hasScore, content[i].scoreItemId)) {
                                html += ' checked';

                            }
                            html += ' onchange="editscore.scoreItemCheck(this,' + (content[i].scoreType == "0" ? "+" : "-") + content[i].score + ')" value="' + content[i].scoreItemId + '" class="chk" name="scoreItems"/><span class="text">' + content[i].scoreItemName + '</span><span class="point">' + (content[i].scoreType == "0" ? "+" : "-") + content[i].score + '</span></label></li>';
                        }
                        $('#scoreitemlist').html(html);
                    }
                });

            } else {
                toastr.error(e.errorMessage);
            }
        });

        //查询质检单评分
        holly.post(holly.getPath() + "/rest/commonRest/getPaperScoreResult", {"paperId": param.paperId}, function (e) {
            if (e.success) {
                if (e.content) {
                    $('#scoreDiv').html(e.content.totalScore + '分');
                    $('#checkerInfoDiv').html(e.content.scorerName + '(' + e.content.scorerCode + ')');
                    $('#scoreTimeDiv').html(e.content.scoreTime);
                    $('#comments').textbox('setValue', e.content.comments);
                    editscore.totalScoreReal = parseInt(e.content.totalScore);


                }
            }
        });

        //典型案例绑定
        // 获取当前质检单绑定的典型案例
        holly.post(holly.getPath() + "/rest/commonRest/getPaperCase", {"paperId": param.paperId}, function (e) {
            if (e.success) {
                if (e.content) {

                    if (e.content.caseItem) {
                        $("#caseItem").combobox('setValue', e.content.caseItem);

                        if (e.content.recordType == "1") {//优秀录音
                            $('#recordType1').attr('checked', 'checked');
                        } else {//问题录音
                            $('#recordType2').attr('checked', 'checked');
                        }
                        editscore.recordTypeEvent(e.content.recordType);
                    } else {
                        $('#recordType2').attr('checked', 'checked');
                    }

                }
            }
        });


    },
    paramJson: function (param, getdictname) {
        if (param) {
            var map = $("#planlistdatagrid").datagrid("getdictnames", {'param': {'codeType': getdictname}});
            var text = '';
            $.each(param.split(","), function (index, item) {
                text += (index == 0 ? '' : ',') + (map[item] ? map[item] : '');
            });
            return text;
        }
    },
    scoreItemCheck: function (obj, point) {

        if (obj.checked) {
            editscore.totalScoreReal += point;
        } else {
            editscore.totalScoreReal -= point;
        }
        val = editscore.totalScoreReal;
        if (val > 100) {
            val = 100;
        } else if (val < 0) {
            val = 0;
        }
        $('#totalScore').val(val);
        $('#scoreDiv').html(val + '分');
    },

    planinfo: function () {
        var url = holly.getPath() + "/hollysqm/planmanager/page/dlg-planinfo.jsp";
        url += "?planId=" + editscore.planId;
        url += "&dataType=" + editscore.dataType;
        $('#dlg-planinfo').dialog('open').dialog('refresh', url);
    },

    recordTypeEvent: function (val) {
        if (val == "1") {
            $('#caseItemRequired').show();
            $('#caseItem').combobox('enable');
        } else if (val == "2") {
            $('#caseItemRequired').hide();
            $('#caseItem').combobox('setValue', '').combobox('disable');
        }
    },

    contains: function (array, val) {
        flag = false;
        for (var i = 0; i < array.length; i++) {
            if (array[i] == val) {
                flag = true;
            }
        }

        return flag;
    },

    //修改评分
    editScore: function () {
        var scoreItem = [];
        $('input[name="scoreItems"]:checked').each(function () {
            scoreItem.push($(this).val());
        });
        var caseItem = $("#caseItem").combobox("getValue");
        var recordType = $('input[name="recordType"]:checked').val();
        if ((recordType == '1') && (caseItem == "")) {
            holly.showError('请选择质检案例');
            return false;
        }

        $("#itemId").val(scoreItem);
        $("#contactId").val(editscore.contactId);

        $.messager.confirm('提醒', '您正在提交质检单，评分为' + $('#totalScore').val() + '分,请确认评分和问题描述是否已输入完整?', function (r) {
            if (r) {
                editscore.formSubmit();
            }
        });


    },

    formSubmit: function () {
        var vaild = $("#scoreForm").form('enableValidation').form('validate');
        if (vaild) {
            $.messager.progress({
                text: '正在提交...'
            });
            $('#scoreForm').form("submit", {
                url: holly.getPath() + "/rest/commonRest/editPaper?paperId=" + editscore.paperId,
                success: function (e) {
                    var d = ($.type(e) == "string") ? $.parseJSON(e) : e;
                    if (d.success) {
                        holly.showSuccess('评分成功');

                        // 如果待评分列表页面没有关闭，并且该页面是从待评分列表打开的
                        // if((parent.$("#"+editscore.frameId).length>0)){
                        // 	var $obj = parent.$("#"+editscore.frameId)[0].contentWindow;
                        // 	$obj.search();//重新搜索列表
                        // }
                        // var $tabs = parent.$('#tabs');
                        // var curTab = $tabs.tabs('getSelected');
                        // var tabIndex = $tabs.tabs('getTabIndex',curTab);
                        // $tabs.tabs('close',tabIndex);

                        //跳转质检成绩页面
                        var url = holly.getPath() + "/hollysqm/paperscore/page/paperscore.jsp?frameId=" + editscore.thisIframeId;
                        window.location = url;
                    } else {
                        holly.showError('提交失败');
                    }
                    $.messager.progress('close');
                }
            });
        }
    }

};

var listenTape = {
    init: function (voicePath, imgPath) {
        var player,//播放器
            intever = 0;//定时器 返回number类型
        player = new _mu.Player({
            baseDir: holly.getPath() + '/hollysqm/common/muplayer/dist'
        });//初始播放器
        //setSong(contextPath+path);//设置播放录音
        //音量调节
        $(document).on('sliderchange', "#voice", function (e, result) {
            player.setVolume(result.value);
        });
        $('.myautoplayer-play').on('click', function () {
            if ($(this).hasClass('icon-play')) {
                player.play();//播放事件
                $(this).removeClass("icon-play").addClass("icon-pause");
                intever = scrollImg();
            } else if ($(this).hasClass('icon-pause')) {
                player.pause();//暂停
                $(this).removeClass("icon-pause").addClass("icon-play");
                clearInterval(intever);//清除事件
            }
        });
        //图片点击播放事件
        $("#divScroll").click(function (e) {
            var scrollLeft = $("#divScroll").scrollLeft();
            var offset = $(this).offset();
            var relativeX = (e.pageX - offset.left) + scrollLeft;
            var len = parseInt(relativeX / $("#imgVoice").width() * player.duration() * 1000);
            player.play(len);
            intever = scrollImg();
            $(".icon-play").addClass("icon-pause").removeClass("icon-play");
        });
        //滚动图片
        function scrollImg() {
            return setInterval(function () {//改变滑动块
                var l = player.curPos() / player.duration();
                var left = $("#imgVoice").width();//图片宽度
                if (l > (1 - 900 / left)) {
                    clearInterval(intever);//清除事件
                    $(".icon-pause").addClass("icon-play").removeClass("icon-pause");
                    return;
                }
                ;
                $("#divScroll").animate({scrollLeft: left * l}, 1000);
            }, 1000);
        }

        //滚动条拖动事件
        $("#divScroll").mousedown(function () {
            var state = player.getState();
            if (state == 'playing') {
                player.pause();//暂停
                $(".icon-pause").addClass("icon-play").removeClass("icon-pause");
                clearInterval(intever);//清除事件
            }
        });
        player.setUrl(holly.getPath() + "/" + voicePath);//添加录音文件
        $("#imgVoice").attr("src", holly.getPath() + "/" + imgPath).removeClass('voiceloading').addClass('voicechatimg');
    }
};