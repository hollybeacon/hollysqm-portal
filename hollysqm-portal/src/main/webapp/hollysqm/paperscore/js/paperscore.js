var paperScore = {
	/**
	 * 初始化加载数据
	 */
	init:function(){

		$("#formDataType").combobox("init_extend",{'codeType':'DATATYPE','hasAllOption':false,event:{
			onLoadSuccess:function(){
				$(this).combobox('setValue','v8');
			}
		}});
		$("#formappealState").combobox("init_filter",{'codeType':'QUALITY_STATUS',"filterValue":"0"});
		//$("#formplanName").combobox("init_extend",{'codeType':'PLAN_NAME'});
		paperScore.planNameOption();
		$("#formcheckScore").combobox("init_extend",{'codeType':'CHECK_SCORE'});
		// 初始化质检单信息dialog
		$('#dlg-sqminfo').dialog({
			onClose:function(){
				// 关闭dialog停止播放语音
				if(listenTape.player.pause){
					listenTape.player.pause();
				}
			}
		});
		$("#paperScoredatagrid").datagrid({
			fit:true,
			url:holly.getPath()+'/rest/paperscore/queryPaperscore',
			method:'get',
			singleSelect : true,
			border:false,
			fitColumns:true,
			queryParams:{'paperType':'v8'},
			pagination: true,
			loadFilter: function(data){return (data.content)?data.content:data;},
			columns:[[
				{field:'paperTypeName',title:'质检来源',width:60},
				{field:'planName',title:'质检计划',formatter:paperScore.formatPlanName},
				{field:'caller',title:'来电号码',width:60},
				{field:'agentCodeName',title:'坐席',width:80},
				{field:'totalScore',title:'评分得分',width:40},
				{field:'modifyTime',title:'质检时间',width:100},
				{field:'qualityStatusName',title:'质检状态'},
				{field:'reviewScore',title:'复核得分',width:40,formatter:paperScore.formatReviewScore},
				{field:'createCodeName',title:'质检员'},
				{field:'hasRead', title:'坐席查看',width:40,formatter:paperScore.formatHasRead},
				{field:'checkScore',title:'评价',width:40,formatter:paperScore.formatCheckerScore},
				{field:'option',title:'操作',width:60,formatter:paperScore.formatOption}
			]],
			loadMsg:'数据加载中请稍后……',
			onLoadSuccess: function (data)  {
				if (data.rows.length == 0) {
					$("#exportTable").hide();//没有数据时隐藏导出按钮
					var body = $(this).data().datagrid.dc.body2;
					body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
				}else $("#exportTable").show();
			}
		});
		$("#paperScoredatagrid_i8").datagrid({
			fit:true,
			method:'get',
			singleSelect : true,
			border:false,
			fitColumns:true,
			pagination: true,
			closed:true,
			loadFilter: function(data){return (data.content)?data.content:data;},
			columns:[[
				{field:'paperTypeName',title:'质检来源',width:60},
				{field:'planName',title:'质检计划',formatter:paperScore.formatPlanName},
				{field:'caller',title:'来电号码',width:60},
				{field:'agentCodeName',title:'坐席',width:80},
				{field:'totalScore',title:'评分得分',width:60},
				{field:'modifyTime',title:'质检时间',width:100},
				{field:'qualityStatusName',title:'质检状态'},
				{field:'reviewScore',title:'复核得分',width:40,formatter:paperScore.formatReviewScore},
				{field:'createCodeName',title:'质检员'},
				{field:'closeType',title:'关闭方式',width:50,formatter:paperScore.formatCloseType},
				{field:'sessionLevel',title:'会话等级',width:50,formatter:paperScore.formatSessionType},
				{field:'checkScore',title:'评价',width:40,formatter:paperScore.formatCheckerScore},
				{field:'option',title:'操作',width:40,formatter:paperScore.formatOption}
			]],
			loadMsg:'数据加载中请稍后……',
			onLoadSuccess: function (data)  {
				if (data.rows.length == 0) {
					$("#exportTable").hide();//没有数据时隐藏导出按钮
					var body = $(this).data().datagrid.dc.body2;
					body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
				}else $("#exportTable").show();
			}
		});
	},

	
	/**
	 * 搜索
	 */
	search:function(){
		strReplace("agentCode");//,
		strReplace("createCode");
		var param = holly.form2json($("#searchpaperScore"));
		if(paperScore.validateTime(param)){
			if(param.paperType=='v8'){
				if($("#paperScoredatagrid").datagrid('getPanel').panel('options').closed){
					$("#paperScoredatagrid_i8").datagrid('getPanel').panel('close');
					$("#paperScoredatagrid").datagrid('getPanel').panel('open').panel('resize');
				}
				$("#paperScoredatagrid").datagrid("search",{'param':param});
			}else if(param.paperType=='i8'){
				$("#paperScoredatagrid_i8").datagrid('options').url = holly.getPath()+'/rest/paperscore/queryPaperscore';
				if($("#paperScoredatagrid_i8").datagrid('getPanel').panel('options').closed){
					$("#paperScoredatagrid").datagrid('getPanel').panel('close');
					$("#paperScoredatagrid_i8").datagrid('getPanel').panel('open').panel('resize');
				}
				$("#paperScoredatagrid_i8").datagrid("search",{'param':param});
			}
		}
	},
	formatPlanName : function(val,row,index){//质检计划名称
        if(val == undefined || val =='' || val == null || val=='null') {
            val = '全文检索';
        }
        return '<span title='+val+'>'+val+'</span>';
	},
	// 质检来源
	formatDataTyp:function(val, row, index) {
		//var res =dataTypeMap[val]; //获取对应的数据
		//return "<span title='"+res+"'>"+res+"</span>";
	},
	formatCloseType:function(val, row, index){
		var res = closeTypeMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	},
	formatSessionType:function(val, row, index){
		var res = sessionTypeMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	},
	// 申诉状态
	formatState:function(val, row, index) {
		//var res =qualityStatusMap[val]; //获取对应的数据
		//return res;
	},
	// 评价
	formatCheckerScore:function(val, row, index) {
		var res =checkScoreMap[val]; //获取对应的数据
		return res; 
	},
	//坐席是否查看
	formatHasRead:function(val, row, index) {
		if(val == '0' || val == 0 || val == null || val == 'null') {
			return '未查看'; 
		} else {
			return '已查看'; 
		}
		
	},
	formatReviewScore : function(val ,row ,index){
		return val ?"<span class = 'review_score'>"+ val +"</span>":val; 
	},
	// 坐席
	formatAgentName:function(val,row,index){ 
		//return val?"<span title='"+row.Agent + "(" +val+ ")"+"'>"+row.Agent + "(" + val+ ")"+"</span>":"";
	},
	// 质检员
	formatCheckerName:function(val,row,index){
		//return val?"<span title='"+row.checker + "(" +val+ ")"+"'>"+row.checker + "(" + val+ ")"+"</span>":"";
	},
	// 操作
	formatOption:function(val,row,index){
		var content = '';
			content += "<span class='grid-opt-wrap'>";
			if(perm.checker){
				content	+= "<a href='javascript:void(0)' onclick=\"paperScore.sqminfo('"+row.paperId+"','"+row.planId+"')\" class='grid-optcon icon-eye-open' title='查看'></a>";
			}
			if(perm.checkAdmin){
				if(!row.checkScore){
					content	+= "<a href='javascript:void(0)' onclick=\"paperScore.checkScore('"+row.paperId+"','"+row.planId+"')\" class='grid-optcon icon-star' title='评价'></a>";
				}
			}
			if(perm.checker){
				if(!row.isPublicResult || row.isPublicResult == '0'){
					content	+= "<a href='javascript:void(0)' onclick=\"paperScore.publishResult('"+row.paperId+"')\" class='grid-optcon icon-legal' title='发布成绩'></a>";
				}
				
				if((!row.isPublicResult || row.isPublicResult == '0') && perm.checkAdmin) {
					content += "<a href='javascript:void(0)' onclick=\"paperScore.editScore('"+row.paperId+ "','"+row.planId+"')\" class='grid-optcon icon-edit' title='修改成绩'></a>";
				}
			}
			content += "</span>";
		return content;
	},
	checkScore : function(paperId,contactId){
		var frameId = window.frameElement && window.frameElement.id || '';
		var url = holly.getPath()+'/hollysqm/paperscore/page/checkscore.jsp';
		url += '?paperId=' + paperId+"&contactId="+contactId;
		url += '&frameId=' + frameId;
		// parent.main.addTab('iframecheckscore',url,'评价');
		window.location = url;
	},
	
	editScore : function(paperId,contactId) {
		var frameId = window.frameElement && window.frameElement.id || '';
		var url = holly.getPath()+'/hollysqm/paperscore/page/editscore.jsp';
		url += '?paperId=' + paperId+"&contactId="+contactId;
		url += '&dataType=v8';
		url += '&frameId=' + frameId;
		// parent.main.addTab('iframeeditscore',url,'修改成绩');
        window.location = url;
    },
	
	sqminfo : function(paperId,contactId){
		var url = holly.getPath()+"/hollysqm/paperscore/page/dlg-sqminfo.jsp";
		url += "?paperId=" + paperId+"&contactId="+contactId;
		$('#dlg-sqminfo').dialog('open').dialog('refresh',url);
	},
	planNameOption : function(){
		$('#formplanName').combobox({
		    url : holly.getPath() + '/rest/commonRest/queryPlan',
		    valueField : 'value', 
			textField : 'name',
			loadFilter: function(data){
				var jsonArray=new Array();
				for(var o in data.content){
					jsonArray.push(data.content[o]);
				}
				return jsonArray;
			},onHidePanel : function(){
				var el = $(this);
				var value = el.combobox("getValue");
				if(!value)//如果没有匹配到值，则默认设置成空
					 el.combobox("setValue","");
			}
		});  
	},
	exportPaper : function(toPage){
		var formDataType = $("#formDataType").combobox("getValue");
		var grid = $('#paperScoredatagrid');  
		if(formDataType == "i8")
			grid = $('#paperScoredatagrid_i8');  
		var options = grid.datagrid('getPager').data("pagination").options;  
		var total = options.total;
		if(total > export_total){
			$.messager.alert("提示","导出数据不能大于"+export_total+"，请继续筛选数据！");
			return false;
		}
		if(toPage){
			var url =holly.getPath()+"/rest/csv/wizard/sqlexport?tableId=csv.score.queryPaperScore_"+formDataType+"&sqlKey=velocity.score.queryPaperScore&mediaId=1";
			var targetIframe ="iframe-export";
			$('#searchpaperScore').attr("action",url);
			$('#searchpaperScore').attr("target",targetIframe);
			$('#searchpaperScore').submit();
			$('#dlg-export').dialog('open');
		}else{
			$("#iframe-export")[0].contentWindow.exportData();
			$('#dlg-export').dialog('close');
		}
	},
	resetForm : function() {
		$('#searchpaperScore').form('reset');

		var data = $('#formDataType').combobox('getData');
		$('#formDataType').combobox('select', data[0].value);// 默认选中第一个

	},
	validateTime : function (params){
		var startTime = params.createStartTime;
		var endTime = params.createEndTime;
		
		if(startTime && endTime && startTime > endTime){
			$.messager.alert('提示','评分开始时间不能大于结束时间!');
			return false;
		}
		/*
		var startTimeLon = new Date(startTime).getTime();
		var lastMonthLon = new Date(endTime).getTime();
		var timeLon = (lastMonthLon - startTimeLon) / (1000*60*60*24);
		if(timeLon > 31){
			$.messager.alert('提示','评分开始时间和结束时间不能超过31天!');
			return false;
		}*/
		return true;
	},
	publishResult : function(paperId){//发布成绩
		var formDataType = $("#formDataType").combobox("getValue");
		strReplace("agentCode");//,
		strReplace("createCode");
		var param = holly.form2json($("#searchpaperScore"));
		param["paperId"] = paperId;
		param["isPublicResult"] = "0";//未发布成绩
		$.messager.confirm('提示', '是否发布成绩？', function(r){
			if(r){
				$.messager.progress({
					text:'正在提交...'
				});
				holly.post(holly.getPath()+"/rest/paperscore/publishedScore",param,function(e){
					if(e.success){
						holly.showSuccess("操作成功!");
						var grid = $('#paperScoredatagrid');  
						if(formDataType == "i8")
							grid = $('#paperScoredatagrid_i8');  
						grid.datagrid('reload');
					}else{
						holly.showError(e.errorMessage);
					}
					$.messager.progress('close');
				});
			}
		});
	}
};
/**
*	过滤特殊字符
*	str：要进行过滤的字符串
*/
function strReplace(id){
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
	var rs = "";
	var str = $("#"+id).textbox("getValue");
	if($.trim(str).length > 0 ){
		for (var i = 0; i < str.length; i++) {
			rs = rs+str.substr(i, 1).replace(pattern, '');
		}
		$("#"+id).textbox("setValue",rs);
	}
}
