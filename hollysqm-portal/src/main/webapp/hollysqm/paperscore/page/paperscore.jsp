<%@page import="com.hollycrm.hollybeacon.business.personoa.system.cache.SysConfigCacheHandler"%>
<%@page import="com.hollycrm.hollybeacon.basic.util.SpringUtil"%>
<%@page import="com.hollycrm.hollybeacon.basic.util.AppConfigUtils"%>
<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>质检成绩</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type="text/javascript" src="../js/paperscore.js" ></script>
<%
Integer export_total = AppConfigUtils.getConfig().getInteger("system.export.total", 100000);
SysConfigCacheHandler sysConfigCacheHandler = SpringUtil.getBean("sysConfigCacheHandler", SysConfigCacheHandler.class);
String roleId = sysConfigCacheHandler.getSysConfigValue("USER_ROLE_TAG", "SQM_ZJY");
%>
<script type="text/javascript">
    var loginUser = ${loginUser};

    var perm={
	checker:<%=SecurityUtils.getSubject().isPermitted("paperscore:checker")%>,
	checkAdmin:<%=SecurityUtils.getSubject().isPermitted("paperscore:checkAdmin")%>
};
  var export_total = <%=export_total%>;
// 获取必要的数据字典MAP		
//var dataTypeMap=$("#paperScoredatagrid").datagrid("getdictnames",{'param':{'codeType':'DATATYPE'}}); 
//var planNameMap=$("#paperScoredatagrid").datagrid("getdictnames",{'param':{'codeType':'PLAN_NAME'}});
//var qualityStatusMap=$("#paperScoredatagrid").datagrid("getdictnames",{'param':{'codeType':'QUALITY_STATUS'}});
var checkScoreMap=$("#paperScoredatagrid").datagrid("getdictnames",{'param':{'codeType':'CHECK_SCORE'}});
var closeTypeMap=$("#unCheckeddatagrid").datagrid("getdictnames",{'param':{'codeType':'CLOSE_TYPE'}});
var sessionTypeMap=$("#unCheckeddatagrid").datagrid("getdictnames",{'param':{'codeType':'SESSION_TYPE'}});
$(function() {
	///paperScore.planNameOption();
	paperScore.init();
	$("#searchpaperScore").keyup(function(event){
		if(event.keyCode==13){
			paperScore.search();
		}
	});
});

function search(){
	paperScore.search();
}
</script> 
<style type="text/css">
.review_score{
	color: red;
}
</style>
</head>
<body class="easyui-layout">
	<div data-options="region:'center',border:false" style="padding:10px;">
		<div class="easyui-layout" data-options="fit:true">
			<!-- 搜索区域 -->
			<div data-options="region:'north',border:false" style="padding-bottom:10px;">
				<form id="searchpaperScore" method="post" class="search-form">
					<div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">质检来源</span>
							<div class="valueGroup">
								<select name="paperType" id="formDataType" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">质检计划</span>
							<div class="valueGroup">
								<select name="planName" id="formplanName" class="easyui-combobox" data-options="width:'100%'"></select>	
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">质检时间</span>
							<div class="valueGroup">
								<input type="text" class="easyui-datebox" name="createStartTime" data-options="editable:false,width:'100%'">
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">至</span>
							<div class="valueGroup">
								<input type="text" class="easyui-datebox" name="createEndTime" data-options="editable:false,width:'100%'">
							</div>
						</span>
					</div>
					<div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">质检状态</span>
							<div class="valueGroup">
								<select name="qualityStatus" id="formappealState" class="easyui-combobox" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">坐席</span>
							<div class="valueGroup">
								<input type="text" class="easyui-textbox" name="agentCode" id="agentCode" data-options="prompt:'请输入姓名或工号',editable:true,width:'100%'">
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">质检员</span>
							<div class="valueGroup">
								<!-- 判断当前用户没有质检员角色 -->
								<shiro:lacksRole name="<%=roleId %>">
   									<input type="text" class="easyui-textbox" name="createCode" id="createCode" data-options="prompt:'请输入姓名或工号',editable:true,width:'100%'">
								</shiro:lacksRole>
								 <!-- 判断当前用户拥有质检员角色 -->
								<shiro:hasRole name="<%=roleId %>">
									<script>
									$(function(){
										$("#createCode").textbox("setValue",loginUser.userCode);
									});	
									</script>
								    <input type="text" class="easyui-textbox" name="createCode" id="createCode" data-options="editable:false,width:'100%'">
								</shiro:hasRole>
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">评价</span>
							<div class="valueGroup">
								<select name="checkScore" id="formcheckScore" class="easyui-combobox" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>						
					</div>
					<div class="btn-group">
						<a href="javascript:;" class="operation search-btn" onclick="paperScore.search()" title="搜索"></a>
						<a href="javascript:;" class="operation reset-btn" onclick="paperScore.resetForm();" title="重置"></a>
					</div>
				</form>
			</div>
			<!-- 搜索区域 end -->	
			<div data-options="region:'center',border:false">
				<div id="tt">
					<a href="javascript:void(0);" class="easyui-linkbutton" onclick="paperScore.publishResult('');">批量发布成绩</a>
					<a href="javascript:void(0);" class="easyui-linkbutton" id="exportTable" onclick="paperScore.exportPaper(true)">导出</a>
				</div>
				<div class="easyui-panel" title="质检成绩" data-options="tools:'#tt',fit:true">
					<table id="paperScoredatagrid"></table>
					<table id="paperScoredatagrid_i8"></table>
				</div>
			</div>
		</div>
	</div>
	<div id="dlg-export" class="easyui-dialog" title="导出质检成绩"
		data-options="
			closed:true,
			buttons: '#dlg-export-buttons',
			width:650,
			height:'95%'
		">
		<iframe id="iframe-export" name="iframe-export" src="" frameborder="0" style="width:100%;height:100%;display:block;"></iframe>
    </div>
	<div id="dlg-export-buttons">
		<a href="javascript:void(0)" class="easyui-linkbutton l-btn-red" onclick="paperScore.exportPaper(false)">确定</a>
		<a href="javascript:void(0)" class="easyui-linkbutton" onclick="$('#dlg-export').dialog('close')">取消</a>
	</div>
	<div id="dlg-sqminfo" class="easyui-dialog" title="质检单信息"
		data-options="
			closed:true,
			width:'95%',
			height:'95%',
			draggable:false
		">
	</div>
</body>
</html>