<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%
	String paperId = request.getParameter("paperId");
%>
<script type="text/javascript">
var paperId = "<%=paperId%>";
</script>
<link rel="stylesheet" href="<%=request.getContextPath() %>/hollysqm/common/css/sqm.css">
<script type="text/javascript" src="<%=request.getContextPath() %>/hollysqm/common/muplayer/dist/player.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/hollysqm/common/jquery-ui/jquery-ui-1.10.3.mouse_core.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/hollysqm/common/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/hollysqm/common/bootstrap/js/bootstrapslider.min.js"></script>
<script type="text/javascript" src="<%=request.getContextPath() %>/hollysqm/paperscore/js/dlg-sqminfo.js"></script>
<script type="text/javascript">
// 获取必要的数据字典MAP		
var satisfactionMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'SATISFACTION'}});
var serviceTypeMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'BUSINESS_TYPE'}});
var caseItemMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'CASE_ITEM'}});
var errorTypeMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'ERROR_TYPE'}});
var appealStatusMap = {0:"已申诉",1:"已通过",2:"已驳回",3:"待处理"};
$(function(){
	dlgSqminfo.init();
});
</script>
<div class="scorepanel">
	<div class="scoreleft">
		<div class="container-sqm1">
			<div class="heading">
				<i class="icon"></i>
				<div class="title">接触记录</div>
			</div>
			<div class="contentpane" style="padding:15px;">
				<div class="iform-horizontal column-3 labelmin iform-text" id="getRecordDiv">
				</div>
				<div class="container-voicechat" id="voicechatDiv" style="margin-bottom:20px;display:none;">
					<!-- 语音波形图 -->
					<div class="voicechatbox" id="divScroll">
						<img src="<%=request.getContextPath() %>/hollysqm/common/images/loading-mask.gif" class="voiceloading" id="imgVoice">
					</div>
					<div class="myautoplayer">
						<span class="myautoplayer-play icon-play"></span>
						<span class="myautoplayer-volume icon-volume-up"></span>
						<div id="voice" class="progress slider">
							<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
						</div>
					</div>
				</div>
				<div class="chatqa" id="chatqa">
				</div>
			</div>
			<!-- <jspinclude page="../../common/weigui.jsp" flush="true"/> -->
		</div>
	</div>

	<div class="scoreright">
				<div class="container-sqm1">
					<div class="heading">
						<i class="icon"></i>
						<div class="title">评分</div>
					</div>
					<div class="contentpane">
						<div class="iform-horizontal labelmin iform-text">
							<div class="icontrol-group">
								<div class="icontrol-label">评分</div>
								<div class="icontrols icontrols-text">
									<ul class="scoreitemlist" id="scoreitemlistText">
									</ul>
								</div>
							</div>
							<div class="icontrol-group">
								<div class="icontrol-label">得分</div>
								<div class="icontrols icontrols-text"><span style="color:#ff0000;" id="totalScoreDiv">100分</span></div>
							</div>
							<div class="icontrol-group">
								<div class="icontrol-label">质检员</div>
								<div class="icontrols" id="checkerInfoDiv">
								</div>
							</div>
							<div class="icontrol-group">
								<div class="icontrol-label">评分时间</div>
								<div class="icontrols" id="scoreTimeDiv">
								</div>
							</div>
							<div class="icontrol-group" id="recordTypeTip">
								<div class="icontrol-label">典型案例</div>
								<div class="icontrols icontrols-text" id="recordTypeDiv">
								</div>
							</div>
							<div class="icontrol-group" id="caseItemTip">
								<div class="icontrol-label">质检案例</div>
								<div class="icontrols" id="caseItemDiv">
								</div>
							</div>
							
							
							<div class="icontrol-group" id="commonsTip">
								<div class="icontrol-label">问题描述</div>
								<div class="icontrols" id="commentsDiv">
								</div>
							</div>
							
						</div>
					</div>
				</div>
				<div class="container-sqm1" style="display:none;" id="reviewContainer">
					<div class="heading">
						<i class="icon"></i>
						<div class="title">复核</div>
					</div>
					<div class="contentpane">
						<div class="iform-horizontal labelmin iform-text">
							
							<div class="icontrol-group" id="scoreitemlistTip2">
								<div class="icontrol-label">复核</div>
								<div class="icontrols icontrols-text">
									<ul class="scoreitemlist" id="scoreitemlistText2">
									</ul>
								</div>
							</div>
							<div class="icontrol-group" id="reviewScoreTip">
								<div class="icontrol-label">得分</div>
								<div class="icontrols icontrols-text"><span style="color:#ff0000;" id="reviewScoreDiv"></span></div>
							</div>
							<div class="icontrol-group">
								<div class="icontrol-label">复核人</div>
								<div class="icontrols icontrols-text" id="checkerInfoDiv2"></div>
							</div>
							<div class="icontrol-group">
								<div class="icontrol-label">复核时间</div>
								<div class="icontrols icontrols-text" id="reviewTimeDiv"></div>
							</div>
							<div class="icontrol-group">
								<div class="icontrol-label">典型案例</div>
								<div class="icontrols icontrols-text" id="recordTypeDiv2">
								</div>
							</div>
							<div class="icontrol-group" id="caseItemTip2">
								<div class="icontrol-label">质检案例</div>
								<div class="icontrols" id="caseItemDiv2">
								</div>
							</div>
							
							<div class="icontrol-group">
								<div class="icontrol-label">问题描述</div>
								<div class="icontrols" id="commentsDiv2">
								</div>
							</div>
							
						</div>
					</div>
				</div>

				<div class="container-sqm1">
						<div class="heading">
							<i class="icon"></i>
							<div class="title">申诉记录</div>
						</div>
						<div class="contentpane">
							<div class="iform-horizontal labelmin">
								<div id="appealPaperDiv">
								</div>
							</div>
						</div>
			    </div>
		<div class="container-sqm1" style="display:none;" id="checkContainer">
			<div class="heading">
				<i class="icon"></i>
				<div class="title">评价</div>
			</div>
			<div class="contentpane">
				<form id="scoreForm" class="easyui-form" method="post">
					<div class="iform-horizontal labelmin iform-text">
						<div class="icontrol-group">
							<div class="icontrol-label">星级</div>
							<div class="icontrols" id="checkScoreDiv">
									<label class="evaluate-basic" title="一星">1 star</label>
									<label class="evaluate-basic" title="二星">2 stars</label>
									<label class="evaluate-basic" title="三星">3 stars</label>
									<label class="evaluate-basic" title="四星">4 stars</label>
									<label class="evaluate-basic" title="五星">5 stars</label>
							</div>
						</div>
						<div class="icontrol-group">
							<div class="icontrol-label">评语</div>
							<div class="icontrols" id="checkInfoDiv"></div>
						</div>
						<div class="icontrol-group">
							<div class="icontrol-label">评价人</div>
							<div class="icontrols" id="modifierDiv"></div>
						</div>
						<div class="icontrol-group">
							<div class="icontrol-label">评价时间</div>
							<div class="icontrols" id="modifyTimeDiv"></div>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>

</div>