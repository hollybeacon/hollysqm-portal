<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/hollybeacon/common/taglibs.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<%@ include file="/hollybeacon/common/meta.jsp" %>
<title>评价</title>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type="text/javascript" src="${ctx}/hollysqm/common/muplayer/dist/player.min.js"></script>
<script type="text/javascript" src="${ctx}/hollysqm/common/jquery-ui/jquery-ui-1.10.3.mouse_core.js"></script>
<script type="text/javascript" src="${ctx}/hollysqm/common/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="${ctx}/hollysqm/common/bootstrap/js/bootstrapslider.min.js"></script>
<script src="${ctx}/hollysqm/paperscore/js/checkscore.js"></script>
<script>
// 获取必要的数据字典MAP		
var satisfactionMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'SATISFACTION'}});
var serviceTypeMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'BUSINESS_TYPE'}});
var caseItemMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'CASE_ITEM'}});
var errorTypeMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'ERROR_TYPE'}});
var appealStatusMap = {0:"已申诉",1:"已通过",2:"已驳回",3:"待处理"};
$(function(){
	checkscore.init();
});
</script>
</head>
<body class="easyui-layout">
<div data-options="region:'center',border:false">

	<div class="container-sqm cfit"> 
		<div class="heading">
			<img class="pic" src="${ctx}/hollysqm/common/images/check.png" />
			<div class="title">评价</div>
		</div>
		<div class="contentpane">
			<div class="scorepanel">
				<div class="scoreleft">
					<div class="container-sqm1">
						<div class="heading">
							<i class="icon"></i>
							<div class="title">接触记录</div>
						</div>
						<div class="contentpane" style="padding:15px;">
							<div class="iform-horizontal column-3 labelmin iform-text" id="getRecordDiv">
							</div>
							<div class="container-voicechat" id="voicechatDiv" style="margin-bottom:20px;display:none;">
								<!-- 语音波形图 -->
								<div class="voicechatbox" id="divScroll">
									<img src="${ctx}/hollysqm/common/images/loading-mask.gif" class="voiceloading" id="imgVoice">
								</div>
								<div class="myautoplayer">
									<span class="myautoplayer-play icon-play"></span>
									<span class="myautoplayer-volume icon-volume-up"></span>
									<div id="voice" class="progress slider">
										<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
									</div>
								</div>
							</div>
							<div class="chatqa" id="chatqa">
							</div>
						</div>
						<%-- <jspinclude page="../../common/weigui.jsp" flush="true"/> --%>
					</div>
				</div>

				<div class="scoreright">
					<div class="container-sqm1">
						<div class="heading">
							<i class="icon"></i>
							<div class="title">评分</div>
						</div>
						<div class="contentpane">
								<div class="iform-horizontal labelmin iform-text">
									<div class="icontrol-group">
										<div class="icontrol-label">评分</div>
										<div class="icontrols icontrols-text">
											<ul class="scoreitemlist" id="scoreitemlistText">
											</ul>
										</div>
									</div>
									<div class="icontrol-group">
										<div class="icontrol-label">得分</div>
										<div class="icontrols icontrols-text"><span style="color:#ff0000;" id="totalScoreDiv">100分</span></div>
									</div>
									<div class="icontrol-group">
										<div class="icontrol-label">质检员</div>
										<div class="icontrols" id="checkerInfoDiv">
										</div>
									</div>
									<div class="icontrol-group">
										<div class="icontrol-label">评分时间</div>
										<div class="icontrols" id="scoreTimeDiv">
										</div>
									</div>
									
									<div class="icontrol-group" id="recordTypeTip">
									<div class="icontrol-label">典型案例</div>
									<div class="icontrols icontrols-text" id="recordTypeDiv">
									</div>
								</div>
									<div class="icontrol-group" id="caseItemTip">
									<div class="icontrol-label">质检案例</div>
									<div class="icontrols" id="caseItemDiv">
									</div>
								</div>
								
								<div class="icontrol-group" id="commonsTip">
									<div class="icontrol-label">问题描述</div>
									<div class="icontrols" id="commentsDiv">
								</div>
								</div>
									
								</div>
						</div>
					</div>
					<div class="container-sqm1" style="display:none;" id="reviewContainer">
						<div class="heading">
							<i class="icon"></i>
							<div class="title">复核</div>
						</div>
						<div class="contentpane">
							<div class="iform-horizontal labelmin iform-text">
								
								<div class="icontrol-group" id="scoreitemlistTip2">
									<div class="icontrol-label">复核</div>
									<div class="icontrols icontrols-text">
										<ul class="scoreitemlist" id="scoreitemlistText2">
										</ul>
									</div>
								</div>
								<div class="icontrol-group" id="reviewScoreTip">
									<div class="icontrol-label">得分</div>
									<div class="icontrols icontrols-text"><span style="color:#ff0000;" id="reviewScoreDiv"></span></div>
								</div>
								<div class="icontrol-group">
									<div class="icontrol-label">复核人</div>
									<div class="icontrols icontrols-text" id="checkerInfoDiv2"></div>
								</div>
								<div class="icontrol-group">
									<div class="icontrol-label">复核时间</div>
									<div class="icontrols icontrols-text" id="reviewTimeDiv"></div>
								</div>
								<div class="icontrol-group">
									<div class="icontrol-label">典型案例</div>
									<div class="icontrols icontrols-text" id="recordTypeDiv2">
									</div>
								</div>
								<div class="icontrol-group" id="caseItemTip2">
									<div class="icontrol-label">质检案例</div>
									<div class="icontrols" id="caseItemDiv2">
									</div>
								</div>
								
						
								
								<div class="icontrol-group">
									<div class="icontrol-label">问题描述</div>
									<div class="icontrols" id="commentsDiv2">
									</div>
								</div>
								
							</div>
						</div>
					</div>

					<div class="container-sqm1">
						<div class="heading">
							<i class="icon"></i>
							<div class="title">申诉记录</div>
						</div>
						<div class="contentpane">
							<div class="iform-horizontal labelmin">
								<div id="appealPaperDiv">
								</div>
							</div>
						</div>
					</div>

					<div class="container-sqm1" style="display:none;" id="checkContainer">
						<div class="heading">
							<i class="icon"></i>
							<div class="title">评价</div>
						</div>
						<div class="contentpane">
							<form id="scoreForm" class="easyui-form" method="post">
								<div class="iform-horizontal labelmin">
									<div class="icontrol-group">
										<div class="icontrol-label"><i class="required">*</i>星级</div>
										<div class="icontrols">
										<div class="starability-basic">
											<input type="radio" id="rate5-1" name="checkScore" value="5" />
											<label for="rate5-1">5 stars</label>

											<input type="radio" id="rate4-1" name="checkScore" value="4" />
											<label for="rate4-1">4 stars</label>

											<input type="radio" id="rate3-1" name="checkScore" value="3" />
											<label for="rate3-1">3 stars</label>

											<input type="radio" id="rate2-1" name="checkScore" value="2" />
											<label for="rate2-1">2 stars</label>

											<input type="radio" id="rate1-1" name="checkScore" value="1" />
											<label for="rate1-1">1 star</label>
										</div>

										</div>
									</div>
									<div class="icontrol-group">
										<div class="icontrol-label"><i class="required">*</i>评语</div>
										<div class="icontrols">
											<input name="checkInfo" id="checkInfo" class="easyui-textbox" data-options="multiline:true,width:'100%',height:'60px',validType:['length[1,500]']" />
										</div>
									</div>
									<div class="icontrol-group">
										<div class="icontrol-label"></div>
										<div class="icontrols">
											<button class="easyui-linkbutton l-btn-main" type="button" onclick="checkscore.saveScore()">提交</button>
										</div>
									</div>
								</div>
								<input id="checkStatus" type="hidden" name="checkStatus" value="1" />
							</form>
						</div>
					</div>
				</div>
			</div>

		</div>
	</div>
</div>
</body>
</html>