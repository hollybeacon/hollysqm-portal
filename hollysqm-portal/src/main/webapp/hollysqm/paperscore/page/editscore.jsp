<%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8" %>
<%@ include file="/hollybeacon/common/taglibs.jsp" %>
<!DOCTYPE html>
<html lang="zh-CN">
<head>
<%@ include file="/hollybeacon/common/meta.jsp" %>
<title>修改评分</title>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type="text/javascript" src="${ctx}/hollysqm/common/muplayer/dist/player.min.js"></script>
<script type="text/javascript" src="${ctx}/hollysqm/common/jquery-ui/jquery-ui-1.10.3.mouse_core.js"></script>
<script type="text/javascript" src="${ctx}/hollysqm/common/jquery-ui-touch-punch/jquery.ui.touch-punch.min.js"></script>
<script type="text/javascript" src="${ctx}/hollysqm/common/bootstrap/js/bootstrapslider.min.js"></script>
<script src="${ctx}/hollysqm/paperscore/js/editscore.js"></script>
<script>
// 获取必要的数据字典MAP		
var satisfactionMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'SATISFACTION'}});
var serviceTypeMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'BUSINESS_TYPE'}});
var caseItemMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'CASE_ITEM'}});
var dataTypeMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'DATATYPE'}});
var stateMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'STATUS'}});
var taskTimerMap=$("#planlistdatagrid").datagrid("getdictnames",{'param':{'codeType':'TASK_TIMER'}});
$(function(){
	editscore.init();
});
</script>
</head>
<body class="easyui-layout">
<div data-options="region:'center',border:false">

	<div class="container-sqm cfit">  
		<div class="heading">
			<img class="pic" src="${ctx}/hollysqm/common/images/add.png" />
			<div class="title">修改</div>
			<span class="sublinkpane" id="planDetail"></span>
		</div>
		<div class="contentpane">
			<div class="scorepanel">
				<div class="scoreleft">
					<div class="container-sqm1">
						<div class="heading">
							<i class="icon"></i>
							<div class="title">接触记录</div>
						</div>
						<div class="contentpane" style="padding:15px;">
							<div class="iform-horizontal column-3 labelmin iform-text" id="getRecordDiv">
							</div>
							<div class="container-voicechat" id="voicechatDiv" style="margin-bottom:20px;display:none;">
								<!-- 语音波形图 -->
								<div class="voicechatbox" id="divScroll">
									<img src="${ctx}/hollysqm/common/images/loading-mask.gif" class="voiceloading" id="imgVoice">
								</div>
								<div class="myautoplayer">
									<span class="myautoplayer-play icon-play"></span>
									<span class="myautoplayer-volume icon-volume-up"></span>
									<div id="voice" class="progress slider">
										<div class="progress-bar" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style="width: 100%;"></div>
									</div>
								</div>
							</div>
							<div class="chatqa" id="chatqa">
							</div>
						</div>
						<%-- <jspinclude page="../../common/weigui.jsp" flush="true"/> --%>
					</div>
				</div>

				<div class="scoreright">
					
					<div class="container-sqm1">
						<div class="heading">
							<i class="icon"></i>
							<div class="title">修改评分</div>
						</div>
						<div class="contentpane">
							<form id="scoreForm" class="easyui-form" method="post">
								<div class="iform-horizontal labelmin">
									
									<div class="icontrol-group">
										<div class="icontrol-label">修改评分</div>
										<div class="icontrols icontrols-text">
											<ul class="scoreitemlist" id="scoreitemlist">
											</ul>
										</div>
									</div>
									<div class="icontrol-group">
										<div class="icontrol-label">得分</div>
										<div class="icontrols icontrols-text"><span style="color:#ff0000;" id="scoreDiv">100分</span></div>
									</div>
								
									<div class="icontrol-group">
										<div class="icontrol-label">典型案例</div>
										<div class="icontrols icontrols-text">
											<label><input id="recordType1" type="radio" name="recordType" value="1" onclick="editscore.recordTypeEvent('1')" /> 是</label>&nbsp;&nbsp;
											<label><input id="recordType2" type="radio" name="recordType" value="2" onclick="editscore.recordTypeEvent('2')" checked /> 否</label>
										</div>
									</div>
									
									<div class="icontrol-group">
										<div class="icontrol-label"><i class="required" id="caseItemRequired" style="display:none;">*</i>质检案例</div>
										<div class="icontrols">
											<select id="caseItem" name="caseItem" data-options="disabled:true,editable:false,width:'100%'"></select>
										</div>
									</div>
									
									<div class="icontrol-group">
										<div class="icontrol-label">质检员</div>
										<div class="icontrols" id="checkerInfoDiv">
										</div>
									</div>
									<div class="icontrol-group">
										<div class="icontrol-label">评分时间</div>
										<div class="icontrols" id="scoreTimeDiv">
										</div>
									</div>
									
									<div class="icontrol-group">
										<div class="icontrol-label">问题描述</div>
										<div class="icontrols">
											<input name="comments" id="comments" class="easyui-textbox" data-options="multiline:true,width:'100%',height:'60px',validType:['length[0,500]']" />
										</div>
									</div>
									<div class="icontrol-group">
										<div class="icontrol-label"></div>
										<div class="icontrols">
											<button class="easyui-linkbutton l-btn-main" type="button" onclick="editscore.editScore('1')">提交</button>
											
										</div>
									</div>
								</div>
							
								<input id="totalScore" type="hidden" name="totalScore" value="100">
								<input id="itemId" type="hidden" name="itemId" value="" />
								<input id="contactId" type="hidden" name="contactId" value="" />
								
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>



<div id="dlg-planinfo" class="easyui-dialog" style="padding:10px;"
		data-options="
			title:'查看计划详情',
			closed:true,
			width:750,
			height:'95%'
		">
</div>
</body>
</html>