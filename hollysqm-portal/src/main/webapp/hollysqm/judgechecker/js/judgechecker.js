var judgeChecker = {
	/**
	 * 初始化加载数据
	 */
	init:function(){

		$("#formDataType").combobox("init_extend",{'codeType':'DATATYPE','hasAllOption':false,"event":{
			onLoadSuccess:function(){
				judgeChecker.dataTypeData = $(this).combobox('getData');
				$(this).combobox('setValue',judgeChecker.dataTypeData[0].value);
			}
		}});
		$("#formappealState").combobox("init_filter",{'codeType':'QUALITY_STATUS',"filterValue":"0"});
		$("#formCheckStatus").combobox("init_extend",{'codeType':'CHECK_STATUS'});
		
		
		$("#formPlanId").combobox({
			url:holly.getPath()+'/rest/commonRest/queryPlan',
			valueField:'value',
			textField:'name',
			method:"post",
			loadFilter:function(data){
				var jsonArray=new Array();
//				var defaultVal = {"value":"","name":'全部'};
//				jsonArray.push(defaultVal);
				for(var o in data.content){
					jsonArray.push(data.content[o]);
				}
				return jsonArray;
			},onHidePanel : function(){
				var el = $(this);
				var value = el.combobox("getValue");
				if(!value)//如果没有匹配到值，则默认设置成空
					 el.combobox("setValue","");
			}
		});
		$("#judgeCheckerdatagrid").datagrid({
			fit:true,
			url:holly.getPath()+'/rest/judgeCheckerRest/getPaperList',
			method:'get',
			singleSelect : true,
			border:false,
			fitColumns:true,
			queryParams:{'dataType':'v8'},
			pagination: true,
			loadFilter: function(data){return (data.content)?data.content:data;},
			columns:[[
				{field:'dataType',title:'质检来源',width:50,formatter:judgeChecker.formatDataType},
				{field:'planName',title:'质检计划',width:100,formatter:judgeChecker.formatPlanName},
				{field:'caller',title:'来电号码',width:60},
				{field:'agentName',title:'坐席',width:80,formatter:judgeChecker.formatAgentName},
				{field:'totalScore',title:'评分得分',width:50},
				{field:'modifyTime',title:'质检时间',width:90},
				{field:'appealStatus',title:'质检状态',width:40,formatter:judgeChecker.formatState},
				{field:'reviewScore',title:'复核得分',width:60,formatter:judgeChecker.formatReviewScore},
				{field:'checkerName',title:'质检员',width:80,formatter:judgeChecker.formatCheckerName},
				{field:'checkScore',title:'评价',width:40,formatter:judgeChecker.formatCheckerScore},
				{field:'option',title:'操作',width:40,formatter:judgeChecker.formatOption}
			]],
			loadMsg:'数据加载中请稍后……',
			onLoadSuccess: function (data)  {
				if (data.rows.length == 0) {
					var body = $(this).data().datagrid.dc.body2;
					body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
				}
			}
		});
		$("#judgeCheckerdatagrid_i8").datagrid({
			fit:true,
			method:'get',
			singleSelect : true,
			border:false,
			fitColumns:true,
			pagination: true,
			closed:true,
			loadFilter: function(data){return (data.content)?data.content:data;},
			columns:[[
				{field:'dataType',title:'质检来源',width:50,formatter:judgeChecker.formatDataType},
				{field:'planName',title:'质检计划',width:100,formatter:judgeChecker.formatPlanName},
				{field:'caller',title:'来电号码',width:60},
				{field:'agentName',title:'坐席',width:75,formatter:judgeChecker.formatAgentName},
				{field:'totalScore',title:'评分得分',width:45},
				{field:'modifyTime',title:'质检时间',width:90},
				{field:'appealStatus',title:'质检状态',width:40,formatter:judgeChecker.formatState},
				{field:'reviewScore',title:'复核得分',width:45,formatter:judgeChecker.formatReviewScore},
				{field:'checkerName',title:'质检员',width:75,formatter:judgeChecker.formatCheckerName},
				{field:'closeType',title:'关闭方式',width:55,formatter:judgeChecker.formatCloseType},
				{field:'sessionType',title:'会话等级',width:55,formatter:judgeChecker.formatSessionType},
				{field:'checkScore',title:'评价',width:30,formatter:judgeChecker.formatCheckerScore},
				{field:'option',title:'操作',width:50,formatter:judgeChecker.formatOption}
			]],
			loadMsg:'数据加载中请稍后……',
			onLoadSuccess: function (data)  {
				if (data.rows.length == 0) {
					var body = $(this).data().datagrid.dc.body2;
					body.append('<div class="nodatagrid"><span class="text">没有数据</span></div>');
				}
			}
		});
	},

	
	/**
	 * 搜索
	 */
	search:function(){
		strReplace("agent");
		strReplace("checker");
		var param = holly.form2json($("#searchjudgeChecker"));
		if(judgeChecker.validateTime(param)){
			if(param.dataType=='v8'){
				if($("#judgeCheckerdatagrid").datagrid('getPanel').panel('options').closed){
					$("#judgeCheckerdatagrid_i8").datagrid('getPanel').panel('close');
					$("#judgeCheckerdatagrid").datagrid('getPanel').panel('open').panel('resize');
				}
				$("#judgeCheckerdatagrid").datagrid("search",{'param':param});
			}else if(param.dataType=='i8'){
				$("#judgeCheckerdatagrid_i8").datagrid('options').url = holly.getPath()+'/rest/judgeCheckerRest/getPaperList';
				if($("#judgeCheckerdatagrid_i8").datagrid('getPanel').panel('options').closed){
					$("#judgeCheckerdatagrid").datagrid('getPanel').panel('close');
					$("#judgeCheckerdatagrid_i8").datagrid('getPanel').panel('open').panel('resize');
				}
				$("#judgeCheckerdatagrid_i8").datagrid("search",{'param':param});
			}
		}
	},
	formatPlanName : function(val,row,index){//质检计划名称
        if(val == undefined || val =='' || val == null || val=='null') {
            val = '全文检索';
        }
        return '<span title='+val+'>'+val+'</span>';
	},
	// 质检来源
	formatDataType:function(val, row, index) {
		var res =dataTypeMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	},
	// 申诉状态
	formatState:function(val, row, index) {
		var res =qualityStatusMap[val]; //获取对应的数据
		return res;
	},
	// 坐席
	formatAgentName:function(val,row,index){ 
		//return val?"<span title='"+row.Agent + "(" +val+ ")"+"'>"+row.Agent + "(" + val+ ")"+"</span>":"";
		if(row.agentName == null || row.userCode == null) {
			return "";
		} else {
			return "<span>" + row.agentName + "(" + row.userCode + ")</span>";
		}
	},
	// 质检员
	formatCheckerName:function(val,row,index){
		//return val?"<span title='"+row.checker + "(" +val+ ")"+"'>"+row.checker + "(" + val+ ")"+"</span>":"";
		if(row.checkerName == null || row.checkerCode == null) {
			return "";
		} else {
			return "<span>" + row.checkerName + "(" + row.checkerCode + ")</span>";
		}
	},
	formatReviewScore:function(val,row,index){
		return val ?"<span class = 'review_score'>"+ val +"</span>":val; 
	},
	// 评价
	formatCheckerScore:function(val, row, index) {
		var res =checkScoreMap[val]; //获取对应的数据
		return res; 
	},
	// 操作
	formatOption:function(val,row,index){
		var	content	= "";
		//未评价
		if(row.checkStatus == 0){
			content += "<a href='javascript:void(0)' onclick=\"judgeChecker.checkScore('" + row.paperId + "')\" title='评价'>评价</a>&nbsp;&nbsp;";
		}
		content += "<a href='javascript:void(0)' onclick=\"judgeChecker.sqminfo('"+row.paperId+"')\" title='查看'>查看</a>";
		
		
		return content;
	},
	checkScore:function(paperId){
		var frameId = window.frameElement && window.frameElement.id || '';
		var url = holly.getPath()+'/hollysqm/paperscore/page/checkscore.jsp';
		url += '?paperId=' + paperId;
		url += '&frameId=' + frameId;
		// parent.main.addTab('iframecheckscore',url,'评价');
		window.location = url;
	},
	sqminfo:function(paperId){
		var url = holly.getPath()+"/hollysqm/paperscore/page/dlg-sqminfo.jsp";
		url += "?paperId=" + paperId;
		$('#dlg-sqminfo').dialog('open').dialog('refresh',url);
	},
	resetForm:function() {
		$('#searchjudgeChecker').form('reset');
		$('#formDataType').combobox('select', judgeChecker.dataTypeData[0].value);// 默认选中第一个
		$('#formCheckStatus').combobox('select', judgeChecker.checkStatusData[0].value);// 默认选中第一个
	},
	validateTime:function (params){
		var startTime = params.startTime;
		var endTime = params.endTime;
		
		if(startTime > endTime && startTime && endTime){
			$.messager.alert('提示','评分开始时间不能大于结束时间!');
			return false;
		}
		/*
		var startTimeLon = new Date(startTime).getTime();
		var lastMonthLon = new Date(endTime).getTime();
		var timeLon = (lastMonthLon - startTimeLon) / (1000*60*60*24);
		if(timeLon > 31){
			$.messager.alert('提示','评分开始时间和结束时间不能超过31天!');
			return false;
		}*/
		return true;
	},
	formatCloseType:function(val, row, index){
		var res = closeTypeMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	},
	formatSessionType:function(val, row, index){
		var res = sessionTypeMap[val]; //获取对应的数据
		return "<span title='"+res+"'>"+res+"</span>";
	}
};

/**
*	过滤特殊字符
*	str：要进行过滤的字符串
*/
function strReplace(id){
	var pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？]");
	var rs = "";
	var str = $("#"+id).textbox("getValue");
	if($.trim(str).length > 0 ){
		for (var i = 0; i < str.length; i++) {
			rs = rs+str.substr(i, 1).replace(pattern, '');
		}
		$("#"+id).textbox("setValue",rs);
	}
}
 