<%@ page language="java" contentType="text/html; charset=UTF-8" errorPage="/hollybeacon/common/error.jsp" pageEncoding="UTF-8"%>
<%@ include file="/hollybeacon/common/taglibs.jsp"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>评价质检员-待评价</title>
<%@ include file="/hollybeacon/common/meta.jsp"%>
<link rel="stylesheet" href="${ctx}/hollysqm/common/css/sqm.css">
<script type="text/javascript" src="../js/judgechecker.js" ></script>
<script type= "text/javascript" src="${ctx}/hollybeacon/resources/ui/web/js/jquery.easyui.extend.js"></script>
<script type="text/javascript">
// 获取必要的数据字典MAP		
var dataTypeMap=$("#judgeCheckerdatagrid").datagrid("getdictnames",{'param':{'codeType':'DATATYPE'}}); 
var checkScoreMap=$("#paperScoredatagrid").datagrid("getdictnames",{'param':{'codeType':'CHECK_SCORE'}});
var qualityStatusMap=$("#judgeCheckerdatagrid").datagrid("getdictnames",{'param':{'codeType':'QUALITY_STATUS'}});
var checkStatusMap = $("#judgeCheckerdatagrid").datagrid("getdictnames", {'param':{'codeType':'CHECK_STATUS'}});
var closeTypeMap=$("#judgeCheckerdatagrid_i8").datagrid("getdictnames",{'param':{'codeType':'CLOSE_TYPE'}});
var sessionTypeMap=$("#judgeCheckerdatagrid_i8").datagrid("getdictnames",{'param':{'codeType':'SESSION_TYPE'}});
$(function() {
	judgeChecker.init();
	$("#searchjudgeChecker").keyup(function(event){
		if(event.keyCode==13){
			judgeChecker.search();
		}
	});
}); 

function search(){
	judgeChecker.search();
}
</script> 
<style type="text/css">
/*复核得分变成红色*/
.review_score{
	color: red;
}
</style>
</head>
<body class="easyui-layout">
	<div data-options="region:'center',border:false" style="padding:10px;">
		<div class="easyui-layout" data-options="fit:true">
			<!-- 搜索区域 -->
			<div data-options="region:'north',border:false" style="padding-bottom:10px;">
				<form id="searchjudgeChecker" method="post" class="search-form">
					<div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">质检来源</span>
							<div class="valueGroup">
								<select name="dataType" id="formDataType" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">质检计划</span>
							<div class="valueGroup">
									<select name="planId" id="formPlanId" data-options="width:'100%'"></select>
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">质检时间</span>
							<div class="valueGroup">
								<input type="text" name="startTime" class="easyui-datebox" data-options="editable:false,width:'100%'">
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">至</span>
							<div class="valueGroup">
								<input type="text" name="endTime" class="easyui-datebox" data-options="editable:false,width:'100%'">
							</div>
						</span>
					</div>
					<div class="screeningbar">
						<span class="screeningbar-item">
							<span class="label">质检状态</span>
							<div class="valueGroup">
								<select name="appealStatus" id="formappealState" class="easyui-combobox" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">坐席</span>
							<div class="valueGroup">
								<input type="text" name="agent" id="agent" class="easyui-textbox" data-options="editable:true,width:'100%'">
							</div>
						</span>
						<span class="screeningbar-item">
							<span class="label">质检员</span>
							<div class="valueGroup">
								<input type="text" name="checker" id="checker" class="easyui-textbox" data-options="editable:true,width:'100%'">
							</div>
						</span>	
						
						<span class="screeningbar-item">
							<span class="label">评价状态</span>
							<div class="valueGroup">
								<select name="checkStatus" id="formCheckStatus" class="easyui-combobox" data-options="editable:false,width:'100%'"></select>	
							</div>
						</span>	
					</div>
					<div class="btn-group">
						<a href="javascript:;" class="operation search-btn" onclick="judgeChecker.search()" title="搜索"></a>
						<a href="javascript:;" class="operation reset-btn" onclick="judgeChecker.resetForm();" title="重置"></a>
					</div>
				</form>
			</div>
			<!-- 搜索区域 end -->	
			<div data-options="region:'center',border:false">
				<div class="easyui-panel" title="评价列表" data-options="tools:'#tt',fit:true">
					<table id="judgeCheckerdatagrid"></table>
					<table id="judgeCheckerdatagrid_i8"></table>
				</div>
			</div>
		</div>
	</div>
		<div id="dlg-sqminfo" class="easyui-dialog" title="质检单信息"
		data-options="
			closed:true,
			width:'95%',
			height:'95%',
			draggable:false
		">
	</div>
</body>
</html>