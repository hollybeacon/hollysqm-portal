prompt PL/SQL Developer import file
prompt Created on 2016年3月18日 by tongbei
set feedback off
set define off
prompt Disabling triggers for TBL_SYS_AGENT_CODE...
alter table TBL_SYS_AGENT_CODE disable all triggers;
prompt Disabling triggers for TBL_SYS_AGENT_SKILL...
alter table TBL_SYS_AGENT_SKILL disable all triggers;
prompt Disabling triggers for TBL_SYS_AUTH_FUNCTION...
alter table TBL_SYS_AUTH_FUNCTION disable all triggers;
prompt Disabling triggers for TBL_SYS_AUTH_FUNC_PERMISSION...
alter table TBL_SYS_AUTH_FUNC_PERMISSION disable all triggers;
prompt Disabling triggers for TBL_SYS_CODE_TYPE...
alter table TBL_SYS_CODE_TYPE disable all triggers;
prompt Disabling triggers for TBL_SYS_SEQUENCE...
alter table TBL_SYS_SEQUENCE disable all triggers;
prompt Disabling triggers for TBL_SYS_CURRENT_MAX...
alter table TBL_SYS_CURRENT_MAX disable all triggers;
prompt Disabling triggers for TBL_SYS_DATA_PERMISSION...
alter table TBL_SYS_DATA_PERMISSION disable all triggers;
prompt Disabling triggers for TBL_SYS_DOMAIN...
alter table TBL_SYS_DOMAIN disable all triggers;
prompt Disabling triggers for TBL_SYS_FUNCTION...
alter table TBL_SYS_FUNCTION disable all triggers;
prompt Disabling triggers for TBL_SYS_FUNC_PERMISSION...
alter table TBL_SYS_FUNC_PERMISSION disable all triggers;
prompt Disabling triggers for TBL_SYS_GENERAL_CODE...
alter table TBL_SYS_GENERAL_CODE disable all triggers;
prompt Disabling triggers for TBL_SYS_GROUP...
alter table TBL_SYS_GROUP disable all triggers;
prompt Disabling triggers for TBL_SYS_LOCKUSER_INFO...
alter table TBL_SYS_LOCKUSER_INFO disable all triggers;
prompt Disabling triggers for TBL_SYS_MEDIA_FILE...
alter table TBL_SYS_MEDIA_FILE disable all triggers;
prompt Disabling triggers for TBL_SYS_ORG...
alter table TBL_SYS_ORG disable all triggers;
prompt Disabling triggers for TBL_SYS_PARAMETER...
alter table TBL_SYS_PARAMETER disable all triggers;
prompt Disabling triggers for TBL_SYS_PRIVATE_KEY...
alter table TBL_SYS_PRIVATE_KEY disable all triggers;
prompt Disabling triggers for TBL_SYS_ROLE...
alter table TBL_SYS_ROLE disable all triggers;
prompt Disabling triggers for TBL_SYS_ROLE_ASSIGN...
alter table TBL_SYS_ROLE_ASSIGN disable all triggers;
prompt Disabling triggers for TBL_SYS_ROLE_FUNC...
alter table TBL_SYS_ROLE_FUNC disable all triggers;
prompt Disabling triggers for TBL_SYS_SKILL...
alter table TBL_SYS_SKILL disable all triggers;
prompt Disabling triggers for TBL_SYS_SUB_SYSTEM...
alter table TBL_SYS_SUB_SYSTEM disable all triggers;
prompt Disabling triggers for TBL_SYS_THIRD_ACCOUNT...
alter table TBL_SYS_THIRD_ACCOUNT disable all triggers;
prompt Disabling triggers for TBL_SYS_TREE_CODE...
alter table TBL_SYS_TREE_CODE disable all triggers;
prompt Disabling triggers for TBL_SYS_USER...
alter table TBL_SYS_USER disable all triggers;
prompt Disabling triggers for TBL_SYS_USER_AGENT...
alter table TBL_SYS_USER_AGENT disable all triggers;
prompt Disabling triggers for TBL_SYS_USER_GROUP...
alter table TBL_SYS_USER_GROUP disable all triggers;
prompt Disabling foreign key constraints for TBL_SYS_CURRENT_MAX...
alter table TBL_SYS_CURRENT_MAX disable constraint FK4CF6F02BED6C63C6;
prompt Disabling foreign key constraints for TBL_SYS_USER...
alter table TBL_SYS_USER disable constraint FK19841D9E44775C;
alter table TBL_SYS_USER disable constraint FK19841D9E677C0E5D;
prompt Disabling foreign key constraints for TBL_SYS_USER_AGENT...
alter table TBL_SYS_USER_AGENT disable constraint FK47249D044AFA37E2;
alter table TBL_SYS_USER_AGENT disable constraint FK47249D048ED4800A;
prompt Disabling foreign key constraints for TBL_SYS_USER_GROUP...
alter table TBL_SYS_USER_GROUP disable constraint FK477E507E4817E7BD;
alter table TBL_SYS_USER_GROUP disable constraint FK477E507E4AFA37E2;
prompt Deleting TBL_SYS_USER_GROUP...
delete from TBL_SYS_USER_GROUP;
commit;
prompt Deleting TBL_SYS_USER_AGENT...
delete from TBL_SYS_USER_AGENT;
commit;
prompt Deleting TBL_SYS_USER...
delete from TBL_SYS_USER;
commit;
prompt Deleting TBL_SYS_TREE_CODE...
delete from TBL_SYS_TREE_CODE;
commit;
prompt Deleting TBL_SYS_THIRD_ACCOUNT...
delete from TBL_SYS_THIRD_ACCOUNT;
commit;
prompt Deleting TBL_SYS_SUB_SYSTEM...
delete from TBL_SYS_SUB_SYSTEM;
commit;
prompt Deleting TBL_SYS_SKILL...
delete from TBL_SYS_SKILL;
commit;
prompt Deleting TBL_SYS_ROLE_FUNC...
delete from TBL_SYS_ROLE_FUNC;
commit;
prompt Deleting TBL_SYS_ROLE_ASSIGN...
delete from TBL_SYS_ROLE_ASSIGN;
commit;
prompt Deleting TBL_SYS_ROLE...
delete from TBL_SYS_ROLE;
commit;
prompt Deleting TBL_SYS_PRIVATE_KEY...
delete from TBL_SYS_PRIVATE_KEY;
commit;
prompt Deleting TBL_SYS_PARAMETER...
delete from TBL_SYS_PARAMETER;
commit;
prompt Deleting TBL_SYS_ORG...
delete from TBL_SYS_ORG;
commit;
prompt Deleting TBL_SYS_MEDIA_FILE...
delete from TBL_SYS_MEDIA_FILE;
commit;
prompt Deleting TBL_SYS_LOCKUSER_INFO...
delete from TBL_SYS_LOCKUSER_INFO;
commit;
prompt Deleting TBL_SYS_GROUP...
delete from TBL_SYS_GROUP;
commit;
prompt Deleting TBL_SYS_GENERAL_CODE...
delete from TBL_SYS_GENERAL_CODE;
commit;
prompt Deleting TBL_SYS_FUNC_PERMISSION...
delete from TBL_SYS_FUNC_PERMISSION;
commit;
prompt Deleting TBL_SYS_FUNCTION...
delete from TBL_SYS_FUNCTION;
commit;
prompt Deleting TBL_SYS_DOMAIN...
delete from TBL_SYS_DOMAIN;
commit;
prompt Deleting TBL_SYS_DATA_PERMISSION...
delete from TBL_SYS_DATA_PERMISSION;
commit;
prompt Deleting TBL_SYS_CURRENT_MAX...
delete from TBL_SYS_CURRENT_MAX;
commit;
prompt Deleting TBL_SYS_SEQUENCE...
delete from TBL_SYS_SEQUENCE;
commit;
prompt Deleting TBL_SYS_CODE_TYPE...
delete from TBL_SYS_CODE_TYPE;
commit;
prompt Deleting TBL_SYS_AUTH_FUNC_PERMISSION...
delete from TBL_SYS_AUTH_FUNC_PERMISSION;
commit;
prompt Deleting TBL_SYS_AUTH_FUNCTION...
delete from TBL_SYS_AUTH_FUNCTION;
commit;
prompt Deleting TBL_SYS_AGENT_SKILL...
delete from TBL_SYS_AGENT_SKILL;
commit;
prompt Deleting TBL_SYS_AGENT_CODE...
delete from TBL_SYS_AGENT_CODE;
commit;
prompt Loading TBL_SYS_AGENT_CODE...
prompt Table is empty
prompt Loading TBL_SYS_AGENT_SKILL...
prompt Table is empty
prompt Loading TBL_SYS_AUTH_FUNCTION...
prompt Table is empty
prompt Loading TBL_SYS_AUTH_FUNC_PERMISSION...
prompt Table is empty
prompt Loading TBL_SYS_CODE_TYPE...
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('XXXX_TYPE', null, 'SYS', 'DICTIONARY', '资源管理', '大大的有', 0, 'HollyRest', 1, null, null, '2015-10-15 18:00:56', null, '2015-11-03 18:29:56', null, '40287d83506ae2d901506af203ac0004', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('CERTIFICATE_TYPE', null, 'SYS', 'DICTIONARY', '系统管理', '证件类型', 1, 'HollyPersonoa', 1, null, null, '2015-06-18 16:26:31', null, null, null, '2c9080ca4e0544ac014e05c6cb010010', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('SEX', null, 'SYS', 'DICTIONARY', '系统管理', '性别', 1, 'HollyPersonoa', 1, null, null, '2015-06-18 16:22:38', null, '2015-06-24 15:54:22', null, '2c9080ca4e0544ac014e05c3400f0008', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('DEGREE', null, 'SYS', 'DICTIONARY', '系统管理', '学历', 1, 'HollyPersonoa', 1, null, null, '2015-06-19 17:23:52', null, null, null, '2c9080ca4e0544ac014e05c4c3ac000b', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('NOTICE_STATUS', null, 'SYS', 'DICTIONARY', '系统管理', '通知状态', 1, 'HollyPersonoa', 1, null, null, '2015-07-27 10:33:42', null, null, null, 'ff8080814ebe80a0014ecd5bccb6077e', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('TRUE_FALSE', null, 'SYS', 'DICTIONARY', '系统管理', '真假', 1, 'HollyPersonoa', 1, null, null, '2015-07-24 17:12:18', null, null, null, 'ff8080814ebe80a0014ebf55a5e003f8', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('ROLE_TYPE', null, 'SYS', 'DICTIONARY', '系统管理', '角色类别', 1, 'HollyPersonoa', 1, null, null, null, null, '2015-06-24 20:01:35', null, '2c9080ca4e25429c014e2571dced03b7', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('NOTICE_TYPE', null, 'SYS', 'DICTIONARY', '系统管理', '通知类型', 1, 'HollyPersonoa', 1, null, null, null, null, '2015-07-21 01:37:01', null, '00000000000001', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('SysConfig', null, 'SYS', 'BUSINESS', '系统管理', '平台系统参数', 1, 'HollyPersonoa', 1, null, null, '2015-07-27 15:43:48', null, null, null, 'ff8080814ece7432014ece77b68d0000', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('EMERGENCY_TYPE', null, 'SYS', 'DICTIONARY', '系统管理', '紧急程度', 1, 'HollyPersonoa', 1, null, null, null, null, '2015-07-03 17:03:07', null, '2c9080ca4e448b31014e448d25d50048', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('YES_NO', null, 'SYS', 'DICTIONARY', '系统管理', '逻辑', 1, 'HollyPersonoa', 1, null, null, null, null, '2015-07-21 15:37:28', null, '2c9080d34eae7d9c014eaf8bc0dc0879', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('ss', null, 'UNICOM', 'BUSINESS', '生产管理', 'sss', 0, 'HollyProduct', 1, null, '100000', '2016-01-20 15:58:18', '100000', '2016-01-26 12:52:59', null, '2c90801f525e068c01525e0a978e0001', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('12', null, 'UNICOM', 'BUSINESS', '运营管理', '12', 0, 'HollyBusiness', 0, null, '100000', '2016-01-26 12:58:42', '100000', '2016-01-26 12:58:46', null, '2c908029527c132201527c4c4ff80006', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('SZW_TYPE_ONE', null, 'SYS', 'DICTIONARY', '工单系统', '石泽葳再测试', 0, 'HollyCase', 1, null, null, '2015-10-23 09:58:55', null, '2015-10-23 09:58:55', null, '402881e650926a150150926b944a0001', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('cy_test', null, 'UNICOM', 'DICTIONARY', '工单系统', '陈瑶测试', 0, 'HollyCase', 1, null, '100000', '2016-01-27 10:33:27', null, '2016-01-27 10:33:27', null, '2c9080295280eb90015280edb4130001', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('RESOURCE_TYPE', null, 'SYS', 'DICTIONARY', '资源管理', '资源类型', 0, 'HollyRest', 1, null, null, '2015-10-15 17:53:11', null, '2015-10-15 17:53:11', null, '40287d83506ae2d901506aeae9260001', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('xxxx', null, 'SYS', 'DICTIONARY', '生产管理', 'xxx7yyy', 0, 'HollyProduct', 1, null, null, '2015-10-22 16:06:40', null, '2015-10-22 16:06:40', null, 'ff808081508dbb0101508e95e7b40835', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('SZW_TEST_TYPE', null, 'UNICOM', 'DICTIONARY', '工单系统', '石泽葳测试数据字典数据字典数据字典', 0, 'HollyCase', 1, null, null, '2015-10-23 09:17:11', '100000', '2016-01-27 09:17:31', null, '402881e650923b120150924561c60001', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('ad', null, 'SYS', 'DICTIONARY', '客户服务1', '我问问', 0, 'HollyCustom', 1, null, null, '2015-11-03 17:34:35', null, '2015-11-03 17:34:35', null, '2c90802950cc80750150ccb2b46f000c', null);
commit;
prompt 19 records loaded
prompt Loading TBL_SYS_SEQUENCE...
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('workTimeRowId', '工作时间ID', '<rule><solidString content=""/><argument/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('autoSendId', '自动派单ID', '<rule><solidString content="AS"/><date format="yyMMddHHmmss"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('smsSendId', '短信发送ID', '<rule><solidString content="SS"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('commonContactLinkId', '通用任务接触记录主键ID', '<rule><solidString content="CCL"/><date format="yyyyMMddHHmmssSSS"/><randNum length="3"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('contactRecordId', '接触记录主键ID', '<rule><solidString content="CR"/><date format="yyyyMMddHHmmssSSS"/><randNum length="3"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('returnRecordId', '回访记录ID', '<rule><solidString content="RR"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('determineRecordId', '定位记录ID', '<rule><solidString content="DR"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('cpmSheetProcessId', '工单过程记录ID', '<rule><solidString content="CSP"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('cpmSheetLinkMonitorId', '环节监控表ID', '<rule><solidString content="LM"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('moonrise', '测试生成规则', '<rule><solidString content="MN"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('qualityTestTaskPlanId', '计划任务ID', '<rule><solidString content="ZJ"/><argument/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('treeRelTree', '树关联树表行ID', '<rule><solidString content="TRT"/><date format="yyyyMMddHHmmssSSS"/><randNum length="5"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('halfWayNote', '中途意见ID', '<rule><solidString content="HWN"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('qualityTestSheetId', '质检单ID', '<rule><solidString content="ZJ"/><argument/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('scoreRule', '分层分级中的指标项分数rowId', '<rule><argument/><date format="yyyyMMddHHmm"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('dealUserStatusID', '处理人员状态ID', '<rule><solidString content="DU"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('cpmSheetSpId', 'SP信息记录ID', '<rule><solidString content="SP"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('cpmSheetTotalMonitorId', '整体时限监控表ID', '<rule><solidString content="TM"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('cpmRelationSheetId', '关联工单记录ID', '<rule><solidString content="RS"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('cpmSheetSmsId', '工单短信记录ID', '<rule><solidString content="SMS"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('sendMessageSignId', '消息发送标识', '<rule><solidString content="MS"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('earlyWarningRowId', '预警提醒Id', '<rule><solidString content="YJ"/><argument/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('functionId', '菜单ID', '<rule><solidString content="FUN"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, null);
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('treeId', '树ID生成器', '<rule><solidString content="TREE"/><date format="yyyyMMddHHmmssSSS"/></rule>', 'Tbl_Class_Tree', null);
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('coderelatedId', '类型关联ID', '<rule><solidString content="TREE"/><date format="yyyyMMddHHmmssSSS"/></rule>', 'TBL_CODE_TYPE_RELATE', null);
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('reserveSheetCode', '预约工单流水号', '<rule><solidString content="YYG"/><date format="yyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('superviseSheetMsgRowId', '升级管办单短信ID', '<rule><solidString content="SM"/><argument/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('tempRefId', '模板引用ID', '<rule><solidString content="TR"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('tempFormCustomId', '模板定制ID', '<rule><solidString content="TFC"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('entityAttrID', '实体属性表中主键ID', '<rule><solidString content="EA"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('tempItemId', '模板控件ID', '<rule><solidString content="TI"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('tempButtonId', '模板按钮ID', '<rule><solidString content="TB"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('tempFormId', '模板配置ID', '<rule><solidString content="TF"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('macroSubId', '宏信息ID', '<rule><solidString content="MACS"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('macroId', '宏分类ID', '<rule><solidString content="MAC"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('dataSourceElementId', '数据源属性ID', '<rule><solidString content="DSE"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('entityId', '实体ID生成器', '<rule><solidString content="BE"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('entityDataId', '实体数据Id', '<rule><solidString content="MN"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('dataSourceId', '数据来源ID', '<rule><solidString content="DS"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('spManagerId', 'SP信息管理ID', '<rule><solidString content="SP"/><argument/><date format="yyyyMMddHHmmss"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('spInfoId', 'SP投诉内容ID', '<rule><solidString content="SP"/><argument/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('problemManage', '问题管理配置ID', '<rule><solidString content="PM"/><argument/><num length="5"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('qualityTestTemplateId', '质检模板ID', '<rule><solidString content="ZJ"/><argument/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('annexId', '附件ID', '<rule><solidString content="FJ"/><date format="yyyyMMddHHmmssSSS"/><num length="5"/></rule>' || chr(10) || '', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('hierarchy', '分层分级树Id', '<rule><date format="yyMMddHHmm"/><randNum length="5"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('dealtaskid', '工单任务ID', '<rule><solidString content="T"/><date format="yyMMddHHmmssSSS"/><randNum length="3"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('dimensionAttrId', '二维表指标ID', '<rule><solidString content="TDA"/><date format="yyyyMMddHHmmss"/><num length="5"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('nodeId', '我的收藏夹文件夹的nodeId', '<rule><date format="yyMMddHH"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('keyIndexId', '关键指标主键ID', '<rule><solidString content="KI"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('relationSheetRowId', '管理督办单关联工单ID', '<rule><solidString content="MN"/><argument/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('sheetErrorId', '工单差错ID', '<rule><solidString content="SE"/><date format="yyyyMMddHHmmssSSS"/><randNum length="5"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('caseId', '工单案例ID', '<rule><solidString content="CI"/><argument/><num length="5"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('operateLog', '操作日志ID', '<rule><solidString content="LG"/><date format="yyyyMMddHHmmssSSS"/><randNum length="3"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('specialNum', '特殊号码ID', '<rule><solidString content="SN"/><argument/><num length="5"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('entityModifyAreaID', '实体修改区主键ID', '<rule><solidString content="EMRID"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('qualityTestItemId', '质检项ID', '<rule><solidString content="ZJ"/><argument/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('rulesId', '规则ID', '<rule><solidString content="RU"/><date format="yyyyMMddHHmmssSSS"/><num length="3"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('commonTaskDealId', '过程任务Id', '<rule><solidString content="CTD"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('qualityTestConditionId', '抽取条件ID', '<rule><solidString content="ZJ"/><argument/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('curTemplate', '固化模板ID', '<rule><solidString content="CT"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('taskCategoryFormId', '任务类别表单引用ID', '<rule><solidString content="TCF"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('remedyProduct', '补救产品ID', '<rule><solidString content="RP"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('processConfig', '流程配置生成规则', '<rule><solidString content="PC"/><date format="yyyyMMddHHmmssSSS"/><num length="3"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('taskCategoryId', '任务类别ID', '<rule><solidString content="TC"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('taskTemplateId', '任务类别模板ID', '<rule><solidString content="TT"/><date format="yyyyMMddHHmmssSSS"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('keyId', '关键字ID', '<rule><solidString content="KI"/><argument/><date format="yyMMddHHmmss"/><randNum length="2"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('entityVersionID', '实体版本号ID', '<rule><solidString content="EVID"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('qualityTemplateId', '质检模板与质检项关联表ID', '<rule><solidString content="ZJ"/<date format="yyyyMMddHHmmss"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('urgeId', '催单记录id', '<rule><solidString content="CDI"/><date format="yyyyMMddHHmmss"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('urgeRelateId', '催单记录关联ID', '<rule><solidString content="CDRI"/><date format="yyyyMMddHHmmss"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('favoriteId', '我的收藏夹ID', '<rule><solidString content="MF"/><argument/><num length="5"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('qualityTestScoreId', '质检分数ID', '<rule><solidString content="ZJ"/><argument/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('entityVersion', '实体版本号', '<rule><solidString content="v."/><argument/><argument/><argument/><argument/><num length="5"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('sheetCodeId', '工单流水号ID', '<rule><solidString content=""/><argument/><date format="yyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('commonTemplate', '常用模板ID', '<rule><solidString content="CT"/><argument/><date format="yyMMddHHmm"/><randNum length="5"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('cpmOperateLogId', '通用任务操作日志ID', '<rule><solidString content="COL"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('delayApplyId', '延时申请ID', '<rule><solidString content="DA"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('halfwayViewId', '中途意见ID', '<rule><solidString content="HV"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('urgeRecordDestinationId', '催单目的地ID', '<rule><solidString content="URD"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('urgeRecordId', '催单记录ID', '<rule><solidString content="UR"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('answerRecordId', '答复记录ID', '<rule><solidString content="AR"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('smsRecvId', '短信接收ID', '<rule><solidString content="SR"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('tapSheetConfigId', '阀门值管理督办单配置主键ID', '<rule><solidString content="TC"/><date format="yyMMddHHmmssSSS"/><randNum length="5"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('servicePlanId', '服务预案ID', '<rule><solidString content="SPI"/><date format="yyMMddHHmmss"/><randNum length="3"/></rule>', null, null);
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('smsConfigurationId', '短信配置ID', '<rule><solidString content="SCI"/><date format="yyyyMMddHHmmssSSS"/></rule>', null, 'HollyCPM');
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('timelyPromiseId', '即时承诺ID', '<rule><solidString content="TPI"/><date format="yyMMddHHmmssSSS"/></rule>', null, null);
insert into TBL_SYS_SEQUENCE (SEQUENCE_ID, SEQUENCE_NAME, SEQUENCE_RULE, MEMO, SUB_SYSTEM_ID)
values ('TSConfig', null, '<rule><solidString content="TS"/><date format="yyyyMMdd"/><num length="5"/></rule>', null, 'HollyCPM');
commit;
prompt 87 records loaded
prompt Loading TBL_SYS_CURRENT_MAX...
prompt Table is empty
prompt Loading TBL_SYS_DATA_PERMISSION...
insert into TBL_SYS_DATA_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, CTRL_TYPE, CTRL_VALUE, TARGET_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('2c9080295280eb9001528219fdd20005', 'UNICOM', null, 'ROLE', 'ff8080814fcec033014fd4ec6a9b0643', 'bySelf', null, 'MENU2', 1, null, null, '2016-01-27 16:01:27', null, null);
insert into TBL_SYS_DATA_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, CTRL_TYPE, CTRL_VALUE, TARGET_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('2c908029527c9dcf01527cc97e4f0003', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'bySelf', null, 'ff80808151b4985b0151b53891c8000e', 1, null, null, '2016-01-26 15:15:25', null, null);
commit;
prompt 2 records loaded
prompt Loading TBL_SYS_DOMAIN...
insert into TBL_SYS_DOMAIN (DOMAIN_ID, DOMAIN_NAME, DOMAIN_LOGO, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('UNICOM', '中国联通', null, 1, null, null, null, null, null);
commit;
prompt 1 records loaded
prompt Loading TBL_SYS_FUNCTION...
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c90802650936f520150937650480001', 'UNICOM', null, '内固定方式', 'HollyProduct', 'HollyProduct', null, '1', 'MODULE', null, 10, 0, null, null, null, 1, '222555', null, '2015-10-23 14:50:15', null, '2015-10-23 14:50:34', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808150cb63740150d6d9b4be0018', 'UNICOM', null, '试试', 'MODULE1', 'HollyCustom', '11', '2', 'MENU', null, 11, 1, null, null, null, 1, null, null, '2015-11-05 16:53:23', null, '2015-11-05 16:53:23', 'ss', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808150cb63740150d6d9b4be0019', 'UNICOM', null, '查看', 'ff80808150cb63740150d6d9b4be0018', 'HollyCustom', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, '不可操作！', null, '2015-11-05 16:53:23', null, '2015-11-05 16:53:23', 'ss:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151b4985b0151b537c3b2000c', 'UNICOM', null, '湖北工单管理视窗', 'MODULE3', 'HollyBusiness', '/hollybeacon/business/bigscreen/screen_module_03.jsp', '2', 'MENU', null, 4, 1, 1, null, null, 1, null, null, '2015-12-18 21:11:49', null, '2015-12-18 21:11:49', '99010104', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151b4985b0151b537c3b2000d', 'UNICOM', null, '查看', 'ff80808151b4985b0151b537c3b2000c', 'HollyBusiness', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, '不可操作！', null, '2015-12-18 21:11:49', null, '2015-12-18 21:11:49', '99010104:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('108', 'UNICOM', null, '删除业务通知', '96', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'NoticeInfo:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('109', 'UNICOM', null, '查看所有通知列表', '96', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'NoticeInfo:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('HollyCustom', 'UNICOM', null, '客户服务', null, 'HollyCustom', '/1/2', '0', 'SUBSYSTEM', null, 1, 0, null, null, null, 1, '客户服务1IE8', null, null, '100000', '2015-12-15 13:54:27', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('HollyProduct', 'UNICOM', null, '生产管理', null, 'HollyProduct', '/1/2', '0', 'SUBSYSTEM', null, 2, 0, null, null, null, 1, '生产管理IE8', null, null, null, '2015-10-26 14:38:49', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('HollyBusiness', 'UNICOM', null, '运营管理', null, 'HollyBusiness', '/1/2', '0', 'SUBSYSTEM', null, 2, 0, null, null, null, 1, '运营管理', null, null, null, '2015-10-26 14:38:36', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('HollyRest', 'UNICOM', null, '资源管理', null, 'HollyRest', '/1/2', '0', 'SUBSYSTEM', null, 5, 0, null, null, null, 0, null, null, null, null, '2015-10-23 14:48:21', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('HollyCase', 'UNICOM', null, '工单系统', null, 'HollyCase', '/1/2', '0', 'SUBSYSTEM', null, 6, 0, null, null, null, 0, null, null, null, null, '2015-10-23 14:47:59', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('1201', 'UNICOM', null, '日志查询', '120', 'HollyPersonoa', null, '3', 'BUTTON', null, 1, 0, null, null, null, 1, null, null, null, null, null, 'SysActionLog.view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('111', 'UNICOM', null, '分配人员', '2', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysUser:assign', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('96', 'UNICOM', null, '业务通知管理', '1', 'HollyPersonoa', '/rest/notice/view.html', '2', 'MENU', null, 8, 0, null, null, null, 1, null, null, null, null, null, null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('97', 'UNICOM', null, '我的业务通知', '1', 'HollyPersonoa', '/rest/noticeUser/view.html', '2', 'MENU', null, 9, 0, null, null, null, 1, null, null, null, null, null, null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('1', 'UNICOM', null, '平台管理', 'HollyPersonoa', 'HollyPersonoa', null, '1', 'MODULE', null, 1, 0, null, null, null, 1, '平台管理', null, null, null, '2015-10-23 14:05:46', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2', 'UNICOM', null, '部门人员管理', '1', 'HollyPersonoa', '/rest/org/view.html', '2', 'MENU', null, 1, 0, null, null, null, 1, null, null, null, null, '2015-09-17 16:54:57', 'SysUser', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('3', 'UNICOM', null, '角色权限管理', '1', 'HollyPersonoa', '/rest/role/view.html', '2', 'MENU', null, 2, 0, null, null, null, 1, 'wewqw', null, null, null, '2015-10-26 14:25:55', 'SysRole', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('4', 'UNICOM', null, '工作组管理', '1', 'HollyPersonoa', '/rest/group/view.html', '2', 'MENU', null, 3, 0, null, null, null, 1, null, null, null, null, '2015-09-17 16:22:45', 'SysGroup', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('120', 'UNICOM', null, '平台日志查询', '1', 'HollyPersonoa', '/hollybeacon/personoa/system/page/actionlog.jsp', '2', 'MENU', null, 4, 0, null, null, null, 1, null, null, null, null, '2015-09-17 16:23:03', 'SysActionLog', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('7', 'UNICOM', null, '系统参数管理', '1', 'HollyPersonoa', '/rest/codeType/view.html', '2', 'MENU', null, 5, 0, null, null, null, 1, null, null, null, null, '2015-09-17 16:24:05', 'SysConfig', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('8', 'UNICOM', null, '开发资源管理', '1', 'HollyPersonoa', '/rest/restIfs/view.html', '2', 'MENU', null, 7, 0, null, null, null, 1, null, null, null, null, '2015-09-17 16:23:22', 'SysResouce', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('3a', 'UNICOM', null, '前端开发帮助', '1', 'HollyPersonoa', '/hollybeacon/resources/ui/web/doc/start.html', '2', 'MENU', null, 11, 0, null, null, null, 1, null, null, null, null, null, 'SysHelp', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('HollyMenu', 'UNICOM', null, '功能菜单管理', '1', 'HollyPersonoa', '/rest/menu/view.html', '2', 'MENU', null, 10, 0, null, null, null, 1, null, null, null, null, '2015-09-14 10:35:22', 'SysMenu', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('MenuButton1', 'UNICOM', null, '查看菜单', 'HollyMenu', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysMenu:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('11', 'UNICOM', null, '新建部门', '2', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysOrg:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('12', 'UNICOM', null, '修改部门', '2', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysOrg:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('13', 'UNICOM', null, '停用部门', '2', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysOrg:delete', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('21', 'UNICOM', null, '新建角色', '3', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysRole:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('22', 'UNICOM', null, '修改角色', '3', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysRole:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('23', 'UNICOM', null, '停用角色', '3', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysRole:delete', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('41', 'UNICOM', null, '新建工作组', '4', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysGroup:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('42', 'UNICOM', null, '修改工作组', '4', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysGroup:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('43', 'UNICOM', null, '删除工作组', '4', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysGroup:delete', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('51', 'UNICOM', null, '新建技能组', '5', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 0, null, null, null, null, null, 'SysSkill:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('52', 'UNICOM', null, '修改技能组', '5', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 0, null, null, null, null, null, 'SysSkill:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('53', 'UNICOM', null, '删除技能组', '5', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 0, null, null, null, null, null, 'SysSkill:delete', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('61', 'UNICOM', null, '新建业务分组', '6', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 0, null, null, null, null, null, 'SysServiceGroup:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('62', 'UNICOM', null, '修改业务分组', '6', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 0, null, null, null, null, null, 'SysServiceGroup:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('63', 'UNICOM', null, '删除业务分组', '6', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 0, null, null, null, null, null, 'SysServiceGroup:delete', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('71', 'UNICOM', null, '新建数据字典分类', '7', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysCodeType:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('72', 'UNICOM', null, '修改数据字典分类', '7', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysCodeType:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('73', 'UNICOM', null, '停用数据字典分类', '7', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 0, null, null, null, null, null, 'SysCodeType:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('MODULE1', 'UNICOM', null, '模块2', 'HollyCustom', 'HollyCustom', null, '1', 'MODULE', null, 1, 0, null, null, null, 1, 'IE8下客服管理模块2', null, null, null, '2015-10-26 14:37:26', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('MENU1', 'UNICOM', null, '菜单1', 'MODULE1', 'HollyCustom', null, '2', 'MENU', null, 1, 0, null, null, null, 1, null, null, null, null, null, 'Menu1', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST4', 'UNICOM', null, '删除菜单1', 'MENU1', 'HollyCustom', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, '菜单1:delete', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST3', 'UNICOM', null, '修改菜单1', 'MENU1', 'HollyCustom', null, '3', 'BUTTON', null, 0, 0, null, null, null, 1, '修改', null, null, null, '2015-10-26 14:27:46', '菜单1:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST2', 'UNICOM', null, '新增菜单1', 'MENU1', 'HollyCustom', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, '菜单1:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST1', 'UNICOM', null, '查看菜单1', 'MENU1', 'HollyCustom', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, '菜单1:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST14', 'UNICOM', null, '删除菜单2', 'MENU2', 'HollyProduct', null, '3', 'BUTTON', null, 0, 0, null, null, null, 1, 'IE8下删除菜单', null, null, null, '2015-10-26 14:39:55', '菜单2:delete', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST13', 'UNICOM', null, '修改菜单2', 'MENU2', 'HollyProduct', null, '3', 'BUTTON', null, 0, 0, null, null, null, 1, 'IE8 修改菜单2', null, null, null, '2015-10-26 14:39:14', '菜单2:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST12', 'UNICOM', null, '新增菜单2', 'MENU2', 'HollyProduct', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, '菜单2:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST11', 'UNICOM', null, '查看菜单2', 'MENU2', 'HollyProduct', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, '菜单2:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('MENU2', 'UNICOM', null, '菜单2', 'MODULE2', 'HollyProduct', null, '2', 'MENU', null, 1, 0, null, null, null, 1, null, null, null, null, null, 'Menu2', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('MODULE2', 'UNICOM', null, '模块2', 'HollyProduct', 'HollyProduct', null, '1', 'MODULE', null, 1, 0, null, null, null, 1, '生产管理模块2', null, null, null, '2015-10-26 16:02:03', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST21', 'UNICOM', null, '查看菜单3', 'MENU3', 'HollyBusiness', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, '菜单3:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('MENU3', 'UNICOM', null, '组件展示', 'MODULE3', 'HollyBusiness', '/hollybeacon/business/bigscreen/screen_all_emp.jsp', '2', 'MENU', null, 2, 1, 1, null, null, 1, 'weww', null, null, '100000', '2015-12-14 20:47:05', '99010102', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('MODULE3', 'UNICOM', null, '大屏组件', 'HollyBusiness', 'HollyBusiness', null, '1', 'MODULE', null, 1, 0, null, null, null, 1, null, null, null, '100000', '2015-12-11 23:26:32', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST34', 'UNICOM', null, '删除菜单4', 'MENU4', 'HollyRest', null, '3', 'BUTTON', null, 0, 0, null, null, null, 1, '删除菜单', null, null, null, '2015-10-26 16:04:18', '菜单4:delete', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST33', 'UNICOM', null, '修改菜单4', 'MENU4', 'HollyRest', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, '菜单4:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST32', 'UNICOM', null, '新增菜单4', 'MENU4', 'HollyRest', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, '菜单4:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST31', 'UNICOM', null, '查看菜单4', 'MENU4', 'HollyRest', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, '菜单4:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('MENU4', 'UNICOM', null, '菜单4', 'MODULE4', 'HollyRest', null, '2', 'MENU', null, 1, 0, null, null, null, 1, null, null, null, null, null, 'Menu4', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('MODULE4', 'UNICOM', null, '模块4', 'HollyRest', 'HollyRest', null, '1', 'MODULE', null, 1, 0, null, null, null, 1, 'dddddddddd', null, null, null, '2015-10-26 14:24:26', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST44', 'UNICOM', null, '删除菜单5', 'MENU5', 'HollyCase', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, '菜单5:delete', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST43', 'UNICOM', null, '修改菜单5', 'MENU5', 'HollyCase', null, '3', 'BUTTON', null, 0, 0, null, null, null, 1, '修改', null, null, null, '2015-10-26 14:28:02', '菜单5:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST42', 'UNICOM', null, '新增菜单5', 'MENU5', 'HollyCase', null, '3', 'BUTTON', null, 0, 0, null, null, null, 1, '新增', null, null, null, '2015-10-26 14:28:19', '菜单5:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('TEST41', 'UNICOM', null, '查看菜单5', 'MENU5', 'HollyCase', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, '菜单5:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('MENU5', 'UNICOM', null, '菜单5', 'MODULE5', 'HollyCase', null, '2', 'MENU', null, 1, 0, null, null, null, 1, null, null, null, null, null, 'Menu5', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('MODULE5', 'UNICOM', null, '模块5', 'HollyCase', 'HollyCase', null, '1', 'MODULE', null, 1, 0, null, null, null, 1, '模块5', null, null, null, '2015-10-23 14:06:15', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('112', 'UNICOM', null, '分配角色成员', '3', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysRole:assign', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('113', 'UNICOM', null, '功能权限授权', '3', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysFuncPermission:save', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('114', 'UNICOM', null, '功能权限查看', '3', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysFuncPermission:funcView', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('116', 'UNICOM', null, '数据权限授权', '3', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysDataPermission:save', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('115', 'UNICOM', null, '数据权限查看', '3', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysDataPermission:dataView', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('104', 'UNICOM', null, '查看编码', '7', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysDictionary:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('105', 'UNICOM', null, '新建编码', '7', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysDictionary:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('106', 'UNICOM', null, '修改编码', '7', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysDictionary:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('107', 'UNICOM', null, '停用编码', '7', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysDictionary:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('HollyKnowledge', 'UNICOM', null, '知识库', null, 'HollyKnowledge', null, '1', 'SUBSYSTEM', null, 7, 0, null, null, null, 0, null, null, null, null, '2015-10-23 14:48:35', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('10', 'UNICOM', null, '查看部门人员', '2', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysUser:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('20', 'UNICOM', null, '查看角色', '3', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysRole:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('40', 'UNICOM', null, '查看工作组', '4', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysGroup:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('50', 'UNICOM', null, '查看技能组', '5', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 0, null, null, null, null, null, 'SysSkill:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('60', 'UNICOM', null, '查看业务分组', '6', 'HollyPersonoa', '/rest/serviceGroup/view.html', '3', 'BUTTON', null, null, 0, null, null, null, 0, null, null, null, null, null, 'SysServiceGroup:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('70', 'UNICOM', null, '查看数据字典', '7', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysCodeType:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('74', 'UNICOM', null, '查看系统参数', '7', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysCodeType:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('14', 'UNICOM', null, '新建人员', '2', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysUser:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('15', 'UNICOM', null, '修改人员', '2', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysUser:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('16', 'UNICOM', null, '停用人员', '2', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysUser:delete', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('75', 'UNICOM', null, '新建系统参数', '7', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysConfig:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('76', 'UNICOM', null, '修改系统参数', '7', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysConfig:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('77', 'UNICOM', null, '停用系统参数', '7', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysConfig:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('HollyPersonoa', 'UNICOM', null, '系统管理', null, 'HollyPersonoa', 'http://172.16.52.5/hollybeacon-personoa/rest/security/main', '0', 'SUBSYSTEM', null, 4, 0, null, null, null, 1, 'gggg', null, null, null, '2015-10-23 14:03:45', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('81', 'UNICOM', null, '新建账号', '8', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysUser:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('82', 'UNICOM', null, '删除帐号', '8', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysUser:delete', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('83', 'UNICOM', null, '分配REST', '8', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysUser:dist', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('84', 'UNICOM', null, '密钥列表', '8', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysPrivateKey:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('85', 'UNICOM', null, '添加密钥', '8', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysPrivateKey:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('86', 'UNICOM', null, '删除密钥', '8', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysPrivateKey:delete', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('87', 'UNICOM', null, '修改密钥', '8', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysPrivateKey:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('9', 'UNICOM', null, '任务引擎管理', '1', 'HollyPersonoa', '/rest/scheduler/view.html', '2', 'MENU', null, 6, 0, null, null, null, 1, null, null, null, null, '2015-09-17 16:24:38', 'Job', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('91', 'UNICOM', null, '添加任务', '9', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'Job:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('92', 'UNICOM', null, '删除任务', '9', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'Job:delete', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('93', 'UNICOM', null, '暂停任务', '9', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'Job:pause', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('94', 'UNICOM', null, '重启任务', '9', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'Job:resume', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('95', 'UNICOM', null, '查看任务', '9', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'Job:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('110', 'UNICOM', null, '更新我的业务通知状态', '97', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'NoticeUser:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('99', 'UNICOM', null, '新建业务通知', '96', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'NoticeInfo:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('100', 'UNICOM', null, '修改业务通知', '96', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'NoticeInfo:update', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('101', 'UNICOM', null, '查看我的业务通知', '97', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'NoticeUser:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('102', 'UNICOM', null, '发送业务通知', '96', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'NoticeUser:create', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('44', 'UNICOM', null, '分配成员', '4', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysGroup:assign', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('3ab', 'UNICOM', null, '查看', '3a', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, null, null, null, null, null, 'SysHelp:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c9080de51a4c6150151a50850430001', 'UNICOM', null, '智能导航接口访问视窗', 'MODULE3', 'HollyBusiness', '/hollybeacon/business/bigscreen/screen_module_02.jsp', '2', 'MENU', null, 6, 1, 1, null, null, 1, null, null, '2015-12-15 17:46:04', '2c9080de51a4c6150151a5098ca30003', '2015-12-18 21:13:15', '99010106', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c9080de51a4c6150151a50850440002', 'UNICOM', null, '查看', '2c9080de51a4c6150151a50850430001', 'HollyBusiness', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, '不可操作！', null, '2015-12-15 17:46:04', null, '2015-12-15 17:46:04', '99010106:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c9080de51a04e550151a088b2550001', 'UNICOM', null, '快速入门', 'MODULE3', 'HollyBusiness', '/hollybeacon/resources/ui/web/doc/echartsHelp.html', '2', 'MENU', null, 1, 0, 1, null, null, 1, null, null, '2015-12-14 20:48:11', '100000', '2015-12-15 10:16:28', '99010101', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c9080de51a04e550151a088b2620002', 'UNICOM', null, '查看', '2c9080de51a04e550151a088b2550001', 'HollyBusiness', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, '不可操作！', null, '2015-12-14 20:48:11', null, '2015-12-14 20:48:11', '99010101:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c9080f6505f318101505f36c9f70002', 'UNICOM', null, '菜单6', 'MODULE5', 'HollyCase', '/test.jsp', '2', 'MENU', null, 2, 1, null, null, null, 1, '嘎嘎嘎', null, '2015-10-13 11:20:37', null, '2015-10-23 14:57:21', 'MENU6', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c9080f6505f318101505f36c9f70003', 'UNICOM', null, '查看', '2c9080f6505f318101505f36c9f70002', 'HollyCase', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, '不可操作！', null, '2015-10-13 11:20:37', null, '2015-10-13 11:20:37', 'MENU6:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c9080de51a04e550151a089a4500003', 'UNICOM', null, '湖北整体运营视窗', 'MODULE3', 'HollyBusiness', '/hollybeacon/business/bigscreen/screen_module_01.jsp', '2', 'MENU', null, 3, 0, 1, null, null, 1, null, null, '2015-12-14 20:49:13', '2c9080de51a4c6150151a5098ca30003', '2015-12-18 21:12:53', '99010103', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c9080de51a04e550151a089a4510004', 'UNICOM', null, '查看', '2c9080de51a04e550151a089a4500003', 'HollyBusiness', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, '不可操作！', null, '2015-12-14 20:49:13', null, '2015-12-14 20:49:13', '99010103:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151b4985b0151b53891c8000e', 'UNICOM', null, '湖北工单分析视窗', 'MODULE3', 'HollyBusiness', '/hollybeacon/business/bigscreen/screen_module_04.jsp', '2', 'MENU', null, 5, 1, 1, null, null, 1, null, null, '2015-12-18 21:12:42', null, '2015-12-18 21:12:42', '99010105', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151b4985b0151b53891c8000f', 'UNICOM', null, '查看', 'ff80808151b4985b0151b53891c8000e', 'HollyBusiness', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, '不可操作！', null, '2015-12-18 21:12:42', null, '2015-12-18 21:12:42', '99010105:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151bbce290151cc82cd790012', 'UNICOM', null, '坐席监控', 'MODULE3', 'HollyBusiness', '/hollybeacon/business/bigscreen/screen_module_07.jsp', '2', 'MENU', null, 9, 1, 1, null, null, 1, null, null, '2015-12-23 09:45:03', null, '2015-12-23 09:45:03', '99010109', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151bbce290151cc82cd7a0013', 'UNICOM', null, '查看', 'ff80808151bbce290151cc82cd790012', 'HollyBusiness', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, '不可操作！', null, '2015-12-23 09:45:03', null, '2015-12-23 09:45:03', '99010109:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c908029537de1ec01537e2fc15a000b', 'UNICOM', null, '角色首页权限授权', '3', 'HollyPersonoa', null, '3', 'BUTTON', null, 0, 0, null, null, null, 1, null, '100000', '2016-03-16 14:49:32', null, '2016-03-16 14:49:32', 'SysIndexPermission:save', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c90801a5331d16c015331d1f9220000', 'UNICOM', null, '会话管理', '1', 'HollyPersonoa', '/rest/sessionManage/view', '2', 'MENU', null, 12, 0, null, null, null, 1, null, '100000', '2016-03-01 18:55:57', null, '2016-03-01 18:55:57', 'redisSession', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151bbce290151c4653f870007', 'UNICOM', null, '技能组监控', 'MODULE3', 'HollyBusiness', '/hollybeacon/business/bigscreen/screen_module_06.jsp', '2', 'MENU', null, 8, 1, 1, null, null, 1, null, null, '2015-12-21 19:55:48', '100000', '2015-12-23 09:44:22', '99010108', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151bbce290151c4653f870008', 'UNICOM', null, '查看', 'ff80808151bbce290151c4653f870007', 'HollyBusiness', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, '不可操作！', null, '2015-12-21 19:55:48', null, '2015-12-21 19:55:48', '990101:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c90801a5331d16c015331d1f92e0001', 'UNICOM', null, '查看', '2c90801a5331d16c015331d1f9220000', 'HollyPersonoa', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, '不可操作！', '100000', '2016-03-01 18:55:57', null, '2016-03-01 18:55:57', 'redisSession:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151bbce290151c3fabe400003', 'UNICOM', null, '湖南整体运营视窗', 'MODULE3', 'HollyBusiness', '/hollybeacon/business/bigscreen/screen_module_05.jsp', '2', 'MENU', null, 7, 1, 1, null, null, 1, null, null, '2015-12-21 17:59:28', null, '2015-12-21 17:59:28', '99010107', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151bbce290151c3fabe400004', 'UNICOM', null, '查看', 'ff80808151bbce290151c3fabe400003', 'HollyBusiness', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, '不可操作！', null, '2015-12-21 17:59:28', null, '2015-12-21 17:59:28', '99010107:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151bbce290151d0d5f007001f', 'UNICOM', null, '湖北工单综合视窗', 'HollyBusiness', 'HollyBusiness', null, '1', 'MODULE', null, 2, 0, null, null, null, 1, null, '100000', '2015-12-24 05:54:20', null, '2015-12-24 05:54:20', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151bbce290151d0d731940020', 'UNICOM', null, '客服视窗', 'ff80808151bbce290151d0d5f007001f', 'HollyBusiness', '/hollybeacon/business/bigscreen/case01_hb/screen_module_01.jsp', '2', 'MENU', null, 1, 1, 1, null, null, 1, null, null, '2015-12-24 05:55:42', null, '2015-12-24 05:55:42', '99010201', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151bbce290151d0d731940021', 'UNICOM', null, '查看', 'ff80808151bbce290151d0d731940020', 'HollyBusiness', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, '不可操作！', null, '2015-12-24 05:55:42', null, '2015-12-24 05:55:42', '99010201:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151bbce290151d0d7c6eb0022', 'UNICOM', null, '分公司视窗', 'ff80808151bbce290151d0d5f007001f', 'HollyBusiness', '/hollybeacon/business/bigscreen/case01_hb/screen_module_02.jsp', '2', 'MENU', null, 2, 1, 1, null, null, 1, null, null, '2015-12-24 05:56:20', null, '2015-12-24 05:56:20', '99010202', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c90802950cc5ba70150cc7f7a650008', 'UNICOM', null, '对对对', 'HollyCustom', 'HollyCustom', null, '1', 'MODULE', null, 2, 0, null, null, null, 1, '对对对', null, '2015-11-03 16:38:37', null, '2015-11-03 16:38:37', null, null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151bbce290151d0d7c6eb0023', 'UNICOM', null, '查看', 'ff80808151bbce290151d0d7c6eb0022', 'HollyBusiness', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, '不可操作！', null, '2015-12-24 05:56:20', null, '2015-12-24 05:56:20', '99010202:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151bbce290151d0d84a010024', 'UNICOM', null, '部门视窗', 'ff80808151bbce290151d0d5f007001f', 'HollyBusiness', '/hollybeacon/business/bigscreen/case01_hb/screen_module_03.jsp', '2', 'MENU', null, 3, 1, 1, null, null, 1, null, null, '2015-12-24 05:56:54', null, '2015-12-24 05:56:54', '99010203', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('ff80808151bbce290151d0d84a010025', 'UNICOM', null, '查看', 'ff80808151bbce290151d0d84a010024', 'HollyBusiness', null, '3', 'BUTTON', null, null, 0, null, null, null, 1, '不可操作！', null, '2015-12-24 05:56:54', null, '2015-12-24 05:56:54', '99010203:view', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c908029537de1ec01537df1f4fa0008', 'UNICOM', null, '角色首页权限查看', '3', 'HollyPersonoa', null, '3', 'BUTTON', null, 0, 0, null, null, null, 1, null, '100000', '2016-03-16 13:42:02', null, '2016-03-16 13:42:02', 'SysIndexPermission:indexView', null);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c9080e458b965640158b967ac300002', 'UNICOM', 'ORG000000', '系统监控', '1', 'HollyPersonoa', '/druid/websession.html', '2', 'MENU', NULL, '15', '1', NULL, NULL, NULL, '1', 'Druid内置的系统监控', '100000', '2016-12-01 16:02:09', '100000', '2016-12-01 16:22:50', 'Druid', NULL);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c9080e458b965640158b967ac340003', 'UNICOM', NULL, '查看', '2c9080e458b965640158b967ac300002', 'HollyPersonoa', NULL, '3', 'BUTTON', NULL, NULL, '0', NULL, NULL, NULL, '1', '不可操作！', '100000', '2016-12-01 16:02:09', NULL, '2016-12-01 16:02:09', 'Druid:view', NULL);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c9080e5581dc0c901581deefaff000c', 'UNICOM', 'ORG000000', '刷新缓存', '1', 'HollyPersonoa', '/rest/cacheFresh/view.html', '2', 'MENU', NULL, '10', '0', NULL, NULL, NULL, '1', NULL, '100000', '2016-11-01 11:29:10', NULL, '2016-11-01 11:29:10', 'CacheFresh', NULL);
insert into TBL_SYS_FUNCTION (FUNCTION_ID, DOMAIN_ID, ORG_ID, FUNCTION_NAME, PARENT_FUNCTION_ID, SUB_SYSTEM_ID, URL, VIEW_LEVEL_NO, FUNCTION_TYPE, SHORTCUT_KEY, SORT_NO, IS_SINGLETON, IS_NEW_WIN, IS_WORKBENCH, IS_CLOSED, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, PERMISSION_KEY, IS_SHOW)
values ('2c9080e5581dc0c901581deefb0d000d', 'UNICOM', 'ORG000000', '查看', '2c9080e5581dc0c901581deefaff000c', 'HollyPersonoa', NULL, '3', 'BUTTON', NULL, NULL, '0', NULL, NULL, NULL, '1', '不可操作！', '100000', '2016-11-01 11:29:10', NULL, '2016-11-01 11:29:10', 'CacheFresh:view', NULL);
commit;

prompt 143 records loaded
prompt Loading TBL_SYS_FUNC_PERMISSION...
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0312B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST11', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0322B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST21', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0332B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST34', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0342B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST33', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0352B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST32', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0362B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST31', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0372B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST44', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0382B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST43', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0392B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST42', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC03A2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST41', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC03B2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '112', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC03C2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '113', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC03D2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '114', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC03E2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '116', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC03F2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '115', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0402B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '104', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0412B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '105', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0422B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '106', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0432B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '107', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0442B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '10', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0452B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '20', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0462B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '40', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0472B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '70', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0482B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '74', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0492B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '14', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC04A2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '15', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC04B2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '16', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC04C2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '75', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC04D2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '76', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC04E2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '77', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC04F2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '81', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0502B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '82', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0512B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '83', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0522B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '84', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0532B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '85', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0542B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '86', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0552B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '87', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0562B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '91', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0572B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '92', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0582B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '93', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0592B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '94', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0192B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'ff80808151b4985b0151b537c3b2000d', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC05A2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '95', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC05B2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '110', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BD75311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'ff80808150cb63740150d6d9b4be0019', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BD85311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'ff80808151b4985b0151b537c3b2000d', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BD95311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '108', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BDA5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '109', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BDB5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '1201', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BDC5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '111', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BDD5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'MenuButton1', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BDE5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '11', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BDF5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '12', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BE05311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '13', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BE15311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '21', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BE25311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '22', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BE35311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '23', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BE45311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '41', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BE55311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '42', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BE65311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '43', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BE75311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '71', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BE85311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '72', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BE95311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST4', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9282E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '44', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C635311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '81', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C645311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '82', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C655311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '83', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C665311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '84', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C675311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '85', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C685311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '86', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C695311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '87', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C6A5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '91', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C6B5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '92', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C6C5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '93', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C6D5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '94', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C6E5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '95', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C6F5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '110', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C705311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '99', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C715311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '100', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C725311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '101', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C735311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '102', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C745311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '44', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C755311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '3ab', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C765311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '2c9080de51a4c6150151a50850440002', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C775311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '2c9080de51a04e550151a088b2620002', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C785311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '2c9080f6505f318101505f36c9f70003', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C795311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '2c9080de51a04e550151a089a4510004', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C7A5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'ff80808151b4985b0151b53891c8000f', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C7B5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'ff80808151bbce290151cc82cd7a0013', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C7C5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'ff80808151bbce290151c4653f870008', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C7D5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'ff80808151bbce290151c3fabe400004', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C7E5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'ff80808151bbce290151d0d731940021', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C7F5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'ff80808151bbce290151d0d7c6eb0023', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C805311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'ff80808151bbce290151d0d84a010025', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C815311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'ff80808150cb63740150d6d9b4be0019', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C825311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'ff80808151b4985b0151b537c3b2000d', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C835311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'TEST4', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C845311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'TEST3', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C855311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'TEST2', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C865311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'TEST1', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013335420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '108', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8E42E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '1201', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C875311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'TEST21', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C885311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'TEST34', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C895311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'TEST33', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C8A5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'TEST32', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C8B5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'TEST31', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C8C5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'TEST44', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C8D5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'TEST43', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C8E5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'TEST42', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C8F5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'TEST41', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C905311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', '2c9080de51a4c6150151a50850440002', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C915311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', '2c9080de51a04e550151a088b2620002', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C925311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', '2c9080f6505f318101505f36c9f70003', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C935311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', '2c9080de51a04e550151a089a4510004', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C945311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'ff80808151b4985b0151b53891c8000f', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C955311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'ff80808151bbce290151cc82cd7a0013', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C965311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'ff80808151bbce290151c4653f870008', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C975311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'ff80808151bbce290151c3fabe400004', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C985311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'ff80808151bbce290151d0d731940021', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C995311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'ff80808151bbce290151d0d7c6eb0023', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C9A5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bdfa9c50002', 'BUTTON', 'ff80808151bbce290151d0d84a010025', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C588A1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '102', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C588B1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '44', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C588C1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '3ab', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C588D1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '2c9080de51a4c6150151a50850440002', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C588E1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '2c9080de51a04e550151a088b2620002', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C588F1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '2c9080de51a04e550151a089a4510004', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58901393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', 'ff80808151b4985b0151b53891c8000f', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58911393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', 'ff80808151bbce290151cc82cd7a0013', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58921393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', 'ff80808151bbce290151c4653f870008', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58931393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', 'ff80808151bbce290151c3fabe400004', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58941393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', 'ff80808151bbce290151d0d731940021', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58951393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', 'ff80808151bbce290151d0d7c6eb0023', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58961393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', 'ff80808151bbce290151d0d84a010025', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A963A1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd24b7b90002', 'BUTTON', 'TEST34', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A963B1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd24b7b90002', 'BUTTON', 'TEST33', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A963C1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd24b7b90002', 'BUTTON', 'TEST32', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A963D1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd24b7b90002', 'BUTTON', 'TEST31', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A963E1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd24b7b90002', 'BUTTON', 'TEST44', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A963F1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd24b7b90002', 'BUTTON', 'TEST43', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96401395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd24b7b90002', 'BUTTON', 'TEST42', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96411395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd24b7b90002', 'BUTTON', 'TEST41', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96421395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd24b7b90002', 'BUTTON', '2c9080f6505f318101505f36c9f70003', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8E02E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'ff80808150cb63740150d6d9b4be0019', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8E12E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'ff80808151b4985b0151b537c3b2000d', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8E22E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '108', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8E32E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '109', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8E52E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '111', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8E62E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'MenuButton1', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8E72E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '11', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8E82E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '12', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8E92E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '13', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8EA2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '21', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8EB2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '22', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8EC2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '23', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8ED2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '41', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8EE2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '42', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8EF2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '43', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013345420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '109', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013355420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '1201', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013365420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '111', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013375420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', 'MenuButton1', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013385420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '11', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013395420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '12', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740133A5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '13', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740133B5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '21', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740133C5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '22', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740133D5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '23', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740133E5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '41', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740133F5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '42', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013405420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '43', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013415420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '71', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013425420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '72', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013435420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '112', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013445420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '113', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013455420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '114', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013465420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '116', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013475420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '115', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013485420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '104', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013495420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '105', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740134A5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '106', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740134B5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '107', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740134C5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '10', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740134D5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '20', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740134E5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '40', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740134F5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '70', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013505420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '74', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013515420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '14', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013525420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '15', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013535420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '16', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013545420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '75', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013555420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '76', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013565420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '77', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013575420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '81', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013585420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '82', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013595420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '83', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740135A5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '84', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740135B5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '85', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740135C5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '86', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
commit;
prompt 200 records committed...
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740135D5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '87', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740135E5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '91', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE8740135F5420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '92', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013605420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '93', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013615420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '94', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013625420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '95', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013635420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '110', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013645420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '99', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013655420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '100', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013665420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '101', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013675420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '102', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013685420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '44', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('20DE874013695420E0531240FADCE4C9', 'UNICOM', null, 'ROLE', 'ROLE000002', 'BUTTON', '3ab', 1, 1, null, 'admin@ORG000000', '29-SEP-15', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8F02E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '71', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8F12E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '72', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8F22E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST4', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8F32E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST3', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8F42E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST2', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8F52E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST1', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8F62E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST14', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8F72E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST13', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8F82E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST12', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8F92E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST11', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8FA2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST21', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8FB2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST34', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8FC2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST33', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8FD2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST32', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8FE2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST31', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE8FF2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST44', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9002E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST43', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9012E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST42', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9022E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'TEST41', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9032E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '112', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9042E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '113', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9052E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '114', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9062E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '116', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9072E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '115', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9082E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '104', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9092E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '105', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE90A2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '106', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE90B2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '107', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE90C2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '10', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE90D2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '20', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE90E2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '40', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE90F2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '70', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9102E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '74', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9112E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '14', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9122E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '15', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9132E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '16', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9142E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '75', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9152E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '76', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9162E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '77', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9172E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '81', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9182E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '82', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9192E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '83', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE91A2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '84', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE91B2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '85', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE91C2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '86', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE91D2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '87', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE91E2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '91', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE91F2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '92', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9202E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '93', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9212E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '94', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9222E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '95', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9232E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '110', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9242E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '99', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9252E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '100', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9262E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '101', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9272E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '102', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC01A2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '108', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC01B2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '109', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC01C2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '1201', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC01D2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '111', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC01E2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'MenuButton1', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC01F2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '11', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0202B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '12', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0212B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '13', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0222B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '21', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0232B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '22', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0242B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '23', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0252B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '41', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0262B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '42', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0272B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '43', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0282B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '71', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0292B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '72', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC02A2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST4', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC02B2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST3', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC02C2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST2', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC02D2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST1', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC02E2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST14', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC02F2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST13', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0302B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'TEST12', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96AE1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '75', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96AF1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '76', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96B01395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '77', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96B11395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '81', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96B21395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '82', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96B31395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '83', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96B41395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '84', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96B51395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '85', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96B61395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '86', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96B71395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '87', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96B81395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '91', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96B91395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '92', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96BA1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '93', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96BB1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '94', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96BC1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '95', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96BD1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '110', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96BE1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '99', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96BF1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '100', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96C01395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '101', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96C11395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '102', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96C21395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '44', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96C31395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '3ab', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96C41395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '2c9080de51a4c6150151a50850440002', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96C51395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '2c9080de51a04e550151a088b2620002', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96C61395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '2c9080de51a04e550151a089a4510004', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96C71395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'ff80808151b4985b0151b53891c8000f', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96C81395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'ff80808151bbce290151cc82cd7a0013', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96C91395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'ff80808151bbce290151c4653f870008', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96CA1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'ff80808151bbce290151c3fabe400004', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96CB1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'ff80808151bbce290151d0d731940021', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96CC1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'ff80808151bbce290151d0d7c6eb0023', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96CD1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'ff80808151bbce290151d0d84a010025', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340703B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '1201', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340713B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '111', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9292E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '3ab', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE92A2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '2c9080de51a4c6150151a50850440002', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE92B2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '2c9080de51a04e550151a088b2620002', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE92C2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '2c9080f6505f318101505f36c9f70003', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE92D2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', '2c9080de51a04e550151a089a4510004', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE92E2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'ff80808151b4985b0151b53891c8000f', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE92F2E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'ff80808151bbce290151cc82cd7a0013', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9302E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'ff80808151bbce290151c4653f870008', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9312E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'ff80808151bbce290151c3fabe400004', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9322E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'ff80808151bbce290151d0d731940021', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9332E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'ff80808151bbce290151d0d7c6eb0023', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A256A6CE9342E2CE0531240FADC43E8', 'UNICOM', null, 'ROLE', '2c90809f4ffd2243014ffd240a360001', 'BUTTON', 'ff80808151bbce290151d0d84a010025', 1, 1, null, 'admin@ORG100001', '25-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340733B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '11', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340743B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '12', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340753B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '13', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340763B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '21', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340773B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '22', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340783B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '23', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39D49DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d4597e00010', 'BUTTON', 'TEST31', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39E49DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d510d210012', 'BUTTON', 'TEST31', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39F49DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d510d210012', 'BUTTON', 'TEST32', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C3A049DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d510d210012', 'BUTTON', 'TEST33', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C3A149DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d510d210012', 'BUTTON', 'TEST34', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C3A249DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d5362aa0013', 'BUTTON', 'TEST1', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C3A349DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d5362aa0013', 'BUTTON', 'TEST2', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C3A449DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d5362aa0013', 'BUTTON', 'TEST3', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C3A549DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d5362aa0013', 'BUTTON', 'TEST4', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C3A649DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d5362aa0013', 'BUTTON', 'ff80808150cb63740150d6d9b4be0019', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A492AB78BB55A52E0531240FADC70AD', 'UNICOM', null, 'ROLE', 'ff8080814fcec033014fd4ed01600650', 'BUTTON', 'TEST1', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A492AB78BB65A52E0531240FADC70AD', 'UNICOM', null, 'ROLE', 'ff8080814fcec033014fd4ed01600650', 'BUTTON', 'TEST2', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A492AB78BB75A52E0531240FADC70AD', 'UNICOM', null, 'ROLE', 'ff8080814fcec033014fd4ed01600650', 'BUTTON', 'TEST3', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A492AB78BB85A52E0531240FADC70AD', 'UNICOM', null, 'ROLE', 'ff8080814fcec033014fd4ed01600650', 'BUTTON', 'TEST4', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A492AB78BB95A52E0531240FADC70AD', 'UNICOM', null, 'ROLE', 'ff8080814fcec033014fd4ed01600650', 'BUTTON', 'ff80808150cb63740150d6d9b4be0019', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A492D09C5DC5A50E0531240FADC1271', 'UNICOM', null, 'ROLE', '2c9080295280eb9001528111822d0002', 'BUTTON', 'TEST31', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A492D09C5DD5A50E0531240FADC1271', 'UNICOM', null, 'ROLE', '2c9080295280eb9001528111822d0002', 'BUTTON', 'TEST32', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A492D09C5DE5A50E0531240FADC1271', 'UNICOM', null, 'ROLE', '2c9080295280eb9001528111822d0002', 'BUTTON', 'TEST33', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A492D09C5DF5A50E0531240FADC1271', 'UNICOM', null, 'ROLE', '2c9080295280eb9001528111822d0002', 'BUTTON', 'TEST34', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A4D344059435A38E0531240FADC9A91', 'UNICOM', null, 'ROLE', 'ff8080814fcec033014fd4ec6a9b0643', 'BUTTON', 'TEST11', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A4D344059445A38E0531240FADC9A91', 'UNICOM', null, 'ROLE', 'ff8080814fcec033014fd4ec6a9b0643', 'BUTTON', 'TEST12', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A4D344059455A38E0531240FADC9A91', 'UNICOM', null, 'ROLE', 'ff8080814fcec033014fd4ec6a9b0643', 'BUTTON', 'TEST13', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A4D344059465A38E0531240FADC9A91', 'UNICOM', null, 'ROLE', 'ff8080814fcec033014fd4ec6a9b0643', 'BUTTON', 'TEST14', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC05C2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '99', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC05D2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '100', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC05E2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '101', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC05F2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '102', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0602B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '44', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0612B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '3ab', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0622B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '2c9080de51a4c6150151a50850440002', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0632B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '2c9080de51a04e550151a088b2620002', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0642B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '2c9080de51a04e550151a089a4510004', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0652B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'ff80808151b4985b0151b53891c8000f', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0662B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'ff80808151bbce290151cc82cd7a0013', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0672B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '2c908029537de1ec01537e2fc15a000b', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A4DA493A81B4957E0531240FADCB03D', 'UNICOM', null, 'ROLE', '2c908029528230050152823643670001', 'BUTTON', 'TEST32', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A4DA493A81C4957E0531240FADCB03D', 'UNICOM', null, 'ROLE', '2c908029528230050152823643670001', 'BUTTON', 'TEST33', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A4DA493A81D4957E0531240FADCB03D', 'UNICOM', null, 'ROLE', '2c908029528230050152823643670001', 'BUTTON', 'TEST34', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142AA3D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'ff80808150cb63740150d6d9b4be0019', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142AB3D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'ff80808151b4985b0151b537c3b2000d', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142AC3D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'TEST4', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142AD3D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'TEST3', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142AE3D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'TEST2', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142AF3D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'TEST1', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142B03D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'TEST21', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0682B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'ff80808151bbce290151c4653f870008', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC0692B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '2c90801a5331d16c015331d1f92e0001', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC06A2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'ff80808151bbce290151c3fabe400004', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC06B2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'ff80808151bbce290151d0d731940021', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC06C2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'ff80808151bbce290151d0d7c6eb0023', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC06D2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', 'ff80808151bbce290151d0d84a010025', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E26074CC06E2B55E0531240FADC1954', 'UNICOM', null, 'ROLE', 'ROLE000001', 'BUTTON', '2c908029537de1ec01537df1f4fa0008', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142B13D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'TEST34', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142B23D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'TEST33', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142B33D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'TEST32', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142B43D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'TEST31', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
commit;
prompt 400 records committed...
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142B53D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'TEST44', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142B63D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'TEST43', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142B73D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'TEST42', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142B83D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'TEST41', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142B93D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', '2c9080de51a4c6150151a50850440002', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142BA3D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', '2c9080de51a04e550151a088b2620002', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142BB3D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', '2c9080f6505f318101505f36c9f70003', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142BC3D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', '2c9080de51a04e550151a089a4510004', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142BD3D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'ff80808151b4985b0151b53891c8000f', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142BE3D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'ff80808151bbce290151cc82cd7a0013', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142BF3D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'ff80808151bbce290151c4653f870008', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142C03D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'ff80808151bbce290151c3fabe400004', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142C13D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'ff80808151bbce290151d0d731940021', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142C23D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'ff80808151bbce290151d0d7c6eb0023', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E28654142C33D55E0531240FADC901D', 'UNICOM', null, 'ROLE', '2c9080af537e15fd01537ed1e8570004', 'BUTTON', 'ff80808151bbce290151d0d84a010025', 1, 1, null, 'admin@ORG000000', '16-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E3B452963DD3C06E0531240FADCB10B', 'UNICOM', null, 'ROLE', 'ROLE000001', 'MENU', '7', 1, 1, null, 'chenyao@ORG000000', '17-MAR-16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2E3B4A06ADED26AFE0531240FADC9CB3', 'UNICOM', null, 'ROLE', 'ROLE000001', 'MENU', '7', 1, 1, null, 'chenyao@ORG000000', '17-3月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A4DA493A81A4957E0531240FADCB03D', 'UNICOM', null, 'ROLE', '2c908029528230050152823643670001', 'BUTTON', 'TEST31', 1, 1, null, 'admin@ORG000000', '27-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483406E3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '108', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58541393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', 'ff80808151b4985b0151b537c3b2000d', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58551393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '108', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58561393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '109', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58571393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '1201', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58581393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '111', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58591393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', 'MenuButton1', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C585A1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '11', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C585B1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '12', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C585C1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '13', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C585D1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '21', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C585E1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '22', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C585F1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '23', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58601393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '41', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58611393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '42', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58621393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '43', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58631393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '71', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58641393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '72', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58651393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', 'TEST21', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58661393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '112', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58671393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '113', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58681393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '114', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58691393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '116', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C586A1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '115', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C586B1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '104', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C586C1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '105', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C586D1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '106', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C586E1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '107', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C586F1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '10', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58701393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '20', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58711393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '40', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58721393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '70', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58731393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '74', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58741393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '14', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C4E5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST41', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C4F5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '112', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C505311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '113', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C515311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '114', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C525311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '116', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C535311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '115', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C545311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '104', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C555311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '105', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C565311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '106', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C575311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '107', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C585311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '10', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C595311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '20', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C5A5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '40', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C5B5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '70', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C5C5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '74', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C5D5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '14', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C5E5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '15', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C5F5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '16', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C605311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '75', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C615311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '76', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C625311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '77', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58751393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '15', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58761393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '16', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58771393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '75', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58781393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '76', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58791393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '77', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C587A1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '81', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C587B1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '82', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C587C1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '83', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C587D1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '84', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C587E1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '85', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C587F1393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '86', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58801393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '87', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58811393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '91', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58821393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '92', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58831393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '93', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58841393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '94', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58851393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '95', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58861393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '110', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58871393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '99', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58881393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '100', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A37DF4C58891393E0531240FADCF96A', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527ca2fe490001', 'BUTTON', '101', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C2C5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'ff80808150cb63740150d6d9b4be0019', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C2D5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'ff80808151b4985b0151b537c3b2000d', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C2E5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '108', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C2F5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '109', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C305311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '1201', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C315311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '111', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C325311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'MenuButton1', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C335311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '11', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C345311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '12', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C355311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '13', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C365311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '21', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C375311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '22', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C385311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '23', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C395311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '41', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C3A5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '42', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C3B5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '43', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C3C5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '71', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C3D5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', '72', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C3E5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST4', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C3F5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST3', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C405311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST2', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C415311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST1', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C425311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST14', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C435311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST13', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C445311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST12', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C455311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST11', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C465311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST21', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C475311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST34', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C485311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST33', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C495311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST32', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C4A5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST31', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C4B5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST44', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C4C5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST43', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C4D5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c9080295276a7f1015276caae5b0007', 'BUTTON', 'TEST42', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340793B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '41', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483407A3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '42', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483407B3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '43', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483407C3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '71', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483407D3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '72', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483407E3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '112', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483407F3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '113', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340803B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '114', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340813B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '116', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340823B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '115', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340833B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '104', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340843B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '105', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340853B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '106', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340863B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '107', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340873B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '10', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340883B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '20', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340893B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '40', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483408A3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '70', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483408B3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '74', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483408C3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '14', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483408D3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '15', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483408E3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '16', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483408F3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '75', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340903B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '76', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340913B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '77', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340923B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '81', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340933B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '82', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340943B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '83', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340953B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '84', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340963B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '85', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340973B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '86', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340983B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '87', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340993B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '91', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483409A3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '92', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483409B3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '93', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483409C3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '94', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483409D3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '95', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483409E3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '110', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483409F3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '99', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340A03B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '100', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340A13B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '101', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340A23B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '102', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340A33B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '44', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340A43B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '3ab', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39149DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d402628000e', 'BUTTON', 'TEST31', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39249DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d402628000e', 'BUTTON', 'TEST32', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39349DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d402628000e', 'BUTTON', 'TEST33', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39449DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d402628000e', 'BUTTON', 'TEST34', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39549DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d4597e00010', 'BUTTON', 'ff80808150cb63740150d6d9b4be0019', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39649DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d4597e00010', 'BUTTON', 'TEST4', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39749DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d4597e00010', 'BUTTON', 'TEST3', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39849DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d4597e00010', 'BUTTON', 'TEST2', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39949DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d4597e00010', 'BUTTON', 'TEST1', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39A49DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d4597e00010', 'BUTTON', 'TEST34', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39B49DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d4597e00010', 'BUTTON', 'TEST33', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A3A4324C39C49DCE0531240FADCF664', 'UNICOM', null, 'ROLE', '2c908029527d0d6d01527d4597e00010', 'BUTTON', 'TEST32', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BEA5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST3', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BEB5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST2', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BEC5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST1', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BED5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST14', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BEE5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST13', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BEF5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST12', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BF05311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST11', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BF15311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST21', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BF25311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST34', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BF35311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST33', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BF45311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST32', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BF55311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST31', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BF65311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST44', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BF75311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST43', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BF85311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST42', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BF95311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'TEST41', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
commit;
prompt 600 records committed...
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BFA5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '112', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BFB5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '113', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BFC5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '114', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BFD5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '116', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BFE5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '115', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591BFF5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '104', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C005311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '105', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C015311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '106', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C025311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '107', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C035311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '10', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C045311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '20', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C055311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '40', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C065311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '70', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C075311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '74', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C085311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '14', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C095311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '15', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C0A5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '16', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C0B5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '75', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C0C5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '76', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C0D5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '77', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C0E5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '81', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C0F5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '82', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C105311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '83', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C115311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '84', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C125311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '85', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C135311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '86', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C145311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '87', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C155311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '91', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C165311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '92', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C175311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '93', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C185311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '94', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C195311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '95', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C1A5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '110', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C1B5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '99', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C1C5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '100', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C1D5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '101', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C1E5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '102', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C1F5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '44', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C205311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '3ab', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C215311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '2c9080de51a4c6150151a50850440002', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C225311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '2c9080de51a04e550151a088b2620002', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C235311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '2c9080f6505f318101505f36c9f70003', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C245311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', '2c9080de51a04e550151a089a4510004', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C255311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'ff80808151b4985b0151b53891c8000f', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C265311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'ff80808151bbce290151cc82cd7a0013', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C275311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'ff80808151bbce290151c4653f870008', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C285311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'ff80808151bbce290151c3fabe400004', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C295311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'ff80808151bbce290151d0d731940021', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C2A5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'ff80808151bbce290151d0d7c6eb0023', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A34BE591C2B5311E0531240FADC9467', 'UNICOM', null, 'ROLE', '2c908029527bd09001527bd5c4a90001', 'BUTTON', 'ff80808151bbce290151d0d84a010025', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C483406F3B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', '109', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96861395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'ff80808150cb63740150d6d9b4be0019', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96871395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'ff80808151b4985b0151b537c3b2000d', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96881395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '108', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96891395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '109', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A968A1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '1201', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A968B1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '111', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A968C1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'MenuButton1', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A968D1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '11', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A968E1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '12', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A968F1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '13', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96901395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '21', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96911395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '22', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96921395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '23', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96931395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '41', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96941395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '42', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96951395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '43', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96961395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '71', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96971395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '72', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96981395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'TEST4', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96991395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'TEST3', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A969A1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'TEST2', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A969B1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'TEST1', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A39C48340723B7EE0531240FADCD496', 'UNICOM', null, 'ROLE', 'ff8080814ffd19c5014ffd1b766d0001', 'BUTTON', 'MenuButton1', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A969C1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', 'TEST21', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A969D1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '112', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A969E1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '113', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A969F1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '114', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96A01395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '116', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96A11395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '115', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96A21395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '104', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96A31395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '105', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96A41395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '106', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96A51395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '107', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96A61395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '10', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96A71395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '20', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96A81395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '40', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96A91395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '70', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96AA1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '74', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96AB1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '14', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96AC1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '15', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
insert into TBL_SYS_FUNC_PERMISSION (PERMISSION_ID, DOMAIN_ID, ORG_ID, OWN_TYPE, OWN_ID, FUNCTION_TYPE, FUNCTION_ID, CTRL_TYPE, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, EXPREESION)
values ('2A38259A96AD1395E0531240FADC9CEB', 'UNICOM', null, 'ROLE', '2c908029527c9dcf01527cc9fb190004', 'BUTTON', '16', 1, 1, null, 'admin@ORG000000', '26-1月 -16', null, null, null);
commit;
prompt 692 records loaded
prompt Loading TBL_SYS_GENERAL_CODE...
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('40287d83506ae2d901506af23f7f0005', 'SYS', null, 'XXXX_TYPE', '1', '12131', 2, 1, 0, null, null, null, '2015-10-15 18:01:12', null, '2015-10-15 18:01:29', null, null, '2015-10-15 18:01:12', null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('40287d83506ae2d901506af2552e0006', 'SYS', null, 'XXXX_TYPE', '2', '12121212', 1, 1, 0, null, null, null, '2015-10-15 18:01:17', null, '2015-10-15 18:01:29', null, null, '2015-10-15 18:01:17', null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c90802950cc3ec90150cc43bf7d0001', 'SYS', null, 'xxxx', 'fff', 'ffff', 10, 1, 0, null, 'sss', null, '2015-11-03 15:33:23', null, '2015-11-03 15:33:23', null, null, '2015-11-03 15:33:23', null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c90802950cc3ec90150cc487a9e0002', 'SYS', null, null, null, null, null, 1, null, null, null, null, '2015-11-03 15:38:33', null, '2015-11-03 15:38:33', null, null, '2015-11-03 15:38:33', null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080d651c375580151c384390d0001', 'UNICOM', null, 'ad', '111', '1111', 12, 1, 0, null, null, '2c9080aa517fb00001517fb54d010001', '2015-12-21 15:50:01', null, '2015-12-21 15:50:01', null, null, '2015-12-21 15:50:01', null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080295276a7f1015276dc64840009', 'UNICOM', null, 'DEGREE', '5', '研究生', 6, 1, 0, null, '研究生', '100000', '2016-01-25 11:38:21', '100000', '2016-01-25 11:38:34', null, null, '2016-01-25 11:38:21', null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080295276a7f1015276dd4cb8000a', 'UNICOM', null, 'SEX', '2', 'yy1', 3, 0, 0, null, 'yy1', '100000', '2016-01-25 11:39:20', '100000', '2016-01-25 11:39:46', null, '2016-01-25 11:39:46', null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e0544ac014e05c50beb000c', 'SYS', null, 'DEGREE', '0', '小学', 1, 1, 1, null, null, null, '2015-06-18 16:24:36', null, null, null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('4028824a4dfc0027014dfc091ed30010', 'SYS', null, 'BEACON_MODULE', 'HollyI8', 'HollyI8', 6, 1, 1, null, '111', null, null, null, '2015-06-26 09:21:59', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e0544ac014e05c369330009', 'SYS', null, 'SEX', '0', '男', 1, 1, 1, null, null, null, '2015-06-18 16:22:49', null, '2016-01-25 11:37:48', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e0544ac014e05c38b6a000a', 'SYS', null, 'SEX', '1', '女', 2, 1, 1, null, null, null, '2015-06-18 16:22:58', null, '2016-01-25 11:37:48', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e0544ac014e05c52b3b000d', 'SYS', null, 'DEGREE', '1', '初中', 2, 1, 1, null, null, null, '2015-06-18 16:24:44', null, null, null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e0544ac014e05c54ce2000e', 'SYS', null, 'DEGREE', '2', '高中', 4, 1, 1, null, null, null, '2015-06-18 16:24:53', null, '2016-01-25 11:36:29', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e0544ac014e05c56dc6000f', 'SYS', null, 'DEGREE', '3', '大学', 3, 1, 1, null, null, null, '2015-06-18 16:25:01', null, '2016-01-25 11:36:29', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e0544ac014e05c7005f0011', 'SYS', null, 'CERTIFICATE_TYPE', '0', '身份证', 4, 1, 1, null, null, null, '2015-06-18 16:26:44', null, '2015-06-26 09:21:07', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e0544ac014e05c7325f0012', 'SYS', null, 'CERTIFICATE_TYPE', '1', '军人证', 1, 1, 1, null, null, null, '2015-06-18 16:26:57', null, '2015-10-23 11:29:50', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e0544ac014e05c75d0e0013', 'SYS', null, 'CERTIFICATE_TYPE', '2', '老年证', 3, 1, 1, null, null, null, '2015-06-18 16:27:08', null, '2015-10-15 17:46:14', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e060124014e060d29700000', 'SYS', null, 'BEACON_MODULE', 'HollyCase', '工单子系统', 5, 1, 1, null, '1', null, '2015-06-18 17:43:22', null, '2015-06-26 09:21:53', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e43dfed014e43e129660028', 'SYS', null, 'NOTICE_TYPE', '1', '通告', 1, 1, 0, null, null, null, null, null, '2015-08-07 11:21:18', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e448b31014e448d772f0087', 'SYS', null, 'EMERGENCY_TYPE', '1', '紧急', 1, 1, 1, null, null, null, null, null, '2015-07-28 15:54:57', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('ff8080814ebe80a0014ecd5c6fec0786', 'SYS', null, 'NOTICE_STATUS', '1', '未发布', 2, 1, 1, null, null, null, '2015-07-27 10:34:24', null, null, null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('ff8080814ebe80a0014ecd5cb101078b', 'SYS', null, 'NOTICE_STATUS', '2', '已发布', 3, 1, 1, null, null, null, '2015-07-27 10:34:40', null, null, null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('ff8080814ebe80a0014ecd5cef23078f', 'SYS', null, 'NOTICE_STATUS', '3', '已过期', 4, 1, 1, null, null, null, '2015-07-27 10:34:56', null, null, null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('ff8080814ebe80a0014ebf5632810405', 'SYS', null, 'TRUE_FALSE', 'true', '是', 1, 1, 1, null, null, null, '2015-07-24 17:12:54', null, null, null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('ff8080814ebe80a0014ebf566b0b0407', 'SYS', null, 'TRUE_FALSE', 'false', '否', 2, 1, 1, null, null, null, '2015-07-24 17:13:08', null, null, null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e1f1055014e1f16c0d00001', 'SYS', null, 'AGE1', '0', '大', 1, 1, 1, null, null, null, '2015-06-23 14:24:21', null, '2015-06-24 16:50:26', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e29b939014e29f3547a1455', 'SYS', null, 'CERTIFICATE_TYPE', '5', '学生证', 2, 1, 1, null, null, null, null, null, '2015-10-23 11:29:50', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e1f1055014e1f16e74d0002', 'SYS', null, 'AGE1', '1', '小', 2, 0, 1, null, null, null, '2015-06-23 14:24:31', null, '2015-06-24 16:50:26', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e448b31014e448c3faa0026', 'SYS', null, 'NOTICE_TYPE', '0', '故障', 2, 1, 1, null, '2', null, null, null, '2015-08-07 11:21:18', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e29b939014e29bb47430191', 'SYS', null, 'ROLE_TYPE', '1', '总公司角色', 1, 1, 1, null, null, null, null, null, null, null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e29b939014e29bb47430192', 'SYS', null, 'ROLE_TYPE', '2', '省公司角色', 2, 1, 1, null, null, null, null, null, null, null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e29b939014e29bb47430193', 'SYS', null, 'ROLE_TYPE', '3', '市公司角色', 3, 1, 1, null, null, null, null, null, null, null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e448b31014e448c77cf003e', 'SYS', null, 'NOTICE_TYPE', '2', '公告', 3, 1, 1, null, '2', null, null, null, '2015-08-07 11:17:36', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e448b31014e448d519d0062', 'SYS', null, 'EMERGENCY_TYPE', '0', '一般', 5, 1, 1, null, null, null, null, null, '2015-07-24 17:43:34', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080ca4e448b31014e448da1e0009f', 'SYS', null, 'EMERGENCY_TYPE', '2', '非常紧急', 3, 1, 1, null, null, null, null, null, '2015-07-28 16:06:41', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080d34eae7d9c014eaf8c035c087f', 'SYS', null, 'YES_NO', '1', '是', 1, 1, 1, null, null, null, null, null, '2015-07-22 01:14:52', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080d34eae7d9c014eaf8c3fa50880', 'SYS', null, 'YES_NO', '0', '否', 2, 1, 1, null, null, null, null, null, '2015-07-22 01:14:52', null, null, null, null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c90801f51c2a2b30151c2a4e6de0001', 'UNICOM', null, 'ad', 'XXX', 'XXXXX', 1, 1, 0, null, '111', '100000', '2015-12-21 11:46:05', null, '2015-12-21 11:46:05', null, null, '2015-12-21 11:46:05', null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080d651c361800151c36c83530004', 'UNICOM', null, 'ad', '1', 'OOOSSS', 2, 1, 0, null, null, '2c9080aa517fb00001517fb54d010001', '2015-12-21 15:24:07', null, '2015-12-21 15:24:07', null, null, '2015-12-21 15:24:07', null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('40287d83506ae2d901506aeb50430002', 'SYS', null, 'RESOURCE_TYPE', '1', '常量', 1, 1, 0, null, null, null, '2015-10-15 17:53:37', null, '2015-10-16 09:20:01', null, null, '2015-10-15 17:53:37', null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('40287d83506ae2d901506aeb9f850003', 'SYS', null, 'RESOURCE_TYPE', '2', '对象属性', 2, 1, 0, null, null, null, '2015-10-15 17:53:58', null, '2015-10-16 09:20:01', null, null, '2015-10-15 17:53:58', null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('40287d83506e315401506e32449e0001', 'SYS', null, 'RESOURCE_TYPE', '3', '测试常量', 3, 1, 0, null, null, null, '2015-10-16 09:09:59', null, '2015-10-16 09:19:58', null, null, '2015-10-16 09:09:59', null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('ff808081508dbb0101508e990b4c0836', 'SYS', null, 'xxxx', 'CCC', 'CCC', 1, 1, 0, null, null, null, '2015-10-22 16:10:06', null, '2015-10-22 16:10:06', null, null, '2015-10-22 16:10:06', null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
values ('2c9080df51a9bbfe0151a9bd746b0002', 'UNICOM', null, 'ad', 'ssssss', 'ssss', 11, 1, 0, null, '11212', '100000', '2015-12-16 15:42:24', '100000', '2015-12-21 11:45:19', null, null, '2015-12-16 15:42:24', null);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
VALUES ('2c9080eb59b9e7260159b9e8baf30000', 'UNICOM', NULL, 'AREA', 'O', '异地', '10', '1', '0', NULL, '默认值', '4028db815557de66015557e942700001', '2017-01-20 11:25:54', NULL, '2017-01-20 11:25:54', NULL, NULL, '2017-01-20 11:25:54', NULL);
insert into TBL_SYS_GENERAL_CODE (CODE_ID, DOMAIN_ID, ORG_ID, CODE_TYPE, CODE_VALUE, CODE_NAME, SORT_NO, ENABLED, IS_SYSTEM, SUB_SYSTEM_ID, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, AREA_CODE, DISENABLED_TIME, ENABLED_TIME, IS_BY_DEVELOPER)
VALUES ('2c9080eb59b9e7260159b9e90c050001', 'UNICOM', NULL, 'VDN_TYPE', 'O', '异地', '10', '1', '0', NULL, '默认值', '4028db815557de66015557e942700001', '2017-01-20 11:26:15', NULL, '2017-01-20 11:26:15', NULL, NULL, '2017-01-20 11:26:15', NULL);
commit;
prompt 44 records loaded
prompt Loading TBL_SYS_GROUP...
insert into TBL_SYS_GROUP (GROUP_ID, GROUP_NAME, ORG_ID, DOMAIN_ID, IS_SYSTEM, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('40287d834ffd2aa1014ffd2bb6be0001', 'dsadasd', 'ORG100001', 'UNICOM', 0, 1, null, null, '2015-09-24 10:25:44', null, null);
insert into TBL_SYS_GROUP (GROUP_ID, GROUP_NAME, ORG_ID, DOMAIN_ID, IS_SYSTEM, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('40287d834ffd2aa1014ffd2d92ac0003', 'dasdasddsa', 'ORG100001', 'UNICOM', 0, 1, null, null, '2015-09-24 10:27:46', null, null);
insert into TBL_SYS_GROUP (GROUP_ID, GROUP_NAME, ORG_ID, DOMAIN_ID, IS_SYSTEM, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('40287d814ffe25ba014ffe292f1a0002', 'fsfsf', 'ORG100001', 'UNICOM', 0, 0, 'ssfsf', null, '2015-09-24 15:02:36', null, '2015-11-26 10:53:13');
insert into TBL_SYS_GROUP (GROUP_ID, GROUP_NAME, ORG_ID, DOMAIN_ID, IS_SYSTEM, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('40287d814ffe25ba014ffe542fb0000f', '1111', 'ORG100001', 'UNICOM', 0, 1, null, null, '2015-09-24 15:49:34', null, null);
insert into TBL_SYS_GROUP (GROUP_ID, GROUP_NAME, ORG_ID, DOMAIN_ID, IS_SYSTEM, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ff8080814fcec033014fd3bda01504b1', 'VIP坐席', 'ORG100001', 'UNICOM', 0, 1, 'VIP坐席', null, '2015-09-16 09:21:04', null, null);
insert into TBL_SYS_GROUP (GROUP_ID, GROUP_NAME, ORG_ID, DOMAIN_ID, IS_SYSTEM, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('40287d814ffe25ba014ffe37d4af000e', 'qqq', 'ORG000000', 'UNICOM', 0, 1, null, null, '2015-09-24 15:18:35', null, null);
insert into TBL_SYS_GROUP (GROUP_ID, GROUP_NAME, ORG_ID, DOMAIN_ID, IS_SYSTEM, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ff8080814ffe21b6014ffe5549360002', 'ADMCCC', 'ORG100006', 'UNICOM', 0, 1, null, null, '2015-09-24 15:50:46', null, null);
commit;
prompt 7 records loaded
prompt Loading TBL_SYS_LOCKUSER_INFO...
prompt Table is empty
prompt Loading TBL_SYS_MEDIA_FILE...
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f51cd40ec0151cd43b1280005', '2015-12-23 13:15:44', null, null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/91f1e29d-3f2f-424d-b9f5-7c136971be1d.jpg', '91f1e29d-3f2f-424d-b9f5-7c136971be1d.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/91f1e29d-3f2f-424d-b9f5-7c136971be1d.jpg', null, 'QQ图片20151028100237.jpg', 58082, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f51cd40ec0151cd4ae17e0006', '2015-12-23 13:23:35', null, null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/1af98a27-eccd-468f-ab6f-2da5c1ea0d0c.jpg', '1af98a27-eccd-468f-ab6f-2da5c1ea0d0c.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/1af98a27-eccd-468f-ab6f-2da5c1ea0d0c.jpg', null, 'QQ图片20151028100237.jpg', 58082, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f5252e60f015252e876bd0002', '2016-01-18 12:05:12', null, null, null, null, null, 'image/gif', null, 'gif', 'http://172.16.52.5:8002/upload/images/0c72fb96-3467-4879-aaed-4a1127f81bda.gif', '0c72fb96-3467-4879-aaed-4a1127f81bda.gif', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//20160118///images/0c72fb96-3467-4879-aaed-4a1127f81bda.gif', null, 'apache_pb.gif', 4463, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f5252e60f015252e9f9710003', '2016-01-18 12:06:51', null, null, null, null, null, 'image/gif', null, 'gif', 'http://172.16.52.5:8002/upload/20160118///images/60680174-c396-446c-a501-247ac71fc744.gif', '60680174-c396-446c-a501-247ac71fc744.gif', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload/20160118///images/60680174-c396-446c-a501-247ac71fc744.gif', null, 'apache_pb.gif', 4463, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f5252e60f015252ec2e3d0004', '2016-01-18 12:09:16', null, null, null, null, null, 'image/gif', null, 'gif', 'http://172.16.52.5:8002/upload/20160118/AAAA//images/e83e2116-3c34-4171-8bca-7b4cbc297d48.gif', 'e83e2116-3c34-4171-8bca-7b4cbc297d48.gif', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload/20160118/AAAA//images/e83e2116-3c34-4171-8bca-7b4cbc297d48.gif', null, 'apache_pb.gif', 4463, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f5252e60f015252ec99d10005', '2016-01-18 12:09:43', null, null, null, null, null, 'image/gif', null, 'gif', 'http://172.16.52.5:8002/upload/20160118/AAAA//images/73d898fc-3dd4-4c16-bf5d-e0caeb266fdb.gif', '73d898fc-3dd4-4c16-bf5d-e0caeb266fdb.gif', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload/20160118/AAAA//images/73d898fc-3dd4-4c16-bf5d-e0caeb266fdb.gif', null, 'apache_pb.gif', 4463, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f5252e60f015252ed5e380006', '2016-01-18 12:10:33', null, null, null, null, null, 'image/gif', null, 'gif', 'http://172.16.52.5:8002/upload/20160118/images/bfe1c033-dc23-40c1-9364-bd163ee18a21.gif', 'bfe1c033-dc23-40c1-9364-bd163ee18a21.gif', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload/20160118/images/bfe1c033-dc23-40c1-9364-bd163ee18a21.gif', null, 'apache_pb.gif', 4463, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f5363dcb2015363df1a220006', '2016-03-11 12:11:18', '100000', null, null, null, null, null, null, null, null, '总部', null, null, '2018/1/1', '防守打法', '中国联通集团公司', null, null);
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f51cc94540151cc9ccd3e0001', '2015-12-23 10:13:26', null, null, null, null, null, 'image/pjpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/06c09344-ed1d-46c2-84ef-a7c8cf6a5a38.jpg', '06c09344-ed1d-46c2-84ef-a7c8cf6a5a38.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/06c09344-ed1d-46c2-84ef-a7c8cf6a5a38.jpg', null, 'QQ截图20150825140644.jpg', 449645, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f51cc94540151ccc365ef0005', '2015-12-23 10:55:36', null, null, null, null, null, 'image/pjpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/a47ce3b4-dd75-42dd-bb4c-19d03764e967.jpg', 'a47ce3b4-dd75-42dd-bb4c-19d03764e967.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/a47ce3b4-dd75-42dd-bb4c-19d03764e967.jpg', null, 'QQ截图20150825140644.jpg', 449645, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f51cc94540151ccc3beef0006', '2015-12-23 10:55:59', null, null, null, null, null, 'image/pjpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/9746b82b-7b83-45c4-9559-e8f5930dcbaf.jpg', '9746b82b-7b83-45c4-9559-e8f5930dcbaf.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/9746b82b-7b83-45c4-9559-e8f5930dcbaf.jpg', null, 'QQ截图20151026155703.jpg', 111106, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f51ce34920151ce3537a20000', '2015-12-23 17:39:32', null, null, null, null, null, 'image/pjpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/c96c0758-94dd-4de8-bea3-bc22d2c3a874.jpg', 'c96c0758-94dd-4de8-bea3-bc22d2c3a874.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/c96c0758-94dd-4de8-bea3-bc22d2c3a874.jpg', null, 'QQ截图20151026155703.jpg', 111106, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f5252df3d015252dffbe10000', '2016-01-18 11:55:56', null, null, null, null, null, 'image/gif', null, 'gif', 'http://172.16.52.5:8002/upload/images/136db597-a3b8-4dbb-b1ba-3cf5575468bb.gif', '136db597-a3b8-4dbb-b1ba-3cf5575468bb.gif', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//20160118///images/136db597-a3b8-4dbb-b1ba-3cf5575468bb.gif', null, 'apache_pb.gif', 4463, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('ff8080814fcec033014ffe944d6f2a98', '2015-09-24 16:59:36', 'Cy', null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/bae1b98a-6f01-411b-a660-5cd8682890ee.jpg', 'bae1b98a-6f01-411b-a660-5cd8682890ee.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/bae1b98a-6f01-411b-a660-5cd8682890ee.jpg', null, 'd05352ce4bee007d66bb0eb5675fd3a1.jpg', 232513, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('ff8080814fcec03301500d89e02a304a', '2015-09-27 14:42:31', null, null, null, null, null, 'image/gif', null, 'gif', 'http://172.16.52.5:8002/upload/images/75847027-51da-427b-8fa6-efe9dd2f2c26.gif', '75847027-51da-427b-8fa6-efe9dd2f2c26.gif', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/75847027-51da-427b-8fa6-efe9dd2f2c26.gif', null, 'loading.gif', 3897, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('ff8080814fcec03301500d89f634304b', '2015-09-27 14:42:36', null, null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/a822d910-bb18-4458-a4b5-c3801e40443a.jpg', 'a822d910-bb18-4458-a4b5-c3801e40443a.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/a822d910-bb18-4458-a4b5-c3801e40443a.jpg', null, 'd05352ce4bee007d66bb0eb5675fd3a1.jpg', 232513, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('ff8080814fcec03301500d8a1c34304c', '2015-09-27 14:42:46', null, null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/3acb0036-f090-4e09-9eec-3be9fd8177a0.jpg', '3acb0036-f090-4e09-9eec-3be9fd8177a0.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/3acb0036-f090-4e09-9eec-3be9fd8177a0.jpg', null, 'psb42Z6IVGB.jpg', 71686, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('ff8080814fcec03301500d8a42de304d', '2015-09-27 14:42:56', null, null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/9a4ae479-6900-401b-bcb8-ae759048877b.jpg', '9a4ae479-6900-401b-bcb8-ae759048877b.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/9a4ae479-6900-401b-bcb8-ae759048877b.jpg', null, 'd05352ce4bee007d66bb0eb5675fd3a1.jpg', 232513, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f51cc94540151cca325a30002', '2015-12-23 10:20:22', null, null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/ec5d31ae-6a28-4ea1-933c-343a20de8f6f.jpg', 'ec5d31ae-6a28-4ea1-933c-343a20de8f6f.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/ec5d31ae-6a28-4ea1-933c-343a20de8f6f.jpg', null, 'QQ截图20150825140644.jpg', 449645, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f51cc94540151ccaf3b6a0003', '2015-12-23 10:33:34', null, null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/cf6a5a9d-518e-4a0b-9eec-eb91d37bc1d1.jpg', 'cf6a5a9d-518e-4a0b-9eec-eb91d37bc1d1.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/cf6a5a9d-518e-4a0b-9eec-eb91d37bc1d1.jpg', null, 'QQ截图20150825140644.jpg', 449645, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f51cc94540151ccaf7bfd0004', '2015-12-23 10:33:51', null, null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/ebffbe81-fd43-489f-b991-3c19329f03d7.jpg', 'ebffbe81-fd43-489f-b991-3c19329f03d7.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/ebffbe81-fd43-489f-b991-3c19329f03d7.jpg', null, 'QQ截图20150825140644.jpg', 449645, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f531b48d701531b4adfe80001', '2016-02-26 09:56:45', '100000', null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/20160226/images/3954e5e9-76a1-4d6a-a61f-71ebc40b3e70.jpg', '3954e5e9-76a1-4d6a-a61f-71ebc40b3e70.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload/20160226/images/3954e5e9-76a1-4d6a-a61f-71ebc40b3e70.jpg', null, 'logo.jpg', 3750, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f53540d530153540f15ac0001', '2016-03-08 10:29:48', '100000', null, null, null, null, 'image/png', null, 'png', 'http://172.16.52.5:8002/upload/20160308/images?origurl=http://172.16.52.5:8002/upload/20160308/images/1097fa39-85c9-4b3a-90c1-e4184792ec35.png', '1097fa39-85c9-4b3a-90c1-e4184792ec35.png', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload/20160308/images/1097fa39-85c9-4b3a-90c1-e4184792ec35.png', null, 'clipboard.png', 38482, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f5363e088015363e2ec270001', '2016-03-11 12:15:29', '100000', null, null, null, null, null, null, null, null, '总部', null, null, '2018/1/1', '防守打法', '中国联通集团公司', null, null);
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f51cc88170151cc89dd430001', '2015-12-23 09:52:45', null, null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/f33c3a2a-c176-4b60-b884-0e10a48cd60b.jpg', 'f33c3a2a-c176-4b60-b884-0e10a48cd60b.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/f33c3a2a-c176-4b60-b884-0e10a48cd60b.jpg', null, 'QQ截图20150825140644.jpg', 449645, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f51cc88170151cc8a75550002', '2015-12-23 09:53:24', null, null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/18127e0f-1439-4eb7-9d10-1aa478a49127.jpg', '18127e0f-1439-4eb7-9d10-1aa478a49127.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/18127e0f-1439-4eb7-9d10-1aa478a49127.jpg', null, 'QQ截图20151026155703.jpg', 111106, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c9080ac52e83a890152e8bc3b2c0000', '2016-02-16 14:19:56', null, null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/94658b4d-b9f7-486f-a548-7ad6139c1705.jpg', '94658b4d-b9f7-486f-a548-7ad6139c1705.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/94658b4d-b9f7-486f-a548-7ad6139c1705.jpg', null, 'u=678777248,4096965288&fm=21&gp=0.jpg', 5498, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c9080ac52e83a890152e8bd8a0e0001', '2016-02-16 14:21:21', null, null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/8dffa2c5-b214-46fe-b39d-e6eee7152c3a.jpg', '8dffa2c5-b214-46fe-b39d-e6eee7152c3a.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/8dffa2c5-b214-46fe-b39d-e6eee7152c3a.jpg', null, 'u=678777248,4096965288&fm=21&gp=0.jpg', 5498, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f5354194c0153541a87000000', '2016-03-08 10:42:17', '100000', null, null, null, null, 'image/png', null, 'png', 'http://172.16.52.5:8002/upload/20160308/images/f59a92a5-bb12-42ce-ab76-179093f9819f.png?origurl=http://localnet/upload/20160308/images/f59a92a5-bb12-42ce-ab76-179093f9819f.png', 'f59a92a5-bb12-42ce-ab76-179093f9819f.png', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload/20160308/images/f59a92a5-bb12-42ce-ab76-179093f9819f.png', null, 'clipboard.png', 38482, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f53541cc20153541d47430000', '2016-03-08 10:45:18', '100000', null, null, null, null, 'image/png', null, 'png', 'http://172.16.52.5:8002/upload/20160308/images/d117fb4e-cabe-4007-89dd-393935e8cbf8.png?origurl=null/images/d117fb4e-cabe-4007-89dd-393935e8cbf8.png', 'd117fb4e-cabe-4007-89dd-393935e8cbf8.png', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload/20160308/images/d117fb4e-cabe-4007-89dd-393935e8cbf8.png', null, 'clipboard.png', 38482, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f53541ed40153541f43600000', '2016-03-08 10:47:28', '100000', null, null, null, null, 'image/png', null, 'png', 'http://172.16.52.5:8002/upload/20160308/images/07d0d379-e216-4c53-8de0-35e53cd230f5.png', '07d0d379-e216-4c53-8de0-35e53cd230f5.png', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload/20160308/images/07d0d379-e216-4c53-8de0-35e53cd230f5.png', null, 'clipboard.png', 38482, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f53541ed40153541f60c50001', '2016-03-08 10:47:35', '100000', null, null, null, null, 'image/png', null, 'png', 'http://172.16.52.5:8002/upload/20160308/images/3f92c88e-ca6c-41a8-ba0d-34c32128d56d.png', '3f92c88e-ca6c-41a8-ba0d-34c32128d56d.png', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload/20160308/images/3f92c88e-ca6c-41a8-ba0d-34c32128d56d.png', null, 'clipboard.png', 38482, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f53541ed40153541f9b0c0002', '2016-03-08 10:47:50', '100000', null, null, null, null, 'image/png', null, 'png', 'http://172.16.52.5:8002/upload/20160308/images/35787f36-b879-4c6c-a620-3b82b71b2e34.png?origurl=http://localnet/upload/20160308/images/35787f36-b879-4c6c-a620-3b82b71b2e34.png', '35787f36-b879-4c6c-a620-3b82b71b2e34.png', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload/20160308/images/35787f36-b879-4c6c-a620-3b82b71b2e34.png', null, 'clipboard.png', 38482, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f5363cb5a015363cff3390001', '2016-03-11 11:54:45', '100000', null, null, null, null, null, null, null, null, '总部', null, null, '2018/1/1', '防守打法', '中国联通集团公司', null, null);
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('ff808081506fcce701506fcf6042001c', '2015-10-16 16:41:13', null, null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/b389b5ce-456a-42d8-a831-996780869613.jpg', 'b389b5ce-456a-42d8-a831-996780869613.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/b389b5ce-456a-42d8-a831-996780869613.jpg', null, 'QQ截图20150825140644.jpg', 449645, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('ff808081509410a00150a2e1b58e000c', '2015-10-26 14:41:52', null, null, null, null, null, 'image/jpeg', null, 'jpg', 'http://172.16.52.5:8002/upload/images/6967a75e-986d-47bf-ad36-c4eb60035567.jpg', '6967a75e-986d-47bf-ad36-c4eb60035567.jpg', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//images/6967a75e-986d-47bf-ad36-c4eb60035567.jpg', null, '9f2f070828381f305aa4ddd9ab014c086e06f005.jpg', 231718, '1');
insert into TBL_SYS_MEDIA_FILE (MEDIA_ID, CREATE_TIME, CREATOR, DOMAIN_ID, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, CONTENT_TYPE, FILE_DESC, FILE_EXT, FILE_HTTP_URL, FILE_NAME, FILE_TITLE, FILE_TYPE, FILE_URI, MEDIA_TYPE, OLD_FILE_NAME, FILE_SIZE, ENABLED)
values ('2c90801f5252d3e3015252da860a0002', '2016-01-18 11:49:58', null, null, null, null, null, 'image/gif', null, 'gif', 'http://172.16.52.5:8002/upload/images/823b4975-81cd-492d-a545-ddf295e27859.gif', '823b4975-81cd-492d-a545-ddf295e27859.gif', null, null, 'sftp://upload:ftp_upload@172.16.52.5/home/upload//20160118///images/823b4975-81cd-492d-a545-ddf295e27859.gif', null, 'apache_pb.gif', 4463, '1');
commit;
prompt 37 records loaded
prompt Loading TBL_SYS_ORG...
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2140410', '铁岭市联通分公司', 'UNICOM', 1, 1, '2140410', '3', 'ORG100021', 'ORG100021', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2140411', '大连市联通分公司', 'UNICOM', 1, 1, '2140411', '3', 'ORG100021', 'ORG100021', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2140412', '鞍山市联通分公司', 'UNICOM', 1, 1, '2140412', '3', 'ORG100021', 'ORG100021', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2140414', '本溪市联通分公司', 'UNICOM', 1, 1, '2140414', '3', 'ORG100021', 'ORG100021', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2140415', '丹东市联通分公司', 'UNICOM', 1, 1, '2140415', '3', 'ORG100021', 'ORG100021', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2140416', '锦州市联通分公司', 'UNICOM', 1, 1, '2140416', '3', 'ORG100021', 'ORG100021', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2140419', '辽阳市联通分公司', 'UNICOM', 1, 1, '2140419', '3', 'ORG100021', 'ORG100021', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2140421', '朝阳市联通分公司', 'UNICOM', 1, 1, '2140421', '3', 'ORG100021', 'ORG100021', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2140427', '盘锦市联通分公司', 'UNICOM', 1, 1, '2140427', '3', 'ORG100021', 'ORG100021', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2140429', '葫芦岛联通分公司', 'UNICOM', 1, 1, '2140429', '3', 'ORG100021', 'ORG100021', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2150951', '银川市联通分公司', 'UNICOM', 1, 1, '2150951', '3', 'ORG100022', 'ORG100022', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2150954', '固原市联通分公司', 'UNICOM', 1, 1, '2150954', '3', 'ORG100022', 'ORG100022', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2160971', '西宁市联通分公司', 'UNICOM', 1, 1, '2160971', '3', 'ORG100023', 'ORG100023', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2160972', '海东市联通分公司', 'UNICOM', 1, 1, '2160972', '3', 'ORG100023', 'ORG100023', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2160973', '同仁市联通分公司', 'UNICOM', 1, 1, '2160973', '3', 'ORG100023', 'ORG100023', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2160977', '德令哈联通分公司', 'UNICOM', 1, 1, '2160977', '3', 'ORG100023', 'ORG100023', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2170530', '菏泽市联通分公司', 'UNICOM', 1, 1, '2170530', '3', 'ORG100024', 'ORG100024', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2170531', '济南市联通分公司', 'UNICOM', 1, 1, '2170531', '3', 'ORG100024', 'ORG100024', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2170532', '青岛市联通分公司', 'UNICOM', 1, 1, '2170532', '3', 'ORG100024', 'ORG100024', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2170533', '淄博市联通分公司', 'UNICOM', 1, 1, '2170533', '3', 'ORG100024', 'ORG100024', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2170536', '淮坊市联通分公司', 'UNICOM', 1, 1, '2170536', '3', 'ORG100024', 'ORG100024', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2170537', '济宁市联通分公司', 'UNICOM', 1, 1, '2170537', '3', 'ORG100024', 'ORG100024', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2170538', '泰安市联通分公司', 'UNICOM', 1, 1, '2170538', '3', 'ORG100024', 'ORG100024', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2170539', '临沂市联通分公司', 'UNICOM', 1, 1, '2170539', '3', 'ORG100024', 'ORG100024', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2180350', '忻州市联通分公司', 'UNICOM', 1, 1, '2180350', '3', 'ORG100025', 'ORG100025', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2180351', '太原市联通分公司', 'UNICOM', 1, 1, '2180351', '3', 'ORG100025', 'ORG100025', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2180352', '大同市联通分公司', 'UNICOM', 1, 1, '2180352', '3', 'ORG100025', 'ORG100025', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2180354', '榆次市联通分公司', 'UNICOM', 1, 1, '2180354', '3', 'ORG100025', 'ORG100025', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2180357', '临汾市联通分公司', 'UNICOM', 1, 1, '2180357', '3', 'ORG100025', 'ORG100025', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2180358', '离石市联通分公司', 'UNICOM', 1, 1, '2180358', '3', 'ORG100025', 'ORG100025', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2180359', '运城市联通分公司', 'UNICOM', 1, 1, '2180359', '3', 'ORG100025', 'ORG100025', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG219029', '西安市联通分公司', 'UNICOM', 1, 1, '219029', '3', 'ORG100026', 'ORG100026', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2190910', '咸阳市联通分公司', 'UNICOM', 1, 1, '2190910', '3', 'ORG100026', 'ORG100026', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2190911', '延安市联通分公司', 'UNICOM', 1, 1, '2190911', '3', 'ORG100026', 'ORG100026', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2190912', '榆林市联通分公司', 'UNICOM', 1, 1, '2190912', '3', 'ORG100026', 'ORG100026', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2190913', '渭南市联通分公司', 'UNICOM', 1, 1, '2190913', '3', 'ORG100026', 'ORG100026', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2190916', '汉中市联通分公司', 'UNICOM', 1, 1, '2190916', '3', 'ORG100026', 'ORG100026', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2190919', '铜川市联通分公司', 'UNICOM', 1, 1, '2190919', '3', 'ORG100026', 'ORG100026', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200691', '景洪市联通分公司', 'UNICOM', 1, 1, '2200691', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200692', '潞西市联通分公司', 'UNICOM', 1, 1, '2200692', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200870', '昭通市联通分公司', 'UNICOM', 1, 1, '2200870', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200871', '昆明市联通分公司', 'UNICOM', 1, 1, '2200871', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200872', '大理市联通分公司', 'UNICOM', 1, 1, '2200872', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200873', '个旧市联通分公司', 'UNICOM', 1, 1, '2200873', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200875', '保山市联通分公司', 'UNICOM', 1, 1, '2200875', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200878', '楚雄市联通分公司', 'UNICOM', 1, 1, '2200878', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200879', '思茅市联通分公司', 'UNICOM', 1, 1, '2200879', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200881', '东川市联通分公司', 'UNICOM', 1, 1, '2200881', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200883', '临沧市联通分公司', 'UNICOM', 1, 1, '2200883', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200886', '六库市联通分公司', 'UNICOM', 1, 1, '2200886', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200887', '中甸市联通分公司', 'UNICOM', 1, 1, '2200887', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200888', '丽江市联通分公司', 'UNICOM', 1, 1, '2200888', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210810', '涪陵市联通分公司', 'UNICOM', 1, 1, '2210810', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210812', '攀枝花联通分公司', 'UNICOM', 1, 1, '2210812', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210813', '自贡市联通分公司', 'UNICOM', 1, 1, '2210813', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210816', '绵阳市联通分公司', 'UNICOM', 1, 1, '2210816', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210817', '南充市联通分公司', 'UNICOM', 1, 1, '2210817', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210818', '达县市联通分公司', 'UNICOM', 1, 1, '2210818', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210819', '万县市联通分公司', 'UNICOM', 1, 1, '2210819', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210825', '遂宁市联通分公司', 'UNICOM', 1, 1, '2210825', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210827', '巴中市联通分公司', 'UNICOM', 1, 1, '2210827', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210830', '泸州市联通分公司', 'UNICOM', 1, 1, '2210830', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210832', '内江市联通分公司', 'UNICOM', 1, 1, '2210832', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210833', '乐山市联通分公司', 'UNICOM', 1, 1, '2210833', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210834', '西昌市联通分公司', 'UNICOM', 1, 1, '2210834', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210836', '康定市联通分公司', 'UNICOM', 1, 1, '2210836', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210837', '马尔康联通分公司', 'UNICOM', 1, 1, '2210837', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210838', '德阳市联通分公司', 'UNICOM', 1, 1, '2210838', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210840', '泸州市联通分公司', 'UNICOM', 1, 1, '2210840', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2220891', '拉萨市联通分公司', 'UNICOM', 1, 1, '2220891', '3', 'ORG100029', 'ORG100029', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2220893', '山南市联通分公司', 'UNICOM', 1, 1, '2220893', '3', 'ORG100029', 'ORG100029', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2230901', '塔城市联通分公司', 'UNICOM', 1, 1, '2230901', '3', 'ORG100030', 'ORG100030', 3, 0, 1, 0, null, null, null, null, '2015-09-24 10:09:37');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2230902', '哈密市联通分公司', 'UNICOM', 1, 1, '2230902', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2230903', '和田市联通分公司', 'UNICOM', 1, 1, '2230903', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2230906', '阿勒泰市联通分公司', 'UNICOM', 1, 1, '2230906', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2230909', '博乐市联通分公司', 'UNICOM', 1, 1, '2230909', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2230990', '克拉玛依市联通分公司', 'UNICOM', 1, 1, '2230990', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2230991', '乌鲁木齐市联通分公司', 'UNICOM', 1, 1, '2230991', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG22309941', '阜康市联通分公司', 'UNICOM', 1, 1, '22309941', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG22309942', '昌吉市联通分公司', 'UNICOM', 1, 1, '22309942', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG22309943', '五家渠市联通分公司', 'UNICOM', 1, 1, '22309943', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2230995', '吐鲁番市联通分公司', 'UNICOM', 1, 1, '2230995', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2230996', '库尔勒市联通分公司', 'UNICOM', 1, 1, '2230996', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2230997', '阿克苏市联通分公司', 'UNICOM', 1, 1, '2230997', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG22309982', '图木舒克市联通分公司', 'UNICOM', 1, 1, '22309982', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2240570', '衢州市联通分公司', 'UNICOM', 1, 1, '2240570', '3', 'ORG100031', 'ORG100031', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2240571', '杭州市联通分公司', 'UNICOM', 1, 1, '2240571', '3', 'ORG100031', 'ORG100031', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2240573', '嘉兴市联通分公司', 'UNICOM', 1, 1, '2240573', '3', 'ORG100031', 'ORG100031', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2240574', '宁波市联通分公司', 'UNICOM', 1, 1, '2240574', '3', 'ORG100031', 'ORG100031', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2240575', '绍兴市联通分公司', 'UNICOM', 1, 1, '2240575', '3', 'ORG100031', 'ORG100031', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2240576', '台州市联通分公司', 'UNICOM', 1, 1, '2240576', '3', 'ORG100031', 'ORG100031', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2240577', '温州市联通分公司', 'UNICOM', 1, 1, '2240577', '3', 'ORG100031', 'ORG100031', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2240579', '金华市联通分公司', 'UNICOM', 1, 1, '2240579', '3', 'ORG100031', 'ORG100031', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2240580', '舟山市联通分公司', 'UNICOM', 1, 1, '2240580', '3', 'ORG100031', 'ORG100031', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2250470', '海拉尔联通分公司', 'UNICOM', 1, 1, '2250470', '3', 'ORG100032', 'ORG100032', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2250473', '乌海市联通分公司', 'UNICOM', 1, 1, '2250473', '3', 'ORG100032', 'ORG100032', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2250475', '通辽市联通分公司', 'UNICOM', 1, 1, '2250475', '3', 'ORG100032', 'ORG100032', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2250476', '赤峰市联通分公司', 'UNICOM', 1, 1, '2250476', '3', 'ORG100032', 'ORG100032', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2250477', '东胜市联通分公司', 'UNICOM', 1, 1, '2250477', '3', 'ORG100032', 'ORG100032', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2250478', '临河市联通分公司', 'UNICOM', 1, 1, '2250478', '3', 'ORG100032', 'ORG100032', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2250482', '乌兰浩特联通分公司', 'UNICOM', 1, 1, '2250482', '3', 'ORG100032', 'ORG100032', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('15540222213C1831E0531240FADC5705', '总部', 'UNICOM', 0, 1, '000000', '4', 'ORG000000', 'ORG000000', 4, 0, 0, -1, null, null, null, null, '2015-12-25 11:42:23');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ff8080814ffd19c5014ffd1cf62e0003', 'csdfsfd', null, 0, 1, '2230901', '4', 'ORG2230901', 'ORG2230901', 4, 1, 0, null, null, null, '2015-09-24 10:09:37', null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('40287d834ffd25a5014ffd26e4c50001', '部门123', null, 0, 1, '100001', '4', '40287d834ffd3dd7014ffd3ebdd80001', 'ORG100001', 3, 0, 0, null, null, null, '2015-09-24 10:20:28', null, '2016-01-25 12:28:30');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('40287d834ffd3dd7014ffd3ebdd80001', 'dasdasd', null, 0, 1, '100001', '4', 'ORG100030', 'ORG100001', 3, 0, 0, null, null, null, '2015-09-24 10:46:31', null, '2016-01-26 14:25:16');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('40287d814ffe25ba014ffe2800a70001', '111', null, 0, 1, '100001', '4', '40287d834ffd3dd7014ffd3ebdd80001', 'ORG100001', 4, 1, 0, null, null, null, '2015-09-24 15:01:18', null, '2016-01-25 15:23:39');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ff808081506fcce701507f61b9050dcb', 'SZW', null, 0, 1, '100001', '4', '40287d834ffd3dd7014ffd3ebdd80001', 'ORG100001', 4, 1, 0, null, null, null, '2015-10-19 17:15:22', null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('2c908029527c935d01527c95c0df0001', '123', null, 0, 1, '100001', '4', '40287d834ffd3dd7014ffd3ebdd80001', 'ORG100001', 4, 1, 0, null, '123', null, '2016-01-26 14:18:55', null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('2c908029533541ab015335f457dc0008', '联通部门测试', null, 0, 1, '100001', '4', 'ORG100001', 'ORG100001', 3, 1, 0, null, '联通部门测试', 'chenyao@ORG000000', '2016-03-02 14:11:59', null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2260450', '阿城市联通分公司', 'UNICOM', 1, 1, '2260450', '3', 'ORG100033', 'ORG100033', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100006', '香港联通公司', 'UNICOM', 1, 1, '100006', '1', 'ORG000000', 'ORG000000', 2, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2260451', '哈尔滨联通分公司', 'UNICOM', 1, 1, '2260451', '3', 'ORG100033', 'ORG100033', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2260452', '齐齐哈尔联通分公司', 'UNICOM', 1, 1, '2260452', '3', 'ORG100033', 'ORG100033', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2260453', '牡丹江联通分公司', 'UNICOM', 1, 1, '2260453', '3', 'ORG100033', 'ORG100033', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2260454', '佳木斯联通分公司', 'UNICOM', 1, 1, '2260454', '3', 'ORG100033', 'ORG100033', 3, 0, 1, 0, null, null, null, null, '2015-07-22 13:52:49');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2260455', '绥化市联通分公司', 'UNICOM', 1, 1, '2260455', '3', 'ORG100033', 'ORG100033', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2260456', '黑河市联通分公司', 'UNICOM', 1, 1, '2260456', '3', 'ORG100033', 'ORG100033', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2260457', '加格达奇联通分公司', 'UNICOM', 1, 1, '2260457', '3', 'ORG100033', 'ORG100033', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2260458', '伊春市联通分公司', 'UNICOM', 1, 1, '2260458', '3', 'ORG100033', 'ORG100033', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2260459', '大庆市联通分公司', 'UNICOM', 1, 1, '2260459', '3', 'ORG100033', 'ORG100033', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2150952', '石嘴山联通分公司', 'UNICOM', 1, 1, '2150952', '3', 'ORG100022', 'ORG100022', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2070335', '秦皇岛联通分公司', 'UNICOM', 1, 1, '2070335', '3', 'ORG100014', 'ORG100014', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080391', '焦作市联通分公司', 'UNICOM', 1, 1, '2080391', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2090712', '孝感市联通分公司', 'UNICOM', 1, 1, '2090712', '3', 'ORG100016', 'ORG100016', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2100731', '株洲市联通分公司', 'UNICOM', 1, 1, '2100731', '3', 'ORG100017', 'ORG100017', 3, 1, 1, 2, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2100745', '娄底市联通分公司', 'UNICOM', 1, 1, '2100745', '3', 'ORG100017', 'ORG100017', 3, 1, 1, 9, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG21105172', '淮安市联通分公司', 'UNICOM', 1, 1, '21105172', '3', 'ORG100018', 'ORG100018', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2120797', '赣州市联通分公司', 'UNICOM', 1, 1, '2120797', '3', 'ORG100019', 'ORG100019', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2130439', '浑江市联通分公司', 'UNICOM', 1, 1, '2130439', '3', 'ORG100020', 'ORG100020', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2140418', '阜新市联通分公司', 'UNICOM', 1, 1, '2140418', '3', 'ORG100021', 'ORG100021', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2160974', '共和市联通分公司', 'UNICOM', 1, 1, '2160974', '3', 'ORG100023', 'ORG100023', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2170535', '烟台市联通分公司', 'UNICOM', 1, 1, '2170535', '3', 'ORG100024', 'ORG100024', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2180356', '晋城市联通分公司', 'UNICOM', 1, 1, '2180356', '3', 'ORG100025', 'ORG100025', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2190917', '宝鸡市联通分公司', 'UNICOM', 1, 1, '2190917', '3', 'ORG100026', 'ORG100026', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200876', '文山市联通分公司', 'UNICOM', 1, 1, '2200876', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210811', '重庆市联通分公司', 'UNICOM', 1, 1, '2210811', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210831', '宜宾市联通分公司', 'UNICOM', 1, 1, '2210831', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2220892', '日喀则联通分公司', 'UNICOM', 1, 1, '2220892', '3', 'ORG100029', 'ORG100029', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2230993', '石河子市联通分公司', 'UNICOM', 1, 1, '2230993', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2240572', '湖州市联通分公司', 'UNICOM', 1, 1, '2240572', '3', 'ORG100031', 'ORG100031', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2250472', '包头市联通分公司', 'UNICOM', 1, 1, '2250472', '3', 'ORG100032', 'ORG100032', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100016', '湖北省联通公司', 'UNICOM', 1, 1, '100016', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, '2015-07-03 16:29:04');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100027', '云南省联通公司', 'UNICOM', 1, 1, '100027', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000553', '芜湖市联通分公司', 'UNICOM', 1, 1, '2000553', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000565', '巢湖市联通分公司', 'UNICOM', 1, 1, '2000565', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2020930', '临夏市联通分公司', 'UNICOM', 1, 1, '2020930', '3', 'ORG100009', 'ORG100009', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2020943', '白银市联通分公司', 'UNICOM', 1, 1, '2020943', '3', 'ORG100009', 'ORG100009', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030756', '珠海市联通分公司', 'UNICOM', 1, 1, '2030756', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2040771', '南宁市联通分公司', 'UNICOM', 1, 1, '2040771', '3', 'ORG100011', 'ORG100011', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2050854', '汕头市联通分公司', 'UNICOM', 1, 1, '2050854', '3', 'ORG100012', 'ORG100012', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2020934', '西峰市联通分公司', 'UNICOM', 1, 1, '2020934', '3', 'ORG100009', 'ORG100009', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030663', '揭西市联通分公司', 'UNICOM', 1, 1, '2030663', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030762', '河源市联通分公司', 'UNICOM', 1, 1, '2030762', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2040774', '梧州市联通分公司', 'UNICOM', 1, 1, '2040774', '3', 'ORG100011', 'ORG100011', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2050858', '肇庆市联通分公司', 'UNICOM', 1, 1, '2050858', '3', 'ORG100012', 'ORG100012', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2160976', '玉树市联通分公司', 'UNICOM', 1, 1, '2160976', '3', 'ORG100023', 'ORG100023', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2250474', '集宁市联通分公司', 'UNICOM', 1, 1, '2250474', '3', 'ORG100032', 'ORG100032', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2250483', '阿拉善左旗联通分公司', 'UNICOM', 1, 1, '2250483', '3', 'ORG100032', 'ORG100032', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2040778', '河池市联通分公司', 'UNICOM', 1, 1, '2040778', '3', 'ORG100011', 'ORG100011', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2050857', '佛山市联通分公司', 'UNICOM', 1, 1, '2050857', '3', 'ORG100012', 'ORG100012', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2070310', '邯郸市联通分公司', 'UNICOM', 1, 1, '2070310', '3', 'ORG100014', 'ORG100014', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2070317', '沧州市联通分公司', 'UNICOM', 1, 1, '2070317', '3', 'ORG100014', 'ORG100014', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080379', '洛阳市联通分公司', 'UNICOM', 1, 1, '2080379', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2090714', '黄石市联通分公司', 'UNICOM', 1, 1, '2090714', '3', 'ORG100016', 'ORG100016', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2100734', '邵阳市联通分公司', 'UNICOM', 1, 1, '2100734', '3', 'ORG100017', 'ORG100017', 3, 1, 1, 5, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2110512', '苏州市联通分公司', 'UNICOM', 1, 1, '2110512', '3', 'ORG100018', 'ORG100018', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2120792', '九江市联通分公司', 'UNICOM', 1, 1, '2120792', '3', 'ORG100019', 'ORG100019', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2130436', '白城市联通分公司', 'UNICOM', 1, 1, '2130436', '3', 'ORG100020', 'ORG100020', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2140417', '营口市联通分公司', 'UNICOM', 1, 1, '2140417', '3', 'ORG100021', 'ORG100021', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2160975', '玛沁市联通分公司', 'UNICOM', 1, 1, '2160975', '3', 'ORG100023', 'ORG100023', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2180353', '阳泉市联通分公司', 'UNICOM', 1, 1, '2180353', '3', 'ORG100025', 'ORG100025', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2190915', '安康市联通分公司', 'UNICOM', 1, 1, '2190915', '3', 'ORG100026', 'ORG100026', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200877', '玉溪市联通分公司', 'UNICOM', 1, 1, '2200877', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210814', '永川市联通分公司', 'UNICOM', 1, 1, '2210814', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210835', '雅安市联通分公司', 'UNICOM', 1, 1, '2210835', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2230908', '阿图什市联通分公司', 'UNICOM', 1, 1, '2230908', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2230999', '伊宁市联通分公司', 'UNICOM', 1, 1, '2230999', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2250471', '呼和浩特联通分公司', 'UNICOM', 1, 1, '2250471', '3', 'ORG100032', 'ORG100032', 3, 0, 1, 0, null, null, null, null, '2015-07-21 15:54:55');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100008', '福建省联通公司', 'UNICOM', 1, 1, '100008', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100020', '吉林省联通公司', 'UNICOM', 1, 1, '100020', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100032', '内蒙古省联通公司', 'UNICOM', 1, 1, '100032', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000562', '铜陵市联通分公司', 'UNICOM', 1, 1, '2000562', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100002', '上海联通公司', 'UNICOM', 1, 1, '100002', '1', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, '100000', '2016-03-18 15:22:45');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100003', '天津联通公司', 'UNICOM', 1, 1, '100003', '1', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, '2c9080295276a7f1015276b0a5f60002', '2016-03-18 15:22:49');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100004', '重庆联通公司', 'UNICOM', 1, 1, '100004', '1', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, '2c9080295276a7f1015276b0a5f60002', '2016-03-18 15:22:53');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100005', '澳门联通公司', 'UNICOM', 1, 1, '100005', '1', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, '2015-07-28 16:59:50');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100007', '安徽省联通公司', 'UNICOM', 1, 1, '100007', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100009', '甘肃省联通公司', 'UNICOM', 1, 1, '100009', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100010', '广东省联通公司', 'UNICOM', 1, 1, '100010', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100012', '贵州省联通公司', 'UNICOM', 1, 1, '100012', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100013', '海南省联通公司', 'UNICOM', 1, 1, '100013', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100014', '河北省联通公司', 'UNICOM', 1, 1, '100014', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100015', '河南省联通公司', 'UNICOM', 1, 1, '100015', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100017', '湖南省联通公司', 'UNICOM', 1, 1, '100017', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100018', '江苏省联通公司', 'UNICOM', 1, 1, '100018', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100019', '江西省联通公司', 'UNICOM', 1, 1, '100019', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100021', '辽宁省联通公司', 'UNICOM', 1, 1, '100021', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100023', '青海省联通公司', 'UNICOM', 1, 1, '100023', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100024', '山东省联通公司', 'UNICOM', 1, 1, '100024', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100025', '山西省联通公司', 'UNICOM', 1, 1, '100025', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
commit;
prompt 200 records committed...
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100026', '陕西省联通公司', 'UNICOM', 1, 1, '100026', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100028', '四川省联通公司', 'UNICOM', 1, 1, '100028', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100029', '西藏省联通公司', 'UNICOM', 1, 1, '100029', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100030', '新疆省联通公司', 'UNICOM', 1, 1, '100030', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, '2016-01-26 14:25:16');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100033', '黑龙江省联通公司', 'UNICOM', 1, 1, '100033', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, '2015-07-23 17:50:30');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000550', '滁州市联通分公司', 'UNICOM', 1, 1, '2000550', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000551', '合肥市联通分公司', 'UNICOM', 1, 1, '2000551', '3', 'ORG100007', 'ORG100007', 3, 0, 1, 0, null, null, null, null, '2015-07-22 10:19:34');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000552', '蚌埠市联通分公司', 'UNICOM', 1, 1, '2000552', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000554', '淮南市联通分公司', 'UNICOM', 1, 1, '2000554', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000555', '马鞍山联通分公司', 'UNICOM', 1, 1, '2000555', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000556', '安庆市联通分公司', 'UNICOM', 1, 1, '2000556', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000557', '宿州市联通分公司', 'UNICOM', 1, 1, '2000557', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000559', '黄山市联通分公司', 'UNICOM', 1, 1, '2000559', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000561', '淮北市联通分公司', 'UNICOM', 1, 1, '2000561', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000563', '宣城市联通分公司', 'UNICOM', 1, 1, '2000563', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000564', '六安市联通分公司', 'UNICOM', 1, 1, '2000564', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000566', '贵池市联通分公司', 'UNICOM', 1, 1, '2000566', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2010591', '福州市联通分公司', 'UNICOM', 1, 1, '2010591', '3', 'ORG100008', 'ORG100008', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2010592', '厦门市联通分公司', 'UNICOM', 1, 1, '2010592', '3', 'ORG100008', 'ORG100008', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2010593', '宁德市联通分公司', 'UNICOM', 1, 1, '2010593', '3', 'ORG100008', 'ORG100008', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG20105951', '泉州市联通分公司', 'UNICOM', 1, 1, '20105951', '3', 'ORG100008', 'ORG100008', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG20105952', '晋江市联通分公司', 'UNICOM', 1, 1, '20105952', '3', 'ORG100008', 'ORG100008', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2010596', '漳州市联通分公司', 'UNICOM', 1, 1, '2010596', '3', 'ORG100008', 'ORG100008', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2010597', '龙岩市联通分公司', 'UNICOM', 1, 1, '2010597', '3', 'ORG100008', 'ORG100008', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2010599', '南平市联通分公司', 'UNICOM', 1, 1, '2010599', '3', 'ORG100008', 'ORG100008', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2020931', '兰州市联通分公司', 'UNICOM', 1, 1, '2020931', '3', 'ORG100009', 'ORG100009', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2020932', '定西市联通分公司', 'UNICOM', 1, 1, '2020932', '3', 'ORG100009', 'ORG100009', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2020933', '平凉市联通分公司', 'UNICOM', 1, 1, '2020933', '3', 'ORG100009', 'ORG100009', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2020935', '武威市联通分公司', 'UNICOM', 1, 1, '2020935', '3', 'ORG100009', 'ORG100009', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2020936', '张掖市联通分公司', 'UNICOM', 1, 1, '2020936', '3', 'ORG100009', 'ORG100009', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2020937', '酒泉市联通分公司', 'UNICOM', 1, 1, '2020937', '3', 'ORG100009', 'ORG100009', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2020938', '天水市联通分公司', 'UNICOM', 1, 1, '2020938', '3', 'ORG100009', 'ORG100009', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2020941', '甘南州联通分公司', 'UNICOM', 1, 1, '2020941', '3', 'ORG100009', 'ORG100009', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030660', '汕尾市联通分公司', 'UNICOM', 1, 1, '2030660', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2010598', '三明市联通分公司', 'UNICOM', 1, 1, '2010598', '3', 'ORG100008', 'ORG100008', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030200', '广州市联通分公司', 'UNICOM', 1, 1, '2030200', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030759', '湛江市联通分公司', 'UNICOM', 1, 1, '2030759', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2040775', '玉林市联通分公司', 'UNICOM', 1, 1, '2040775', '3', 'ORG100011', 'ORG100011', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG20608981', '海口市联通分公司', 'UNICOM', 1, 1, '20608981', '3', 'ORG100013', 'ORG100013', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100001', '北京联通公司', 'UNICOM', 1, 1, '100001', '1', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, '2c9080295276a7f1015276b0a5f60002', '2016-03-02 14:11:59');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100011', '广西省联通公司', 'UNICOM', 1, 1, '100011', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100022', '宁夏省联通公司', 'UNICOM', 1, 1, '100022', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG100031', '浙江省联通公司', 'UNICOM', 1, 1, '100031', '2', 'ORG000000', 'ORG000000', 2, 0, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2070318', '衡水市联通分公司', 'UNICOM', 1, 1, '2070318', '3', 'ORG100014', 'ORG100014', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080377', '南阳市联通分公司', 'UNICOM', 1, 1, '2080377', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2090710', '襄城市联通分公司', 'UNICOM', 1, 1, '2090710', '3', 'ORG100016', 'ORG100016', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2090724', '荆门市联通分公司', 'UNICOM', 1, 1, '2090724', '3', 'ORG100016', 'ORG100016', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2100739', '郴州市联通分公司', 'UNICOM', 1, 1, '2100739', '3', 'ORG100017', 'ORG100017', 3, 1, 1, 8, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2110515', '盐城市联通分公司', 'UNICOM', 1, 1, '2110515', '3', 'ORG100018', 'ORG100018', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2120793', '上饶市联通分公司', 'UNICOM', 1, 1, '2120793', '3', 'ORG100019', 'ORG100019', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2130434', '四平市联通分公司', 'UNICOM', 1, 1, '2130434', '3', 'ORG100020', 'ORG100020', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2140413', '抚顺市联通分公司', 'UNICOM', 1, 1, '2140413', '3', 'ORG100021', 'ORG100021', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2150953', '吴忠市联通分公司', 'UNICOM', 1, 1, '2150953', '3', 'ORG100022', 'ORG100022', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2170534', '德州市联通分公司', 'UNICOM', 1, 1, '2170534', '3', 'ORG100024', 'ORG100024', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2180355', '长治市联通分公司', 'UNICOM', 1, 1, '2180355', '3', 'ORG100025', 'ORG100025', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2190914', '商洛市联通分公司', 'UNICOM', 1, 1, '2190914', '3', 'ORG100026', 'ORG100026', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2200874', '曲靖市联通分公司', 'UNICOM', 1, 1, '2200874', '3', 'ORG100027', 'ORG100027', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210280', '成都市联通分公司', 'UNICOM', 1, 1, '2210280', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210826', '广安市联通分公司', 'UNICOM', 1, 1, '2210826', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2210839', '广元市联通分公司', 'UNICOM', 1, 1, '2210839', '3', 'ORG100028', 'ORG100028', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2230977', '阿拉尔市联通分公司', 'UNICOM', 1, 1, '2230977', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG22309981', '喀什市联通分公司', 'UNICOM', 1, 1, '22309981', '3', 'ORG100030', 'ORG100030', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2240578', '丽水市联通分公司', 'UNICOM', 1, 1, '2240578', '3', 'ORG100031', 'ORG100031', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2250479', '锡林浩特联通分公司', 'UNICOM', 1, 1, '2250479', '3', 'ORG100032', 'ORG100032', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2000558', '阜阳市联通分公司', 'UNICOM', 1, 1, '2000558', '3', 'ORG100007', 'ORG100007', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2010594', '莆田市联通分公司', 'UNICOM', 1, 1, '2010594', '3', 'ORG100008', 'ORG100008', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG000000', '中国联通集团公司', 'UNICOM', 1, 1, '000000', '0', null, null, 1, 0, 1, 0, null, null, null, '100000', '2016-03-18 15:22:32');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030661', '潮阳市联通分公司', 'UNICOM', 1, 1, '2030661', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030662', '阳江市联通分公司', 'UNICOM', 1, 1, '2030662', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030751', '韶关市联通分公司', 'UNICOM', 1, 1, '2030751', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030752', '惠州市联通分公司', 'UNICOM', 1, 1, '2030752', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030753', '梅州市联通分公司', 'UNICOM', 1, 1, '2030753', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030754', '汕头市联通分公司', 'UNICOM', 1, 1, '2030754', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030755', '深圳市联通分公司', 'UNICOM', 1, 1, '2030755', '3', 'ORG100010', 'ORG100010', 3, 0, 1, 0, null, null, null, null, '2015-07-29 14:57:19');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030757', '佛山市联通分公司', 'UNICOM', 1, 1, '2030757', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030758', '肇庆市联通分公司', 'UNICOM', 1, 1, '2030758', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030760', '中山市联通分公司', 'UNICOM', 1, 1, '2030760', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030763', '清远市联通分公司', 'UNICOM', 1, 1, '2030763', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030765', '顺德市联通分公司', 'UNICOM', 1, 1, '2030765', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030766', '云浮市联通分公司', 'UNICOM', 1, 1, '2030766', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030768', '潮州市联通分公司', 'UNICOM', 1, 1, '2030768', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2030769', '东莞市联通分公司', 'UNICOM', 1, 1, '2030769', '3', 'ORG100010', 'ORG100010', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2040770', '防城港联通分公司', 'UNICOM', 1, 1, '2040770', '3', 'ORG100011', 'ORG100011', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2040772', '柳州市联通分公司', 'UNICOM', 1, 1, '2040772', '3', 'ORG100011', 'ORG100011', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2040773', '桂林市联通分公司', 'UNICOM', 1, 1, '2040773', '3', 'ORG100011', 'ORG100011', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2040776', '百色市联通分公司', 'UNICOM', 1, 1, '2040776', '3', 'ORG100011', 'ORG100011', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2040777', '钦州市联通分公司', 'UNICOM', 1, 1, '2040777', '3', 'ORG100011', 'ORG100011', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2040779', '北海市联通分公司', 'UNICOM', 1, 1, '2040779', '3', 'ORG100011', 'ORG100011', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2050851', '韶关市联通分公司', 'UNICOM', 1, 1, '2050851', '3', 'ORG100012', 'ORG100012', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2050852', '惠州市联通分公司', 'UNICOM', 1, 1, '2050852', '3', 'ORG100012', 'ORG100012', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2050853', '梅州市联通分公司', 'UNICOM', 1, 1, '2050853', '3', 'ORG100012', 'ORG100012', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2050855', '深圳市联通分公司', 'UNICOM', 1, 1, '2050855', '3', 'ORG100012', 'ORG100012', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2050856', '珠海市联通分公司', 'UNICOM', 1, 1, '2050856', '3', 'ORG100012', 'ORG100012', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2050859', '湛江市联通分公司', 'UNICOM', 1, 1, '2050859', '3', 'ORG100012', 'ORG100012', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2060890', '儋州市联通分公司', 'UNICOM', 1, 1, '2060890', '3', 'ORG100013', 'ORG100013', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG20608982', '三沙市联通分公司', 'UNICOM', 1, 1, '20608982', '3', 'ORG100013', 'ORG100013', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2060899', '三亚市联通分公司', 'UNICOM', 1, 1, '2060899', '3', 'ORG100013', 'ORG100013', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2070311', '石家庄联通分公司', 'UNICOM', 1, 1, '2070311', '3', 'ORG100014', 'ORG100014', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2070312', '保定市联通分公司', 'UNICOM', 1, 1, '2070312', '3', 'ORG100014', 'ORG100014', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2070313', '张家口联通分公司', 'UNICOM', 1, 1, '2070313', '3', 'ORG100014', 'ORG100014', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2070314', '承德市联通分公司', 'UNICOM', 1, 1, '2070314', '3', 'ORG100014', 'ORG100014', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2070315', '唐山市联通分公司', 'UNICOM', 1, 1, '2070315', '3', 'ORG100014', 'ORG100014', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2070316', '廊坊市联通分公司', 'UNICOM', 1, 1, '2070316', '3', 'ORG100014', 'ORG100014', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2070319', '邢台市联通分公司', 'UNICOM', 1, 1, '2070319', '3', 'ORG100014', 'ORG100014', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080370', '商丘市联通分公司', 'UNICOM', 1, 1, '2080370', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080371', '郑州市联通分公司', 'UNICOM', 1, 1, '2080371', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080372', '安阳市联通分公司', 'UNICOM', 1, 1, '2080372', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080373', '新乡市联通分公司', 'UNICOM', 1, 1, '2080373', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080374', '许昌市联通分公司', 'UNICOM', 1, 1, '2080374', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080375', '平顶山联通分公司', 'UNICOM', 1, 1, '2080375', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080376', '信阳市联通分公司', 'UNICOM', 1, 1, '2080376', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080378', '开封市联通分公司', 'UNICOM', 1, 1, '2080378', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080392', '鹤壁市联通分公司', 'UNICOM', 1, 1, '2080392', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080393', '濮阳市联通分公司', 'UNICOM', 1, 1, '2080393', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080394', '周口市联通分公司', 'UNICOM', 1, 1, '2080394', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080395', '漯河市联通分公司', 'UNICOM', 1, 1, '2080395', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080396', '驻马店联通分公司', 'UNICOM', 1, 1, '2080396', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2080398', '三门峡联通分公司', 'UNICOM', 1, 1, '2080398', '3', 'ORG100015', 'ORG100015', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2090270', '武汉市联通分公司', 'UNICOM', 1, 1, '2090270', '3', 'ORG100016', 'ORG100016', 3, 0, 1, 0, null, null, null, null, '2015-07-27 17:02:51');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2090711', '鄂州市联通分公司', 'UNICOM', 1, 1, '2090711', '3', 'ORG100016', 'ORG100016', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2090713', '黄州市联通分公司', 'UNICOM', 1, 1, '2090713', '3', 'ORG100016', 'ORG100016', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2090715', '咸宁市联通分公司', 'UNICOM', 1, 1, '2090715', '3', 'ORG100016', 'ORG100016', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2090716', '荆沙市联通分公司', 'UNICOM', 1, 1, '2090716', '3', 'ORG100016', 'ORG100016', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2090717', '宜昌市联通分公司', 'UNICOM', 1, 1, '2090717', '3', 'ORG100016', 'ORG100016', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2090718', '恩施市联通分公司', 'UNICOM', 1, 1, '2090718', '3', 'ORG100016', 'ORG100016', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2090719', '十堰市联通分公司', 'UNICOM', 1, 1, '2090719', '3', 'ORG100016', 'ORG100016', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2090722', '随枣市联通分公司', 'UNICOM', 1, 1, '2090722', '3', 'ORG100016', 'ORG100016', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2090728', '江汉市联通分公司', 'UNICOM', 1, 1, '2090728', '3', 'ORG100016', 'ORG100016', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2100730', '长沙市联通分公司', 'UNICOM', 1, 1, '2100730', '3', 'ORG100017', 'ORG100017', 3, 1, 1, 1, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2100732', '湘潭市联通分公司', 'UNICOM', 1, 1, '2100732', '3', 'ORG100017', 'ORG100017', 3, 1, 1, 3, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2100733', '衡阳市联通分公司', 'UNICOM', 1, 1, '2100733', '3', 'ORG100017', 'ORG100017', 3, 1, 1, 4, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2100735', '岳阳市联通分公司', 'UNICOM', 1, 1, '2100735', '3', 'ORG100017', 'ORG100017', 3, 1, 1, 6, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2100736', '常德市联通分公司', 'UNICOM', 1, 1, '2100736', '3', 'ORG100017', 'ORG100017', 3, 1, 1, 7, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2100737', '张家界市联通分公司', 'UNICOM', 1, 1, '2100737', '3', 'ORG100017', 'ORG100017', 3, 1, 1, 10, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2100738', '益阳市联通分公司', 'UNICOM', 1, 1, '2100738', '3', 'ORG100017', 'ORG100017', 3, 1, 1, 11, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2100743', '永州市联通分公司', 'UNICOM', 1, 1, '2100743', '3', 'ORG100017', 'ORG100017', 3, 1, 1, 12, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2100744', '怀化市联通分公司', 'UNICOM', 1, 1, '2100744', '3', 'ORG100017', 'ORG100017', 3, 1, 1, 13, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2100746', '湘西自治州联通分公司', 'UNICOM', 1, 1, '2100746', '3', 'ORG100017', 'ORG100017', 3, 1, 1, 14, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG211025', '南京市联通分公司', 'UNICOM', 1, 1, '211025', '3', 'ORG100018', 'ORG100018', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2110510', '无锡市联通分公司', 'UNICOM', 1, 1, '2110510', '3', 'ORG100018', 'ORG100018', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2110511', '镇江市联通分公司', 'UNICOM', 1, 1, '2110511', '3', 'ORG100018', 'ORG100018', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2110513', '南通市联通分公司', 'UNICOM', 1, 1, '2110513', '3', 'ORG100018', 'ORG100018', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2110514', '扬州市联通分公司', 'UNICOM', 1, 1, '2110514', '3', 'ORG100018', 'ORG100018', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2110516', '徐州市联通分公司', 'UNICOM', 1, 1, '2110516', '3', 'ORG100018', 'ORG100018', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG21105171', '淮阴市联通分公司', 'UNICOM', 1, 1, '21105171', '3', 'ORG100018', 'ORG100018', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2110518', '连云港联通分公司', 'UNICOM', 1, 1, '2110518', '3', 'ORG100018', 'ORG100018', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2110519', '常州市联通分公司', 'UNICOM', 1, 1, '2110519', '3', 'ORG100018', 'ORG100018', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2110523', '泰州市联通分公司', 'UNICOM', 1, 1, '2110523', '3', 'ORG100018', 'ORG100018', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2120701', '鹰潭市联通分公司', 'UNICOM', 1, 1, '2120701', '3', 'ORG100019', 'ORG100019', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2120790', '新余市联通分公司', 'UNICOM', 1, 1, '2120790', '3', 'ORG100019', 'ORG100019', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2120791', '南昌市联通分公司', 'UNICOM', 1, 1, '2120791', '3', 'ORG100019', 'ORG100019', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2120794', '临川市联通分公司', 'UNICOM', 1, 1, '2120794', '3', 'ORG100019', 'ORG100019', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2120795', '宜春市联通分公司', 'UNICOM', 1, 1, '2120795', '3', 'ORG100019', 'ORG100019', 3, 0, 1, 0, null, null, null, '2c9080295276a7f1015276b0a5f60002', '2016-03-02 15:17:18');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2120796', '吉安市联通分公司', 'UNICOM', 1, 1, '2120796', '3', 'ORG100019', 'ORG100019', 3, 0, 1, 0, null, null, null, '2c9080295276a7f1015276b0a5f60002', '2016-03-02 15:18:08');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2120798', '景德镇联通分公司', 'UNICOM', 1, 1, '2120798', '3', 'ORG100019', 'ORG100019', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2120799', '萍乡市联通分公司', 'UNICOM', 1, 1, '2120799', '3', 'ORG100019', 'ORG100019', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2130431', '长春市联通分公司', 'UNICOM', 1, 1, '2130431', '3', 'ORG100020', 'ORG100020', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2130432', '吉林市联通分公司', 'UNICOM', 1, 1, '2130432', '3', 'ORG100020', 'ORG100020', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2130433', '延吉市联通分公司', 'UNICOM', 1, 1, '2130433', '3', 'ORG100020', 'ORG100020', 3, 0, 1, 0, null, null, null, null, '2015-07-27 17:10:05');
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2130435', '通化市联通分公司', 'UNICOM', 1, 1, '2130435', '3', 'ORG100020', 'ORG100020', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2130437', '辽源市联通分公司', 'UNICOM', 1, 1, '2130437', '3', 'ORG100020', 'ORG100020', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2130438', '松原市联通分公司', 'UNICOM', 1, 1, '2130438', '3', 'ORG100020', 'ORG100020', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2130440', '珲春市联通分公司', 'UNICOM', 1, 1, '2130440', '3', 'ORG100020', 'ORG100020', 3, 1, 1, 0, null, null, null, null, null);
insert into TBL_SYS_ORG (ORG_ID, ORG_NAME, DOMAIN_ID, IS_ORG, ENABLED, AREA_CODE, ORG_TYPE, PARENT_ID, ROOT_ID, LEVEL_NO, IS_LEAF, IS_SYSTEM, SORT_NO, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ORG2140240', '沈阳市联通分公司', 'UNICOM', 1, 1, '2140240', '3', 'ORG100021', 'ORG100021', 3, 1, 1, 0, null, null, null, null, null);
commit;
prompt 364 records loaded
prompt Loading TBL_SYS_PARAMETER...
insert into TBL_SYS_PARAMETER (SYSTEM_PARAMETERS_ID, CODE_TYPE, NAME, CODE, IS_SYSTEM, DOMAIN_ID, ORG_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, ENABLED_TIME, DISABLED_TIME)
values ('2c9080ca4e516f4f014e517177550107', 'SYS_NORMAL_CONFIG', '数据库配置', 'DB', 1, 'SYS', null, 1, null, null, null, null, '2015-07-03 09:57:58', 2, null, null);
insert into TBL_SYS_PARAMETER (SYSTEM_PARAMETERS_ID, CODE_TYPE, NAME, CODE, IS_SYSTEM, DOMAIN_ID, ORG_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, ENABLED_TIME, DISABLED_TIME)
values ('2c9080ca4e516f4f014e5171aaa20112', 'SYS_NORMAL_CONFIG', '启动配置', 'START', 1, 'SYS', null, 1, null, null, null, null, '2015-07-03 09:57:49', 12, null, null);
insert into TBL_SYS_PARAMETER (SYSTEM_PARAMETERS_ID, CODE_TYPE, NAME, CODE, IS_SYSTEM, DOMAIN_ID, ORG_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, ENABLED_TIME, DISABLED_TIME)
values ('2c9080ca4e516f4f014e5171ea84011d', 'SYS_NORMAL_CONFIG', '后台配置', 'BACK_END', 1, 'SYS', null, 1, null, null, null, null, '2015-07-03 09:57:58', 1, null, null);
insert into TBL_SYS_PARAMETER (SYSTEM_PARAMETERS_ID, CODE_TYPE, NAME, CODE, IS_SYSTEM, DOMAIN_ID, ORG_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, ENABLED_TIME, DISABLED_TIME)
values ('2c9080ca4e231134014e231c9277009b', 'SYS_ORDER_CONFIG', '黑名单', '5', 1, 'SYS', null, 1, '1', null, null, null, '2015-06-26 11:45:53', 2, null, null);
insert into TBL_SYS_PARAMETER (SYSTEM_PARAMETERS_ID, CODE_TYPE, NAME, CODE, IS_SYSTEM, DOMAIN_ID, ORG_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, ENABLED_TIME, DISABLED_TIME)
values ('2c9080ca4e1fb003014e1fb0f4e90000', 'SYS_ORDER_CONFIG', '白名单', '2', 1, 'SYS', null, 1, null, null, '2015-06-23 17:25:18', null, '2015-06-26 11:53:31', 1, null, null);
insert into TBL_SYS_PARAMETER (SYSTEM_PARAMETERS_ID, CODE_TYPE, NAME, CODE, IS_SYSTEM, DOMAIN_ID, ORG_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, ENABLED_TIME, DISABLED_TIME)
values ('ff8080814ece7432014ece78278d0001', 'SysConfig', 'isCheckVaild', 'true', 0, 'SYS', null, 0, '是否登陆验证码', null, '2015-07-27 15:44:17', '100000', '2016-02-25 17:11:29', 2, null, '2016-02-25 17:11:29');
insert INTO TBL_SYS_PARAMETER (SYSTEM_PARAMETERS_ID, CODE_TYPE, NAME, CODE, IS_SYSTEM, DOMAIN_ID, ORG_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, ENABLED_TIME, DISABLED_TIME)
VALUES ('2c9080e356977bcc015697829fcd0002', 'SysConfig', 'isCheckOrg', 'true', '0', NULL, NULL, '1', '是否启用机构', NULL, '2016-08-17 15:58:55', '100000', '2016-08-19 09:19:07', '4', '2016-08-18 10:12:20', NULL);
insert INTO TBL_SYS_PARAMETER (SYSTEM_PARAMETERS_ID, CODE_TYPE, NAME, CODE, IS_SYSTEM, DOMAIN_ID, ORG_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, ENABLED_TIME, DISABLED_TIME)
VALUES ('2c90803c577542aa01577553553a003c', 'SysConfig', 'noticeMode', 'true', '0', 'SYS', NULL, '1', '判断业务通知是通过哪种方式获取的', '2c9080b6532bc27201532ca4380402fb', '2016-09-29 17:42:58', NULL, '2016-09-29 17:42:58', '10', '2016-09-29 17:42:58', NULL);
insert into TBL_SYS_PARAMETER (SYSTEM_PARAMETERS_ID, CODE_TYPE, NAME, CODE, IS_SYSTEM, DOMAIN_ID, ORG_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, ENABLED_TIME, DISABLED_TIME)
values ('ff8080814ed4a893014ed4b09f830001', 'SysConfig', 'lockAccountTime', '5', 0, 'SYS', null, 1, '用于系统登陆时连续输入密码错误 锁定账号的时间设置单位分钟', null, '2015-07-28 20:43:41', null, '2015-08-04 15:12:15', 1, null, null);
insert into TBL_SYS_PARAMETER (SYSTEM_PARAMETERS_ID, CODE_TYPE, NAME, CODE, IS_SYSTEM, DOMAIN_ID, ORG_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, ENABLED_TIME, DISABLED_TIME)
values ('ff8080814ed4a893014ed4b399b90002', 'SysConfig', 'loginErrorMaxNums', '3', 0, 'SYS', null, 1, '登陆时错误多少次即锁定用户账号', null, '2015-07-28 20:46:56', null, '2015-07-28 20:46:56', 3, null, null);
insert into TBL_SYS_PARAMETER (SYSTEM_PARAMETERS_ID, CODE_TYPE, NAME, CODE, IS_SYSTEM, DOMAIN_ID, ORG_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, ENABLED_TIME, DISABLED_TIME)
values ('2c908029527c132201527c4bdd8e0005', 'ss', '1', '1', 0, 'UNICOM', null, 1, '1', '100000', '2016-01-26 12:58:12', null, '2016-01-26 12:58:12', 2, '2016-01-26 12:58:12', null);
insert into TBL_SYS_PARAMETER (SYSTEM_PARAMETERS_ID, CODE_TYPE, NAME, CODE, IS_SYSTEM, DOMAIN_ID, ORG_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, ENABLED_TIME, DISABLED_TIME)
values ('2c908029527c132201527c4b00770004', 'ss', '11', '11', 0, 'UNICOM', null, 0, '1', '100000', '2016-01-26 12:57:16', '100000', '2016-01-26 12:58:23', 1, null, '2016-01-26 12:58:23');
commit;
prompt 10 records loaded
prompt Loading TBL_SYS_PRIVATE_KEY...
insert into TBL_SYS_PRIVATE_KEY (PRIVATE_KEY_ID, PRIVATE_KEY, PRIVATE_KEY_VERSION, USER_ID, SWITCH, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, DOMAIN_ID)
values ('2c9080af537e15fd01537e328c080002', 'haZETatSes', '1', '2c9080af537e15fd01537e2fa0bf0001', 'on', null, null, '100000', '2016-03-16 14:52:35', null, '2016-03-16 14:52:35', null);
commit;
prompt 1 records loaded
prompt Loading TBL_SYS_ROLE...
insert into TBL_SYS_ROLE (ROLE_ID, DOMAIN_ID, ORG_ID, ROLE_NAME, ROLE_TYPE, IS_SYSTEM, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ff8080814eb1075b014eb3b39ab6003a', 'UNICOM', 'ORG2000550', '市公司管理员', '3', 0, 1, null, null, '2015-07-22 10:59:29', null, null);
insert into TBL_SYS_ROLE (ROLE_ID, DOMAIN_ID, ORG_ID, ROLE_NAME, ROLE_TYPE, IS_SYSTEM, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('1', 'UNICOM', null, '第三方系统', 'REST', 0, 1, '第三方系统', null, null, null, null);
insert into TBL_SYS_ROLE (ROLE_ID, DOMAIN_ID, ORG_ID, ROLE_NAME, ROLE_TYPE, IS_SYSTEM, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ROLE000001', 'UNICOM', 'ORG000000', '超级管理员', null, 1, 1, null, null, null, null, null);
insert into TBL_SYS_ROLE (ROLE_ID, DOMAIN_ID, ORG_ID, ROLE_NAME, ROLE_TYPE, IS_SYSTEM, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ROLE000002', 'UNICOM', 'ORG000000', '省管理员', null, 1, 1, null, null, null, null, null);
insert into TBL_SYS_ROLE (ROLE_ID, DOMAIN_ID, ORG_ID, ROLE_NAME, ROLE_TYPE, IS_SYSTEM, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ROLE000003', 'UNICOM', 'ORG100016', '武汉市管理员', null, 1, 1, null, null, null, null, null);
insert into TBL_SYS_ROLE (ROLE_ID, DOMAIN_ID, ORG_ID, ROLE_NAME, ROLE_TYPE, IS_SYSTEM, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('2c9080295280eb9001528111822d0002', 'UNICOM', 'ORG100030', '陈瑶测试哦', null, 0, 1, '陈瑶测试哦', null, '2016-01-27 11:12:34', null, null);
insert into TBL_SYS_ROLE (ROLE_ID, DOMAIN_ID, ORG_ID, ROLE_NAME, ROLE_TYPE, IS_SYSTEM, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME)
values ('ff8080814ffd19c5014ffd1bccaf0002', 'UNICOM', 'ORG100030', 'sksdfsssf', null, 0, 1, null, null, '2015-09-24 10:08:21', null, null);
commit;
prompt 7 records loaded
prompt Loading TBL_SYS_ROLE_ASSIGN...
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('10', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('100', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('100000', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('11', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('12', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('13', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('14', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('15', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('16', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('17', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('18', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('19', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('20', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('21', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('22', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('23', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('24', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('25', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('26', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('27', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('28', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('29', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('2c9080295276a7f1015276b0a5f60002', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('2c9080295277e5ec015277e90bed0007', '2c90809f4ffd2243014ffd240a360001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('2c9080aa517fb00001517fb54d010001', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('2c9080af537e15fd01537e2fa0bf0001', '1');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('2c9080c04fd5201f014fd520f9910001', '2c90809f4ffd2243014ffd240a360001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('2c9080c04fd5201f014fd520f9910001', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('2c9080c04fd5201f014fd5219e500002', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('2c9080d24fef1219014fef15c56d0001', '1');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('2c9080d651d6e4cf0151d6ed24a60007', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('2c9080de51a4c6150151a5098ca30003', '2c9080295276a7f1015276caae5b0007');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('2c9080de51a4c6150151a5098ca30003', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('2c9080de51a4c6150151a50b1f880004', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('2c9080de51a4c6150151a50b70d90005', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('30', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('31', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('32', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('33', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('34', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('35', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('36', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('37', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('38', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('39', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('4', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('40', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('41', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('42', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('43', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('44', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('45', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('46', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('47', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('48', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('49', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('50', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('51', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('52', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('53', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('54', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('55', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('56', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('57', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('58', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('59', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('6', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('60', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('61', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('62', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('63', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('64', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('65', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('66', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('67', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('68', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('69', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('7', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('70', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('71', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('72', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('73', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('74', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('75', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('76', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('77', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('78', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('79', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('8', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('80', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('81', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('82', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('83', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('84', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('85', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('86', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('87', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('88', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('89', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('9', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('90', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('91', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('92', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('93', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('94', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('95', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('96', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('97', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('98', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('99', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('ff808081506fcce701507ded32eb0574', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('ff808081506fcce7015084777d3a1122', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('ff808081506fcce70150847842511135', 'ROLE000001');
insert into TBL_SYS_ROLE_ASSIGN (USER_ID, ROLE_ID)
values ('ff808081506fcce701508478871c1136', 'ROLE000001');
commit;
prompt 114 records loaded
prompt Loading TBL_SYS_ROLE_FUNC...
prompt Table is empty
prompt Loading TBL_SYS_SKILL...
prompt Table is empty
prompt Loading TBL_SYS_SUB_SYSTEM...
insert into TBL_SYS_SUB_SYSTEM (SUB_SYSTEM_ID, SUB_SYSTEM_NAME, SUB_SYSTEM, URL, ENABLED, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, DOMAIN_ID)
values ('2', 'hollyi8', 'hollyi8', 'http://172.16.0.96:8081/hollyi8/rest/restIfs/getRestIfs', 0, null, null, null, null, null, null);
insert into TBL_SYS_SUB_SYSTEM (SUB_SYSTEM_ID, SUB_SYSTEM_NAME, SUB_SYSTEM, URL, ENABLED, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, DOMAIN_ID)
values ('1', 'hollybeacon-personoa', 'hollybeacon-personoa', 'http://localhost:80/hollybeacon-personoa/rest/restIfs/getRestIfs', 1, null, null, null, null, null, null);
insert into TBL_SYS_SUB_SYSTEM (SUB_SYSTEM_ID, SUB_SYSTEM_NAME, SUB_SYSTEM, URL, ENABLED, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, REMARK, DOMAIN_ID)
values ('1', 'hollybeacon-personoa', 'hollybeacon-personoa', 'http://localhost:8080/hollybeacon-personoa/rest/restIfs/getRestIfs', 1, null, null, null, null, null, null);
commit;
prompt 3 records loaded
prompt Loading TBL_SYS_THIRD_ACCOUNT...
prompt Table is empty
prompt Loading TBL_SYS_TREE_CODE...
insert into TBL_SYS_TREE_CODE (code_id, domain_id, org_id, code_type, code_value, code_name, sort_no, parent_id, enabled, is_system, sub_system_id, remark, creator, create_time, last_modifier, last_modify_time, disenabled_time, enabled_time, is_by_developer, is_parent, node_path)
values ('2c90b48258f31f7d01591506acf10451', 'UNICOM', 'ORG000000', 'H1-CODE', 'H1-CODE', 'H1-CODE', null, null, 1, 0, 'HollyPersonoa', '默认首选树状菜单', '2c90801a57ffdd470157ffed6917000c', '2016-12-19 11:01:16', '2c90801a57ffdd470157ffed6917000c', '2016-12-23 17:29:09', null, null, null, 1, 'H1-CODE');
commit;
prompt 1 records loaded
prompt Loading TBL_SYS_USER...
insert into TBL_SYS_USER (USER_ID, USER_CODE, USER_NAME, USER_NO, USER_TYPE, DEPT_ID, ORG_ID, PASSWORD, PWD_VALID_DAY, BIRTHDAY, SEX, EDUCATION, TELEPHONE, OFFICEPHONE, MOBILE, EMAIL, ADDRESS, POST_CODE, CONTACT_NAME, CONTACT_PHONE, PHOTO, CERVIFICATE_TYPE, CERVIFICATE_NO, HIRE_DATE, IS_EDIT, ENABLED, EXTEND_FIELD1, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, DOMAIN_ID, IS_LOGIN, IS_SIGN)
values ('100000', 'admin', 'admin', null, '0', '15540222213C1831E0531240FADC5705', 'ORG000000', 'f5aa100da00005471f59e52cbe761a80', '2020-07-30', null, null, null, null, null, null, null, '1111        11111', null, null, null, null, null, null, '2015-11-05', null, 1, null, 'fsdfsdfdsf    fdsfsdfsdf   fdsafafafafafaf              afafafaffdsafs', null, null, null, '2015-12-30 20:13:54', 'UNICOM', null, null);
commit;
prompt 1 records loaded
prompt Loading TBL_SYS_USER_AGENT...
prompt Table is empty
prompt Loading TBL_SYS_USER_GROUP...
prompt Table is empty
prompt Enabling foreign key constraints for TBL_SYS_CURRENT_MAX...
alter table TBL_SYS_CURRENT_MAX enable constraint FK4CF6F02BED6C63C6;
prompt Enabling foreign key constraints for TBL_SYS_USER...
alter table TBL_SYS_USER enable constraint FK19841D9E44775C;
alter table TBL_SYS_USER enable constraint FK19841D9E677C0E5D;
prompt Enabling foreign key constraints for TBL_SYS_USER_AGENT...
alter table TBL_SYS_USER_AGENT enable constraint FK47249D044AFA37E2;
alter table TBL_SYS_USER_AGENT enable constraint FK47249D048ED4800A;
prompt Enabling foreign key constraints for TBL_SYS_USER_GROUP...
alter table TBL_SYS_USER_GROUP enable constraint FK477E507E4817E7BD;
alter table TBL_SYS_USER_GROUP enable constraint FK477E507E4AFA37E2;
prompt Enabling triggers for TBL_SYS_AGENT_CODE...
alter table TBL_SYS_AGENT_CODE enable all triggers;
prompt Enabling triggers for TBL_SYS_AGENT_SKILL...
alter table TBL_SYS_AGENT_SKILL enable all triggers;
prompt Enabling triggers for TBL_SYS_AUTH_FUNCTION...
alter table TBL_SYS_AUTH_FUNCTION enable all triggers;
prompt Enabling triggers for TBL_SYS_AUTH_FUNC_PERMISSION...
alter table TBL_SYS_AUTH_FUNC_PERMISSION enable all triggers;
prompt Enabling triggers for TBL_SYS_CODE_TYPE...
alter table TBL_SYS_CODE_TYPE enable all triggers;
prompt Enabling triggers for TBL_SYS_SEQUENCE...
alter table TBL_SYS_SEQUENCE enable all triggers;
prompt Enabling triggers for TBL_SYS_CURRENT_MAX...
alter table TBL_SYS_CURRENT_MAX enable all triggers;
prompt Enabling triggers for TBL_SYS_DATA_PERMISSION...
alter table TBL_SYS_DATA_PERMISSION enable all triggers;
prompt Enabling triggers for TBL_SYS_DOMAIN...
alter table TBL_SYS_DOMAIN enable all triggers;
prompt Enabling triggers for TBL_SYS_FUNCTION...
alter table TBL_SYS_FUNCTION enable all triggers;
prompt Enabling triggers for TBL_SYS_FUNC_PERMISSION...
alter table TBL_SYS_FUNC_PERMISSION enable all triggers;
prompt Enabling triggers for TBL_SYS_GENERAL_CODE...
alter table TBL_SYS_GENERAL_CODE enable all triggers;
prompt Enabling triggers for TBL_SYS_GROUP...
alter table TBL_SYS_GROUP enable all triggers;
prompt Enabling triggers for TBL_SYS_LOCKUSER_INFO...
alter table TBL_SYS_LOCKUSER_INFO enable all triggers;
prompt Enabling triggers for TBL_SYS_MEDIA_FILE...
alter table TBL_SYS_MEDIA_FILE enable all triggers;
prompt Enabling triggers for TBL_SYS_ORG...
alter table TBL_SYS_ORG enable all triggers;
prompt Enabling triggers for TBL_SYS_PARAMETER...
alter table TBL_SYS_PARAMETER enable all triggers;
prompt Enabling triggers for TBL_SYS_PRIVATE_KEY...
alter table TBL_SYS_PRIVATE_KEY enable all triggers;
prompt Enabling triggers for TBL_SYS_ROLE...
alter table TBL_SYS_ROLE enable all triggers;
prompt Enabling triggers for TBL_SYS_ROLE_ASSIGN...
alter table TBL_SYS_ROLE_ASSIGN enable all triggers;
prompt Enabling triggers for TBL_SYS_ROLE_FUNC...
alter table TBL_SYS_ROLE_FUNC enable all triggers;
prompt Enabling triggers for TBL_SYS_SKILL...
alter table TBL_SYS_SKILL enable all triggers;
prompt Enabling triggers for TBL_SYS_SUB_SYSTEM...
alter table TBL_SYS_SUB_SYSTEM enable all triggers;
prompt Enabling triggers for TBL_SYS_THIRD_ACCOUNT...
alter table TBL_SYS_THIRD_ACCOUNT enable all triggers;
prompt Enabling triggers for TBL_SYS_TREE_CODE...
alter table TBL_SYS_TREE_CODE enable all triggers;
prompt Enabling triggers for TBL_SYS_USER...
alter table TBL_SYS_USER enable all triggers;
prompt Enabling triggers for TBL_SYS_USER_AGENT...
alter table TBL_SYS_USER_AGENT enable all triggers;
prompt Enabling triggers for TBL_SYS_USER_GROUP...
alter table TBL_SYS_USER_GROUP enable all triggers;
set feedback on
set define on
prompt Done.

insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('AREA', null, 'SYS', 'DICTIONARY', '系统管理', '地市区域', 1, 'HollyPersonoa', 1, null, null, '2015-11-03 17:34:35', null, '2015-11-03 17:34:35', null, '2c90802950cc80750150ccb2b46f1234', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('VDN_TYPE', null, 'SYS', 'DICTIONARY', '系统管理', '呼叫中心', 1, 'HollyPersonoa', 1, null, null, '2015-11-03 17:34:35', null, '2015-11-03 17:34:35', null, '2c90802950cc80750150ccb2b46f4567', null);
insert into TBL_SYS_CODE_TYPE (CODE_TYPE, ORG_ID, DOMAIN_ID, CODETYPE_TYPE, MODULE_NAME, CODE_TYPE_DESC, IS_SYSTEM, SUB_SYSTEM_ID, ENABLED, REMARK, CREATOR, CREATE_TIME, LAST_MODIFIER, LAST_MODIFY_TIME, SORT_NO, CODE_TYPE_ID, IS_BY_DEVELOPER)
values ('GROUP', null, 'SYS', 'DICTIONARY', '系统管理', '工作组类型', 1, 'HollyPersonoa', 1, null, null, '2015-11-03 17:34:35', null, '2015-11-03 17:34:35', null, '2c90802950cc80750150ccb2b46f6789', null);
commit;