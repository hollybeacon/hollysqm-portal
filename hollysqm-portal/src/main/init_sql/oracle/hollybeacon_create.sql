create table QRTZ_JOB_DETAILS
(
  SCHED_NAME        VARCHAR2(120) not null,
  JOB_NAME          VARCHAR2(200) not null,
  JOB_GROUP         VARCHAR2(200) not null,
  DESCRIPTION       VARCHAR2(250),
  JOB_CLASS_NAME    VARCHAR2(250) not null,
  IS_DURABLE        VARCHAR2(1) not null,
  IS_NONCONCURRENT  VARCHAR2(1) not null,
  IS_UPDATE_DATA    VARCHAR2(1) not null,
  REQUESTS_RECOVERY VARCHAR2(1) not null,
  JOB_DATA          BLOB
);
alter table QRTZ_JOB_DETAILS
  add constraint QRTZ_JOB_DETAILS_PK primary key (SCHED_NAME, JOB_NAME, JOB_GROUP);
create index IDX_QRTZ_J_GRP on QRTZ_JOB_DETAILS (SCHED_NAME, JOB_GROUP);
create index IDX_QRTZ_J_REQ_RECOVERY on QRTZ_JOB_DETAILS (SCHED_NAME, REQUESTS_RECOVERY);



create table QRTZ_TRIGGERS
(
  SCHED_NAME     VARCHAR2(120) not null,
  TRIGGER_NAME   VARCHAR2(200) not null,
  TRIGGER_GROUP  VARCHAR2(200) not null,
  JOB_NAME       VARCHAR2(200) not null,
  JOB_GROUP      VARCHAR2(200) not null,
  DESCRIPTION    VARCHAR2(250),
  NEXT_FIRE_TIME NUMBER(13),
  PREV_FIRE_TIME NUMBER(13),
  PRIORITY       NUMBER(13),
  TRIGGER_STATE  VARCHAR2(16) not null,
  TRIGGER_TYPE   VARCHAR2(8) not null,
  START_TIME     NUMBER(13) not null,
  END_TIME       NUMBER(13),
  CALENDAR_NAME  VARCHAR2(200),
  MISFIRE_INSTR  NUMBER(2),
  JOB_DATA       BLOB
);
alter table QRTZ_TRIGGERS
  add constraint QRTZ_TRIGGERS_PK primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);
alter table QRTZ_TRIGGERS
  add constraint QRTZ_TRIGGER_TO_JOBS_FK foreign key (SCHED_NAME, JOB_NAME, JOB_GROUP)
  references QRTZ_JOB_DETAILS (SCHED_NAME, JOB_NAME, JOB_GROUP);
create index IDX_QRTZ_T_C on QRTZ_TRIGGERS (SCHED_NAME, CALENDAR_NAME);
create index IDX_QRTZ_T_G on QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_GROUP);
create index IDX_QRTZ_T_J on QRTZ_TRIGGERS (SCHED_NAME, JOB_NAME, JOB_GROUP);
create index IDX_QRTZ_T_JG on QRTZ_TRIGGERS (SCHED_NAME, JOB_GROUP);
create index IDX_QRTZ_T_NEXT_FIRE_TIME on QRTZ_TRIGGERS (SCHED_NAME, NEXT_FIRE_TIME);
create index IDX_QRTZ_T_NFT_MISFIRE on QRTZ_TRIGGERS (SCHED_NAME, MISFIRE_INSTR, NEXT_FIRE_TIME);
create index IDX_QRTZ_T_NFT_ST on QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_STATE, NEXT_FIRE_TIME);
create index IDX_QRTZ_T_NFT_ST_MISFIRE on QRTZ_TRIGGERS (SCHED_NAME, MISFIRE_INSTR, NEXT_FIRE_TIME, TRIGGER_STATE);
create index IDX_QRTZ_T_NFT_ST_MISFIRE_GRP on QRTZ_TRIGGERS (SCHED_NAME, MISFIRE_INSTR, NEXT_FIRE_TIME, TRIGGER_GROUP, TRIGGER_STATE);
create index IDX_QRTZ_T_N_G_STATE on QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_GROUP, TRIGGER_STATE);
create index IDX_QRTZ_T_N_STATE on QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP, TRIGGER_STATE);
create index IDX_QRTZ_T_STATE on QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_STATE);


create table QRTZ_BLOB_TRIGGERS
(
  SCHED_NAME    VARCHAR2(120) not null,
  TRIGGER_NAME  VARCHAR2(200) not null,
  TRIGGER_GROUP VARCHAR2(200) not null,
  BLOB_DATA     BLOB
);
alter table QRTZ_BLOB_TRIGGERS
  add constraint QRTZ_BLOB_TRIG_PK primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);
alter table QRTZ_BLOB_TRIGGERS
  add constraint QRTZ_BLOB_TRIG_TO_TRIG_FK foreign key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
  references QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);




create table QRTZ_CALENDARS
(
  SCHED_NAME    VARCHAR2(120) not null,
  CALENDAR_NAME VARCHAR2(200) not null,
  CALENDAR      BLOB not null
);
alter table QRTZ_CALENDARS
  add constraint QRTZ_CALENDARS_PK primary key (SCHED_NAME, CALENDAR_NAME);



create table QRTZ_CRON_TRIGGERS
(
  SCHED_NAME      VARCHAR2(120) not null,
  TRIGGER_NAME    VARCHAR2(200) not null,
  TRIGGER_GROUP   VARCHAR2(200) not null,
  CRON_EXPRESSION VARCHAR2(120) not null,
  TIME_ZONE_ID    VARCHAR2(80)
);
alter table QRTZ_CRON_TRIGGERS
  add constraint QRTZ_CRON_TRIG_PK primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);
alter table QRTZ_CRON_TRIGGERS
  add constraint QRTZ_CRON_TRIG_TO_TRIG_FK foreign key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
  references QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);




create table QRTZ_FIRED_TRIGGERS
(
  SCHED_NAME        VARCHAR2(120) not null,
  ENTRY_ID          VARCHAR2(95) not null,
  TRIGGER_NAME      VARCHAR2(200) not null,
  TRIGGER_GROUP     VARCHAR2(200) not null,
  INSTANCE_NAME     VARCHAR2(200) not null,
  FIRED_TIME        NUMBER(13) not null,
  SCHED_TIME        NUMBER(13) not null,
  PRIORITY          NUMBER(13) not null,
  STATE             VARCHAR2(16) not null,
  JOB_NAME          VARCHAR2(200),
  JOB_GROUP         VARCHAR2(200),
  IS_NONCONCURRENT  VARCHAR2(1),
  REQUESTS_RECOVERY VARCHAR2(1)
);
alter table QRTZ_FIRED_TRIGGERS
  add constraint QRTZ_FIRED_TRIGGER_PK primary key (SCHED_NAME, ENTRY_ID);
create index IDX_QRTZ_FT_INST_JOB_REQ_RCVRY on QRTZ_FIRED_TRIGGERS (SCHED_NAME, INSTANCE_NAME, REQUESTS_RECOVERY);
create index IDX_QRTZ_FT_JG on QRTZ_FIRED_TRIGGERS (SCHED_NAME, JOB_GROUP);
create index IDX_QRTZ_FT_J_G on QRTZ_FIRED_TRIGGERS (SCHED_NAME, JOB_NAME, JOB_GROUP);
create index IDX_QRTZ_FT_TG on QRTZ_FIRED_TRIGGERS (SCHED_NAME, TRIGGER_GROUP);
create index IDX_QRTZ_FT_TRIG_INST_NAME on QRTZ_FIRED_TRIGGERS (SCHED_NAME, INSTANCE_NAME);
create index IDX_QRTZ_FT_T_G on QRTZ_FIRED_TRIGGERS (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);



create table QRTZ_LOCKS
(
  SCHED_NAME VARCHAR2(120) not null,
  LOCK_NAME  VARCHAR2(40) not null
);
alter table QRTZ_LOCKS
  add constraint QRTZ_LOCKS_PK primary key (SCHED_NAME, LOCK_NAME);



create table QRTZ_PAUSED_TRIGGER_GRPS
(
  SCHED_NAME    VARCHAR2(120) not null,
  TRIGGER_GROUP VARCHAR2(200) not null
);
alter table QRTZ_PAUSED_TRIGGER_GRPS
  add constraint QRTZ_PAUSED_TRIG_GRPS_PK primary key (SCHED_NAME, TRIGGER_GROUP);




create table QRTZ_SCHEDULER_STATE
(
  SCHED_NAME        VARCHAR2(120) not null,
  INSTANCE_NAME     VARCHAR2(200) not null,
  LAST_CHECKIN_TIME NUMBER(13) not null,
  CHECKIN_INTERVAL  NUMBER(13) not null
);
alter table QRTZ_SCHEDULER_STATE
  add constraint QRTZ_SCHEDULER_STATE_PK primary key (SCHED_NAME, INSTANCE_NAME);



create table QRTZ_SIMPLE_TRIGGERS
(
  SCHED_NAME      VARCHAR2(120) not null,
  TRIGGER_NAME    VARCHAR2(200) not null,
  TRIGGER_GROUP   VARCHAR2(200) not null,
  REPEAT_COUNT    NUMBER(7) not null,
  REPEAT_INTERVAL NUMBER(12) not null,
  TIMES_TRIGGERED NUMBER(10) not null
);
alter table QRTZ_SIMPLE_TRIGGERS
  add constraint QRTZ_SIMPLE_TRIG_PK primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);
alter table QRTZ_SIMPLE_TRIGGERS
  add constraint QRTZ_SIMPLE_TRIG_TO_TRIG_FK foreign key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
  references QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);



create table QRTZ_SIMPROP_TRIGGERS
(
  SCHED_NAME    VARCHAR2(120) not null,
  TRIGGER_NAME  VARCHAR2(200) not null,
  TRIGGER_GROUP VARCHAR2(200) not null,
  STR_PROP_1    VARCHAR2(512),
  STR_PROP_2    VARCHAR2(512),
  STR_PROP_3    VARCHAR2(512),
  INT_PROP_1    NUMBER(10),
  INT_PROP_2    NUMBER(10),
  LONG_PROP_1   NUMBER(13),
  LONG_PROP_2   NUMBER(13),
  DEC_PROP_1    NUMBER(13,4),
  DEC_PROP_2    NUMBER(13,4),
  BOOL_PROP_1   VARCHAR2(1),
  BOOL_PROP_2   VARCHAR2(1)
);
alter table QRTZ_SIMPROP_TRIGGERS
  add constraint QRTZ_SIMPROP_TRIG_PK primary key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);
alter table QRTZ_SIMPROP_TRIGGERS
  add constraint QRTZ_SIMPROP_TRIG_TO_TRIG_FK foreign key (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP)
  references QRTZ_TRIGGERS (SCHED_NAME, TRIGGER_NAME, TRIGGER_GROUP);



create table TBL_CSV_IMP
(
  FLOW_ID     VARCHAR2(1000),
  NAME        VARCHAR2(50),
  EMAIL       VARCHAR2(1000),
  WX_NO       VARCHAR2(1000),
  CREATE_DATE DATE,
  SYS_ID      NUMBER
);


create table TBL_CUST_SERV_EVENT_LOG
(
  LOG_ID           VARCHAR2(1024) not null,
  SESSION_ID       VARCHAR2(1024),
  DOMAIN_ID        VARCHAR2(1024),
  DESTINATION      VARCHAR2(1024),
  CUST_SERV_ID     VARCHAR2(1024),
  EVENT            VARCHAR2(1024),
  EVENT_TIME       VARCHAR2(1024),
  TIMESTAMP        INTEGER,
  CREATOR          VARCHAR2(1024),
  CREATE_TIME      VARCHAR2(1024),
  LAST_MODIFIER    VARCHAR2(1024),
  LAST_MODIFY_TIME VARCHAR2(1024),
  REMARK           VARCHAR2(1024)
);
comment on table TBL_CUST_SERV_EVENT_LOG
  is '客服操作轨迹';
comment on column TBL_CUST_SERV_EVENT_LOG.LOG_ID
  is 'LOG_ID';
comment on column TBL_CUST_SERV_EVENT_LOG.DOMAIN_ID
  is '租户ID';
comment on column TBL_CUST_SERV_EVENT_LOG.DESTINATION
  is '所属消息通道';
comment on column TBL_CUST_SERV_EVENT_LOG.CUST_SERV_ID
  is '用户在渠道中的标示';
comment on column TBL_CUST_SERV_EVENT_LOG.EVENT
  is '事件';
comment on column TBL_CUST_SERV_EVENT_LOG.EVENT_TIME
  is '事件时间';
comment on column TBL_CUST_SERV_EVENT_LOG.TIMESTAMP
  is '时间戳';
comment on column TBL_CUST_SERV_EVENT_LOG.CREATOR
  is '创建者';
comment on column TBL_CUST_SERV_EVENT_LOG.CREATE_TIME
  is '创建时间';
comment on column TBL_CUST_SERV_EVENT_LOG.LAST_MODIFIER
  is '修改者';
comment on column TBL_CUST_SERV_EVENT_LOG.LAST_MODIFY_TIME
  is '修改时间';
comment on column TBL_CUST_SERV_EVENT_LOG.REMARK
  is '备注';
alter table TBL_CUST_SERV_EVENT_LOG
  add constraint PK_TBL_CUST_SERV_EVENT_LOG primary key (LOG_ID);



create table TBL_ECHARTS_DATA
(
  LEGEND   VARCHAR2(100),
  CATEGORY VARCHAR2(100),
  VALUE    VARCHAR2(100),
  CODE     VARCHAR2(100)
);



create table TBL_QRTZ_JOB_INFO
(
  JOB_ID           VARCHAR2(40) not null,
  JOB_TYPE         VARCHAR2(40) default '0',
  JOB_NAME         VARCHAR2(40),
  JOB_GROUP_NAME   VARCHAR2(80) default 'DEFAULT',
  JOB_STATUS       VARCHAR2(60) default '0',
  CRON_EXPRESSION  VARCHAR2(40),
  DESCRIPTION      VARCHAR2(40),
  SPRING_BEAN_ID   VARCHAR2(40),
  JOB_CLASS_NAME   VARCHAR2(400),
  METHOD_NAME      VARCHAR2(40) default 'execute',
  API_URL          VARCHAR2(400),
  CREATOR          VARCHAR2(80),
  CREATE_TIME      VARCHAR2(19),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19),
  REMARK           VARCHAR2(256),
  DOMAIN_ID        VARCHAR2(40),
  IS_CONCURRENT    VARCHAR2(40) default '0',
  JOB_DATA         VARCHAR2(400),
  TRIGGER_NAME     VARCHAR2(400),
  TRIGGER_STATE    VARCHAR2(400)
);
comment on column TBL_QRTZ_JOB_INFO.JOB_ID
  is '主键';
comment on column TBL_QRTZ_JOB_INFO.JOB_TYPE
  is 'JOB类型';
comment on column TBL_QRTZ_JOB_INFO.JOB_NAME
  is 'JOB名称';
comment on column TBL_QRTZ_JOB_INFO.JOB_GROUP_NAME
  is 'JOB分组';
comment on column TBL_QRTZ_JOB_INFO.JOB_STATUS
  is 'JOB是否添加到调度服务';
comment on column TBL_QRTZ_JOB_INFO.CRON_EXPRESSION
  is 'cron表达式';
comment on column TBL_QRTZ_JOB_INFO.DESCRIPTION
  is '描述';
comment on column TBL_QRTZ_JOB_INFO.SPRING_BEAN_ID
  is 'SPRING容器中的beanId';
comment on column TBL_QRTZ_JOB_INFO.JOB_CLASS_NAME
  is '类名';
comment on column TBL_QRTZ_JOB_INFO.METHOD_NAME
  is '方法名';
comment on column TBL_QRTZ_JOB_INFO.API_URL
  is 'HTTPURL';
comment on column TBL_QRTZ_JOB_INFO.IS_CONCURRENT
  is '是否并发';
comment on column TBL_QRTZ_JOB_INFO.TRIGGER_NAME
  is '触发器名称';
comment on column TBL_QRTZ_JOB_INFO.TRIGGER_STATE
  is '触发状态';
alter table TBL_QRTZ_JOB_INFO
  add constraint PK_JOB_INFO primary key (JOB_ID);
alter table TBL_QRTZ_JOB_INFO
  add constraint UNIQ_JOB_INFO unique (JOB_NAME, JOB_GROUP_NAME);



create table TBL_SEQUENCE
(
  SEQUENCE_ID   VARCHAR2(40) not null,
  SEQUENCE_NAME VARCHAR2(60),
  SEQUENCE_RULE VARCHAR2(512),
  MEMO          VARCHAR2(200),
  SUB_SYSTEM_ID VARCHAR2(40)
);
comment on table TBL_SEQUENCE
  is 'TBL_SEQUENCE';
comment on column TBL_SEQUENCE.SEQUENCE_ID
  is '序列ID';
comment on column TBL_SEQUENCE.SEQUENCE_NAME
  is '序列名称';
comment on column TBL_SEQUENCE.SEQUENCE_RULE
  is '序列规则';
comment on column TBL_SEQUENCE.MEMO
  is '备注';
comment on column TBL_SEQUENCE.SUB_SYSTEM_ID
  is '子系统标识';
alter table TBL_SEQUENCE
  add constraint PK_TBL_SEQUENCE primary key (SEQUENCE_ID);



create table TBL_SYS_ACTION_LOG
(
  ACTION_LOG_ID     VARCHAR2(40),
  OPERATE_USER_ID   VARCHAR2(400),
  OPERATE_ACCOUNT   VARCHAR2(500),
  OPERATION_CONTENT VARCHAR2(1000),
  OPERATE_TYPE      VARCHAR2(100),
  MODULE_NAME       VARCHAR2(100),
  ORG_ID            VARCHAR2(40),
  DOMAIN_ID         VARCHAR2(40),
  COMPUTER_NAME     VARCHAR2(30),
  COMPUTER_IP       VARCHAR2(20),
  ENABLED           NUMBER(1),
  REMARK            VARCHAR2(256),
  CREATOR           VARCHAR2(80),
  CREATE_TIME       VARCHAR2(19) default to_char(sysdate, 'yyyy-MM-dd hh24:mi'),
  LAST_MODIFIER     VARCHAR2(80),
  LAST_MODIFY_TIME  VARCHAR2(19),
  CLASS_NAME        VARCHAR2(1000),
  METHOD_INFO       VARCHAR2(1000),
  COST_TIME         NUMBER(10),
  IN_PARAM          CLOB,
  RT_VALUE          CLOB,
  URL               VARCHAR2(1000),
  BROWSER           VARCHAR2(1000),
  IS_SUCCESS        VARCHAR2(1)
);
comment on table TBL_SYS_ACTION_LOG
  is '操作日志';
comment on column TBL_SYS_ACTION_LOG.ORG_ID
  is 'IS_ORG=1的机构ID';
comment on column TBL_SYS_ACTION_LOG.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_ACTION_LOG.COMPUTER_NAME
  is '计算机名称';
comment on column TBL_SYS_ACTION_LOG.COMPUTER_IP
  is '计算机IP';
comment on column TBL_SYS_ACTION_LOG.ENABLED
  is '状态，0为正常1为停用';
comment on column TBL_SYS_ACTION_LOG.REMARK
  is '备注';
comment on column TBL_SYS_ACTION_LOG.CREATOR
  is '创建者';
comment on column TBL_SYS_ACTION_LOG.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_ACTION_LOG.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_ACTION_LOG.LAST_MODIFY_TIME
  is '最后修改时间';
comment on column TBL_SYS_ACTION_LOG.CLASS_NAME
  is '类名';
comment on column TBL_SYS_ACTION_LOG.METHOD_INFO
  is '方法名';
comment on column TBL_SYS_ACTION_LOG.COST_TIME
  is '耗时';
comment on column TBL_SYS_ACTION_LOG.IN_PARAM
  is '入参';
comment on column TBL_SYS_ACTION_LOG.RT_VALUE
  is '返回值';
comment on column TBL_SYS_ACTION_LOG.URL
  is 'URL';
comment on column TBL_SYS_ACTION_LOG.BROWSER
  is '浏览器';
comment on column TBL_SYS_ACTION_LOG.IS_SUCCESS
  is '是否成功';
alter table TBL_SYS_ACTION_LOG
  add constraint TBL_SYS_ACTION_LOG_PK primary key (ACTION_LOG_ID);
alter table TBL_SYS_ACTION_LOG
  add constraint FSDEGGWWWW
  check ("ACTION_LOG_ID" IS NOT NULL);
create index IDX_ACTION_LOG_CT on TBL_SYS_ACTION_LOG (SUBSTR(CREATE_TIME,1,10));
create index IDX_ACTION_LOG_CT_ORDER on TBL_SYS_ACTION_LOG (CREATE_TIME);
create index IDX_ACTION_LOG_IS_SUC on TBL_SYS_ACTION_LOG (IS_SUCCESS);




create table TBL_SYS_AGENT_CODE
(
  AGENT_ID         VARCHAR2(40),
  AGENT_CODE       VARCHAR2(12),
  DOMAIN_ID        VARCHAR2(40),
  ORG_ID           VARCHAR2(40),
  VDN_ID           VARCHAR2(8),
  TEL_CODE         VARCHAR2(12),
  TELEPHONE        VARCHAR2(20),
  SERVER_TYPE      VARCHAR2(2),
  SERVERID         VARCHAR2(40),
  CREATOR          VARCHAR2(80),
  CREATE_TIME      VARCHAR2(19),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19),
  REMARK           VARCHAR2(255 CHAR),
  ENABLED          NUMBER(1)
);
comment on table TBL_SYS_AGENT_CODE
  is '工号表';
comment on column TBL_SYS_AGENT_CODE.AGENT_CODE
  is '工号ID';
comment on column TBL_SYS_AGENT_CODE.DOMAIN_ID
  is '租户ID';
comment on column TBL_SYS_AGENT_CODE.ORG_ID
  is 'IS_ORG=1的机构ID';
comment on column TBL_SYS_AGENT_CODE.VDN_ID
  is 'VDNID';
comment on column TBL_SYS_AGENT_CODE.TEL_CODE
  is '分机号';
comment on column TBL_SYS_AGENT_CODE.TELEPHONE
  is '绑定号码';
comment on column TBL_SYS_AGENT_CODE.SERVER_TYPE
  is '1:mcp、2:agw';
comment on column TBL_SYS_AGENT_CODE.SERVERID
  is '服务器id';
comment on column TBL_SYS_AGENT_CODE.CREATOR
  is '创建者';
comment on column TBL_SYS_AGENT_CODE.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_AGENT_CODE.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_AGENT_CODE.LAST_MODIFY_TIME
  is '最后修改时间';
comment on column TBL_SYS_AGENT_CODE.ENABLED
  is '状态，0为正常1为停用';
alter table TBL_SYS_AGENT_CODE
  add constraint TBL_SYS_AGENT_CODE_PK primary key (AGENT_ID);
alter table TBL_SYS_AGENT_CODE
  add constraint HDFFSDSDSF
  check ("AGENT_ID" IS NOT NULL);



create table TBL_SYS_AGENT_SKILL
(
  SKILL_ID VARCHAR2(40) not null,
  AGENT_ID VARCHAR2(40) not null
);
comment on table TBL_SYS_AGENT_SKILL
  is '系统-工号技能关系表';
comment on column TBL_SYS_AGENT_SKILL.SKILL_ID
  is '技能主键ID';
comment on column TBL_SYS_AGENT_SKILL.AGENT_ID
  is '工号主键ID';
alter table TBL_SYS_AGENT_SKILL
  add constraint PK_TBL_SYS_AGENT_SKILLS primary key (SKILL_ID, AGENT_ID);




create table TBL_SYS_AUTH_FUNCTION
(
  FUNCTION_ID        VARCHAR2(40),
  DOMAIN_ID          VARCHAR2(40),
  ORG_ID             VARCHAR2(40),
  FUNCTION_NAME      VARCHAR2(60),
  PARENT_FUNCTION_ID VARCHAR2(40),
  SUB_SYSTEM_ID      VARCHAR2(40),
  URL                VARCHAR2(200),
  VIEW_LEVEL_NO      VARCHAR2(30),
  FUNCTION_TYPE      VARCHAR2(8),
  SHORTCUT_KEY       VARCHAR2(20),
  SORT_NO            NUMBER,
  IS_SINGLETON       NUMBER(1),
  IS_NEW_WIN         NUMBER(1),
  IS_WORKBENCH       NUMBER(1),
  IS_CLOSED          NUMBER(1),
  ENABLED            NUMBER(1),
  REMARK             VARCHAR2(256),
  CREATOR            VARCHAR2(80),
  CREATE_TIME        VARCHAR2(19),
  LAST_MODIFIER      VARCHAR2(80),
  LAST_MODIFY_TIME   VARCHAR2(19),
  PERMISSION_KEY     VARCHAR2(40),
  IS_SHOW            NUMBER(1)
);
comment on table TBL_SYS_AUTH_FUNCTION
  is '系统-系统功能表';
comment on column TBL_SYS_AUTH_FUNCTION.FUNCTION_ID
  is '功能ID';
comment on column TBL_SYS_AUTH_FUNCTION.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_AUTH_FUNCTION.ORG_ID
  is 'IS_ORG=1的机构ID（字段默认为空表示为通用。有值表示专属）';
comment on column TBL_SYS_AUTH_FUNCTION.FUNCTION_NAME
  is '功能名称';
comment on column TBL_SYS_AUTH_FUNCTION.PARENT_FUNCTION_ID
  is '父功能ID';
comment on column TBL_SYS_AUTH_FUNCTION.SUB_SYSTEM_ID
  is '所属子系统(为空表示公共，有值表示专属)';
comment on column TBL_SYS_AUTH_FUNCTION.URL
  is '功能调用链接';
comment on column TBL_SYS_AUTH_FUNCTION.VIEW_LEVEL_NO
  is '显示层次编号';
comment on column TBL_SYS_AUTH_FUNCTION.FUNCTION_TYPE
  is 'generalCode表中的code_type=''FUNC_TYPE'' 0菜单1按钮2其它';
comment on column TBL_SYS_AUTH_FUNCTION.SHORTCUT_KEY
  is '快捷键';
comment on column TBL_SYS_AUTH_FUNCTION.SORT_NO
  is '排序号';
comment on column TBL_SYS_AUTH_FUNCTION.IS_SINGLETON
  is '是否单例';
comment on column TBL_SYS_AUTH_FUNCTION.IS_NEW_WIN
  is '是否新开页面';
comment on column TBL_SYS_AUTH_FUNCTION.IS_WORKBENCH
  is '判断是是否自动打开，如果为1：则说明登录后字段打开此菜单页，0：则说明登录后不会字段打开此菜单页，默认为0';
comment on column TBL_SYS_AUTH_FUNCTION.IS_CLOSED
  is '判断能否关闭，如果为1：则说明页面能关闭，0：则说明页面不能关闭，默认为1';
comment on column TBL_SYS_AUTH_FUNCTION.ENABLED
  is '状态，0为停用1为正常';
comment on column TBL_SYS_AUTH_FUNCTION.REMARK
  is '备注';
comment on column TBL_SYS_AUTH_FUNCTION.CREATOR
  is '创建者';
comment on column TBL_SYS_AUTH_FUNCTION.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_AUTH_FUNCTION.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_AUTH_FUNCTION.LAST_MODIFY_TIME
  is '最后修改时间';
comment on column TBL_SYS_AUTH_FUNCTION.PERMISSION_KEY
  is '功能权限表达式';
alter table TBL_SYS_AUTH_FUNCTION
  add constraint HKKHFHIIEE
  check ("VIEW_LEVEL_NO" IS NOT NULL);
alter table TBL_SYS_AUTH_FUNCTION
  add constraint RERETGDDGDG
  check ("FUNCTION_ID" IS NOT NULL);




create table TBL_SYS_AUTH_FUNC_PERMISSION
(
  PERMISSION_ID    VARCHAR2(40),
  DOMAIN_ID        VARCHAR2(40),
  ORG_ID           VARCHAR2(40),
  OWN_TYPE         VARCHAR2(40),
  OWN_ID           VARCHAR2(40),
  FUNCTION_TYPE    VARCHAR2(8),
  FUNCTION_ID      VARCHAR2(40),
  CTRL_TYPE        NUMBER(1),
  ENABLED          NUMBER(1),
  REMARK           VARCHAR2(256),
  CREATOR          VARCHAR2(80),
  CREATE_TIME      VARCHAR2(19),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19),
  EXPREESION       VARCHAR2(40),
  SUB_SYSTEM_ID    VARCHAR2(40)
);
comment on table TBL_SYS_AUTH_FUNC_PERMISSION
  is '系统-功能授权许可表';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.PERMISSION_ID
  is '角色ID';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.ORG_ID
  is 'IS_ORG=1的机构ID';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.OWN_TYPE
  is '授权对象类型(用户/角色/工作组)';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.OWN_ID
  is '授权对象ID';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.FUNCTION_TYPE
  is '授权类型（菜单/个人导航/个人收藏/页面/按钮）对应系统编码表FUNCTION_TYPE';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.FUNCTION_ID
  is '功能ID';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.CTRL_TYPE
  is '控制类别（0为禁止，1为允许，2为分配，3为授权）';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.ENABLED
  is '状态，0为停用1为正常';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.REMARK
  is '备注';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.CREATOR
  is '创建者';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.LAST_MODIFY_TIME
  is '最后修改时间';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.EXPREESION
  is '表达式';
comment on column TBL_SYS_AUTH_FUNC_PERMISSION.SUB_SYSTEM_ID
  is '子系统ID';
alter table TBL_SYS_AUTH_FUNC_PERMISSION
  add constraint CJBVDHUSS
  check ("PERMISSION_ID" IS NOT NULL);



create table TBL_SYS_CODE_TYPE
(
  CODE_TYPE        VARCHAR2(40),
  ORG_ID           VARCHAR2(40),
  DOMAIN_ID        VARCHAR2(40),
  CODETYPE_TYPE    VARCHAR2(60),
  MODULE_NAME      VARCHAR2(40),
  CODE_TYPE_DESC   VARCHAR2(60),
  IS_SYSTEM        NUMBER(1),
  SUB_SYSTEM_ID    VARCHAR2(40),
  ENABLED          NUMBER(1),
  REMARK           VARCHAR2(256),
  CREATOR          VARCHAR2(80),
  CREATE_TIME      VARCHAR2(19),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19),
  SORT_NO          NUMBER,
  CODE_TYPE_ID     VARCHAR2(255 CHAR),
  IS_BY_DEVELOPER  NUMBER(1)
);
comment on table TBL_SYS_CODE_TYPE
  is '存储常用代码的代码类型、系统分类树表的根类型、系统参数类型,系统表无需用户维护，在系统设计时完成此表的初始化数据';
comment on column TBL_SYS_CODE_TYPE.CODE_TYPE
  is '编码类别 常用代码中的：CLIENT_TYPE,SEX_TYPE etc, 分类树中的CLICK_TYPE等等。';
comment on column TBL_SYS_CODE_TYPE.ORG_ID
  is 'IS_ORG=1的机构ID';
comment on column TBL_SYS_CODE_TYPE.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_CODE_TYPE.CODETYPE_TYPE
  is '代码类型分类(直接用中文). 1:常用代码类型，2：分类树代码类型，3：系统参数类型，4：服务请求根类型';
comment on column TBL_SYS_CODE_TYPE.MODULE_NAME
  is '模块名称';
comment on column TBL_SYS_CODE_TYPE.CODE_TYPE_DESC
  is '代码类别描述';
comment on column TBL_SYS_CODE_TYPE.IS_SYSTEM
  is '是否是系统代码,系统代码对与用户不可修改，删除。0--否,1--是';
comment on column TBL_SYS_CODE_TYPE.SUB_SYSTEM_ID
  is '子系统标识';
comment on column TBL_SYS_CODE_TYPE.ENABLED
  is '状态，0为停用1为正常';
comment on column TBL_SYS_CODE_TYPE.REMARK
  is '备注';
comment on column TBL_SYS_CODE_TYPE.CREATOR
  is '创建者';
comment on column TBL_SYS_CODE_TYPE.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_CODE_TYPE.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_CODE_TYPE.LAST_MODIFY_TIME
  is '最后修改时间';
comment on column TBL_SYS_CODE_TYPE.SORT_NO
  is '排序';
alter table TBL_SYS_CODE_TYPE
  add constraint TBL_SYS_CODE_TYPE_PK primary key (CODE_TYPE);
alter table TBL_SYS_CODE_TYPE
  add constraint TBL_SYS_CODE_TYPE7 unique (CODE_TYPE_ID);
alter table TBL_SYS_CODE_TYPE
  add constraint TBL_SYS_CODE_TYPE1
  check ("CODE_TYPE" IS NOT NULL);
alter table TBL_SYS_CODE_TYPE
  add constraint TBL_SYS_CODE_TYPE2
  check ("CODETYPE_TYPE" IS NOT NULL);
alter table TBL_SYS_CODE_TYPE
  add constraint TBL_SYS_CODE_TYPE3
  check ("MODULE_NAME" IS NOT NULL);
alter table TBL_SYS_CODE_TYPE
  add constraint TBL_SYS_CODE_TYPE4
  check ("CODE_TYPE_DESC" IS NOT NULL);
alter table TBL_SYS_CODE_TYPE
  add constraint TBL_SYS_CODE_TYPE5
  check ("IS_SYSTEM" IS NOT NULL);
alter table TBL_SYS_CODE_TYPE
  add constraint TBL_SYS_CODE_TYPE6
  check ("CODE_TYPE_ID" IS NOT NULL);




create table TBL_SYS_SEQUENCE
(
  SEQUENCE_ID   VARCHAR2(40) not null,
  SEQUENCE_NAME VARCHAR2(60),
  SEQUENCE_RULE VARCHAR2(512),
  MEMO          VARCHAR2(200),
  SUB_SYSTEM_ID VARCHAR2(40)
);
comment on table TBL_SYS_SEQUENCE
  is 'TBL_SYS_SEQUENCE';
comment on column TBL_SYS_SEQUENCE.SEQUENCE_ID
  is '序列ID';
comment on column TBL_SYS_SEQUENCE.SEQUENCE_NAME
  is '序列名称';
comment on column TBL_SYS_SEQUENCE.SEQUENCE_RULE
  is '序列规则';
comment on column TBL_SYS_SEQUENCE.MEMO
  is '备注';
comment on column TBL_SYS_SEQUENCE.SUB_SYSTEM_ID
  is '子系统标识';
alter table TBL_SYS_SEQUENCE
  add constraint PK_TBL_SYS_SEQUENCE primary key (SEQUENCE_ID);



create table TBL_SYS_CURRENT_MAX
(
  SEQUENCE_ID   VARCHAR2(40) not null,
  MAX_ID        VARCHAR2(100) not null,
  CURRENT_VALUE NUMBER,
  MAX_TYPE      CHAR(1),
  MEMO          VARCHAR2(200)
);
comment on table TBL_SYS_CURRENT_MAX
  is 'TBL_SYS_CURRENT_MAX';
comment on column TBL_SYS_CURRENT_MAX.SEQUENCE_ID
  is '序列ID';
comment on column TBL_SYS_CURRENT_MAX.MAX_ID
  is '最大值模式';
comment on column TBL_SYS_CURRENT_MAX.CURRENT_VALUE
  is '当前值';
comment on column TBL_SYS_CURRENT_MAX.MAX_TYPE
  is '预留字段';
comment on column TBL_SYS_CURRENT_MAX.MEMO
  is '备注';
alter table TBL_SYS_CURRENT_MAX
  add constraint PK_TBL_SYS_CURRENT_MAX primary key (SEQUENCE_ID, MAX_ID);
alter table TBL_SYS_CURRENT_MAX
  add constraint FK4CF6F02BED6C63C6 foreign key (SEQUENCE_ID)
  references TBL_SYS_SEQUENCE (SEQUENCE_ID);




create table TBL_SYS_DATA_PERMISSION
(
  PERMISSION_ID    VARCHAR2(40),
  DOMAIN_ID        VARCHAR2(40),
  ORG_ID           VARCHAR2(40),
  OWN_TYPE         VARCHAR2(8),
  OWN_ID           VARCHAR2(40),
  CTRL_TYPE        VARCHAR2(40),
  CTRL_VALUE       VARCHAR2(1000),
  TARGET_ID        VARCHAR2(40),
  ENABLED          NUMBER(1),
  REMARK           VARCHAR2(256),
  CREATOR          VARCHAR2(80),
  CREATE_TIME      VARCHAR2(19),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19)
);
comment on table TBL_SYS_DATA_PERMISSION
  is '授权许可表';
comment on column TBL_SYS_DATA_PERMISSION.PERMISSION_ID
  is '数据授权ID';
comment on column TBL_SYS_DATA_PERMISSION.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_DATA_PERMISSION.ORG_ID
  is 'IS_ORG=1的机构ID';
comment on column TBL_SYS_DATA_PERMISSION.OWN_TYPE
  is '授权对象类型(用户/角色/工作组)';
comment on column TBL_SYS_DATA_PERMISSION.OWN_ID
  is '授权对象(用户/角色/组织机构/工作组)表的对应ID';
comment on column TBL_SYS_DATA_PERMISSION.CTRL_TYPE
  is '0本机构1指定机构2本部门3指定部门4本人';
comment on column TBL_SYS_DATA_PERMISSION.CTRL_VALUE
  is '数据受控范围(具体的若干部门ID或具体的若干组织ID)';
comment on column TBL_SYS_DATA_PERMISSION.TARGET_ID
  is '受控目标ID（仅指功能表中标识为允许数据受控的菜单页面ID）';
comment on column TBL_SYS_DATA_PERMISSION.ENABLED
  is '状态，0为停用1为正常';
comment on column TBL_SYS_DATA_PERMISSION.REMARK
  is '备注';
comment on column TBL_SYS_DATA_PERMISSION.CREATOR
  is '创建者';
comment on column TBL_SYS_DATA_PERMISSION.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_DATA_PERMISSION.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_DATA_PERMISSION.LAST_MODIFY_TIME
  is '最后修改时间';
alter table TBL_SYS_DATA_PERMISSION
  add constraint TBL_SYS_DATA_PERMISSION_PK primary key (PERMISSION_ID);
alter table TBL_SYS_DATA_PERMISSION
  add constraint TBL_SYS_DATA_PERMISSION1
  check ("PERMISSION_ID" IS NOT NULL);




create table TBL_SYS_DOMAIN
(
  DOMAIN_ID        VARCHAR2(40),
  DOMAIN_NAME      VARCHAR2(40),
  DOMAIN_LOGO      VARCHAR2(400),
  ENABLED          NUMBER(1),
  REMARK           VARCHAR2(256),
  CREATOR          VARCHAR2(80),
  CREATE_TIME      VARCHAR2(19),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19)
);
comment on table TBL_SYS_DOMAIN
  is '租户表';
comment on column TBL_SYS_DOMAIN.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_DOMAIN.DOMAIN_NAME
  is '租户名称';
comment on column TBL_SYS_DOMAIN.DOMAIN_LOGO
  is '租户LOGO';
comment on column TBL_SYS_DOMAIN.ENABLED
  is '租户状态，0为正常1为停用';
comment on column TBL_SYS_DOMAIN.REMARK
  is '备注';
comment on column TBL_SYS_DOMAIN.CREATOR
  is '创建者';
comment on column TBL_SYS_DOMAIN.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_DOMAIN.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_DOMAIN.LAST_MODIFY_TIME
  is '最后修改时间';
alter table TBL_SYS_DOMAIN
  add constraint TBL_SYS_DOMAIN_PK primary key (DOMAIN_ID);
alter table TBL_SYS_DOMAIN
  add constraint TBL_SYS_DOMAIN1
  check ("DOMAIN_ID" IS NOT NULL);
alter table TBL_SYS_DOMAIN
  add constraint TBL_SYS_DOMAIN2
  check ("DOMAIN_NAME" IS NOT NULL);
alter table TBL_SYS_DOMAIN
  add constraint TBL_SYS_DOMAIN3
  check ("ENABLED" IS NOT NULL);



create table TBL_SYS_FUNCTION
(
  FUNCTION_ID        VARCHAR2(40),
  DOMAIN_ID          VARCHAR2(40),
  ORG_ID             VARCHAR2(40),
  FUNCTION_NAME      VARCHAR2(60),
  PARENT_FUNCTION_ID VARCHAR2(40),
  SUB_SYSTEM_ID      VARCHAR2(40),
  URL                VARCHAR2(200),
  VIEW_LEVEL_NO      VARCHAR2(30),
  FUNCTION_TYPE      VARCHAR2(40),
  SHORTCUT_KEY       VARCHAR2(20),
  SORT_NO            NUMBER,
  IS_SINGLETON       NUMBER(1),
  IS_NEW_WIN         NUMBER(1),
  IS_WORKBENCH       NUMBER(1),
  IS_CLOSED          NUMBER(1),
  ENABLED            NUMBER(1),
  REMARK             VARCHAR2(256),
  CREATOR            VARCHAR2(80),
  CREATE_TIME        VARCHAR2(19),
  LAST_MODIFIER      VARCHAR2(80),
  LAST_MODIFY_TIME   VARCHAR2(19),
  PERMISSION_KEY     VARCHAR2(40),
  IS_SHOW            NUMBER(1)
);
comment on table TBL_SYS_FUNCTION
  is '系统-系统功能表';
comment on column TBL_SYS_FUNCTION.FUNCTION_ID
  is '功能ID';
comment on column TBL_SYS_FUNCTION.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_FUNCTION.ORG_ID
  is 'IS_ORG=1的机构ID（字段默认为空表示为通用。有值表示专属）';
comment on column TBL_SYS_FUNCTION.FUNCTION_NAME
  is '功能名称';
comment on column TBL_SYS_FUNCTION.PARENT_FUNCTION_ID
  is '父功能ID';
comment on column TBL_SYS_FUNCTION.SUB_SYSTEM_ID
  is '所属子系统(为空表示公共，有值表示专属)';
comment on column TBL_SYS_FUNCTION.URL
  is '功能调用链接';
comment on column TBL_SYS_FUNCTION.VIEW_LEVEL_NO
  is '显示层次编号';
comment on column TBL_SYS_FUNCTION.FUNCTION_TYPE
  is 'generalCode表中的code_type=''FUNC_TYPE'' 0菜单1按钮2其它';
comment on column TBL_SYS_FUNCTION.SHORTCUT_KEY
  is '快捷键';
comment on column TBL_SYS_FUNCTION.SORT_NO
  is '排序号';
comment on column TBL_SYS_FUNCTION.IS_SINGLETON
  is '是否单例';
comment on column TBL_SYS_FUNCTION.IS_NEW_WIN
  is '是否新开页面';
comment on column TBL_SYS_FUNCTION.IS_WORKBENCH
  is '判断是是否自动打开，如果为1：则说明登录后字段打开此菜单页，0：则说明登录后不会字段打开此菜单页，默认为0';
comment on column TBL_SYS_FUNCTION.IS_CLOSED
  is '判断能否关闭，如果为1：则说明页面能关闭，0：则说明页面不能关闭，默认为1';
comment on column TBL_SYS_FUNCTION.ENABLED
  is '状态，0为停用1为正常';
comment on column TBL_SYS_FUNCTION.REMARK
  is '备注';
comment on column TBL_SYS_FUNCTION.CREATOR
  is '创建者';
comment on column TBL_SYS_FUNCTION.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_FUNCTION.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_FUNCTION.LAST_MODIFY_TIME
  is '最后修改时间';
comment on column TBL_SYS_FUNCTION.PERMISSION_KEY
  is '功能权限表达式';
alter table TBL_SYS_FUNCTION
  add constraint TBL_SYS_FUNCTION_PK primary key (FUNCTION_ID);
alter table TBL_SYS_FUNCTION
  add constraint TBL_SYS_FUNCTION1
  check ("FUNCTION_ID" IS NOT NULL);
alter table TBL_SYS_FUNCTION
  add constraint TBL_SYS_FUNCTION2
  check ("FUNCTION_NAME" IS NOT NULL);
alter table TBL_SYS_FUNCTION
  add constraint TBL_SYS_FUNCTION3
  check ("VIEW_LEVEL_NO" IS NOT NULL);
alter table TBL_SYS_FUNCTION
  add constraint TBL_SYS_FUNCTION4
  check ("IS_SINGLETON" IS NOT NULL);




create table TBL_SYS_FUNC_PERMISSION
(
  PERMISSION_ID    VARCHAR2(40),
  DOMAIN_ID        VARCHAR2(40),
  ORG_ID           VARCHAR2(40),
  OWN_TYPE         VARCHAR2(40),
  OWN_ID           VARCHAR2(40),
  FUNCTION_TYPE    VARCHAR2(40),
  FUNCTION_ID      VARCHAR2(40),
  CTRL_TYPE        NUMBER(1),
  ENABLED          NUMBER(1),
  REMARK           VARCHAR2(256),
  CREATOR          VARCHAR2(80),
  CREATE_TIME      VARCHAR2(19),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19),
  EXPREESION       VARCHAR2(40)
);
comment on table TBL_SYS_FUNC_PERMISSION
  is '系统-功能授权许可表';
comment on column TBL_SYS_FUNC_PERMISSION.PERMISSION_ID
  is '角色ID';
comment on column TBL_SYS_FUNC_PERMISSION.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_FUNC_PERMISSION.ORG_ID
  is 'IS_ORG=1的机构ID';
comment on column TBL_SYS_FUNC_PERMISSION.OWN_TYPE
  is '授权对象类型(用户/角色/工作组)';
comment on column TBL_SYS_FUNC_PERMISSION.OWN_ID
  is '授权对象ID';
comment on column TBL_SYS_FUNC_PERMISSION.FUNCTION_TYPE
  is '授权类型（菜单/个人导航/个人收藏/页面/按钮）对应系统编码表FUNCTION_TYPE';
comment on column TBL_SYS_FUNC_PERMISSION.FUNCTION_ID
  is '功能ID';
comment on column TBL_SYS_FUNC_PERMISSION.CTRL_TYPE
  is '控制类别（0为禁止，1为允许，2为分配，3为授权）';
comment on column TBL_SYS_FUNC_PERMISSION.ENABLED
  is '状态，0为停用1为正常';
comment on column TBL_SYS_FUNC_PERMISSION.REMARK
  is '备注';
comment on column TBL_SYS_FUNC_PERMISSION.CREATOR
  is '创建者';
comment on column TBL_SYS_FUNC_PERMISSION.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_FUNC_PERMISSION.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_FUNC_PERMISSION.LAST_MODIFY_TIME
  is '最后修改时间';
alter table TBL_SYS_FUNC_PERMISSION
  add constraint TBL_SYS_FUNC_PERMISSION_PK primary key (PERMISSION_ID);
alter table TBL_SYS_FUNC_PERMISSION
  add constraint TBL_SYS_FUNC_PERMISSION1
  check ("PERMISSION_ID" IS NOT NULL);




create table TBL_SYS_GENERAL_CODE
(
  CODE_ID          VARCHAR2(40),
  DOMAIN_ID        VARCHAR2(40),
  ORG_ID           VARCHAR2(40),
  CODE_TYPE        VARCHAR2(40),
  CODE_VALUE       VARCHAR2(40),
  CODE_NAME        VARCHAR2(60),
  SORT_NO          NUMBER,
  ENABLED          NUMBER(1),
  IS_SYSTEM        NUMBER(1),
  SUB_SYSTEM_ID    VARCHAR2(40),
  REMARK           VARCHAR2(256),
  CREATOR          VARCHAR2(80),
  CREATE_TIME      VARCHAR2(19),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19),
  AREA_CODE        VARCHAR2(8 CHAR),
  DISENABLED_TIME  VARCHAR2(19 CHAR),
  ENABLED_TIME     VARCHAR2(19 CHAR),
  IS_BY_DEVELOPER  NUMBER(1)
);
comment on table TBL_SYS_GENERAL_CODE
  is '通用编码表';
comment on column TBL_SYS_GENERAL_CODE.CODE_ID
  is '主键ID';
comment on column TBL_SYS_GENERAL_CODE.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_GENERAL_CODE.ORG_ID
  is 'IS_ORG=1的机构ID';
comment on column TBL_SYS_GENERAL_CODE.CODE_TYPE
  is '编码类别';
comment on column TBL_SYS_GENERAL_CODE.CODE_VALUE
  is '编码值';
comment on column TBL_SYS_GENERAL_CODE.CODE_NAME
  is '编码名称';
comment on column TBL_SYS_GENERAL_CODE.SORT_NO
  is '排序号';
comment on column TBL_SYS_GENERAL_CODE.ENABLED
  is '是否启用';
comment on column TBL_SYS_GENERAL_CODE.IS_SYSTEM
  is '是否系统';
comment on column TBL_SYS_GENERAL_CODE.SUB_SYSTEM_ID
  is '子系统标识';
comment on column TBL_SYS_GENERAL_CODE.REMARK
  is '备注';
comment on column TBL_SYS_GENERAL_CODE.CREATOR
  is '创建者';
comment on column TBL_SYS_GENERAL_CODE.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_GENERAL_CODE.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_GENERAL_CODE.LAST_MODIFY_TIME
  is '最后修改时间';
comment on column TBL_SYS_GENERAL_CODE.AREA_CODE
  is '区域编码';
comment on column TBL_SYS_GENERAL_CODE.DISENABLED_TIME
  is '停用时间';
comment on column TBL_SYS_GENERAL_CODE.ENABLED_TIME
  is '启用时间';
comment on column TBL_SYS_GENERAL_CODE.IS_BY_DEVELOPER
  is '是否开发者';
alter table TBL_SYS_GENERAL_CODE
  add constraint TBL_SYS_GENERAL_CODE_PK primary key (CODE_ID);
alter table TBL_SYS_GENERAL_CODE
  add constraint TBL_SYS_GENERAL_CODE1
  check ("CODE_ID" IS NOT NULL);


create table TBL_SYS_GROUP
(
  GROUP_ID         VARCHAR2(40),
  GROUP_NAME       VARCHAR2(60),
  ORG_ID           VARCHAR2(40),
  DOMAIN_ID        VARCHAR2(40),
  IS_SYSTEM        NUMBER(1),
  ENABLED          NUMBER(1),
  REMARK           VARCHAR2(256),
  CREATOR          VARCHAR2(80),
  CREATE_TIME      VARCHAR2(19),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19)
);
comment on table TBL_SYS_GROUP
  is '工作组';
comment on column TBL_SYS_GROUP.GROUP_ID
  is '工作组ID';
comment on column TBL_SYS_GROUP.GROUP_NAME
  is '工作组名称';
comment on column TBL_SYS_GROUP.ORG_ID
  is 'IS_ORG=1的机构ID';
comment on column TBL_SYS_GROUP.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_GROUP.IS_SYSTEM
  is '是否为系统工作组1为是0为否';
comment on column TBL_SYS_GROUP.ENABLED
  is '状态，0为停用1为正常';
comment on column TBL_SYS_GROUP.REMARK
  is '备注';
comment on column TBL_SYS_GROUP.CREATOR
  is '创建者';
comment on column TBL_SYS_GROUP.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_GROUP.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_GROUP.LAST_MODIFY_TIME
  is '最后修改时间';
alter table TBL_SYS_GROUP
  add constraint TBL_SYS_GROUP_PK primary key (GROUP_ID);
alter table TBL_SYS_GROUP
  add constraint TBL_SYS_GROUP1
  check ("GROUP_ID" IS NOT NULL);
alter table TBL_SYS_GROUP
  add constraint TBL_SYS_GROUP2
  check ("GROUP_NAME" IS NOT NULL);
alter table TBL_SYS_GROUP
  add constraint TBL_SYS_GROUP3
  check ("IS_SYSTEM" IS NOT NULL);




create table TBL_SYS_LOCKUSER_INFO
(
  LOG_ID           VARCHAR2(40),
  USER_CODE        VARCHAR2(40),
  ORG_ID           VARCHAR2(40),
  FAIL_TIMES       NUMBER(4),
  FAIL_REASON      VARCHAR2(256),
  LAST_FAIL_TIME   VARCHAR2(20),
  IS_LOCKED        NUMBER(1),
  LOCK_EXPIRE_TIME VARCHAR2(20)
);
comment on column TBL_SYS_LOCKUSER_INFO.LOG_ID
  is '登录ID';
comment on column TBL_SYS_LOCKUSER_INFO.USER_CODE
  is '用户账号';
comment on column TBL_SYS_LOCKUSER_INFO.ORG_ID
  is 'IS_ORG=1的机构ID';
comment on column TBL_SYS_LOCKUSER_INFO.FAIL_TIMES
  is '失败次数';
comment on column TBL_SYS_LOCKUSER_INFO.FAIL_REASON
  is '失败原因';
comment on column TBL_SYS_LOCKUSER_INFO.LAST_FAIL_TIME
  is '上次失败时间';
comment on column TBL_SYS_LOCKUSER_INFO.IS_LOCKED
  is '是否锁定';
comment on column TBL_SYS_LOCKUSER_INFO.LOCK_EXPIRE_TIME
  is '锁定过期时间';
alter table TBL_SYS_LOCKUSER_INFO
  add constraint TBL_SYS_LOCKUSER_INFO_PK primary key (LOG_ID);
alter table TBL_SYS_LOCKUSER_INFO
  add constraint TBL_SYS_LOCKUSER_INFO1
  check ("LOG_ID" IS NOT NULL);
alter table TBL_SYS_LOCKUSER_INFO
  add constraint TBL_SYS_LOCKUSER_INFO2
  check ("ORG_ID" IS NOT NULL);
create index IDX_LOCK_USER_IS_LOCKED on TBL_SYS_LOCKUSER_INFO (IS_LOCKED);
create index IDX_LOCK_USER_PK on TBL_SYS_LOCKUSER_INFO (USER_CODE||'@'||ORG_ID);




create table TBL_SYS_LOGON_LOG
(
  LOG_ID           VARCHAR2(40),
  ORG_ID           VARCHAR2(40),
  DOMAIN_ID        VARCHAR2(40),
  SUB_SYSTEM_ID    VARCHAR2(40),
  COMPUTER_NAME    VARCHAR2(30),
  COMPUTER_IP      VARCHAR2(20),
  LOGON_TIME       VARCHAR2(20),
  LOGOUT_TIME      VARCHAR2(20),
  REFRESH_TIME     VARCHAR2(20),
  CREATE_TIME      VARCHAR2(255 CHAR),
  CREATOR          VARCHAR2(255 CHAR),
  LAST_MODIFIER    VARCHAR2(255 CHAR),
  LAST_MODIFY_TIME VARCHAR2(255 CHAR),
  REMARK           VARCHAR2(255 CHAR),
  ENABLED          NUMBER(1),
  USER_CODE        VARCHAR2(80)
);
comment on table TBL_SYS_LOGON_LOG
  is '登录日志表';
comment on column TBL_SYS_LOGON_LOG.LOG_ID
  is '登陆日志ID';
comment on column TBL_SYS_LOGON_LOG.ORG_ID
  is 'IS_ORG=1的机构ID';
comment on column TBL_SYS_LOGON_LOG.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_LOGON_LOG.SUB_SYSTEM_ID
  is '模块名称';
comment on column TBL_SYS_LOGON_LOG.COMPUTER_NAME
  is '计算机名称';
comment on column TBL_SYS_LOGON_LOG.COMPUTER_IP
  is '计算机IP';
comment on column TBL_SYS_LOGON_LOG.LOGON_TIME
  is '登录时间';
comment on column TBL_SYS_LOGON_LOG.LOGOUT_TIME
  is '退出时间';
comment on column TBL_SYS_LOGON_LOG.REFRESH_TIME
  is '刷新时间';
comment on column TBL_SYS_LOGON_LOG.ENABLED
  is '状态，0为正常1为停用';
alter table TBL_SYS_LOGON_LOG
  add constraint TBL_SYS_LOGON_LOG_PK primary key (LOG_ID);
alter table TBL_SYS_LOGON_LOG
  add constraint TBL_SYS_LOGON_LOG1
  check ("LOG_ID" IS NOT NULL);
alter table TBL_SYS_LOGON_LOG
  add constraint TBL_SYS_LOGON_LOG2
  check ("ORG_ID" IS NOT NULL);




create table TBL_SYS_MEDIA_FILE
(
  MEDIA_ID         VARCHAR2(255) not null,
  CREATE_TIME      VARCHAR2(255),
  CREATOR          VARCHAR2(255),
  DOMAIN_ID        VARCHAR2(255),
  LAST_MODIFIER    VARCHAR2(255),
  LAST_MODIFY_TIME VARCHAR2(255),
  REMARK           VARCHAR2(255),
  CONTENT_TYPE     VARCHAR2(255),
  FILE_DESC        VARCHAR2(255),
  FILE_EXT         VARCHAR2(255),
  FILE_HTTP_URL    VARCHAR2(255),
  FILE_NAME        VARCHAR2(255),
  FILE_TITLE       VARCHAR2(255),
  FILE_TYPE        VARCHAR2(255),
  FILE_URI         VARCHAR2(255),
  MEDIA_TYPE       VARCHAR2(255),
  OLD_FILE_NAME    VARCHAR2(255),
  FILE_SIZE        NUMBER(19),
  ENABLED          VARCHAR2(255)
);
comment on column TBL_SYS_MEDIA_FILE.CREATE_TIME
  is '时间';
comment on column TBL_SYS_MEDIA_FILE.FILE_EXT
  is '扩展名';
comment on column TBL_SYS_MEDIA_FILE.FILE_NAME
  is '文件名';
comment on column TBL_SYS_MEDIA_FILE.OLD_FILE_NAME
  is '原文件名';
alter table TBL_SYS_MEDIA_FILE
  add primary key (MEDIA_ID);



create table TBL_SYS_NOTICE_INFO
(
  NOTICE_ID        VARCHAR2(40) not null,
  NOTICE_TYPE      VARCHAR2(8) not null,
  EMERGENCY        VARCHAR2(8) not null,
  NOTICE_TITLE     VARCHAR2(60) not null,
  NOTICE_CONTENT   CLOB,
  PUBLISHER        VARCHAR2(32) not null,
  SENDEE           CLOB not null,
  SENDEE_NAMES     CLOB not null,
  CREATE_TIME      VARCHAR2(19) not null,
  IS_IMMEDIATELY   VARCHAR2(1) not null,
  PUBLISH_TIME     VARCHAR2(19) not null,
  ATTENTION_TYPE   VARCHAR2(60) not null,
  EXPIRY_TIME      VARCHAR2(19) not null,
  IS_SAVE_RELA     VARCHAR2(1),
  MODIFY_USER      VARCHAR2(32),
  STICKY_TIME      VARCHAR2(19),
  IS_DELETE        VARCHAR2(1),
  AREA             VARCHAR2(8),
  DOMAIN_ID        VARCHAR2(20),
  REMARK           VARCHAR2(256),
  CREATOR          VARCHAR2(80),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19),
  IS_STICKY        VARCHAR2(1),
  ENABLED          NUMBER(1),
  ORG_ID           VARCHAR2(40)
);
comment on table TBL_SYS_NOTICE_INFO
  is '(业务通知模块)通知记录信息';
comment on column TBL_SYS_NOTICE_INFO.NOTICE_ID
  is '通知ID';
comment on column TBL_SYS_NOTICE_INFO.NOTICE_TYPE
  is '公告类型';
comment on column TBL_SYS_NOTICE_INFO.EMERGENCY
  is '紧急程度:0:一般,1:紧急,2:非常紧急';
comment on column TBL_SYS_NOTICE_INFO.NOTICE_TITLE
  is '通知标题';
comment on column TBL_SYS_NOTICE_INFO.NOTICE_CONTENT
  is '通知内容';
comment on column TBL_SYS_NOTICE_INFO.PUBLISHER
  is '发布人';
comment on column TBL_SYS_NOTICE_INFO.SENDEE
  is '收件人账号';
comment on column TBL_SYS_NOTICE_INFO.SENDEE_NAMES
  is '收件人名称';
comment on column TBL_SYS_NOTICE_INFO.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_NOTICE_INFO.IS_IMMEDIATELY
  is '发布方式:0:立即发送;1:定时发送';
comment on column TBL_SYS_NOTICE_INFO.PUBLISH_TIME
  is '发布时间';
comment on column TBL_SYS_NOTICE_INFO.ATTENTION_TYPE
  is '提醒方式,0:滚动,1:弹出';
comment on column TBL_SYS_NOTICE_INFO.EXPIRY_TIME
  is '失效时间';
comment on column TBL_SYS_NOTICE_INFO.IS_SAVE_RELA
  is '是否已保存关联';
comment on column TBL_SYS_NOTICE_INFO.MODIFY_USER
  is '修改人账号';
comment on column TBL_SYS_NOTICE_INFO.STICKY_TIME
  is '置顶时间';
comment on column TBL_SYS_NOTICE_INFO.IS_DELETE
  is '是否已删除 0:已删除,1:未发布,2:已发布,3:已过期';
comment on column TBL_SYS_NOTICE_INFO.AREA
  is '区域';
comment on column TBL_SYS_NOTICE_INFO.DOMAIN_ID
  is '租户ID';
comment on column TBL_SYS_NOTICE_INFO.REMARK
  is '备注';
comment on column TBL_SYS_NOTICE_INFO.CREATOR
  is '创建者';
comment on column TBL_SYS_NOTICE_INFO.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_NOTICE_INFO.LAST_MODIFY_TIME
  is '最后修改时间';
comment on column TBL_SYS_NOTICE_INFO.IS_STICKY
  is '是否置顶,0:是,1:否';
comment on column TBL_SYS_NOTICE_INFO.ENABLED
  is '状态，0为正常1为停用';
comment on column TBL_SYS_NOTICE_INFO.ORG_ID
  is '机构ID';
alter table TBL_SYS_NOTICE_INFO
  add primary key (NOTICE_ID);




create table TBL_SYS_NOTICE_USER
(
  ID               VARCHAR2(40),
  NOTICE_ID        VARCHAR2(40),
  PUBLISHER        VARCHAR2(60),
  NOTICE_TYPE      VARCHAR2(8),
  PUBLISH_TIME     VARCHAR2(19),
  STATUS           VARCHAR2(1),
  DOMAIN_ID        VARCHAR2(40),
  REMARK           VARCHAR2(256),
  CREATOR          VARCHAR2(80),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19),
  CREATE_TIME      VARCHAR2(19),
  USER_ID          VARCHAR2(40) not null,
  ENABLED          NUMBER(1)
);
comment on table TBL_SYS_NOTICE_USER
  is '通知记录与人员关联信息';
comment on column TBL_SYS_NOTICE_USER.ID
  is '记录ID';
comment on column TBL_SYS_NOTICE_USER.NOTICE_ID
  is '通知ID';
comment on column TBL_SYS_NOTICE_USER.PUBLISHER
  is '发布人账号';
comment on column TBL_SYS_NOTICE_USER.NOTICE_TYPE
  is '公告类型';
comment on column TBL_SYS_NOTICE_USER.PUBLISH_TIME
  is '发布时间';
comment on column TBL_SYS_NOTICE_USER.STATUS
  is '阅读状态.0:未读,1:已读';
comment on column TBL_SYS_NOTICE_USER.DOMAIN_ID
  is '租户ID';
comment on column TBL_SYS_NOTICE_USER.REMARK
  is '备注';
comment on column TBL_SYS_NOTICE_USER.CREATOR
  is '创建者';
comment on column TBL_SYS_NOTICE_USER.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_NOTICE_USER.LAST_MODIFY_TIME
  is '最后修改时间';
comment on column TBL_SYS_NOTICE_USER.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_NOTICE_USER.ENABLED
  is '状态，0为正常1为停用';
alter table TBL_SYS_NOTICE_USER
  add constraint TBL_SYS_NOTICE_USER_PK primary key (ID);
alter table TBL_SYS_NOTICE_USER
  add constraint TBL_SYS_NOTICE_USER1
  check ("ID" IS NOT NULL);
alter table TBL_SYS_NOTICE_USER
  add constraint TBL_SYS_NOTICE_USER2
  check ("NOTICE_ID" IS NOT NULL);
alter table TBL_SYS_NOTICE_USER
  add constraint TBL_SYS_NOTICE_USER3
  check ("NOTICE_TYPE" IS NOT NULL);
alter table TBL_SYS_NOTICE_USER
  add constraint TBL_SYS_NOTICE_USER4
  check ("PUBLISH_TIME" IS NOT NULL);
alter table TBL_SYS_NOTICE_USER
  add constraint TBL_SYS_NOTICE_USER5
  check ("CREATE_TIME" IS NOT NULL);


create table TBL_SYS_ORG
(
  ORG_ID           VARCHAR2(40),
  ORG_NAME         VARCHAR2(80),
  DOMAIN_ID        VARCHAR2(40),
  IS_ORG           NUMBER(1),
  ENABLED          NUMBER(1),
  AREA_CODE        VARCHAR2(40),
  ORG_TYPE         VARCHAR2(8),
  PARENT_ID        VARCHAR2(40),
  ROOT_ID          VARCHAR2(40),
  LEVEL_NO         NUMBER(3),
  IS_LEAF          NUMBER(1),
  IS_SYSTEM        NUMBER(1),
  SORT_NO          NUMBER,
  REMARK           VARCHAR2(256),
  CREATOR          VARCHAR2(80),
  CREATE_TIME      VARCHAR2(19),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19)
);
comment on table TBL_SYS_ORG
  is '系统-组织机构表';
comment on column TBL_SYS_ORG.ORG_ID
  is '机构/部门ID';
comment on column TBL_SYS_ORG.ORG_NAME
  is '机构/部门简称';
comment on column TBL_SYS_ORG.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_ORG.ENABLED
  is '状态，0为正常1为停用';
comment on column TBL_SYS_ORG.AREA_CODE
  is '系统树型编码表中对应的AREA_CODE';
comment on column TBL_SYS_ORG.ORG_TYPE
  is '机构部门类型=系统编码表中对应的ORG_TYPE';
comment on column TBL_SYS_ORG.PARENT_ID
  is '父节点ID';
comment on column TBL_SYS_ORG.ROOT_ID
  is '根节点ID(部门的根节点为某个机构。机构的根节点上顶级机构)';
comment on column TBL_SYS_ORG.IS_LEAF
  is '是否子节点(0是1否)';
comment on column TBL_SYS_ORG.IS_SYSTEM
  is '是否系统记录';
comment on column TBL_SYS_ORG.SORT_NO
  is '排序号';
comment on column TBL_SYS_ORG.REMARK
  is '备注';
comment on column TBL_SYS_ORG.CREATOR
  is '创建者';
comment on column TBL_SYS_ORG.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_ORG.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_ORG.LAST_MODIFY_TIME
  is '最后修改时间';
alter table TBL_SYS_ORG
  add constraint TBL_SYS_ORG_PK primary key (ORG_ID);
alter table TBL_SYS_ORG
  add constraint TBL_SYS_ORG1
  check ("ORG_ID" IS NOT NULL);
create index IDX_ORG_PARENT_ID on TBL_SYS_ORG (PARENT_ID);
create index IDX_ORG_ROOT_ID on TBL_SYS_ORG (ROOT_ID);



create table TBL_SYS_PARAMETER
(
  SYSTEM_PARAMETERS_ID VARCHAR2(40),
  CODE_TYPE            VARCHAR2(60),
  NAME                 VARCHAR2(60),
  CODE                 VARCHAR2(500),
  IS_SYSTEM            NUMBER(1),
  DOMAIN_ID            VARCHAR2(40),
  ORG_ID               VARCHAR2(40),
  ENABLED              NUMBER(1),
  REMARK               VARCHAR2(256),
  CREATOR              VARCHAR2(80),
  CREATE_TIME          VARCHAR2(19),
  LAST_MODIFIER        VARCHAR2(80),
  LAST_MODIFY_TIME     VARCHAR2(19),
  SORT_NO              NUMBER(10),
  ENABLED_TIME         VARCHAR2(19),
  DISABLED_TIME        VARCHAR2(19)
);
comment on table TBL_SYS_PARAMETER
  is '系统参数';
comment on column TBL_SYS_PARAMETER.SYSTEM_PARAMETERS_ID
  is '模块名称';
comment on column TBL_SYS_PARAMETER.CODE_TYPE
  is '配置类型';
comment on column TBL_SYS_PARAMETER.NAME
  is '配置名称';
comment on column TBL_SYS_PARAMETER.CODE
  is '配置值';
comment on column TBL_SYS_PARAMETER.IS_SYSTEM
  is '是否系统';
comment on column TBL_SYS_PARAMETER.DOMAIN_ID
  is '租户ID';
comment on column TBL_SYS_PARAMETER.ORG_ID
  is 'IS_ORG=1的机构ID';
comment on column TBL_SYS_PARAMETER.ENABLED
  is '是否启用';
comment on column TBL_SYS_PARAMETER.REMARK
  is '备注';
comment on column TBL_SYS_PARAMETER.CREATOR
  is '创建者';
comment on column TBL_SYS_PARAMETER.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_PARAMETER.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_PARAMETER.LAST_MODIFY_TIME
  is '最后修改时间';
comment on column TBL_SYS_PARAMETER.SORT_NO
  is '排序号';
comment on column TBL_SYS_PARAMETER.ENABLED_TIME
  is '启用时间';
comment on column TBL_SYS_PARAMETER.DISABLED_TIME
  is '停用时间';
alter table TBL_SYS_PARAMETER
  add constraint TBL_SYS_PARAMETER_PK primary key (SYSTEM_PARAMETERS_ID);
alter table TBL_SYS_PARAMETER
  add constraint TBL_SYS_PARAMETER1
  check ("IS_SYSTEM" IS NOT NULL);
alter table TBL_SYS_PARAMETER
  add constraint TBL_SYS_PARAMETER2
  check ("SYSTEM_PARAMETERS_ID" IS NOT NULL);




create table TBL_SYS_PRIVATE_KEY
(
  PRIVATE_KEY_ID      VARCHAR2(40),
  PRIVATE_KEY         VARCHAR2(256),
  PRIVATE_KEY_VERSION VARCHAR2(40),
  USER_ID             VARCHAR2(40),
  SWITCH              VARCHAR2(40),
  ENABLED             NUMBER(1),
  REMARK              VARCHAR2(256),
  CREATOR             VARCHAR2(80),
  CREATE_TIME         VARCHAR2(19),
  LAST_MODIFIER       VARCHAR2(80),
  LAST_MODIFY_TIME    VARCHAR2(19),
  DOMAIN_ID           VARCHAR2(8)
);
comment on column TBL_SYS_PRIVATE_KEY.ENABLED
  is '状态，0为停用1为正常';
alter table TBL_SYS_PRIVATE_KEY
  add constraint TBL_SYS_PRIVATE_KEY1
  check ("PRIVATE_KEY_ID" IS NOT NULL);




create table TBL_SYS_ROLE
(
  ROLE_ID          VARCHAR2(40),
  DOMAIN_ID        VARCHAR2(40),
  ORG_ID           VARCHAR2(40),
  ROLE_NAME        VARCHAR2(60),
  ROLE_TYPE        VARCHAR2(8),
  IS_SYSTEM        NUMBER(1),
  ENABLED          NUMBER(1),
  REMARK           VARCHAR2(256),
  CREATOR          VARCHAR2(80),
  CREATE_TIME      VARCHAR2(19),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19)
);
comment on table TBL_SYS_ROLE
  is '角色表';
comment on column TBL_SYS_ROLE.ROLE_ID
  is '角色ID';
comment on column TBL_SYS_ROLE.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_ROLE.ORG_ID
  is 'IS_ORG=1的机构ID';
comment on column TBL_SYS_ROLE.ROLE_NAME
  is '角色名称';
comment on column TBL_SYS_ROLE.ROLE_TYPE
  is '角色类型';
comment on column TBL_SYS_ROLE.IS_SYSTEM
  is '0是1否';
comment on column TBL_SYS_ROLE.ENABLED
  is '状态，0为停用1为正常';
comment on column TBL_SYS_ROLE.REMARK
  is '备注';
comment on column TBL_SYS_ROLE.CREATOR
  is '创建者';
comment on column TBL_SYS_ROLE.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_ROLE.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_ROLE.LAST_MODIFY_TIME
  is '最后修改时间';
alter table TBL_SYS_ROLE
  add constraint TBL_SYS_ROLE_PK primary key (ROLE_ID);
alter table TBL_SYS_ROLE
  add constraint TBL_SYS_ROLE1
  check ("ROLE_ID" IS NOT NULL);




create table TBL_SYS_ROLE_ASSIGN
(
  USER_ID VARCHAR2(40) not null,
  ROLE_ID VARCHAR2(40) not null
);
alter table TBL_SYS_ROLE_ASSIGN
  add primary key (USER_ID, ROLE_ID);




create table TBL_SYS_ROLE_FUNC
(
  ROLE_ID     VARCHAR2(40) not null,
  FUNCTION_ID VARCHAR2(40) not null
);
alter table TBL_SYS_ROLE_FUNC
  add primary key (ROLE_ID, FUNCTION_ID);



create table TBL_SYS_SKILL
(
  SKILL_ID          VARCHAR2(40),
  SKILL_CODE        VARCHAR2(40),
  DOMAIN_ID         VARCHAR2(40),
  ORG_ID            VARCHAR2(40),
  SKILL_NAME        VARCHAR2(50),
  SKILL_DESCRIPTION VARCHAR2(120),
  ENABLED           NUMBER(1),
  REMARK            VARCHAR2(256),
  CREATOR           VARCHAR2(80),
  CREATE_TIME       VARCHAR2(19),
  LAST_MODIFIER     VARCHAR2(80),
  LAST_MODIFY_TIME  VARCHAR2(19)
);
comment on table TBL_SYS_SKILL
  is '系统-技能表';
comment on column TBL_SYS_SKILL.SKILL_ID
  is '技能ID';
comment on column TBL_SYS_SKILL.SKILL_CODE
  is '技能ID';
comment on column TBL_SYS_SKILL.DOMAIN_ID
  is '租户ID';
comment on column TBL_SYS_SKILL.ORG_ID
  is 'IS_ORG=1的机构ID';
comment on column TBL_SYS_SKILL.SKILL_NAME
  is '技能名称';
comment on column TBL_SYS_SKILL.SKILL_DESCRIPTION
  is '技能描述';
comment on column TBL_SYS_SKILL.ENABLED
  is '状态，0为停用1为正常';
comment on column TBL_SYS_SKILL.REMARK
  is '备注';
comment on column TBL_SYS_SKILL.CREATOR
  is '创建者';
comment on column TBL_SYS_SKILL.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_SKILL.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_SKILL.LAST_MODIFY_TIME
  is '最后修改时间';
alter table TBL_SYS_SKILL
  add constraint TBL_SYS_SKILL_PK primary key (SKILL_ID);
alter table TBL_SYS_SKILL
  add constraint TBL_SYS_SKILL1
  check ("SKILL_ID" IS NOT NULL);
alter table TBL_SYS_SKILL
  add constraint TBL_SYS_SKILL2
  check ("ORG_ID" IS NOT NULL);


create table TBL_SYS_SUB_SYSTEM
(
  SUB_SYSTEM_ID    VARCHAR2(40),
  SUB_SYSTEM_NAME  VARCHAR2(50),
  SUB_SYSTEM       VARCHAR2(50),
  URL              VARCHAR2(255),
  ENABLED          NUMBER(8),
  CREATOR          VARCHAR2(80),
  CREATE_TIME      VARCHAR2(45),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(45),
  REMARK           VARCHAR2(45),
  DOMAIN_ID        VARCHAR2(45)
);
alter table TBL_SYS_SUB_SYSTEM
  add constraint TBL_SYS_SUB_SYSTEM1
  check ("SUB_SYSTEM_ID" IS NOT NULL);



create table TBL_SYS_THIRD_ACCOUNT
(
  DOMAIN_ID       VARCHAR2(4) not null,
  ORG_ID          VARCHAR2(4) not null,
  THIRD_ACCOUNT   VARCHAR2(50) not null,
  CREATETIME      VARCHAR2(255),
  CREATOR         VARCHAR2(255),
  DOMAINID        VARCHAR2(255),
  LASTMODIFIER    VARCHAR2(255),
  LASTMODIFYTIME  VARCHAR2(255),
  REMARK          VARCHAR2(255),
  ENABLED         NUMBER(1),
  REQUESTURL      VARCHAR2(255),
  THIRDPASSWORD   VARCHAR2(255),
  THIRDSYSTEMNAME VARCHAR2(255),
  USERID          VARCHAR2(255),
  VAILDTYPE       VARCHAR2(255)
);
alter table TBL_SYS_THIRD_ACCOUNT
  add primary key (DOMAIN_ID, ORG_ID, THIRD_ACCOUNT);



create table TBL_SYS_TREE_CODE
(
  CODE_ID          VARCHAR2(40),
  DOMAIN_ID        VARCHAR2(40),
  ORG_ID           VARCHAR2(40),
  CODE_TYPE        VARCHAR2(40),
  CODE_VALUE       VARCHAR2(80),
  CODE_NAME        VARCHAR2(60),
  SORT_NO          NUMBER,
  PARENT_ID        VARCHAR2(40),
  ENABLED          NUMBER(1),
  IS_SYSTEM        NUMBER(1),
  SUB_SYSTEM_ID    VARCHAR2(40),
  REMARK           VARCHAR2(256),
  CREATOR          VARCHAR2(80),
  CREATE_TIME      VARCHAR2(19),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19),
  DISENABLED_TIME  VARCHAR2(40),
  ENABLED_TIME     VARCHAR2(40),
  IS_BY_DEVELOPER  NUMBER(1),
  IS_PARENT        NUMBER(1),
  NODE_PATH        VARCHAR2(500)
);
comment on table TBL_SYS_TREE_CODE
  is '树型编码表';
comment on column TBL_SYS_TREE_CODE.CODE_ID
  is '主键ID';
comment on column TBL_SYS_TREE_CODE.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_TREE_CODE.ORG_ID
  is 'IS_ORG=1的机构ID';
comment on column TBL_SYS_TREE_CODE.CODE_VALUE
  is '编码值';
comment on column TBL_SYS_TREE_CODE.CODE_NAME
  is '编码名称';
comment on column TBL_SYS_TREE_CODE.SORT_NO
  is '排序号';
comment on column TBL_SYS_TREE_CODE.ENABLED
  is '状态，0为停用1为正常';
comment on column TBL_SYS_TREE_CODE.IS_SYSTEM
  is '系统代码对与用户不可修改，删除。0--否,1--是';
comment on column TBL_SYS_TREE_CODE.SUB_SYSTEM_ID
  is '子系统标识';
comment on column TBL_SYS_TREE_CODE.REMARK
  is '备注';
comment on column TBL_SYS_TREE_CODE.CREATOR
  is '创建者';
comment on column TBL_SYS_TREE_CODE.CREATE_TIME
  is '创建时间';
comment on column TBL_SYS_TREE_CODE.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_TREE_CODE.LAST_MODIFY_TIME
  is '最后修改时间';
comment on column TBL_SYS_TREE_CODE.NODE_PATH
  is '树形节点路径的冗余字段';
alter table TBL_SYS_TREE_CODE
  add constraint TBL_SYS_TREE_CODE_PK primary key (CODE_ID);
alter table TBL_SYS_TREE_CODE
  add constraint TBL_SYS_TREE_CODE1
  check ("CODE_ID" IS NOT NULL);



create table TBL_SYS_USER
(
  USER_ID          VARCHAR2(40),
  USER_CODE        VARCHAR2(40),
  USER_NAME        VARCHAR2(60),
  USER_NO          VARCHAR2(40),
  USER_TYPE        VARCHAR2(8),
  DEPT_ID          VARCHAR2(40),
  ORG_ID           VARCHAR2(40),
  PASSWORD         VARCHAR2(32) default 'f5aa100da00005471f59e52cbe761a80',
  PWD_VALID_DAY    VARCHAR2(30),
  BIRTHDAY         VARCHAR2(30),
  SEX              VARCHAR2(8),
  EDUCATION        VARCHAR2(8),
  TELEPHONE        VARCHAR2(20),
  OFFICEPHONE      VARCHAR2(20),
  MOBILE           VARCHAR2(20),
  EMAIL            VARCHAR2(60),
  ADDRESS          VARCHAR2(100),
  POST_CODE        VARCHAR2(20),
  CONTACT_NAME     VARCHAR2(60),
  CONTACT_PHONE    VARCHAR2(20),
  PHOTO            VARCHAR2(256),
  CERVIFICATE_TYPE VARCHAR2(8),
  CERVIFICATE_NO   VARCHAR2(20),
  HIRE_DATE        VARCHAR2(30),
  IS_EDIT          NUMBER(1),
  ENABLED          NUMBER(1) default 1,
  EXTEND_FIELD1    VARCHAR2(20),
  REMARK           VARCHAR2(256),
  CREATOR          VARCHAR2(80),
  CREATE_TIME      VARCHAR2(19),
  LAST_MODIFIER    VARCHAR2(80),
  LAST_MODIFY_TIME VARCHAR2(19),
  DOMAIN_ID        VARCHAR2(40),
  IS_LOGIN         NUMBER(1),
  IS_SIGN          NUMBER(1),
  AGENT_NAME   VARCHAR2(60)
);
comment on table TBL_SYS_USER
  is '用户基类表';
comment on column TBL_SYS_USER.USER_ID
  is '用户ID';
comment on column TBL_SYS_USER.USER_CODE
  is '用户账号(不可为空)';
comment on column TBL_SYS_USER.USER_NAME
  is '用户姓名';
comment on column TBL_SYS_USER.USER_NO
  is '用户内部编号';
comment on column TBL_SYS_USER.USER_TYPE
  is '用户类型(通用编码表中关联USER_TYPE 0系统内置，1开发人员，2客户职员)';
comment on column TBL_SYS_USER.DEPT_ID
  is 'IS_ORG=0的机构ID(不可为空)';
comment on column TBL_SYS_USER.ORG_ID
  is 'IS_ORG=1的机构ID(不可为空)';
comment on column TBL_SYS_USER.PASSWORD
  is '密码';
comment on column TBL_SYS_USER.PWD_VALID_DAY
  is '密码到期日(yyyy-MM-dd)';
comment on column TBL_SYS_USER.BIRTHDAY
  is '生日';
comment on column TBL_SYS_USER.SEX
  is '性别';
comment on column TBL_SYS_USER.EDUCATION
  is '教育水平';
comment on column TBL_SYS_USER.TELEPHONE
  is '联系电话';
comment on column TBL_SYS_USER.OFFICEPHONE
  is '单位电话';
comment on column TBL_SYS_USER.MOBILE
  is '手机';
comment on column TBL_SYS_USER.EMAIL
  is 'EMAIL';
comment on column TBL_SYS_USER.ADDRESS
  is '联系地址';
comment on column TBL_SYS_USER.POST_CODE
  is '邮政编码';
comment on column TBL_SYS_USER.CONTACT_NAME
  is '联系人';
comment on column TBL_SYS_USER.CONTACT_PHONE
  is '联系人电话';
comment on column TBL_SYS_USER.PHOTO
  is '[预留]照片（文件名）';
comment on column TBL_SYS_USER.CERVIFICATE_TYPE
  is '证件类型';
comment on column TBL_SYS_USER.CERVIFICATE_NO
  is '证件号码';
comment on column TBL_SYS_USER.HIRE_DATE
  is '入职时间(yyyy-MM-dd)';
comment on column TBL_SYS_USER.ENABLED
  is '状态，0为停用1为正常';
comment on column TBL_SYS_USER.EXTEND_FIELD1
  is '扩展字段1';
comment on column TBL_SYS_USER.REMARK
  is '备注';
comment on column TBL_SYS_USER.CREATOR
  is '创建者';
comment on column TBL_SYS_USER.CREATE_TIME
  is '创建时间(yyyy-MM-dd)';
comment on column TBL_SYS_USER.LAST_MODIFIER
  is '最后修改者';
comment on column TBL_SYS_USER.LAST_MODIFY_TIME
  is '最后修改时间(yyyy-MM-dd)';
comment on column TBL_SYS_USER.DOMAIN_ID
  is '租户编码';
comment on column TBL_SYS_USER.IS_LOGIN
  is '是否免登录';
comment on column TBL_SYS_USER.IS_SIGN
  is '是否签名';
comment on column TBL_SYS_USER.AGENT_NAME is '坐席昵称';
alter table TBL_SYS_USER
  add constraint TBL_SYS_USER_PK primary key (USER_ID);
alter table TBL_SYS_USER
  add constraint FK19841D9E44775C foreign key (DEPT_ID)
  references TBL_SYS_ORG (ORG_ID);
alter table TBL_SYS_USER
  add constraint FK19841D9E677C0E5D foreign key (ORG_ID)
  references TBL_SYS_ORG (ORG_ID);
alter table TBL_SYS_USER
  add constraint TBL_SYS_USER1
  check ("USER_ID" IS NOT NULL);
create index IDX_SYS_USER_PK on TBL_SYS_USER (USER_CODE||'@'||ORG_ID);




create table TBL_SYS_USER_AGENT
(
  USER_ID  VARCHAR2(40) not null,
  AGENT_ID VARCHAR2(40) not null
);
alter table TBL_SYS_USER_AGENT
  add primary key (USER_ID, AGENT_ID);
alter table TBL_SYS_USER_AGENT
  add constraint FK47249D044AFA37E2 foreign key (USER_ID)
  references TBL_SYS_USER (USER_ID);
alter table TBL_SYS_USER_AGENT
  add constraint FK47249D048ED4800A foreign key (AGENT_ID)
  references TBL_SYS_AGENT_CODE (AGENT_ID);



create table TBL_SYS_USER_GROUP
(
  GROUP_ID VARCHAR2(40) not null,
  USER_ID  VARCHAR2(40) not null
);
comment on table TBL_SYS_USER_GROUP
  is '工作组关联表';
comment on column TBL_SYS_USER_GROUP.GROUP_ID
  is '工作组ID';
comment on column TBL_SYS_USER_GROUP.USER_ID
  is '用户ID';
alter table TBL_SYS_USER_GROUP
  add constraint PK_TBL_SYS_USER_GROUP primary key (GROUP_ID, USER_ID);
alter table TBL_SYS_USER_GROUP
  add constraint FK477E507E4817E7BD foreign key (GROUP_ID)
  references TBL_SYS_GROUP (GROUP_ID);
alter table TBL_SYS_USER_GROUP
  add constraint FK477E507E4AFA37E2 foreign key (USER_ID)
  references TBL_SYS_USER (USER_ID);



create table TBL_USER_EVENT_LOG
(
  LOG_ID           VARCHAR2(1024) not null,
  SESSION_ID       VARCHAR2(1024),
  DESTINATION      VARCHAR2(1024),
  DOMAIN_ID        VARCHAR2(1024),
  CHANNEL_TYPE     VARCHAR2(1024),
  CHANNEL_ID       VARCHAR2(1024),
  USER_ID          VARCHAR2(1024),
  EVENT            VARCHAR2(1024),
  EVENT_TIME       VARCHAR2(1024),
  TIMESTAMP        INTEGER,
  CREATOR          VARCHAR2(1024),
  CREATE_TIME      VARCHAR2(1024),
  LAST_MODIFIER    VARCHAR2(1024),
  LAST_MODIFY_TIME VARCHAR2(1024),
  REMARK           VARCHAR2(1024)
);
comment on table TBL_USER_EVENT_LOG
  is '记录用户上线下线时间操作轨迹';
comment on column TBL_USER_EVENT_LOG.LOG_ID
  is 'LOG_ID';
comment on column TBL_USER_EVENT_LOG.DESTINATION
  is '所属消息通道';
comment on column TBL_USER_EVENT_LOG.DOMAIN_ID
  is '租户ID';
comment on column TBL_USER_EVENT_LOG.CHANNEL_TYPE
  is '渠道类型';
comment on column TBL_USER_EVENT_LOG.CHANNEL_ID
  is '渠道ID';
comment on column TBL_USER_EVENT_LOG.USER_ID
  is '用户在渠道中的标示';
comment on column TBL_USER_EVENT_LOG.EVENT
  is '事件';
comment on column TBL_USER_EVENT_LOG.EVENT_TIME
  is '事件时间';
comment on column TBL_USER_EVENT_LOG.TIMESTAMP
  is '事件时间戳';
comment on column TBL_USER_EVENT_LOG.CREATOR
  is '创建者';
comment on column TBL_USER_EVENT_LOG.CREATE_TIME
  is '创建时间';
comment on column TBL_USER_EVENT_LOG.LAST_MODIFIER
  is '修改者';
comment on column TBL_USER_EVENT_LOG.LAST_MODIFY_TIME
  is '修改时间';
comment on column TBL_USER_EVENT_LOG.REMARK
  is '备注';
alter table TBL_USER_EVENT_LOG
  add constraint PK_TBL_USER_EVENT_LOG primary key (LOG_ID);


-- ----------------------------
-- Table structure for TBL_SNS_USER_EVENT_LOG
-- ----------------------------
CREATE TABLE "TBL_SNS_USER_EVENT_LOG" (
"LOG_ID" VARCHAR2(40 BYTE) NOT NULL ,
"SESSION_ID" VARCHAR2(255 BYTE) NULL ,
"DESTINATION" VARCHAR2(255 BYTE) NULL ,
"DOMAIN_ID" VARCHAR2(40 BYTE) NULL ,
"CHANNEL_TYPE" VARCHAR2(40 BYTE) NULL ,
"CHANNEL_ID" VARCHAR2(40 BYTE) NULL ,
"USER_ID" VARCHAR2(40 BYTE) NULL ,
"EVENT" VARCHAR2(40 BYTE) NULL ,
"EVENT_TIME" VARCHAR2(19 BYTE) NULL ,
"TIMESTAMP" NUMBER NULL ,
"CREATOR" VARCHAR2(80 BYTE) NULL ,
"CREATE_TIME" VARCHAR2(19 BYTE) NULL ,
"LAST_MODIFIER" VARCHAR2(80 BYTE) NULL ,
"LAST_MODIFY_TIME" VARCHAR2(19 BYTE) NULL ,
"REMARK" VARCHAR2(200 BYTE) NULL
);
COMMENT ON TABLE "TBL_SNS_USER_EVENT_LOG" IS '记录用户上线下线时间操作轨迹';
COMMENT ON COLUMN "TBL_SNS_USER_EVENT_LOG"."LOG_ID" IS 'LOG_ID';
COMMENT ON COLUMN "TBL_SNS_USER_EVENT_LOG"."DESTINATION" IS '所属消息通道';
COMMENT ON COLUMN "TBL_SNS_USER_EVENT_LOG"."DOMAIN_ID" IS '租户ID';
COMMENT ON COLUMN "TBL_SNS_USER_EVENT_LOG"."CHANNEL_TYPE" IS '渠道类型';
COMMENT ON COLUMN "TBL_SNS_USER_EVENT_LOG"."CHANNEL_ID" IS '渠道ID';
COMMENT ON COLUMN "TBL_SNS_USER_EVENT_LOG"."USER_ID" IS '用户在渠道中的标示';
COMMENT ON COLUMN "TBL_SNS_USER_EVENT_LOG"."EVENT" IS '事件';
COMMENT ON COLUMN "TBL_SNS_USER_EVENT_LOG"."EVENT_TIME" IS '事件时间';
COMMENT ON COLUMN "TBL_SNS_USER_EVENT_LOG"."TIMESTAMP" IS '事件时间戳';
COMMENT ON COLUMN "TBL_SNS_USER_EVENT_LOG"."CREATOR" IS '创建者';
COMMENT ON COLUMN "TBL_SNS_USER_EVENT_LOG"."CREATE_TIME" IS '创建时间';
COMMENT ON COLUMN "TBL_SNS_USER_EVENT_LOG"."LAST_MODIFIER" IS '修改者';
COMMENT ON COLUMN "TBL_SNS_USER_EVENT_LOG"."LAST_MODIFY_TIME" IS '修改时间';
COMMENT ON COLUMN "TBL_SNS_USER_EVENT_LOG"."REMARK" IS '备注';


-- ----------------------------
-- Indexes structure for table TBL_SNS_USER_EVENT_LOG
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_SNS_USER_EVENT_LOG
-- ----------------------------
ALTER TABLE "TBL_SNS_USER_EVENT_LOG" ADD CHECK ("LOG_ID" IS NOT NULL);
ALTER TABLE "TBL_SNS_USER_EVENT_LOG" ADD CHECK ("LOG_ID" IS NOT NULL);
ALTER TABLE "TBL_SNS_USER_EVENT_LOG" ADD CHECK ("LOG_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_SNS_USER_EVENT_LOG
-- ----------------------------
ALTER TABLE "TBL_SNS_USER_EVENT_LOG" ADD PRIMARY KEY ("LOG_ID");



-- ----------------------------
-- Table structure for TBL_SNS_CHAT_MSG
-- ----------------------------
CREATE TABLE "TBL_SNS_CHAT_MSG" (
"ID" VARCHAR2(40 BYTE) NOT NULL ,
"DOMAIN_ID" VARCHAR2(40 BYTE) NULL ,
"CHANNEL_TYPE" VARCHAR2(10 BYTE) NULL ,
"CHANNEL_ID" VARCHAR2(50 BYTE) NULL ,
"FROM_USER" VARCHAR2(100 BYTE) NULL ,
"FROM_USER_TYPE" VARCHAR2(20 BYTE) NULL ,
"TO_USER" VARCHAR2(100 BYTE) NULL ,
"TO_USER_TYPE" VARCHAR2(20 BYTE) NULL ,
"MSG_TYPE" VARCHAR2(15 BYTE) NULL ,
"CONTENT_TXT" VARCHAR2(4000 BYTE) NULL ,
"MEDIA_ID" VARCHAR2(100 BYTE) NULL ,
"MEDIA_URL" VARCHAR2(4000 BYTE) NULL ,
"CONTACT_TIME" VARCHAR2(21 BYTE) NULL ,
"SESSION_ID" VARCHAR2(40 BYTE) NULL ,
"DESTINATION" VARCHAR2(500 BYTE) NULL ,
"TIMESTAMP" NUMBER NULL ,
"CREATOR" VARCHAR2(80 BYTE) NULL ,
"CREATE_TIME" VARCHAR2(21 BYTE) NULL ,
"LAST_MODIFIER" VARCHAR2(80 BYTE) NULL ,
"LAST_MODIFY_TIME" VARCHAR2(21 BYTE) NULL ,
"REMARK" VARCHAR2(200 BYTE) NULL ,
"FROM_USER_NAME" VARCHAR2(100 BYTE) NULL ,
"TO_USER_NAME" VARCHAR2(100 BYTE) NULL ,
"SEND_TYPE" VARCHAR2(10 BYTE) NULL ,
"TRANSFER_ID" VARCHAR2(40 BYTE) NULL ,
"SKILL_ID" VARCHAR2(40 BYTE) NULL ,
"ORG_ID" VARCHAR2(40 BYTE) NULL ,
"SIGN_TYPE" VARCHAR2(2 BYTE) NULL
);
COMMENT ON TABLE "TBL_SNS_CHAT_MSG" IS '记录客服接触信息记录';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."ID" IS 'ID';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."DOMAIN_ID" IS '租户ID';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."CHANNEL_TYPE" IS '渠道类型';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."CHANNEL_ID" IS '渠道ID';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."FROM_USER" IS '发送人';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."FROM_USER_TYPE" IS '发送人身份';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."TO_USER" IS '接收人';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."TO_USER_TYPE" IS '接收人身份';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."MSG_TYPE" IS '消息类型';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."CONTENT_TXT" IS '文本';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."MEDIA_ID" IS '媒体ID';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."MEDIA_URL" IS 'MEDIA访问地址';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."CONTACT_TIME" IS '首次接触时间';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."TIMESTAMP" IS '时间戳';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."CREATOR" IS '创建者';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."CREATE_TIME" IS '创建时间';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."LAST_MODIFIER" IS '修改者';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."LAST_MODIFY_TIME" IS '修改时间';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."REMARK" IS '备注';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."SEND_TYPE" IS ' 1表示访客发的，2表示坐席发的';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."TRANSFER_ID" IS '转接';
COMMENT ON COLUMN "TBL_SNS_CHAT_MSG"."SKILL_ID" IS ' 技能组ID';


-- ----------------------------
-- Indexes structure for table TBL_SNS_CHAT_MSG
-- ----------------------------
CREATE INDEX "chat_msg_session_id_index"
ON "TBL_SNS_CHAT_MSG" ("SESSION_ID" ASC);
CREATE INDEX "union_chat_msg_index"
ON "TBL_SNS_CHAT_MSG" ("DOMAIN_ID" ASC, "SESSION_ID" ASC);

-- ----------------------------
-- Checks structure for table TBL_SNS_CHAT_MSG
-- ----------------------------
ALTER TABLE "TBL_SNS_CHAT_MSG" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_SNS_CHAT_MSG" ADD CHECK ("ID" IS NOT NULL);
ALTER TABLE "TBL_SNS_CHAT_MSG" ADD CHECK ("ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_SNS_CHAT_MSG
-- ----------------------------
ALTER TABLE "TBL_SNS_CHAT_MSG" ADD PRIMARY KEY ("ID");


-- ----------------------------
-- Table structure for TBL_SNS_CUST_SERV_EVENT_LOG
-- ----------------------------
CREATE TABLE "TBL_SNS_CUST_SERV_EVENT_LOG" (
"LOG_ID" VARCHAR2(40 BYTE) NOT NULL ,
"SESSION_ID" VARCHAR2(40 BYTE) NULL ,
"DOMAIN_ID" VARCHAR2(40 BYTE) NULL ,
"DESTINATION" VARCHAR2(400 BYTE) NULL ,
"CUST_SERV_ID" VARCHAR2(40 BYTE) NULL ,
"EVENT" VARCHAR2(40 BYTE) NULL ,
"EVENT_TIME" VARCHAR2(40 BYTE) NULL ,
"TIMESTAMP" NUMBER NULL ,
"CREATOR" VARCHAR2(80 BYTE) NULL ,
"CREATE_TIME" VARCHAR2(20 BYTE) NULL ,
"LAST_MODIFIER" VARCHAR2(80 BYTE) NULL ,
"LAST_MODIFY_TIME" VARCHAR2(19 BYTE) NULL ,
"REMARK" VARCHAR2(200 BYTE) NULL
);
COMMENT ON TABLE "TBL_SNS_CUST_SERV_EVENT_LOG" IS '客服操作轨迹';
COMMENT ON COLUMN "TBL_SNS_CUST_SERV_EVENT_LOG"."LOG_ID" IS 'LOG_ID';
COMMENT ON COLUMN "TBL_SNS_CUST_SERV_EVENT_LOG"."DOMAIN_ID" IS '租户ID';
COMMENT ON COLUMN "TBL_SNS_CUST_SERV_EVENT_LOG"."DESTINATION" IS '所属消息通道';
COMMENT ON COLUMN "TBL_SNS_CUST_SERV_EVENT_LOG"."CUST_SERV_ID" IS '用户在渠道中的标示';
COMMENT ON COLUMN "TBL_SNS_CUST_SERV_EVENT_LOG"."EVENT" IS '事件';
COMMENT ON COLUMN "TBL_SNS_CUST_SERV_EVENT_LOG"."EVENT_TIME" IS '事件时间';
COMMENT ON COLUMN "TBL_SNS_CUST_SERV_EVENT_LOG"."TIMESTAMP" IS '时间戳';
COMMENT ON COLUMN "TBL_SNS_CUST_SERV_EVENT_LOG"."CREATOR" IS '创建者';
COMMENT ON COLUMN "TBL_SNS_CUST_SERV_EVENT_LOG"."CREATE_TIME" IS '创建时间';
COMMENT ON COLUMN "TBL_SNS_CUST_SERV_EVENT_LOG"."LAST_MODIFIER" IS '修改者';
COMMENT ON COLUMN "TBL_SNS_CUST_SERV_EVENT_LOG"."LAST_MODIFY_TIME" IS '修改时间';
COMMENT ON COLUMN "TBL_SNS_CUST_SERV_EVENT_LOG"."REMARK" IS '备注';


-- ----------------------------
-- Indexes structure for table TBL_SNS_CUST_SERV_EVENT_LOG
-- ----------------------------

-- ----------------------------
-- Checks structure for table TBL_SNS_CUST_SERV_EVENT_LOG
-- ----------------------------
ALTER TABLE "TBL_SNS_CUST_SERV_EVENT_LOG" ADD CHECK ("LOG_ID" IS NOT NULL);
ALTER TABLE "TBL_SNS_CUST_SERV_EVENT_LOG" ADD CHECK ("LOG_ID" IS NOT NULL);
ALTER TABLE "TBL_SNS_CUST_SERV_EVENT_LOG" ADD CHECK ("LOG_ID" IS NOT NULL);

-- ----------------------------
-- Primary Key structure for table TBL_SNS_CUST_SERV_EVENT_LOG
-- ----------------------------
ALTER TABLE "TBL_SNS_CUST_SERV_EVENT_LOG" ADD PRIMARY KEY ("LOG_ID");


create sequence HIBERNATE_SEQUENCE
minvalue 1
maxvalue 9999999999999999999999999999
start with 1
increment by 1
cache 20;

alter table tbl_sys_user add (AREA_CODE VARCHAR2(20) null);
comment on column tbl_sys_user.AREA_CODE is '区域部门';
alter table tbl_sys_user add (VDN_ID VARCHAR2(20) null);
comment on column tbl_sys_user.VDN_ID is '呼叫中心编号';
alter table tbl_sys_user add (NATION_CODE VARCHAR2(20) null);
comment on column tbl_sys_user.NATION_CODE is '民族';
alter table tbl_sys_user add (IDENTITY_ID VARCHAR2(40) null);
comment on column tbl_sys_user.IDENTITY_ID is '身份证号';
alter table tbl_sys_user add (IS_UNICOM NUMBER(1) null);
comment on column tbl_sys_user.IS_UNICOM is '是否联通员工 0：否 1：是';
alter table tbl_sys_user add (RESIGNATION_DATE VARCHAR2(19) null);
comment on column tbl_sys_user.RESIGNATION_DATE is '离职时间';

alter table tbl_sys_org add (DIVSION_ID VARCHAR2(40) null);
comment on column tbl_sys_org.DIVSION_ID is '行政区域ID';
alter table tbl_sys_org add (COMPANY_ID VARCHAR2(40) null);
comment on column tbl_sys_org.COMPANY_ID is '公司ID';
alter table tbl_sys_org add (IS_AREA NUMBER(1) null);
comment on column tbl_sys_org.IS_AREA is '是否地市';
alter table tbl_sys_org add (IS_VDN NUMBER(1) null);
comment on column tbl_sys_org.IS_VDN is '是否VDN 0：否 1：是';
alter table tbl_sys_org add (VDN_ID VARCHAR2(20) null);
comment on column tbl_sys_org.VDN_ID is '呼叫中心ID';

alter table tbl_sys_role add (AREA_CODE VARCHAR2(20) null);
comment on column tbl_sys_role.AREA_CODE is '地市区域';
alter table tbl_sys_role add (ROLE_CODE VARCHAR2(60) null);
comment on column tbl_sys_role.ROLE_CODE is '兼容老系统CS客户端';

alter table tbl_sys_function add (IS_MENU NUMBER(1) default 1 not null);
comment on column tbl_sys_function.IS_MENU is '是否是目录';

alter table tbl_sys_group add (GROUP_TYPE VARCHAR2(20) null);
comment on column tbl_sys_group.GROUP_TYPE is '组类型';

alter table tbl_sys_third_account add (VAILD_TYPE_CLASS VARCHAR2(20) null);
comment on column tbl_sys_third_account.VAILD_TYPE_CLASS is '验证方式class类';

alter table tbl_sys_user add (FIELD_ID VARCHAR2(40) null);
comment on column tbl_sys_user.FIELD_ID is '属性ID';

--用户扩展属性表--
CREATE TABLE "TBL_SYS_FIELD"
(
  "FIELD_ID" VARCHAR2(40 BYTE) NOT NULL,
  "USER_ID"  VARCHAR2(40 BYTE) NULL,
  "FIELD1"   VARCHAR2(200 BYTE) NULL,
  "FIELD2"   VARCHAR2(200 BYTE) NULL,
  "FIELD3"   VARCHAR2(200 BYTE) NULL,
  "FIELD4"   VARCHAR2(200 BYTE) NULL,
  "FIELD5"   VARCHAR2(200 BYTE) NULL,
  "FIELD6"   VARCHAR2(200 BYTE) NULL,
  "FIELD7"   VARCHAR2(200 BYTE) NULL,
  "FIELD8"   VARCHAR2(200 BYTE) NULL,
  "FIELD9"   VARCHAR2(200 BYTE) NULL,
  "FIELD10"  VARCHAR2(200 BYTE) NULL
);
COMMENT ON TABLE "TBL_SYS_FIELD" IS '用户扩展属性表';
COMMENT ON COLUMN "TBL_SYS_FIELD"."FIELD_ID" IS '属性表ID';
COMMENT ON COLUMN "TBL_SYS_FIELD"."USER_ID" IS '用户ID';
COMMENT ON COLUMN "TBL_SYS_FIELD"."FIELD1"  IS '属性值1';
COMMENT ON COLUMN "TBL_SYS_FIELD"."FIELD2"  IS '属性值2';
COMMENT ON COLUMN "TBL_SYS_FIELD"."FIELD3"  IS '属性值3';
COMMENT ON COLUMN "TBL_SYS_FIELD"."FIELD4"  IS '属性值4';
COMMENT ON COLUMN "TBL_SYS_FIELD"."FIELD5"  IS '属性值5';
COMMENT ON COLUMN "TBL_SYS_FIELD"."FIELD6"  IS '属性值6';
COMMENT ON COLUMN "TBL_SYS_FIELD"."FIELD7"  IS '属性值7';
COMMENT ON COLUMN "TBL_SYS_FIELD"."FIELD8"  IS '属性值8';
COMMENT ON COLUMN "TBL_SYS_FIELD"."FIELD9"  IS '属性值9';
COMMENT ON COLUMN "TBL_SYS_FIELD"."FIELD10"  IS '属性值10';

ALTER TABLE "TBL_SYS_FIELD" ADD CONSTRAINT PK_FIELD_ID PRIMARY KEY ("FIELD_ID");


CREATE OR REPLACE PROCEDURE TBL_ECHARTS_PROC  IS
BEGIN
  UPDATE TBL_ECHARTS_DATA a SET a.value=trunc(dbms_random.value(80,100)) where a.code='scjjl' ;
  UPDATE TBL_ECHARTS_DATA a SET a.value=trunc(dbms_random.value(80,100)) where a.code='fwmyl' ;
  UPDATE TBL_ECHARTS_DATA a SET a.value=trunc(dbms_random.value(80,100)) where a.code='20mjtl' ;


update tbl_echarts_data set value=trunc(dbms_random.value(50,60)) where code='hb_map' and category='宜昌市';
update tbl_echarts_data set value=trunc(dbms_random.value(50,80)) where code='hb_map' and category='荆门市';
update tbl_echarts_data set value=trunc(dbms_random.value(40,80)) where code='hb_map' and category='武汉市';
update tbl_echarts_data set value=trunc(dbms_random.value(80,100)) where code='hb_map' and category='恩施土家族苗族自治州';

  COMMIT;
EXCEPTION
  WHEN OTHERS THEN
    DBMS_OUTPUT.PUT_LINE('Exception happened,data was rollback');
    ROLLBACK;
END;
/


create or replace function oracle_to_unix(in_date IN DATE) return number is

begin

  return( (in_date -TO_DATE('19700101','yyyymmdd'))*86400 - TO_NUMBER(SUBSTR(TZ_OFFSET(sessiontimezone),1,3))*3600);

end oracle_to_unix;
/



create or replace function unix_to_oracle(in_number NUMBER) return date is

begin

  return(TO_DATE('19700101','yyyymmdd') + in_number/86400 +TO_NUMBER(SUBSTR(TZ_OFFSET(sessiontimezone),1,3))/24);

end unix_to_oracle;
/
